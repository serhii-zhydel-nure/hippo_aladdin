﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace PSV
{
    public static class ExternalLinksManager
    {
        #region Variables

        private static List<IExternalLinkView> links_list = new List<IExternalLinkView>();

        public static Action<bool> OnExternalLinksVisible;

        private static bool is_interstitial_shown;
        private static bool is_rewarded_shown;
        #endregion

        [AwakeStatic]
        public static void Subscribe()
        {
            AwakeStaticAttribute.Done( typeof( ExternalLinksManager ) );
            ManagerGoogle.OnInterstitialShown += OnInterstitialShown;
            ManagerGoogle.OnInterstitialClosed += OnInterstitialClosed;
            ManagerGoogle.OnRewardedShown += OnRewardedShown;
            ManagerGoogle.OnRewardedClosed += OnRewardedClosed;
        }

        private static void OnInterstitialShown()
        {
            is_interstitial_shown = true;
            UpdateLinks();
        }

        private static void OnInterstitialClosed()
        {
            is_interstitial_shown = false;
            UpdateLinks();
        }
        private static void OnRewardedShown()
        {
            is_rewarded_shown = true;
            UpdateLinks();
        }
        private static void OnRewardedClosed()
        {
            is_rewarded_shown = false;
            UpdateLinks();
        }

        public static void AddLink (IExternalLinkView externalLink)
		{
			if (!links_list.Contains ( externalLink ))
			{
				links_list.Add ( externalLink );
				UpdateLinks ( );
			}
		}

		public static void DeleteLink (IExternalLinkView externalLink)
		{
			if (links_list.Contains ( externalLink ))
			{
				links_list.Remove ( externalLink );
				UpdateLinks ( );
			}
		}

		private static void UpdateLinks ()
		{
			int selected_link = -1;

			if (links_list.Count > 0)
			{
				if (!IsFullscreenAdVisible ( ))
				{
					for (int i = links_list.Count - 1; i >= 0; i--)
					{
						if (!links_list [i].IsStatic)
						{
							selected_link = i;
							break;
						}
					}
					if (selected_link < 0)
					{
						selected_link = links_list.Count - 1;
					}
				}
				ShowLink ( selected_link );
			}
			ExternalLinkVisible ( selected_link >= 0 || IsFullscreenAdVisible ( ) );
		}


		private static bool IsFullscreenAdVisible ()
		{
			return is_interstitial_shown || is_rewarded_shown;
		}

		private static void ShowLink (int link)
		{
			for (int i = 0; i < links_list.Count; i++)
			{
				links_list [i].Show ( link == i );
			}
		}


		#region Service

		private static void ExternalLinkVisible (bool param)
		{
			if (OnExternalLinksVisible != null)
			{
				OnExternalLinksVisible ( param );
			}
		}

		#endregion
	}

}