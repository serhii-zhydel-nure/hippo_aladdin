﻿namespace PSV
{
	public interface IExternalLinkBuilder
	{
		//called when link present on screen
		void CreateLink (); // call here ExternalLinksManager.Instance.AddLink (your class)
		//called when link no longer present on screen
		void DestroyLink (); // call here ExternalLinksManager.Instance.DeleteLink (your class)
	}
}