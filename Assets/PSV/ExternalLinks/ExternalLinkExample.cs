﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PSV
{

	public class ExternalLinkExample :MonoBehaviour, IExternalLink
	{
#pragma warning disable 649
		private bool isStatic;
#pragma warning restore 649

		public bool IsStatic
		{
			get
			{
				return isStatic;
			}
		}

		public void CreateLink ()
		{
			ExternalLinksManager.AddLink ( this );
		}

		public void DestroyLink ()
		{
            ExternalLinksManager.DeleteLink ( this );
		}

		public void Show (bool param)
		{
			//show or hide here the link 
			//be aware of making it the same as CreateLink or DestroyLink if it is not the way you want your class to act in that way
		}
	}
}