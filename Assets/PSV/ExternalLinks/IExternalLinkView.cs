﻿namespace PSV
{
	public interface IExternalLinkView
	{
		bool IsStatic
		{
			get;
		}

		void Show (bool param); //be careful not to call here methods that implement IExternalLinkBuilder - it can loop the app
	}
}