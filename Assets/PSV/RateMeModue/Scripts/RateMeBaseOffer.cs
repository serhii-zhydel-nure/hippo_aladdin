﻿using UnityEngine;
using System.Collections;
using PSV;

namespace RateMePlugin
{
    public abstract class RataMeBaseOffer : ISceneOffer
    {
        public int priority { get { return 50; } }

        public virtual OfferReply TryShow( Scenes current_scene, Scenes target_scene )
        {
            if (CanShowRateMe( current_scene, target_scene ) && ExtraConditions( current_scene, target_scene ))
            {
                if (RateMeModule.ShowDialogue())
                {
                    OnShowDialog( current_scene, target_scene );
                    return OfferReply.ShownNoAds;
                }
            }
            return OfferReply.Skipped;
        }

        public virtual bool IsEmpty()
        {
            return ScenesList().Length == 0;
        }

        protected abstract bool ExtraConditions( Scenes current_scene, Scenes target_scene );
        protected abstract void OnShowDialog( Scenes current_scene, Scenes target_scene );

        protected bool CanShowRateMe( Scenes current_scene, Scenes target_scene )
        {
            if (target_scene == current_scene)
                return false;
            var sceneList = ScenesList();
            for (int i = 0; i < sceneList.Length; i++)
            {
                if (sceneList[i] == current_scene)
                {
                    return true;
                }
            }
            return false;
        }

        public abstract Scenes[] ScenesList();

        public abstract Scenes OfferScene();
    }
}