﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


namespace RateMePlugin
{
    [RequireComponent( typeof( Button ) )]
    public class RateButtonScript : MonoBehaviour, IPointerClickHandler
    {
        public static event System.Action OnRateClicked;

        public bool ignoreStars = false;

        private Button
            btn;

        public void Awake()
        {
            btn = GetComponent<Button>();
        }

        public void OnEnable()
        {
            if (!ignoreStars)
            {
                btn.interactable = false;
                StarScript.OnStarClicked += OnStarClicked;
            }
        }

        public void OnDisable()
        {
            if (!ignoreStars)
                StarScript.OnStarClicked -= OnStarClicked;
        }

        void OnStarClicked( bool _positive, float x )
        {
            btn.interactable = true;
        }


        public void OnPointerClick( PointerEventData eventData )
        {
            if (ignoreStars)
                RateMeModule.positive = true;
            if (btn.interactable && OnRateClicked != null)
            {
                OnRateClicked();
            }
        }

    }
}
