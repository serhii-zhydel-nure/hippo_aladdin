﻿//#define BRAND_NEW_PROTOTYPE

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using PSV;

namespace RateMePlugin
{
	[RequireComponent ( typeof ( Image ) )]
	public class PanelScript :MonoBehaviour, IExternalLink
	{
		private bool
			//dialogue_shown = false,
			animate = false;

		private Image
			img;

		//private GameObject
		//	child;

		public float
			anim_speed = 0.05f;

		private float
			target_alpha = 100 / 255f;

		public void OnEnable ()
		{
			ShowBackground ( );
			CreateLink ( );
			RateMeModule.PlaySound ( true );
		}


		public void OnDisable ()
		{
			DestroyLink ( );
		}

		public void Awake ()
		{
			img = GetComponent<Image> ( );
			//Transform t = transform.GetChild ( 0 );
			//if (t != null)
			//{
			//	child = t.gameObject;
			//}
			//else
			//{
			//	Debug.LogError ( "RateMe dialog: You are missing Dialog window in Panel" );
			//}
		}

		void ShowBackground ()
		{
			SetAlpha ( -img.color.a );
			animate = true;
		}

		private void ActivatePanel (bool param)
		{
			this.transform.localScale = param ? Vector3.one : Vector3.zero;
		}

		// Update is called once per frame
		void Update ()
		{
			if (animate)
			{
				float step = /*(dialogue_shown ? 1: -1) * */ Time.unscaledDeltaTime;
				float res = SetAlpha ( step );
				if (res == target_alpha /*|| res == 0*/)
				{
					animate = false;
					//if (!dialogue_shown)
					//{
					//    this.gameObject.SetActive ( false );
					//}
				}
			}
		}

		float SetAlpha (float val)
		{
			Color col = img.color;
			col.a += val;
			if (col.a >= target_alpha)
				col.a = target_alpha;
			//if (col.a < 0.05f)
			//    col.a = 0;
			img.color = col;
			return col.a;
		}


		#region External link implementation
		public bool IsStatic
		{
			get
			{
#if BRAND_NEW_PROTOTYPE
				return RateMeModule.Instance.scene_mode;
#else
				return false;
#endif
			}
		}

		public void Show (bool param)
		{
			ActivatePanel ( param );
		}

		public void CreateLink ()
		{
			ExternalLinksManager.AddLink ( this );
		}

		public void DestroyLink ()
		{
            ExternalLinksManager.DeleteLink ( this );
		}
		#endregion
	}
}
