﻿#define BRAND_NEW_PROTOTYPE // Prototype 3 or newer
#define Prototype_4_2_OR_NEWER

using UnityEngine;
//using System.Collections;
using System.Collections.Generic;
using PSV;
using System.Collections;

/// <summary>
/// ChangeLog
/// v2
///     - fixed bug when variables were not saved on change
///     - fixed CanShow() - replaced || operator with &&
/// v3
///     - optimised canvas settings
///     - use_time_scale set to false by default
///     - PanelScript now uses Time.unscaledDeltaTime for fade animation
/// v4
///		- implemented scene mode as option
///		- use_time_scale deprecated - practice shows it was usless feature
///		- Panel script now implements IExternalLnk
///	v5
///		- Changed Bhavior of close button (now it also goes to market if rate positive)
///	v6
///		- 4-5 stars click leads directly to PlayMarket without submitting
///	v7
///		- closed_limit changed from 5 to 3
///	v8
///	    - implemented delay before auto rate when 4-5 stars clicked
///		
/// </summary>


namespace RateMePlugin
{

    public static class RateMeModule
    {
        //public static RateMeModule Instance;

        private const string
            closed_var = "RateMeModule:closed",
            negative_var = "RateMeModule:negative",
            positive_var = "RateMeModule:positive";

        private const int
            negative_limit = 2,
            closed_limit = 3;
        private static int
            closed_val,
            negative_val,
            positive_val;

#if BRAND_NEW_PROTOTYPE
#if Prototype_4_2_OR_NEWER
        private static float
            time_interval { get { return RateMeOffer.time_interval; } }
#else
        public static Scenes
            rate_scene = Scenes.RateMeScene;
#endif
        private static Scenes 
            target_scene;           // TODO: Can be replaced to OffersManager.suspendedScene
#else
        private const float
            time_interval = 60; //60
#endif
        private static float
            last_time = -time_interval; //to show dialogue after first call - set to zero to avoid early activation

        private static bool
            is_shown = false;

        public static bool
            positive = false; // Unlock Rate Application

#if BRAND_NEW_PROTOTYPE
        public static bool
            scene_mode = true;
#else
		private bool
			scene_mode = false;
#endif

        private static AudioSource src;

        private const string clip_prefix = "rateme";

#if PROTOTYPE_4_OR_NEWER
        [AwakeStatic]
#else
        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.BeforeSceneLoad )]
#endif
        public static void AwakeStatic()
        {
            InitializeAudio();
            LoadResources();
            GetVariables();
            Subscribe();
        }


        public struct LocalisedData
        {
            public AudioClip audio;
            public string txt;
            public LocalisedData( AudioClip a, string t )
            {
                audio = a;
                txt = t;
            }

            public override string ToString()
            {
                return "[audio = " + (audio != null ? audio.name : "null") + "], [txt = " + txt + "]";
            }
        }

        private static Dictionary<Languages.Language, LocalisedData> localised_data;


        public static GameObject
            rate_me_panel;
        

        static void InitializeAudio()
        {
            GameObject go = new GameObject( "RateMeAudio" );
            src = go.AddComponent<AudioSource>();
            GameObject.DontDestroyOnLoad( go );
        }

        static void LoadResources()
        {
            localised_data = new Dictionary<Languages.Language, LocalisedData>();
            LoadTexts();
            LoadSounds();
            LoadPrefab();
        }

        static void LoadSounds()
        {
            Object[] res = Resources.LoadAll( "RateMeModuleRes/", typeof( AudioClip ) );
            if (res != null)
            {
                for (int i = 0; i < res.Length; i++)
                {
                    AudioClip clip = res[i] as AudioClip;
                    string[] name = clip.name.Split( '_' );
                    if (name.Length > 1 && name[0].Equals( clip_prefix ))
                    {
                        Languages.Language ln = (Languages.Language) System.Enum.Parse( typeof( Languages.Language ), name[1] );
                        if (System.Enum.IsDefined( typeof( Languages.Language ), ln ))
                        {
                            if (localised_data.ContainsKey( ln ))
                            {
                                LocalisedData d = localised_data[ln];
                                d.audio = clip;
                                localised_data[ln] = d;
                            }
                            else
                            {
                                localised_data.Add( ln, new LocalisedData( clip, "" ) );
                            }
                        }
                    }
                }
            }
        }

        static void LoadTexts()
        {
            TextAsset txt = (TextAsset) Resources.Load( "RateMeModuleRes/RateMeText", typeof( TextAsset ) );
            if (txt != null)
            {
                string[] items = System.Text.RegularExpressions.Regex.Split( txt.text, "\r\n|\r|\n" );
                for (int i = 0; i < items.Length; i++)
                {
                    int splitter = items[i].IndexOf( ":" );
                    if (splitter > 0)
                    {
                        string key = items[i].Substring( 0, splitter );
                        string val = items[i].Substring( splitter + 1 );

                        Languages.Language ln = (Languages.Language) System.Enum.Parse( typeof( Languages.Language ), key );
                        if (System.Enum.IsDefined( typeof( Languages.Language ), ln ))
                        {
                            if (localised_data.ContainsKey( ln ))
                            {
                                LocalisedData d = localised_data[ln];
                                d.txt = val;
                                localised_data[ln] = d;
                            }
                            else
                            {
                                localised_data.Add( ln, new LocalisedData( null, val ) );
                            }
                        }
                    }
                }
            }
        }

        static void LoadPrefab()
        {
            if (rate_me_panel == null & !scene_mode)
            {
                GameObject go = (GameObject) Resources.Load( "RateMeModuleRes/RateMeCanvas", typeof( GameObject ) );
                if (go == null)
                {
                    Debug.LogError( "RateMeModuleRes contains no RateMeCanvas prefab" );
                    return;
                }
                go.SetActive( false );
                rate_me_panel = GameObject.Instantiate( go );
                GameObject.DontDestroyOnLoad( rate_me_panel );
            }
        }

        private static bool IsLangAvailable( Languages.Language lang )
        {
            return localised_data != null && localised_data.ContainsKey( lang );
        }

        static Languages.Language GetLang()
        {
            Languages.Language lang = Languages.GetLanguage();
            //set overrides
            if (!IsLangAvailable( lang ))
            {
                switch (lang)
                {
                    case Languages.Language.Ukrainian:
                    case Languages.Language.Belarusian:
                        lang = Languages.Language.Russian;
                        break;
                }
            }
            if (!IsLangAvailable( lang ))
            {
                lang = Languages.Language.English;
            }
            return lang;
        }
        
        public static string GetLocalisedText()
        {
            return localised_data[GetLang()].txt;
        }

        public static AudioClip GetLocalisedAudio()
        {
            var audio = localised_data[GetLang()].audio;
            if(!audio)
                audio = localised_data[Languages.Language.English].audio;
            return audio;
        }


        static void GetVariables()
        {
            closed_val = GetVal( closed_var );
            negative_val = GetVal( negative_var );
            positive_val = GetVal( positive_var );
        }

        static int GetVal( string var )
        {
            return PlayerPrefs.GetInt( var, 0 );
        }

        static void SetVal( string var, int val )
        {
            PlayerPrefs.SetInt( var, val );
            NextFrameCall.SavePrefs();
        }


        static void Unsubscribe()
        {
            RateButtonScript.OnRateClicked -= OnRateClicked;
            CloseButton.OnClose -= OnClosed;
            StarScript.OnStarClicked -= OnStarClicked;
        }

        static void Subscribe()
        {
            RateButtonScript.OnRateClicked += OnRateClicked;
            CloseButton.OnClose += OnClosed;
            StarScript.OnStarClicked += OnStarClicked;
        }

        private static IEnumerator DelayedRateClicked()
        {
            yield return new WaitForSeconds( 1 );
            OnRateClicked();
        }

        private static Coroutine d_rate = null;

        static void OnStarClicked( bool _positive, float x )
        {
            Debug.Log( "OnStarClicked " + _positive );
            positive = _positive;
            if (positive)
            {
                CoroutineHandler.Stop( d_rate );
                d_rate = CoroutineHandler.Start( DelayedRateClicked() );
            }
        }

        static void OnClosed()
        {
            if (positive)
            {
                OnRateClicked();
            }
            else
            {
                closed_val++;
                SetVal( closed_var, closed_val );
                ShowDialogue( false );
            }
        }


        static void OnRateClicked()
        {
            if (positive)
            {
                positive_val++;
                SetVal( positive_var, positive_val );
                RateApplication();
            }
            else
            {
                negative_val++;
                SetVal( negative_var, negative_val );
            }
            ShowDialogue( false );
        }

#if BRAND_NEW_PROTOTYPE
        private static void SaveTargetScene( Scenes scene )
        {
            target_scene = scene;
        }
#endif

        [ContextMenu( "ForseShowDialogue" )]
        static void ForseShow()
        {
            ShowDialogue( true, true );
        }


        public static bool ShowDialogue( bool show = true, bool forse = false )
        {
            if(!Application.isEditor && Application.platform != RuntimePlatform.Android)
			    return false;

            bool res = false;

            if (forse || !show || CanShow( Time.time ))
            {
#if Prototype_4_2_OR_NEWER
                Scenes rate_scene = RateMeOffer.rateme_scene;
#endif
                if (show)
                {
                    positive = false;
                }

                if (IsVisible())
                {
                    last_time = Time.time;
                }

                is_shown = show;
#if BRAND_NEW_PROTOTYPE
                if (scene_mode)
                {
                    if (show)
                    {
                        if (rate_scene == Scenes.None)
                        {
                            Debug.LogError( "RateMeModule: no scene defined." );
                        }
                        else
                        {
                            Scenes _target_scene = SceneLoader.GetTargetScene();
                            if (_target_scene != rate_scene)
                            {
                                //if rate_me can be shown we tell scene loader to change destination and return false
                                SaveTargetScene( _target_scene );
                                SceneLoader.SetTargetScene( rate_scene );
                                res = true;
                            }
                            else
                            {
                                Debug.LogError( "RateMeModule: Do not call transition to RateMeScene directly! Use RateMeModule.ShowDialogue() before normal transition to the scene you need. RateMeModule will override transition order itself" );
                            }
                        }
                    }
                    else
                    {
                        if (SceneLoader.GetCurrentScene() == rate_scene)
                        {
                            SceneLoader.SwitchToScene( target_scene );
                            res = true;
                        }
                    }
                }
#endif
                if (!scene_mode)
                {
                    ActivatePanel( show );
                    res = true;
                }

                if (!show)
                {
                    PlaySound( false );
                }
            }
            return res;
        }



        public static void PlaySound( bool play )
        {
            if (play)
            {
                bool mute =
#if BRAND_NEW_PROTOTYPE
#if Prototype_4_2_OR_NEWER
                    AudioController.GetGroup( StreamGroup.VOICE ).IsMuted();
#else
                    !GameSettings.IsSoundsEnabled();
#endif
#else
					!AudioController.IsSoundsEnabled ( );
#endif
                src.clip = GetLocalisedAudio();
                src.mute = mute;
                src.Play();
            }
            else
            {
                src.Stop();
            }
        }


        private static void ActivatePanel( bool show )
        {
            if (rate_me_panel != null)
            {
                //PlaySound ( show );
                rate_me_panel.SetActive( show );
            }
            else
            {
                Debug.LogError( "RateMeModule: rate_me_panel not set" );
            }
        }

        static bool CanShow( float t )
        {
            return IsOnline() && t - last_time >= time_interval && positive_val < 1 && negative_val < negative_limit && closed_val < closed_limit;
        }

        static bool IsOnline()
        {
            return Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork;
        }

        public static bool IsVisible()
        {
            return is_shown;
        }


#if Prototype_4_2_OR_NEWER
        public static void RateApplication()
        {
            string url = ConstSettings.GetApplicationMarketURL();
            if (url.Length > 1)
            {
                Debug.Log( "RateMeDialogue: open URL=" + url );
                Application.OpenURL( url );
            }
            else
            {
                Debug.LogError( "RateMeDialogue: package_name is empty" );
            }
        }
#else

        public const string
            ios_bundle_id = "",
            wp_bundle_id = "";


        private const string
            android_makret_app_url = "market://details?id=",
            ios_market_app_url = "itms-apps://itunes.apple.com/app/",
            wp_market_app_url = "ms-windows-store:navigate?appid=";


        public static void RateApplication()
        {
            string app_alias = GetAppAlias();
            if (!string.IsNullOrEmpty( app_alias ))
            {
                string url = GetAppMarketURL( app_alias );
                Debug.Log( "RateMeDialogue: open URL=" + url );
                Application.OpenURL( url );
            }
            else
            {
                Debug.LogError( "RateMeDialogue: package_name is empty" );
            }
        }



        static string GetAppAlias()
        {
            string res = "";
#if UNITY_ANDROID
            res = GetBundleID();
#elif UNITY_IPHONE
            res = ios_bundle_id;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
            res = wp_bundle_id;
#endif
            return res;
        }

        static string GetAppMarketURL( string app_alias )
        {
            string res = "";
#if UNITY_ANDROID
            res = android_makret_app_url + app_alias;
#elif UNITY_IPHONE
            res = ios_market_app_url + app_alias;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
            res = wp_market_app_url + app_alias;
#endif
            return res;
        }

        private static string GetBundleID()
        {
#if UNITY_2017_1_OR_NEWER
			return Application.identifier;
#else
            return Application.bundleIdentifier;
#endif
        }

#endif
    }
}