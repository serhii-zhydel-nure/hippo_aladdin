﻿using UnityEngine;
using UnityEngine.UI;


namespace RateMePlugin
{
    [RequireComponent ( typeof ( Text ) )]
    public class TextScript :MonoBehaviour
    {
        Text txt;

        public void Awake ()
        {
            txt = GetComponent<Text> ( );
        }

        public void OnEnable ()
        {
            txt.text = RateMeModule.GetLocalisedText ( );
        }
    }
}
