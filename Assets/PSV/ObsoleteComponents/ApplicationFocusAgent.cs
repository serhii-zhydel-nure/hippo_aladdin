﻿#if false // Deleted. Logic moved to ApplicationEventAgent
using UnityEngine;
using System.Collections;
using System;

namespace PSV
{
	public class ApplicationFocusAgent : AgentObject<ApplicationFocusAgent> 
	{
        private static Action<bool> applicationFocus;

        public static event Action<bool> Event
        {
            add
            {
                CheckAgent();
                applicationFocus += value;
            }
            remove
            {
                applicationFocus -= value;
            }
        }
        
        private void OnApplicationFocus( bool focus )
        {
            if (applicationFocus != null)
                applicationFocus( focus );
        }
    }
}
#endif