﻿#if false // Deleted. Logic moved to ApplicationEventAgent
using UnityEngine;
using System.Collections;
using System;

namespace PSV
{
	public class ApplicationPauseAgent : AgentObject<ApplicationPauseAgent> 
	{
        private static Action<bool> applicationPause;

        public static event Action<bool> Event
        {
            add
            {
                CheckAgent();
                applicationPause += value;
            }
            remove
            {
                applicationPause -= value;
            }
        }
        
        private void OnApplicationPause( bool pause )
        {
            if (applicationPause != null)
                applicationPause( pause );
        }
        
    }
}
#endif