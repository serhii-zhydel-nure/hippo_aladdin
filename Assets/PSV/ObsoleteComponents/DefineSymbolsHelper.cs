﻿#if false //DELETED Logic moved to PSVSettings
using UnityEngine;
using System.Collections;
using UnityEditor;

public class DefineSymbolsHelper :MonoBehaviour
{

	private static BuildTargetGroup [] target_groups = new BuildTargetGroup []
		{
		BuildTargetGroup.Android,
		BuildTargetGroup.iOS,
		};

	private const string define_symbols = "HelpTools/Define symbols/";
	private const string FIREBASE_PRESENT = "FIREBASE_PRESENT";
	private const string GOOGLE_ADS_PRESENT = "GOOGLE_ADS_PRESENT";
	private const string USE_UNITY_IAP = "USE_UNITY_IAP";
	private const string GOOGLE_PLAY_GAMES_PRESENT = "GOOGLE_PLAY_GAMES_PRESENT";
	private const char define_separator = ';';


	private static void AddDirectives (string directive)
	{
		for (int i = 0; target_groups != null && i < target_groups.Length; i++)
		{
			string tmp = PlayerSettings.GetScriptingDefineSymbolsForGroup ( target_groups [i] );
			//search for the directive in define symbols
			int dir_ndex = tmp.IndexOf ( directive );
			//if we already have the directive - lets remove it
			if (dir_ndex >= 0)
			{
				int len = directive.Length;
				int last_symbol = dir_ndex + len;
				//if there are some other defines we will include to len separator symbol
				if (tmp.Length > last_symbol && tmp [last_symbol] == define_separator)
				{
					len++;
				}
				tmp = tmp.Remove ( dir_ndex, len );
			}
			//if there is no such directive - lets add it
			else
			{
				if (tmp.Length > 0)
				{
					tmp += define_separator;
				}
				tmp += directive;
			}
			PlayerSettings.SetScriptingDefineSymbolsForGroup ( target_groups [i], tmp );
		}
	}


	[UnityEditor.MenuItem ( define_symbols + FIREBASE_PRESENT )]
	public static void AddFirebasePresentDirective ()
	{
		AddDirectives ( FIREBASE_PRESENT );
	}

	[UnityEditor.MenuItem ( define_symbols + GOOGLE_ADS_PRESENT )]
	public static void AddGoogleAdsPresentDirective ()
	{
		AddDirectives ( GOOGLE_ADS_PRESENT );
	}

	[UnityEditor.MenuItem ( define_symbols + USE_UNITY_IAP )]
	public static void AddUnityIapDirective ()
	{
		AddDirectives ( USE_UNITY_IAP );
	}

	[UnityEditor.MenuItem ( define_symbols + GOOGLE_PLAY_GAMES_PRESENT )]
	public static void AddGPGSDirective ()
	{
		AddDirectives ( GOOGLE_PLAY_GAMES_PRESENT );
	}

}
#endif