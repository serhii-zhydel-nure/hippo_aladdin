﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace PSV
{
    [Obsolete( "Logic moved to LoadableObject! Will be removed." )]
    public class BackgroundProcess : IEnumerable
    {
        public readonly LoadableObject loadable;

        /// <summary>
        /// Create background instruction. For init process need call <see cref="Start(bool)"/>
        /// </summary>
        /// <param name="loadable">Any loadable object</param>
        public BackgroundProcess( LoadableObject loadable )
        {
            this.loadable = loadable;
        }

        /// <summary>
        /// Create background instruction. For init process need call <see cref="Start(bool)"/>
        /// </summary>
        /// <param name="instruction">Any IEnumerator instructions.</param>
        /// <param name="identifier">Unique identifier process</param>
        public BackgroundProcess( IEnumerator instruction, string identifier )
        {
            this.loadable = new LoadableObject.LoadableEnumerator( instruction, identifier );
        }

        /// <summary>
        /// Start process work.
        /// </summary>
        /// <returns></returns>
        public BackgroundProcess Start()
        {
            loadable.LoadAsync();
            return this;
        }

        #region Params

        /// <summary>
        /// Warranty that the process will be completed before loading any next scene.
        /// </summary>
        public BackgroundProcess SetNecessaryAll()
        {
            loadable.SetNecessaryAll();
            return this;
        }

        /// <summary>
        /// Warranty that the process will be completed before loading the specified scenes.
        /// </summary>
        public BackgroundProcess SetNecessaryFor( Scenes[] necessary_for_scenes )
        {
            loadable.SetNecessaryFor( necessary_for_scenes );
            return this;
        }

        /// <summary>
        /// Remove warranty that the process will be completed before loading the specified scenes.
        /// </summary>
        public BackgroundProcess ClearNecessaryAll()
        {
            loadable.ClearNecessaryAll();
            return this;
        }
        #endregion


        /// <summary>
        /// Abort process instruction.
        /// </summary>
        /// <param name="key">Key for defining a specific process</param>
        public static void Abort( string identifier )
        {
            LoadableObject.AbortLoad( identifier );
        }

        /// <summary>
        /// Abort this process operation.
        /// </summary>
        public BackgroundProcess Abort()
        {
            loadable.Dispose();
            return this;
        }

        public IEnumerator GetEnumerator()
        {
            return loadable.GetEnumerator();
        }
    }
}