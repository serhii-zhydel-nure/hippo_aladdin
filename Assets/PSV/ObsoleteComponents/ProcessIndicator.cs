﻿#if false // Deleted!

using UnityEngine;
using System.Collections;
using System;

namespace PSV
{
    public class ProcessIndicator : AgentObject<ProcessIndicator>
    {
        private const string prefab_path = "ProcessIndicator";
        
        / <summary>
        / Show background process indicator.
        / </summary>
        public static void Show()
        {
            if (CheckUIAgent( prefab_path ))
                agent.gameObject.SetActive( true );
        }

        / <summary>
        / Hide background process indicator.
        / </summary>
        public static void Hide()
        {
            if (CheckUIAgent( prefab_path ))
                agent.gameObject.SetActive( false );
        }
    }
}
#endif