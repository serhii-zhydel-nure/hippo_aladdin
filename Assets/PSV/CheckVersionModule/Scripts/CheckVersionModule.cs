﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV.CheckVersion
{
    public static class CheckVersionModule
    {
        private const string dataLastCheckVersionPref = "dateVersLastCheck";
        private const string firstRequereUpdatePref = "dateFirstRequereUpdate";
        private const string requereUpdateVersionPref = "requereVersionUpdate";

        /// <summary>
        /// String param is loaded version.
        /// </summary>
        public static event Action<uint> OnVersionChecked;

        [AwakeStatic]
        private static void StaticAwake()
        {
            AwakeStaticAttribute.Done( typeof( CheckVersionModule ) );
            NextFrameCall.Add( CheckVersion );
        }

        public static void CheckVersion()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable && IsTimeHasPassed( dataLastCheckVersionPref, 1, true ))
            {
                if (Application.platform == RuntimePlatform.Android)
                    CoroutineHandler.Start( DownloadVersionGooglePlay() );
            }
            else
            {
                ShowCheckResult();
            }
        }

        private static void ShowCheckResult()
        {
            if (IsAvailableUpdateApp()) // Is need update application
            {
                if (IsTimeHasPassed( firstRequereUpdatePref, 14, false )) // Must be update version
                {
                    WarningPanel.ShowUpdateApplication()
                        .OnAccept( OnUpdateAppAccept, false )
                        .OnCancel( OnUpdateAppCancel )
                        .OnCancel( WarningPanel.ApplicationQuit );
                }
                else // Still can play on this versions
                {
                    WarningPanel.ShowUpdateApplication()
                       .OnAccept( OnUpdateAppAccept, true )
                       .OnCancel( OnUpdateAppCancel )
                       .OnCancel( WarningPanel.Hide );
                }
            }
        }

        private static void OnUpdateAppAccept()
        {
            AnalyticsManager.LogEvent( AnalyticsEvents.VersionApplication, "Choice update" );
        }

        private static void OnUpdateAppCancel()
        {
            AnalyticsManager.LogEvent( AnalyticsEvents.VersionApplication, "Cancel update" );
        }

        public static bool IsAvailableUpdateApp()
        {
            return ConstSettings.GetVersionNumber() < PlayerPrefs.GetInt( requereUpdateVersionPref, 0 );
        }

        public static void SetCurrentTimeToPref( string prefKey )
        {
            PlayerPrefs.SetString( prefKey, DateTime.Now.ToBinary().ToString() );
            NextFrameCall.SavePrefs();
        }

        private static bool IsTimeHasPassed( string prefKey, int days, bool emptyRes )
        {
            string pref = PlayerPrefs.GetString( prefKey, string.Empty );
            if (string.IsNullOrEmpty( pref ))
            {
                return emptyRes;
            }
            else
            {
                DateTime checkTime;
                try
                {
                    long binartDate = long.Parse( pref );
                    checkTime = DateTime.FromBinary( binartDate );
                }
                catch
                {
                    return emptyRes;
                }
                checkTime = checkTime.AddDays( days );
                return DateTime.Compare( DateTime.Now, checkTime ) > 0; // Now time is later than checkTime
            }
        }

        private static void CompareVersion( string loadedVersion )
        {
            uint currVer = ConstSettings.GetVersionNumber();
            uint marketVer = ConstSettings.GetVersionNumber( loadedVersion );
            if (marketVer != 0 && currVer < marketVer)
            {
                PlayerPrefs.SetInt( requereUpdateVersionPref, ( int )marketVer );
                if (!PlayerPrefs.HasKey( firstRequereUpdatePref ))
                    SetCurrentTimeToPref( firstRequereUpdatePref );
            }
            else
            {
                PlayerPrefs.DeleteKey( firstRequereUpdatePref );
                PlayerPrefs.DeleteKey( requereUpdateVersionPref );
            }
            if (OnVersionChecked != null)
                OnVersionChecked( marketVer );
        }

        private static IEnumerator DownloadVersionGooglePlay()
        {
            string bundle = ConstSettings.GetApplicationAlias();
            using (WWW loader = new WWW( "https://play.google.com/store/apps/details?id=" + bundle + "&hl=en" ))
            {
                while (!loader.isDone)
                    yield return null;

                if (string.IsNullOrEmpty( loader.error ))
                {
                    SetCurrentTimeToPref( dataLastCheckVersionPref );
                    string page = loader.text;
                    string versionHeader = ">Current Version<";
                    int indexVersionLine = page.IndexOf( versionHeader );
                    if (indexVersionLine > 0)
                    {
                        string versuinLine = page.Substring( indexVersionLine + versionHeader.Length, 100 );
                        int divIndex = versuinLine.IndexOf( '>' );
                        while (divIndex >= 0)
                        {
                            yield return null;
                            if (versuinLine[divIndex + 1] == '<' || !Char.IsDigit( versuinLine[divIndex + 1] ))
                            {
                                divIndex = versuinLine.IndexOf( '>', divIndex + 1 );
                            }
                            else
                            {
                                int endVersion = versuinLine.IndexOf( '<', divIndex + 1 );
                                string version = versuinLine.Substring( divIndex + 1, endVersion - divIndex - 1 );
                                CompareVersion( version );
                                break;
                            }
                        }
                    }
                }
                else
                {
                    Debug.LogError( "Check version on market fail! Error:" + loader.error );
                }
            }
            ShowCheckResult();
        }
    }
}