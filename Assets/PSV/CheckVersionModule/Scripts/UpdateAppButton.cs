﻿using UnityEngine;
using System.Collections;

namespace PSV.CheckVersion
{
    public class UpdateAppButton : ButtonClickHandler
    {

        private new void Awake()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                if (CheckVersionModule.IsAvailableUpdateApp())
                {
                    base.Awake();
                    return;
                }
                else
                {
                    CheckVersionModule.OnVersionChecked += OnVersionChecked;
                }
                break;
            }
            gameObject.SetActive( false );
        }

        private void OnDestroy()
        {
            CheckVersionModule.OnVersionChecked -= OnVersionChecked;
        }

        private void OnVersionChecked( uint newVersion )
        {
            if (CheckVersionModule.IsAvailableUpdateApp())
            {
                gameObject.SetActive( true );
                base.Awake();
            }
        }

        protected override void OnButtonClick()
        {
            AnalyticsManager.LogEvent( AnalyticsEvents.VersionApplication, "Click Update button" );
            Application.OpenURL( ConstSettings.GetApplicationMarketURL() );
        }
    }
}