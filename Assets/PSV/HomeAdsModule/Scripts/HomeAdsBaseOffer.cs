﻿/* 
 * Dependence plugins: 
 * com.psvplugins.priorityapp:1.1
 * com.psvplugins.notification:1.1
 * com.android.support:support-v4:26.1.0+
 * 
 * Build SDK Tools Version '27.0.3'
 * 
 * Prototype 4.2 +
 * */

using UnityEngine;
using System.Collections;
using PSV.ADS;

namespace PSV.HomeAds
{
    public abstract partial class HomeAdsBaseOffer : ISceneOffer
    {
        public const string ACTION_OPEN_NOTIFY = "NotificationPushAds"; // Native const notification intent action

        private static float timeNextShow = 0.0f;
        public static bool nextShowByOpenNotify = false;
        
        public bool is_force_show;
        public int priority { get { return -100; } }

        public static bool isSkipInterOnSessin = false;
        
        [AwakeStatic]
        public static void StaticAwake()
        {
            AwakeStaticAttribute.Done( typeof( HomeAdsBaseOffer ) );
            SubscribeToHomeInterEvent();
            PopularNotify.PopularityRating.OnInitialize += HomeChildrenAdsData.InitChildrenAds;
            string intent = NativeNotifications.GetLaunchIntent();
            nextShowByOpenNotify = intent == ACTION_OPEN_NOTIFY;
            OffersManager.AddOffer( new HomeAdsOffer( nextShowByOpenNotify ) );
            if (nextShowByOpenNotify)
                AnalyticsManager.LogEvent( AnalyticsEvents.Notification, "Open game by Push ads" );
        }

        public static void ForceShowNextScene()
        {
            var handler = OffersManager.Find<HomeAdsOffer>();
            if (handler == default( HomeAdsOffer ))
            {
                OffersManager.AddOffer( new HomeAdsOffer( true ) );
            }
            else
            {
                handler.is_force_show = true;
            }
        }

        private static void SubscribeToHomeInterEvent()
        {
            var providerSettings = PSVSettings.settings.ads_provider_settings;
            for (int i = 0; i < providerSettings.Length; i++)
            {
                if (providerSettings[i].param == ProviderParams.HOMEADS_INTERSTITIAL_ID)
                    return; // Then wait ready home interstitial
            }
            isSkipInterOnSessin = true;
        }

        public static bool isHomeInterstitialReady
        {
            get
            {
                if (HomeChildrenAdsData.isChildrenGame || isSkipInterOnSessin || Application.isEditor)
                    return true;
                var allInterstitial = AdEventsListener.GetAdReady( AdEventsListener.EventSource.Interstitial );
                bool isReady = false;
                allInterstitial.TryGetValue( AdNetwork.HomeAds.ToString(), out isReady );
                return isReady;
            }
        }

        public HomeAdsBaseOffer( bool is_force_show = false )
        {
            this.is_force_show = is_force_show;
        }

        public virtual bool IsEmpty()
        {
            return Application.platform == RuntimePlatform.IPhonePlayer || ( !is_force_show && ScenesList().Length == 0 );
        }

        public virtual OfferReply TryShow( Scenes current_scene, Scenes target_scene )
        {
            if (current_scene == OfferScene()) // After HomeAdsBox scene.
            {
                is_force_show = false;
                if (!HomeChildrenAdsData.isChildrenGame) // Children Ads already shown on HomeAdsBox scene then skip home interstitial.
                {
                    if (ManagerGoogle.ShowInterstitial( true, true ))
                        return OfferReply.ShowWithAds;
                }
            }
            else if (ExtraConditions( current_scene, target_scene ))
            {
                timeNextShow = Time.time + HomeAdsOffer.time_interval;
                SceneLoader.SetTargetScene( OfferScene() );
                return OfferReply.ShownNoAds;
            }
            return OfferReply.Skipped;
        }

        protected virtual bool ExtraConditions( Scenes current_scene, Scenes target_scene )
        {
            return Application.internetReachability != NetworkReachability.NotReachable
                    && ManagerGoogle.IsAdmobEnabled()
                    && ( is_force_show
                        || ( timeNextShow < Time.time && IsNeedShowAfter( current_scene ) ) )
                    && !( isSkipInterOnSessin && !HomeChildrenAdsData.isChildrenGame );
        }

        public abstract Scenes OfferScene();
        public abstract Scenes[] ScenesList();


        private bool IsNeedShowAfter( Scenes current_Scene )
        {
            var sceneList = ScenesList();
            for (int i = 0; i < sceneList.Length; i++)
            {
                if (sceneList[i] == current_Scene)
                    return true;
            }
            return false;
        }
    }
}