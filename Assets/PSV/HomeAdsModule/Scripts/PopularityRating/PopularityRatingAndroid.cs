﻿using UnityEngine;
using System.Collections;

namespace PSV.PopularNotify
{
    public class PopularityRatingAndroid : PopularityRating
    {
        private const string native_class_name = "com.psvplugins.priorityapp.Priority";
        private AndroidJavaObject native_class;

        public PopularityRatingAndroid()
        {
            positionLocal = NOT_POPULAR;
            if (Application.isEditor)
                return;
            try
            {
                native_class = new AndroidJavaObject( native_class_name );
                if (native_class != null && native_class.GetRawClass() != System.IntPtr.Zero)
                    return;
            }
            catch { }
            native_class = null;
            Debug.LogError( "PopularityData: " + native_class_name + " class not found!" );
        }

        public override int GetLastRatingPosition()
        {
            if (native_class == null)
                return base.GetLastRatingPosition();
            return native_class.Call<int>( "getLastPriorityPosition" );
        }

        public override PopularityRating FindAllInstalledApps()
        {
            if (native_class != null)
                native_class.Call<int>( "setInstalledApplication" );
            return this;
        }

        public override PopularityRating FindSelfInRatingList()
        {
            if (native_class != null)
            {
                positionInRatingList = native_class.Call<int>( "setPriorityList" );
                if (positionInRatingList == NOT_FOUND_LIST)
                {
                    Debug.LogError( "PopularityRating: Rating list not found. Try call InitPopularityData() before InitRatingList()." );
                    positionInRatingList = NOT_EXIST_IN_LIST;
                }
            }
            return this;
        }

        public override PopularityRating InitLocalRatingPosition()
        {
            if (native_class != null)
                positionLocal = native_class.Call<int>( "setPriorityPosition" );
            return this;
        }


        public override PopularityRating SetModeScheduleNotify( byte mode = 0 )
        {
            if (native_class != null)
                native_class.Call( "setModeScheduleNotify", mode );
            return this;
        }

        public override PopularityRating BuildNotifications()
        {
            if (native_class != null)
                native_class.Call( "buildNotification" );
            return this;
        }

        public override PopularityRating CancelSelfNotifications()
        {
            if (native_class != null)
                native_class.Call( "cancelNotify" );
            return this;
        }

        public override PopularityRating СheckOtherLocalRatingModules()
        {
            if (native_class != null)
                native_class.Call( "callCheckAllApplications" );
            return this;
        }

        protected override PopularityRating InitNativePopularityData( string jsonData )
        {
            if (native_class != null)
                native_class.Call( "loadPopularityData", jsonData );
            return this;
        }
        
        public override int GetStoredDataVersion()
        {
            if (native_class != null)
                return native_class.Call<int>( "getStoredDataVersion" );
            return base.GetStoredDataVersion();
        }

        public override PopularityRating SetStoredDataVersion( int version )
        {
            if (native_class != null)
                native_class.Call( "setStoredDataVersion", version );
            return this;
        }

        public override PopularityRating SetNotificationStyle( int style, string background )
        {
            if (native_class != null)
                native_class.Call( "setNotificationStyle", style, background );
            return this;
        }

        public override string[] GetInstalledAlias()
        {
            if (native_class != null)
            {
                string parse = native_class.Call<string>( "getInstalledList" );
                return parse.Split( '\n' );
            }
            return base.GetInstalledAlias();
        }

        public override string GetPopularityDataPath()
        {
            if (native_class != null)
                return native_class.Call<string>( "getPopularityDataPath" );
            return base.GetPopularityDataPath();
        }

        public override PopularityRating SetNotificationText( string title, string message )
        {
            if (native_class != null)
                native_class.Call( "setNotificationText", title, message );
            return this;
        }

        public override PopularityRating SetNotificationTextFromServer()
        {
            if (native_class != null)
                native_class.Call( "clearNotificationText" );
            return this;
        }

        public override void Dispose()
        {
            if (native_class != null)
            {
                native_class.Dispose();
                native_class = null;
            }
        }


        public static void SetLanguage( Languages.Language lang )
        {
            try
            {
                using (AndroidJavaClass native_static = new AndroidJavaClass( native_class_name ))
                    native_static.CallStatic( "setSelectedLanguage", lang.ToString() );
            }
            catch
            {
                Debug.LogError( "PopularityData: " + native_class_name + " class not found!" );
            }
        }
    }
}