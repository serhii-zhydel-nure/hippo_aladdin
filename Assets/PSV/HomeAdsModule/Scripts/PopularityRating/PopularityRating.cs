﻿/* 
 * Dependence plugins: 
 * com.psvplugins.priorityapp:1.3
 * com.psvplugins.notification:1.2
 * com.android.support:support-v4:26.1.0+
 * 
 * Build SDK Tools Version '27.0.3 +'
 * 
 * Prototype 4.2 +
 * Loalization Text module.
 * HomeChildrenAds.
 * 
 * Default Popularity Rating data in Resource folder check path in const: dataFileResourcePath
 * Download Popularity Rating data from URL in const: dataFileServerURL
 * 
 * */

using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace PSV.PopularNotify
{
    public partial class PopularityRating : IDisposable
    {
        /// <summary>
        /// Can be called on next frame StaticAwake
        /// </summary>
        public static event Action<PopularityData, PopularityRating> OnInitialize;

        public const string serverHost = "http://psdata.psvgamestudio.com/PSVData/";
        public const string fileOnServer = "PriorityGames.json";

        //* Native Contants
        public const int NOT_EXIST_IN_LIST = -5;
        public const int NOT_FOUND_LIST = -10;
        public const int NOT_POPULAR = -1;
        //* ^

        /// <summary>
        /// Index 1 - 7, other = NOT_POPULAR.
        /// </summary>
        public int positionLocal = NOT_POPULAR;
        /// <summary>
        /// Start index = 1
        /// </summary>
        public int positionInRatingList = NOT_EXIST_IN_LIST;

        public PopularityData popularityData;

        protected PopularityRating() { }

        public static PopularityRating Builder()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                return new PopularityRatingAndroid();
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer:
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                return new PopularityRating();
                default:
                return null;
            }
        }

        [AwakeStatic]
        public static void InitDefault()
        {
            AwakeStaticAttribute.Done( typeof( PopularityRating ) );
            PopularityRating handler = Builder();
            if (handler == null)
                return;
            Subscribe();

            if (!Application.isEditor && IsShouldDownloadData())
            {
                CoroutineHandler.Start( PopularityData.DownloadData( dataFileServerURL, handler.InitDefault ) );
            }
            else
            {
                handler.InitDefault( string.Empty );
            }
        }

        public void InitDefault( string dataJson )
        {
            if (string.IsNullOrEmpty( dataJson ))
            {
                if (!LoadLocalDataJson( ref dataJson ))
                    return;
            }
            else
            {
                SetTimeCheckVersionData();
            }

            popularityData = JsonUtility.FromJson<PopularityData>( dataJson );
            SetDelayCheckVersionDays( popularityData.delayUpdateDays );
            int storedVersion = GetStoredDataVersion();
            if (storedVersion < popularityData.version)
                UpdateLocalData( popularityData, dataJson );
            else
                positionLocal = GetLastRatingPosition();

            NextFrameCall.Add( InitializeDoneCall );
        }

        public void InitializeDoneCall()
        {
            if (OnInitialize != null)
            {
                OnInitialize( popularityData, this );
                OnInitialize = null;
            }
            Dispose();
        }

        public void UpdateLocalData( PopularityData popularityData, string dataJson )
        {
            string folderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Priority";
            if (!Directory.Exists( folderPath ))
                Directory.CreateDirectory( folderPath );
            string dataPath = GetPopularityDataPath();
            if (!string.IsNullOrEmpty( dataPath ))
            {
                File.WriteAllText( dataPath, dataJson );
                SetStoredDataVersion( popularityData.version );
            }
            else
            {
                Debug.LogError( "PopularityRating: Can not be saved because native GetPopularityDataPath() is empty." );
            }
            InitNativePopularityData( dataJson );
            FindSelfInRatingList();

            if (positionInRatingList == NOT_EXIST_IN_LIST) // Skip. not exist self in list
            {
                positionInRatingList = NOT_POPULAR;
            }
            else
            {
                FindAllInstalledApps();
                InitLocalRatingPosition();
                CancelSelfNotifications();
                СheckOtherLocalRatingModules();
                SetNotificationStyle( default_notify_style, default_notify_background_res );
                BuildNotifications();

                string analyticsMessage = "Rating:";
                if (positionLocal < 1 || positionLocal > 7)
                    analyticsMessage += "Not popular";
                else
                    analyticsMessage += positionLocal;
                AnalyticsManager.LogEvent( AnalyticsEvents.Notification, analyticsMessage );
            }
        }

        public bool LoadLocalDataJson( ref string dataJson )
        {
            string localFile = GetPopularityDataPath();
            if (!string.IsNullOrEmpty( localFile ) && File.Exists( localFile ))
            {
                dataJson = File.ReadAllText( localFile );
            }
            if (string.IsNullOrEmpty( dataJson ))
            {
                TextAsset localData = Resources.Load<TextAsset>( dataFileResourcePath );
                if (localData)
                {
                    dataJson = localData.text;
                }
                else
                {
                    Debug.LogError( "PopularityRating: PopularityData not found. Initialization Popularity modules are canceled." );
                    return false;
                }
            }
            return true;
        }

        public static string dataFileServerURL
        {
            get
            {
                string platformPrefix = string.Empty;
                switch (Application.platform)
                {
                    case RuntimePlatform.Android:
                    case RuntimePlatform.WindowsEditor:
                    platformPrefix = "Android";
                    break;
                    case RuntimePlatform.IPhonePlayer:
                    case RuntimePlatform.OSXEditor:
                    platformPrefix = "IOS";
                    break;
                }
                return serverHost + platformPrefix + fileOnServer;
            }
        }

        public static void Subscribe()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                Languages.OnLangChange += PopularityRatingAndroid.SetLanguage;
                PopularityRatingAndroid.SetLanguage( Languages.GetLanguage() );
                break;
            }
        }

        public static void Unsubscribe()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                Languages.OnLangChange -= PopularityRatingAndroid.SetLanguage;
                break;
            }
        }

        public static bool IsShouldDownloadData()
        {
            string pref = PlayerPrefs.GetString( dataNextCheckDatePref, string.Empty );
            if (string.IsNullOrEmpty( pref ))
            {
                return true;
            }
            else
            {
                int delaydays = PlayerPrefs.GetInt( delayDaysCheckDataPref, 1 );
                long binartDate = long.Parse( pref );
                DateTime checkTime = DateTime.FromBinary( binartDate );
                checkTime = checkTime.AddDays( delaydays );
                return DateTime.Compare( DateTime.Now, checkTime ) > 0; // Now time is later than checkTime
            }
        }

        public static void SetTimeCheckVersionData()
        {
            PlayerPrefs.SetString( dataNextCheckDatePref, DateTime.Now.ToBinary().ToString() );
        }

        public static void SetDelayCheckVersionDays( int delayDays )
        {
            PlayerPrefs.SetInt( delayDaysCheckDataPref, delayDays );
            NextFrameCall.SavePrefs();
        }

        public static int FindAliasInList( string alias, string[] list )
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (alias == list[i])
                {
                    return i;
                }
            }
            return NOT_EXIST_IN_LIST;
        }

        #region Native methods

        /// <summary>
        /// Init popularity data from json data and save local.
        /// </summary>
        protected virtual PopularityRating InitNativePopularityData( string jsonData ) { return this; }

        /// <summary>
        /// Dependence: <see cref="InitNativePopularityData"/>
        /// </summary>
        public virtual PopularityRating FindSelfInRatingList() { return this; }

        /// <summary>
        /// Init all installed application bundles.
        /// </summary>
        public virtual PopularityRating FindAllInstalledApps() { return this; }

        /// <summary>
        /// Get list installed alias. Auto-call <see cref="FindAllInstalledApps"/> if list is empty.
        /// </summary>
        /// <returns></returns>
        public virtual string[] GetInstalledAlias() { return new string[0]; }

        /// <summary>
        /// Dependence: <see cref="FindSelfInRatingList"/> && <see cref="FindAllInstalledApps"/>.
        /// </summary>
        public virtual PopularityRating InitLocalRatingPosition() { return this; }

        /// <summary>
        /// Get last self rating position saved in local native storage.
        /// </summary>
        public virtual int GetLastRatingPosition() { return NOT_POPULAR; }

        /// <summary>
        /// Canceled notifications PopularityRating module.
        /// </summary>
        /// <returns></returns>
        public virtual PopularityRating CancelSelfNotifications() { return this; }

        /// <summary>
        /// Call event for all installed games with PopularityRating module
        /// to check self rating position by local data and refresh Popularity notifications.
        /// Dependence: <see cref="FindAllInstalledApps"/>
        /// </summary>
        public virtual PopularityRating СheckOtherLocalRatingModules() { return this; }

        /// <summary>
        /// Create Notifications for this PopularityRating module. Skip build on application are NOT_POPULAR
        /// Dependence: <see cref="InitLocalRatingPosition"/>
        /// </summary>
        public virtual PopularityRating BuildNotifications() { return this; }

        /// <summary>
        /// Toggle Notification mode. Default mode is id 2.
        /// 0 = "Each application has its own day of the week"
        /// 1 = "All applications use current day" (Mode used for quick testing)
        /// 2 = "Calculate day by table and count installed applications"
        /// Warning! Mode will be not save state native, so for native call CheckAllInsalledAppRating() restore logic mode still default!
        /// </summary>
        public virtual PopularityRating SetModeScheduleNotify( byte mode = 2 ) { return this; }

        /// <summary>
        /// Style popularity notification. 
        /// Background not supported for DEFAULT_ANDROID_STYLE!
        /// </summary>
        /// <param name="style">
        /// 0 - DEFAULT_ANDROID_STYLE - default android notification style.
        /// 1 - WITH_DEFAULT_BG_STYLE - default PSV background style.
        /// 2 - WITH_HIPPO_BG_STYLE - hippo style with background.</param>
        /// <param name="background">Empty - set native image by selected style. 
        /// Resource name, from Android/res/drawable folder, like "psv_background_1" in Android resource name format.</param>
        public virtual PopularityRating SetNotificationStyle( int style, string background ) { return this; }

        public virtual PopularityRating SetNotificationText( string title, string message ) { return this; }

        public virtual PopularityRating SetNotificationTextFromServer() { return this; }

        public virtual int GetStoredDataVersion() { return int.MaxValue; }

        /// <summary>
        /// Use native storage data version for 
        /// </summary>
        public virtual PopularityRating SetStoredDataVersion( int version ) { return this; }

        public virtual string GetPopularityDataPath()
        {
            return Application.persistentDataPath + "/LocalPriorityGames.json";
        }

        public virtual void Dispose() { }
        #endregion

    }
}