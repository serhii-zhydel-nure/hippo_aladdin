﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV.PopularNotify
{
    [Serializable]
    public class PopularityData
    {
        public int version;
        public int delayUpdateDays;
        public WeekByCount[] scheduleOfWeek;
        public int[] triggerTime;
        public string[] rating;
        public byte[] isChildrens;

        public int[] adsAlias;
        public string[] adsContent;

        public string[] languages;
        public string[] localTitle;
        public string[] localMessage;

        
        public static IEnumerator DownloadData( string url, Action<string> callback )
        {
            string source = string.Empty;
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                using (WWW downloader = new WWW( url ))
                {
                    yield return downloader;
                    while (!downloader.isDone)
                        yield return null;
                    if (string.IsNullOrEmpty( downloader.error ))
                    {
                        source = downloader.text;
                    }
                    else
                    {
                        Debug.LogError( "PopularityData: data download eror: " + downloader.error + ". URL:" + url );
                    }
                }
            }
            if (callback != null)
            {
                callback( source );
            }
        }
    }

    [Serializable]
    public struct WeekByCount
    {
        public byte[] day;
    }
}