﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace PSV.HomeAds
{
    [RequireComponent( typeof( AudioSource ) )]
    public class HomeChildrenBox : MonoBehaviour
    {
        public HomeAdsBox adsBox;
        public HomeAdsChildrenElem gamePanelElemPrefab;
        public Sprite marketAndroidIcon;
        public Sprite marketIosIcon;
        public GameObject blocker;

        public RectTransform gamesContainer;

        public Sprite hippoLogo;
        public Sprite openSprite;

        public LocalizationAudio[] soundsOnShowIcons;

        private HomeAdsChildrenElem[] elements;
        private HomeChildrenAdsData contentAds;
        private bool isMarketOpen = false;
        private AudioSource audioSource;
        private bool isReadyToOpen;

        public void InitAds()
        {
            audioSource = GetComponent<AudioSource>();
            contentAds = HomeChildrenAdsData.Load();
            if (contentAds == null)
            {
                Debug.LogWarning( "HomeChildrenBox: Not found HomeChildrenAds data." );
                Invoke( "InitAds", 0.5f );
            }
            else
            {
                InitContent();
            }
        }

        public void InitContent()
        {
            if (HomeAdsOffer.nextShowByOpenNotify || PSVSettings.settings.portrait_orientation_only)
            {
                elements = new HomeAdsChildrenElem[1];
                elements[0] = InstantiateElement( Random.Range( 0, contentAds.alias.Length ) );
            }
            else
            {
                int[] randomIndex = new int[contentAds.alias.Length];
                for (int i = 0; i < randomIndex.Length; i++)
                {
                    randomIndex[i] = i;
                }
                randomIndex = randomIndex.Shuffle();

                elements = new HomeAdsChildrenElem[contentAds.countShow];
                for (int i = 0; i < contentAds.countShow; i++)
                {
                    elements[i] = InstantiateElement( randomIndex[i] );
                }
            }
        }

        public void Show()
        {
            bool canBeShow = false;
            for (int i = 0; i < elements.Length; i++)
            {
                if(elements[i].gameObject.activeSelf)
                {
                    canBeShow = true;
                    break;
                }
            }
            if (canBeShow)
            {
                gamesContainer.gameObject.SetActive( true );
                blocker.SetActive( true );
                PlayShowSound( Languages.GetLanguage() );
                gamesContainer.DOScale( 0.0f, 1.5f ).OnComplete( UnlockItems ).From()
                    .SetEase( Ease.OutElastic ).SetUpdate( true );
                HomeAdsOffer.nextShowByOpenNotify = false;
            }
            else
            {
                adsBox.SwitchToNextScene();
            }
        }
        public void UnlockItems()
        {
            isReadyToOpen = true;
            if (elements != null)
                for (int i = 0; i < elements.Length; i++)
                    elements[i].button.interactable = true;
        }

        private void PlayShowSound( Languages.Language lang )
        {
            if (AudioController.GetGroup( StreamGroup.FX ).IsMuted())
                return;

            for (int i = 0; i < soundsOnShowIcons.Length; i++)
            {
                if (soundsOnShowIcons[i].lang == lang)
                {
                    audioSource.volume = AudioController.GetGroup(StreamGroup.FX).Volume;
                    audioSource.clip = soundsOnShowIcons[i].clip;
                    audioSource.loop = false;
                    audioSource.Play();
                    return;
                }
            }
            if (lang != Languages.Language.English)
                PlayShowSound( Languages.Language.English );
        }

        private HomeAdsChildrenElem InstantiateElement( int contentIndex )
        {
#if UNITY_5_4_OR_NEWER
            HomeAdsChildrenElem elem = ( HomeAdsChildrenElem )Object.Instantiate( gamePanelElemPrefab, gamesContainer, false );
#else
            HomeAdsChildrenElem elem = ( HomeAdsChildrenElem )Object.Instantiate( gamePanelElemPrefab );
            RectTransform instance_t = elem.GetComponent<RectTransform>();
            instance_t.SetParent( gamesContainer, false );
#endif
#if UNITY_IOS
            elem.SetMarket( marketIosIcon );
#else
            elem.Init( marketAndroidIcon, hippoLogo, openSprite, isReadyToOpen );
#endif
            elem.OnOpenMarket = OnOpenMarket;
            elem.OnOpenCard = OnOpenCard;
            contentAds.LoadContent( contentIndex, elem.SetContent );
            return elem;
        }

        private void OnApplicationPause( bool pause )
        {
            if (isMarketOpen && !pause)
            {
                gameObject.SetActive( false );
                adsBox.SwitchToNextScene();
            }
        }

        public bool OnOpenMarket( string alias )
        {
            if (isMarketOpen)
                return false;
            isMarketOpen = true;
            return true;
        }

        private void OnOpenCard()
        {
            for (int i = 0; i < elements.Length; i++)
            {
                if (!elements[i].isOpen)
                {
                    return;
                }
            }
            CanvasBlocker.OnClick += adsBox.SwitchToNextScene;
        }

        [System.Serializable]
        public class LocalizationAudio
        {
            public Languages.Language lang;
            public AudioClip clip;
        }
    }
}