﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace PSV.HomeAds
{
    public class HomeAdsChildrenElem : MonoBehaviour
    {
        public System.Func<string, bool> OnOpenMarket;
        public System.Action OnOpenCard;

        public Image gameIcon;
        public Image panelIcon;
        public Image glassIcon;
        public CanvasGroup contentGroup;
        public Image marketIcon;
        public ParticleSystem lightParticle;
        public Button button;

        private Sprite openSprite;

        private string targetAlias = string.Empty;
        public bool isOpen { get; set; }


        public void Init( Sprite marketIcon, Sprite closedSprite, Sprite openSprite, bool isReadyToOpen )
        {
            button.onClick.AddListener( OnPointerClick );
            this.marketIcon.sprite = marketIcon;
            this.marketIcon.color = Color.white;
            panelIcon.sprite = closedSprite;
            this.openSprite = openSprite;
            button.interactable = isReadyToOpen;
        }

        public void SetContent( string alias, Sprite icon )
        {
            if (icon)
            {
                gameIcon.sprite = icon;
                targetAlias = alias;
            }
            else
            {
                gameObject.SetActive( false );
            }
        }

        public void OnPointerClick()
        {
            if (isOpen)
            {
                if (targetAlias.Length > 0 && ( OnOpenMarket == null || OnOpenMarket( targetAlias ) ))
                {
                    button.interactable = false;
                    AnalyticsManager.LogEvent( AnalyticsEvents.OpenPromo, targetAlias + " HomeBox" );
                    Application.OpenURL( ConstSettings.GetApplicationMarketURL( targetAlias ) );
                }
            }
            else
            {
                button.interactable = false;
                StartCoroutine( OpenContentAction() );
            }
        }

        private IEnumerator OpenContentAction()
        {
            yield return new WaitForSeconds( 0.2f );
            RectTransform transform = GetComponent<RectTransform>();
            Vector3 currRotate = transform.localEulerAngles;
            while (currRotate.y < 90.0f)
            {
                currRotate.y += Time.deltaTime * 720.0f;
                transform.localEulerAngles = currRotate;
                yield return null;
            }
            if (targetAlias.Length > 0)
            {
                panelIcon.sprite = openSprite;
                contentGroup.alpha = 1.0f;
                glassIcon.color = new Color32( 255, 255, 255, 170 );
            }
            while (currRotate.y > 0.0f)
            {
                currRotate.y -= Time.deltaTime * 720.0f;
                transform.localEulerAngles = currRotate;
                yield return null;
            }
            transform.localEulerAngles = Vector3.zero;
            button.interactable = true;
            if (targetAlias.Length > 0)
            {
                lightParticle.gameObject.SetActive( true );
                isOpen = true;
                if (OnOpenCard != null)
                    OnOpenCard();
            }
        }
    }
}