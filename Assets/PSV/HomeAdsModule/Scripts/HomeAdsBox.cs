﻿#define Prototype_4_2_OR_NEWER

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace PSV.HomeAds
{
    [RequireComponent( typeof( AudioSource ) )]
    public class HomeAdsBox : MonoBehaviour, IExternalLinkView, IPointerClickHandler
    {
        private const float open_box_duration = 1.4f;

        public System.Action OnClosed;

        [Header( "Warning! Specify camera for Canvases!" )]
        public bool isSimulatedTransition = false;

        public Animator animator;
        public HomeChildrenBox homeChildrenBox;

        public ParticleSystem confettiClickBox;
        public ParticleSystem confettiOpenBox;

        public AudioClip musicClip;
        public AudioClip openBoxClip;
        public AudioClip fireworksClip;

        private AudioSource fxSource;
        private float delayTime = 0;
        private int errorsCount = 0;
        private Dictionary<string, float> pausedMusics;

        private void Awake()
        {
            fxSource = GetComponent<AudioSource>();
            fxSource.loop = false;
#if Prototype_4_2_OR_NEWER
            Audio.AudioStreamContainer musicGroup = AudioController.GetGroup( StreamGroup.MUSIC );
            pausedMusics = musicGroup.GetPlayingNames();
            musicGroup.Release();
            AudioController.Play( musicClip ).Group( StreamGroup.MUSIC ).Volume( 0.6f ).Loop( true ).Start();
#endif
        }
        private void Start()
        {
            ExternalLinksManager.AddLink( this );
            delayTime = Time.time + 1.0f;
            if (HomeChildrenAdsData.isChildrenGame)
            {
                homeChildrenBox.InitAds();
            }
            animator.SetTrigger( "NewChest" );
        }
        private void OnDestroy()
        {
            ExternalLinksManager.DeleteLink( this );
        }

        public void OnPointerClick( PointerEventData eventData )
        {
            if (delayTime < Time.time)
            {
                if (HomeAdsOffer.isHomeInterstitialReady)
                {
                    TryOpenBox();
                }
                else
                {
                    ShowErrorReadyPanel();
                    InvokeRepeating( "TryOpenBox", 0.0f, 0.5f );
                }
                delayTime = float.MaxValue;
            }
        }

        public void Show()
        {
            gameObject.SetActive( true );
        }
        public void Hide()
        {
            gameObject.SetActive( false );
        }

        /// <summary>
        /// Invoke repeating method: <see cref="OnPointerClick(PointerEventData)"/>
        /// </summary>
        public void TryOpenBox()
        {
            if (HomeAdsOffer.isHomeInterstitialReady)
            {
                CancelInvoke( "TryOpenBox" );
                WarningPanel.Hide();
                animator.SetTrigger( "OpenChest" );
                fxSource.clip = openBoxClip;
                fxSource.mute = AudioController.GetGroup(StreamGroup.FX).IsMuted();
                fxSource.Play();
                Invoke( "PlayFireworksSound", openBoxClip.length );
                if (HomeChildrenAdsData.isChildrenGame)
                    Invoke( "AfterOpenChildrenAds", open_box_duration - 0.2f );
                else
                    Invoke( "SwitchToNextScene", open_box_duration );
                confettiClickBox.Play();
            }
            else if (errorsCount < 10)
            {
                errorsCount++;
            }
            else
            {
                SwitchToNextScene();
            }
        }

        /// <summary>
        /// Invoke method: <see cref="TryOpenBox"/>
        /// </summary>
        private void PlayFireworksSound()
        {
            fxSource.clip = fireworksClip;
            fxSource.mute = AudioController.GetGroup(StreamGroup.FX).IsMuted();
            fxSource.loop = true;
            fxSource.Play();
        }

        private void ShowErrorReadyPanel()
        {
            string message;
            if (Languages.GetLanguage() == Languages.Language.Russian)
            {
                message = "Ой, нужно подождать.";
            }
            else
            {
                message = "Oh, you need to wait.";
            }
            WarningPanel.Show( message, false );
        }

        /// <summary>
        /// Invoke method: <see cref="TryOpenBox"/>
        /// </summary>
        public void SwitchToNextScene()
        {
            CancelInvoke();
            if (OnClosed != null)
                OnClosed();
            CanvasBlocker.OnClick -= SwitchToNextScene;
#if Prototype_4_2_OR_NEWER
            Audio.AudioStreamContainer musicGroup = AudioController.GetGroup( StreamGroup.MUSIC );
            musicGroup.Release();
            foreach (var music in pausedMusics)
            {
                AudioController.PlayMusic( music.Key, music.Value );
            }
#endif
            WarningPanel.Hide();
            if (isSimulatedTransition)
            {
                SceneLoader.SimulateTransition( Hide, true );
            }
            else
            {
                SceneLoader.SwitchToScene( OffersManager.suspendedScene );
            }
        }

        /// <summary>
        /// Invoke method: <see cref="TryOpenBox"/>
        /// </summary>
        private void AfterOpenChildrenAds()
        {
            animator.SetTrigger( "CloseChest" );
            confettiOpenBox.Play();
            homeChildrenBox.Show();
        }
        
        bool IExternalLinkView.IsStatic { get { return false; } }
        void IExternalLinkView.Show( bool param ) { }
        
    }
}