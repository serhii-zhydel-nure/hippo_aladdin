﻿#define PromoModuleDependency

using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using PSV.PopularNotify;
using System;

namespace PSV.HomeAds
{
    [System.Serializable]
    public class HomeChildrenAdsData
    {
        private const string thisAppForChildrenPref = "IsChildrenAdsOnly";
        private const string savedVersionPref = "ChildrenAdsDataVersion";
        private const string storageFileName = "AdsData.json";
        private const string serverContentHandler = "http://psvpromo.psvgamestudio.com/Scr/popular_notify.php";

        public delegate void OnLoadContent( string alias, Sprite content );

        public string[] alias;
        public string[] content;
        public int countShow;


        public void LoadContent( int index, OnLoadContent onLoad )
        {
            if (onLoad == null)
                return;
            string localContentPath = GetStorageContentPath( alias[index] );
            if (File.Exists( localContentPath ))
            {
                Texture2D icon = new Texture2D( 180, 180, TextureFormat.RGBA32, false );
                if (icon.LoadImage( File.ReadAllBytes( localContentPath ) ))
                {
                    Sprite resultIcon = Sprite.Create( icon, new Rect( 0, 0, icon.width, icon.height ), new Vector2( 0.5f, 0.5f ) );
                    onLoad( alias[index], resultIcon );
                    return;
                }
                Debug.LogError( "HomeChildrenADS: LoadContent can't read content alias " + alias[index] );
            }
            CoroutineHandler.Start( DownloadIcon( alias[index], content[index], onLoad ) );
        }

        public void Save()
        {
            if (this == null || alias == null || content == null)
                return;
            string result = JsonUtility.ToJson( this );
            File.WriteAllText( GetStorageFolderPath() + Path.DirectorySeparatorChar + storageFileName, result );
        }

        public static void InitChildrenAds( PopularityData popularityData, PopularityRating ratingHandler )
        {
            if (popularityData == null)
            {
                Debug.LogWarning( "HomeChildrenAdsData: Initialization skiped because popularity data is null!" );
                return;
            }
            if (StorageVersion >= popularityData.version)
                return;
            int counеChildrenAds = 3; // default count icons
            if (popularityData.isChildrens.Length > 0)
            {
                if (ratingHandler.positionInRatingList < 1)
                {
                    ratingHandler.positionInRatingList = PopularityRating.FindAliasInList( ConstSettings.GetApplicationAlias(), popularityData.rating );
                    if (ratingHandler.positionInRatingList != PopularityRating.NOT_EXIST_IN_LIST)
                    {
                        ratingHandler.positionInRatingList++; // rating position start at 1
                        counеChildrenAds = popularityData.isChildrens[ratingHandler.positionInRatingList - 1];
                    }
                }
                else
                {
                    counеChildrenAds = popularityData.isChildrens[ratingHandler.positionInRatingList - 1];
                }
            }
            if (isChildrenGame = counеChildrenAds > 0)
            {
                SelectChildrenAdsContent( popularityData, ratingHandler.GetInstalledAlias(), counеChildrenAds, popularityData.version )
                              .Save();
            }
        }

        public static HomeChildrenAdsData SelectChildrenAdsContent( PopularityData data, string[] installedAlias, int minimumCount, int version )
        {
            if (HomeChildrenAdsData.StorageVersion >= version)
            {
                return null;
            }
            int countAdsDone = 0;
            int countAds = Math.Max( minimumCount, data.adsAlias.Length );
            var result = new HomeChildrenAdsData
            {
                alias = new string[countAds],
                content = new string[countAds],
                countShow = minimumCount,
            };
            for (int i = 0; i < data.adsAlias.Length; i++)
            {
                int index = data.adsAlias[i] - 1;
                if (index < 0 || index >= data.rating.Length)
                {
                    continue;
                }
                string alias = data.rating[index];

                if (PopularityRating.FindAliasInList( alias, installedAlias ) == PopularityRating.NOT_EXIST_IN_LIST)
                {
                    result.alias[countAdsDone] = alias;
                    if (i >= data.adsContent.Length || string.IsNullOrEmpty( data.adsContent[i] ))
                        result.content[countAdsDone] = string.Empty;
                    else if (data.adsContent[i].StartsWith( "http" ))
                        result.content[countAdsDone] = data.adsContent[i];
                    else
                        result.content[countAdsDone] = "https://" + data.adsContent[i];
                    countAdsDone++;
                }
            }
            for (int i = 0; i < data.rating.Length && countAds > countAdsDone; i++)
            {
                string alias = data.rating[i];
                bool isReady = false;
                for (int j = 0; j < countAdsDone; j++)
                {
                    if (alias == result.alias[j])
                    {
                        isReady = true;
                        break;
                    }
                }
                if (!isReady && PopularityRating.FindAliasInList( alias, installedAlias ) == PopularityRating.NOT_EXIST_IN_LIST)
                {
                    result.alias[countAdsDone] = alias;
                    result.content[countAdsDone] = string.Empty;
                    countAdsDone++;
                }
            }
            HomeAds.HomeChildrenAdsData.StorageVersion = version;
            return result;
        }

        public static bool isChildrenGame
        {
            get
            {
                return PlayerPrefs.HasKey( thisAppForChildrenPref );
            }
            set
            {
                if (value)
                    PlayerPrefs.SetString( thisAppForChildrenPref, "1" );
                else
                    PlayerPrefs.DeleteKey( thisAppForChildrenPref );
                NextFrameCall.SavePrefs();
            }
        }

        public static HomeChildrenAdsData Load()
        {
            string folder = GetStorageFolderPath();
            if (Directory.Exists( folder ))
            {
                string filePath = folder + Path.DirectorySeparatorChar + storageFileName;
                if (File.Exists( filePath ))
                {
                    try
                    {
                        string content = File.ReadAllText( filePath );
                        return JsonUtility.FromJson<HomeChildrenAdsData>( content );
                    }
                    catch (System.Exception e)
                    {
                        Debug.LogError( e.ToString() );
                    }
                }
            }
            return null;
        }

        private static string GetStorageFolderPath()
        {
            string folder = Application.persistentDataPath + Path.DirectorySeparatorChar + "ChildrenAds";
            if (!Directory.Exists( folder ))
            {
                Directory.CreateDirectory( folder );
            }
            return folder;
        }
        private static string GetStorageContentPath( string alias )
        {
#if PromoModuleDependency
            string folder = PromoPlugin.PromoModule.GetExternalPath();
            if (!Directory.Exists( folder ))
            {
                Directory.CreateDirectory( folder );
            }
#else
            string folder = GetStorageFolderPath() + Path.DirectorySeparatorChar;
#endif
            return folder + alias.Split( '/' )[0] + ".png";
        }

        private static IEnumerator DownloadIcon( string alias, string content, OnLoadContent onLoad )
        {
            if (string.IsNullOrEmpty( content ))
            {
                string platform = "0";
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                    platform = "1";

                string secretKey = "PoPuLaR";

                string hash = Md5Sum( alias + platform + secretKey );
                string urlGetContetn = serverContentHandler
                                        + "?alias=" + WWW.EscapeURL( alias )
                                        + "&platform=" + platform
                                        + "&hash=" + hash;

                using (WWW wwwGetContent = new WWW( urlGetContetn ))
                {
                    yield return wwwGetContent;
                    while (!wwwGetContent.isDone)
                        yield return null;
                    if (string.IsNullOrEmpty( wwwGetContent.error ))
                    {
                        content = wwwGetContent.text;
                    }
                    else
                    {
                        Debug.LogError( "ChildrenAds: Can't get content url from server. Error: " + wwwGetContent.error );
                        yield break;
                    }
                }
            }

            if (string.IsNullOrEmpty( content ))
            {
                if (onLoad != null)
                    onLoad( alias, null );
            }
            using (WWW www = new WWW( content ))
            {
                yield return www;
                while (!www.isDone)
                    yield return null;
                if (string.IsNullOrEmpty( www.error ))
                {
                    Texture2D result = www.texture;
                    if (result != null)
                    {
                        File.WriteAllBytes( GetStorageContentPath( alias ), result.EncodeToPNG() );
                        Sprite resultIcon = Sprite.Create( result, new Rect( 0, 0, result.width, result.height ), new Vector2( 0.5f, 0.5f ) );
                        if (onLoad != null)
                            onLoad( alias, resultIcon );
                    }
                }
            }
        }

        public static string Md5Sum( string strToEncrypt )
        {
#if PromoModuleDependency
            return PromoPlugin.Crypto.Md5Sum( strToEncrypt );
#else
            System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
            byte[] bytes = ue.GetBytes( strToEncrypt );
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash( bytes );
            string hashString = "";
            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += System.Convert.ToString( hashBytes[i], 16 ).PadLeft( 2, '0' );
            }
            return hashString.PadLeft( 32, '0' );
#endif
        }

        public static int StorageVersion
        {
            get
            {
                return PlayerPrefs.GetInt( savedVersionPref, -1 );
            }
            set
            {
                PlayerPrefs.SetInt( savedVersionPref, value );
                NextFrameCall.SavePrefs();
            }
        }
    }
}