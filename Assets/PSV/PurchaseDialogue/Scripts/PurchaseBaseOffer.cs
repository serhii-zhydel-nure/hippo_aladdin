﻿using UnityEngine;

namespace PSV.PurchaseDialogue
{
    public abstract class PurchaseBaseOffer : ISceneOffer
    {
        public int priority { get { return 50; } }

        public virtual OfferReply TryShow( Scenes current_scene, Scenes target_scene )
        {
            var sceneList = ScenesList();
            if (ExtraConditions( current_scene, target_scene ))
            {
                for (int i = 0; i < sceneList.Length; i++)
                {
                    if (sceneList[i] == current_scene)
                    {
                        if (PurchaseMeService.Instance != null &&
                               PurchaseMeService.Instance.ShowPurchaseDialogue( current_scene, target_scene ))
                        {
                            OnShowDialog( current_scene, target_scene );
                            return OfferReply.ShownNoAds;
                        }
                    }
                }
            }
            return OfferReply.Skipped;
        }

        protected abstract bool ExtraConditions( Scenes current_scene, Scenes target_scene );

        protected abstract void OnShowDialog( Scenes current_scene, Scenes target_scene );

        public virtual bool IsEmpty()
        {
            return ScenesList().Length == 0;
        }

        public abstract Scenes[] ScenesList();

        public abstract Scenes OfferScene();
    }
}
