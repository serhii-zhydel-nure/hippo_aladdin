﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PSV.PurchaseDialogue
{

	public class DataManager :ScriptableObject
	{
		public AudioClip
			buy_full_ar,
			buy_full_cn,
			buy_full_de,
			buy_full_en,
			buy_full_fr,
			buy_full_it,
			buy_full_pl,
			buy_full_pt,
			buy_full_ru,
			buy_full_sp;

		private Dictionary<Languages.Language, AudioClip>
			_phrases = null;

		public Dictionary<Languages.Language, AudioClip>
			Phrases
		{
			get
			{
				if (_phrases == null)
				{
					_phrases = new Dictionary<Languages.Language, AudioClip> ( )
					{
						{Languages.Language.Arabic, buy_full_ar },
						{Languages.Language.Chinese, buy_full_cn },
						{Languages.Language.German, buy_full_de },
						{Languages.Language.English, buy_full_en },
						{Languages.Language.French, buy_full_fr },
						{Languages.Language.Italian, buy_full_it },
						{Languages.Language.Polish, buy_full_pl },
						{Languages.Language.Portuguese, buy_full_pt },
						{Languages.Language.Russian, buy_full_ru },
						{Languages.Language.Spanish, buy_full_sp },
					};
				}
				return _phrases; 
			}
		}

		public AudioClip GetPhrase ()
		{
			AudioClip res = GetPhrase ( Languages.GetLanguage ( ) );
			if (res == null)
			{
				res = GetPhrase ( Languages.Language.English );
			}
			return res;
		}


		public AudioClip GetPhrase (Languages.Language lang)
		{

			AudioClip res = null;
			if (Phrases.ContainsKey ( lang ))
			{
				res = Phrases [lang];
			}
			return res;
		}



#if UNITY_EDITOR
		[MenuItem ( "Assets/Create/PurchaseDialogue/DataManager" )]
		public static void CreateAsset ()
		{
				DataManager Instance = ScriptableObject.CreateInstance<DataManager> ( );

				string path = AssetDatabase.GetAssetPath ( Selection.activeObject );
				if (path == "")
				{
					path = "Assets";
				}
				else if (Path.GetExtension ( path ) != "")
				{
					path = path.Replace ( Path.GetFileName ( AssetDatabase.GetAssetPath ( Selection.activeObject ) ), "" );
				}

				string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath ( path + "/NewDataManager.asset" );

				AssetDatabase.CreateAsset ( Instance, assetPathAndName );

				AssetDatabase.SaveAssets ( );
				AssetDatabase.Refresh ( );
				EditorUtility.FocusProjectWindow ( );
				Selection.activeObject = Instance;
			//}
			//else
			//{
			//	Debug.LogError ( "There can be only one DataManager asset in project.\n" + AssetDatabase.GetAssetPath ( Instance ) );
			//	Selection.activeObject = Instance;
			//}
		}
#endif

	}
}
