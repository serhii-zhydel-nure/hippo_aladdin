﻿#define Prototype_4_2_OR_NEWER
using UnityEngine;


namespace PSV.PurchaseDialogue
{
    [RequireComponent(typeof(AudioSource))]
    public class PurchasePhrase : MonoBehaviour
    {

        private AudioSource src;

        public DataManager data_manager;


        private void Awake()
        {
            src = GetComponent<AudioSource>();
            src.loop = false;
            src.playOnAwake = false;
        }

        void OnEnable()
        {
            PlayPhrase(true);
        }


        void OnDisable()
        {
            PlayPhrase(false);
        }

        private void PlayPhrase(bool play)
        {
            if (play)
            {
                Debug.Log("Play phrase " + data_manager);

                if (data_manager != null)
                {
                    AudioClip phrase = data_manager.GetPhrase();
                    if (phrase != null)
                    {
                        src.clip = phrase;
#if Prototype_4_2_OR_NEWER
                        src.volume = AudioController.GetGroup(StreamGroup.VOICE).Volume;
                        src.mute = AudioController.GetGroup( StreamGroup.VOICE ).IsMuted();
#else
                        src.volume = GameSettings.GetSoundsVol();
                        src.mute = !GameSettings.IsSoundsEnabled();
#endif
                        Debug.Log("Play phrase " + phrase.name);
                        src.Play();

                    }
                }
            }
            else
            {
                src.Stop();
            }
        }
    }
}
