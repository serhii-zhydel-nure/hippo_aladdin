﻿using UnityEngine;
using System.Collections;


namespace PSV.PurchaseDialogue
{

    using IAP;


    public class PurchaseDialogueUI : DialoguePanel
    {
        public Products
            product = Products.SKU_ADMOB;


        public void OpenDialogue()
        {
            if (!BillingManager.IsProductPurchased(this.product))
            {
                ShowPannel(true);
            }
        }


        public void CloseDialogue()
        {
            BillingManager.OnPurchaseSucceded -= OnPurchaseSucceded;
            ShowPannel(false);
        }



        public void Purchase()
        {
            BillingManager.OnPurchaseSucceded += OnPurchaseSucceded;
            BillingManager.Purchase(product);
        }


        void OnPurchaseSucceded( ProductProperties prop )
        {
            if (prop.product == this.product)
            {
                CloseDialogue();
            }
        }

        protected override void ShowPannel(bool param, bool use_tween = true)
        {
            base.ShowPannel(param, use_tween);
        }

    }
}
