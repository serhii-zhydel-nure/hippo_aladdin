﻿#define Prototype_4_2_OR_NEWER

using UnityEngine;
using System.Collections;

namespace PSV.PurchaseDialogue
{
    using IAP;


    public class PurchaseMeService : MonoBehaviour
    {

        public Products
            product = Products.SKU_ADMOB;

        private Scenes
            saved_target;
#if Prototype_4_2_OR_NEWER
        private static float
            primary_interval { get { return PurchaseOffer.primary_interval; } }
        private static float
            secondary_interval { get { return PurchaseOffer.secondary_interval; } }
#else
        private Scenes
            purchase_scene = Scenes.PurchaseMe;
        private const float
            primary_interval = 60f,
            secondary_interval = 120f;
#endif

        private bool
            use_secondary = false;

        

        private float show_time = primary_interval;

        private const string SECONDARY_INTERVAL_PREF = "PurchaseMeService:use_secondary";

        #region Singleton

        private static PurchaseMeService instance;

        public static PurchaseMeService Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject e_man = new GameObject( "PurchaseMeService" );

                    SetInstance(e_man.AddComponent<PurchaseMeService>());
                }
                return instance;
            }
        }


        static void SetInstance(PurchaseMeService _instance)
        {
            instance = _instance;
            DontDestroyOnLoad(instance.gameObject);
            instance.use_secondary = PlayerPrefs.GetInt( SECONDARY_INTERVAL_PREF, 0 ) == 1;
        }

        void Awake()
        {
            if (instance == null)
            {
                SetInstance(this);
            }
            else
            {
                Debug.LogError( "PurchaseMeService: not allowed multiple instances" );
            }
        }


        #endregion



        void OnEnable()
        {
            PurchaseMeCloseButton.OnPurchaseClosed += DialogueClosed;
            PurchaseMeBuyButton.OnPurchaseProceed += ProceedWithPurchase;
        }

        void OnDisable()
        {
            PurchaseMeCloseButton.OnPurchaseClosed -= DialogueClosed;
            PurchaseMeBuyButton.OnPurchaseProceed -= ProceedWithPurchase;
        }

        //Call this at the end of Game cycle (all levels complete, passed 2-3 game loops, 2-3 days since installation)
        public void SetSecondaryInterval()
        {
            use_secondary = true;
            PlayerPrefs.SetInt( "", use_secondary ? 1 : 0 );
            NextFrameCall.SavePrefs();
        }

        public bool ShowPurchaseDialogue(Scenes current_scene, Scenes target_scene)
        {
            bool res = false;
            if (CanShowPurchaseMe())
            {
                ResetShowTime();
                saved_target = SceneLoader.GetTargetScene();
#if Prototype_4_2_OR_NEWER
                Scenes purchase_scene = PurchaseOffer.purchase_scene;
#endif
                SceneLoader.SetTargetScene(purchase_scene);
                res = true;
            }
            return res;
        }

        private void ProceedWithPurchase()
        {
            BillingManager.OnPurchaseSucceded += OnPurchaseSucceded;
            BillingManager.Purchase(product);
        }


        private void OnPurchaseSucceded(ProductProperties prop)
        {
            if (prop.product == product && prop.type == ProductType.NonConsumable)
            {
                BillingManager.OnPurchaseSucceded -= OnPurchaseSucceded;
                DialogueClosed();
            }
        }

        private void DialogueClosed()
        {
#if Prototype_4_2_OR_NEWER
            Scenes purchase_scene = PurchaseOffer.purchase_scene;
#endif
            if (SceneLoader.GetCurrentScene() == purchase_scene)
            {
                SceneLoader.SwitchToScene(saved_target);
            }
        }

        private bool CanShowPurchaseMe()
        {
#if Prototype_4_2_OR_NEWER
            Scenes purchase_scene = PurchaseOffer.purchase_scene;
#endif
            if (purchase_scene == Scenes.None)
            {
                Debug.LogError( "PurchaseMeService: no scene defined." );
                return false;
            }
            return ManagerGoogle.IsAdmobEnabled() && Time.time > show_time;
        }

        private void ResetShowTime()
        {
            show_time = Time.time + GetInterval();
        }


        private float GetInterval()
        {
            return use_secondary ? secondary_interval : primary_interval;
        }

    }
}
