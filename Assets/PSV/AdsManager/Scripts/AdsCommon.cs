﻿namespace PSV.ADS
{
    public enum AdNetwork
    {
        NeatPlug,
        GoogleAds,
        HomeAds,
        MobFox,
        Kidoz,
        MidGAD,
        HighGAD,
        Appodeal,
    }

    public class ProviderParams
    {
        public const string
            NEATPLUG_BANNER_ID = "NEATPLUG_BANNER_ID",
            NEATPLUG_INTERSTITIAL_ID = "NEATPLUG_INTERSTITIAL_ID",

            HOMEADS_BANNER_ID = "HOMEADS_BANNER_ID",
            HOMEADS_INTERSTITIAL_ID = "HOMEADS_INTERSTITIAL_ID",
            HOMEADS_VUNGLE_PLACEMENT_ID_INTERSTITIAL = "HOMEADS_VUNGLE_PLACEMENT_ID_INTERSTITIAL",

            GOOGLEADS_BANNER_ID = "GOOGLEADS_BANNER_ID",
            GOOGLEADS_INTERSTITIAL_ID = "GOOGLEADS_INTERSTITIAL_ID",
            GOOGLEADS_NATIVE_ID = "GOOGLEADS_NATIVE_ID",
            GOOGLEADS_REWARDED_ID = "GOOGLEADS_REWARDED_ID",  //due to singleton implementation of rewarded it can't create extra instances
            GOOGLEADS_VUNGLE_PLACEMENT_ID_INTERSTITIAL = "GOOGLEADS_VUNGLE_PLACEMENT_ID_INTERSTITIAL",
            GOOGLEADS_VUNGLE_PLACEMENT_ID_REWARDED = "GOOGLEADS_VUNGLE_PLACEMENT_ID_REWARDED",

            MIDGAD_BANNER_ID = "MIDGAD_BANNER_ID",
            MIDGAD_INTERSTITIAL_ID = "MIDGAD_INTERSTITIAL_ID",
            MIDGAD_VUNGLE_PLACEMENT_ID_INTERSTITIAL = "MIDGAD_VUNGLE_PLACEMENT_ID_INTERSTITIAL",

            HIGHGAD_BANNER_ID = "HIGHGAD_BANNER_ID",
            HIGHGAD_INTERSTITIAL_ID = "HIGHGAD_INTERSTITIAL_ID",
            HIGHGAD_VUNGLE_PLACEMENT_ID_INTERSTITIAL = "HIGHGAD_VUNGLE_PLACEMENT_ID_INTERSTITIAL",

            APPODEAL_APPKEY = "APPODEAL_APPKEY",

            MOBFOX_BANNER_ID = "MOBFOX_BANNER_ID",
            MOBFOX_INTERSTITIAL_ID = "MOBFOX_INTERSTITIAL_ID",

            KIDOZ_PUB_ID = "KIDOZ_PUB_ID",
            KIDOZ_SECURITY_TOKEN = "KIDOZ_SECURITY_TOKEN";

        private readonly string[] all_params = new string[]
        {
            NEATPLUG_BANNER_ID,
            NEATPLUG_INTERSTITIAL_ID,
            "",
            GOOGLEADS_BANNER_ID,
            GOOGLEADS_INTERSTITIAL_ID,
            GOOGLEADS_NATIVE_ID,
            GOOGLEADS_REWARDED_ID,	//due to singleton implementation of rewarded it can't create extra instances
            GOOGLEADS_VUNGLE_PLACEMENT_ID_INTERSTITIAL,
            GOOGLEADS_VUNGLE_PLACEMENT_ID_REWARDED,
            "",
            HIGHGAD_BANNER_ID,
            HIGHGAD_INTERSTITIAL_ID,
            HIGHGAD_VUNGLE_PLACEMENT_ID_INTERSTITIAL,
            "",
            MIDGAD_BANNER_ID,
            MIDGAD_INTERSTITIAL_ID,
            MIDGAD_VUNGLE_PLACEMENT_ID_INTERSTITIAL,
            "",
            HOMEADS_BANNER_ID,
            HOMEADS_INTERSTITIAL_ID,
            HOMEADS_VUNGLE_PLACEMENT_ID_INTERSTITIAL,
            "",
            APPODEAL_APPKEY,
            "",
            MOBFOX_BANNER_ID,
            MOBFOX_INTERSTITIAL_ID,
            "",
            KIDOZ_PUB_ID,
            KIDOZ_SECURITY_TOKEN,
        };

        public string this[int index]
        {
            get
            {
                if (all_params.Length > index)
                    return all_params[index];
                else
                    return "####";
            }
        }

        public int this[string param_name]
        {
            get
            {
                for (int i = 0; i < all_params.Length; i++)
                {
                    if (all_params[i].Equals( param_name ))
                    {
                        return i;
                    }
                }
                return -1;
            }
        }

        public static implicit operator string[] ( ProviderParams p )
        {
            return p.all_params;
        }

        public static string FromObsoleteOrder( int obsolete_index )
        {
            string[] obsolete_order =
            {
                HOMEADS_BANNER_ID,
                HOMEADS_INTERSTITIAL_ID,
                "HOMEADS_NATIVE_ID",    // Removed
                "HOMEADS_REWARDED_ID",  // Removed
                NEATPLUG_BANNER_ID,
                NEATPLUG_INTERSTITIAL_ID,
                GOOGLEADS_BANNER_ID,
                GOOGLEADS_INTERSTITIAL_ID,
                GOOGLEADS_NATIVE_ID,
                GOOGLEADS_REWARDED_ID,
                MOBFOX_BANNER_ID,
                MOBFOX_INTERSTITIAL_ID,
                KIDOZ_PUB_ID,
                KIDOZ_SECURITY_TOKEN,
                MIDGAD_BANNER_ID,
                MIDGAD_INTERSTITIAL_ID,
                HIGHGAD_BANNER_ID,
                HIGHGAD_INTERSTITIAL_ID,
            };
            return obsolete_order[obsolete_index];
        }
    }
}