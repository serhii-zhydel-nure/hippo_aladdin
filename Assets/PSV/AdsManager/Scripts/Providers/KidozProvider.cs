﻿//#define KIDOZ_PRESENT

#if KIDOZ_PRESENT
using System;
using System.Collections.Generic;
using KidozSDK;
using UnityEngine;

namespace PSV.ADS
{
	public class KidozProvider :IAdProvider
	{


		//protected AdSize
		//	banner_size = AdSize.Banner,
		//	native_size = new AdSize ( 320, 80 ); //minimum height = 80

		protected Kidoz.BANNER_POSITION
			banner_position = Kidoz.BANNER_POSITION.BOTTOM_CENTER;
		//protected Kidoz.FLEXI_VIEW_POSITION
		//	native_position = Kidoz.FLEXI_VIEW_POSITION.BOTTOM_CENTER;

		protected bool
			ads_enabled = true,
			banner_ready = false,
			interstitial_ready = false,
			rewarded_ready = false;

		private bool
			is_designed_for_families = false,
			tagged_for_children = true,
			rewarded_loading = false,
			banner_loading = false,
			interstitial_loading = false,
			banner_visible = false;

		public string
			publisher_id = "",
			security_token = "";

		private const string
			publisher_id_param = ProviderParams.KIDOZ_PUB_ID,
			security_token_param = ProviderParams.KIDOZ_SECURITY_TOKEN;

		private string[]
			necessary_params = new string[] {
				publisher_id_param,
				security_token_param,
			};

		public event Action<AdNetwork, bool>
			OnBannerEvent,
			OnNativeEvent,
			OnInterstitialEvent,
			OnRewardedEvent;
		public event Action<string, bool>
			OnMessageLogged;
		public event Action
			OnInterstitialShown,
			OnInterstitialClosed,
			OnBannerReadyChanged,
			OnNativeReadyChanged,
			OnRewardedShown,
			OnRewardedClosed,
			OnRewardedCompleted;

		private AdNetwork
			net = AdNetwork.Kidoz;

		private float
			request_interval = 10, 
			last_banner_request = 0,
			last_interstitial_request,
			last_native_request = 0,
			last_rewarded_request = 0;

		private bool
			ready
		{
			get
			{
				return Kidoz.isInitialised ( );
			}
		}
#region NotSupported

		public void ShowNativeAd (bool show)
		{
		}

		public bool IsNativeAdAvailable ()
		{
			return false;
		}

		public void RefreshNativeAd (AdPosition ad_pos, AdSize ad_size)
		{
		}


#endregion

#region General Methods

		public new string ToString ()
		{
			return GetNetworkType ( ).ToString ( );
		}


		private bool CanRequestAd (float last_time)
		{
			return Time.time - last_time >= AdsManager.REQUEST_INTERVAL;
		}


		protected void LogMessage (string message, bool error = false)
		{
			if (OnMessageLogged != null)
			{
				OnMessageLogged ( this.GetType ( ).ToString ( ) + " " + message, error );
			}
		}

		public void DisableAds ()
		{
			ads_enabled = false;
			HideBanner ( );
			//rewarded stays active to keep user receiving bonuses for watching ad
		}

		public void EnableAds ()
		{
			ads_enabled = true;
			LoadBanner ( );
			LoadInterstitial ( );
		}

		virtual public AdNetwork GetNetworkType ()
		{
			return net;
		}

		virtual public string [] GetNecessaryParams ()
		{
			return necessary_params;
		}


		virtual public void SetRequestInterval (float interval)
		{
			request_interval = interval;
		}

		public void Init (bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized, object settings )
		{
			Kidoz.initSuccess += onKidozInitSuccess;
			Kidoz.initError += onKidozInitError;
			Kidoz.viewOpened += viewOpened;
			Kidoz.viewClosed += viewClosed;
			Kidoz.panelExpand += panelExpand;
			Kidoz.panelCollapse += panelCollapse;
			Kidoz.panelReady += panelReady;
			//Kidoz.flexiViewReady += flexiReady;		//deprecated
			//Kidoz.flexiViewShow += flexiShow;			//deprecated
			//Kidoz.flexiViewHide += flexiHide;			//deprecated
			Kidoz.playerOpen += playerOpen;
			Kidoz.playerClose += playerClose;
			Kidoz.interstitialOpen += interstitialOpen;
			Kidoz.interstitialClose += interstitialClose;
			Kidoz.interstitialReady += interstitialReady;
			Kidoz.interstitialOnLoadFail += interstitialOnLoadFail;
			Kidoz.interstitialOnNoOffers += interstitialOnNoOffers;
			Kidoz.onRewardedDone += onRewardedDone;
			Kidoz.onRewardedVideoStarted += onRewardedVideoStarted;
			Kidoz.rewardedOpen += rewardedOpen;
			Kidoz.rewardedClose += rewardedClose;
			Kidoz.rewardedReady += rewardedReady;
			Kidoz.rewardedOnLoadFail += rewardedOnLoadFail;
			Kidoz.rewardedOnNoOffers += rewardedOnNoOffers;
			Kidoz.bannerReady += bannerReady;
			Kidoz.bannerClose += bannerClose;
			Kidoz.bannerError += bannerError;


			tagged_for_children = children_tagged;
			is_designed_for_families = for_families;
			this.ads_enabled = ads_enabled;
			LogMessage ( "Initializing with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + tagged_for_children + ", is_designed_for_families=" + is_designed_for_families );
			ProcessParams ( settings );

		}


		protected Kidoz.BANNER_POSITION ConvertAdPosition (AdPosition pos)
		{
			Kidoz.BANNER_POSITION res = Kidoz.BANNER_POSITION.BOTTOM_CENTER;

			switch (pos)
			{
				case AdPosition.Bottom_Centered:
				case AdPosition.Middle_Centered:
					res = Kidoz.BANNER_POSITION.BOTTOM_CENTER;
					break;
				case AdPosition.Bottom_Left:
				case AdPosition.Middle_Left:
					res = Kidoz.BANNER_POSITION.BOTTOM_LEFT;
					break;
				case AdPosition.Bottom_Right:
				case AdPosition.Middle_Right:
					res = Kidoz.BANNER_POSITION.BOTTOM_RIGHT;
					break;
				case AdPosition.Top_Centered:
					res = Kidoz.BANNER_POSITION.TOP_CENTER;
					break;
				case AdPosition.Top_Right:
					res = Kidoz.BANNER_POSITION.TOP_RIGHT;
					break;
				case AdPosition.Top_Left:
					res = Kidoz.BANNER_POSITION.TOP_LEFT;
					break;
			}
			return res;
		}


		virtual protected void ProcessParams (object settings)
		{
			Dictionary<string, string> _settings = settings as Dictionary<string, string>;
			LogMessage ( "Processing params: " + _settings.ConvertToString ( ) );

			if (_settings != null)
			{
				if (!_settings.TryGetValue ( publisher_id_param, out publisher_id ))
				{
					LogMessage ( "error parsing " + publisher_id_param, true );
				}

				if (!_settings.TryGetValue ( security_token_param, out security_token ))
				{
					LogMessage ( "error parsing " + security_token_param, true );
				}
				if (!string.IsNullOrEmpty ( publisher_id ) && !string.IsNullOrEmpty ( security_token ))
				{
					Kidoz.init ( publisher_id, security_token );
					LogMessage ( "initializing with  " + publisher_id + " " + security_token );
				}
				else
				{
					LogMessage ( "init failure, not all params received", true );
				}
			}
		}

#endregion

#region Banner

		protected void RequestBanner (bool forse)
		{
			if (!banner_loading || forse)
			{
				if (ads_enabled)
				{
					LogMessage ( "Requesting banner " + banner_position );
					banner_loading = true;
					Kidoz.loadBanner ( false, banner_position );
					last_banner_request = Time.time;
				}
			}
		}


		protected void LoadBanner (bool forse = false)
		{
			RequestBanner ( forse );
			HideBanner ( );
		}

		public bool IsBannerAdAvailable ()
		{
			return ready && banner_ready;
		}

		public void RefreshBannerAd (AdPosition ad_pos, AdSize ad_size)
		{
			LogMessage ( "RepositionAd" );
			Kidoz.BANNER_POSITION new_pos = ConvertAdPosition ( ad_pos );
			if (banner_position != new_pos)
			{
				banner_position = new_pos;
				Kidoz.setBannerPosition ( banner_position );
			}
		}

		public void ShowBannerAd (bool show)
		{
			if (ready)
			{
				if (show)
				{
					ShowBanner ( );
				}
				else
				{
					HideBanner ( );
				}
			}
			else
			{
				LogMessage ( "SDK not initialized yet" );
			}
		}

		protected void ShowBanner ()
		{
			banner_visible = true;
			Kidoz.showBanner ( );
		}

		protected void HideBanner ()
		{
			banner_visible = false;
			Kidoz.hideBanner ( );
		}

		protected void BannerReadyChanged (bool ready)
		{
			bool last_state = banner_ready;
			banner_ready = ready;
			banner_loading = false;
			if (OnBannerEvent != null)
			{
				OnBannerEvent ( GetNetworkType ( ), banner_ready );
			}
			if (OnBannerReadyChanged != null)
			{
				OnBannerReadyChanged ( );
			}
			if (!banner_ready)
			{
				if (last_state == true || CanRequestAd ( last_banner_request )) //if ad was loaded or last request was made some time before
				{
					LoadBanner ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.DelayedCall ( () => LoadBanner ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

#endregion

#region Interstitial

		public bool IsInterstitialAdAvailable ()
		{
			return interstitial_ready && Kidoz.getIsInterstitialLoaded ( );
		}

		protected void RequestInterstitial (bool forse)
		{
			if (!interstitial_loading || forse)
			{
				LogMessage ( "Requesting interstitial" );
				interstitial_loading = true;
				Kidoz.loadInterstitialAd ( false );
				last_interstitial_request = Time.time;
			}
		}



		protected void LoadInterstitial (bool forse = false)
		{
			if (forse || !IsInterstitialAdAvailable ( ))
			{
				RequestInterstitial ( forse );
			}
		}

		public bool ShowInterstitialAd ()
		{
			if (IsInterstitialAdAvailable ( ))
			{
				Kidoz.showInterstitial ( );
				return true;
			}
			return false;
		}

		protected void InterstitialReadyChanged (bool ready)
		{
			bool last_state = interstitial_ready;
			interstitial_ready = ready;
			interstitial_loading = false;
			if (OnInterstitialEvent != null)
			{
				OnInterstitialEvent ( GetNetworkType ( ), ready );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_interstitial_request )) //if ad was loaded or last request was made some time before
				{
					LoadInterstitial ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.DelayedCall ( () => LoadInterstitial ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

		private void InterstitialShown ()
		{
			if (OnInterstitialShown != null)
			{
				OnInterstitialShown ( );
			}
		}

		private void InterstitialClosed ()
		{
			if (OnInterstitialClosed != null)
			{
				OnInterstitialClosed ( );
			}
		}
#endregion

#region Rewarded

		public bool IsRewardedAdAvailable ()
		{
			return rewarded_ready && Kidoz.getIsRewardedLoaded ( );
		}


		protected void RequestRewardedAd (bool forse)
		{
			if (!rewarded_loading || forse)
			{
				LogMessage ( "Requesting rewarded" );
				rewarded_loading = true;
				rewarded_ready = false;
				Kidoz.loadRewardedAd ( false );
				last_rewarded_request = Time.time;
			}
		}

		protected void LoadRewardedVideoAd (bool forse = false)
		{
			RequestRewardedAd ( forse );
		}


		public bool ShowRewardedAd ()
		{
			if (IsRewardedAdAvailable ( ))
			{
				Kidoz.showRewarded ( );
				return true;
			}
			return false;
		}

		protected void RewardedReadyChanged (bool ready)
		{
			bool last_state = rewarded_ready;
			rewarded_ready = ready;
			rewarded_loading = false;

			if (OnRewardedEvent != null)
			{
				OnRewardedEvent ( GetNetworkType ( ), ready );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_rewarded_request )) //if ad was loaded or last request was made some time before
				{
					LoadRewardedVideoAd ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.DelayedCall ( () => LoadRewardedVideoAd ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

		private void RewardedShown ()
		{
			if (OnRewardedShown != null)
			{
				OnRewardedShown ( );
			}
		}
		private void RewardedClosed ()
		{
			if (OnRewardedClosed != null)
			{
				OnRewardedClosed ( );
			}
		}
		private void RewardedCompleted ()
		{
			if (OnRewardedCompleted != null)
			{
				OnRewardedCompleted ( );
			}
		}

#endregion

		/* ====== ADVERT EVENTS ====== */

#region Kidos Events

		private void onKidozInitSuccess (string value)
		{
			LogMessage ( "InitSuccess " + value );
			//ready = true;
			LoadBanner ( );
			LoadInterstitial ( );
			LoadRewardedVideoAd ( );
		}

		private void onKidozInitError (string value)
		{
			LogMessage ( "InitError: " + value, true );
			//ready = false;
		}



		private void viewOpened (string value)
		{
			LogMessage ( "Feed view opened " + value );
		}

		private void viewClosed (string value)
		{
			LogMessage ( "Feed view closed " + value );
		}

		private void panelExpand (string value)
		{
			LogMessage ( "panel view opened " + value );
		}

		private void panelCollapse (string value)
		{
			LogMessage ( "panel view closed " + value );
		}
		private void panelReady (string value)
		{
			LogMessage ( "panel ready " + value );

			//		Kidoz.changePanelVisibility(false);	
		}

		//private void flexiReady (string value)
		//{
		//	LogMessage ( "flexiview ready " + value );

		//}

		//private void flexiShow (string value)
		//{
		//	LogMessage ( "flexishow " + value );

		//}
		//private void flexiHide (string value)
		//{
		//	LogMessage ( "flexihide " + value );

		//}

		private void playerOpen (string value)
		{
			LogMessage ( "playerOpen " + value );

		}
		private void playerClose (string value)
		{
			LogMessage ( "playerClose " + value );

		}

#endregion



#region Banner callback handlers

		private void bannerReady (string value)
		{
			LogMessage ( "bannerReady " + value );
			BannerReadyChanged ( true );
		}

		private void bannerClose (string value)
		{
			LogMessage ( "bannerHide " + value );
		}

		private void bannerError (string value)
		{
			LogMessage ( "bannerError: " + value, true );
			BannerReadyChanged ( false );
		}

#endregion



#region Interstitial callback handlers


		private void interstitialOpen (string value)
		{
			if (interstitial_ready)
			{
				LogMessage ( "interstitialOpen " + value );
				InterstitialShown ( );
			}
			else
			{
				interstitialOnLoadFail ( value );
			}
		}

		private void interstitialClose (string value)
		{
			LogMessage ( "interstitialClose " + value );
			InterstitialClosed ( );
			InterstitialReadyChanged ( false );
		}

		private void interstitialReady (string value)
		{
			LogMessage ( "interstitialReady " + value );
			InterstitialReadyChanged ( true );
		}

		private void interstitialOnLoadFail (string value)
		{
			LogMessage ( "interstitialOnLoadFail " + value, true );
			InterstitialReadyChanged ( false );
		}

		private void interstitialOnNoOffers (string value)
		{
			LogMessage ( "interstitialOnNoOffers " + value, true );
			InterstitialReadyChanged ( false );
		}

#endregion

#region Rewarded callback holders

		private void onRewardedDone (string value)
		{
			LogMessage ( "onRewardedDone " + value );
			RewardedCompleted ( );
		}

		private void onRewardedVideoStarted (string value)
		{
			LogMessage ( "onRewardedVideoStarted " + value );
		}

		private void rewardedOpen (string value)
		{
			if (rewarded_ready)
			{
				LogMessage ( "rewardedOpen " + value );
				RewardedShown ( );
			}
			else
			{
				rewardedOnLoadFail ( value );
			}
		}

		private void rewardedClose (string value)
		{
			LogMessage ( "rewardedClose " + value );
			RewardedClosed ( );
			RewardedReadyChanged ( false );
		}

		private void rewardedReady (string value)
		{
			LogMessage ( "rewardedReady " + value );
			RewardedReadyChanged ( true );
		}

		private void rewardedOnLoadFail (string value)
		{
			LogMessage ( "rewardedOnLoadFail " + value, true );
			RewardedReadyChanged ( false );
		}

		private void rewardedOnNoOffers (string value)
		{
			LogMessage ( "rewardedOnNoOffers", true );
			RewardedReadyChanged ( false );
		}

#endregion

	}
}
#endif