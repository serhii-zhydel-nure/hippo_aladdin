﻿//#define MOBFOX_PRESENT
//#define EMULATE_REWARDED

#if MOBFOX_PRESENT
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PSV.ADS
{

	public class MobFoxAdsProvider :IAdProvider
	{

		private struct BannerPosition
		{
			public int left;
			public int top;

			public BannerPosition (int left = 0, int top = 0)
			{
				this.left = left;
				this.top = top;
			}

			public override string ToString ()
			{
				return ("<left: " + left + ", top: " + top + ">");
			}
		}

		private AdSize
			banner_size = AdSize.Standard_banner_320x50;

		private AdPosition
			ad_pos = AdPosition.Bottom_Centered;


		private bool
			emulating_rewarded = false,
			ads_enabled = true,
			banner_ready = false,
			interstitial_ready = false,
			rewarded_ready = false;

		private bool
			is_designed_for_families = false,
			tagged_for_children = true,
			rewarded_loading = false,
			banner_loading = false,
			interstitial_loading = false,
			banner_visible = false,
			native_visible = false;

		private string
			banner_id = "",
			interstitial_id = "",
			rewarded_id = "";

		private const string
			banner_param = ProviderParams.MOBFOX_BANNER_ID,
			interstitial_param = ProviderParams.MOBFOX_INTERSTITIAL_ID;

		private string[]
			necessary_params = new string[] {
				banner_param,
				interstitial_param,
			};

		public event Action<AdNetwork, bool>
			OnBannerEvent,
			OnNativeEvent,
			OnInterstitialEvent,
			OnRewardedEvent;
		public event Action<string, bool>
			OnMessageLogged;
		public event Action
			OnInterstitialShown,
			OnInterstitialClosed,
			OnBannerReadyChanged,
			OnNativeReadyChanged,
			OnRewardedShown,
			OnRewardedClosed,
			OnRewardedCompleted;

		private AdNetwork
			net = AdNetwork.MobFox;

		private float
			request_interval = 10, 
			last_banner_request = 0,
			last_interstitial_request,
			last_rewarded_request = 0;



#region NotSupported

		public void ShowNativeAd (bool show)
		{
		}

		public bool IsNativeAdAvailable ()
		{
			return false;
		}

		public void RefreshNativeAd (AdPosition ad_pos, AdSize ad_size)
		{
		}


#endregion

#region General Methods

		public new string ToString ()
		{
			return GetNetworkType ( ).ToString ( );
		}


		private bool CanRequestAd (float last_time)
		{
			return Time.time - last_time >= AdsManager.REQUEST_INTERVAL;
		}


		protected void LogMessage (string message, bool error = false)
		{
			if (OnMessageLogged != null)
			{
				OnMessageLogged ( this.GetType ( ).ToString ( ) + " " + message, error );
			}
		}

		public void DisableAds ()
		{
			ads_enabled = false;
			DestroyBanner ( );
			DestroyInterstitial ( );
			//rewarded stays active to keep user receiving bonuses for watching ad
		}

		public void EnableAds ()
		{
			ads_enabled = true;
			LoadBanner ( );
			LoadInterstitial ( );
		}

		public AdNetwork GetNetworkType ()
		{
			return net;
		}


		public string[] GetNecessaryParams ()
		{
			return necessary_params;
		}

		private void Subscribe ()
		{
			LogMessage ( "Subscribe" );
			MobFox.OnOrientationChange += OnOrientationChange;
			MobFox.OnSdkReady += OnSdkReady;
			MobFox.OnBannerClicked += HandleAdClicked;
			MobFox.OnBannerClosed += HandleAdClosed;
			MobFox.OnBannerError += HandleAdFailedToLoad;
			MobFox.OnBannerFinished += HandleAdFinished;
			MobFox.OnBannerReady += HandleAdLoaded;
			MobFox.OnInterstitialError += HandleInterstitialFailedToLoad;
			MobFox.OnInterstitialClicked += HandleInterstitialClicked;
			MobFox.OnInterstitialClosed += HandleInterstitialClosed;
			MobFox.OnInterstitialReady += HandleInterstitialLoaded;
			MobFox.OnInterstitialFinished += HandleInterstitialFinished;
		}

		private void OnOrientationChange (ScreenOrientation orientation)
		{
			LogMessage ( "Screen.width: " + Screen.width + ", Screen.height: " + Screen.height );
			DestroyBanner ( );
			BannerReadyChanged ( false, true );
		}

		private void OnSdkReady ()
		{
			LogMessage ( "OnSdkReady" );
			//UnityMainThreadDispatcher.Instance.Enqueue ( () =>
			//{
			LoadBanner ( );
			LoadInterstitial ( );
			//} );
		}

		virtual public void SetRequestInterval (float interval)
		{
			request_interval = interval;
		}

		public void Init (bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized, object settings )
		{
			tagged_for_children = children_tagged;
			is_designed_for_families = for_families;
			this.ads_enabled = ads_enabled;
			LogMessage ( "Initializing with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + tagged_for_children + ", is_designed_for_families=" + is_designed_for_families );
			Subscribe ( );
			ProcessParams ( settings );
			if (MobFox.Instance != null && MobFox.Instance.IsInitialized ( ))
			{
				OnSdkReady ( );
			}
			else
			{
				MobFox.CreateSingletone ( );
			}
		}


		private BannerPosition ConvertAdPosition (AdPosition pos, AdSize size)
		{
			float scale_coef = 160 / Screen.dpi;
			int width = Mathf.RoundToInt ( Screen.width * scale_coef );
			int height = Mathf.RoundToInt ( Screen.height * scale_coef );
			int half_w = (int) (size.w * 0.5f);
			BannerPosition res = new BannerPosition ( );
			switch (pos)
			{
				case AdPosition.Bottom_Centered:
					res.top = height;
					res.left = Mathf.RoundToInt ( width * 0.5f ) - half_w;
					break;
				case AdPosition.Bottom_Left:
					res.top = height;
					res.left = 0;
					break;
				case AdPosition.Bottom_Right:
					res.top = height;
					res.left = width - half_w;
					break;
				case AdPosition.Middle_Centered:
					res.top = Mathf.RoundToInt ( height * 0.5f );
					res.left = Mathf.RoundToInt ( width * 0.5f ) - half_w;
					break;
				case AdPosition.Middle_Left:
					res.top = Mathf.RoundToInt ( height * 0.5f ) - half_w;
					res.left = 0;
					break;
				case AdPosition.Middle_Right:
					res.top = Mathf.RoundToInt ( height * 0.5f );
					res.left = width - half_w;
					break;
				case AdPosition.Top_Centered:
					res.top = 0;
					res.left = Mathf.RoundToInt ( width * 0.5f ) - half_w;
					break;
				case AdPosition.Top_Left:
					res.top = 0;
					res.left = 0;
					break;
				case AdPosition.Top_Right:
					res.top = 0;
					res.left = width - half_w;
					break;
			}
			LogMessage ( "ConvertAdPosition : " + res + " half_w = " + half_w );
			return res;
		}


		private void ProcessParams (object settings)
		{
			Dictionary<string, string> _settings = settings as Dictionary<string, string>;
			LogMessage ( "Processing params: " + _settings.ConvertToString ( ) );

			if (_settings != null)
			{
				if (_settings.TryGetValue ( banner_param, out banner_id ))
				{
					LogMessage ( "banner initialized" );
					//LoadBanner ( true ); //will be load ONSdkReady
				}

				if (_settings.TryGetValue ( interstitial_param, out interstitial_id ))
				{
					LogMessage ( "interstitial initialized" );
					//LoadInterstitial ( true ); //will be load ONSdkReady
				}
			}
		}

		private string GetBannerID ()
		{
			return banner_id;
		}

		private string GetInterstitialID ()
		{
			return interstitial_id;
		}

		private string GetRewardedID ()
		{
			return rewarded_id;
		}

#endregion

#region Banner

		private void RequestBanner (bool forse)
		{
			if (!banner_loading || forse)
			{
				DestroyBanner ( );
				string id = GetBannerID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						LogMessage ( "Requesting banner" );
						banner_loading = true;
						BannerPosition banner_position = ConvertAdPosition ( ad_pos, banner_size );
						try
						{
							MobFox.Instance.RequestMobFoxBanner ( id, banner_position.left, banner_position.top, banner_size.w, banner_size.h );
						}
						catch (Exception e)
						{
							LogMessage ( "RequestBanner exception: " + e.Message, true );
						}
						HideBanner ( );
						last_banner_request = Time.time;
					}
					else
					{
						OnBannerRequestError ( id );
					}
				}
			}
		}

		private void OnBannerRequestError (string id)
		{
			LogMessage ( "BannerRequestError empty id", true );
		}

		private void LoadBanner (bool forse = false)
		{
			RequestBanner ( forse );
		}

		public bool IsBannerAdAvailable ()
		{
			return banner_ready;
		}

		public void RefreshBannerAd (AdPosition ad_pos, AdSize ad_size)
		{
			LogMessage ( "RepositionAd" );
			bool need_to_refresh = banner_size != ad_size || this.ad_pos != ad_pos;
			banner_size = ad_size;
			this.ad_pos = ad_pos;
			if (need_to_refresh)
			{
				BannerReadyChanged ( false, true );
			}
		}

		public void ShowBannerAd (bool show)
		{
			if (show)
			{
				ShowBanner ( );
			}
			else
			{
				HideBanner ( );
			}
		}

		private void ShowBanner ()
		{
			banner_visible = true;
			MobFox.Instance.ShowMobFoxBanner ( );
		}

		private void HideBanner ()
		{
			banner_visible = false;
			MobFox.Instance.HideMobFoxBanner ( );
		}

		private void DestroyBanner ()
		{
			HideBanner ( );
		}

		Coroutine delayed_request;

		private void BannerReadyChanged (bool ready, bool forse_load = false)
		{
			if (delayed_request != null)
			{
				DelayedCallHandler.StopCoroutine ( delayed_request );
			}
			bool last_state = banner_ready;
			banner_ready = ready;
			banner_loading = false;
			if (OnBannerEvent != null)
			{
				OnBannerEvent ( GetNetworkType ( ), ready );
			}
			if (OnBannerReadyChanged != null)
			{
				OnBannerReadyChanged ( );
			}
			if (!ready)
			{
				if (forse_load || last_state == true || CanRequestAd ( last_banner_request )) //if ad was loaded or last request was made some time before
				{
					LoadBanner ( forse_load );
				}
				else
				{
					// delay ad request frequency
					delayed_request = DelayedCallHandler.DelayedCall ( () => LoadBanner ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

#endregion

#region Interstitial

		public bool IsInterstitialAdAvailable ()
		{
			return interstitial_ready;
		}

		private void RequestInterstitial (bool forse)
		{
			if (!interstitial_loading || forse)
			{
				DestroyInterstitial ( );
				// Create an interstitial.
				string id = GetInterstitialID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						LogMessage ( "Requesting interstitial" );
						interstitial_loading = true;
						try
						{
							MobFox.Instance.RequestMobFoxInterstitial ( id );
						}
						catch (Exception e)
						{
							LogMessage ( "RequestInterstitial exception: " + e.Message, true );
						}
						last_interstitial_request = Time.time;
					}
					else
					{
						OnInterstitialRequestError ( id );
					}
				}
			}
		}

		private void OnInterstitialRequestError (string id)
		{
			LogMessage ( "InterstitialRequestError empty id", true );
		}

		private void LoadInterstitial (bool forse = false)
		{
			if (forse || !IsInterstitialAdAvailable ( ))
			{
				RequestInterstitial ( forse );
			}
		}

		public bool ShowInterstitialAd ()
		{
			if (IsInterstitialAdAvailable ( ))
			{
				MobFox.Instance.ShowMobFoxInterstitial ( );
				return true;
			}
			return false;
		}

		private void DestroyInterstitial ()
		{

		}

		private void InterstitialReadyChanged (bool ready)
		{
			bool last_state = interstitial_ready;
			interstitial_ready = ready;
			interstitial_loading = false;
			if (OnInterstitialEvent != null)
			{
				OnInterstitialEvent ( GetNetworkType ( ), ready );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_interstitial_request )) //if ad was loaded or last request was made some time before
				{
					LoadInterstitial ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.DelayedCall ( () => LoadInterstitial ( ), AdsManager.REQUEST_INTERVAL );
				}
			}
		}

		private void InterstitialShown ()
		{
			if (OnInterstitialShown != null)
			{
				OnInterstitialShown ( );
			}
		}

		private void InterstitialClosed ()
		{
			if (emulating_rewarded)
			{
				emulating_rewarded = false;
				if (OnRewardedClosed != null)
				{
					OnRewardedClosed ( );
				}
				if (OnRewardedCompleted != null)
				{
					OnRewardedCompleted ( );
				}
			}
			else
			{
				if (OnInterstitialClosed != null)
				{
					OnInterstitialClosed ( );
				}
			}
		}

#endregion


#region rewarded_emulation
		public bool IsRewardedAvailable ()
		{
#if EMULATE_REWARDED
			return IsInterstitialAdAvailable ( );
#else
			return false;
#endif
		}

		public bool ShowRewardedAd ()
		{
#if EMULATE_REWARDED
			bool res = false;
			if (interstitial_ready)
			{
				LogMessage ( "Show Rewarded" );
				MobFox.Instance.ShowMobFoxInterstitial ( );
				emulating_rewarded = res = true;
			}
			return res;
#else
			return false;
#endif
		}
#endregion



		/* ====== ADVERT EVENTS ====== */

#region Banner callback handlers

		private void HandleAdClicked ()
		{
			string msg = "Banner Ad clicked";
			LogMessage ( msg );
		}

		private void HandleAdClosed ()
		{
			LogMessage ( "Banner Ad closed" );
			BannerReadyChanged ( false );
		}

		private void HandleAdFailedToLoad (string msg)
		{
			LogMessage ( "Failed to receive Banner Ad with message: " + msg );
			BannerReadyChanged ( false );
		}

		private void HandleAdFinished ()
		{
			string msg = "Banner Ad finished";
			LogMessage ( msg );
		}

		private void HandleAdLoaded ()
		{
			LogMessage ( "Banner Ad loaded" );
			BannerReadyChanged ( true );
		}



		//private void HandleAdOpened (object sender, EventArgs args)
		//{
		//	LogMessage ( "Banner Ad opened" );
		//}

#endregion



#region Interstitial callback handlers

		private void HandleInterstitialFailedToLoad (string msg)
		{
			LogMessage ( "Interstitial failed to load with message: " + msg );
			InterstitialReadyChanged ( false );
		}

		private void HandleInterstitialClicked ()
		{
			string msg = "Banner Interstitial clicked";
			LogMessage ( msg );
		}


		private void HandleInterstitialClosed ()
		{
			LogMessage ( "Interstitial closed" );
			InterstitialClosed ( );
			InterstitialReadyChanged ( false );
		}

		private void HandleInterstitialLoaded ()
		{
			LogMessage ( "Interstitial loaded" );
			InterstitialReadyChanged ( true );
		}

		private void HandleInterstitialFinished ()
		{
			string msg = "Banner Interstitial finished";
			LogMessage ( msg );
		}

		private void HandleInterstitialOpened ()
		{
			LogMessage ( "Interstitial opened" );
			InterstitialShown ( );
		}

		//private void HandleInterstitialLeftApplication (object sender, EventArgs args)
		//{
		//	LogMessage ( "Interstitial left Application" );
		//}

#endregion



	}
}
#endif
