﻿#define NEATPLUG_PRESENT

#if NEATPLUG_PRESENT && !UNITY_IPHONE
using UnityEngine;
using System;
using System.Collections.Generic;
using PSV.ADS;

namespace PSV
{
    /// <summary>
    /// ChangeLog:
    /// - Load <ad> methods were refactored to avoid exceptions while loading ads (mediation adapters can cause it if some errors occur in their code). Now if error occurs ad will be requested again.
    /// </summary>
    public class NeatPlugProvider : IAdProvider
    {
        private AdmobAdAgent
            agent = null;

        private bool
            non_personalized =false,
            is_designed_for_families = false,
            tagged_for_children = true,
            ads_enabled = true,
            interstitial_ready = false,
            interstitial_loading = false,
            banner_ready = false,
            banner_loading = false;

        private float
            request_interval = 10,
            last_banner_request = 0,
            last_interstitial_request;

        protected AdSize
            ad_size = null;
        protected AdPosition
            ad_pos = AdPosition.Undefined;


        protected AdmobAd.BannerAdType
            banner_type = AdmobAd.BannerAdType.Universal_Banner_320x50; //use this only due to higher fill-rate
        protected AdmobAd.AdLayout
            banner_position = AdmobAd.AdLayout.Bottom_Centered; //default position

#pragma warning disable 0067
        public event Action<AdNetwork, bool>
            OnBannerEvent,
            OnNativeEvent,
            OnInterstitialEvent,
            OnRewardedEvent;
        public event Action<string, bool>
            OnMessageLogged;
        public event Action
            OnInterstitialShown,
            OnInterstitialClosed,
            OnBannerReadyChanged,
            OnNativeReadyChanged,
            OnRewardedShown,
            OnRewardedClosed,
            OnRewardedCompleted;
#pragma warning restore 0067

        protected AdNetwork
            net = AdNetwork.NeatPlug;

        protected string
            banner_param = ProviderParams.NEATPLUG_BANNER_ID,
            interstitial_param = ProviderParams.NEATPLUG_INTERSTITIAL_ID;

        private Coroutine
            delayed_load_banner,
            delayed_load_interstitial;

#region NotSupported

        public void ShowNativeAd( bool show )
        {
        }

        public bool IsNativeAdAvailable()
        {
            return false;
        }

        public void RefreshNativeAd( AdPosition ad_pos, AdSize ad_size )
        {

        }


        public bool IsRewardedAvailable()
        {
            return false;
        }

        public bool ShowRewardedAd()
        {
            return false;
        }
#endregion


#region General Methods

        public new string ToString()
        {
            return GetNetworkType().ToString();
        }

        private bool CanRequestAd( float last_time )
        {
            return Time.time - last_time >= request_interval;
        }

        protected void LogMessage( string message, bool error = false )
        {
            if (OnMessageLogged != null)
            {
                OnMessageLogged( this.GetType().ToString() + " " + message, error );
            }
        }

        public AdNetwork GetNetworkType()
        {
            return net;
        }

        public string[] GetNecessaryParams()
        {
            return new string[] { banner_param, interstitial_param };
        }

        virtual public void SetRequestInterval( float interval )
        {
            request_interval = interval;
        }

        public void Init( bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized, object settings )
        {
            this.agent = GameObject.FindObjectOfType<AdmobAdAgent>();
            if (this.agent == null)
            {
                LogMessage( "AdmobAdAgent not found - creating new instance" );
                GameObject agent = new GameObject();
                GameObject.DontDestroyOnLoad( agent );
                this.agent = agent.AddComponent<AdmobAdAgent>();
            }
            this.non_personalized = non_personalized;
            is_designed_for_families = for_families;
            tagged_for_children = children_tagged;
            this.ads_enabled = ads_enabled;
            LogMessage( "Initializing with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + tagged_for_children + ", is_designed_for_families=" + is_designed_for_families + ", non_personalized=" + non_personalized );
            SubscribeListeners();
            InitAdMob( settings );
            LoadBanner();
            LoadInterstitial();
        }

        private bool is_initialised = false;
        protected void InitAdMob( object settings ) //receiving array with banner and interstitial ids
        {
            Dictionary<string, string> _settings = settings as Dictionary<string, string>;
            LogMessage( "Processing params: " + _settings.ConvertToString() );
            string
                banner = "",
                interstitial = "";
            if (_settings != null &&
                _settings.TryGetValue( banner_param, out banner ) &&
                _settings.TryGetValue( interstitial_param, out interstitial ))
            {
                AdmobAd.Instance().Init( banner, interstitial );
                is_initialised = true;
            }
        }

        public void DisableAds()
        {
            ads_enabled = false;
            HideBanner();
        }

        public void EnableAds()
        {
            ads_enabled = true;
            LoadBanner();
            LoadInterstitial();
        }

        protected AdmobAd.AdLayout ConvertPosition( AdPosition pos )
        {
            return Utils.ToEnum<AdmobAd.AdLayout>( pos.ToString() );
        }

        protected AdmobAdAgent.AdLayout ConvertAgentPosition( AdPosition pos )
        {
            return Utils.ToEnum<AdmobAdAgent.AdLayout>( pos.ToString() );
        }


        protected void SubscribeListeners()
        {
            AdmobAdAgent.OnReceiveAd += OnReceiveAd;
            AdmobAdAgent.OnFailedToReceiveAd += OnFailedToReceiveAd;
            AdmobAdAgent.OnReceiveAdInterstitial += OnReceiveAdInterstitial;
            AdmobAdAgent.OnFailedToReceiveAdInterstitial += OnFailedToReceiveAdInterstitial;
            AdmobAdAgent.OnPresentScreenInterstitial += OnPresentScreenInterstitial;
            AdmobAdAgent.OnDismissScreenInterstitial += OnDismissScreenInterstitial;
            AdmobAdAgent.OnAdShown += OnAdShown;
            AdmobAdAgent.OnAdHidden += OnAdHidden;
            AdmobAdAgent.OnLeaveApplicationInterstitial += OnLeaveApplicationInterstitial;
            AdmobAdAgent.OnLeaveApplication += OnLeaveApplication;
        }

        protected void UnsubscribeListeners()
        {
            AdmobAdAgent.OnReceiveAd -= OnReceiveAd;
            AdmobAdAgent.OnFailedToReceiveAd -= OnFailedToReceiveAd;
            AdmobAdAgent.OnReceiveAdInterstitial -= OnReceiveAdInterstitial;
            AdmobAdAgent.OnFailedToReceiveAdInterstitial -= OnFailedToReceiveAdInterstitial;
            AdmobAdAgent.OnPresentScreenInterstitial -= OnPresentScreenInterstitial;
            AdmobAdAgent.OnDismissScreenInterstitial -= OnDismissScreenInterstitial;
            AdmobAdAgent.OnAdShown -= OnAdShown;
            AdmobAdAgent.OnAdHidden -= OnAdHidden;
            AdmobAdAgent.OnLeaveApplicationInterstitial -= OnLeaveApplicationInterstitial;
            AdmobAdAgent.OnLeaveApplication -= OnLeaveApplication;
        }

        protected delegate void LoadAd();

#endregion

#region Banner

        public bool IsBannerAdAvailable()
        {
            return banner_ready;
        }

        protected void LoadBanner( bool force = false )
        {
            if (ads_enabled && (force || !banner_loading && !banner_ready))
            {
                LogMessage( "Requesting banner is_initialised = " + is_initialised );
                banner_loading = true;
                last_banner_request = Time.time;

                Vector2 size = AdmobAd.Instance().GetAdSizeInPixels( banner_type );
                AdmobAd.TagForChildrenDirectedTreatment for_children = tagged_for_children ? AdmobAd.TagForChildrenDirectedTreatment.Yes : AdmobAd.TagForChildrenDirectedTreatment.Unset;
                Dictionary<string, string> extras = new Dictionary<string, string>();

                extras.Add( "is_designed_for_families", is_designed_for_families ? "true" : "false");
                extras.Add( "pma", non_personalized ? "1" : "0");

                try
                {
                    if (Application.platform == RuntimePlatform.Android)
                    {
                        AdmobAd.Instance().
                            LoadBannerAd(
                                            banner_type,
                                            banner_position,
                                            Vector2.zero,
                                            true,
                                            extras,
                                            for_children
                            );
                    }
                    else if (Application.platform == RuntimePlatform.IPhonePlayer)
                    {
                        AdmobAd.Instance().LoadBannerAd(
                            banner_type,
                            (int) (Screen.height * 0.5f - size.y * 0.5f),
                            (int) (Screen.width * 0.25f - size.x * 0.25f),
                            true,
                            extras,
                            for_children
                        );
                    }
                }
                catch (Exception e)
                {
                    LogMessage( "LoadBanner exception: " + e.Message, true );
                    BannerReadyChanged( false );
                }
            }
        }

        public void ShowBannerAd( bool show )
        {
            if (show)
            {
                ShowBanner();
            }
            else
            {
                HideBanner();
            }
        }

        public void RefreshBannerAd( AdPosition ad_pos, AdSize ad_size )
        {
            if (this.ad_size != ad_size)
            {
                this.ad_size = ad_size;
                banner_type = (AdmobAd.BannerAdType) ad_size;
                //if (agent != null)
                //{
                //	agent.autoLoadBannerAdType = (AdmobAdAgent.BannerAdType)ad_size;
                //}
                LoadBanner( true );
            }
            if (this.ad_pos != ad_pos)
            {
                this.ad_pos = ad_pos;
                banner_position = ConvertPosition( ad_pos );
                //if (agent != null)
                //{
                //    agent.autoLoadBannerAdLayout = ConvertAgentPosition ( ad_pos );
                //}
                LogMessage( "RepositionBannerAd is_initialised = " + is_initialised );
                AdmobAd.Instance().RepositionBannerAd( banner_position );
            }
        }

        protected void ShowBanner()
        {
            LogMessage( "ShowBanner: banner_ready - " + banner_ready + " is_initialised=" + is_initialised);
            if (banner_ready)
            {
                AdmobAd.Instance().ShowBannerAd();
            }
        }

        protected void HideBanner()
        {
            LogMessage( "HideBanner is_initialised=" + is_initialised );
            AdmobAd.Instance().HideBannerAd();
        }

        protected void BannerReadyChanged( bool ready )
        {
            bool last_state = banner_ready;
            banner_ready = ready;
            banner_loading = false;

            if (!banner_ready)
            {
                DelayedCallHandler.StopDelayedCall( delayed_load_banner );

                if (last_state == true || CanRequestAd( last_banner_request )) //if ad was loaded or last request was made some time before
                {
                    LoadBanner();
                }
                else
                {
                    // lower ad request frequency
                    delayed_load_banner = DelayedCallHandler.DelayedCall( () => LoadBanner(), request_interval );
                }
            }

            if (OnBannerEvent != null)
            {
                OnBannerEvent( GetNetworkType(), ready );
            }

            if (OnBannerReadyChanged != null)
            {
                OnBannerReadyChanged();
            }
        }

#endregion

#region Interstitial

        public bool IsInterstitialAdAvailable() //IAdProvider
        {
            return interstitial_ready;
        }


        void LoadInterstitial()
        {
            //LogMessage ( "Trying to request interstitial/nads_enabled=" + ads_enabled + "\ninterstitial_loading=" + interstitial_loading + "\ninterstitial_ready=" + interstitial_ready );

            if (/*ads_enabled && */!interstitial_loading && !interstitial_ready)
            {
                LogMessage( "Requesting interstitial is_initialised=" + is_initialised );
                interstitial_loading = true;
                last_interstitial_request = Time.time;

                AdmobAd.TagForChildrenDirectedTreatment for_children = tagged_for_children ? AdmobAd.TagForChildrenDirectedTreatment.Yes : AdmobAd.TagForChildrenDirectedTreatment.Unset;
                Dictionary<string, string> extras = is_designed_for_families ? new Dictionary<string, string>() { { "is_designed_for_families", "true" } } : null;
                try
                {
                    AdmobAd.Instance().LoadInterstitialAd(
                        true,
                        extras,
                        for_children
                    );
                }
                catch (Exception e)
                {
                    LogMessage( "LoadInterstitial exception: " + e.Message, true );
                    InterstitialReadyChanged( false );
                }
            }
        }

        public bool ShowInterstitialAd() //IAdProvider
        {
            if (ads_enabled && interstitial_ready)
            {
                LogMessage( "Show Interstitial is_initialised=" + is_initialised );
                AdmobAd.Instance().ShowInterstitialAd();
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void InterstitialReadyChanged( bool ready )
        {
            bool last_state = interstitial_ready;
            interstitial_loading = false;
            interstitial_ready = ready;
            if (!ready)
            {
                DelayedCallHandler.StopDelayedCall( delayed_load_interstitial );

                if (last_state == true || CanRequestAd( last_interstitial_request )) //if ad was loaded or last request was made some time before
                {
                    LoadInterstitial();
                }
                else
                {
                    // delay ad request frequency
                    delayed_load_interstitial = DelayedCallHandler.DelayedCall( () => LoadInterstitial(), request_interval );
                }
            }

            if (OnInterstitialEvent != null)
            {
                OnInterstitialEvent( GetNetworkType(), ready );
            }
        }

        private void InterstitialShown()
        {
            if (OnInterstitialShown != null)
            {
                OnInterstitialShown();
            }
        }

        private void InterstitialClosed()
        {
            if (OnInterstitialClosed != null)
            {
                OnInterstitialClosed();
            }
        }
#endregion

        //Ad callbacks holders

#region BANNER_EVENTS

        // Banner advert loaded and ready to be displayed
        void OnReceiveAd()
        {
            LogMessage( "OnReceiveAd() Fired." );
            BannerReadyChanged( true );
        }

        // Failed to receive banner advert
        void OnFailedToReceiveAd( string error )
        {
            LogMessage( "OnFailedToReceiveAd() Fired. Error: " + error, true );
            BannerReadyChanged( false );
        }

        // Banner advert is visible
        void OnAdShown()
        {
            LogMessage( "OnAdShown() Fired." );
        }

        // Banner advert is hidden
        void OnAdHidden()
        {
            LogMessage( "OnAdHidden() Fired." );
        }

        void OnLeaveApplication()
        {
            LogMessage( "OnLeaveApplication() Fired." );
            AnalyticsManager.LogEvent( AnalyticsEvents.BannerClicked );
        }

#endregion

#region INTERSTITIAL_EVENTS

        // Interstitial loaded and ready to be displayed
        void OnReceiveAdInterstitial()
        {
            LogMessage( "OnReceiveAdInterstitial() Fired." );
            InterstitialReadyChanged( true );
        }

        // Failed to receive interstitial advert (e.g no INTERNET connection)
        void OnFailedToReceiveAdInterstitial( string error )
        {
            LogMessage( "OnFailedToReceiveAdInterstitial() Fired. Error: " + error, true );
            InterstitialReadyChanged( false );
        }

        // When interstitial window opens
        void OnPresentScreenInterstitial()
        {
            LogMessage( "OnPresentScreenInterstitial() Fired." );
            InterstitialShown();
        }

        // When interstitial window is closed (Via hardware back button or clicking the X)
        void OnDismissScreenInterstitial()
        {
            LogMessage( "OnDismissScreenInterstitial() Fired." );
            InterstitialClosed();
            InterstitialReadyChanged( false );
        }

        // The player clicked an interstitial advert and the app has minimized
        void OnLeaveApplicationInterstitial()
        {
            LogMessage( "OnLeaveApplicationInterstitial() Fired." );
        }

#endregion

    }
}
#endif
