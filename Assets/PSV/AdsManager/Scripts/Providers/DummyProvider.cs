﻿
using System;
using UnityEngine;

namespace PSV.ADS
{
    public class DummyProvider : MonoBehaviour, IAdProvider
    {

        protected AdSize
            banner_size = AdSize.Standard_banner_320x50,
            native_size = AdSize.Native_banner_minimal_320x80;

        protected AdPosition
            banner_position = AdPosition.Bottom_Centered,
            native_position = AdPosition.Bottom_Centered;

        protected bool
            ads_enabled = true;



        private bool
            rewarded_visible = false,
            interstitial_visible = false,
            banner_visible = false,
            native_visible = false;


        private string[]
            necessary_params = new string[] {
            };

#pragma warning disable 0067
        public event Action<AdNetwork, bool>
            OnBannerEvent,
            OnNativeEvent,
            OnInterstitialEvent,
            OnRewardedEvent;
        public event Action<string, bool>
            OnMessageLogged;
        public event Action
            OnInterstitialShown,
            OnInterstitialClosed,
            OnBannerReadyChanged,
            OnNativeReadyChanged,
            OnRewardedShown,
            OnRewardedClosed,
            OnRewardedCompleted;
#pragma warning restore 0067

        private AdNetwork
            net = AdNetwork.HomeAds;


        #region Ads emulation


        public void OnGUI()
        {
            if (Application.isEditor)
            {
                if (banner_visible)
                {
                    AdUtils.DrawBannerRectGUI( banner_size, banner_position, label: "BANNER_AD " );
                }


                if (native_visible)
                {
                    AdUtils.DrawBannerRectGUI( native_size, native_position, label: "NATIVE_AD " );
                }

                if (interstitial_visible)
                {
                    if (GUI.Button( new Rect( 0, 0, Screen.width, Screen.height ), "Close\n\ninterstitial" ))
                    {
                        interstitial_visible = false;
                        InterstitialClosed();
                    }
                }

                if (rewarded_visible)
                {
                    if (GUI.Button( new Rect( 0, 0, Screen.width, Screen.height * 0.5f ), "Close\n\nrewarded" ))
                    {
                        rewarded_visible = false;
                        RewardedClosed();
                    }
                    if (GUI.Button( new Rect( 0, Screen.height * 0.5f, Screen.width, Screen.height * 0.5f ), "Complete\n\nrewarded" ))
                    {
                        rewarded_visible = false;
                        RewardedComplete();
                        RewardedClosed();
                    }
                }
            }
        }

        public static DummyProvider Create()
        {
            if (Application.isEditor)
            {
                GameObject go = new GameObject( "DummyProvider", typeof( DummyProvider ) );
                DontDestroyOnLoad( go );
                return go.GetComponent<DummyProvider>();
            }
            return null;
        }

        private void InterstitialClosed()
        {
            if (OnInterstitialClosed != null)
            {
                OnInterstitialClosed();
            }
        }
        private void RewardedComplete()
        {
            if (OnRewardedCompleted != null)
            {
                OnRewardedCompleted();
            }
        }
        private void RewardedClosed()
        {
            if (OnRewardedClosed != null)
            {
                OnRewardedClosed();
            }
        }

        #endregion

        #region General Methods

        public new string ToString()
        {
            return GetNetworkType().ToString();
        }

        protected void LogMessage( string message, bool error = false )
        {
            if (OnMessageLogged != null)
            {
                OnMessageLogged( this.GetType().ToString() + " " + message, error );
            }
        }

        public void DisableAds()
        {
            banner_visible = native_visible = ads_enabled = false;
            //rewarded stays to keep user receiving bonuses for watching ad
        }

        public void EnableAds()
        {
            ads_enabled = true;
            if (OnBannerReadyChanged != null)
            {
                OnBannerReadyChanged();
            }
            if (OnNativeReadyChanged != null)
            {
                OnNativeReadyChanged();
            }
        }

        virtual public AdNetwork GetNetworkType()
        {
            return net;
        }


        virtual public string[] GetNecessaryParams()
        {
            return necessary_params;
        }

        virtual public void SetRequestInterval( float interval )
        {

        }

        public void Init( bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized, object settings )
        {
            this.ads_enabled = ads_enabled;
            LogMessage( "Initializing with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + children_tagged + ", is_designed_for_families=" + for_families + ", non_personalized=" + non_personalized );
        }

        #endregion

        #region Banner

        public bool IsBannerAdAvailable()
        {
            return ads_enabled;
        }

        public void RefreshBannerAd( AdPosition ad_pos, AdSize ad_size )
        {
            banner_position = ad_pos;
            banner_size = ad_size;
        }

        public void ShowBannerAd( bool show )
        {
            if (show)
            {
                ShowBanner();
            }
            else
            {
                HideBanner();
            }
        }

        protected void ShowBanner()
        {
            if (banner_visible = ads_enabled)
            {
                LogMessage( "ShowBanner" );
            }
        }

        protected void HideBanner()
        {
            LogMessage( "HideBanner" );
            banner_visible = false;
        }

        #endregion

        #region Interstitial

        public bool IsInterstitialAdAvailable()
        {
            return ads_enabled;
        }

        public bool ShowInterstitialAd()
        {
            LogMessage( "ShowInterstitialAd" );
            InterstitialShown();
            return interstitial_visible = ads_enabled;
        }

        private void InterstitialShown()
        {
            if (OnInterstitialShown != null)
            {
                OnInterstitialShown();
            }
        }

        #endregion

        #region Rewarded

        public bool ShowRewardedAd()
        {
            LogMessage( "ShowRewardedAd" );
            RewardedShown();
            return rewarded_visible = true;
        }

        private void RewardedShown()
        {
            if (OnRewardedShown != null)
            {
                OnRewardedShown();
            }
        }
        #endregion

        #region Native

        public bool IsNativeAdAvailable()
        {
            return ads_enabled;
        }


        public void ShowNativeAd( bool show )
        {
            if (show)
            {
                ShowNativeAd();
            }
            else
            {
                HideNativeAd();
            }
        }

        public void RefreshNativeAd( AdPosition ad_pos, AdSize ad_size )
        {
            native_position = ad_pos;
            native_size = ad_size;
        }

        protected void ShowNativeAd()
        {
            if (native_visible = ads_enabled)
            {
                LogMessage( "ShowNativeAd" );
            }
        }

        protected void HideNativeAd()
        {
            LogMessage( "HideNativeAd" );
            native_visible = false;
        }



        #endregion

    }
}