﻿#define GOOGLE_ADS_PRESENT

#define GAD_3_10_0 //will enable better banner reposition method
#define GAD_3_11_0 //IOS additional initialization 
#define GAD_3_13_PLUS //will remove NativeAds support  (is deprecated in latest SDK versions)
//#define USE_VUNGLE_EXTRAS

#if GOOGLE_ADS_PRESENT
using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

namespace PSV.ADS
{
    /// <summary>
    /// ChangeLog:
    /// - Request methods were refactored to avoid exceptions while loading ads (mediation adapters can cause it if some errors occur in their code). Now if error occurs ad will be requested again.
    /// </summary>
    public class GoogleAdsProvider : IAdProvider
    {
        protected BannerView
            bannerView;
        protected InterstitialAd
            interstitial;
        protected RewardBasedVideoAd
            rewardBasedVideo;
#if !GAD_3_13_PLUS
		protected NativeExpressAdView
			nativeView;
#endif

        protected GoogleMobileAds.Api.AdSize
#if !GAD_3_13_PLUS
            native_size = new GoogleMobileAds.Api.AdSize( 320, 80 ), //minimum height = 80
#endif
            banner_size = GoogleMobileAds.Api.AdSize.Banner;

        protected GoogleMobileAds.Api.AdPosition
#if !GAD_3_13_PLUS
            native_position = GoogleMobileAds.Api.AdPosition.Bottom,
#endif
            banner_position = GoogleMobileAds.Api.AdPosition.Bottom;

        protected bool
            banner_ready = false,
            interstitial_ready = false,
            rewarded_ready = false,
#if !GAD_3_13_PLUS
            native_ready = false,
#endif
            ads_enabled = true;

#pragma warning disable 414
        private bool
            non_personalized = false,
            is_designed_for_families = false,
            tagged_for_children = true,
#if !GAD_3_13_PLUS
            native_loading = false,
            native_visible = false,
#endif
            rewarded_loading = false,
            interstitial_loading = false,
            banner_loading = false,
            banner_visible = false;
#pragma warning restore 414

        private string
            banner_id = "",
            interstitial_id = "",
#if !GAD_3_13_PLUS
            native_id = "",
#endif
            rewarded_id = "",
            vungle_placement_interstitial_id = "",
            vungle_placement_rewarded_id = "";

        private const string
#if USE_VUNGLE_EXTRAS
            vungle_placement_interstitial_param = ProviderParams.GOOGLEADS_VUNGLE_PLACEMENT_ID_INTERSTITIAL,
            vungle_placement_rewarded_param = ProviderParams.GOOGLEADS_VUNGLE_PLACEMENT_ID_REWARDED,
#endif
            banner_param = ProviderParams.GOOGLEADS_BANNER_ID,
            interstitial_param = ProviderParams.GOOGLEADS_INTERSTITIAL_ID,
#if !GAD_3_13_PLUS
            native_param = ProviderParams.GOOGLEADS_NATIVE_ID,
#endif
            rewarded_param = ProviderParams.GOOGLEADS_REWARDED_ID;

        private string[]
            necessary_params = new string[] {
                banner_param,
                interstitial_param,
#if !GAD_3_13_PLUS
                native_param,
#endif
                rewarded_param,
#if USE_VUNGLE_EXTRAS
                vungle_placement_interstitial_param,
                vungle_placement_rewarded_param,
#endif
            };

#pragma warning disable 0067
        public event Action<AdNetwork, bool>
            OnBannerEvent,
            OnNativeEvent,
            OnInterstitialEvent,
            OnRewardedEvent;
        public event Action<string, bool>
            OnMessageLogged;
        public event Action
            OnInterstitialShown,
            OnInterstitialClosed,
            OnBannerReadyChanged,
            OnNativeReadyChanged,
            OnRewardedShown,
            OnRewardedClosed,
            OnRewardedCompleted;
#pragma warning restore 0067

        private AdNetwork
            net = AdNetwork.GoogleAds;

        private float
            request_interval = 10,
            last_banner_request = 0,
            last_interstitial_request,
#if !GAD_3_13_PLUS
            last_native_request = 0,
#endif
            last_rewarded_request = 0;

        private Coroutine
            delayed_load_banner,
            delayed_load_interstitial,
#if !GAD_3_13_PLUS
            delayed_load_native,
#endif
            delayed_load_rewarded;


        #region General Methods

        public new string ToString()
        {
            return GetNetworkType().ToString();
        }


        private bool CanRequestAd( float last_time )
        {
            return Time.time - last_time >= request_interval;
        }


        protected void LogMessage( string message, bool error = false )
        {
            if (OnMessageLogged != null)
            {
                OnMessageLogged( this.GetType().ToString() + " " + message, error );
            }
        }

        public void DisableAds()
        {
            ads_enabled = false;
            DestroyBanner();
#if !GAD_3_13_PLUS
            DestroyNative();
#endif
            DestroyInterstitial();
            //rewarded stays active to keep user receiving bonuses for watching ad
        }

        public void EnableAds()
        {
            ads_enabled = true;
            LoadBanner();
#if !GAD_3_13_PLUS
            LoadNativeAd();
#endif
            LoadInterstitial();
        }

        virtual public AdNetwork GetNetworkType()
        {
            return net;
        }

        virtual protected string GetBannerParam()
        {
            return banner_param;
        }

        virtual protected string GetInterstitialParam()
        {
            return interstitial_param;
        }

#if !GAD_3_13_PLUS
        virtual protected string GetNativeParam()
        {
            return native_param;
        }
#endif

        private string GetRewardedParam()
        {
            return rewarded_param;
        }

#if USE_VUNGLE_EXTRAS
        virtual protected string GetVungleInterstitialParam()
        {
            return vungle_placement_interstitial_param;
        }

        private string GetVungleRewardedParam()
        {
            return vungle_placement_rewarded_param;
        }
#endif

        virtual public string[] GetNecessaryParams()
        {
            return necessary_params;
        }

        virtual public void SetRequestInterval( float interval )
        {
            request_interval = interval;
        }

        public void Init( bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized, object settings )
        {
#if GAD_3_11_0
            MobileAds.SetiOSAppPauseOnBackground( true );
#endif
            this.non_personalized = non_personalized;
            tagged_for_children = children_tagged;
            is_designed_for_families = for_families;
            this.ads_enabled = ads_enabled;
            LogMessage( "Initializing with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + tagged_for_children + ", is_designed_for_families=" + is_designed_for_families + ", non_personalized=" + non_personalized );
            ProcessParams( settings );
        }


        protected GoogleMobileAds.Api.AdPosition ConvertAdPosition( ADS.AdPosition pos )
        {
            GoogleMobileAds.Api.AdPosition res = GoogleMobileAds.Api.AdPosition.Bottom;

            switch (pos)
            {
                case ADS.AdPosition.Bottom_Centered:
                    res = GoogleMobileAds.Api.AdPosition.Bottom;
                    break;
                case ADS.AdPosition.Bottom_Left:
                    res = GoogleMobileAds.Api.AdPosition.BottomLeft;
                    break;
                case ADS.AdPosition.Bottom_Right:
                    res = GoogleMobileAds.Api.AdPosition.BottomRight;
                    break;
                case ADS.AdPosition.Middle_Centered:
                case ADS.AdPosition.Middle_Left:
                case ADS.AdPosition.Middle_Right:
                    res = GoogleMobileAds.Api.AdPosition.Center;
                    break;
                case ADS.AdPosition.Top_Centered:
                    res = GoogleMobileAds.Api.AdPosition.Top;
                    break;
                case ADS.AdPosition.Top_Left:
                    res = GoogleMobileAds.Api.AdPosition.TopLeft;
                    break;
                case ADS.AdPosition.Top_Right:
                    res = GoogleMobileAds.Api.AdPosition.TopRight;
                    break;
            }
            return res;
        }

        virtual protected void ProcessParams( object settings )
        {
            Dictionary<string, string> _settings = settings as Dictionary<string, string>;
            LogMessage( "Processing params: " + _settings.ConvertToString() );

            if (_settings != null)
            {
#if USE_VUNGLE_EXTRAS
                if (_settings.TryGetValue( GetVungleInterstitialParam(), out vungle_placement_interstitial_id ))
                {
                    LogMessage( "vungle_placement_interstitial_id initialized" );
                }

                if (_settings.TryGetValue( GetVungleRewardedParam(), out vungle_placement_rewarded_id ))
                {
                    LogMessage( "vungle_placement_rewarded_id initialized" );
                }
#endif
                if (_settings.TryGetValue( GetBannerParam(), out banner_id ))
                {
                    LogMessage( "banner initialized" );
                    LoadBanner( true );
                }

                if (_settings.TryGetValue( GetInterstitialParam(), out interstitial_id ))
                {
                    LogMessage( "interstitial initialized" );
                    LoadInterstitial( true );
                }

                if (_settings.TryGetValue( GetRewardedParam(), out rewarded_id ))
                {
                    LogMessage( "rewarded initialized" );
                    LoadRewardedVideoAd( true );
                }

#if !GAD_3_13_PLUS
                if (_settings.TryGetValue( GetNativeParam(), out native_id ))
                {
                    LogMessage( "native_ad initialized" );
                    LoadNativeAd( true );
                }
#endif

            }
        }

        virtual protected string GetBannerID()
        {
            return banner_id;
        }

        virtual protected string GetInterstitialID()
        {
            return interstitial_id;
        }

#if !GAD_3_13_PLUS
        virtual protected string GetNativeID()
        {
            return native_id;
        }
#endif

        virtual protected string GetRewardedID()
        {
            return rewarded_id;
        }

        virtual protected string GetVungleInterstitialID()
        {
            return vungle_placement_interstitial_id;
        }

        private string GetVungleRewardedID()
        {
            return vungle_placement_rewarded_id;
        }

        protected AdRequest.Builder createAdRequest()
        {
            AdRequest.Builder res = new AdRequest.Builder()
                //.AddTestDevice (AdRequest.TestDeviceSimulator)
                //.AddTestDevice ("0123456789ABCDEF0123456789ABCDEF")
                //.AddKeyword ("game")
                .AddExtra( "color_bg", "9B30FF" );

            if (tagged_for_children)
            {
                res = res.TagForChildDirectedTreatment( true );
            }
            if (is_designed_for_families)
            {
                res = res.AddExtra( "is_designed_for_families", "true" );
            }
            if (non_personalized)
            {
                res = res.AddExtra( "npa", "1" );
            }

            return res;
        }

        #endregion

        #region Banner

        protected void RequestBanner( bool forse )
        {
            if (!banner_loading || forse)
            {
                DestroyBanner();
                string id = GetBannerID();
                if (ads_enabled)
                {
                    if (!string.IsNullOrEmpty( id ))
                    {
                        LogMessage( "Requesting banner" );
                        last_banner_request = Time.time;
                        banner_loading = true;
                        bannerView = new BannerView( id, banner_size, banner_position );
                        // Register for ad events.
                        bannerView.OnAdLoaded += HandleAdLoaded;
                        bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
                        bannerView.OnAdLoaded += HandleAdOpened;
                        bannerView.OnAdClosed += HandleAdClosed;
                        bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
                        // Load a banner ad.
                        try
                        {
                            bannerView.LoadAd( createAdRequest().Build() );
                        }
                        catch (Exception e)
                        {
                            LogMessage( "RequestBanner exception: " + e.Message, true );
                            BannerReadyChanged( false );
                        }
                    }
                    else
                    {
                        OnBannerRequestError( id );
                    }
                }
            }
        }

        virtual protected void OnBannerRequestError( string id )
        {
            LogMessage( "BannerRequestError empty id", true );
        }

        protected void LoadBanner( bool forse = false )
        {
            RequestBanner( forse );
            HideBanner();
        }

        public bool IsBannerAdAvailable()
        {
            return banner_ready;
        }

        public void RefreshBannerAd( ADS.AdPosition ad_pos_psv, ADS.AdSize ad_size_psv )
        {
            bool need_to_refresh = false;
            GoogleMobileAds.Api.AdPosition new_pos = ConvertAdPosition( ad_pos_psv );
            if (banner_position != new_pos)
            {
                LogMessage( "Repositioning Ad" );

                banner_position = new_pos;
#if GAD_3_10_0
                if (bannerView != null)
                {
                    bannerView.SetPosition( banner_position );
                }
#else
                need_to_refresh = true;
#endif
            }
            GoogleMobileAds.Api.AdSize new_size = (GoogleMobileAds.Api.AdSize) ad_size_psv;
            if (banner_size.Width != new_size.Width || banner_size.Height != new_size.Height)
            {
                LogMessage( "Resizing Ad banner_size = " + banner_size.Width + ":" + banner_size.Height + " new_size = " + new_size.Width + ":" + new_size.Height );

                banner_size = new_size;
                need_to_refresh = true;
            }
            if (need_to_refresh)
            {
                BannerReadyChanged( false, true );
            }
        }

        public void ShowBannerAd( bool show )
        {
            if (show)
            {
                ShowBanner();
            }
            else
            {
                HideBanner();
            }
        }

        protected void ShowBanner()
        {
            banner_visible = true;
            if (bannerView != null)
            {
                bannerView.Show();
            }
        }

        protected void HideBanner()
        {
            banner_visible = false;
            if (bannerView != null)
            {
                bannerView.Hide();
            }
        }

        protected void DestroyBanner()
        {
            if (bannerView != null)
            {
                bannerView.Destroy();
            }
        }

        protected void BannerReadyChanged( bool ready, bool force_load = false )
        {
            bool last_state = banner_ready;
            banner_ready = ready;
            banner_loading = false;
            if (!ready)
            {
                DelayedCallHandler.StopDelayedCall( delayed_load_banner );

                if (force_load == true || last_state == true || CanRequestAd( last_banner_request )) //if ad was loaded or last request was made some time before
                {
                    LoadBanner();
                }
                else
                {
                    // delay ad request frequency
                    delayed_load_banner = DelayedCallHandler.DelayedCall( () => LoadBanner(), request_interval );
                }
            }

            if (OnBannerEvent != null)
            {
                OnBannerEvent( GetNetworkType(), ready );
            }
            if (OnBannerReadyChanged != null)
            {
                OnBannerReadyChanged();
            }
        }

        #endregion

        #region Interstitial

        public bool IsInterstitialAdAvailable()
        {
            return interstitial != null && interstitial_ready; /*interstitial.IsLoaded ( );*/
        }

        protected void RequestInterstitial( bool forse )
        {
            if (!interstitial_loading || forse)
            {
                DestroyInterstitial();
                // Create an interstitial.
                string id = GetInterstitialID();
                if (ads_enabled)
                {
                    if (!string.IsNullOrEmpty( id ))
                    {
                        LogMessage( "Requesting interstitial" );
                        last_interstitial_request = Time.time;
                        interstitial_loading = true;
                        interstitial = new InterstitialAd( id );
                        // Register listeners for ad events.
                        interstitial.OnAdLoaded += HandleInterstitialLoaded;
                        interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
                        interstitial.OnAdOpening += HandleInterstitialOpened;
                        interstitial.OnAdClosed += HandleInterstitialClosed;
                        interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;
                        // Load an interstitial ad.
                        AdRequest.Builder request = createAdRequest();
#if USE_VUNGLE_EXTRAS
                        GoogleMobileAds.Api.Mediation.Vungle.VungleInterstitialMediationExtras extras = new GoogleMobileAds.Api.Mediation.Vungle.VungleInterstitialMediationExtras();
                        extras.SetAllPlacements( new string[] { GetVungleInterstitialID() } );
                        request = request.AddMediationExtras( extras );
#endif
                        try
                        {
                            interstitial.LoadAd( request.Build() );
                        }
                        catch (Exception e)
                        {
                            LogMessage( "RequestInterstitial exception: " + e.Message, true );
                            InterstitialReadyChanged( false );
                        }
                    }
                    else
                    {
                        OnInterstitialRequestError( id );
                    }
                }
            }
        }

        virtual protected void OnInterstitialRequestError( string id )
        {
            LogMessage( "InterstitialRequestError empty id", true );
        }

        protected void LoadInterstitial( bool forse = false )
        {
            if (forse || !interstitial_loading)
            {
                RequestInterstitial( forse );
            }
        }

        public bool ShowInterstitialAd()
        {
            if (IsInterstitialAdAvailable())
            {
                interstitial.Show();
                return true;
            }
            return false;
        }

        protected void DestroyInterstitial()
        {
            if (interstitial != null)
            {
                interstitial.Destroy();
            }
        }

        protected void InterstitialReadyChanged( bool ready )
        {
            bool last_state = interstitial_ready;
            interstitial_ready = ready;
            interstitial_loading = false;

            if (!ready)
            {
                DelayedCallHandler.StopDelayedCall( delayed_load_interstitial );

                if (last_state == true || CanRequestAd( last_interstitial_request )) //if ad was loaded or last request was made some time before
                {
                    LoadInterstitial();
                }
                else
                {
                    // delay ad request frequency
                    delayed_load_interstitial = DelayedCallHandler.DelayedCall( () => LoadInterstitial(), request_interval );
                }
            }

            if (OnInterstitialEvent != null)
            {
                OnInterstitialEvent( GetNetworkType(), ready );
            }
        }

        private void InterstitialShown()
        {
            if (OnInterstitialShown != null)
            {
                OnInterstitialShown();
            }
        }

        private void InterstitialClosed()
        {
            if (OnInterstitialClosed != null)
            {
                OnInterstitialClosed();
            }
        }
        #endregion

        #region Rewarded

        protected void RequestRewardedAd( bool forse )
        {
            if (!rewarded_loading || forse)
            {
                if (rewardBasedVideo == null)
                {
                    rewardBasedVideo = RewardBasedVideoAd.Instance;
                    rewardBasedVideo.OnAdClosed += OnRewardedVideoAdClosed;
                    rewardBasedVideo.OnAdFailedToLoad += OnRewardedVideoAdFailedToLoad;
                    rewardBasedVideo.OnAdLoaded += OnRewardedVideoAdLoaded;
                    rewardBasedVideo.OnAdLeavingApplication += OnRewardedVideoAdLeavingApplication;
                    rewardBasedVideo.OnAdOpening += OnRewardedVideoAdOpening;
                    rewardBasedVideo.OnAdRewarded += OnRewardedVideoAdRewarded;
                    rewardBasedVideo.OnAdStarted += OnRewardedVideoAdStarted;
                }
                string id = GetRewardedID();
                if (!string.IsNullOrEmpty( id ))
                {
                    LogMessage( "Requesting rewarded" );
                    last_rewarded_request = Time.time;
                    rewarded_loading = true;
                    AdRequest.Builder request = createAdRequest();
#if USE_VUNGLE_EXTRAS
                    GoogleMobileAds.Api.Mediation.Vungle.VungleRewardedVideoMediationExtras extras = new GoogleMobileAds.Api.Mediation.Vungle.VungleRewardedVideoMediationExtras();
                    extras.SetAllPlacements( new string[] { GetVungleRewardedID() } );
                    request = request.AddMediationExtras( extras );
#endif
                    try
                    {
                        rewardBasedVideo.LoadAd( request.Build(), id );
                    }
                    catch (Exception e)
                    {
                        LogMessage( "RequestRewardedAd exception: " + e.Message, true );
                        RewardedReadyChanged( false );
                    }
                }
                else
                {
                    OnRewardedRequestError( id );
                }
            }
        }

        virtual protected void OnRewardedRequestError( string id )
        {
            LogMessage( "RewardedRequestError empty id", true );
        }

        protected void LoadRewardedVideoAd( bool forse = false )
        {
            RequestRewardedAd( forse );
        }


        public bool ShowRewardedAd()
        {
            if (rewardBasedVideo != null && rewarded_ready /*rewardBasedVideo.IsLoaded ( )*/)
            {
                rewardBasedVideo.Show();
                return true;
            }
            return false;
        }

        protected void RewardedReadyChanged( bool ready )
        {
            bool last_state = rewarded_ready;
            rewarded_ready = ready;
            rewarded_loading = false;

            if (!ready)
            {
                DelayedCallHandler.StopDelayedCall( delayed_load_rewarded );

                if (last_state == true || CanRequestAd( last_rewarded_request )) //if ad was loaded or last request was made some time before
                {
                    LoadRewardedVideoAd();
                }
                else
                {
                    // delay ad request frequency
                    delayed_load_rewarded = DelayedCallHandler.DelayedCall( () => LoadRewardedVideoAd(), request_interval );
                }
            }

            if (OnRewardedEvent != null)
            {
                OnRewardedEvent( GetNetworkType(), ready );
            }
        }

        private void RewardedShown()
        {
            if (OnRewardedShown != null)
            {
                OnRewardedShown();
            }
        }
        private void RewardedClosed()
        {
            if (OnRewardedClosed != null)
            {
                OnRewardedClosed();
            }
        }
        private void RewardedCompleted()
        {
            if (OnRewardedCompleted != null)
            {
                OnRewardedCompleted();
            }
        }

        #endregion

        #region Native

        public bool IsNativeAdAvailable()
        {
#if !GAD_3_13_PLUS
            return native_ready;
#else
            return false;
#endif
        }

        public void ShowNativeAd( bool show )
        {
#if !GAD_3_13_PLUS
            if (show)
            {
                ShowNativeAd();
            }
            else
            {
                HideNativeAd();
            }
#endif
        }

        public void RefreshNativeAd( ADS.AdPosition ad_pos_psv, ADS.AdSize ad_size_psv )
        {
#if !GAD_3_13_PLUS
			    native_position = ConvertAdPosition ( ad_pos_psv );
                native_size = (AdSize)ad_size_psv;
			    NativeReadyChanged ( false, true );
			    if (!native_visible)
				    HideNativeAd ( );
#endif
        }

        protected void LoadNativeAd( bool forse = false )
        {
#if !GAD_3_13_PLUS
            RequestNativeAd( forse );
            HideNativeAd();
#endif
        }


        protected void DestroyNative()
        {
#if !GAD_3_13_PLUS
            if (nativeView != null)
            {
                nativeView.Destroy();
            }
#endif
        }


#if !GAD_3_13_PLUS

		protected void RequestNativeAd (bool forse)
		{
			if (!native_loading || forse)
			{
				DestroyNative ( );

				string id = GetNativeID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						LogMessage ( "Requesting native ad" );
						last_native_request = Time.time;
						native_loading = true;
						nativeView = new NativeExpressAdView ( id, native_size, native_position );
						nativeView.OnAdLoaded += OnNativeAdLoaded;
						nativeView.OnAdFailedToLoad += OnNativeAdFailedToLoad;
						nativeView.OnAdClosed += OnNativeAdClosed;
						nativeView.OnAdLeavingApplication += OnNativeAdLeavingApplication;
						nativeView.OnAdOpening += OnNativeAdOpening;
						try
						{
							nativeView.LoadAd ( createAdRequest ( ) );
						}
						catch (Exception e)
						{
							LogMessage ( "RequestNativeAd exception: " + e.Message, true );
							NativeReadyChanged ( false );
						}
					}
					else
					{
						OnNativeRequestError ( id );
					}
				}
			}
		}

        virtual protected void OnNativeRequestError( string id )
        {
            LogMessage( "NativeRequestError empty id", true );
        }
        

		protected void ShowNativeAd ()
		{
            native_visible = true;
			if (ads_enabled && nativeView != null)
			{
				nativeView.Show ( );
			}
		}

		protected void HideNativeAd ()
		{
			native_visible = false;
			if (nativeView != null)
			{
				nativeView.Hide ( );
			}
		}

		protected void RefreshNativeAd ()
		{
			if (nativeView != null)
			{
				nativeView.LoadAd ( createAdRequest ( ) );
			}
		}

		protected void NativeReadyChanged (bool ready, bool force_load = false)
		{
			bool last_state = native_ready;
			native_ready = ready;
			native_loading = false;

			if (!ready)
			{
				DelayedCallHandler.StopDelayedCall ( delayed_load_native );

				if (force_load == true || last_state == true || CanRequestAd ( last_native_request )) //if ad was loaded or last request was made some time before
				{
					LoadNativeAd ( );
				}
				else
				{
					// delay ad request frequency
					delayed_load_native = DelayedCallHandler.DelayedCall ( () => LoadNativeAd ( ), request_interval );
				}
			}

			if (OnNativeEvent != null)
			{
				OnNativeEvent ( GetNetworkType ( ), ready );
			}
			if (OnNativeReadyChanged != null)
			{
				OnNativeReadyChanged ( );
			}
		}
#endif

        #endregion

        /* ====== ADVERT EVENTS ====== */

        #region Banner callback handlers

        protected void HandleAdLoaded( object sender, EventArgs args )
        {

            LogMessage( "Banner Ad loaded");
            BannerReadyChanged( true );
        }

        protected void HandleAdFailedToLoad( object sender, AdFailedToLoadEventArgs args )
        {
            LogMessage( "Failed to receive Banner Ad with message: " + args.Message, true );
            BannerReadyChanged( false );
        }

        protected void HandleAdOpened( object sender, EventArgs args )
        {
            LogMessage( "Banner Ad opened");
        }

        protected void HandleAdClosing( object sender, EventArgs args )
        {
            LogMessage( "Banner Ad closing" );
        }

        protected void HandleAdClosed( object sender, EventArgs args )
        {
            LogMessage( "Banner Ad closed" );
        }

        protected void HandleAdLeftApplication( object sender, EventArgs args )
        {
            LogMessage( "Banner Ad left application" );
        }

        #endregion

        #region Native callbacks holder

#if !GAD_3_13_PLUS

        protected void OnNativeAdFailedToLoad( object sender, AdFailedToLoadEventArgs e )
        {
            LogMessage( "Native Ad Failed To Load with message: " + args.Message, true);
            NativeReadyChanged( false );
        }

        protected void OnNativeAdLoaded( object sender, EventArgs e )
        {
            LogMessage( "Native Ad Loaded");
            NativeReadyChanged( true );
        }

        protected void OnNativeAdOpening( object sender, EventArgs e )
        {
            LogMessage( "Native Ad Opening");
        }

        protected void OnNativeAdLeavingApplication( object sender, EventArgs e )
        {
            LogMessage( "Native Ad Leaving Application" );
        }

        protected void OnNativeAdClosed( object sender, EventArgs e )
        {
            LogMessage( "Native Ad Closed" );
        }

#endif

        #endregion

        #region Interstitial callback handlers

        protected void HandleInterstitialClosed( object sender, EventArgs args )
        {
            LogMessage( "Interstitial closed" );
            InterstitialClosed();
            InterstitialReadyChanged( false );
        }


        protected void HandleInterstitialFailedToLoad( object sender, AdFailedToLoadEventArgs args )
        {
            LogMessage( "Interstitial failed to load with message: " + args.Message, true );
            InterstitialReadyChanged( false );
        }


        protected void HandleInterstitialLoaded( object sender, EventArgs args )
        {
            LogMessage( "Interstitial loaded" );
            InterstitialReadyChanged( true );
        }

        protected void HandleInterstitialOpened( object sender, EventArgs args )
        {
            LogMessage( "Interstitial opened" );
            InterstitialShown();
        }

        protected void HandleInterstitialClosing( object sender, EventArgs args )
        {
            LogMessage( "Interstitial Closing" );
        }



        protected void HandleInterstitialLeftApplication( object sender, EventArgs args )
        {
            LogMessage( "Interstitial left Application" );
        }

        #endregion

        #region Rewarded callback holders


        protected void OnRewardedVideoAdClosed( object sender, EventArgs e )
        {
            LogMessage( "Rewarded Video Ad Closed" );
            RewardedClosed();
            RewardedReadyChanged( false );
        }

        protected void OnRewardedVideoAdRewarded( object sender, Reward e )
        {
            LogMessage( "Rewarded Video Ad Rewarded" );
            RewardedCompleted();
        }

        protected void OnRewardedVideoAdFailedToLoad( object sender, AdFailedToLoadEventArgs e )
        {
            LogMessage( "Rewarded Video Ad Failed To Load: " + e.Message, true );
            RewardedReadyChanged( false );
        }

        protected void OnRewardedVideoAdLeavingApplication( object sender, EventArgs e )
        {
            LogMessage( "Rewarded Video Ad Leaving Application" );
        }

        protected void OnRewardedVideoAdLoaded( object sender, EventArgs e )
        {
            LogMessage( "Rewarded Video Ad Loaded" );
            RewardedReadyChanged( true );
        }

        protected void OnRewardedVideoAdOpening( object sender, EventArgs e )
        {
            LogMessage( "Rewarded Video Ad Opening" );
            RewardedShown();
        }

        protected void OnRewardedVideoAdStarted( object sender, EventArgs e )
        {
            LogMessage( "Rewarded Video Ad Started" );
        }

        #endregion


    }
}
#endif
