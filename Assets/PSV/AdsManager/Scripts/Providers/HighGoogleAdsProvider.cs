﻿using UnityEngine;
using System.Collections;

namespace PSV.ADS
{
	public class HighGoogleAdsProvider :GoogleAdsProvider
	{

        private const string
#if USE_VUNGLE_EXTRAS
            vungle_placement_interstitial_param = ProviderParams.HIGHGAD_VUNGLE_PLACEMENT_ID_INTERSTITIAL,
#endif
            banner_param = ProviderParams.HIGHGAD_BANNER_ID,
            interstitial_param = ProviderParams.HIGHGAD_INTERSTITIAL_ID;

        private string[]
			necessary_params = new string[] {
				banner_param,
				interstitial_param,
#if USE_VUNGLE_EXTRAS
                vungle_placement_interstitial_param,
#endif
        };

		private AdNetwork
			net = AdNetwork.HighGAD;

		public override AdNetwork GetNetworkType ()
		{
			return net;
		}

		protected override string GetBannerParam ()
		{
			return banner_param;
		}

		protected override string GetInterstitialParam ()
		{
			return interstitial_param;
		}

#if USE_VUNGLE_EXTRAS
        protected override string GetVungleInterstitialID()
        {
            return vungle_placement_interstitial_param;
        }
#endif

        public override string[] GetNecessaryParams ()
		{
			return necessary_params;
		}
	}


}