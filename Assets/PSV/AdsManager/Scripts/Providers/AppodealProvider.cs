﻿//#define APPODEAL_PRESENT

#if APPODEAL_PRESENT

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using AppodealAds.Unity.Common;
using AppodealAds.Unity.Api;

namespace PSV.ADS
{
    public class AppodealProvider : IAdProvider, IBannerAdListener, IInterstitialAdListener, IRewardedVideoAdListener
    {
        private bool
            auto_cache = true,
            testing = true,
            smart_banners = false,
            use_location = false,
            banner_animation = false,
            banner_background = false;

        protected bool
            ads_enabled = true,
            banner_ready = false,
            interstitial_ready = false,
            rewarded_ready = false,
            native_ready = false;

#pragma warning disable 414
        protected bool
            non_personalized = false,
            is_designed_for_families = false,
            tagged_for_children = true,
            native_loading = false,
            rewarded_loading = false,
            banner_loading = false,
            interstitial_loading = false,
            banner_visible = false,
            native_visible = false;
#pragma warning restore 414

        protected string
            appKey = "";

        virtual public event Action<AdNetwork, bool>
            OnBannerEvent,
            OnNativeEvent,
            OnInterstitialEvent,
            OnRewardedEvent;
        virtual public event Action<string>
            OnEventOccured;
        virtual public event Action<string, bool>
            OnMessageLogged;
        virtual public event Action
            OnInterstitialShown,
            OnInterstitialClosed,
            OnBannerReadyChanged,
            OnNativeReadyChanged,
            OnRewardedShown,
            OnRewardedClosed,
            OnRewardedCompleted;

        protected float
            request_interval = 10,
            last_banner_request = 0,
            last_interstitial_request,
            last_native_request = 0,
            last_rewarded_request = 0;

        private const string
            appKey_param = ProviderParams.APPODEAL_APPKEY;

        private string[]
            necessary_params = new string[] {
                appKey_param,
            };

        private AdNetwork
            net = AdNetwork.Appodeal;

        private BannerViewPos
            banner_pos = null;

        #region General Methods

        virtual public new string ToString()
        {
            return GetNetworkType().ToString();
        }


        virtual protected bool CanRequestAd( float last_time )
        {
            return Time.time - last_time >= request_interval;
        }


        virtual protected void LogMessage( string message, bool error = false )
        {
            if (OnMessageLogged != null)
            {
                OnMessageLogged( this.GetType().ToString() + " " + message, error );
            }
        }

        virtual protected void LogEvent( string message )
        {
            string msg = (GetNetworkType() + "_" + message).Replace( ' ', '_' );
            if (OnEventOccured != null)
            {
                OnEventOccured( msg );
            }
        }

        virtual public void DisableAds()
        {
            ads_enabled = false;
            throw new NotImplementedException();
        }

        virtual public void EnableAds()
        {
            ads_enabled = true;
            throw new NotImplementedException();
        }

        virtual public AdNetwork GetNetworkType()
        {
            return net;
        }

        virtual protected string GetAppKeyParam()
        {
            return appKey_param;
        }

        virtual public string[] GetNecessaryParams()
        {
            return necessary_params;
        }

        virtual public void SetRequestInterval( float interval )
        {
            request_interval = interval;
        }

        virtual public void Init( bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized, object settings )
        {
            this.non_personalized = non_personalized;
            tagged_for_children = children_tagged;
            is_designed_for_families = for_families;
            this.ads_enabled = ads_enabled;

            if (!use_location) { Appodeal.disableLocationPermissionCheck(); }

            Appodeal.setBannerAnimation( banner_animation );
            //Appodeal.setBannerBackground( banner_background );
            //Appodeal.setSmartBanners( smart_banners );
            Appodeal.setTesting( testing );
            Appodeal.setAutoCache( Appodeal.INTERSTITIAL | Appodeal.BANNER_VIEW | Appodeal.REWARDED_VIDEO, auto_cache );
            Appodeal.setLogLevel( testing ? Appodeal.LogLevel.Verbose : Appodeal.LogLevel.None );
            LogMessage( "Initializing with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + tagged_for_children + ", is_designed_for_families=" + is_designed_for_families );
            ProcessParams( settings );
        }


        virtual protected void ProcessParams( object settings )
        {
            Dictionary<string, string> _settings = settings as Dictionary<string, string>;
            LogMessage( "Processing params: " + _settings.ConvertToString() );

            if (_settings != null)
            {
                if (_settings.TryGetValue( GetAppKeyParam(), out appKey ))
                {
                    LogMessage( "banner initialized" );
                    Appodeal.setBannerCallbacks( this );
                    Appodeal.setInterstitialCallbacks( this );
                    Appodeal.setRewardedVideoCallbacks( this );
                    Appodeal.initialize( appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_VIEW | Appodeal.REWARDED_VIDEO );
                    BannerReadyChanged( false );
                    InterstitialReadyChanged( false );
                    RewardedReadyChanged( false );
                }
            }
        }

        virtual protected string GetAppKey()
        {
            return appKey;
        }

        #endregion

        #region Banner

        virtual protected void LoadBanner( bool forse = false )
        {
            LogMessage( "LoadBanner, force = " + forse + ", banner_loading = " + banner_loading );
            if (!banner_loading || forse)
            {
                if (ads_enabled)
                {
                    Appodeal.cache( Appodeal.BANNER_VIEW );
                }
            }
            HideBanner();
        }

        virtual public bool IsBannerAdAvailable()
        {
            return banner_ready;
        }

        virtual public void RefreshBannerAd( AdPosition ad_pos, AdSize ad_size )
        {
            banner_pos = new BannerViewPos( ad_pos );
            if (banner_visible)
            {
                ShowBanner();
            }
        }

        virtual public void ShowBannerAd( bool show )
        {
            if (show)
            {
                ShowBanner();
            }
            else
            {
                HideBanner();
            }
        }

        virtual protected void ShowBanner()
        {
            banner_visible = true;
            Appodeal.showBannerView( banner_pos.yPos, banner_pos.xPos, "default" );
        }

        virtual protected void HideBanner()
        {
            banner_visible = false;
            Appodeal.hideBannerView();
        }

        virtual protected void BannerReadyChanged( bool ready, bool force_load = false )
        {
            bool last_state = banner_ready;
            banner_ready = ready;
            banner_loading = false;
            if (OnBannerReadyChanged != null)
            {
                OnBannerReadyChanged();
            }
            if (OnBannerEvent != null)
            {
                OnBannerEvent( GetNetworkType(), ready );
            }
            if (!Appodeal.isPrecache( Appodeal.BANNER_VIEW ) && !ready)
            {
                if (force_load == true || last_state == true || CanRequestAd( last_banner_request )) //if ad was loaded or last request was made some time before
                {
                    LoadBanner();
                }
                else
                {
                    // delay ad request frequency
                    DelayedCallHandler.DelayedCall( () => LoadBanner(), request_interval );
                }
            }
        }

        #region Banner callback handlers
        public void onBannerLoaded( bool precache ) { LogMessage( "banner loaded" ); BannerReadyChanged( true ); }
        public void onBannerFailedToLoad() { LogMessage( "banner failed" ); BannerReadyChanged( false ); }
        public void onBannerShown() { LogMessage( "banner opened" ); }
        public void onBannerClicked() { LogMessage( "banner clicked" ); }
        #endregion

        #endregion

        #region Interstitial


        virtual protected void LoadInterstitial( bool forse = false )
        {
            if (!interstitial_loading || forse)
            {
                // Create an interstitial.
                if (ads_enabled)
                {
                    Appodeal.cache( Appodeal.INTERSTITIAL );
                }
            }
        }

        virtual public bool ShowInterstitialAd()
        {
            if (Appodeal.isLoaded( Appodeal.INTERSTITIAL ))
            {
                Appodeal.show( Appodeal.INTERSTITIAL );
                return true;
            }
            else
            {
                return false;
            }
        }

        virtual protected void InterstitialReadyChanged( bool ready )
        {
            bool last_state = interstitial_ready;
            interstitial_ready = ready;
            interstitial_loading = false;
            if (OnInterstitialEvent != null)
            {
                OnInterstitialEvent( GetNetworkType(), ready );
            }
            if (!Appodeal.isPrecache( Appodeal.INTERSTITIAL ) && !ready)
            {
                if (last_state == true || CanRequestAd( last_interstitial_request )) //if ad was loaded or last request was made some time before
                {
                    LoadInterstitial();
                }
                else
                {
                    // delay ad request frequency
                    DelayedCallHandler.DelayedCall( () => LoadInterstitial(), request_interval );
                }
            }
        }

        virtual protected void InterstitialShown()
        {
            if (OnInterstitialShown != null)
            {
                OnInterstitialShown();
            }
        }

        virtual protected void InterstitialClosed()
        {
            if (OnInterstitialClosed != null)
            {
                OnInterstitialClosed();
            }
        }

        #region Interstitial callback handlers
        public void onInterstitialLoaded( bool isPrecache ) { LogMessage( "Interstitial loaded" ); InterstitialReadyChanged( true ); }
        public void onInterstitialFailedToLoad() { LogMessage( "Interstitial failed" ); InterstitialReadyChanged( false ); }
        public void onInterstitialShown() { LogMessage( "Interstitial opened" ); InterstitialShown(); InterstitialReadyChanged( false ); }
        public void onInterstitialClosed() { LogMessage( "Interstitial closed" ); InterstitialClosed(); }
        public void onInterstitialClicked() { LogMessage( "Interstitial clicked" ); }
        #endregion

        #endregion

        #region Rewarded

        virtual protected void LoadRewardedVideoAd( bool forse = false )
        {
            if (!rewarded_loading || forse)
            {
                Appodeal.cache( Appodeal.REWARDED_VIDEO );
            }
        }


        virtual public bool ShowRewardedAd()
        {
            if (Appodeal.isLoaded( Appodeal.REWARDED_VIDEO ))
            {
                Appodeal.show( Appodeal.REWARDED_VIDEO );
                return true;
            }
            else
            {
                return false;
            }
        }

        virtual protected void RewardedReadyChanged( bool ready )
        {
            bool last_state = rewarded_ready;
            rewarded_ready = ready;
            rewarded_loading = false;

            if (OnRewardedEvent != null)
            {
                OnRewardedEvent( GetNetworkType(), ready );
            }
            if (!Appodeal.isPrecache( Appodeal.REWARDED_VIDEO ) && !ready)
            {
                if (last_state == true || CanRequestAd( last_rewarded_request )) //if ad was loaded or last request was made some time before
                {
                    LoadRewardedVideoAd();
                }
                else
                {
                    // delay ad request frequency
                    DelayedCallHandler.DelayedCall( () => LoadRewardedVideoAd(), request_interval );
                }
            }
        }

        virtual protected void RewardedShown()
        {
            if (OnRewardedShown != null)
            {
                OnRewardedShown();
            }
        }

        virtual protected void RewardedClosed()
        {
            if (OnRewardedClosed != null)
            {
                OnRewardedClosed();
            }
        }

        virtual protected void RewardedCompleted()
        {
            if (OnRewardedCompleted != null)
            {
                OnRewardedCompleted();
            }
        }

        #region Rewarded Video callback handlers
        //Appodeal-Unity-2.8.19-180418-all
        public void onRewardedVideoLoaded() { LogMessage( "Video loaded" ); RewardedReadyChanged( true ); }
        //Appodeal-Unity-2.8.42-220518-all
        public void onRewardedVideoLoaded( bool precache ) { LogMessage( "Video loaded" ); RewardedReadyChanged( true ); }
        public void onRewardedVideoFailedToLoad() { LogMessage( "Video failed" ); RewardedReadyChanged( false ); }
        public void onRewardedVideoShown() { LogMessage( "Video shown" ); RewardedShown(); RewardedReadyChanged( false ); }
        public void onRewardedVideoClosed( bool finished ) { LogMessage( "Video closed" ); RewardedClosed(); }
        //Appodeal-Unity-2.8.19-180418-all
        public void onRewardedVideoFinished( int amount, string name ) { LogMessage( "Reward: " + amount + " " + name ); RewardedCompleted(); }
        //Appodeal-Unity-2.8.42-220518-all
        public void onRewardedVideoFinished( double amount, string name ) { LogMessage( "Reward: " + amount + " " + name ); RewardedCompleted(); }
        #endregion

        #endregion

        #region Native

        virtual public bool IsNativeAdAvailable()
        {
            return native_ready;
        }


        virtual public void ShowNativeAd( bool show )
        {

        }

        virtual public void RefreshNativeAd( AdPosition ad_pos, AdSize ad_size )
        {

        }

        #endregion


        private class BannerViewPos
        {
            public int xPos = 0;
            public int yPos = 0;

            public override string ToString()
            {
                return "[ x = " + xPos + ", y = " + yPos + " ]";
            }

            public BannerViewPos( AdPosition pos )
            {
                switch (pos)
                {
                    case AdPosition.Bottom_Centered:
                    case AdPosition.Middle_Centered:
                        xPos = Appodeal.BANNER_HORIZONTAL_CENTER;
                        yPos = Appodeal.BANNER_BOTTOM;
                        break;
                    case AdPosition.Bottom_Left:
                    case AdPosition.Middle_Left:
                        xPos = Appodeal.BANNER_HORIZONTAL_LEFT;
                        yPos = Appodeal.BANNER_BOTTOM;
                        break;
                    case AdPosition.Bottom_Right:
                    case AdPosition.Middle_Right:
                        xPos = Appodeal.BANNER_HORIZONTAL_RIGHT;
                        yPos = Appodeal.BANNER_BOTTOM;
                        break;
                    case AdPosition.Top_Centered:
                        xPos = Appodeal.BANNER_HORIZONTAL_CENTER;
                        yPos = Appodeal.BANNER_TOP;
                        break;
                    case AdPosition.Top_Left:
                        xPos = Appodeal.BANNER_HORIZONTAL_LEFT;
                        yPos = Appodeal.BANNER_TOP;
                        break;
                    case AdPosition.Top_Right:
                        xPos = Appodeal.BANNER_HORIZONTAL_RIGHT;
                        yPos = Appodeal.BANNER_TOP;
                        break;
                }
            }
        }

    }
}
#endif