﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using PSV.ADS;

namespace PSV
{
	//use it as a sample for new providers
	public class AdProviderBase :IAdProvider
	{

		protected bool
			ads_enabled = true,
			banner_ready = false,
			interstitial_ready = false,
			rewarded_ready = false,
			native_ready = false;

#pragma warning disable 414
		protected bool
            non_personalized = false,
            is_designed_for_families = false,
			tagged_for_children = true,
			native_loading = false,
			rewarded_loading = false,
			banner_loading = false,
			interstitial_loading = false,
			banner_visible = false,
			native_visible = false;
#pragma warning restore 414

		protected string
			banner_id = "",
			interstitial_id = "",
			native_id = "",
			rewarded_id = "";

		virtual public event Action<AdNetwork, bool>
			OnBannerEvent,
			OnNativeEvent,
			OnInterstitialEvent,
			OnRewardedEvent;
		virtual public event Action<string>
			OnEventOccured;
		virtual public event Action<string, bool>
			OnMessageLogged;
		virtual public event Action
			OnInterstitialShown,
			OnInterstitialClosed,
			OnBannerReadyChanged,
			OnNativeReadyChanged,
			OnRewardedShown,
			OnRewardedClosed,
			OnRewardedCompleted;

		protected float
			request_interval = 10,
			last_banner_request = 0,
			last_interstitial_request,
			last_native_request = 0,
			last_rewarded_request = 0;

		#region General Methods

		virtual public new string ToString ()
		{
			return GetNetworkType ( ).ToString ( );
		}


		virtual protected bool CanRequestAd (float last_time)
		{
			return Time.time - last_time >= request_interval;
		}


		virtual protected void LogMessage (string message, bool error = false)
		{
			if (OnMessageLogged != null)
			{
				OnMessageLogged ( this.GetType ( ).ToString ( ) + " " + message, error );
			}
		}

		virtual protected void LogEvent (string message)
		{
			string msg = (GetNetworkType ( ) + "_" + message).Replace ( ' ', '_' );
			if (OnEventOccured != null)
			{
				OnEventOccured ( msg );
			}
		}

		virtual public void DisableAds ()
		{
			ads_enabled = false;
			throw new NotImplementedException ( );
		}

		virtual public void EnableAds ()
		{
			ads_enabled = true;
			throw new NotImplementedException ( );
		}

		virtual public AdNetwork GetNetworkType ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected string GetBannerParam ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected string GetInterstitialParam ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected string GetNativeParam ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected string GetRewardedParam ()
		{
			throw new NotImplementedException ( );
		}

		virtual public string[] GetNecessaryParams ()
		{
			throw new NotImplementedException ( );
		}

		virtual public void SetRequestInterval (float interval)
		{
			request_interval = interval;
		}

		virtual public void Init (bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized, object settings)
		{
            this.non_personalized = non_personalized;
            tagged_for_children = children_tagged;
			is_designed_for_families = for_families;

			this.ads_enabled = ads_enabled;
			LogMessage ( "Initializing with ads_enabled=" + this.ads_enabled + ", tagged_for_children=" + tagged_for_children + ", is_designed_for_families=" + is_designed_for_families );
			ProcessParams ( settings );
		}


		virtual protected void ProcessParams (object settings)
		{
			Dictionary<string, string> _settings = settings as Dictionary<string, string>;
			LogMessage ( "Processing params: " + _settings.ConvertToString ( ) );

			if (_settings != null)
			{
				if (_settings.TryGetValue ( GetBannerParam ( ), out banner_id ))
				{
					LogMessage ( "banner initialized" );
					LoadBanner ( true );
				}

				if (_settings.TryGetValue ( GetInterstitialParam ( ), out interstitial_id ))
				{
					LogMessage ( "interstitial initialized" );
					LoadInterstitial ( true );
				}

				//optional
				if (_settings.TryGetValue ( GetRewardedParam ( ), out rewarded_id ))
				{
					LogMessage ( "rewarded initialized" );
					LoadRewardedVideoAd ( true );
				}

				if (_settings.TryGetValue ( GetNativeParam ( ), out native_id ))
				{
					LogMessage ( "native_ad initialized" );
					LoadNativeAd ( true );
				}
			}
		}

		virtual protected string GetBannerID ()
		{
			return banner_id;
		}

		virtual protected string GetInterstitialID ()
		{
			return interstitial_id;
		}

		virtual protected string GetNativeID ()
		{
			return native_id;
		}


		virtual protected string GetRewardedID ()
		{
			return rewarded_id;
		}

		#endregion

		#region Banner

		virtual protected void RequestBanner (string id, bool forse)
		{
			throw new NotImplementedException ( );
		}

		virtual protected void LoadBanner (bool forse = false)
		{
			LogMessage ( "LoadBanner, force = "+ forse + ", banner_loading = " + banner_loading );
			if (!banner_loading || forse)
			{
				string id = GetBannerID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						RequestBanner ( id, forse );
					}
					else
					{
						LogMessage ( "BannerRequestError empty id", true );
					}
				}
			}
			HideBanner ( );
		}

		virtual public bool IsBannerAdAvailable ()
		{
			return banner_ready;
		}

		virtual public void RefreshBannerAd (AdPosition ad_pos, AdSize ad_size)
		{
			throw new NotImplementedException ( );
		}

		virtual public void ShowBannerAd (bool show)
		{
			if (show)
			{
				ShowBanner ( );
			}
			else
			{
				HideBanner ( );
			}
		}

		virtual protected void ShowBanner ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected void HideBanner ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected void BannerReadyChanged (bool ready, bool force_load = false)
		{
			bool last_state = banner_ready;
			banner_ready = ready;
			banner_loading = false;
			if (OnBannerReadyChanged != null)
			{
				OnBannerReadyChanged ( );
			}
			if (OnBannerEvent != null)
			{
				OnBannerEvent ( GetNetworkType ( ), ready );
			}
			if (!ready)
			{
				if (force_load == true || last_state == true || CanRequestAd ( last_banner_request )) //if ad was loaded or last request was made some time before
				{
					LoadBanner ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.DelayedCall ( () => LoadBanner ( ), request_interval );
				}
			}
		}

		#endregion

		#region Interstitial


		virtual protected void RequestInterstitial (string id, bool forse)
		{
			throw new NotImplementedException ( );
		}


		virtual protected void LoadInterstitial (bool forse = false)
		{
			if (!interstitial_loading || forse)
			{
				// Create an interstitial.
				string id = GetInterstitialID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						RequestInterstitial ( id, forse );
					}
					else
					{
						LogMessage ( "InterstitialRequestError empty id", true );
					}
				}
			}
		}

		virtual public bool ShowInterstitialAd ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected void InterstitialReadyChanged (bool ready)
		{
			bool last_state = interstitial_ready;
			interstitial_ready = ready;
			interstitial_loading = false;
			if (OnInterstitialEvent != null)
			{
				OnInterstitialEvent ( GetNetworkType ( ), ready );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_interstitial_request )) //if ad was loaded or last request was made some time before
				{
					LoadInterstitial ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.DelayedCall ( () => LoadInterstitial ( ), request_interval );
				}
			}
		}

		virtual protected void InterstitialShown ()
		{
			if (OnInterstitialShown != null)
			{
				OnInterstitialShown ( );
			}
		}

		virtual protected void InterstitialClosed ()
		{
			if (OnInterstitialClosed != null)
			{
				OnInterstitialClosed ( );
			}
		}
		#endregion

		#region Rewarded

		virtual protected void RequestRewardedAd (string id, bool forse)
		{
			throw new NotImplementedException ( );
		}

		virtual protected void LoadRewardedVideoAd (bool forse = false)
		{
			if (!rewarded_loading || forse)
			{
				// Create an interstitial.
				string id = GetRewardedID ( );
				if (!string.IsNullOrEmpty ( id ))
				{
					RequestRewardedAd ( id, forse );
				}
				else
				{
					LogMessage ( "RewardedRequestError empty id", true );
				}
			}
		}


		virtual public bool ShowRewardedAd ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected void RewardedReadyChanged (bool ready)
		{
			bool last_state = rewarded_ready;
			rewarded_ready = ready;
			rewarded_loading = false;

			if (OnRewardedEvent != null)
			{
				OnRewardedEvent ( GetNetworkType ( ), ready );
			}
			if (!ready)
			{
				if (last_state == true || CanRequestAd ( last_rewarded_request )) //if ad was loaded or last request was made some time before
				{
					LoadRewardedVideoAd ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.DelayedCall ( () => LoadRewardedVideoAd ( ), request_interval );
				}
			}
		}

		virtual protected void RewardedShown ()
		{
			if (OnRewardedShown != null)
			{
				OnRewardedShown ( );
			}
		}

		virtual protected void RewardedClosed ()
		{
			if (OnRewardedClosed != null)
			{
				OnRewardedClosed ( );
			}
		}

		virtual protected void RewardedCompleted ()
		{
			if (OnRewardedCompleted != null)
			{
				OnRewardedCompleted ( );
			}
		}

		#endregion

		#region Native

		virtual public bool IsNativeAdAvailable ()
		{
			return native_ready;
		}

		virtual protected void RequestNativeAd (string id, bool forse)
		{
			throw new NotImplementedException ( );
		}

		virtual protected void OnNativeRequestError (string id)
		{
			LogMessage ( "NativeRequestError empty id", true );
		}

		virtual protected void LoadNativeAd (bool forse = false)
		{
			if (!native_loading || forse)
			{
				string id = GetNativeID ( );
				if (ads_enabled)
				{
					if (!string.IsNullOrEmpty ( id ))
					{
						RequestNativeAd ( id, forse );
					}
					else
					{
						OnNativeRequestError ( id );
					}
				}
			}

			HideNativeAd ( );
		}

		virtual public void ShowNativeAd (bool show)
		{
			if (show)
			{
				ShowNativeAd ( );
			}
			else
			{
				HideNativeAd ( );
			}
		}

		virtual public void RefreshNativeAd (AdPosition ad_pos, AdSize ad_size)
		{
			NativeReadyChanged ( false, true );
			if (!native_visible)
				HideNativeAd ( );
		}


		virtual protected void ShowNativeAd ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected void HideNativeAd ()
		{
			throw new NotImplementedException ( );
		}

		virtual protected void NativeReadyChanged (bool ready, bool force_load = false)
		{
			bool last_state = native_ready;
			native_ready = ready;
			native_loading = false;
			if (OnNativeEvent != null)
			{
				OnNativeEvent ( GetNetworkType ( ), ready );
			}
			if (OnNativeReadyChanged != null)
			{
				OnNativeReadyChanged ( );
			}
			if (!ready)
			{
				if (force_load == true || last_state == true || CanRequestAd ( last_native_request )) //if ad was loaded or last request was made some time before
				{
					LoadNativeAd ( );
				}
				else
				{
					// delay ad request frequency
					DelayedCallHandler.DelayedCall ( () => LoadNativeAd ( ), request_interval );

                }
			}
		}

		#endregion


	}
}