#define NEATPLUG_PRESENT
#define GOOGLE_ADS_PRESENT
//#define KIDOZ_PRESENT
//#define MOBFOX_PRESENT
//#define APPODEAL_PRESENT

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using PSV.ADS;

namespace PSV
{

    public static class AdsManager
    {
        public static Action<string> OnMessageLogged;

        private static List<IAdProvider>
            ad_providers;

        private static int
            home_ads_index = -1;

        private const AdNetwork
            HOME_NETWORK = AdNetwork.HomeAds;

        public const float
            REQUEST_INTERVAL = 10f;
        private static bool
            banner_use_home_ads = false,
            native_use_home_ads = false,
            banner_visible = false,
            native_visible = false;

        private enum AdType
        {
            Banner,
            Interstitial,
            Rewarded,
            Native,
        }

        [AwakeStatic]
        public static void AwakeStatic()
        {
            AwakeStaticAttribute.Done( typeof( AdsManager ) );
            Initialize();
            SubscribeListeners();
            //notifying ManagerGoogle that we are ready to start
            AdsInterop.AdsReady();
        }
        

#pragma warning disable 162
        //change it when providers added
        private static IAdProvider GetProvider( AdNetwork net )
        {
            IAdProvider res = null;
            switch (net)
            {
#if NEATPLUG_PRESENT && !UNITY_IPHONE
                case AdNetwork.NeatPlug:
                    res = new NeatPlugProvider();
                    break;
#endif
#if GOOGLE_ADS_PRESENT
                case AdNetwork.GoogleAds:
                    res = new GoogleAdsProvider();
                    break;
                case AdNetwork.HomeAds:
                    res = new HomeAdsProvider();
                    break;
                case AdNetwork.HighGAD:
                    res = new HighGoogleAdsProvider();
                    break;
                case AdNetwork.MidGAD:
                    res = new MidGoogleAdsProvider();
                    break;

#endif
#if APPODEAL_PRESENT
                case AdNetwork.Appodeal:
                    res = new AppodealProvider();
                    break;
#endif
#if MOBFOX_PRESENT
				case AdNetwork.MobFox:
					res = new MobFoxAdsProvider ( );
					break;
#endif
#if KIDOZ_PRESENT
				case AdNetwork.Kidoz:
					res = new KidozProvider ( );
					break;
#endif
            }
            return res;
        }
#pragma warning restore 162

        private static void Initialize()
        {
            AdsSettings.Init(); //get saved params for ad providers
            SetProviders( AdsSettings.GetProvidersList() ); //set a list of available providers in given order
        }


        private static void SubscribeListeners()
        {
            AdsInterop.OnShowBannerAd += ShowBanner;
            AdsInterop.OnHideBannerAd += HideBanner;
            AdsInterop.OnShowNativeAd += ShowNativeAd;
            AdsInterop.OnHideNativeAd += HideNativeAd;
            AdsInterop.OnInitialiseAds += InitAds;
            AdsInterop.OnShowInterstitialAd += ShowInterstitial;
            AdsInterop.OnShowRewarded += ShowRewarded;
            AdsInterop.OnRefreshBannerAd += RefreshBannerAd;
            AdsInterop.OnRefreshNativeAd += RefreshNativeAd;
            AdsInterop.OnDisableAds += DisableAds;
            AdsInterop.OnEnableAds += EnableAds;
        }

        /*
        private static void UnsubscribeListeners()
        {
            AdsInterop.OnShowBannerAd -= ShowBanner;
            AdsInterop.OnHideBannerAd -= HideBanner;
            AdsInterop.OnShowNativeAd -= ShowNativeAd;
            AdsInterop.OnHideNativeAd -= HideNativeAd;
            AdsInterop.OnInitialiseAds -= InitAds;
            AdsInterop.OnShowInterstitialAd -= ShowInterstitial;
            AdsInterop.OnShowRewarded -= ShowRewarded;
            AdsInterop.OnRefreshBannerAd -= RefreshBannerAd;
            AdsInterop.OnRefreshNativeAd -= RefreshNativeAd;
            AdsInterop.OnDisableAds -= DisableAds;
            AdsInterop.OnEnableAds -= EnableAds;
        }
        */

        #region Banner

        private static void ShowBanner( bool use_home_ads )
        {
            LogMessage( "ShowBanner: use_home_ads " + use_home_ads );
            banner_visible = true;
            banner_use_home_ads = use_home_ads;
            UpdateBannerAds();
        }

        private static void HideBanner()
        {
            banner_visible = false;
            UpdateBannerAds();
        }

        private static void UpdateBannerAds()
        {
            LogMessage( "UpdateBannerAds, banner_visible = " + banner_visible + ", banner_use_home_ads = " + banner_use_home_ads );
            int provider_selected = -1;
            if (banner_visible && banner_use_home_ads)
            {
                //set HomeAdsovider as first candidate to show banner ad
                IAdProvider home_provider = GetHomeAdsProvider();
                if (home_provider != null && home_provider.IsBannerAdAvailable())
                {
                    provider_selected = home_ads_index;
                }
            }
            for (int i = 0; i < ad_providers.Count; i++)
            {
                if (banner_visible && provider_selected < 0 && ad_providers[i].IsBannerAdAvailable()) //check if any provider showed banner and if no - check whether current provider has banner
                {
                    provider_selected = i;
                }
                ad_providers[i].ShowBannerAd( banner_visible && i == provider_selected ); //showing providers ad if home_ads weren't shown and if it is a provider that cached ads
            }
        }

        private static void RefreshBannerAd( AdPosition ad_pos, AdSize ad_size )
        {
            for (int i = 0; i < ad_providers.Count; i++)
            {
                ad_providers[i].RefreshBannerAd( ad_pos, ad_size );
            }
            UpdateBannerAds();
        }

        #endregion

        #region Native

        private static void ShowNativeAd( bool use_home_ads )
        {
            LogMessage( "ShowNativeAd: use_home_ads " + use_home_ads );
            native_visible = true;
            native_use_home_ads = use_home_ads;
            UpdateNativeAds();
        }

        private static void HideNativeAd()
        {
            native_visible = false;
            UpdateNativeAds();
        }

        private static void UpdateNativeAds()
        {
            bool home_shown = false;
            int provider_selected = -1;
            if (banner_visible && native_use_home_ads)
            {
                //set HomeAdsProvider as first candidate to show native ad
                IAdProvider home_provider = GetHomeAdsProvider();
                if (home_provider != null && home_provider.IsNativeAdAvailable())
                {
                    provider_selected = home_ads_index;
                }
            }
            for (int i = 0; i < ad_providers.Count; i++)
            {
                if (native_visible && provider_selected < 0 && ad_providers[i].IsNativeAdAvailable()) //check if any provider showed banner and if no - check whether current provider has banner
                {
                    provider_selected = i;
                }
                ad_providers[i].ShowNativeAd( !home_shown && native_visible && i == provider_selected );
            }
        }

        private static void RefreshNativeAd( AdPosition ad_pos, AdSize ad_size )
        {
            for (int i = 0; i < ad_providers.Count; i++)
            {
                ad_providers[i].RefreshNativeAd( ad_pos, ad_size );
            }
            UpdateNativeAds();
        }

        #endregion

        #region Interstitial

        private static void ShowInterstitial( bool use_home_ads = false )
        {
            LogMessage( "ShowInterstitial: use_home_ads " + use_home_ads );
            //selecting home ads first and then main_providers in given order
            if (use_home_ads)
            {
                IAdProvider home_provider = GetHomeAdsProvider();
                if (home_provider != null && home_provider.ShowInterstitialAd())
                {
                    return;
                }
            }
            for (int i = 0; i < ad_providers.Count; i++)
            {
                if (ad_providers[i].ShowInterstitialAd())
                {
                    return;
                }
            }
            OnInterClosed();
        }

        #endregion

        #region Rewarded

        private static void ShowRewarded( bool use_home_ads = false )
        {
            LogMessage( "ShowRewarded: use_home_ads " + use_home_ads );

            //selecting home ads first and then main_providers in given order
            if (use_home_ads)
            {
                IAdProvider home_provider = GetHomeAdsProvider();
                if (home_provider != null && home_provider.ShowRewardedAd())
                {
                    return;
                }
            }
            for (int i = 0; i < ad_providers.Count; i++)
            {
                if (ad_providers[i].ShowRewardedAd())
                {
                    return;
                }
            }
            OnRewardedClosed();
        }

        #endregion

        #region General methods

        public static void InitAds( bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized )
        {
            bool home_ads_initialised = false;

            LogMessage( "Initializing providers count " + ad_providers.Count );

            //init providers with their ids
            for (int i = 0; i < ad_providers.Count; i++)
            {
                InitialiseProvider( ad_providers[i], ads_enabled, children_tagged, for_families, non_personalized );
                if (!home_ads_initialised && ad_providers[i].GetNetworkType() == HOME_NETWORK)
                {
                    home_ads_initialised = true;
                }
            }
        }


        private static void InitialiseProvider( IAdProvider provider, bool ads_enabled, bool children_tagged, bool for_families , bool non_personalized )
        {
            SubscribeProvider( provider );
            provider.SetRequestInterval( REQUEST_INTERVAL );
            provider.Init( ads_enabled, children_tagged, for_families, non_personalized, AdsSettings.GetSettings( provider.GetNecessaryParams() ) );
        }


        private static void SubscribeProvider( IAdProvider provider )
        {
            provider.OnInterstitialClosed += OnInterClosed;
            provider.OnBannerReadyChanged += BannerReadyChanged;
            provider.OnNativeReadyChanged += NativeReadyChanged;
            provider.OnRewardedClosed += OnRewardedClosed;
            provider.OnRewardedCompleted += OnRewardedCompleted;
            provider.OnInterstitialShown += InterstitialShown;
            provider.OnRewardedShown += RewardedShown;
            provider.OnBannerEvent += SendBannerAvailable;
            provider.OnInterstitialEvent += SendInterstitialAvailable;
            provider.OnNativeEvent += SendNativeAvailable;
            provider.OnRewardedEvent += SendRewardedAvailable;
            provider.OnMessageLogged += LogMessage;
        }

        /*
        private static void UnsubscribeProvider( IAdProvider provider )
        {
            provider.OnInterstitialClosed -= OnInterClosed;
            provider.OnBannerReadyChanged -= BannerReadyChanged;
            provider.OnNativeReadyChanged -= NativeReadyChanged;
            provider.OnRewardedClosed -= OnRewardedClosed;
            provider.OnRewardedCompleted -= OnRewardedCompleted;
            provider.OnInterstitialShown -= InterstitialShown;
            provider.OnRewardedShown -= RewardedShown;
            provider.OnBannerEvent -= SendBannerAvailable;
            provider.OnInterstitialEvent -= SendInterstitialAvailable;
            provider.OnNativeEvent -= SendNativeAvailable;
            provider.OnRewardedEvent -= SendRewardedAvailable;
            provider.OnMessageLogged -= LogMessage;
        }
        */

        public static void SetProviders( AdNetwork[] providers )
        {
            int _home_ads_index = -1;

            //set main_providers array
            List<IAdProvider> i_providers = new List<IAdProvider>();
            if (Application.isEditor)
            {
                if (providers.Length > 0)
                {
                    i_providers.Add( DummyProvider.Create());
                }
            }
            else
            {
                for (int i = 0; i < providers.Length; i++)
                {
                    AdNetwork net = providers[i];
                    if (i_providers.FindIndex( X => X.GetNetworkType() == net ) < 0)
                    {
                        //add unique providers only
                        IAdProvider i_prov = GetProvider( net );

                        if (i_prov != null)
                        {
                            i_providers.Add( i_prov );
                            if (net == HOME_NETWORK)
                            {
                                //save home_ads index in providers array
                                _home_ads_index = i_providers.Count - 1;
                            }
                        }
                    }
                }
            }
            home_ads_index = _home_ads_index; //-1 will tell us that home_ads are not present in main queue
            ad_providers = i_providers;
        }

        private static IAdProvider GetHomeAdsProvider()
        {
            IAdProvider res = null;
            if (home_ads_index >= 0 && home_ads_index < ad_providers.Count)
            {
                res = ad_providers[home_ads_index];
            }
            return res;
        }


        static bool HasProvider( AdNetwork net )
        {
            return ad_providers.FindIndex( X => X.GetNetworkType() == net ) >= 0;
        }

        private static void DisableAds()
        {
            for (int i = 0; i < ad_providers.Count; i++)
            {
                ad_providers[i].DisableAds();
            }
        }

        private static void EnableAds()
        {
            for (int i = 0; i < ad_providers.Count; i++)
            {
                ad_providers[i].EnableAds();
            }
            //UpdateBannerAds ( );
            //UpdateNativeAds ( );
        }

        public static void LogMessage( string message, bool error = false )
        {
            if (error)
            {
                Debug.LogError( "@ AdsError @ " + message );
            }
            else if (PSVSettings.settings.ads_manager_debug)
            {
                Debug.Log( "@ Ads @ " + message );
            }

            if (OnMessageLogged != null)
            {
                OnMessageLogged( (error ? "~" : "") + message );
            }
        }


        public delegate void DelayedCallback();


        public static void DelayedCall( DelayedCallback callback, float t )
        {
            CoroutineHandler.Start( WaitAndExecute( callback, t ) );
        }


        static IEnumerator WaitAndExecute( DelayedCallback callback, float t )
        {
            yield return new WaitForSeconds( t );
            callback();
        }

        #endregion

        #region Provider callbacks holder

        public static void SendBannerAvailable( AdNetwork net, bool is_available )
        {
            AdsInterop.BannerEvent( net.ToString(), is_available );
        }

        private static void BannerReadyChanged()
        {
            UpdateBannerAds();
        }


        private static void InterstitialShown()
        {
            AdsInterop.InterstitialShown();
        }

        private static void OnInterClosed()
        {
            AdsInterop.InterstitialClosed();
        }

        public static void SendInterstitialAvailable( AdNetwork net, bool is_available )
        {
            AdsInterop.InterstitialEvent( net.ToString(), is_available );
        }


        private static void RewardedShown()
        {
            AdsInterop.RewardedShown();
        }

        private static void OnRewardedCompleted()
        {
            AdsInterop.RewardedCompleted();
        }

        private static void OnRewardedClosed()
        {
            AdsInterop.RewardedClosed();
        }

        public static void SendRewardedAvailable( AdNetwork net, bool is_available )
        {
            AdsInterop.RewardedEvent( net.ToString(), is_available );
        }


        private static void NativeReadyChanged()
        {
            UpdateNativeAds();
        }

        public static void SendNativeAvailable( AdNetwork net, bool is_available )
        {
            AdsInterop.NativeEvent( net.ToString(), is_available );
        }

        #endregion

    }
}
