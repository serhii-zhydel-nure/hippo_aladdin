﻿using System;

namespace PSV.ADS
{
	public interface IAdProvider
	{
		event Action<AdNetwork, bool>
			OnBannerEvent,
			OnNativeEvent,
			OnInterstitialEvent,
			OnRewardedEvent;
		event Action<string,bool>
			OnMessageLogged;
		event Action
			OnInterstitialShown,
			OnInterstitialClosed,
			OnBannerReadyChanged,
			OnNativeReadyChanged,
			OnRewardedShown,
			OnRewardedClosed,
			OnRewardedCompleted;

		void SetRequestInterval (float interval);

		void Init (bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized, object settings);

		AdNetwork GetNetworkType ();

		string [] GetNecessaryParams ();

		bool IsBannerAdAvailable (); //return false if not supported

		bool IsNativeAdAvailable (); //return false if not supported

		bool ShowInterstitialAd (); //return false if not supported

		bool ShowRewardedAd (); //return false if not supported

		void ShowBannerAd (bool show);

		void ShowNativeAd (bool show);

		void RefreshBannerAd (AdPosition ad_pos, AdSize ad_size);

		void RefreshNativeAd (AdPosition ad_pos, AdSize ad_size);

		void DisableAds ();

		void EnableAds ();

		string ToString ();
	}
}
