﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace PSV.ADS
{
    public static class AdsSettings
    {
        private static AdNetwork[]
            providers_order;

        static Dictionary<string, string> 
            InitialParams = new Dictionary<string, string>();

        #region Predefined const params and their values

        public const string
            HOME_ADS_PARAM = "HOME",
            DEMO_ADS_PARAM = "DEMO";

        private const string
            HOME_BANNER = "ca-app-pub-1022958838828668/3557542231",
            HOME_INTERSTITIAL = "ca-app-pub-1022958838828668/5353405837",
            ADMOB_BANNER_DEMO = "ca-app-pub-3940256099942544/6300978111",
            ADMOB_INTERSTITIAL_DEMO = "ca-app-pub-3940256099942544/4411468910";


        private static string CheckForCanstants( string param, string val )
        {
            if (val != null)
            {
                switch (val)
                {
                    case DEMO_ADS_PARAM:
                        val = GetDemoSetting( param );
                        break;
                    case HOME_ADS_PARAM:
                        val = GetHomeSetting( param );
                        break;
                }
            }
            return val;
        }

        private static string GetDemoSetting( string param )
        {
            string res = null;
            if (IsBannerParam( param ))
            {
                res = ADMOB_BANNER_DEMO;
            }
            else if (IsInterstitialParam( param ))
            {
                res = ADMOB_INTERSTITIAL_DEMO;
            }
            return res;
        }

        private static string GetHomeSetting( string param )
        {
            string res = null;
            if (IsBannerParam( param ))
            {
                res = HOME_BANNER;
            }
            else if (IsInterstitialParam( param ))
            {
                res = HOME_INTERSTITIAL;
            }
            return res;
        }

        private static bool IsBannerParam( string param )
        {
            bool res = false;
            switch (param)
            {
                case ProviderParams.GOOGLEADS_BANNER_ID:
                case ProviderParams.NEATPLUG_BANNER_ID:
                case ProviderParams.MIDGAD_BANNER_ID:
                case ProviderParams.HIGHGAD_BANNER_ID:
                case ProviderParams.HOMEADS_BANNER_ID:
                    res = true;
                    break;
            }
            return res;
        }

        private static bool IsInterstitialParam( string param )
        {
            bool res = false;
            switch (param)
            {
                case ProviderParams.GOOGLEADS_INTERSTITIAL_ID:
                case ProviderParams.NEATPLUG_INTERSTITIAL_ID:
                case ProviderParams.MIDGAD_INTERSTITIAL_ID:
                case ProviderParams.HIGHGAD_INTERSTITIAL_ID:
                case ProviderParams.HOMEADS_INTERSTITIAL_ID:
                    res = true;
                    break;
            }
            return res;
        }

#endregion

        public static void Init()
        {
            LogMessage( "AdsSettings initializing" );
            LoadSavedParams();
        }


        public static void LoadSavedParams()
        {
            LoadInitialParams();
            LoadProvidersOrder();
        }

        private static void LoadInitialParams()
        {
            //taking settings from scriptable object
            var settings = PSVSettings.settings.ads_provider_settings;
            if (settings != null)
            {
                for (int i = 0; i < settings.Length; i++)
                {
                    SetInitialParam( settings[i].param, settings[i].value );
                }
            }
        }


        private static void SetInitialParam( string key, string val )
        {
            if (!string.IsNullOrEmpty( val ))
            {
                if (InitialParams.ContainsKey( key ))
                {
                    InitialParams[key] = val;
                }
                else
                {
                    InitialParams.Add( key, val );
                }
            }
        }



        private static void LoadProvidersOrder()
        {
            //* Obsolete logic
            //List<AdNetwork> new_order = new List<AdNetwork>();
            ////getting providers order, set through scriptable object
            //new_order.AddRange( PSVSettings.settings.providers_order );

            //if (new_order != null && new_order.Count > 0)
            //{
            //    providers_order = new_order.ToArray();
            //}
            providers_order = PSVSettings.settings.providers_order;
        }


        private static void LogMessage( string message, bool error = false )
        {
            AdsManager.LogMessage( "AdsSettings: " + message, error );
        }

        #region Getters

        public static AdNetwork[] GetProvidersList()
        {
#if UNITY_STANDALONE || UNITY_WEBGL
			return new AdNetwork [0];
#else
            return providers_order;
#endif
        }


        private static string GetLocalParam( string key )
        {
            return PlayerPrefs.GetString( key, "" );
        }


        public static Dictionary<string, string> GetSettings( string[] _params )
        {
            Dictionary<string, string> res = new Dictionary<string, string>();
            for (int i = 0; i < _params.Length; i++)
            {
                string param = _params[i];
                string val = null;
                if (!InitialParams.TryGetValue( param, out val ))
                {
                    LogMessage( "no settings found for param " + param );
                }

                val = CheckForCanstants( param, val );

                if (!string.IsNullOrEmpty( val ))
                {
                    res.Add( _params[i], val );
                }
            }
            return res;
        }

        #endregion

        #region Setters

        private static void SetLocalParam( string key, string value )
        {
            LogMessage( "Setting param " + key + " = " + value );
            PlayerPrefs.SetString( key, value );
            NextFrameCall.SavePrefs();
        }

        #endregion



        public static void ReadFromString( this float target, string val )
        {
            float new_val;
            if (float.TryParse( val, out new_val ))
            {
                target = new_val;
            }
        }

        public static void ReadFromString( this bool target, string val )
        {
            bool new_val;
            if (!string.IsNullOrEmpty( val ) && bool.TryParse( val, out new_val ))
            {
                target = new_val;
            }
        }

        public static void ReadFromString( this int target, string val )
        {
            int new_val;
            if (!string.IsNullOrEmpty( val ) && int.TryParse( val, out new_val ))
            {
                target = new_val;
            }
        }
    }
}