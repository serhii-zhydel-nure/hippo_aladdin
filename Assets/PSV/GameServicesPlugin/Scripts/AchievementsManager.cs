﻿#if UNITY_ANDROID
//#define GOOGLE_PLAY_GAMES_PRESENT
#endif

using UnityEngine;
using UnityEngine.SocialPlatforms;
using System;
using System.Collections.Generic;


//https://habrahabr.ru/post/256951/

namespace PSV.GameServices
{

	public class AchievementsManager
	{
		public static event Action<bool, string> OnScoreReported;
		public static event Action<bool, string> OnProgressReported;

		private const string authenticate_on_start_param = "AUTHENTICATE_GPGS_AUTOMATICALLY";

		private static bool debug_mode = false;


		private static Dictionary<string, Common.Achievement> achievements;

		private static Dictionary<string, Common.Leaderboard> leaderboards;


		// achievements stored on server (for IOS only)
		private static Dictionary<string, IAchievement> achievementDict = new Dictionary<string, IAchievement> ( );

		/// <summary>
		/// Activates platform and signs in if allowed
		/// </summary>
		public static void Init (AchievementProperty [] achievements_prop, LeaderboardProperty [] leaderboard_prop)
		{
			LogMessage ( "Init" );

			SetAchievements ( achievements_prop );
			SetLeaderboards ( leaderboard_prop );

			AuthManager.OnAuthenticated += Authenticated;

			AuthManager.Activate ( );

		}

		/// <summary>
		/// Initializes achievements dictionary and fills it with given items
		/// </summary>
		/// <param name="achievements_prop"></param>
		private static void SetAchievements (AchievementProperty [] achievements_prop)
		{
			achievements = new Dictionary<string, Common.Achievement> ( );

			if (achievements_prop != null && achievements_prop.Length > 0)
			{
				for (int i = 0; i < achievements_prop.Length; i++)
				{
					AchievementProperty prop = achievements_prop [i];
					achievements.Add ( prop.id, new Common.Achievement ( prop.id, prop.is_incremental ) );
				}
			}
			else
			{
				LogMessage ( "Init is called without achievements" );
			}
		}

		/// <summary>
		/// Initializes leader boards dictionary and fills it with given items
		/// </summary>
		/// <param name="leaderboard_prop"></param>
		private static void SetLeaderboards (LeaderboardProperty [] leaderboard_prop)
		{
			leaderboards = new Dictionary<string, Common.Leaderboard> ( );

			if (leaderboard_prop != null && leaderboard_prop.Length > 0)
			{
				for (int i = 0; i < leaderboard_prop.Length; i++)
				{
					LeaderboardProperty prop = leaderboard_prop [i];
					leaderboards.Add ( prop.id, new Common.Leaderboard ( prop.id, prop.more_is_better ) );
				}
			}
			else
			{
				LogMessage ( "Init is called without leader boards" );
			}
		}

		/// <summary>
		/// Display service interface for Achievements
		/// </summary>
		public static void ShowAchievementsUI ()
		{
			LogMessage ( "ShowAchievementsUI" );
			Social.ShowAchievementsUI ( );
		}

		/// <summary>
		/// Display service interface for Leader boards
		/// </summary>
		public static void ShowLeaderboardUI ()
		{
			LogMessage ( "ShowLeaderboardUI" );
			Social.ShowLeaderboardUI ( );
		}




		/// <summary>
		/// logs message to console
		/// </summary>
		/// <param name="message"></param>
		/// <param name="error"></param>
		private static void LogMessage (string message, bool error = false)
		{
			message = "GameServices.AchievementsManager :: " + message;
			if (error)
			{
				UnityEngine.Debug.LogError ( message );
			}
			else if (debug_mode)
			{
				UnityEngine.Debug.Log ( message );
			}
		}

		/// <summary>
		/// Returns Leader board that contains saved score.
		/// </summary>
		/// <param name="leaderboard_id"></param>
		/// <returns></returns>
		private static Common.Leaderboard GetSavedScores (string leaderboard_id)
		{
			Common.Leaderboard score;
			if (!leaderboards.TryGetValue ( leaderboard_id, out score ))
			{
				score = null;
				LogMessage ( "GetSavedScores: no scores available [" + leaderboard_id + "]" );
			}
			return score;
		}



		/// <summary>
		/// Returns Leader board that contains saved score.
		/// </summary>
		/// <param name="leaderboard_id"></param>
		/// <returns></returns>
		private static Common.Achievement GetSavedProgress (string achievement_id)
		{
			Common.Achievement progress;
			if (!achievements.TryGetValue ( achievement_id, out progress ))
			{
				progress = null;
				LogMessage ( "GetSavedProgress: no progress available [" + achievement_id + "]" );
			}
			return progress;
		}


		/// <summary>
		/// Used to get IAchievemetn to report progress based on existing data (for IOS only)
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public IAchievement GetAchievement (string id)
		{
			return achievementDict [id];
		}

		/// <summary>
		/// Reports score to the given leader board
		/// </summary>
		/// <param name="progress"></param>
		/// <param name="leaderboard_id"></param>
		public static void ReportScore (long progress, string leaderboard_id)
		{
			Common.Leaderboard score = GetSavedScores ( leaderboard_id );
			if (score != null)
			{
				score.SaveScore ( progress );
				long value;
				if (score.TryGetValue ( out value ))
				{
					if (AuthManager.IsAuthenticated ( ))
					{
						Social.ReportScore ( value, leaderboard_id, (bool result) => ScoreReported ( result, leaderboard_id ) );
					}
					else
					{
						LogMessage ( "ReportScore: not authenticated, score stored locally" );
					}
				}
				else
				{
					LogMessage ( "ReportScore failed, SaveScore didn't work as it should [" + leaderboard_id + "]", true );
				}
			}
			else
			{
				LogMessage ( "ReportScore failed, leader board was not initialized [" + leaderboard_id + "]", true );
			}
		}

		/// <summary>
		/// Reports progress on achievement
		/// </summary>
		/// <param name="achievement_id"></param>
		/// <param name="steps"></param>
		public void ReportProgress (string achievement_id, int steps)
		{
			Common.Achievement progress = GetSavedProgress ( achievement_id );
			progress.SaveProgress ( steps );
			int value = progress.GetProgress ( );
			if (value > 0)
			{
				if (AuthManager.IsAuthenticated ( ))
				{
					progress.ProgressReported ( value );
#if UNITY_ANDROID && GOOGLE_PLAY_GAMES_PRESENT
					(Social.Active as GooglePlayGames.PlayGamesPlatform).IncrementAchievement ( achievement_id, steps, (bool result) => ProgressReported ( result, achievement_id, value ) );
#elif UNITY_IPHONE
					IAchievement achievement = GetAchievement ( achievement_id );

					// normalizing value in range 0 - 100
					achievement.percentCompleted += 100.0 / value;

					achievement.ReportProgress ( (bool result) => ProgressReported ( result, achievement_id, value ) );
#endif
				}
				else
				{
					LogMessage ( "ReportProgress: not authenticated, progress stored locally" );
				}
			}
		}

		#region EventHandlers

		/// <summary>
		/// Listens for authentication status
		/// </summary>
		/// <param name="result"></param>
		private static void Authenticated (bool result)
		{
#if UNITY_IPHONE
			Social.LoadAchievements ( AchievementsLoadComplete );
#endif
		}


		/// <summary>
		/// Listens to ScoreReport result
		/// </summary>
		/// <param name="result"></param>
		/// <param name="leaderboard_id"></param>
		private static void ScoreReported (bool result, string leaderboard_id)
		{
			if (OnScoreReported != null)
			{
				OnScoreReported ( result, leaderboard_id );
			}
		}

		/// <summary>
		/// Listens to ScoreReport result
		/// </summary>
		/// <param name="result"></param>
		/// <param name="leaderboard_id"></param>
		private static void ProgressReported (bool result, string achievement_id, int value)
		{
			Common.Achievement progress = GetSavedProgress ( achievement_id );
			if (progress != null)
			{
				progress.ResetProgress ( value, result );
			}
			else
			{
				LogMessage ( "ProgressReported: Something went wrong, no achievement found [" + achievement_id + "]", true );
			}
			if (OnProgressReported != null)
			{
				OnProgressReported ( result, achievement_id );
			}
		}

#if UNITY_IPHONE
		/// <summary>
		/// Listens to achievements loading process
		/// </summary>
		/// <param name="ach"></param>
		private static void AchievementsLoadComplete (IAchievement [] ach)
		{
			// caching achievements that already have a progress
			foreach (IAchievement item in ach)
			{
				achievementDict.Add ( item.id, item );
			}

			// create the rest of achievements that does not have any progress yet
			foreach (var item in achievements)
			{
				string id = item.Key;
				if (!achievementDict.ContainsKey ( id ))
				{
					IAchievement achievement = Social.CreateAchievement ( );
					achievement.id = id;
					achievementDict.Add ( id, achievement );
				}
			}
		}
#endif

		#endregion
	}
}