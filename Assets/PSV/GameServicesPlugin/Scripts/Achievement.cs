﻿using UnityEngine;
using System.Collections;


namespace PSV.GameServices.Common
{
	public class Achievement
	{
		private const string param_prefix = "PSV_Achievements_";
		private const char separator = ';';

		//Social platform identifier for the item
		private string identifier;
		private bool is_incremental;
		private int progress = 0;
		private int cached_progress = 0;

		/// <summary>
		/// creates Achievement with given identifier and type
		/// </summary>
		/// <param name="identifier"></param>
		/// <param name="is_incremental"></param>
		public Achievement (string identifier, bool is_incremental)
		{
			this.identifier = identifier;
			this.is_incremental = is_incremental;
		}



		/// <summary>
		/// Saves achievement progress
		/// </summary>
		/// <param name="score"></param>
		public void SaveProgress (int progress)
		{
			if (is_incremental)
			{
				progress += progress;
			}
			else
			{
				this.progress = progress;
			}
			StoreScore ( );
		}

		/// <summary>
		/// Decreasing saved progress by reported_value. Made to avoid sequential reports and achievement faulty value
		/// </summary>
		/// <param name="reported_value"></param>
		public void ProgressReported (int reported_value)
		{
			if (progress >= reported_value)
			{
				progress -= reported_value;
				cached_progress += reported_value;
			}
		}


		/// <summary>
		/// Will reset progress after it was reported to Social platform. If process finished correctly cache will be decreased by reported value. Else saved progress will be increased by cached value.
		/// </summary>
		/// <param name="reported_value"></param>
		public void ResetProgress (int reported_value, bool success)
		{
			if (cached_progress >= reported_value)
			{
				if (!success)
				{
					SaveProgress ( reported_value );
				}
				cached_progress -= reported_value;
			}
		}


		/// <summary>
		/// Converts Achievement from string
		/// </summary>
		/// <param name="score"></param>
		/// <returns></returns>
		private void ParseProgress (string value)
		{
			if (!int.TryParse ( value, out progress ))
			{
				this.progress = 0;
			}
		}

		/// <summary>
		/// Returns progress on current achievement
		/// </summary>
		/// <returns></returns>
		public int GetProgress ()
		{
			return progress;
		}

		/// <summary>
		/// returns player prefs key for current High Score
		/// </summary>
		/// <returns></returns>
		private string GetParamName ()
		{
			return param_prefix + identifier;
		}


		private void StoreScore ()
		{
			PlayerPrefs.SetString ( GetParamName ( ), GetProgress().ToString ( ) );
		}


		private void LoadAchievement ()
		{
			string value = PlayerPrefs.GetString ( GetParamName ( ), "" );
			ParseProgress ( value );
		}
	}
}
