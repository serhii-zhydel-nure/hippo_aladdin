﻿using UnityEngine;
using System.Collections;

namespace PSV.GameServices.Common
{
	/// <summary>
	/// Stores min and max score values to report to Social platform (will work with both leader boards - more is better and less is better).
	/// Will keep only to values to report them to social platform after authentication.
	/// </summary>
	public class Leaderboard
	{
		//TODO: Implement leader board type MoreIsBetter and LessIsBetter

		private const string param_prefix = "PSV_Leaderboards_";

		//tells if class has real values. we can't use default values as both of them will be reported 
		private bool is_set = false;

		//Social platform identifier for the item
		private string identifier;

		private bool more_is_better;

		private long value;
		//private long min_value = long.MaxValue;
		//private long max_value = long.MinValue;


		/// <summary>
		/// Creates HighSCore with given identifier
		/// </summary>
		/// <param name="identifier"></param>
		public Leaderboard (string identifier, bool more_is_better)
		{
			this.identifier = identifier;
			this.more_is_better = more_is_better;
			LoadScore ( );
		}


		/// <summary>
		/// sets min and max values for current score. Will be saved only if previous data is worse then current (one of two cases - new score is higher or lower then previous)
		/// </summary>
		/// <param name="score"></param>
		public bool SaveScore (long score)
		{
			bool res = false;
			if (more_is_better)
			{
				if (value < score)
				{
					value = score;
					res = true;
				}
			}
			else
			{
				if (value > score)
				{
					value = score;
					res = true;
				}
			}
			return res;
		}


		/// <summary>
		/// Takes string which contains score and stores its value
		/// </summary>
		/// <param name="score"></param>
		private bool ParseScore (string score)
		{
			if (!string.IsNullOrEmpty ( score ))
			{
				if (long.TryParse ( score, out value ))
				{
					is_set = true;
				}
			}
			return is_set;
		}


		/// <summary>
		/// Returns true score was set before. Value will contain min or max score (depends on more_is_better)
		/// </summary>
		/// <param name="value">Score container</param>
		/// <returns></returns>
		public bool TryGetValue (out long value)
		{
			if (is_set)
			{
				value = this.value;
				return true;
			}
			else
			{
				value = -1;
				return false;
			}
		}

		/// <summary>
		/// Converts score to string
		/// </summary>
		/// <returns></returns>
		public override string ToString ()
		{
			string res = "";
			if (is_set)
			{
				res = value.ToString ( );
			}
			return res;
		}

		/// <summary>
		/// returns player prefs key for current High Score
		/// </summary>
		/// <returns></returns>
		private string GetParamName ()
		{
			return param_prefix + identifier;
		}

		/// <summary>
		/// Saves score to player prefs
		/// </summary>
		private void StoreScore ()
		{
			PlayerPrefs.SetString ( GetParamName ( ), this.ToString ( ) );
		}

		/// <summary>
		/// Reads core from player prefs
		/// </summary>
		private void LoadScore ()
		{
			string value = PlayerPrefs.GetString ( GetParamName ( ), "" );
			ParseScore ( value );
		}
	}
}