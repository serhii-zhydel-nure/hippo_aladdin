﻿#if UNITY_ANDROID
//#define GOOGLE_PLAY_GAMES_PRESENT
#endif

using System;
using UnityEngine;
using UnityEngine.SocialPlatforms;
#if GOOGLE_PLAY_GAMES_PRESENT
using GooglePlayGames;
#endif

namespace PSV.GameServices
{
	public class AuthManager
	{
		public static event Action<bool> OnAuthenticated;


		private static bool debug_mode = false;


		//private static bool is_Authenticated = false;

		private static bool authenticate_on_start = false;

		private const string authenticate_on_start_param = "AUTHENTICATE_GPGS_AUTOMATICALLY";



		public static void Activate ()
		{
#if UNITY_ANDROID && GOOGLE_PLAY_GAMES_PRESENT
			// Activating GooglePlayGames platform to make Social receive it's interface
			PlayGamesPlatform.Activate ( );
#endif

#if UNITY_IPHONE
			// by default nothing happens on IOS as Achievement revealed. So we enable banner.
			UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
#endif

#if UNITY_ANDROID && GOOGLE_PLAY_GAMES_PRESENT
			authenticate_on_start = GetAuthenticateOnStart ( );
#elif UNITY_IPHONE
			authenticate_on_start = true;
#endif
			if (authenticate_on_start)
			{
				Authenticate ( );
			}
		}


		/// <summary>
		/// Returns authentication status
		/// </summary>
		/// <returns></returns>
		public static bool IsAuthenticated ()
		{
			//return is_Authenticated;
			return Social.localUser != null && Social.localUser.authenticated;
		}

		/// <summary>
		/// Authentication on Social platform. Should perform only on user submission. Have to show prompt asking for Signing in.
		/// As user signed in once - he will be authenticated on start every time, till he will sign out.
		/// </summary>
		public static void Authenticate ()
		{
			LogMessage ( "Authenticate" );
			ILocalUser local_user = Social.localUser;
			if (local_user != null)
			{
				local_user.Authenticate ( Authenticated );
			}
		}



		/// <summary>
		/// Signs out from service (android only)
		/// </summary>
		public static void SignOut ()
		{
#if GOOGLE_PLAY_GAMES_PRESENT
			LogMessage ( "SignOut" );
			PlayGamesPlatform.Instance.SignOut ( );
			SetAuthenticateOnStart ( false ); //if user logged out we disable auto sign in
#elif UNITY_IPHONE
			//there is no way to logout from IOS GameCenter
#endif
		}

		/// <summary>
		/// Saves parameter that defines whether to Sign in on start
		/// </summary>
		/// <param name="param"></param>
		private static void SetAuthenticateOnStart (bool param)
		{
			authenticate_on_start = param;
			PlayerPrefs.SetInt ( authenticate_on_start_param, authenticate_on_start ? 1 : 0 );
            NextFrameCall.SavePrefs();
        }


		/// <summary>
		/// Tells if we have to sign in on start
		/// </summary>
		/// <returns></returns>
		private static bool GetAuthenticateOnStart ()
		{
			return PlayerPrefs.GetInt ( authenticate_on_start_param, 0 ) == 1;
		}


		/// <summary>
		/// logs message to console
		/// </summary>
		/// <param name="message"></param>
		/// <param name="error"></param>
		private static void LogMessage (string message, bool error = false)
		{
			message = "GameServices.AuthManager :: " + message;
			if (error)
			{
				UnityEngine.Debug.LogError ( message );
			}
			else if (debug_mode)
			{
				UnityEngine.Debug.Log ( message );
			}
		}

		#region EventHandlers

		/// <summary>
		/// Listens for authentication status
		/// </summary>
		/// <param name="result"></param>
		private static void Authenticated (bool result)
		{
			//is_Authenticated = result;
			LogMessage ( "Authentication result = " + IsAuthenticated ( ) );
			if (IsAuthenticated ( ))
			{
				SetAuthenticateOnStart ( true );
			}

			if (OnAuthenticated != null)
			{
				OnAuthenticated ( IsAuthenticated ( ) );
			}
		}

		#endregion

	}
}
