﻿
namespace PSV.GameServices
{
	public struct AchievementProperty
	{
		public string id;
		public bool is_incremental;

		public AchievementProperty (string id, bool is_incremental)
		{
			this.id = id;
			this.is_incremental = is_incremental;
		}
	}

	public struct LeaderboardProperty
	{
		public string id;
		public bool more_is_better;

		public LeaderboardProperty (string id, bool more_is_better)
		{
			this.id = id;
			this.more_is_better = more_is_better;
		}
	}
}