﻿using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitSceneLoader :MonoBehaviour
{
	[MenuItem ( "Play/Play % " )]
	static void CheckAndLoadInitScene ()
	{
		if (EditorApplication.isPlaying == true)
		{
			EditorApplication.isPlaying = false;
			return;
		}

		//Debug.Log ( "CheckAndLoadInitScene" );
		string init_scene = PSV.Scenes.None.ToString ( );
		Scene active_s = SceneManager.GetActiveScene ( );
		if (!active_s.name.Equals ( init_scene ))
		{
			Scene init_s = EditorSceneManager.OpenScene ( GetLevelPath ( init_scene ), OpenSceneMode.Additive );
			EditorSceneManager.MoveSceneBefore ( init_s, active_s );
			SceneManager.SetActiveScene ( init_s );
			
            if (PSV.EditorPSVSettings.settings.splash_transition)
            {
                PSV.EditorPSVSettings.settings.splash_transition = false;
                Debug.Log( "Disabling splash_transition for SceneLoader" );
            }
        }
		EditorApplication.isPlaying = true;
	}

	private static string GetLevelPath (string level_name)
	{
		string [] res = (from scene in EditorBuildSettings.scenes where scene.path.Contains ( level_name ) select scene.path).ToArray ( );
		return res.Length > 0 ? res [0] : "";
	}

}
