﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

namespace PSV.SettingsEditor
{
    public static class PrototypeExport
    {
        private const string ExportFolder = "ExportedPackages";
        private const string dateReleaseFile = "ProjectSettings/PrototypeReleaseDate.json";
        private const string FullExportLabels = "l:PSVPrototype l:PSVCustom"; // l:First l:Seconds

        [MenuItem( "HelpTools/Export prototype/Full " + FullExportLabels )]
        public static void ExportFull()
        {
            Export( FullExportLabels, false, false );
        }
        [MenuItem( "HelpTools/Export prototype/Release " + FullExportLabels )]
        public static void ExportRelease()
        {
            Export( FullExportLabels, false, true );
        }
        [MenuItem( "HelpTools/Export prototype/Update for last release " + FullExportLabels )]
        public static void ExportUpdate()
        {
            Export( FullExportLabels, true, false );
        }
        [MenuItem( "HelpTools/Export prototype/Update for last release l:PSVPrototype" )]
        public static void ExportUpdateWithoutCustom()
        {
            Export( "l:PSVPrototype", true, false );
        }

        public static void Export( string findLabels, bool updateForLastRelease, bool exportLikeRelease )
        {
            var labledAssets = AssetDatabase.FindAssets( findLabels );
            DateTime releaseTime = DateTime.MinValue;
            string exportName;
            if (updateForLastRelease)
            {
                string releaseVersion = "0.0";
                if (File.Exists( dateReleaseFile ))
                {
                    ReleaseInfo releaseInfo = JsonUtility.FromJson<ReleaseInfo>( File.ReadAllText( dateReleaseFile ) );
                    releaseTime = DateTime.FromFileTimeUtc( releaseInfo.dateTime );
                    releaseVersion = releaseInfo.version;
                }
                exportName = GetExportNameUpdate( releaseVersion );
            }
            else
            {
                exportName = GetExportNameFull();
            }

            int countAssets = labledAssets.Length;
            List<string> updateFiles = new List<string>( countAssets / 2 );
            for (int i = 0; i < countAssets; i++)
            {
                labledAssets[i] = AssetDatabase.GUIDToAssetPath( labledAssets[i] );
                if (EditorUtility.DisplayCancelableProgressBar( "Prepare " + exportName, labledAssets[i], ( float )i / countAssets ))
                {
                    EditorUtility.ClearProgressBar();
                    return;
                }
                if (updateForLastRelease && DateTime.Compare( File.GetLastWriteTimeUtc( labledAssets[i] ), releaseTime ) > 0)
                    updateFiles.Add( labledAssets[i] );
            }
            if (updateForLastRelease)
            {
                labledAssets = updateFiles.ToArray();
            }
            if (labledAssets.Length > 0)
            {
                EditorUtility.DisplayProgressBar( "Hold on", "Build: " + exportName, 0.9f );
                AssetsHelper.CreateFolder( ExportFolder );
                AssetDatabase.ExportPackage( labledAssets, ExportFolder + Path.DirectorySeparatorChar + exportName, ExportPackageOptions.Recurse );

                if (exportLikeRelease)
                    File.WriteAllText( dateReleaseFile, JsonUtility.ToJson( new ReleaseInfo( DateTime.UtcNow ) ) );
                System.Diagnostics.Process.Start( ExportFolder );
            }
            else
            {
                EditorUtility.DisplayDialog( "Build skipped", "There are no asetas for create unity package", "Ok" );
            }
            EditorUtility.ClearProgressBar();
        }

        public static string GetExportNameFull()
        {
            return string.Format( System.Globalization.CultureInfo.InvariantCulture,
                "Full_{0}_{1}.unitypackage", ConstSettings.prototypeVersion, DateTime.Now.ToString( "dd-MM-yyyy" ) );
        }
        public static string GetExportNameUpdate( string releaseVersion )
        {
            return string.Format( System.Globalization.CultureInfo.InvariantCulture,
                "Update_{0}_for_{1}_{2}.unitypackage", ConstSettings.prototypeVersion, releaseVersion, DateTime.Now.ToString( "dd-MM-yyyy" ) );
        }

        [Serializable]
        public class ReleaseInfo
        {
            public string version;
            public long dateTime;

            public ReleaseInfo() { }

            public ReleaseInfo( DateTime time )
            {
                dateTime = time.ToFileTimeUtc();
                version = ConstSettings.prototypeVersion;
            }

        }
    }
}