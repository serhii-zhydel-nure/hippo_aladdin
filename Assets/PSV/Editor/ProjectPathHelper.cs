﻿using System;
using UnityEditor;
using UnityEngine;

public static class ProjectPathHelper  {
#if UNITY_EDITOR_WIN

    [MenuItem( "Help/Clear PlayerPrefs" )]
    public static void ClearPrefs()
    {
        PlayerPrefs.DeleteAll();
    }

    [MenuItem ( "Help/Open Persistent Folder" )]
	public static void OpenPersistentFolder ()
	{
		System.Diagnostics.Process.Start ( "explorer.exe", Application.persistentDataPath.Replace ( "/", @"\" ) );
	}

	[MenuItem ( "Help/Open Project Folder" )]
	public static void OpenProjectFolder ()
	{
		System.Diagnostics.Process.Start ( "explorer.exe", Application.dataPath.Replace ( "/Assets", "/" ).Replace ( "/", @"\" ) );
	}

	[MenuItem ( "Help/Open Register Path" )]
	public static void OpenRegisterPath ()
	{
		//Project location path
		string registryLocation = @"HKEY_CURRENT_USER\Software\Unity\UnityEditor\" + Application.companyName + @"\" + Application.productName + @"\";
		//Last location path
		string registryLastKey = @"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Applets\Regedit";

		try
		{
			//Set LastKey value that regedit will go directly to
			Microsoft.Win32.Registry.SetValue ( registryLastKey, "LastKey", registryLocation );
			System.Diagnostics.Process.Start ( "regedit.exe" );
		}
		catch (Exception e)
		{
			Debug.LogException ( e );
		}
	}
#endif
}
