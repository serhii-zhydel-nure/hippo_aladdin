﻿using UnityEngine;
using System.Collections;
using UnityEditor.Callbacks;
using UnityEditor;
using System.IO;

namespace PSV.SettingsEditor
{
    public static class ShowUsedAssetsAfterBuild
    {

        [PostProcessBuild( 50 )]
        public static void OnPostprocessBuild( BuildTarget target, string pathToBuiltProject )
        {
            string info = FindNotUsedFiles.SelectLastBuildInfo();
            if (info != null)
            {
                System.Diagnostics.Process.Start( Path.GetFullPath( FindNotUsedFiles.lastBuildInfoPath ) );
            }
        }
    }
}