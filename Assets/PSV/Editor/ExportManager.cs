﻿#if false // Disabled. Try now use PrototypeExport functions.
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class ExportManager : MonoBehaviour
{

    private const string menu_path = "HelpTools/Export package/";

    public class PackageSet
    {
        public string[]
            FoldersToInclude,
            FilesToInclude,
            BlackList;

        public PackageSet( string[] folders, string[] files )
        {
            FoldersToInclude = folders;
            FilesToInclude = files;
        }

        public PackageSet( string[] folders, string[] files, string[] black_list )
        {
            FoldersToInclude = folders;
            FilesToInclude = files;
            BlackList = black_list;
        }

        public string[] GetFilesFromPackage()
        {
            List<string> files = new List<string>();
            //scan folders
            if (FoldersToInclude != null)
            {
                for (int i = 0; i < FoldersToInclude.Length; i++)
                {
                    files.AddRange( (string[])Directory.GetFiles( FoldersToInclude[i], "*.*", SearchOption.AllDirectories ) );
                }
            }
            //check all files
            if (FilesToInclude != null)
            {
                for (int i = 0; i < FilesToInclude.Length; i++)
                {
                    string f = FilesToInclude[i];
                    if (File.Exists( f ))
                    {
                        files.Add( f );
                    }
                    else
                    {
                        Debug.Log( "File '" + f + "' does not exist" );
                    }
                }
            }
            if (BlackList != null)
            {
                for (int i = 0; i < files.Count; i++)
                {
                    for (int j = 0; j < BlackList.Length; j++)
                    {
                        if (files[i] == BlackList[j])
                            files.RemoveAt( i-- );
                    }
                }
            }
            return files.ToArray();
        }
    }

    public static PackageSet
        full_set = new PackageSet(
            new string[]
                {
                    //"Assets/AdMobAndAnalytics",
                    //"Assets/AdsManager",
                    //"Assets/Demigiant",
                    //"Assets/EasyTouchBundle",
                    //"Assets/Editor",
                    //"Assets/ExternalLinks",
                    "Assets/Game",
                    "Assets/PSV",
                    "Assets/Mediation",
                    //"Assets/ObsoleteScripts",
                    //"Assets/PSVPromoPlugin",
                    //"Assets/PSVPushNews",
                    //"Assets/RateMeModue",
                    //"Assets/PurchaseDialogue",
                    "Assets/Resources/Sounds",

                    "Assets/Plugins/Android/libs",
                    //"Assets/Plugins/Android/res/drawable",
                    "Assets/Plugins/IOS/Mediation",
					//"Assets/Plugins/MobFox",
					//"Assets/Plugins/InMobiAndroid",

                    "Assets/VungleExtras",
                },
            new string[]
                {
                    "Assets/Plugins/LitJson.dll",
                    "Assets/Plugins/NP_AdmobAd_WP.dll",
                    "Assets/Plugins/Android/AndroidManifest.xml",
                    "Assets/PSV/OpenIAB_plugin.unitypackage",
                },
            new string[]
                {
                    "Assets/PSV\\Editor\\EditorSettingsData.asset",
                    "Assets/PSV\\Editor\\EditorSettingsData.asset.meta",
                }
            ),
		lite_set = new PackageSet(
			new string[]
				{
                    //scripts
					"Assets/AdMobAndAnalytics",
					"Assets/AdsManager",
                    "Assets/ExternalLinks",
                    "Assets/Editor",
                    "Assets/Game/Scripts/Ads",
					"Assets/Game/Scripts/Analytics",
                    "Assets/Game/Scripts/AssetBundleLoader",
                    "Assets/Game/Scripts/Common",
					"Assets/Game/Scripts/IAP",
                    "Assets/Game/Scripts/GUI",
                    "Assets/Game/Scripts/Helpers",
                    "Assets/Game/Scripts/Localisation",
					"Assets/Game/Scripts/Offers",
                    "Assets/Game/Scripts/PSVSettings",
					"Assets/Game/Scripts/Utils",
                    "Assets/Mediation",
                    "Assets/ObsoleteScripts",
                    "Assets/PSVPromoPlugin",
                    //"Assets/PSVPushNews",
					"Assets/PurchaseDialogue",
					"Assets/RateMeModue",

					"Assets/Game/Animations/GUI",
					"Assets/Game/Font",
					"Assets/Game/Images/GUI",
                    "Assets/Game/Images/Loading",


					//plugins
					"Assets/Plugins/Android/res/drawable",
					"Assets/Plugins/IOS/Mediation",
					"Assets/Plugins/Android/libs",
					//"Assets/Plugins/InMobiAndroid",
					//"Assets/Plugins/MobFox",
					"Assets/Plugins/KIDOZ",
					"Assets/Plugins/Android/KidozSdk",
				},
			new string[]
				{
                    //scripts
                    
                    //prefabs
					"Assets/Game/Prefabs/GUI/BannerPanelCanvas.prefab",
                    "Assets/Game/Prefabs/GUI/loading_indicator.prefab",
                    "Assets/Game/Prefabs/GUI/Resources/FadeScreen.prefab",
                    "Assets/Game/Prefabs/GUI/Resources/WarningPanel.prefab",
                    "Assets/Game/Prefabs/GUI/Resources/ProcessIndicator.prefab",
                    "Assets/Game/Prefabs/GUI/Resources/ServiceMessage.prefab",

                    "Assets/Game/Scenes/Push.unity",

                    //plugins
					"Assets/Plugins/Android/AndroidManifest.xml",
					"Assets/Plugins/LitJson.dll",
					"Assets/Plugins/NP_AdmobAd_WP.dll",

                    "Assets/OpenIAB_plugin.unitypackage",
				},
            new string[]
                {
                    "Assets/Editor\\EditorSettingsData.asset",
                    "Assets/Editor\\EditorSettingsData.asset.meta",
                    "Assets/Game/Scripts/Helpers\\SceneLoaderSettings.cs",
                    "Assets/Game/Scripts/Helpers\\SceneLoaderSettings.cs.meta",
                }
			),
		audio_set = new PackageSet(
            new string[]
                {
                    "Assets/Game/Scripts/Common",
                    "Assets/Game/Scripts/Audio",
                    "Assets/Game/Scripts/Localisation/",

                },
			new string[]
				{
					"Assets/Game/Scripts/GUI/ToggleButton.cs",
					"Assets/Game/Scripts/Helpers/GameSettings.cs",
				}
			),
		push_promo = new PackageSet(
			new string[]
				{
					"Assets/PSVPushNews",
                    "Assets/PSVPromoPlugin",
                    "Assets/Game/Scripts/Common",
                },
			new string[]
				{
					"Assets/Game/Scenes/Push.unity",
                    "Assets/Plugins/Android/libs/PsvPlugins/psvpushservice_v3.jar",
                    "Assets/Game/Scripts/PSVSettings/ConstSettings.cs",
                }
            ),
        ads_set = new PackageSet(
            new string[]
                {
                    "Assets/Game/Scripts/Analytics",
                    "Assets/Game/Scripts/Common",
                    "Assets/Game/Scripts/PSVSettings",

                    "Assets/AdMobAndAnalytics",
                    "Assets/AdsManager",
					"Assets/ExternalLinks",
                    "Assets/Game/Scripts/Ads",
                },
            null
        ),
        psv_settings = new PackageSet(
            new string[]
                {
                    "Assets/Game/Scripts/PSVSettings/",
					"Assets/Game/Scripts/IAP", //it depends on IAP
                    "Assets/Game/Scripts/Localization", //it depends on Localization
                },
            null
        ),
        gui_set = new PackageSet(
			new string[]
				{
					"Assets/Game/Animations/GUI",
					"Assets/Game/Images/GUI",
					"Assets/Game/Images/Loading",
					"Assets/Game/Prefabs/GUI",
					"Assets/Game/Scripts/GUI/",
				},
			new string[]
			{
					"Assets/Game/Scripts/Ads/BannerListener.cs",
                    "Assets/Game/Scripts/Ads/BannerPannelCollider.cs",
					"Assets/Game/Scripts/Utils/RectTransformUtils.cs",
			}
		);

	[MenuItem( menu_path + "Export Full package" )]
	public static void ExportFullPackage()
	{
		ExportPackage( full_set, "full" );
	}


	[MenuItem( menu_path + "Export Full, Lite, GUI packages" )]
	public static void ExportAllPackages()
	{
		ExportFullPackage();
		ExportLitePackage();
		ExportGUIPackage();
	}


	[MenuItem( menu_path + "Export Lite Update package" )]
	public static void ExportLitePackage()
	{
		ExportPackage( lite_set, "lite" );
	}

	[MenuItem( menu_path + "Export GUI package" )]
	public static void ExportGUIPackage()
	{
		ExportPackage( gui_set, "gui" );
	}

	[MenuItem( menu_path + "Export Push And Promo package" )]
	public static void ExportPushPackage()
	{
		ExportPackage( push_promo, "push_promo" );
	}

    [MenuItem( menu_path + "Export Audio package" )]
    public static void ExportAudioPackage()
    {
        ExportPackage( audio_set, "audio" );
    }
    

    [MenuItem( menu_path + "Export Ads package" )]
    public static void ExportAdsPackage()
    {
        ExportPackage( ads_set, "ads" );
    }

    [MenuItem( menu_path + "Export PSV Settings package" )]
    public static void ExportSettingsPackage()
    {
        ExportPackage( psv_settings, "settings" );
    }


    static void ExportPackage( PackageSet set, string prefix )
	{
		Debug.Log( "Exporting " + prefix + " Unity Package..." );

		var path = OutputPath( prefix );

        EditorUtility.DisplayProgressBar( "ExportPackage", prefix, 1.0f );
        try
		{
			string[] files = set.GetFilesFromPackage();

			if (files != null && files.Length > 0)
			{
                AssetDatabase.ExportPackage( files, path, ExportPackageOptions.Recurse );
            }
			else
			{
				Debug.LogError( "Error exporting package: no files found from package set" );
			}
		}
		catch (System.Exception e)
		{
			Debug.LogError( "Error exporting package " + e.Message );
		}
        finally
        {
            EditorUtility.DisplayDialog( "ExportPackage", "Export done: " + path, "OK" );
        }
        EditorUtility.ClearProgressBar();
    }

	private static string OutputPath( string prefix )
	{
		var outputDirectory = new DirectoryInfo( Path.Combine( Directory.GetCurrentDirectory(), "ExportedPackages" ) );

		// Create the directory if it doesn't exist
		outputDirectory.Create();
		return Path.Combine( outputDirectory.FullName, PackageName( prefix ) );
	}


	private static string PackageName( string prefix )
	{
		System.DateTime date = System.DateTime.Now.Date;
		return string.Format(
			System.Globalization.CultureInfo.InvariantCulture,
			prefix + " {0}.unitypackage",
			date.Day + "-" + date.Month + "-" + date.Year );
	}
}
#endif