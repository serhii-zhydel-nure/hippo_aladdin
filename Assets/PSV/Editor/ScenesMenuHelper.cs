﻿using UnityEngine;
using System.Collections;

public class ScenesMenuHelper : MonoBehaviour
{

    [UnityEditor.MenuItem( "HelpTools/Generate Scene Menu" )]
    public static void GenerateMenu()
    {
        //Class strings
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine( "using UnityEngine;" );
        sb.AppendLine( "using UnityEditor;" );
        sb.AppendLine( "using UnityEngine.SceneManagement;" );
        sb.AppendLine( "using UnityEditor.SceneManagement;" );
        sb.AppendLine( "" );
        sb.AppendLine( "public static class ScenesMenu {" );
        sb.AppendLine( "" );
        sb.AppendLine( "    private static void OpenScene(string path)" );
        sb.AppendLine( "    {" );
        sb.AppendLine( "        OpenSceneMode openMode = OpenSceneMode.Single;" );
        sb.AppendLine( "        if (EditorUtility.DisplayDialog(\"Select open scene mode\", \"Chose scene opening mode.\", \"Single\", \"Additive\"))" );
        sb.AppendLine( "        {" );
        sb.AppendLine( "             Scene[] scenes = new Scene[SceneManager.sceneCount];" );
        sb.AppendLine( "             for (int i = 0; i < scenes.Length; i++)" );
        sb.AppendLine( "             {" );
        sb.AppendLine( "                 scenes[i] = SceneManager.GetSceneAt(i);" );
        sb.AppendLine( "             }" );
        sb.AppendLine( "             EditorSceneManager.SaveModifiedScenesIfUserWantsTo(scenes);" );
        sb.AppendLine( "        }" );
        sb.AppendLine( "        else" );
        sb.AppendLine( "        {" );
        sb.AppendLine( "            openMode = OpenSceneMode.Additive;" );
        sb.AppendLine( "        }" );
        sb.AppendLine( "        EditorSceneManager.OpenScene(path, openMode);" );
        sb.AppendLine( "    }" );
        sb.AppendLine( "" );

        int sceneIndex = 0;
        foreach (UnityEditor.EditorBuildSettingsScene scene in UnityEditor.EditorBuildSettings.scenes)
        {
            if (scene.enabled)
            {
                string sceneName = scene.path.Substring( scene.path.LastIndexOf( '/' ) + 1 );

                sb.AppendLine( "    [MenuItem(\"Scenes/" + sceneName + "\")]" );
                sb.AppendLine( "    public static void Scene" + sceneIndex + "()" );
                sb.AppendLine( "    {" );
                sb.AppendLine( "        OpenScene(\"" + scene.path + "\");" );
                sb.AppendLine( "    }" );
                sb.AppendLine( "" );

                sceneIndex++;
            }
        }

        sb.AppendLine( "}" );

        string path = "/Editor/";
        string fileName = "ScenesMenu.cs";
        string fullPath = Application.dataPath + path + fileName;

        //Check if path exists
        string[] pathFolders = path.Split( '/' );
        for (int i = 0; i < pathFolders.Length; i++)
        {
            string tempPath = "";

            for (int j = 0; j < i; j++)
            {
                tempPath += "/" + pathFolders[j];
            }

            if (!System.IO.Directory.Exists( Application.dataPath + tempPath + "/" + pathFolders[i] ))
            {
                System.IO.Directory.CreateDirectory( Application.dataPath + tempPath + "/" + pathFolders[i] );
            }
        }

        if (System.IO.File.Exists( fullPath ))
            System.IO.File.Delete( fullPath );

        System.IO.File.WriteAllText( fullPath, sb.ToString(), System.Text.Encoding.UTF8 );

        UnityEditor.AssetDatabase.Refresh();
    }
}
