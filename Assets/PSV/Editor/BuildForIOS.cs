﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;

public class BuildForIOS :MonoBehaviour
{


#if UNITY_IPHONE
	[MenuItem ( "HelpTools/Build for IOS" )]
	static void BuildGame ()
	{
		 Debug.LogWarning ( "Check if Assets/PSVPromoPlugin/Scripts/ServiceUtils.cs has no IOS_IsAppInstalled() method" );

		// Get filename
		string path = EditorUtility.SaveFolderPanel ( "Choose empty folder for XCode Project", "", "" );
		// Get scenes in build
		string [] levels = EditorBuildSettings.scenes.Where(_scene => _scene.enabled == true).Select(_scene => _scene.path).ToArray();

		string assets_path = Application.dataPath;

		HideAssets ( true, assets_path );

		AssetDatabase.Refresh();

		// Build Xcode project
		BuildPipeline.BuildPlayer ( levels, path, BuildTarget.iOS, BuildOptions.None );

		AssetDatabase.Refresh();

		HideAssets ( false, assets_path );

	}


	static string []
		folders_to_hide = new string []
		{
			"AdMobAndAnalytics/NeatPlug",
			"AdMobAndAnalytics/Editor",
			"Chartboost",
			"MobFox",
			"Plugins/InMobiAndroid",
		},
		files_to_hide = new string []
		{
			"Plugins/NP_AdmobAd_WP.dll",
		};

	static void HideAssets (bool hide, string assets_path)
	{
		string
			current_prefix = !hide ? "." : "",
			new_prefix = hide ? "." : "";

		ReplacePrefixInArray ( assets_path, folders_to_hide, current_prefix, new_prefix );
		ReplacePrefixInArray ( assets_path, files_to_hide, current_prefix, new_prefix );
	}


	static void ReplacePrefixInArray (string path, string [] arr, string old_pref, string new_pref)
	{
		foreach (string item in arr)
		{
			string old_item = AddPrefix ( item, old_pref );
			string new_item = AddPrefix ( item, new_pref );

			string old_item_path = path + "/" + old_item;
			string new_item_path = path + "/" + new_item;

			if (Directory.Exists ( old_item_path ))
			{
				Directory.Move ( old_item_path, new_item_path );
			}
			if (File.Exists ( old_item_path ))
			{
				File.Move ( old_item_path, new_item_path );
			}
		}
	}


	static string AddPrefix (string _string, string prefix)
	{
		string res = _string;
		if (!string.IsNullOrEmpty ( res ))
		{
			int name_start = res.LastIndexOf ( '/' ) + 1;
			if (!string.IsNullOrEmpty ( prefix ))
			{
				res = res.Insert ( name_start, prefix );
			}
		}
		return res;
	}

#endif
}
