﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Reflection;

//based on this https://gist.github.com/yuandra/8264869
public class TextureImportHelper :MonoBehaviour
{

	static int currentMaxTextureSize;
	static TextureImporterFormat currentTIFormat;
	static string logTitle = "ChangeTextureImportSettings. ";



	[MenuItem ( "HelpTools/Texture/Set optimized texture params" )]
	static void SetOptimalParamsWithout2048 ()
	{
		SelectedProcessWithOptimalSettings ( );
	}

	[MenuItem ( "HelpTools/Texture/Set optimized texture params +2048" )]
	static void SetOptimalParams ()
	{
		SelectedProcessWithOptimalSettings ( true );
	}

	[MenuItem ( "HelpTools/Texture/Set optimized texture params\tleave format" )]
	static void SetOptimalParamsWithOldFormatWithout2048 ()
	{
		SelectedProcessWithOptimalSettings ( leave_fornmat: true );
	}

	[MenuItem ( "HelpTools/Texture/Set optimized texture params +2048\tleave format" )]
	static void SetOptimalParamsWithOldFormat ()
	{
		SelectedProcessWithOptimalSettings ( true, true);
	}

	static void SelectedProcessWithOptimalSettings (bool include_2048 = false, bool leave_fornmat = false)
	{
		int processingTexturesNumber;
		Object [] originalSelection = Selection.objects;
		Object [] textures = GetSelectedTextures ( );
		Selection.objects = new Object [0]; //Clear selection (for correct data representation on GUI)
		processingTexturesNumber = textures.Length;
		if (processingTexturesNumber == 0)
		{
			Debug.LogWarning ( logTitle + "Nothing to do. Please select objects/folders with 2d textures in Project tab" );
			return;
		}
		AssetDatabase.StartAssetEditing ( );
		foreach (Texture2D texture in textures)
		{
			string path = AssetDatabase.GetAssetPath ( texture );
			TextureImporter textureImporter = AssetImporter.GetAtPath ( path ) as TextureImporter;
			if (textureImporter != null)
			{
				textureImporter.mipmapEnabled = false;
				textureImporter.maxTextureSize = GetOptimalSize ( textureImporter, include_2048 );
				if (!leave_fornmat)
				{
#if UNITY_5_5_OR_NEWER
				    textureImporter.textureCompression = TextureImporterCompression.Uncompressed;
#else
                    textureImporter.textureFormat = TextureImporterFormat.AutomaticTruecolor;
#endif
				}
				textureImporter.filterMode = FilterMode.Bilinear;
				//apply changes
				AssetDatabase.ImportAsset ( path, ImportAssetOptions.ForceUpdate );
			}
			else
			{
				processingTexturesNumber--;
				Debug.LogError ( logTitle + "Processing failed at " + path );
			}
		}
		AssetDatabase.StopAssetEditing ( );
		Selection.objects = originalSelection; //Restore selection
		Debug.Log ( logTitle + "Textures processed: " + processingTexturesNumber );
	}



	static int GetOptimalSize (TextureImporter importer, bool include_2048)
	{
		int [] sizes = new int [] { 32, 64, 128, 256, 512, 1024, 2048 };
		int res = 1024;
		int width, height;
		GetImageSize ( importer, out width, out height );
		int max_size = Mathf.Max ( width, height );
		for (int i = 0; i < sizes.Length; i++)
		{
			int limit = include_2048 ? 2048 : 1024;
			if (sizes [i] == limit)
			{
				res = sizes [i];
				break;
			}
			if (max_size <= sizes [i])
			{
				res = sizes [i];
				break;
			}
		}
		return res;
	}


	public static void GetImageSize (TextureImporter importer, out int width, out int height)
	{
		object [] args = new object [2] { 0, 0 };
		MethodInfo mi = typeof ( TextureImporter ).GetMethod ( "GetWidthAndHeight", BindingFlags.NonPublic | BindingFlags.Instance );
		mi.Invoke ( importer, args );

		width = (int) args [0];
		height = (int) args [1];
	}

	static Object [] GetSelectedTextures ()
	{
		return Selection.GetFiltered ( typeof ( Texture2D ), SelectionMode.DeepAssets );
	}

}
