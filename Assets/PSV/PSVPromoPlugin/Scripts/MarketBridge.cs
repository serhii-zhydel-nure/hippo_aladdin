﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PromoPlugin
{
    public static class MarketBridge
    {

        private const string
            android_makret_app_url = "market://details?id=",
            ios_market_app_url = "itms-apps://itunes.apple.com/app/",
            wp_market_app_url = "ms-windows-store:navigate?appid=";


        public static string GetAppMarketURL( string app_alias )
        {
            string res = "";
            switch (ServiceUtils.GetPlatform())
            {
                case 0:
                {
                    res = android_makret_app_url + app_alias;
                    break;
                }
                case 1:
                {
                    res = ios_market_app_url + app_alias;
                    break;
                }
                case 2:
                {
                    res = wp_market_app_url + app_alias;
                    break;
                }
            }
            return res;
        }



    }
}