﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using PSV;

/*
version 3 
    - implemented better platform override for testing capabilities 
    - implemented market page locale override according to language presets
    - implemented settings for disabling app name search
    - implemented version tracking
version 4
    - implemented announce for local data (needed for games with menu_scene(with ui_promo) going first (without splash screen))
    - added option to enable local announce
version 5
    - moved some settings to external class for better usability
    - added animated moreGames button for hippo games
    - due to need to scale buttons prefabs were et to bigger size
version 6
    - added standalone UI module with stationary set of buttons that can be placed anywhere
version 7
    - added promo box prefab with scrolling promo buttons
    - added label "ADS" at the top of wooden frame
version 8
    - changed algorithm of ReceivePromoSet: if there is no loaded data only one primary_set icon will be shown by PromoUI prefab
version 9
    - added compatibility for old prefabs
version 10
    - added compatibility for old prefabs
    - added label "ADS" at the top of wooden frame
version 12
    - updated for WSA compatibility
    - implemented namespace PromoModule
version 13
    - updated analytics manager to support firebase
version 14
	- implemented firebase mode and psv_server mode as switchable param through remoteconfig
	- implemented receiving promoset from firebase database using db_url got from remoteconfig
	- implemented receiving publisher name from remoteconfig
	- implemented promo_items_limit_param to get from remoteconfig (PromoAnimatedUI.cs)
	- splitted sprite-sheets into separate sprites and tagged them for SpritePacker (SettingsPanel, QuitDialogue, LangPannel)
	- updated push plugin - canvas only news prefab
	- push sprites are splitted in separate files
version 15
	- primary settings are deprecated
	- will give only what is cached from server
	- UI should not be active if promo_set has no items
version 16
    - moved get icon url logic to server. list of icon URLs is received from promo SRV
    - refactory
*/


namespace PromoPlugin
{

    public static class PromoModule
    {
        public static event System.Action<PackageData> OnPackageReceived;
        public static event System.Action<string> OnAccAliasReceived;


        public const int promo_version = 16;

        private const string
            last_promo_upd = "LastPromoUpd";

        //params

        public static bool
            debug = false;

        private const string
            promo_key = "PrOmOhAsH",
            host = "http://psvpromo.psvgamestudio.com/Scr/",
            promo_data_file = "promo_data.json";

        //variables
        private static bool
            downloads_in_progress = false,
            blocking_access = false;

        private const float
            promo_set_update_interval = 1;

        private const int
            max_retry_efforts = 5;


        private static string
            publisher = "";

        private static List<string>
            enqueued_budnles = new List<string>();

        //variables
        private static List<PromoItem>
            promo_set = new List<PromoItem>();


        private static Dictionary<string, Texture2D>
            icons_cache = new Dictionary<string, Texture2D>();

        [AwakeStatic]
        public static void AwakeStatic()
        {
            AwakeStaticAttribute.Done( typeof( PromoModule ) );
            if (!Directory.Exists( GetExternalPath() ))
            {
                Directory.CreateDirectory( GetExternalPath() );
            }
            CheckVersion();
            NextFrameCall.Add( LoadAtInitialization );
        }

        private static void CheckVersion()
        {
            int curent_version = PlayerPrefs.GetInt( "PromoVersion", -1 ); //due to all previous versions didn't save this value we use -1 by default for all versions bellow 3
                                                                           //fixes local data for versions below 3
            if (curent_version < promo_version)
            {
                //make some stuff to fix differences in saved data between versions

                //cleaning old cache to keep new behavior correct
                ClearLocalData();
                //reset last update date
                PlayerPrefs.SetString( last_promo_upd, "" );

                //save version of saved data
                PlayerPrefs.SetInt( "PromoVersion", promo_version );
                NextFrameCall.SavePrefs();
            }

        }

        private static void ClearLocalData()
        {
            RemoveFile( GetExternalPath() + promo_key );
        }

        private static void RemoveFile( string path )
        {
            if (File.Exists( path ))
            {
                File.Delete( path );
            }
        }

        private static void LoadAtInitialization()
        {
            //performing init before there will be requests to Instance
            LoadLocalResources();

            LoadPromoSet();
        }

        private static void LoadLocalResources()
        {
            string dat_path = GetExternalPath() + promo_data_file;
            if (File.Exists( dat_path ))
            {
                string _dat = File.ReadAllText( dat_path );

                //converting string to object
                PromoData data = ParsePromoData( _dat );

                if (data != null)

                {
                    LoadCachedIcons( data );
                    SetPublisher( data.pub );
                    InitPromoSet( data.promo_set );
                }
            }
        }

        private static void LoadCachedIcons( PromoData data )
        {
            for (int i = 0; i < data.promo_set.Length; i++)
            {
                string app_bundle = data.promo_set[i].app_bundle;
                string icon_path = GetExternalPath() + app_bundle + ".png";
                if (File.Exists( icon_path ))
                {
                    Texture2D icon = new Texture2D( 300, 300 );
                    icon.LoadImage( File.ReadAllBytes( icon_path ) );
                    CacheIcon( app_bundle, icon );
                }
            }
        }

        public static void LoadPromoSet( string bundle = "", int platform = -1, bool ignore_timer = false )
        {
            System.DateTime next_promo_update;
            System.DateTime.TryParse( PlayerPrefs.GetString( last_promo_upd, System.DateTime.Now.ToString() ), out next_promo_update );
            if (next_promo_update <= System.DateTime.Now || ignore_timer)
            {
                CoroutineHandler.Start( ReceivePromoSet( bundle, platform ) );
            }
        }



        private static void SetPublisher( string val )
        {
            if (!string.IsNullOrEmpty( val ))
            {
                publisher = val;
                if (debug)
                {
                    Debug.Log( "PromoModule: publisher=" + val );
                }
                if (OnAccAliasReceived != null)
                {
                    OnAccAliasReceived( val );
                }
            }
        }


        /// <summary>
        /// Initialize promoSet using local data or data received from DB (can be in pending state (receiving icons))
        /// </summary>
        /// <param name="set">Array of items to check if allowed</param>
        /// <param name="local">Param that tells us if items were taken from local storage or were received from DB, if UI module starts with PromoModule simultaneously use announce local to make changes visible after UI started</param>
        private static void InitPromoSet( PromoItem[] set )
        {
            var result = ServiceUtils.FilterPromoSets( set );
            if(result.Count > 0)
                CoroutineHandler.Start( SelectPromoItems( result ) );
        }


        /// <summary>
        /// create list of items selected in accordance to their weights
        /// </summary>     
        /// /// <param name="set">List of items to select for further processing</param>
        /// <param name="local">if we process local data and UI module starts with promo module we need to announce new data available to see icons after start (load of resources will take some time)</param>
        /// <returns></returns>
        private static IEnumerator SelectPromoItems( List<PromoItem> set )
        {
            List<PromoItem> p_selection = new List<PromoItem>();
            List<float> weights = new List<float>();
            List<int> w_ind = new List<int>();
            for (int i = 0; i < set.Count; i++)
            {
                weights.Add( set[i].GetPromoWeight() );
                w_ind.Add( i );
            }
            for (int i = 0; i < set.Count; i++)
            {
                if (weights.Count > 0)
                {
                    int selection = WeightedRandom.RandomW( weights.ToArray() );
                    p_selection.Add( set[w_ind[selection]] );
                    weights.RemoveAt( selection );
                    w_ind.RemoveAt( selection );
                }
            }
            while (blocking_access)
            {
                yield return null;
            }

            //store new promoset
            BlockAccess( true );

            promo_set = p_selection;

            BlockAccess( false );

            EnqueDownloads();

            AnnounceAvailableItems();

        }


        /// <summary>
        /// Creates queue of items to load from promoSet if icon is 
        /// </summary>
        private static void EnqueDownloads()
        {
            List<PackageInfo> queue = new List<PackageInfo>();
            if (!blocking_access)
            {
                BlockAccess( true );
                for (int i = 0; i < promo_set.Count; i++)
                {
                    string bundle = promo_set[i].app_bundle;

                    if (!enqueued_budnles.Contains( bundle ))
                    {
                        enqueued_budnles.Add( bundle );

                        queue.Add( new PackageInfo( bundle, promo_set[i].icon_url ) );
                    }
                }
                BlockAccess( false );
            }
            if (queue.Count > 0)
            {
                CoroutineHandler.Start( GetIcons( queue ) );
            }
        }


        private static IEnumerator GetIcons( List<PackageInfo> queue )
        {
            while (downloads_in_progress)
            {
                //will wait till previously started downloads are complete
                yield return null;
            }
            downloads_in_progress = true;
            for (int i = 0; i < queue.Count; i++)
            {
                yield return LoadAppIcon( queue[i], CacheIcon );
            }
            downloads_in_progress = false;
        }



        private delegate void IconLoaded( string app_bundle, Texture2D icon );

        private static IEnumerator LoadAppIcon( PackageInfo package_inf, IconLoaded on_complete )
        {
            Texture2D icon = null;
            if (package_inf != null && !string.IsNullOrEmpty( package_inf.icon_url ))
            {
                using (WWW www = new WWW( package_inf.icon_url ))
                {
                    yield return www;
                    if (string.IsNullOrEmpty( www.error ))
                    {
                        if (www.texture.width >= 100f)
                            icon = www.texture;
                    }
                }
            }
            if (icon != null)
            {
                if (on_complete != null)
                {
                    on_complete( package_inf.app_bundle, icon );
                }
            }
        }




        public static string GetPublisher()
        {
            return publisher;
        }




        private static void SetNextPromoUpdate()
        {
            PlayerPrefs.SetString( last_promo_upd, System.DateTime.Now.AddDays( promo_set_update_interval ).AddHours( Random.Range( 0, 23 ) ).ToString() );
            NextFrameCall.SavePrefs();
        }


        private static void BlockAccess( bool param )
        {
            blocking_access = param;
        }




        /// <summary>
        /// Receiving Promo set from DB depending on bundleID and platform
        /// </summary>
        /// <param name="b">bundle id as param for DB request</param>
        /// <param name="p">platform as param for DB request</param>
        /// <returns></returns>
        private static IEnumerator ReceivePromoSet( string b, int p )
        {

            string bundle_id = b == "" ? ServiceUtils.GetAppBundle() : b;
            string platform = p < 0 ? ServiceUtils.GetPlatform().ToString() : p.ToString();
            string locale = Languages.GetLanguage().ToString();
            string hash = Crypto.Md5Sum( bundle_id + platform + locale + promo_key );

            string url = host + "getpromosetjson.php?bundle=" + WWW.EscapeURL( bundle_id ) + "&platform=" + WWW.EscapeURL( platform ) + "&locale=" + locale + "&hash=" + hash;

            if (debug)
                Debug.Log( "PromoModule: ReceivePromoSet " + url );

            int attempts = 0;
            using (WWW www = new WWW( url ))
            {
                do
                {
                    if (attempts > 0)
                    {
                        //timeout before next request
                        yield return new WaitForSeconds( 5 );
                    }
                    attempts++;
                    yield return www;

                } while (!string.IsNullOrEmpty( www.error ) && attempts < max_retry_efforts);
                if (!string.IsNullOrEmpty( www.error ))
                {
                    if (debug)
                        Debug.LogWarning( "PromoModule: Receive PromoSet from PSV server failed: " + www.error );
                }
                else
                {
                    SavePromoData( www.text );

                    if (debug)
                    {
                        Debug.Log( "PromoModule: Received PromoSet: " + www.text );
                    }
                    PromoData data = ParsePromoData( www.text );
                    if (data != null)
                    {
                        SetPublisher( data.pub );
                        InitPromoSet( data.promo_set );
                    }
                }
            }
        }

        [ContextMenu( "test" )]
        private static void test()
        {
            PromoData data = new PromoData( new PromoItem[3], "pub_acc" );
            Debug.Log( JsonUtility.ToJson( data ) );
        }

        private static PromoData ParsePromoData( string raw_set )
        {
            if (string.IsNullOrEmpty( raw_set ))
                return null;
            try
            {
                //TODO: check on WindowsPhone
                PromoData data = JsonUtility.FromJson<PromoData>( raw_set );
                return data;
            }
            catch (System.Exception e)
            {
                Debug.LogError( "PromoModule: ParsePromoSet failed: " + e.Message );
                return null;
            }
        }



        private static void SavePromoData( string data )
        {
            //save promo_set
            if (data.Length > 0)
            {
                if (!Directory.Exists( GetExternalPath() ))
                {
                    Directory.CreateDirectory( GetExternalPath() );
                }
                File.WriteAllText( GetExternalPath() + promo_data_file, data );
                SetNextPromoUpdate();
            }
        }




        private static void CacheIcon( string bundle, Texture2D icon )
        {
            if (!icons_cache.ContainsKey( bundle ))
            {
                SaveIcon( icon, bundle );
                icons_cache.Add( bundle, icon );
                if (enqueued_budnles.Remove( bundle ))
                {
                    AnnounceAvailableItem( bundle );
                }
            }
        }

        private static void AnnounceAvailableItem( string bundle )
        {
            PromoItem p_item = promo_set.Find( X => X.app_bundle == bundle );
            if (p_item != null)
            {
                AnnounceAvailablePackage( p_item );
            }
        }


        /// <summary>
        /// Tells subscribers that new data is available
        /// </summary>
        private static void AnnounceAvailablePackage( PromoItem p_item )
        {
            PackageData package = GetPackageData( p_item );
            if (package != null && OnPackageReceived != null)
            {

                OnPackageReceived( package );
            }
        }



        private static void AnnounceAvailableItems()
        {
            if (!blocking_access)
            {
                BlockAccess( true );
                for (int i = 0; i < promo_set.Count; i++)
                {
                    AnnounceAvailablePackage( promo_set[i] );
                }
                BlockAccess( false );
            }
        }



        public static PackageData[] GetPromoItems( int required_items )
        {
            List<PackageData> res = new List<PackageData>();
            if (!blocking_access)
            {
                BlockAccess( true );
                for (int i = 0; i < promo_set.Count; i++)
                {
                    PackageData item = GetPackageData( promo_set[i] );
                    if (item != null && item.app_icon != null)
                    {
                        res.Add( item );
                    }
                    //limit items count to required
                    if (res.Count == required_items)
                        break;
                }
                BlockAccess( false );
            }

            return res.ToArray();
        }


        private static PackageData GetPackageData( PromoItem p_item )
        {
            if (icons_cache.ContainsKey( p_item.app_bundle ))
            {
                return new PackageData( p_item, icons_cache[p_item.app_bundle] );
            }
            return null;
        }



        private static void SaveIcon( Texture2D icon, string app_bundle )
        {
            File.WriteAllBytes( GetExternalPath() + app_bundle + ".png", icon.EncodeToPNG() );
        }


        public static string GetExternalPath()
        {
            return Application.persistentDataPath + "/PSVPromo/";
        }


        private class PromoData
        {
            public PromoItem[] promo_set;
            public string pub;

            public PromoData( PromoItem[] promo_set, string pub )
            {
                this.promo_set = promo_set;
                this.pub = pub;
            }

            public new string ToString()
            {
                if (promo_set == null) return "NULL";
                return "PromoData: [" + promo_set.ConvertToString() + " \npub=" + pub + "]";
            }
        }

    }
}
