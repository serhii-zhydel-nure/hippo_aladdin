﻿using UnityEngine;
using System.Collections;

namespace PromoPlugin
{
    [System.Serializable]
    public class PromoItem
    {
        public string
            app_bundle;

        public string
            app_alias;

        public string
            app_name;

        public double
            app_weight;

        public string
            icon_url;


        public float GetPromoWeight ()
        {
            return (float) app_weight;
        }

        public PromoItem (string bundle = "", string alias = "", float weight = 0, string name = "", string icon = "")
        {
            app_alias = alias;
            app_bundle = bundle;
            app_name = name;
            app_weight = weight;
            icon_url = icon;
        }


        public new string ToString()
        {
            return "PromoItem ['" + app_bundle + "', '" + app_alias + "', '" + app_name + "', '" + app_weight + "', '" + icon_url + "']";
        }
    }
}