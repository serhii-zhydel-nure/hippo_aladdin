﻿using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace PromoPlugin
{
    public class MovableIcon : MovableObject
    {
        List<PromoButton> icons = new List<PromoButton>();
        PromoAnimatedUI promo_animator;

        private void Awake()
        {
            promo_animator = GetComponentInParent<PromoAnimatedUI>();
        }

        protected override void Start()
        {
            FixIconSize();
            base.Start();
            CacheItems();
        }

        //will make its size enough to keep visible only one icon at the same time while animation active
        void FixIconSize()
        {
            obj.GetComponent<Image>().preserveAspect = true;
            if (direction == Directions.Vertical)
            {
                float min_interval = mask.rect.height + obj.rect.width;
                if (obj.rect.height < min_interval)
                {
                    obj.SetSizeWithCurrentAnchors( RectTransform.Axis.Vertical, min_interval );
                }
            }
            else
            {
                float min_interval = mask.rect.width + obj.rect.height;
                if (obj.rect.width < min_interval)
                {
                    obj.SetSizeWithCurrentAnchors( RectTransform.Axis.Horizontal, min_interval );
                }
            }
        }

        void CacheItems()
        {
            for (int i = icons.Count; i < items_to_move.Count; i++)
            {
                icons.Add( items_to_move[i].GetComponentInChildren<PromoButton>() );
            }
        }

        protected override void ItemRepositioned( int index )
        {
            CacheItems(); //will check if new icons were spawned and will cache their image component
            if (promo_animator != null)
            {
                promo_animator.OnItemRepositioned( icons[index], icons.Count );
            }
            else
            {
                Debug.LogError( "PromoAnimatedUI not found on scene" );
            }
        }

    }
}
