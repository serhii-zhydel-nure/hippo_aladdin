﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using PSV;
using System;
using UnityEngine.Events;

namespace PromoPlugin
{
    public class PromoAnimatedUI : PromoUI
    {
        [Header( "Scroll settings" )]
        private float
            scroll_inertia = 0.3f;
        private float
            swipe_threshold = 20,
            max_swipe_forse = 10,
            scroll_acceleration = 15f,
            scroll_speed = 25f;

        private Vector2
            touch_pos = Vector2.zero;
        private bool
            focus = false;
        private float
            my_time_scale = 1;
        public bool
            use_swipe = true;

        [Header( "Objects" )]
        public RectTransform
            scroller;

        private IMovable[]
            movable_objects;

        protected override void Awake()
        {
            base.Awake();
            movable_objects = GetComponentsInChildren<IMovable>( true );
            Canvas c = GetComponentInParent<Canvas>();
            if (c != null) { c.pixelPerfect = false; }
        }
        
        void Update()
        {
            if (!focus && my_time_scale != 1)
            {
                my_time_scale = Mathf.Lerp( my_time_scale, Mathf.Sign( my_time_scale ), Time.deltaTime * scroll_inertia );
                if (Mathf.Abs( my_time_scale ) < 1.01f)
                    my_time_scale = Mathf.Sign( my_time_scale );
            }
            UpdateSpeed();
        }

        public void OnPointerDown( BaseEventData e )
        {
            focus = true;
            PointerEventData pointerData = e as PointerEventData;
            Vector2 point;
            RectTransformUtility.ScreenPointToLocalPointInRectangle( scroller, pointerData.position, Camera.main, out point );
            if (use_swipe)
            {
                //for swipe
                touch_pos = point;
            }
            else
            {
                //for touch control
                my_time_scale = -Mathf.Clamp( point.y, scroller.rect.yMin, scroller.rect.yMax ) / scroller.rect.yMax * scroll_acceleration;
            }
        }

        public void OnDrag( BaseEventData e )
        {
            if (focus)
            {
                PointerEventData pointerData = e as PointerEventData;
                Vector2 point;
                RectTransformUtility.ScreenPointToLocalPointInRectangle( scroller, pointerData.position, Camera.main, out point );
                if (use_swipe)
                {
                    //for swipe
                    ProcessSwipe( point );
                }
                else
                {
                    //for touch control
                    my_time_scale = -Mathf.Clamp( point.y, scroller.rect.yMin, scroller.rect.yMax ) / scroller.rect.yMax * scroll_acceleration;
                }
            }
        }

        void ProcessSwipe( Vector2 point )
        {
            float swipe_dist = Vector2.Distance( point, touch_pos );
            if (swipe_dist > swipe_threshold)
            {
                focus = false;
                my_time_scale = Mathf.Clamp( Mathf.Clamp( (scroll_acceleration * 0.1f * (swipe_dist / swipe_threshold)), 0, scroll_acceleration ) * Mathf.Sign( point.y - touch_pos.y ) + my_time_scale, -max_swipe_forse, max_swipe_forse );
            }
        }

        public void OnPointerUp( BaseEventData e )
        {
            if (focus)
            {
                PointerEventData pointerData = e as PointerEventData;
                Vector2 point;
                RectTransformUtility.ScreenPointToLocalPointInRectangle( scroller, pointerData.position, Camera.main, out point );
                ProcessSwipe( point );
                focus = false;
            }
        }

        private PackageData GetItem( int index )
        {
            return items[items_order[index]];
        }

        private int
            head = 0, //by default head is the first item in the order
            tail = -1; //as long as new items with index -1 are be spawned tail will be updated

        public void OnItemRepositioned( PromoButton item, int total )
        {
            int index = item.GetIndex();
            if (index < 0)
            {
                index = tail = FixIndex( tail + 1 );
            }
            else
            {
                int sign = (int) Mathf.Sign( saved_speed );
                bool two_or_more = total > 1 && items_order.Length > 1;
                if (head != tail || (two_or_more && sign < 0))
                {
                    head = FixIndex( head + sign );
                }
                if (head != tail || (two_or_more && sign > 0))
                {
                    tail = FixIndex( tail + sign );
                }
                index = sign > 0 ? tail : head;
            }
            item.Init( GetItem( index ), index );
        }


        private int FixIndex( int index )
        {
            return (int) Mathf.Repeat( index, items_order.Length );
        }

        float saved_speed = 0;

        void UpdateSpeed()
        {
            float new_speed = scroll_speed * my_time_scale * Time.deltaTime;
            if (new_speed != saved_speed)
            {
                saved_speed = new_speed;

                if (movable_objects != null)
                {
                    for (int i = 0; i < movable_objects.Length; i++)
                    {
                        movable_objects[i].SetSpeed( saved_speed );
                    }
                }
            }
        }

        protected override void OnPackageReceived( PackageData data )
        {
            base.OnPackageReceived( data );
            RefreshUI();
        }

        protected override void RefreshUI()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild( i ).gameObject.SetActive( true );
            }
            base.RefreshUI();
        }
    }
}