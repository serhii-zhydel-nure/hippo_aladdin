﻿using UnityEngine;
using System.Collections;


namespace PromoPlugin
{
    [RequireComponent( typeof( RectTransform ) )]
    public class RotatableObject : MonoBehaviour, IMovable
    {
        private RectTransform
            rect;

        [SerializeField]
        private float
            speed = 0,
            acceleration = 1;

        private Quaternion rotate_per_frame;

        private void Awake()
        {
            rect = GetComponent<RectTransform>();
            if (!rect)
                enabled = false;
            SetSpeed( speed );
        }

        private void Update()
        {
            if(speed != 0.0f)
                rect.localRotation *= rotate_per_frame;
        }

        public void SetSpeed( float speed )
        {
            this.speed = speed * acceleration;
            rotate_per_frame = Quaternion.Euler( new Vector3( 0.0f, 0.0f, this.speed ) );
        }
    }
}
