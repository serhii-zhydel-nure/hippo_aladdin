﻿using UnityEngine;
using PSV;
using System.Collections.Generic;

namespace PromoPlugin
{

    /// <summary>
    /// Takes one item that will move in a set direction. Will spawn extra clones of this item according to direction to fill the mask rect
    /// </summary>
    public class MovableObject : MonoBehaviour, IMovable
    {
        public enum Directions
        {
            Horizontal,
            Vertical
        }

        public Directions
            direction;

        //object to move (will be cloned to fill the area it will be moving in)
        public RectTransform
            obj;
        
        //mask the object is located in
        public RectTransform
            mask;

        protected List<RectTransform>
            items_to_move = null;

        //values of array items indexes for corresponding corners
        private const int
            bottom_left_corner = 0,
            top_left_corner = 1,
            top_right_corner = 2,
            bottom_right_corner = 3;

        [SerializeField]
        private float
            speed = 0,
            acceleration = 1;

        protected virtual void Start()
        {
            SetObjInitialPosition();
            items_to_move = new List<RectTransform> { obj };
            ItemRepositioned( 0 );
            FillInMissingObjects();
        }

        void FillInMissingObjects()
        {
            Vector3[] object_corners = new Vector3[4];
            obj.GetWorldCorners( object_corners );
            Vector3[] mask_corners = new Vector3[4];
            mask.GetWorldCorners( mask_corners );

            float dist = 0;

            Vector2 dir;

            if (direction == Directions.Horizontal)
            {
                dir = Vector2.left * GetWidth( obj );
                dist = GetWidth( mask ); ;
            }
            else
            {
                dir = Vector2.down * GetHeight( obj );
                dist = GetHeight( mask );
            }

            SpawnClones( dist, dir );
        }

        //sets object to the bottom/left side
        private void SetObjInitialPosition()
        {
            Vector2 pos = obj.position;
            if (direction == Directions.Horizontal)
            {
                pos.x = GetObjRightPos();
            }
            else
            {
                pos.y = GetObjTopPos();
            }
            obj.position = pos;
        }

        private int GetNecessaryItems( float dist, float dimention )
        {
            return Mathf.Clamp( Mathf.CeilToInt( (dist - dimention * 0.5f) / dimention ), 1, int.MaxValue );
        }

        private int SpawnClones( float dist, Vector2 dimention )
        {
            return SpawnClones( GetNecessaryItems( dist, dimention.magnitude ), dimention );
        }


        private int SpawnClones( int count, Vector2 dimention )
        {
            for (int i = 0; i < count; i++)
            {
                Vector3 pos = obj.position;
                pos.x += dimention.x * (i + 1);
                pos.y += dimention.y * (i + 1);
                AddObjClone( pos );
            }
            return count;
        }

        //Spawns clone of object at given position
        private void AddObjClone( Vector3 a_pos )
        {
            GameObject clone = Instantiate( obj.gameObject);
            clone.transform.SetParent( obj.transform.parent );
            clone.transform.localScale = obj.transform.localScale;
            clone.transform.rotation = obj.transform.rotation;
            clone.transform.position = a_pos;
            RectTransform rt = clone.GetComponent<RectTransform>();
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, obj.rect.width);
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, obj.rect.height );
            items_to_move.Add( rt );
            ItemRepositioned( items_to_move.Count - 1 );
        }

        protected virtual void Update()
        {
            Move( speed * acceleration );
        }

        //changes the speed of line movement
        public void SetSpeed( float speed )
        {
            this.speed = speed;
        }


        //applies shift to items in line 
        private void Move( float speed )
        {
            if (speed != 0 && items_to_move != null)
            {
                Vector2 offset = (direction == Directions.Horizontal ? Vector2.right : Vector2.up) * speed;
                Vector3[] mask_corners = new Vector3[4];
                mask.GetWorldCorners( mask_corners );

                for (int i = 0; i < items_to_move.Count; i++)
                {
                    Vector2 pos = items_to_move[i].anchoredPosition + offset;
                    items_to_move[i].anchoredPosition = pos;
                    CheckBounds( i, mask_corners, offset );
                }
            }
        }

        //is called when item is spawned or placed at the opposite side of line
        protected virtual void ItemRepositioned( int index )
        {

        }

        private void CheckBounds( int i, Vector3[] frame_corners, Vector2 offset )
        {
            if (mask != null)
            {
                Vector3[] object_corners = new Vector3[4];
                items_to_move[i].GetWorldCorners( object_corners );
                bool is_out = false;
                if (offset.x != 0)
                {
                    if (offset.x > 0)
                    {
                        is_out |= IsOutRight( object_corners, frame_corners );
                    }
                    else
                    {
                        is_out |= IsOutLeft( object_corners, frame_corners );
                    }
                }
                if (offset.y != 0)
                {
                    if (offset.y > 0)
                    {
                        is_out |= IsOutTop( object_corners, frame_corners );
                    }
                    else
                    {
                        is_out |= IsOutBottom( object_corners, frame_corners );
                    }
                }
                if (is_out)
                {
                    Vector2 dir = offset.normalized * -1;
                    dir = GetDistBetweenItems( dir ) * items_to_move.Count;
                    Vector3 pos = items_to_move[i].position;
                    pos.x += dir.x;
                    pos.y += dir.y;
                    items_to_move[i].position = pos;
                    ItemRepositioned( i );
                }
            }
        }

        private Vector2 GetDistBetweenItems( Vector2 dir )
        {
            dir.y *= GetHeight( obj );
            dir.x *= GetWidth( obj );
            return dir;
        }

        private float GetObjLeftPos()
        {
            return GetLeftPos( mask ) + GetDistToLeft( obj );
        }

        private float GetObjRightPos()
        {
            return GetRightPos( mask ) - GetDistToRight( obj );
        }

        private float GetObjBottomPos()
        {
            return GetBottomPos( mask ) + GetDistToBottom( obj );
        }

        private float GetObjTopPos()
        {
            return GetTopPos( mask ) - GetDistToTop( obj );
        }

        #region RectTransform Utilities

        private float GetDistToLeft( RectTransform rt )
        {
            return GetWidth( rt ) * rt.pivot.x;
        }

        private float GetDistToRight( RectTransform rt )
        {
            float w = GetWidth( rt );
            return w - w * rt.pivot.x;
        }

        private float GetDistToBottom( RectTransform rt )
        {
            return GetHeight( rt ) * rt.pivot.y;
        }

        private float GetDistToTop( RectTransform rt )
        {
            float h = GetHeight( rt );
            return h - h * rt.pivot.y;
        }

        private float GetLeftPos( RectTransform rt )
        {
            return rt.position.x - GetDistToLeft( rt );
        }

        private float GetRightPos( RectTransform rt )
        {
            return rt.position.x + GetDistToRight( rt );
        }

        private float GetBottomPos( RectTransform rt )
        {
            return rt.position.y - GetDistToBottom( rt );
        }

        private float GetTopPos( RectTransform rt )
        {
            return rt.position.y + GetDistToTop( rt );
        }

        private float GetWidth( RectTransform rt )
        {
            return rt.rect.width * rt.lossyScale.x;
        }

        private float GetHeight( RectTransform rt )
        {
            return rt.rect.height * rt.lossyScale.y;
        }

        #endregion

        #region check if rect lays outside mask

        private bool IsOutBottom( Vector3[] obj, Vector3[] mask )
        {
            return mask[bottom_left_corner].y > obj[top_left_corner].y;
        }

        private bool IsOutTop( Vector3[] obj, Vector3[] mask )
        {
            return mask[top_left_corner].y < obj[bottom_left_corner].y;
        }

        private bool IsOutRight( Vector3[] obj, Vector3[] mask )
        {
            return mask[top_right_corner].x < obj[top_left_corner].x;
        }

        private bool IsOutLeft( Vector3[] obj, Vector3[] mask )
        {
            return mask[top_left_corner].x > obj[top_right_corner].x;
        }

        #endregion

    }


}