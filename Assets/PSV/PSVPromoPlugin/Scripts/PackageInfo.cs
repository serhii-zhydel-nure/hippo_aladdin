﻿using UnityEngine;
using System.Collections;


namespace PromoPlugin
{
	[System.Serializable]
    public class PackageInfo
    {

        public string
            app_bundle;

        public string
            icon_url;

        public PackageInfo (string bundle = "", string url = "")
        {
            app_bundle = bundle;
            icon_url = url;
        }
    }

}