﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV;
using UnityEngine.EventSystems;

namespace PromoPlugin
{
    public class PromoUI : MonoBehaviour, IExternalLink
    {
        //params
        private int
            max_buttons_to_show = 10;
        private float
            duplicates_portion = 0.25f;

        //variables
        protected PackageData[]
            items = new PackageData[0];
        protected int[]
            items_order = new int[0];

        protected CanvasGroup
            canvas_group;


        protected bool is_initialized = false;
        protected bool is_active = false;

        protected virtual void Awake()
        {
            if (!Application.isEditor && !Application.isMobilePlatform)
            {
                gameObject.SetActive( false );
                return;
            }
            canvas_group = GetComponent<CanvasGroup>();
            if (canvas_group == null)
            {
                canvas_group = gameObject.AddComponent<CanvasGroup>();
            }

            Show( false );
        }


        protected virtual void OnEnable()
        {
            if (Application.isEditor || Application.isMobilePlatform)
            {
                Init();
                Subscribe();
            }
        }

        protected virtual void OnDisable()
        {
            if (Application.isEditor || Application.isMobilePlatform)
            {
                Unsubscribe();
                DestroyLink();
            }
        }

        protected virtual void OnPackageReceived( PackageData data )
        {
            if (!PackageExists( data ))
            {
                TryReorderItems( data );
            }
        }


        private bool PackageExists( PackageData data )
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i].app_bundle == data.app_bundle)
                {
                    return true;
                }
            }
            return false;
        }

        private void Subscribe()
        {
            PromoModule.OnPackageReceived += OnPackageReceived;
        }

        private void Unsubscribe()
        {
            PromoModule.OnPackageReceived -= OnPackageReceived;
        }



        void Init()
        {
            TryReorderItems( PromoModule.GetPromoItems( max_buttons_to_show ) );
        }


        protected virtual void RefreshUI()
        {
            CreateLink();
        }

        private PackageData GetItem( int index )
        {
            return items[items_order[index]];
        }




        List<PackageData> items_queue = new List<PackageData>();


        private void TryReorderItems( PackageData[] _items )
        {
            if (!reorder_busy && items_queue.Count > 0)
            {
                Debug.LogError( "TryReorderItems: reorder_busy = " + reorder_busy + ", but items_queue still has " + items_queue.Count + " items - clearing queue" );
                items_queue.Clear();
            }

            items_queue.AddRange( _items );

            ProcessItemsQueue();
        }

        private void TryReorderItems( PackageData _item )
        {
            if (!reorder_busy && items_queue.Count > 0)
            {
                Debug.LogError( "TryReorderItems: reorder_busy = " + reorder_busy + ", but items_queue still has " + items_queue.Count + " items - clearing queue" );
                items_queue.Clear();
            }

            items_queue.Add( _item );

            ProcessItemsQueue();
        }

        void ProcessItemsQueue()
        {
            if (!reorder_busy && items_queue.Count > 0)
            {
                StartCoroutine( ReorderItems( items_queue.ToArray(), ProcessItemsQueue ) );
                items_queue.Clear();
            }
        }

        void StoreItems( PackageData[] _items, int[] _items_order )
        {
            if (_items.Length > 0 && _items_order.Length > 0)
            {
                items = _items;
                items_order = _items_order;
                PrintItemsOrder();
                RefreshUI();
            }
        }


        void PrintItemsOrder()
        {
            if (PromoModule.debug)
            {
                string log = "<color=green>PromoUI: items_order</color>";
                for (int i = 0; i < items_order.Length; i++)
                {
                    log += "\n" + i + " " + items[items_order[i]].app_bundle;
                }
                Debug.LogWarning( log );
            }
        }

        bool reorder_busy = false;
        IEnumerator ReorderItems( PackageData[] _items, System.Action on_complete )
        {
            reorder_busy = true;
            List<int> _items_order = new List<int>();
            float max_weight = float.MinValue;
            List<int> repeatings = new List<int>();
            //get max weigh values
            for (int i = 0; i < _items.Length; i++)
            {
                float app_weight = _items[i].app_weight;

                if (app_weight > max_weight)
                    max_weight = app_weight;
            }
            //get max repeating items for max weight value
            int max_repeat = Mathf.CeilToInt( _items.Length * duplicates_portion );
            //count repeatings for each item
            for (int i = 0; i < _items.Length; i++)
            {
                int q = Mathf.CeilToInt( Mathf.Clamp( (_items[i].app_weight) / max_weight * max_repeat, 1, max_repeat ) );
                repeatings.Add( q );
            }
            //store each item in order list
            for (int i = 0; i < _items.Length; i++)
            {
                _items_order.Add( i );
            }
            //spawn duplicates of items
            for (int i = 0; i < repeatings.Count; i++)
            {
                int rep = repeatings[i];
                for (int j = 0; j < rep; j++)
                {
                    _items_order.Add( i );
                }
            }

            _items_order = _items_order.Shuffle();

            StoreItems( _items, _items_order.ToArray() );
            yield return null;

            reorder_busy = false;
            if (on_complete != null)
            {
                on_complete();
            }
        }


        void PrintPromoSet( PackageData[] set )
        {
            string res = "PromoUI PrintPromoSet items count = " + set.Length + "\n";
            for (int i = 0; i < set.Length; i++)
            {
                res += "| " + set[i].app_bundle + " | " + set[i].app_alias + " | " + set[i].app_name + "|\n";
            }
            Debug.Log( "PromoModule: " + res );
        }


        #region External link implementation
        public bool IsStatic
        {
            get
            {
                return true;
            }
        }

        public void Show( bool param )
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild( i ).gameObject.SetActive( param );
            }
        }

        public void CreateLink()
        {
            if (!is_active)
            {
                is_active = true;
                Show( true );
                ExternalLinksManager.AddLink( this );
            }
        }

        public void DestroyLink()
        {
            if (is_active)
            {
                ExternalLinksManager.DeleteLink( this );
                is_active = false;
            }
        }
        #endregion
    }
}