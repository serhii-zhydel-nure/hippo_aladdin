﻿#define Prototype_4_2_OR_NEWER

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
using System.Security.Cryptography;
using UnityEngine.Windows;
using UnityEngine.WindowsPhone;
#endif

namespace PromoPlugin
{
    public static class WeightedRandom
    {
        public static float [] CalcLookups (float [] weights)
        {
            float total_weight = 0;
            for (int i = 0; i < weights.Length; i++)
            {
                total_weight += weights [i];
            }
            float [] lookups = new float [weights.Length];
            for (int i = 0; i < weights.Length; i++)
            {
                lookups [i] = (weights [i] / total_weight) + (i == 0 ? 0 : lookups [i - 1]);
            }
            return lookups;
        }

        private static int Binary_search (float needle, float [] lookups)
        {
            int high = lookups.Length - 1;
            int low = 0;
            int probe = 0;
            if (lookups.Length < 2)
            {
                return 0;
            }
            while (low < high)
            {
                probe = (int) ((high + low) / 2);

                if (lookups [probe] < needle)
                {
                    low = probe + 1;
                }
                else if (lookups [probe] > needle)
                {
                    high = probe - 1;
                }
                else
                {
                    return probe;
                }
            }

            if (low != high)
            {
                return probe;
            }
            else
            {
                return (lookups [low] >= needle) ? low : low + 1;
            }
        }


        public static int RandomW (float [] weights)
        {
            weights = CalcLookups ( weights );

            if (weights.Length > 0)
                return Binary_search ( Random.value, weights );
            else
                return -1;
        }

    }



    public static class Crypto
    {
#if UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
    public static string Md5Sum (string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding ( );
        byte [] bytes = ue.GetBytes ( strToEncrypt );

        byte [] hashBytes = Crypto.ComputeMD5Hash ( bytes );
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString ( hashBytes [i], 16 ).PadLeft ( 2, '0' );
        }

        return hashString.PadLeft ( 32, '0' );
    }
#else
        public static string Md5Sum (string strToEncrypt)
        {
            System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding ( );
            byte [] bytes = ue.GetBytes ( strToEncrypt );
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider ( );
            byte [] hashBytes = md5.ComputeHash ( bytes );
            string hashString = "";
            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += System.Convert.ToString ( hashBytes [i], 16 ).PadLeft ( 2, '0' );
            }
            return hashString.PadLeft ( 32, '0' );
        }
#endif
    }



    public static class ServiceUtils
    {
        private static int
               platform_override = -1;

        public const int ANDROID_PLATFORM = 0;
        public const int IOS_PLATFORM = 1;

        public static List<PromoItem> FilterPromoSets( PromoItem[] input_set )
        {
            List<PromoItem> res = new List<PromoItem>( input_set.Length);
            if (input_set == null || input_set.Length == 0)
                return res;

            string selfBundle = GetAppBundle();
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                using (AndroidJavaClass up = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ))
                {
                    using (AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>( "currentActivity" ))
                    {
                        using (AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>( "getPackageManager" ))
                        {
                            if (packageManager == null || packageManager.GetRawObject() == System.IntPtr.Zero)
                                return res; // Not found PackageManager then skip
                            for (int i = 0; i < input_set.Length; i++)
                            {
                                if (input_set[i].GetPromoWeight() <= 0 || selfBundle.Equals( input_set[i].app_bundle ))
                                    continue;

                                AndroidJavaObject launchIntent = null;
                                bool isInstalled = false;
                                //if the app is installed, no errors. Else, doesn't get past next line
                                try
                                {
                                    launchIntent = packageManager.Call<AndroidJavaObject>( "getLaunchIntentForPackage", input_set[i].app_bundle );
                                    //        
                                    //        ca.Call("startActivity",launchIntent);
                                    isInstalled = launchIntent != null && launchIntent.GetRawObject() != System.IntPtr.Zero;
                                }
                                catch { }
                                finally
                                {
                                    if (launchIntent != null)
                                        launchIntent.Dispose();
                                }

                                if (!isInstalled)
                                    res.Add( input_set[i] );

                                if (PromoModule.debug)
                                    Debug.Log( input_set[i].app_bundle + " installed = " + isInstalled );
                            }
                        }
                    }
                }
                break;
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsEditor:
                res.AddRange( input_set );
                break;
            }

            return res;
        }
        
        public static void OverridePlatform (int target = -1)
        {
            platform_override = target;
        }

        public static int GetPlatform ()
        {
            int res;
            switch (Application.platform)
            {
                case RuntimePlatform.IPhonePlayer:
                res = IOS_PLATFORM;
                break;
                default: //case RuntimePlatform.Android:
                res = ANDROID_PLATFORM;
                break;
            }
            return platform_override < 0 ? res : platform_override;
        }


        public static string GetPlatformName (int p = -1)
        {
            string res = "";
			int _p = p < 0 ? GetPlatform ( ) : p; //get name of defined platform or define current platform
			switch (_p)
            {
                case ANDROID_PLATFORM:
                    {
                        res = "Android";
                        break;
                    }
                case IOS_PLATFORM:
                    {
                        res = "IOS";
                        break;
                    }
                case 2:
                    {
                        res = "WP";
                        break;
                    }
            }
            return res;
        }


        public static string GetAppBundle ()
        {
#if Prototype_4_2_OR_NEWER
            return PSV.ConstSettings.GetApplicationAlias();
#else
#if UNITY_2017_1_OR_NEWER
            return Application.identifier;
#else
            return Application.bundleIdentifier;
#endif
#endif
        }


        public static Sprite TextureToSprite (Texture2D texture)
        {
            if (texture != null)
                return Sprite.Create ( texture, new Rect ( 0, 0, texture.width, texture.height ), new Vector2 ( 0.5f, 0.5f ) );
            else
                return null;
        }

    }
}