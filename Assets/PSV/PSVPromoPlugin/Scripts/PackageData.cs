﻿using UnityEngine;
using System.Collections;


namespace PromoPlugin
{

    public class PackageData
    {
        public string
            app_bundle,
            app_alias;

        public float
            app_weight;

        [Tooltip( "Use English by default" )]
        public string
            app_name;
        public Texture2D
            app_icon;


        public PackageData( string bundle = "", string alias = "", string _name = "", float weight = 0, Texture2D icon = null )
        {
            app_name = _name;
            app_alias = alias;
            app_bundle = bundle;
            app_icon = icon;
            app_weight = weight;
        }

        public PackageData( PromoItem p_item, Texture2D icon = null )
        {
            app_bundle = p_item.app_bundle;
            app_alias = p_item.app_alias;
            app_name = p_item.app_name;
            app_weight = p_item.GetPromoWeight();
            app_icon = icon;
        }
    }
}