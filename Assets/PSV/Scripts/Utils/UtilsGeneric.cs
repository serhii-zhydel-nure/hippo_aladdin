﻿using UnityEngine;
using System.Collections.Generic;



namespace PSV
{
	static public partial class Utils
	{

		public static List<T> Shuffle<T>( this List<T> list )
		{
			int n = list.Count;
			List<T> new_list = list.CopyList();
			while (n > 0)
			{
				int k = Random.Range( 0, n );
				T item = new_list[n - 1];
				new_list[n - 1] = new_list[k];
				new_list[k] = item;
				n--;
			}
			return new_list;
		}

		public static T[] Shuffle<T>( this T[] array )
		{
			int n = array.Length;
			T[] new_array = array.CopyArray();
			while (n > 0)
			{
				int k = Random.Range( 0, n );
				T item = new_array[n - 1];
				new_array[n - 1] = new_array[k];
				new_array[k] = item;
				n--;
			}
			return new_array;
		}

		public static List<T> CopyList<T>( this List<T> list )
		{
			return new List<T>( list );
		}

		public static T[] CopyArray<T>( this T[] array )
		{
			int n = array.Length;
			T[] new_array = new T[n];
            System.Array.Copy( array, new_array, n);
            return new_array;
		}

		static public T GetRandomElement<T>( this List<T> list )
		{
			return list[Random.Range( 0, list.Count )];
		}


		public static T GetRandomElement<T>( this T[] array )
		{
			return array[Random.Range( 0, array.Length )];
		}


		public static T ToEnum<T>( this string value )
		{
			return (T) System.Enum.Parse( typeof( T ), value, true );
		}


		static public T GetRandomEnum<T>()
		{
			System.Array A = System.Enum.GetValues( typeof( T ) );
			T V = (T) A.GetValue( Random.Range( 0, A.Length ) );
			return V;
		}

        public static string ConvertToString<T>( this List<T> arr )
        {
            string res = "";
            for (int i = 0; i < arr.Count; i++)
            {
                res += i + ":\t" + arr[i].ToString() + "\n";
            }
            return res;
        }
        
        public static string ConvertToString<T>( this T[] arr )
        {
            string res = "Length = " + arr.Length + "\n";
            for (int i = 0; i < arr.Length; i++)
            {
                res += i + ":\t" + arr[i].ToString() + "\n";
            }
            return res;
        }

        public static string ConvertToString<T1, T2>( this Dictionary<T1, T2> dic )
        {
            string res = "count = " + dic.Count + "\n";

            foreach (var item in dic)
            {
                res += item.Key.ToString() + " : " + item.Value.ToString() + "\n";
            }
            return res;
        }

    }
}