﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV
{
    public static class NetworkReachabilityHandler
    {
        /// <summary>
        /// Is wait <see cref="NetworkReachability.ReachableViaLocalAreaNetwork"/> for start download.
        /// </summary>
        public static bool isUseWiFiOnly = true;

        private static bool analyticsAlreadySend = false;

        /// <summary>
        /// Get Localization string message "Waiting for network connection"
        /// </summary>
        public static string LangTextWaitNetworcConnection()
        {
            if (Languages.GetLanguage() == Languages.Language.Russian)
                return "Ожидание сетевого подключения";
            else
                return "Waiting for network connection";
        }

        /// <summary>
        /// Wait allow network reachability for start download. 
        /// Instruction wait network connection.
        /// Allows the user to select use Wi-Fi.
        /// </summary>
        public static IEnumerator WaitAllowLoading()
        {
            return WaitAllowLoading( AlwaysWaitConnetion );
        }

        /// <summary>
        /// Wait allow network reachability for start download. 
        /// Instruction wait network connection.
        /// Allows the user to select use Wi-Fi.
        /// </summary>
        /// <param name="IsStillWaitReachable">Is still need wait network connection. Called only on Network Not Reachable</param>
        public static IEnumerator WaitAllowLoading( Func<bool> IsStillWaitReachable )
        {
            bool needSendResult = false;
            if (!analyticsAlreadySend && Application.internetReachability != NetworkReachability.ReachableViaLocalAreaNetwork)
            {
                AnalyticsManager.LogEvent( AnalyticsEvents.DownloadFromServer, "Network Status: "
                    + ( Application.internetReachability == NetworkReachability.NotReachable ? "not connected" : "Mobile" ) );
                needSendResult = true;
            }
            while (true)// Check Network Reachability again
            {
                switch (Application.internetReachability)
                {
                    case NetworkReachability.NotReachable:
                    if (!IsStillWaitReachable())
                    {
                        if (needSendResult)
                            SandAnalyticsStatus();
                        yield break; // Skip loop Check Network Reachability
                    }

                    WarningPanel.Show( LangTextWaitNetworcConnection(), true );

                    do // Wait network connection
                    {
                        yield return null;
                    }
                    while (IsStillWaitReachable() && Application.internetReachability == NetworkReachability.NotReachable);
                    WarningPanel.Hide();
                    continue; // Check Network Reachability again

                    case NetworkReachability.ReachableViaCarrierDataNetwork:
                    if (!isUseWiFiOnly)
                    {
                        if (needSendResult)
                            SandAnalyticsStatus();
                        yield break; // Skip loop Check Network Reachability
                    }
                    ShowWarningWiFiOnly();

                    do // Wait connect Local Area Network
                    {
                        yield return null;
                    }
                    while (isUseWiFiOnly && Application.internetReachability != NetworkReachability.ReachableViaLocalAreaNetwork);

                    WarningPanel.Hide();
                    continue; // Check Network Reachability again
                }
                if (needSendResult)
                    SandAnalyticsStatus();
                break; // Skip loop Check Network Reachability
            }
        }

        private static void SandAnalyticsStatus()
        {
            if (analyticsAlreadySend)
                return;
            analyticsAlreadySend = true;
            string resultStatus;
            switch (Application.internetReachability)
            {
                case NetworkReachability.NotReachable:
                resultStatus = "not connected";
                break;
                case NetworkReachability.ReachableViaCarrierDataNetwork:
                resultStatus = "Mobile";
                break;
                default:
                resultStatus = "WIFI";
                break;
            }
            AnalyticsManager.LogEvent( AnalyticsEvents.DownloadFromServer, "Network Continue: " + resultStatus );
        }

        private static bool AlwaysWaitConnetion()
        {
            return true;
        }

        private static void ShowWarningWiFiOnly()
        {
            string warning_message;
            string toggle_message;
            if (Languages.GetLanguage() == Languages.Language.Russian)
            {
                warning_message = "Рекомендуется использовать только сеть Wi-Fi";
                toggle_message = "Использовать Wi-Fi сеть";
            }
            else
            {
                warning_message = "Recommended to use Wi-Fi only";
                toggle_message = "Using Wi-Fi only";
            }
            WarningPanel.Show( warning_message, true )
                .SetToggle( toggle_message )
                .OnAccept( WarningMessageAccept, true )
                .OnCancel( WarningMessageCancel );
        }

        public static void WarningMessageAccept()
        {
            WarningPanel.TryGetToggleValue( ref isUseWiFiOnly );
        }

        private static void WarningMessageCancel()
        {
            //Application.Quit(); // Close game.

            // Disable WiFi only and continue load
            isUseWiFiOnly = false;
        }
    }
}