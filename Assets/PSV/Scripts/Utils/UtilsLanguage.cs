﻿using UnityEngine;
using System.Collections.Generic;


namespace PSV
{

	static public partial class Utils
	{

		public static Languages.Language GetSystemLanguage()
		{
			Languages.Language res;
			string lang = "";
#if UNITY_ANDROID
			lang = GetAndroidDisplayLanguage();
#else
			lang =  Application.systemLanguage.ToString ( );
#endif
			Debug.Log( "Device language is " + lang );
			try
			{
				res = (Languages.Language) System.Enum.Parse( typeof( Languages.Language ), lang, true );
			}
			catch (System.Exception e)
			{
				Debug.LogWarning( "Utils.GetSystemLanguage: couldn't recognize " + lang + ", using English instead\n" + e );
				res = Languages.Language.English;
			}
			return res;

		}
        

		public static bool IsMuslim()
		{
            string display_lang = GetAndroidDisplayLanguage();
            switch (display_lang)
            {
                case "Arabic":
                case "Albanian":
                case "Bengali":
                case "Indonesian":
                case "Kyrgyz":
                case "Kazakh":
                case "Sinhala":
                case "Turkish":
                    return true;
                default:
                    return false;
            }
		}



		public static string GetAndroidDisplayLanguage()
		{
#if UNITY_EDITOR
			return Application.systemLanguage.ToString();
#elif UNITY_ANDROID
        AndroidJavaClass localeClass = new AndroidJavaClass ( "java/util/Locale" );
        AndroidJavaObject defaultLocale = localeClass.CallStatic<AndroidJavaObject> ( "getDefault" );
        AndroidJavaObject usLocale = localeClass.GetStatic<AndroidJavaObject> ( "US" );
        string systemLanguage = defaultLocale.Call<string> ( "getDisplayLanguage", usLocale );
        Debug.Log ( "Android language is " + systemLanguage + " detected as " + systemLanguage );
        return systemLanguage;
#else
        return "";
#endif
		}

	}
}
