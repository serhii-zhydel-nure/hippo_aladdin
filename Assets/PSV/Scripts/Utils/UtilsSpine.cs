﻿#define SPINE_PRESENT

using UnityEngine;
using System.Collections.Generic;
#if SPINE_PRESENT
using Spine.Unity;
#endif

namespace PSV
{

	static public partial class Utils
	{

#if SPINE_PRESENT
		static public void SetSpineAnimation( this SkeletonAnimation anim, string clip1, string clip2 = "", bool delay = false, float from = 0, float to = 0 )
		{
			if (anim != null)
			{
				if (anim.state == null)
				{
					anim.Initialize( false );
				}
				if (delay)
				{
					DG.Tweening.DOVirtual.DelayedCall( Random.Range( from, to ), () => SetSpineAnimation( anim, clip1, clip2, false ) );
				}
				else
				{
					if (clip1 != "")
					{
						anim.state.SetAnimation( 0, clip1, clip2 == "" );
						if (clip2 != "")
						{
							anim.state.AddAnimation( 0, clip2, true, 0 );
						}
					}
				}
			}
			else
			{
				Debug.Log( "SetAnimation: Animation is null" );
			}
		}
#endif
	}
}

