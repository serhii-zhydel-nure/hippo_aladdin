using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using DG.Tweening;

/// <summary>
/// Divided class into separate files.
/// Made class partial to make it user extendable
/// </summary>

namespace PSV
{

    static public partial class Utils
    {
        /// <summary>
        /// Will return aspect delta for current screen: if aspect is close to min_value - will return 0, if aspect is close to max_value - will return 1
        /// </summary>
        /// <param name="min_aspect">4x3 aspect</param>
        /// <param name="max_aspect">16x9 aspect</param>
        /// <returns></returns>
        [Obsolete( "Logic has not been updated for a long time and may be return invalid result" )]
        public static float GetAspectDelta( float min_aspect = 4f / 3f, float max_aspect = 16f / 9f )
        {
            float min_value = Mathf.Min( min_aspect, max_aspect );
            float max_value = Mathf.Min( min_aspect, max_aspect );
            float aspect_diff = 0;
            Camera cam = Camera.main;
            if (cam != null)
            {
                float aspect = Mathf.Clamp( cam.aspect, min_value, max_value );
                float coef = aspect / max_value;
                //normalize coef value from 0.75..1 to 0..1
                float min_coef = min_value / max_value;
                float max_coef = 1; //(rectangle_aspect/ rectangle_aspect)
                float coef_scale = ( max_coef / ( max_coef - min_coef ) );
                aspect_diff = ( coef - min_coef ) * coef_scale;
            }
            return aspect_diff;
        }

        public static bool RandomBool()
        {
            return UnityEngine.Random.value > 0.5f;
        }

        public static string GetUID()
        {
            string UID = System.Guid.NewGuid().ToString();
            if (PlayerPrefs.HasKey( "UID" ))
            {
                UID = PlayerPrefs.GetString( "UID" );
            }
            else
            {
                PlayerPrefs.SetString( "UID", UID );
                NextFrameCall.SavePrefs();
            }
            return UID;
        }


        /// <summary>
        /// Get clip lenth in animator.
        /// </summary>
        /// <param name="clipName">It is not state name. Use animation clip name.</param>
        public static float GetClipLength( this Animator animator, string clipName )
        {
            var all = animator.runtimeAnimatorController.animationClips;
            for (int i = 0; i < all.Length; ++i)
            {
                if (all[i].name == clipName)
                    return all[i].length;
            }
            Debug.LogError( "Animator[" + animator.name + " have no clip with name: " + clipName );
            return 0.0f;
        }

        /// <summary>
        /// Fade canvas group by time.
        /// You can put this method to <see cref="CoroutineHandler.Start(IEnumerator)"/>
        /// </summary>
        /// <param name="target">End alpha value</param>
        /// <param name="duration">Value change duration</param>
        /// <param name="callback">Callback on alpha change done</param>
        /// <param name="toggle_object">Is need togle group.gameObject ont target values 0 and 1 alpha</param>
        public static IEnumerator FadeAction( this CanvasGroup group, float target, float duration, Action callback, bool toggle_object = true )
        {
            target = Mathf.Clamp01( target );
            if (toggle_object && target == 1.0f)
                group.gameObject.SetActive( true );
            if (duration > 0.0f)
            {
                float multiplier = ( 1.0f / duration ) * Mathf.Abs( group.alpha - target );
                while (group.alpha != target)
                {
                    group.alpha = Mathf.MoveTowards( group.alpha, target, Time.unscaledDeltaTime * multiplier );
                    yield return null;
                }
            }
            else
            {
                group.alpha = target;
            }
            if (toggle_object && target == 0.0f)
                group.gameObject.SetActive( false );
            if (callback != null)
                callback();
        }
        
    }
}