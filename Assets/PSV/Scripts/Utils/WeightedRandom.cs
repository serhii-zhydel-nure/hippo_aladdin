﻿using UnityEngine;
using System.Collections;



namespace PSV
{

	public static class WeightedRandom
	{
		public static float[] CalcLookups( float[] weights )
		{
			float total_weight = 0;
			for (int i = 0; i < weights.Length; i++)
			{
				total_weight += weights[i];
			}
			float[] lookups = new float[weights.Length];
			for (int i = 0; i < weights.Length; i++)
			{
				lookups[i] = (weights[i] / total_weight) + (i == 0 ? 0 : lookups[i - 1]);
			}
			return lookups;
		}

		private static int binary_search( float needle, float[] lookups )
		{
			int high = lookups.Length - 1;
			int low = 0;
			int probe = 0;
			if (lookups.Length < 2)
			{
				return 0;
			}
			while (low < high)
			{
				probe = (int) ((high + low) / 2);

				if (lookups[probe] < needle)
				{
					low = probe + 1;
				}
				else if (lookups[probe] > needle)
				{
					high = probe - 1;
				}
				else
				{
					return probe;
				}
			}

			if (low != high)
			{
				return probe;
			}
			else
			{
				return (lookups[low] >= needle) ? low : low + 1;
			}
		}


		public static int RandomW( float[] weights )
		{
			float[] lookups = CalcLookups( weights );

			return PrecalculatedRandomW( lookups );
		}


		public static int PrecalculatedRandomW( float[] lookups ) //used for getting random on the same set to optimize time by excluding CalcLookups from each cycle of getting random
		{
			if (lookups.Length > 0)
				return binary_search( Random.value, lookups );
			else
				return -1;
		}
	}
}
