﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.IO;

namespace PSV
{
    /// <summary>
    /// Allow for Editor only!
    /// </summary>
    public class EditorPSVSettingsData : ScriptableObject
    {
        public bool storage_target_platform_bundles_streaming = false;
        public bool always_local_url_in_editor = true;
        /// <summary>
        /// Uncheck this to leave all scenes in Hierarchy and keep all necessary Instances created without switching to splashes
        /// </summary>
        public bool splash_transition = true;

        public string path_buffer_bundles = "Assets/BundlesStorage/CompiledBundles";
        public string path_reses_assetbundles = "Assets/BundlesStorage/BundleResources";
        public string name_multilanguage_sounds_folder = "MultiLanguage";
        public string name_nonlanguage_sounds_folder = "NonLanguage";
        public string cis_application_name = string.Empty;
        public string path_game_folder = "Assets/Game";
        public bool project_visualisation = true;
        public bool check_version_prototype = true;
    }

    /// <summary>
    /// Allow for Editor only!
    /// </summary>
    public class EditorPSVSettings
    {
        public const string path_to_editor_settings = "Assets/PSV/Editor/EditorSettingsData.asset";
        public const string path_sounds_in_resources = "Assets/Resources/Sounds";

        public static EditorPSVSettingsData settings;

        static EditorPSVSettings()
        {
            Init();
        }

        public static void Init()
        {
            settings = UnityEditor.AssetDatabase.LoadAssetAtPath<EditorPSVSettingsData>( path_to_editor_settings );

            if (settings == null)
            {
                // Serch old version.
                string[] search = UnityEditor.AssetDatabase.FindAssets( "t:EditorPSVSettingsData" );
                if (search.Length != 0)
                {
                    settings = UnityEditor.AssetDatabase.LoadAssetAtPath<EditorPSVSettingsData>(
                        UnityEditor.AssetDatabase.GUIDToAssetPath( search[0] ) );
                }
                else
                {
                    string folder = Path.GetDirectoryName( path_to_editor_settings );
                    if (!Directory.Exists( folder ))
                        Directory.CreateDirectory( folder );
                    settings = ScriptableObject.CreateInstance<EditorPSVSettingsData>();
                    UnityEditor.AssetDatabase.CreateAsset( settings, path_to_editor_settings );
                }
            }
        }
    }
}
#endif