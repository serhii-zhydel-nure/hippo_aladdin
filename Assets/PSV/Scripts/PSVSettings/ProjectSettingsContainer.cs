﻿using UnityEngine;
using System.Collections.Generic;
using PSV.IAP;

namespace PSV
{
    public static class PSVSettings
    {
        private static ProjectSettingsContainer container;

        public static ProjectSettingsContainer settings { get { return container; } }

        static PSVSettings()
        {
            Init();
        }

        public static void Init()
        {
            container = Resources.Load<ProjectSettingsContainer>( "ProjectSettings" );
            if (container == null)
            {
                container = ScriptableObject.CreateInstance<ProjectSettingsContainer>();
#if UNITY_EDITOR
                UnityEditor.AssetDatabase.CreateAsset( container, "Assets/Resources/ProjectSettings.asset" );
#endif
            }
        }
    }

    [System.Obsolete( "Use PSVSettings class." )]
    public static class ProjectSettingsManager
    {
        public static ProjectSettingsContainer settings { get { return PSVSettings.settings; } }
    }

    public class ProjectSettingsContainer : ScriptableObject
    {
        #region *** IAP purchase
        public string public_key = string.Empty;

#if UNITY_ANDROID || UNITY_EDITOR
        public ProductProperties[] android_products_SKU =
        {
            //list here as constants all your products, registered in GooglePlay console
		    //no capital letters
		    new ProductProperties( Products.SKU_ADMOB,                  "admob",       ProductType.NonConsumable),
        };
#endif
#if UNITY_IOS || UNITY_EDITOR
        public ProductProperties[] ios_products_SKU =
        {
            //IOS SKUs are unique per account, so we have to add app's bundle to common product name
            //use your projects' SKU
            new ProductProperties( Products.SKU_ADMOB,                  "com.PSVGamestudio.BrandNewPrototype_admob",        ProductType.NonConsumable),
        };
#endif

        public Products[] ads_related_products =
        {
                Products.SKU_ADMOB,
                //Products.SKU_SUBSCRIPTION,
        };


        public ProductProperties[] products_SKU
        {
            get
            {
#if UNITY_ANDROID
                return android_products_SKU;
#elif UNITY_IOS
                return ios_products_SKU;
#else
                return new ProductProperties[0];
#endif
            }
        }
        #endregion

        #region *** ADS and Analytics
        public bool
            ads_manager_debug,
            manager_google_debug,
            //firebase_manager_debug,
            billing_manager_debug,
            analytics_manager_debug;

        public string android_analytics_id = string.Empty;
        public string ios_analytics_id = string.Empty;
        public string windows_analytics_id { get { return android_analytics_id; } } // Not supported

        public float ads_interstitial_delay = 30.0f;
        public float ads_rewarded_vidoe_delay = 0;

#if UNITY_ANDROID || UNITY_EDITOR
        public ProviderSettings[] android_provider_settings =
        {
            new ProviderSettings( ADS.ProviderParams.NEATPLUG_BANNER_ID, ADS.AdsSettings.DEMO_ADS_PARAM ),
            new ProviderSettings( ADS.ProviderParams.NEATPLUG_INTERSTITIAL_ID, ADS.AdsSettings.DEMO_ADS_PARAM ),
            new ProviderSettings( ADS.ProviderParams.HOMEADS_INTERSTITIAL_ID, ADS.AdsSettings.HOME_ADS_PARAM )
        };
#endif
#if UNITY_IOS || UNITY_EDITOR
        public ProviderSettings[] ios_provider_settings =
        {
            new ProviderSettings( ADS.ProviderParams.GOOGLEADS_BANNER_ID, ADS.AdsSettings.DEMO_ADS_PARAM ),
            new ProviderSettings( ADS.ProviderParams.GOOGLEADS_INTERSTITIAL_ID, ADS.AdsSettings.DEMO_ADS_PARAM )
        };
#endif

#if UNITY_ANDROID || UNITY_EDITOR
        public ADS.AdNetwork[]
            android_providers_order = new ADS.AdNetwork[]
            {
                ADS.AdNetwork.NeatPlug,
                ADS.AdNetwork.HomeAds,
            };
#endif
#if UNITY_IOS || UNITY_EDITOR
        public ADS.AdNetwork[]
            ios_providers_order = new ADS.AdNetwork[]
            {
                ADS.AdNetwork.GoogleAds,
            };
#endif


        public ProviderSettings[] ads_provider_settings
        {
            get
            {
#if UNITY_ANDROID
                return android_provider_settings;
#elif UNITY_IOS
				return ios_provider_settings;
#else
                return new ProviderSettings[0];
#endif
            }
        }
        public ADS.AdNetwork[] providers_order
        {
            get
            {
#if UNITY_ANDROID
                return android_providers_order;
#elif UNITY_IOS
				return ios_providers_order;
#else
				return new ADS.AdNetwork [0];
#endif
            }
        }

        [System.Serializable]
        public class ProviderSettings
        {
            [UnityEngine.Serialization.FormerlySerializedAs( "param" )] // Support obsolete structure
            public int paramIndex;
            public string param;
            public string value;

            public ProviderSettings( string _param, string _value )
            {
                param = _param;
                value = _value;
                paramIndex = -1;
            }

            public void RestoreObsolete( ADS.ProviderParams providerParams ) // Can not be called in constructor.
            {
                if (param.Length == 0)
                {
                    if (paramIndex > -1)
                    {
                        param = ADS.ProviderParams.FromObsoleteOrder( paramIndex );
                        paramIndex = providerParams[param];
                    }
                }
                else
                {
                    paramIndex = providerParams[param];
                }
            }
        }
        #endregion

        #region *** SceneLoader
        public bool async_load = true;         //load scenes asynchronously and have possibility to animate loading progress or load it as usually
        public float splash_duration = 2f;       //splash screen lifetime
        public float transition_duration = 0.5f;   //transition animation duration

        public Scenes[] splash_scenes = new Scenes[0];   //list here all scenes that should be shown at the start of the game with some short lifetime
        public Scenes first_scene = Scenes.MainMenu;       //scene to load after splashes (usually MainMenu)
        public SceneLoader.TransitionMethod transition_method = SceneLoader.TransitionMethod.Tween;             //how to switch between scenes
        public bool never_sleep_screen = false;
        #endregion

        #region *** Languages
        public Languages.Language[] available_languages =
        {
            Languages.Language.English,
            Languages.Language.French,
            Languages.Language.Portuguese,
            Languages.Language.Russian,
            Languages.Language.Spanish,
        };
        #endregion

        #region *** AssetBundles resources
#if ASSET_BUNDLES_STORAGE
        /// <summary>
        /// All bundle paths in project. Path 0 & 1 reserved for localization manager.
        /// </summary>
        public AssetBundles.PathToBundle[] asssetbundle_pathes = new AssetBundles.PathToBundle[]
        {
                new AssetBundles.PathToBundle( string.Empty, ".locale" ),
                new AssetBundles.PathToBundle( string.Empty, "nonlanguage.locale" ),
        };
        public uint custom_global_assets_version = 0;

        public AssetBundles.PathToBundle language_bundle_path
        {
            get { return asssetbundle_pathes[0]; }
            set { asssetbundle_pathes[0] = value; }
        }
        public AssetBundles.PathToBundle non_language_bundle_path
        {
            get { return asssetbundle_pathes[1]; }
            set { asssetbundle_pathes[1] = value; }
        }

        /// <summary>
        /// Bundles Caching on device by version. 
        /// </summary>
        public uint global_assets_version
        {
            get
            {
                if (custom_global_assets_version > 0)
                    return custom_global_assets_version;
                return ConstSettings.GetVersionNumber();
            }
        }
#endif
        #endregion

        #region *** Other Settings
        /// <summary>
        /// Seted in Editor on open PSVSettings Window
        /// </summary>
        public bool portrait_orientation_only = false;

        public Localization.TextFormat locale_text_format = Localization.TextFormat.none;
        public string locale_text_path = "LanguageText/Text";
        #endregion
    }

}
