﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV
{
    public static partial class ConstSettings
    {
        [Obsolete( "Better use AwakeStaticAttribute" )]
        public const RuntimeInitializeLoadType STATIC_MANAGER_LOAD_TYPE = RuntimeInitializeLoadType.BeforeSceneLoad;

        public static string GetApplicationAlias()
        {
#if UNITY_IOS
            return ios_application_name + "/" + ios_application_id;
#elif UNITY_5_6_OR_NEWER
			return Application.identifier;
#else
            return Application.bundleIdentifier;
#endif
        }

        public static string GetApplicationMarketURL()
        {
            return GetApplicationMarketURL( GetApplicationAlias() );
        }

        public static string GetApplicationMarketURL( string app_alias )
        {
            if (app_alias.Length < 2)
            {
                Debug.LogError( "ConstSettings: GetApplicationMarketURL( app_alias ) is empty" );
                return string.Empty;
            }
#if UNITY_ANDROID
            return "market://details?id=" + app_alias;
#elif UNITY_IOS
            return "itms-apps://itunes.apple.com/app/" + app_alias;
#elif UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA
            // TODO: Check correct link
            return "ms-windows-store:navigate?appid=" + app_alias;
#else
            return string.Empty;
#endif
        }

        public static uint GetVersionNumber( string version_str = null )
        {
            if (string.IsNullOrEmpty( version_str ))
                version_str = Application.version;
            string appVersion = string.Empty;
            for (int i = 0; i < version_str.Length; i++)
            {
                if (System.Char.IsDigit( version_str[i] ))
                    appVersion += version_str[i];
            }
            uint version;
            if (uint.TryParse( appVersion, out version ))
                return version;
            else
                return 0;
        }
    }
}