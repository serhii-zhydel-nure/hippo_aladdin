﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System;

namespace PSV.SettingsEditor
{
    public static class AssetsHelper
    {
        public const string assetsFolderName = "Assets";

        /// <summary>
        /// Find asset and check valid folder
        /// </summary>
        /// <param name="folderName">Folder name only</param>
        public static bool IsExistFolderInAssets( string folderName )
        {
            string[] select = AssetDatabase.FindAssets( folderName );
            for (int i = 0; i < select.Length; i++)
            {
                string asset = AssetDatabase.GUIDToAssetPath( select[0] );
                if (AssetDatabase.IsValidFolder( asset ))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Return code:
        /// -1 - not found;
        /// 0 - empty;
        /// 1 - not empty;
        /// </summary>
        public static int IsFolderEmpty( string path )
        {
            if (Directory.Exists( path ))
            {
                string[] files_in = Directory.GetFiles( path );
                string[] folders_in = Directory.GetDirectories( path );
                if (files_in.Length > 0 || folders_in.Length > 0)
                {
                    return 1;
                }
                return 0;
            }
            return -1;
        }

        /// <summary>
        /// Check exist 'from' folder and empty 'to' path
        /// </summary>
        public static bool IsCanMoveFolder( string from, string to )
        {
            if (Directory.Exists( from ))
            {
                int folderCode = IsFolderEmpty( to );
                if (folderCode == 0)
                {
                    if (GetPathInAssets( ref to ))
                        AssetDatabase.DeleteAsset( to );
                }
                else if (folderCode == 1)
                {
                    Debug.LogError( "Folder [" + to + "] already exist and can't move [" + from + "]!" );
                    return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Combine <see cref="IsCanMoveFolder(string, string)"/> and <see cref="MoveFolder(string, string)"/>
        /// </summary>
        public static bool TryMoveFolder( string from, string to )
        {
            if (IsCanMoveFolder( from, to ))
            {
                MoveFolder( from, to );
                return true;
            }
            return false;
        }

        public static void MoveFolder( string from, string to )
        {
            EditorUtility.DisplayProgressBar( "Hold on. Move folder", from + " -> " + to, 1.0f );
            string toFolder = Path.GetDirectoryName( to );
            CreateFolder( toFolder );

            GetPathInAssets( ref to );
            GetPathInAssets( ref from );
            AssetDatabase.MoveAsset( from, to );
            Debug.Log( "Folder " + from + " moved to " + to );
            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// Create folder if not exist
        /// </summary>
        public static void CreateFolder( string path )
        {
            if (!Directory.Exists( path ))
            {
                Directory.CreateDirectory( path );
                AssetDatabase.ImportAsset( path );
            }
        }

        /// <summary>
        /// Find asset and call <see cref="SelectFile(string, bool)"/>
        /// </summary>
        /// <param name="nameFile">Name file only</param>
        /// <param name="openFile">Is open file in default viewer.</param>
        public static void FindAndOpen( string nameFile, bool openFile )
        {
            string[] select = AssetDatabase.FindAssets( nameFile );
            if (select.Length == 0)
            {
                Debug.LogError( "Not found file [" + nameFile + "]" );
                return;
            }
            SelectFile( AssetDatabase.GUIDToAssetPath( select[0] ), openFile );
        }

        /// <summary>
        /// Select asset in Project view and ping.
        /// Can be open in default viewer.
        /// </summary>
        /// <param name="path">Path to asset</param>
        /// <param name="openFile">Is open file in default viewer.</param>
        public static void SelectFile( string path, bool openFile )
        {
            if (GetPathInAssets( ref path ))
            {
                UnityEngine.Object asset = AssetDatabase.LoadAssetAtPath( path, typeof( UnityEngine.Object ) );
                if (asset)
                {
                    Selection.activeObject = asset;
                    EditorGUIUtility.PingObject( asset );
                    if (openFile)
                        AssetDatabase.OpenAsset(asset);
                    return;
                }
            }
            else if (openFile && File.Exists( path ))
            {
                System.Diagnostics.Process.Start( Path.GetFullPath( path ) );
                return;
            }
            Debug.LogError( "Not found file [" + path + "]" );
        }

        public static IEnumerator HandleAllFilesAtPath( string path, Action<string> foreachFile, string titleProgress )
        {
            if (foreachFile == null)
            {
                Debug.LogError( "HandleAllFilesAtPath started with empty handler!" );
                yield break;
            }
            EditorUtility.DisplayProgressBar( titleProgress, string.Format( "Select all files." ), 0.1f );
            yield return null;
            if (!Directory.Exists( path ))
            {
                if (File.Exists( path ))
                    foreachFile( path );
            }
            else
            {
                string[] files_in = Directory.GetFiles( path, "*.*", SearchOption.AllDirectories );
                for (int i = 0; i < files_in.Length; i++)
                {
                    EditorUtility.DisplayProgressBar( titleProgress, files_in[i], ( float )i / files_in.Length );
                    yield return null;
                    foreachFile( files_in[i] );
                }
            }
            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// AssetDatabase support only pathes from Assets Folder then use this method for convert.
        /// Return true
        /// </summary>
        public static bool GetPathInAssets( ref string path )
        {
            if (path.StartsWith( assetsFolderName ))
            {
                char nextChar = path[assetsFolderName.Length];
                if (nextChar == Path.DirectorySeparatorChar || nextChar == Path.AltDirectorySeparatorChar)
                    return true;
            }
            int index = path.IndexOf( Path.DirectorySeparatorChar + assetsFolderName + Path.DirectorySeparatorChar );
            if (index < 0)
            {
                index = path.IndexOf( Path.AltDirectorySeparatorChar + assetsFolderName + Path.AltDirectorySeparatorChar );
                if (index < 0)
                {
                    return false;
                }
            }
            path = path.Substring( index + 1 );
            return true;
        }
    }
}