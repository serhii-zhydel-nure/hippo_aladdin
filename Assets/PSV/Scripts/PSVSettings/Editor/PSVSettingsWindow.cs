﻿#define FoldoutOneOpen
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

namespace PSV.SettingsEditor
{
    public partial class PSVSettingsWindow : EditorWindow, IHasCustomMenu
    {
        private Vector2 scrollPos;
        private bool enableForPlayMode = false;
        private PSVWindowElement[] elements;
        private string editorFileName;
        public BuildTargetGroup[] validPlatforms;

        private void OnEnable()
        {
            if (enableForPlayMode || !EditorApplication.isPlaying)
            {
                if (PSVSettings.settings == null)
                {
                    PSVSettings.Init();
                }
                validPlatforms = new BuildTargetGroup[] { BuildTargetGroup.Android, BuildTargetGroup.iOS };
                InitTargetPlatform();

                elements = new PSVWindowElement[]{
                    new PSVVersionInfo(),
                    new PSVDefineWindow(),
                    new PSVPurchaiseWindow(),
                    new PSVAnalyticsWindow(),
                    new PSVAdsWindow(),
                    new PSVSceneManagementWindow(),
                    new PSVLanguagesWindow(),
                    new PSVResourcesSettings(),
#if ASSET_BUNDLES_STORAGE
                    new PSVBundleSettingsWindow(),
                    new PSVBundleManagerWindow(),
#endif
                    new PSVOtherSettingsWindow(),
                    //new InternalIconViewer(),
                };
#if !FoldoutOneOpen
                if (PSVWindowElement.spoilers == null)
                    PSVWindowElement.spoilers = new List<bool>( elements.Length );
#endif
            }
        }

        private void OnGUI()
        {
            if (!enableForPlayMode && EditorApplication.isPlaying)
            {
                EditorGUILayout.HelpBox( "Settings are disabled to optimize the play mode.", MessageType.Info );
                GUILayout.FlexibleSpace();
                if (GUILayout.Button( "Show settings", GUILayout.Height( 50.0f ) ))
                {
                    enableForPlayMode = true;
                    OnEnable();
                }
                else
                {
                    elements = null;
                }
                return;
            }
            if (EditorApplication.isCompiling)
            {
                EditorGUILayout.HelpBox( "Now global changes are begin implemented. Please refresh the window after execution.", MessageType.Info );
                GUILayout.FlexibleSpace();
                if (GUILayout.Button( "Refresh window", GUILayout.Height( 50.0f ) ))
                { }
                return;
            }
            if (PSVSettings.settings == null)
            {
                PSVSettings.Init();
                Repaint();
                return;
            }
            if (EditorPSVSettings.settings == null)
            {
                EditorPSVSettings.Init();
                Repaint();
                return;
            }
            if (elements == null)
            {
                OnEnable();
                return;
            }
            try
            { EditorGUILayout.BeginHorizontal(); } // Head
            catch { return; }
            PSVWindowElement.ShowBtnInfoURL( "Version " + ConstSettings.prototypeVersion, PSVVersionInfo.changelogDocumentURL );

            GUILayout.FlexibleSpace();
            GUILayout.Label( "Platform:", GUILayout.ExpandWidth( false ) );
            PSVWindowElement.settingsTarget = PSVWindowElement.ShowToggleTargetPlatform( validPlatforms, PSVWindowElement.settingsTarget );
            EditorGUILayout.EndHorizontal();

            PSVWindowElement.InitFoldouts();
            scrollPos = EditorGUILayout.BeginScrollView( scrollPos );
            GUILayout.Space( 10 );

            for (int i = 0; i < elements.Length; i++)
            {
                if (elements[i].IsEnabled())
                    using (PSVWindowElement.Box.Scope())
                        elements[i].OnGUI( this );
            }
            
#if !ASSET_BUNDLES_STORAGE
            EditorGUILayout.HelpBox( "Now supported only Resources folder storage.\n" +
                "For support AssetBundles storage need enable it in scripting define switches.", MessageType.Info );
#endif

            EditorGUILayout.EndScrollView();
            if (GUI.changed)
            {
                EditorUtility.SetDirty( PSVSettings.settings );
                EditorUtility.SetDirty( EditorPSVSettings.settings );
            }
        }

        public void AddItemsToMenu( GenericMenu menu )
        {
            if (string.IsNullOrEmpty( editorFileName ))
                editorFileName = new System.Diagnostics.StackTrace( true ).GetFrame( 0 ).GetFileName();
            menu.AddItem( new GUIContent( Path.GetFileName( editorFileName ).Substring( 3 ) ), false, EditScriptWindow, editorFileName );

            if (elements == null)
                return;
            for (int i = 0; i < elements.Length; i++)
            {
                var content = elements[i].EditorFileName();
                if (!string.IsNullOrEmpty( content ))
                    menu.AddItem( new GUIContent( Path.GetFileName( content ).Substring( 3 ) ), false, EditScriptWindow, content );
            }
            menu.AddSeparator( string.Empty );
            menu.AddItem( new GUIContent( "Check version" ), EditorPSVSettings.settings.check_version_prototype,
                ( ( PSVVersionInfo )elements[0] ).ToggleVersionChecked );
            menu.AddItem( new GUIContent( "Create current Info" ), false, PSVVersionInfo.CreateCurrentInfoFile );
        }
        private void EditScriptWindow( object path )
        {
            AssetsHelper.SelectFile( ( string )path, true );
        }

        private void InitTargetPlatform()
        {
            PSVWindowElement.settingsTarget = EditorUserBuildSettings.selectedBuildTargetGroup;
            for (int i = 0; i < validPlatforms.Length; i++)
            {
                if (validPlatforms[i] == PSVWindowElement.settingsTarget)
                    return;
            }
            PSVWindowElement.settingsTarget = BuildTargetGroup.Android;
        }

        [MenuItem( "Window/PSV Project Settings", false, int.MinValue )]
        public static void OpenSettingsWindow()
        {
            var window = EditorWindow.GetWindow<PSVSettingsWindow>( "PSV Settings", typeof( SceneView ) );
            var position = window.position;
            if (position.size.x < 450.0f)
            {
                position.size = new Vector2( 450.0f, 500.0f );
                position.position = new Vector2( Screen.width / 2, Screen.height / 2 );
                window.position = position;
            }
        }

        [MenuItem( "HelpTools/Clear PlayerPrefs" )]
        private static void ClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            Debug.Log( "Warning: player prefs cleared" );
        }
    }

    public abstract class PSVWindowElement
    {
        #region Fields
        public static event Action changeTargetPlatform;
        protected const float buttonLenght = 65.0f;
        public static GUIStyle spoiler_style;
#if FoldoutOneOpen
        public static int spoiler_open = -1;
#else
        public static List<bool> spoilers;
#endif
        public static int global_foldout_index;
        public static BuildTargetGroup settingsTarget;

        public static GUIContent badge_new_icon;
        private static GUIContent script_select;
        private static GUIContent remove_element;
        private static GUIContent add_element;
        protected static GUIContent android_icon;
        protected static GUIContent ios_icon;
        private static GUIContent default_folder_icon;
        private static GUIContent help_icon;

        private string editorFileName;
        #endregion

        #region Initialize
        static PSVWindowElement()
        {
            script_select = new GUIContent( EditorGUIUtility.IconContent( "cs Script Icon" ) ) { text = "Select", };

            remove_element = new GUIContent( EditorGUIUtility.IconContent( "Toolbar Minus" ) ) { text = "Remove", };
            add_element = new GUIContent( EditorGUIUtility.IconContent( "Toolbar Plus" ) ) { text = "Add", };
            android_icon = EditorGUIUtility.IconContent( "BuildSettings.Android.Small" );
            ios_icon = EditorGUIUtility.IconContent( "BuildSettings.iPhone.Small" );
            default_folder_icon = new GUIContent( EditorGUIUtility.FindTexture( "Folder Icon" ) );
            help_icon = new GUIContent( EditorGUIUtility.IconContent( "_Help" ) );

            string newBudgeName;
#if UNITY_2017_1_OR_NEWER
            newBudgeName = "PackageBadgeNew";
#else
            newBudgeName = "AS Badge New";
#endif
            badge_new_icon = EditorGUIUtility.IconContent( newBudgeName );
        }

        /// <summary>
        /// Can be init only on GUI
        /// </summary>
        public static void InitFoldouts()
        {
            if (spoiler_style == default( GUIStyle ))
                spoiler_style = GUI.skin.FindStyle( "IN TitleText" ) ?? EditorGUIUtility.GetBuiltinSkin( EditorSkin.Inspector ).FindStyle( "IN TitleText" );

            global_foldout_index = 0;
        }

        #endregion

        public static ProjectSettingsContainer settings { get { return PSVSettings.settings; } }
        public static EditorPSVSettingsData editor_settings { get { return EditorPSVSettings.settings; } }

        public abstract void OnGUI( PSVSettingsWindow window );
        public virtual string EditorFileName()
        {
            if (string.IsNullOrEmpty( editorFileName ))
            {
                editorFileName = new System.Diagnostics.StackTrace( true )
                                .GetFrame( 1 )
                                .GetFileName();
                AssetsHelper.GetPathInAssets( ref editorFileName );
            }
            return editorFileName;
        }
        public virtual bool IsEnabled()
        {
            return true;
        }

        #region GUI helpers
        public static string GetArrowSymbl( bool is_unfolded )
        {
            int arrow = is_unfolded ? 0x25BC : 0x25BA;
            return ( ( char )arrow ).ToString();
        }

        public static bool InFoldout( string label, bool new_badge = false )
        {
            if (label.Length == 0)
                return true;
#if FoldoutOneOpen
            bool isOpen = spoiler_open == global_foldout_index;
#else
            bool isOpen = spoilers[global_foldout_index];
            if (global_foldout_index >= spoilers.Count)
                spoilers.Add( false );
#endif
            label = GetArrowSymbl( isOpen ) + " " + label;
            if (new_badge)
                GUILayout.BeginHorizontal();
            bool val = GUILayout.Toggle( isOpen, label, spoiler_style, GUILayout.ExpandWidth( !new_badge ) );

            if (new_badge)
            {
                GUILayout.Label( badge_new_icon );
                GUILayout.EndHorizontal();
            }
            if (val != isOpen)
            {
                EditorGUIUtility.editingTextField = false;
#if FoldoutOneOpen
                spoiler_open = val ? global_foldout_index : -1;
#else
                spoilers[global_foldout_index] = val;
#endif
                GUI.changed = false;
            }
            global_foldout_index++;
            return val;
        }

        public static bool ShowToggleLeft( ref bool is_toggled, GUIContent label, params GUILayoutOption[] option )
        {
            if (is_toggled != EditorGUILayout.ToggleLeft( label, is_toggled, option ))
            {
                is_toggled = !is_toggled;
                return true;
            }
            return false;
        }
        public static bool ShowToggleLeft( ref bool is_toggled, string label, params GUILayoutOption[] option )
        {
            if (is_toggled != EditorGUILayout.ToggleLeft( label, is_toggled, option ))
            {
                is_toggled = !is_toggled;
                return true;
            }
            return false;
        }

        public static void ShowPathFieldAssets( ref string path )
        {
            ShowField( ref path );

            default_folder_icon.text = "Path";
            if (GUILayout.Button( default_folder_icon, EditorStyles.miniButton,
                GUILayout.Width( buttonLenght ), GUILayout.MaxHeight( EditorGUIUtility.singleLineHeight ) ))
            {
                string full_path = EditorUtility.SaveFolderPanel( "Select folder for resources.", path, string.Empty );
                if (AssetsHelper.GetPathInAssets( ref full_path ))
                    path = full_path;
                else
                    GUI.changed = false;
            }
        }

        public static void ShowField( ref string field, params GUILayoutOption[] option )
        {
            field = EditorGUILayout.TextField( field, option ).Trim( ' ' );
        }

        public static void ShowBtnSelectAsset( string path, bool openFile = false )
        {
            default_folder_icon.text = "Select";
            if (GUILayout.Button( default_folder_icon, EditorStyles.miniButton,
                GUILayout.Width( buttonLenght ), GUILayout.MaxHeight( EditorGUIUtility.singleLineHeight ) ))
            {
                AssetsHelper.SelectFile( path, openFile );
                GUI.changed = false;
            }
        }
        public static void ShowBtnSelectAsset( string label, string path, bool openFile = false )
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label( label, /*EditorStyles.wordWrappedLabel,*/ GUILayout.ExpandWidth( false ) );
            ShowBtnSelectAsset( path, openFile );
            GUILayout.EndHorizontal();
        }
        public static void ShowBtnSelectScript( string nameScript )
        {
            if (GUILayout.Button( script_select, EditorStyles.miniButton, GUILayout.Width( buttonLenght ), GUILayout.MaxHeight( EditorGUIUtility.singleLineHeight ) ))
            {
                AssetsHelper.FindAndOpen( nameScript, true );
                GUI.changed = false;
            }
        }
        public static void ShowBtnSelectScript( string label, string nameScript )
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label( label, /*EditorStyles.wordWrappedLabel,*/ GUILayout.ExpandWidth( false ) );
            ShowBtnSelectScript( nameScript );
            GUILayout.EndHorizontal();
        }

        public static void ShowBtnInfoURL( string label, string url )
        {
            help_icon.text = label;
            if (GUILayout.Button( help_icon, EditorStyles.miniLabel, GUILayout.ExpandWidth( false ) ))
            {
                Help.BrowseURL( url );
                GUI.changed = false;
            }
        }
        public static GUIContent IconContent( BuildTargetGroup platform, string text = "" )
        {
            switch (platform)
            {
                case BuildTargetGroup.iOS:
                ios_icon.text = text;
                return ios_icon;
                default:
                android_icon.text = text;
                return android_icon;
            }
        }

        public static BuildTargetGroup ShowToggleTargetPlatform( BuildTargetGroup[] validPlatforms, BuildTargetGroup settingsTarget )
        {
            for (int i = 0; i < validPlatforms.Length; i++)
            {
                bool isSelect = validPlatforms[i] == settingsTarget;
                if (GUILayout.Toggle( validPlatforms[i] == settingsTarget, IconContent( validPlatforms[i] ), EditorStyles.toolbarButton, GUILayout.Width( buttonLenght ) ) && !isSelect)
                {
                    settingsTarget = validPlatforms[i];
                    if (changeTargetPlatform != null)
                        changeTargetPlatform();
                    EditorGUIUtility.editingTextField = false;
                    GUI.changed = false;
                }
            }
            return settingsTarget;
        }

        public delegate void ShowElementArray<T>( ref T element, int index );
        public static void EdditArray<T>( ref T[] array, ShowElementArray<T> show_element, System.Func<bool, int, bool> on_change_size = null, int start_elem = 0 )
        {
            var buttonOptions = new GUILayoutOption[] { GUILayout.Width( buttonLenght ), GUILayout.Height( EditorGUIUtility.singleLineHeight ) };
            for (int i = start_elem; i < array.Length; i++)
            {
                GUILayout.BeginHorizontal();
                show_element( ref array[i], i );

                if (GUILayout.Button( remove_element, EditorStyles.miniButton, buttonOptions ))
                {
                    bool isNeedRemove = true;
                    if (on_change_size != null)
                    {
                        isNeedRemove = on_change_size( false, i );
                    }
                    if (isNeedRemove)
                    {
                        RemoveArrayElementAt( ref array, i );
                    }

                }
                GUILayout.EndHorizontal();
            }
            if (GUILayout.Button( add_element, EditorStyles.miniButton, buttonOptions ))
            {
                AddArrayElementToEnd( ref array, default( T ) );
                if (on_change_size != null)
                {
                    on_change_size( true, array.Length - 1 );
                }
            }
            EditorGUILayout.Space();
        }
        public static void RemoveArrayElementAt<T>( ref T[] array, int index )
        {
            if (index >= array.Length || index < 0)
                return;
            int newSize = array.Length - 1;
            T[] newArray = new T[newSize];
            if (index < newSize)
            {
                Array.Copy( array, index + 1, newArray, index, newSize - index );
            }
            Array.Copy( array, 0, newArray, 0, index );
            array = newArray;
        }
        public static void AddArrayElementToEnd<T>( ref T[] array, T newElement )
        {
            T[] newArray = new T[array.Length + 1];
            Array.Copy( array, newArray, array.Length );
            array = newArray;
            array[array.Length - 1] = newElement;
        }

        public class Box : System.IDisposable
        {
            public static Box lastBox;
            public GUIStyle boxScopeStyle;
            private bool spaceAfter;

            private Box()
            {
                boxScopeStyle = new GUIStyle( EditorStyles.helpBox );
                var p = boxScopeStyle.padding;
                p.right += 6;
                p.top += 1;
                p.left += 3;
            }

            public static Box Scope( bool spaceAfter = true )
            {
                if (lastBox == null)
                    lastBox = new Box();
                lastBox.spaceAfter = spaceAfter;
                EditorGUILayout.BeginVertical( lastBox.boxScopeStyle );
                return lastBox;
            }
            public void Dispose()
            {
                EditorGUILayout.EndVertical();
                if (spaceAfter)
                    EditorGUILayout.Space();
            }
        }
        #endregion
    }

    [CustomEditor( typeof( ProjectSettingsContainer ) )]
    public class ProjectSettingsBlocker : Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button( "Edit settings", GUILayout.Height( 50 ) ))
            {
                PSVSettingsWindow.OpenSettingsWindow();
            }
            EditorGUILayout.HelpBox( "For edit settings use window: Window/PSV Project Settings", MessageType.Info );
        }
    }

    [CustomEditor( typeof( EditorPSVSettingsData ) )]
    public class EditorSettingsBlocker : ProjectSettingsBlocker { }

    public class EditorCoroutine
    {
        private Stack<IEnumerator> enumerators;

        public static EditorCoroutine Start( IEnumerator instruction )
        {
            return new EditorCoroutine( instruction );
        }

        private EditorCoroutine( IEnumerator instruction )
        {
            enumerators = new Stack<IEnumerator>( 1 );
            enumerators.Push( instruction );
            EditorApplication.update += Update;
        }

        public void Stop()
        {
            EditorApplication.update -= Update;
        }
        private void Update()
        {
            try
            {
                if (enumerators.Count > 0)
                {
                    if (enumerators.Peek().MoveNext())
                        Call( enumerators.Peek().Current );
                    else
                        enumerators.Pop();
                }
                else
                {
                    Stop();
                }
            }
            catch (Exception e)
            {
                Debug.LogError( e );
            }
        }
        private void Call( object curr )
        {
            if (curr != null && curr is IEnumerator)
            {
                enumerators.Push( ( IEnumerator )curr );
            }
        }
    }
}