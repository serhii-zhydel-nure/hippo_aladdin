﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using PSV.Localization;
using System.IO;

namespace PSV.SettingsEditor
{
    public class PSVResourcesSettings : PSVWindowElement
    {
        protected const string path_to_android_res_str_folder = "Assets/Plugins/Android/res/values";
        protected const string path_to_popularity_rating_data_prefix = "Assets/Resources/";
        protected const string path_to_obsolete_locale_strings = "Assets/Resources/strings.csv";
        protected const string path_to_android_res_log = "ProjectSettings/LogTranslateAppResValues.txt";
        protected const int maximum_res_value_lenght = 30;
        protected const string name_popularity_const_scripts = "PopularityRatingConst";

        protected bool requere_download_popularity_data = false;
        protected bool requere_generate_android_res = false;

        public PSVResourcesSettings()
        {
#if !NO_POPULARITY_MODULE
            requere_download_popularity_data = !File.Exists( GetPathToResourceRatingFile() );
#endif
            requere_generate_android_res = !Directory.Exists( path_to_android_res_str_folder );
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Resources Settings", true ))
            {
                using (Box.Scope())
                    ShowSettingsLocalizationText();

                if (settingsTarget == BuildTargetGroup.Android)
                    using (Box.Scope())
                        ShowSettingsAndroidResourceName();

#if !NO_POPULARITY_MODULE
                using (Box.Scope())
                    ShowSettingsPopularityRating();
#endif
                using (Box.Scope())
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label( badge_new_icon, GUILayout.ExpandWidth( false ) );
                    EditorGUILayout.LabelField( "Used assets in build", EditorStyles.boldLabel );
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label( "Find and move all unused assets in build:", GUILayout.ExpandWidth( false ) );
                    if (GUILayout.Button("Move to folder " + FindNotUsedFiles.notUsedFolderPath, EditorStyles.miniButton, GUILayout.ExpandWidth( false ) ))
                    {
                        GUI.changed = false;
                        FindNotUsedFiles.MoveNotUsedAssets();
                    }
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label( "Read last build info:", GUILayout.ExpandWidth( false ) );
                    ShowBtnSelectAsset( FindNotUsedFiles.lastBuildInfoPath, true );
                    EditorGUILayout.EndHorizontal();
                    GUILayout.Label( "Before find unused assets need do build with all the scenes.", EditorStyles.centeredGreyMiniLabel );
                    GUILayout.Label( "For disable open info after each build need delete script ShowUsedAssetsAfterBuild.cs", EditorStyles.centeredGreyMiniLabel );
                }
            }
            if (requere_generate_android_res && settingsTarget == BuildTargetGroup.Android)
            {
                EditorGUILayout.HelpBox( "Android resources strings not found! Please create game name strings. " + path_to_android_res_str_folder, MessageType.Warning );
            }
            if (requere_download_popularity_data)
            {
                EditorGUILayout.HelpBox( "Please download Popularity Rating Data. And update data before release.", MessageType.Error );
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        public static void MoveSoundsFolder( bool toResources )
        {
            if (toResources)
                AssetsHelper.TryMoveFolder( EditorPSVSettings.path_sounds_in_resources, EditorPSVSettings.settings.path_reses_assetbundles );
            else
                AssetsHelper.TryMoveFolder( EditorPSVSettings.settings.path_reses_assetbundles, EditorPSVSettings.path_sounds_in_resources );
        }

        #region Localization text resources
        private void ShowSettingsLocalizationText()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField( "Localization Text:", EditorStyles.boldLabel );
            GUILayout.Label( "Format:", GUILayout.ExpandWidth( false ) );
            settings.locale_text_format = ( TextFormat )EditorGUILayout.EnumPopup( settings.locale_text_format, GUILayout.Width( 100.0f ) );
            EditorGUILayout.EndHorizontal();
            switch (settings.locale_text_format)
            {
                case TextFormat.none:
                EditorGUILayout.HelpBox( "Is now disabled. Select format for enable.", MessageType.Info );
                break;
                case TextFormat.Obsolete:
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label( "Path:", GUILayout.ExpandWidth( false ) );
                ShowField( ref settings.locale_text_path );
                EditorGUILayout.EndHorizontal();
                if (settings.locale_text_path.StartsWith( "http" ))
                {
                    EditorGUILayout.HelpBox( "Example: " + settings.locale_text_path.ToString(), MessageType.None );
                }
                else
                {
                    int index_dot = settings.locale_text_path.LastIndexOf( '.' );
                    if (index_dot > 0)
                    {
                        EditorGUILayout.HelpBox( "Remove file extensions: " + settings.locale_text_path.Substring( index_dot,
                            settings.locale_text_path.Length - index_dot ), MessageType.Error );
                    }
                    else
                    {
                        EditorGUILayout.HelpBox( "Example: Assets/Resource/" + settings.locale_text_path.ToString(), MessageType.None );
                    }
                }
                break;
                case TextFormat.json:
                case TextFormat.xml:
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label( "Path:", GUILayout.ExpandWidth( false ) );
                ShowField( ref settings.locale_text_path );
                GUILayout.Label( "language*." + settings.locale_text_format.ToString(), GUILayout.ExpandWidth( false ) );
                EditorGUILayout.EndHorizontal();
                bool is_url_link = settings.locale_text_path.StartsWith( "http" );
                EditorGUILayout.HelpBox( "Example: " + ( is_url_link ? string.Empty : "Assets/Resource/" )
                    + settings.locale_text_path.ToString() + Languages.Language.Russian.ToString()
                    + "." + settings.locale_text_format.ToString(), MessageType.None );

                EditorGUILayout.BeginHorizontal();
                GUILayout.Label( "Сreate template files for all available languages:", GUILayout.ExpandWidth( false ) );
                if (GUILayout.Button( "Create", EditorStyles.miniButton, GUILayout.Width( 80.0f ) ))
                {
                    СreateTemplateFilesForLanguages();
                    GUI.changed = false;
                }
                EditorGUILayout.EndHorizontal();

                if (File.Exists( path_to_obsolete_locale_strings ))
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.HelpBox( "Localized text is now stored in a new " + settings.locale_text_format.ToString()
                        + " format. Rewrite the 'strings' file in the new format or delete it from the Resources folder", MessageType.Warning );
                    if (GUILayout.Button( "Reserialize To " + settings.locale_text_format.ToString(), GUILayout.Height( 40.0f ) ))
                    {
                        ReserializeObsoleteLocaleTextFiles();
                        GUI.changed = false;
                    }
                    GUILayout.EndHorizontal();
                }

                EditorGUILayout.Space();
                GUILayout.Label( "* Includes all available languages in project. On runtime 'language' replace to current language name.", EditorStyles.wordWrappedMiniLabel );

                break;
            }
        }

        private void ReserializeObsoleteLocaleTextFiles()
        {
            EditorUtility.DisplayProgressBar( "Reserialize strings file", "Create localization text files.", 1.0f );
            var localeAsset = Resources.Load<TextAsset>( "strings" );
            List<Dictionary<string, object>> data = CSVReader.Read( localeAsset.text );

            var langs_enumerator = data[0].Keys.GetEnumerator();
            int lang_count = data[0].Count - 1; // Count - Key column
            langs_enumerator.MoveNext();
            string key_key_column = langs_enumerator.Current;
            for (int i = 0; langs_enumerator.MoveNext(); i++)
            {
                string lang_name = langs_enumerator.Current;
                LocalTextData[] text_for_lang = new LocalTextData[data.Count];
                for (int j = 0; j < data.Count; j++)
                {
                    text_for_lang[j] = new LocalTextData()
                    {
                        key = data[j][key_key_column].ToString().Trim(),
                        text = data[j][lang_name].ToString().Trim()
                    };
                }
                CreateLocaleTextAsset( new LocalizationText() { data = text_for_lang }, settings.locale_text_path, lang_name, i == lang_count - 1 );
            }

            EditorUtility.ClearProgressBar();
            EditorUtility.DisplayDialog( "Reserialize strings file", "Localization text files created.", "OK" );
        }

        public static void CreateLocaleTextAsset( LocalizationText data, string path, string lang, bool show_file )
        {
            if (path.StartsWith( "http" ))
            {
                Debug.LogError( "TextAsset " + path + " was not created since URL are not supported." );
                return;
            }
            path = "Assets/Resources/" + path;
            string folder = path.Substring( 0, path.LastIndexOf( '/' ) );
            path += lang;
            switch (PSVSettings.settings.locale_text_format)
            {
                case TextFormat.json:
                path += LoadableLangText.json_format;
                if (File.Exists( path ))
                {
                    Debug.LogError( "TextAsset " + path + " was not created since it already exists." );
                    return;
                }
                AssetsHelper.CreateFolder( folder );
                File.WriteAllText( path, JsonUtility.ToJson( data, true ) );
                break;
                case TextFormat.xml:
                path += LoadableLangText.xml_format;
                if (File.Exists( path ))
                {
                    Debug.LogError( "TextAsset " + path + " was not created since it already exists." );
                    return;
                }
                AssetsHelper.CreateFolder( folder );
                var serializer = new System.Xml.Serialization.XmlSerializer( typeof( LocalizationText ) );
                using (var writer = new StreamWriter( path ))
                {
                    serializer.Serialize( writer, data );
                }
                break;
                default:
                return;
            }

            AssetDatabase.ImportAsset( path );
            if (show_file)
            {
                AssetsHelper.SelectFile( path, false );
            }
        }
        public void СreateTemplateFilesForLanguages()
        {
            EditorUtility.DisplayProgressBar( "Reserialize strings file", "Create localization text files.", 0.1f );
            LocalizationText empty_data = new LocalizationText()
            {
                data = new LocalTextData[]
                { new LocalTextData(){ key = "test", text = "empty" } }
            };

            for (int i = 0; i < settings.available_languages.Length; i++)
            {
                CreateLocaleTextAsset( empty_data, settings.locale_text_path,
                    settings.available_languages[i].ToString(), i == settings.available_languages.Length );
            }
            EditorUtility.ClearProgressBar();
            EditorUtility.DisplayDialog( "Reserialize strings file", "Localization text files created.", "OK" );
        }
        #endregion

        #region Android/res/values language names
        private void ShowSettingsAndroidResourceName()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField( "Android resources strings:", EditorStyles.boldLabel, GUILayout.ExpandWidth( false ) );
            GUILayout.Label( path_to_android_res_str_folder, EditorStyles.centeredGreyMiniLabel );
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( "English name: ", GUILayout.ExpandWidth( false ) );
            string engName = GetAndroidResNameFile( PlayerSettings.productName );
            EditorGUILayout.LabelField( engName, EditorStyles.boldLabel );
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( "Russian name:", GUILayout.ExpandWidth( false ) );
            editor_settings.cis_application_name = GetAndroidResNameFile( EditorGUILayout.TextField(
                editor_settings.cis_application_name ), false );
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();

            if (engName.Length > maximum_res_value_lenght)
                EditorGUILayout.HelpBox( "The length of the name can not exceed "
                    + maximum_res_value_lenght + " characters. Please change the name in Player settings.", MessageType.Error );
            else if (editor_settings.cis_application_name.Length > maximum_res_value_lenght)
                EditorGUILayout.HelpBox( "The length of the name can not exceed "
                    + maximum_res_value_lenght + " characters. Please change the Russian name.", MessageType.Error );
            else
            {
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label( "Translations the application name:", GUILayout.ExpandWidth( false ) );
                ShowBtnSelectAsset( path_to_android_res_str_folder );
                if (GUILayout.Button( "Generate", EditorStyles.miniButton, GUILayout.Width( 100.0f ) ))
                {
                    GUI.changed = false;
                    EditorCoroutine.Start( CreateLocalAndroidResFiles() );
                }
                EditorGUILayout.EndHorizontal();
            }
            ShowBtnSelectAsset( "Open Log last translations name:  ", path_to_android_res_log, true );
        }

        private string GetAndroidResNameFile( string name, bool toLower = true )
        {
            if (string.IsNullOrEmpty( name ))
                return string.Empty;
            if (toLower)
                name = name.ToLower();
            string result = string.Empty;
            bool firstChar = true;
            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] != ':' && name[i] != ',' && System.Char.IsPunctuation( name[i] ))
                    result += ' ';
                else if (firstChar)
                {
                    result += char.ToUpper( name[i] );
                    firstChar = false;
                }
                else
                    result += name[i];
            }
            result = result.Trim();
            return result;
        }

        private string CreateAndroidResString( string app_name, string lang )
        {
            if (string.IsNullOrEmpty( app_name ))
            {
                return "Translation does not exist for lang [" + lang + "] Create resource string skiped!";
            }
            if (app_name.Length > maximum_res_value_lenght)
            {
                return "Symbol limit[" + maximum_res_value_lenght
                    + "] exceeded for app name lang [" + lang + "] = '" + app_name + "' Create resource value skiped!";
            }
            string foldern_path = path_to_android_res_str_folder + lang;
            foldern_path = foldern_path.TrimEnd( ' ' );
            AssetsHelper.CreateFolder( foldern_path );

            app_name = GetAndroidResNameFile( app_name, false );

            string text = "<?xml version=\"1.0\" encoding=\"utf-8\"?><resources><string name=\"app_name\">" + app_name + "</string></resources>";
            File.WriteAllText( foldern_path + "/strings.xml", text );
            return "Translate to [" + lang + "] = " + app_name;
        }

        private IEnumerator CreateLocalAndroidResFiles()
        {
            if (string.IsNullOrEmpty( editor_settings.cis_application_name ))
            {
                EditorUtility.DisplayDialog( "Android resource strings", "ERROR!\nPlease specify russian application name!", "OK" );
                yield break;
            }
            string textDialog = "Plugins/Android/res/values localization names.\nEnglish: "
                   + GetAndroidResNameFile( PlayerSettings.productName )
                   + "\nCIS: " + editor_settings.cis_application_name
                   + "\n\nDo you want to translate the name of the application to all languages?";
            if (!EditorUtility.DisplayDialog( "Preparation Android resource strings", textDialog, "Create res", "Cancel" ))
            {
                yield break;
            }

            EditorApplication.ExecuteMenuItem( "Window/Console" );

            string[] sng_langs = { "ru", "uk", "be" };
            // km, my - empty languages
            string[] langs = {
                     /*"en",*/ "fr", "es", "it", "de", "pt", /*"ru",*/ "pl", "hy", "az", "sq", "ar", "af", "eu", /*"be",*/ "bn", /*"my",*/ "bg",
                     "bs", "cy", "hu", "vi", "gl", "el", "ka", "gu", "da", "zu", "iw", "ig", "yi", "id", "ga", "is", "yo", "kk",
                     "kn", "ca", "ko", "ht", /*"km",*/ "lo", "la", "lv", "lt", "mk", "mg", "ms", "ml", "mt", "mi",
                     "mr", "mn", "ne", "nl", "no", "pa", "fa", "ro", "sr", "st", "si", "sk", "sl", "so", "sw", "su", "tl", "ja",
                     "tg", "th", "ta", "te", "tr", "uz", /*"uk",*/ "ur", "fi", "hi", "hr", "ny", "cs", "sv", "eo", "et", "jw" };
            string root = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto";
            string eng_name = GetAndroidResNameFile( PlayerSettings.productName );
            string url_en_name = "&dt=t&q=" + WWW.EscapeURL( eng_name );

            int done_count = 0;
            int count = sng_langs.Length + langs.Length + 1;

            StreamWriter logWriter = new StreamWriter( path_to_android_res_log, false );
            logWriter.WriteLine( "Last log date: " + System.DateTime.Now.ToString() );
            logWriter.WriteLine( "Original: " + eng_name );
            string logText;

            for (int i = 0; i < langs.Length; i++, done_count++)
            {
                if (EditorUtility.DisplayCancelableProgressBar( "Create Android resource strings", "Game name translate to " + langs[done_count] + " language.", ( float )done_count / count ))
                {
                    logWriter.Close();
                    EditorUtility.ClearProgressBar();
                    yield break;
                }
                string url = root + "&tl=" + langs[done_count] + url_en_name;
                using (WWW www = new WWW( url ))
                {
                    while (!www.isDone)
                    {
                        yield return null;
                    }
                    if (string.IsNullOrEmpty( www.error ))
                    {
                        string translated_text = www.text.Split( '"' )[1];
                        logText = CreateAndroidResString( translated_text, '-' + langs[done_count] );
                        Debug.Log( logText );
                        logWriter.WriteLine( logText );
                    }
                }
            }
            for (int i = 0; i < sng_langs.Length; i++, done_count++) // create SNG strings
            {
                if (EditorUtility.DisplayCancelableProgressBar( "Create Android resource strings", "Game name translate to " + sng_langs[i] + " language.", ( float )done_count / count ))
                {
                    logWriter.Close();
                    EditorUtility.ClearProgressBar();
                    yield break;
                }
                string url = root + "&tl=" + sng_langs[i] + url_en_name;
                using (WWW www = new WWW( url ))
                {
                    while (!www.isDone)
                    {
                        yield return null;
                    }
                    if (string.IsNullOrEmpty( www.error ))
                    {
                        logText = CreateAndroidResString( editor_settings.cis_application_name, '-' + sng_langs[i] );
                        Debug.Log( logText );
                        logWriter.WriteLine( logText );
                    }
                }
            }
            logText = CreateAndroidResString( eng_name, string.Empty ); // Create empty string
            //CreateAndroidResString( eng_name, "en" ); // Create Eng string

            Debug.Log( logText );
            logWriter.WriteLine( logText );
            logWriter.Close();
            requere_generate_android_res = false;
            EditorUtility.ClearProgressBar();
            AssetsHelper.SelectFile( path_to_android_res_log, true );
        }
        #endregion

        #region Popularity rating
#if !NO_POPULARITY_MODULE
        private void ShowSettingsPopularityRating()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( badge_new_icon, GUILayout.ExpandWidth( false ) );
            EditorGUILayout.LabelField( "Popularity games:", EditorStyles.boldLabel );
            EditorGUILayout.EndHorizontal();
            ShowBtnSelectScript( "Modify module constants in scripts", name_popularity_const_scripts );
            GUILayout.Label( "Data file location:" );
            EditorGUILayout.HelpBox( "On device: " + GetPathToResourceRatingFile(), MessageType.None );
            EditorGUILayout.HelpBox( "Server URL: " + PSV.PopularNotify.PopularityRating.dataFileServerURL, MessageType.None );

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( "Update data file in resource folder:", GUILayout.ExpandWidth( false ) );
            if (GUILayout.Button( "Download", EditorStyles.miniButton, GUILayout.Width( 100.0f ) ))
            {
                EditorCoroutine.Start( DownloadPopularityData() );
                GUI.changed = false;
            }
            ShowBtnSelectAsset( GetPathToResourceRatingFile() );
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.HelpBox( "Please download popularity rating games data to Resource folder before release build!", MessageType.Info );
        }

        private IEnumerator DownloadPopularityData()
        {
            if (Application.internetReachability != NetworkReachability.NotReachable)
            {
                using (WWW downloader = new WWW( PSV.PopularNotify.PopularityRating.dataFileServerURL ))
                {
                    while (!downloader.isDone)
                    {
                        EditorUtility.DisplayProgressBar( "Download Popularity Rating data",
                            PopularNotify.PopularityRating.dataFileServerURL,
                            Mathf.Repeat( ( float )EditorApplication.timeSinceStartup, 1.0f ) );
                        yield return null;
                    }
                    EditorUtility.ClearProgressBar();
                    if (string.IsNullOrEmpty( downloader.error ))
                    {
                        string path = GetPathToResourceRatingFile();
                        File.WriteAllText( path, downloader.text );
                        Debug.Log( "Created: " + path );

                        requere_download_popularity_data = false;
                        AssetDatabase.ImportAsset( path );
                        AssetsHelper.SelectFile( path, false );
                    }
                    else
                    {
                        Debug.LogError( "PopularityData: data download eror: " + downloader.error
                            + ". URL:" + PopularNotify.PopularityRating.dataFileServerURL );
                    }
                }
            }
            else
            {
                Debug.LogError( "PopularityData: download canceled. Network not reachable!" );
            }
        }

        private static string GetPathToResourceRatingFile()
        {
            return path_to_popularity_rating_data_prefix + PSV.PopularNotify.PopularityRating.dataFileResourcePath
                + LoadableLangText.json_format;
        }
#endif
        #endregion
    }
}