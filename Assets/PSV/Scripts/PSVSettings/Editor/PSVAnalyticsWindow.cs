﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;

namespace PSV.SettingsEditor
{
    public class PSVAnalyticsWindow : PSVWindowElement
    {
        private const string analytics_list_name = "AnalyticsEvents";


        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Analytics Settings" ))
            {
                ShowBtnSelectScript( "For add new analytic events need to modify the script", analytics_list_name );
                switch (settingsTarget)
                {
                    case BuildTargetGroup.Android:
                    EditorGUI.BeginChangeCheck();
                    string newAndroid = EditorGUILayout.TextField( IconContent( BuildTargetGroup.Android, "Android ID:" ), settings.android_analytics_id ).Trim( ' ' );
                    if (EditorGUI.EndChangeCheck())
                    {
                        if (settings.ios_analytics_id == settings.android_analytics_id)
                            settings.ios_analytics_id = newAndroid;
                        settings.android_analytics_id = newAndroid;
                    }
                    break;
                    case BuildTargetGroup.iOS:
                    EditorGUI.BeginChangeCheck();
                    string newIOS = EditorGUILayout.TextField( IconContent( BuildTargetGroup.iOS, "IOS ID:" ), settings.ios_analytics_id ).Trim( ' ' );
                    if (EditorGUI.EndChangeCheck())
                    {
                        settings.ios_analytics_id = newIOS;
                    }
                    break;
                }
            }
            if (( settingsTarget == BuildTargetGroup.Android && settings.android_analytics_id.Length == 0 )
                || ( settingsTarget == BuildTargetGroup.iOS && settings.ios_analytics_id.Length == 0 ))
            {
                EditorGUILayout.HelpBox( "Please specify IDs of Analytics.", MessageType.Warning );
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }
    }
}