﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using System.Collections.Generic;
using PSV.IAP;

namespace PSV.SettingsEditor
{
    public class PSVPurchaiseWindow : PSVWindowElement
    {
        protected const string name_billing_products_script = "BillingManagerProducts";
        protected const string purchased_prefs = "BillingManager:Product:";

        protected GUIContent android_products_content;
        protected GUIContent ios_products_content;

        protected List<bool> purchasedProducts;

        public PSVPurchaiseWindow()
        {
            android_products_content = new GUIContent( android_icon.image )
            {
                text = "Android products:",
                tooltip = "List here as constants all your products, registered in GooglePlay console"
            };
            ios_products_content = new GUIContent( ios_icon.image )
            {
                text = "IOS products:",
                tooltip = "IOS SKUs are unique per account, so we have to add app's bundle to common product name"
            };
            purchasedProducts = new List<bool>( settings.android_products_SKU.Length );
            for (int i = 0; i < settings.android_products_SKU.Length; i++)
            {
                var prop = settings.android_products_SKU[i];
                purchasedProducts.Add( IsPurchased( prop.product ) );
            }
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Products SKU", true ))
            {
                ShowBtnSelectScript( "For add new types of products need to modify the script",
                    name_billing_products_script );

                using (Box.Scope())
                {
                    EditorGUILayout.LabelField( "IAP purchase key:", EditorStyles.boldLabel, GUILayout.ExpandWidth( false ) );
                    ShowField( ref settings.public_key );
                }

                switch (settingsTarget)
                {
                    case BuildTargetGroup.Android:
                    ShowProductList( android_products_content, ref settings.android_products_SKU );
                    break;
                    case BuildTargetGroup.iOS:
                    ShowProductList( ios_products_content, ref settings.ios_products_SKU );
                    break;
                }

                //GUILayout.Label( "Example products:" );
                //EditorGUILayout.HelpBox( "Products.SKU_CONSUMABLE\t\t| cons_id\t\t| ProductType.Consumable", MessageType.None );
                //EditorGUILayout.HelpBox( "Products.SKU_PREMIUM_SUBSCRIPTION\t| premium\t| ProductType.Subscription", MessageType.None );

                EditorGUILayout.Space();
                using (Box.Scope())
                {
                    GUILayout.Label( "Products that should disable ads:" );
                    GUILayout.BeginHorizontal();
                    ShowIAPProductsElement( ref settings.ads_related_products[0], 0 );
                    GUILayout.Space( buttonLenght );
                    GUILayout.EndHorizontal();
                    EdditArray( ref settings.ads_related_products, ShowIAPProductsElement, null, 1 );
                }
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        private void ShowProductList( GUIContent label, ref ProductProperties[] array )
        {
            using (Box.Scope())
            {
                GUILayout.Label( label, EditorStyles.boldLabel );
                ShowHeadersTable();
                GUILayout.BeginHorizontal();
                ShowProductElement( ref array[0], 0 );
                GUILayout.Space( buttonLenght + 5.0f );
                GUILayout.EndHorizontal();
                EdditArray( ref array, ShowProductElement, OnChangeProductArray, 1 );
                GUILayout.Label( "'Purchased' checkbox is Editor only", EditorStyles.centeredGreyMiniLabel );
            }
        }
        private void ShowProductElement( ref ProductProperties element, int index )
        {
            if (element.sku == null)
                element = ProductProperties.empty;
            if (purchasedProducts.Count <= index)
                purchasedProducts.Add( IsPurchased( element.product ) );
            var changedProduct = ( Products )EditorGUILayout.EnumPopup( element.product, GUILayout.MaxWidth( 150.0f ) );
            if (changedProduct != element.product)
            {
                element.product = changedProduct;
                purchasedProducts[index] = IsPurchased( element.product );
                switch (settingsTarget)
                {
                    case BuildTargetGroup.Android:
                    settings.ios_products_SKU[index].product = changedProduct;
                    break;
                    case BuildTargetGroup.iOS:
                    settings.android_products_SKU[index].product = changedProduct;
                    break;
                }
            }
            element.sku = EditorGUILayout.TextField( element.sku ).Trim( ' ' );
            var changedType = ( ProductType )EditorGUILayout.EnumPopup( element.type, GUILayout.MaxWidth( 150.0f ) );
            if (changedType != element.type)
            {
                element.type = changedType;
                switch (settingsTarget)
                {
                    case BuildTargetGroup.Android:
                    settings.ios_products_SKU[index].type = changedType;
                    break;
                    case BuildTargetGroup.iOS:
                    settings.android_products_SKU[index].type = changedType;
                    break;
                }
            }
            bool isPurchased = purchasedProducts[index];
            if (isPurchased != GUILayout.Toggle( isPurchased, string.Empty, GUILayout.ExpandWidth( false ) ))
            {
                GUI.changed = false;
                purchasedProducts[index] = !isPurchased;
                SetPurchaisedEditor( element.product, !isPurchased );
            }
        }

        private bool OnChangeProductArray( bool isAdd, int index )
        {
            if (isAdd)
            {
                switch (settingsTarget)
                {
                    case BuildTargetGroup.Android:
                    purchasedProducts.Add( IsPurchased( settings.android_products_SKU[index].product ) );
                    AddArrayElementToEnd( ref settings.ios_products_SKU, ProductProperties.empty );
                    break;
                    case BuildTargetGroup.iOS:
                    purchasedProducts.Add( IsPurchased( settings.ios_products_SKU[index].product ) );
                    AddArrayElementToEnd( ref settings.android_products_SKU, ProductProperties.empty );
                    break;
                }
            }
            else
            {
                purchasedProducts.RemoveAt( index );
                switch (settingsTarget)
                {
                    case BuildTargetGroup.Android:
                    RemoveArrayElementAt( ref settings.ios_products_SKU, index );
                    break;
                    case BuildTargetGroup.iOS:
                    RemoveArrayElementAt( ref settings.android_products_SKU, index );
                    break;
                }

            }
            return true;
        }


        private void ShowIAPProductsElement( ref IAP.Products product, int index )
        {
            product = ( IAP.Products )EditorGUILayout.EnumPopup( product );
        }

        private void ShowHeadersTable()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( "Product", EditorStyles.centeredGreyMiniLabel, GUILayout.MaxWidth( 150.0f ) );
            GUILayout.Label( "SKU", EditorStyles.centeredGreyMiniLabel );
            GUILayout.Label( "Type", EditorStyles.centeredGreyMiniLabel, GUILayout.MaxWidth( 150.0f ) );
            GUILayout.Label( "Purchased", EditorStyles.miniLabel, GUILayout.MaxWidth( buttonLenght + 25.0f ) );
            EditorGUILayout.EndHorizontal();
        }

        public static void SetPurchaisedEditor( IAP.Products product, bool purchaise )
        {
            PlayerPrefs.SetInt( purchased_prefs + product.ToString(), purchaise ? 1 : 0 );
            PlayerPrefs.Save();
        }

        public static bool IsPurchased( IAP.Products product )
        {
            return PlayerPrefs.GetInt( purchased_prefs + product.ToString(), 0 ) == 1;
        }

    }
}