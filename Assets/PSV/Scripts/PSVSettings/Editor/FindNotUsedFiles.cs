﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using System.IO;
using System.Linq;
using UnityEditor.Callbacks;

namespace PSV.SettingsEditor
{
    public class FindNotUsedFiles
    {
        private const string tempLogStorage = "BuildFilesLogTemp.log";
        public const string lastBuildInfoPath = "LastBuildAssetsInfo.log";
        public const string notUsedFolderPath = "Assets/UnusedAssets";
        private const string usedFolderPath = "Assets/UsedAssets";

        private readonly string[] resourceExtensions =
        {
            ".png", ".jpg", ".ogg", ".anim", /*".prefab"*/
        };

        private readonly string[] ignoreFolders =
        {
            "/Editor/", "/Resources/", "/Gizmos/", "/Test", "/Plugins/"
        };

        private readonly string[] ignoreLabels =
        {
            "PSVPrototype"
        };

        public static void MoveNotUsedAssets()
        {
            if (EditorUtility.DisplayDialog( "Want to move unused Assets?", "All unused assets in last Build will be move to "
                + notUsedFolderPath + " folder.", "Move", "Cancel" ))
            {
                new FindNotUsedFiles().Action();
            }
        }

        private void Action()
        {
            if (EditorUtility.DisplayCancelableProgressBar( "Find build info", "Prepare last build assets info", 0.4f ))
            {
                EditorUtility.ClearProgressBar();
                return;
            }
            string logFile = SelectLastBuildInfo();
            if (logFile == null)
            {
                EditorUtility.ClearProgressBar();
                return;
            }
            
            if (EditorUtility.DisplayCancelableProgressBar( "Find build info", "Prepare last build assets info", 0.7f ))
            {
                EditorUtility.ClearProgressBar();
                return;
            }
            var assetsInBuild = logFile
                                .Split( new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None )
                                .Where( WhereNeedfulResources )
                                .Select<string, string>( SelectPathFromInfoLine )
                                .ToList();
            
            if (EditorUtility.DisplayCancelableProgressBar( "Find build info", "Prepare last build assets info", 0.9f ))
            {
                EditorUtility.ClearProgressBar();
                return;
            }
            AssetsHelper.CreateFolder( notUsedFolderPath );

            var allAsset = AssetDatabase.GetAllAssetPaths();
            int countAll = allAsset.Length;
            for (int i = 0; i < countAll; i++)
            {
                string currentAsset = allAsset[i];
                if (!WhereNeedfulResources( currentAsset ))
                    continue;
                bool skipThis = false;
                for (int ignoreIndex = 0; ignoreIndex < ignoreFolders.Length; ignoreIndex++)
                {
                    if (currentAsset.IndexOf( ignoreFolders[ignoreIndex] ) >= 0)
                    {
                        skipThis = true;
                        break;
                    }
                }
                var assetLabels = AssetDatabase.GetLabels( AssetDatabase.LoadAssetAtPath( currentAsset, typeof( UnityEngine.Object ) ) );
                for (int labelIndex = 0; labelIndex < assetLabels.Length; labelIndex++)
                {
                    for (int ignoreIndex = 0; ignoreIndex < ignoreLabels.Length; ignoreIndex++)
                    {
                        if (assetLabels[labelIndex].Equals( ignoreLabels[ignoreIndex] ))
                        {
                            skipThis = true;
                            break;
                        }
                    }
                }
                if (skipThis)
                    continue;
                if (EditorUtility.DisplayCancelableProgressBar( "Check of use assets", currentAsset, ( float )i / countAll ))
                {
                    EditorUtility.ClearProgressBar();
                    return;
                }
                bool inTrash = currentAsset.StartsWith( notUsedFolderPath );

                if (assetsInBuild.Contains( currentAsset ) == inTrash) // (Not usad and not in Trash) or (Is used and in Trash)
                {
                    AssetDatabase.MoveAsset( currentAsset, ( inTrash ? usedFolderPath : notUsedFolderPath ) + "/" + Path.GetFileName( currentAsset ) );
                }
            }
            
            EditorUtility.ClearProgressBar();
            AssetsHelper.SelectFile( notUsedFolderPath, false );
        }

        private bool WhereNeedfulResources( string line )
        {
            for (int i = 0; i < resourceExtensions.Length; i++)
            {
                if (line.EndsWith( resourceExtensions[i], StringComparison.OrdinalIgnoreCase ))
                    return true;
            }
            return false;
        }
        private string SelectPathFromInfoLine( string line )
        {
            int index = line.IndexOf( '%' );
            if (index > 0)
                return line.Substring( index + 2 ); // 2 offset for remove space
            return line;
        }
        
        public static string SelectLastBuildInfo()
        {
            string editorLogFile;
            switch (Application.platform)
            {
                case RuntimePlatform.WindowsEditor:
                editorLogFile = Environment.GetFolderPath( Environment.SpecialFolder.LocalApplicationData ) + "/Unity/Editor/Editor.log";
                break;
                case RuntimePlatform.OSXEditor:
                editorLogFile = "~/Library/Logs/Unity/Editor.log";
                break;
                default:
                Debug.LogError( "Are not supported platform!" );
                return null;
            }

            if (!File.Exists( editorLogFile ))
            {
                Debug.LogError( "Build info are not found. Do Build and try again." );
                return null;
            }
            if (File.Exists( tempLogStorage ))
                File.Delete( tempLogStorage );
            File.Copy( editorLogFile, tempLogStorage ); // We can't read unity editor log file IOException: Sharing violation
            string logFile = File.ReadAllText( tempLogStorage );
            File.Delete( tempLogStorage );

            string startTextInfoLine;
            string endTextInfoLine;
            if (Application.unityVersion.StartsWith( "201" ))
            {
                startTextInfoLine = "Build Report";
                endTextInfoLine = "-------------------------------------------------------------------------------";
            }
            else
            {
                startTextInfoLine = "Textures";
                endTextInfoLine = "AndroidSDKTools:";
            }

            int indexStart = logFile.LastIndexOf( "Used Assets and files from the Resources folder, sorted by uncompressed size:" );
            
            int indexEnd = logFile.LastIndexOf( endTextInfoLine );
            if (indexStart < 0 || indexEnd < 0)
            {
                if (File.Exists( lastBuildInfoPath ))
                {
                    logFile = File.ReadAllText( lastBuildInfoPath );
                    return logFile;
                }
                Debug.LogError( "Build info are not found. Do Build and try again." );

                return null;
            }
            int indexStartPercentInfo = logFile.LastIndexOf( startTextInfoLine, indexStart );

            logFile = logFile.Substring( indexStartPercentInfo, indexEnd - indexStartPercentInfo );
            File.WriteAllText( lastBuildInfoPath, logFile );
            return logFile;
        }

    }
}