﻿#if ASSET_BUNDLES_STORAGE
using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using PSV.AssetBundles;

namespace PSV.SettingsEditor
{
    public class PSVBundleManagerWindow : PSVWindowElement
    {
        public const string streamingAssetsInfoURL = "https://docs.unity3d.com/Manual/StreamingAssets.html";

        private bool can_moved_bundles = false;

        public PSVBundleManagerWindow()
        {
            can_moved_bundles = IsCanMoveBundlesToStreaming( EditorUserBuildSettings.activeBuildTarget );
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Asset Bundles Manager" ))
            {
#if UNITY_5_6_OR_NEWER
                using (Box.Scope())
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField( "Browser AssetBundles:", EditorStyles.boldLabel );
                    if (GUILayout.Button( "Open (Window/AssetBundle Browser)", EditorStyles.miniButton, GUILayout.ExpandWidth( false ) ))
                    {
                        GUI.changed = false;
                        UnityEngine.AssetBundles.AssetBundleBrowserMain.ShowWindow();
                    }
                    GUILayout.EndHorizontal();
                }
#else
                EditorGUILayout.HelpBox( "Recommended update unity to v5.6 or newer for use the AssetBundle Browser and eliminate the presence of duplicate resources.", MessageType.Info );
#endif
                using (Box.Scope())
                {
                    EditorGUILayout.LabelField( "Build bundles:", EditorStyles.boldLabel );
                    GUILayout.BeginHorizontal();
                    GUILayout.Label( "For current platform:", GUILayout.Width( 125.0f ) );
                    ShowBtnBuildBundles( "Target", EditorUserBuildSettings.activeBuildTarget, false );
                    ShowBtnBuildBundles( "Editor", BuildTargetForEditor(), false );
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.Label( "For the platforms:", GUILayout.Width( 125.0f ) );
                    ShowBtnBuildBundles( "Android", BuildTarget.Android );
                    ShowBtnBuildBundles( "IOS", BuildTarget.iOS );
                    ShowBtnBuildBundles( "OSX", BuildTargetOSX() );
                    ShowBtnBuildBundles( "Windows", BuildTarget.StandaloneWindows );
                    GUILayout.EndHorizontal();
                    EditorGUILayout.HelpBox( "You can create bundles much faster of selected files in Project window using the context menu: Build AssetBundles/Build from Selection.", MessageType.Info );
                }

                using (Box.Scope())
                {
                    EditorGUILayout.LabelField( "Storage bundles:", EditorStyles.boldLabel );
                    GUILayout.BeginHorizontal();
                    GUILayout.Label( "Buffer storage path:", GUILayout.ExpandWidth( false ) );
                    ShowPathFieldAssets( ref editor_settings.path_buffer_bundles );
                    GUILayout.EndHorizontal();

                    ShowToggleLeft( ref editor_settings.storage_target_platform_bundles_streaming,
                        "Moved target platform asset bundles to StreamingAssets* folder." );

                    if (editor_settings.storage_target_platform_bundles_streaming)
                    {
                        var targetPlatform = EditorUserBuildSettings.activeBuildTarget;
                        if (Event.current.isMouse)
                        {
                            if (IsCanMoveBundlesToStreaming( targetPlatform ) != can_moved_bundles)
                            {
                                can_moved_bundles = !can_moved_bundles;
                                return;
                            }
                        }
                        if (can_moved_bundles)
                        {
                            string platformFolderName = PathToBundle.GetPlatformFolderName( targetPlatform );

                            GUILayout.BeginHorizontal();
                            GUILayout.Label( "Found compiling bundles for " + targetPlatform.ToString(), GUILayout.ExpandWidth( false ) );

                            if (GUILayout.Button( "Move " + platformFolderName + " to StreamingAssets",
                                EditorStyles.miniButton, GUILayout.ExpandWidth( false ) ))
                            {
                                GUI.changed = false;
                                if (AssetsHelper.TryMoveFolder( PathToBundle.compilingFolder + platformFolderName,
                                                                PathToBundle.streamingFolder + platformFolderName ))
                                {
                                    MoveAllPlatformToBuffer( targetPlatform );
                                    can_moved_bundles = false;
                                }

                            }
                            GUILayout.EndHorizontal();
                        }
                    }
                    ShowBtnInfoURL( "* StreamingAssets are part of the installation archive, so AssetBundles should be\nstored there which will not be remotely located. It used for local storage bundles.",
                        streamingAssetsInfoURL );
                }
            }
        }

        private void ShowBtnBuildBundles( string label, BuildTarget platform, bool isSelectPath = true )
        {
            if (GUILayout.Button( label, EditorStyles.miniButton, GUILayout.Width( 70.0f ) ))
            {
                GUI.changed = false;
                Build( platform, null, isSelectPath );
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        public bool IsCanMoveBundlesToStreaming( BuildTarget platform )
        {
            string platformFolderName = PathToBundle.GetPlatformFolderName( platform );
            return AssetsHelper.IsCanMoveFolder( PathToBundle.compilingFolder + platformFolderName,
                                                 PathToBundle.streamingFolder + platformFolderName );
        }

        #region Build bundles
        public static void Build( BuildTarget platform, AssetBundleBuild[] builds = null, bool isStremingAssets = true, bool isSelectPath = false )
        {
            string path;
            isStremingAssets &= EditorPSVSettings.settings.storage_target_platform_bundles_streaming
                                && platform == EditorUserBuildSettings.activeBuildTarget;

            if (isStremingAssets)
            {
                MoveAllPlatformToBuffer( platform );
                if (TryMovePlatformToStreaming( platform ))
                    return;
                else
                    path = PathToBundle.streamingFolder + PathToBundle.GetPlatformFolderName( platform );
            }
            else
            {
                path = PathToBundle.compilingFolder + PathToBundle.GetPlatformFolderName( platform );
            }

            if (isSelectPath)
            {
                path = EditorUtility.SaveFolderPanel( "Select Platform folder storage",
                    ( isStremingAssets ? Application.streamingAssetsPath : EditorPSVSettings.settings.path_buffer_bundles ),
                    PathToBundle.GetPlatformFolderName( platform ) );
                if (string.IsNullOrEmpty( path ))
                {
                    return;
                }
            }
            else
            {
                string bundle_names_description;
                if (builds == null)
                {
                    bundle_names_description = "All bundle names in project.";
                }
                else
                {
                    bundle_names_description = "Bundle names: ";
                    for (int i = 0; i < builds.Length; i++)
                        bundle_names_description += builds[i].assetBundleName + "." + builds[i].assetBundleVariant + "; ";
                }
                if (!EditorUtility.DisplayDialog( "Create Asset Bundle for " + platform.ToString(),
                    "Create Asset Bundles in '" + path + "'?\n" + bundle_names_description,
                    "Build", "Cancel" ))
                {
                    return;
                }
            }

            BuildAssetBundleOptions options = BuildAssetBundleOptions.None;
            if (platform == BuildTargetOSX()
                || ( platform == BuildTarget.iOS && !PlayerSettings.iOS.useOnDemandResources )
                || platform == BuildTarget.StandaloneWindows || platform == BuildTarget.StandaloneWindows64)
                options |= BuildAssetBundleOptions.UncompressedAssetBundle;

            AssetsHelper.CreateFolder( path );

            if (builds == null || builds.Length == 0)
            {
                BuildPipeline.BuildAssetBundles( path, options, platform );
            }
            else
            {
                BuildPipeline.BuildAssetBundles( path, builds, options, platform );
            }
            Debug.Log( "AssetBundle compiled to " + path );
        }

        private static IEnumerator BuildSelectedBundles( BuildTarget platform, bool isStremingAssets = true, bool isSelectPath = false )
        {
            UnityEngine.Object[] selectedAssets = Selection.objects;
            List<AssetImporter> selectedImporters = new List<AssetImporter>();

            for (int i = 0; i < selectedAssets.Length; i++)
            {
                var assetPath = AssetDatabase.GetAssetPath( selectedAssets[i] );
                if (!string.IsNullOrEmpty( assetPath ))
                {
                    yield return GetAllAssetImporterAtPath( assetPath, selectedImporters );
                }
            }

            List<AssetBundleBuild> assetBundleBuilds = new List<AssetBundleBuild>();
            HashSet<string> processedBundles = new HashSet<string>();

            for (int i = 0; i < selectedImporters.Count; i++)
            {
                // Get asset bundle name & variant
                var assetBundleName = selectedImporters[i].assetBundleName;
                if (string.IsNullOrEmpty( assetBundleName ))
                {
                    continue;
                }

                EditorUtility.DisplayProgressBar( "Preparation AssetBundles for Build", string.Format( "Add file to build: " + assetBundleName ), ( float )selectedImporters.Count / i );
                yield return null;

                var assetBundleVariant = selectedImporters[i].assetBundleVariant;
                var assetBundleFullName = string.IsNullOrEmpty( assetBundleVariant ) ? assetBundleName : assetBundleName + "." + assetBundleVariant;

                // Only process assetBundleFullName once. No need to add it again.
                if (processedBundles.Contains( assetBundleFullName ))
                {
                    continue;
                }

                processedBundles.Add( assetBundleFullName );

                AssetBundleBuild build = new AssetBundleBuild
                {
                    assetBundleName = assetBundleName,
                    assetBundleVariant = assetBundleVariant,
                    assetNames = AssetDatabase.GetAssetPathsFromAssetBundle( assetBundleFullName )
                };

                assetBundleBuilds.Add( build );
            }
            if (assetBundleBuilds.Count > 0)
                Build( platform, assetBundleBuilds.ToArray(), isStremingAssets, isSelectPath );

            EditorUtility.ClearProgressBar();

        }

        [MenuItem( "Assets/AssetBundles/Build from Selection for Target" )]
        public static void BuildBundlesFromSelectionTarget()
        {
            EditorCoroutine.Start( BuildSelectedBundles( EditorUserBuildSettings.activeBuildTarget ) );
        }

        [MenuItem( "Assets/AssetBundles/Build from Selection for Editor" )]
        public static void BuildBundlesFromSelectionEditor()
        {
            EditorCoroutine.Start( BuildSelectedBundles( BuildTargetForEditor(), false ) );
        }

        #endregion

        public static void MoveAllPlatformToBuffer( BuildTarget exceptPlatform )
        {
            BuildTarget[] supportPlatforms = { BuildTarget.Android, BuildTarget.iOS, BuildTarget.StandaloneWindows64, BuildTarget.StandaloneWindows, BuildTargetOSX() };
            for (int i = 0; i < supportPlatforms.Length; i++)
            {
                if (supportPlatforms[i] != exceptPlatform)
                {
                    string platformFolderName = PathToBundle.GetPlatformFolderName( supportPlatforms[i] );
                    AssetsHelper.TryMoveFolder( PathToBundle.streamingFolder + platformFolderName,
                                                PathToBundle.compilingFolder + platformFolderName );
                }
            }
        }

        public static bool TryMovePlatformToStreaming( BuildTarget platform )
        {
            string platformFolderName = PathToBundle.GetPlatformFolderName( platform );
            return AssetsHelper.TryMoveFolder( PathToBundle.compilingFolder + platformFolderName,
                                               PathToBundle.streamingFolder + platformFolderName );
        }

        public static BuildTarget BuildTargetForEditor()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.IPhonePlayer:
                return BuildTargetOSX();
                default:
                return BuildTarget.StandaloneWindows;
            }
        }

        public static BuildTarget BuildTargetOSX()
        {
#if UNITY_2017_3_OR_NEWER
            return BuildTarget.StandaloneOSX;
#else
            return BuildTarget.StandaloneOSXUniversal;
#endif
        }

        public static IEnumerator GetAllAssetImporterAtPath( string path, List<AssetImporter> result )
        {
            if (result == null)
                result = new List<AssetImporter>();
            yield return AssetsHelper.HandleAllFilesAtPath( path, ( file ) =>
            {
                if (!file.EndsWith( ".meta" ))
                {
                    AssetImporter assetImporter = AssetImporter.GetAtPath( file );
                    if (assetImporter)
                    {
                        result.Add( assetImporter );
                    }
                }
            }, "Preparation AssetBundles Importers" );
        }
    }


#if UNITY_2017_1_OR_NEWER
    public class MovedBundlesOnSwitchTarget : UnityEditor.Build.IActiveBuildTargetChanged
    {
        public int callbackOrder { get { return 100; } }

        public void OnActiveBuildTargetChanged( BuildTarget previousTarget, BuildTarget newTarget )
        {
            // Enable|Disable moved bundles on select target platform.
            if (EditorPSVSettings.settings.storage_target_platform_bundles_streaming)
            {
                PSVBundleManagerWindow.MoveAllPlatformToBuffer( newTarget );
                PSVBundleManagerWindow.TryMovePlatformToStreaming( newTarget );
            }
        }
    }
#else
    [InitializeOnLoad]
    public class MovedBundlesOnSwitchTarget
    {
        static MovedBundlesOnSwitchTarget()
        {
            EditorUserBuildSettings.activeBuildTargetChanged += MoveBundlesForTargetPlatform;
        }
        private static void MoveBundlesForTargetPlatform()
        {
            // Enable|Disable moved bundles on select target platform.
            if (EditorPSVSettings.settings.storage_target_platform_bundles_streaming)
            {
                PSVBundleManagerWindow.MoveAllPlatformToBuffer( EditorUserBuildSettings.activeBuildTarget );
                PSVBundleManagerWindow.TryMovePlatformToStreaming( EditorUserBuildSettings.activeBuildTarget );
            }
        }
    }
#endif

}
#endif