﻿#if ASSET_BUNDLES_STORAGE
using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using System.Collections.Generic;

namespace PSV.SettingsEditor
{
    public class PSVBundleLabels : EditorWindow
    {
        private string[] allVariants;
        private string[] allNames;
        private string nameBundle = string.Empty;
        private string variant = string.Empty;
        private string folderPath;

        private void OnEnable()
        {
            var selected = Selection.activeObject;
            if (selected == null)
            {
                Debug.LogError( "Folder are not selected!" );
                return;
            }
            folderPath = AssetDatabase.GetAssetPath( Selection.activeObject );
            if (!AssetDatabase.IsValidFolder( folderPath ))
            {
                Debug.LogError( "Folder are not selected!" );
                return;
            }
            var internalMethod = typeof( AssetDatabase ).GetMethod( "GetAllAssetBundleVariants", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static );
            allVariants = ( string[] )internalMethod.Invoke( null, null );
            internalMethod = typeof( AssetDatabase ).GetMethod( "GetAllAssetBundleNamesWithoutVariant", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static );
            allNames = ( string[] )internalMethod.Invoke( null, null );
        }

        private void OnGUI()
        {
            if (string.IsNullOrEmpty( folderPath ))
            {
                Close();
                return;
            }
            GUILayout.Label( "AssetBundle" );

            GUILayout.BeginHorizontal();
            ShowSelectString( ref nameBundle, allNames );
            ShowSelectString( ref variant, allVariants, GUILayout.Width( 90.0f ) );
            GUILayout.EndHorizontal();

            GUILayout.FlexibleSpace();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button( "Set labels" ))
            {
                SetBundleNameForFolder( folderPath, nameBundle, variant );
                Close();
                return;
            }
            if (GUILayout.Button( "Cancel", GUILayout.Width( 110.0f ) ))
                Close();
            GUILayout.EndHorizontal();
        }

        private void ShowSelectString( ref string value, string[] array, params GUILayoutOption[] options )
        {
            int selected_index = EditorGUILayout.Popup( -1, array, GUILayout.Width( 18.0f ) );
            if (selected_index != -1)
            {
                value = array[selected_index];
            }
            PSVWindowElement.ShowField( ref value, options );
        }

        [MenuItem( "Assets/AssetBundles/Set labels" )]
        private static void ShowChangeAssetsLabel()
        {
            PSVBundleLabels window = ScriptableObject.CreateInstance<PSVBundleLabels>();
            window.position = new Rect( Screen.width / 2, Screen.height / 2, 400, 65 );
            window.ShowPopup();
        }

        [MenuItem( "Assets/AssetBundles/Set labels", true )]
        private static bool IsValidateMenuItem()
        {
            return Selection.activeObject.GetType() == typeof( DefaultAsset );
        }

        public static void SubscribeFilesToBundle()
        {
            EditorCoroutine.Start( SubscribeFilesToBundleAction() );
        }
        public static void SetBundleNameForFolder( string folder, string bundle_name_with_variant )
        {
            EditorCoroutine.Start( SetBundleNameForFolderAction( folder, bundle_name_with_variant ) );
        }
        public static void SetBundleNameForFolder( string folder, string name, string variant )
        {
            EditorCoroutine.Start( SetBundleNameForFolderAction( folder, name, variant ) );
        }

        private static IEnumerator SubscribeFilesToBundleAction()
        {
            string path_nonlanguage = EditorPSVSettings.settings.path_reses_assetbundles + "/" + EditorPSVSettings.settings.name_nonlanguage_sounds_folder;

            yield return SetBundleNameForFolderAction( path_nonlanguage, PSVSettings.settings.non_language_bundle_path.postfix );

            string path_language = EditorPSVSettings.settings.path_reses_assetbundles + "/" + EditorPSVSettings.settings.name_multilanguage_sounds_folder;
            for (int i = 0; i < PSVSettings.settings.available_languages.Length; i++)
            {
                string language_name = PSVSettings.settings.available_languages[i].ToString();
                yield return SetBundleNameForFolderAction( path_language + "/" + language_name, language_name.ToLower() + PSVSettings.settings.language_bundle_path.postfix );
            }
            EditorUtility.ClearProgressBar();
        }

        private static IEnumerator SetBundleNameForFolderAction( string folder, string bundle_name_with_variant )
        {
            string[] splitName = bundle_name_with_variant.Split( '.' );
            return SetBundleNameForFolderAction( folder, splitName[0], splitName.Length > 1 ? splitName[1] : string.Empty );
        }

        private static IEnumerator SetBundleNameForFolderAction( string folder, string bundle_name, string bundle_variant )
        {
            List<AssetImporter> allAssetImporters = new List<AssetImporter>();
            yield return PSVBundleManagerWindow.GetAllAssetImporterAtPath( folder, allAssetImporters );
            if (allAssetImporters.Count == 0)
                yield break;

            for (int i = 0; i < allAssetImporters.Count; i++)
            {
                allAssetImporters[i].SetAssetBundleNameAndVariant( bundle_name, bundle_variant );
            }
            EditorUtility.ClearProgressBar();
        }
    }
}
#endif