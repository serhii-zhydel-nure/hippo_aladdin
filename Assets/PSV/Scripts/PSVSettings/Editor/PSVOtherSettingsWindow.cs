﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;
using System.IO;

namespace PSV.SettingsEditor
{
    public class PSVOtherSettingsWindow : PSVWindowElement
    {
        protected const string name_const_settings_script = "ConstSettings";
        protected const string name_audio_settings_script = "AudioCommon";

        public PSVOtherSettingsWindow()
        {
            settings.portrait_orientation_only = OrientationIsPortrait();
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Other Settings" ))
            {
                using (Box.Scope())
                {
                    ShowBtnSelectScript( "Modify project constants in the script", name_const_settings_script );
                    ShowBtnSelectScript( "Modify Audio Groups in the script", name_audio_settings_script );
                }

                using (Box.Scope())
                {
                    GUILayout.Label( "Presentation", EditorStyles.boldLabel );
                    if (ShowToggleLeft( ref settings.portrait_orientation_only, "Portrait orientation game only" ))
                    {
                        PlayerSettings.defaultInterfaceOrientation = UIOrientation.AutoRotation;
                        PlayerSettings.allowedAutorotateToLandscapeLeft = !settings.portrait_orientation_only;
                        PlayerSettings.allowedAutorotateToPortrait = settings.portrait_orientation_only;
                        PlayerSettings.allowedAutorotateToLandscapeRight = !settings.portrait_orientation_only;
                        PlayerSettings.allowedAutorotateToPortraitUpsideDown = settings.portrait_orientation_only;
                    }
                }

                using (Box.Scope())
                {
                    GUILayout.Label( "Log debugging switches", EditorStyles.boldLabel );
                    GUILayout.BeginHorizontal();
                    GUILayout.Label( "To manage the logging in the whole project\nyou need to use PlayerSettings/Other Settings", EditorStyles.wordWrappedLabel, GUILayout.ExpandWidth( false ) );

                    if (GUILayout.Button( "Open", EditorStyles.miniButton, GUILayout.Width( buttonLenght ), GUILayout.ExpandWidth( false ) ))
                    {
                        EditorApplication.ExecuteMenuItem( "Edit/Project Settings/Player" );
                        GUI.changed = false;
                    }
                    GUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    ShowToggleLeft( ref settings.ads_manager_debug, "ADS Manager", GUILayout.Width( 200.0f ) );
                    ShowToggleLeft( ref settings.manager_google_debug, "Manager Google", GUILayout.Width( 200.0f ) );
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal();
                    //ShowToggleLeft( ref settings.firebase_manager_debug, "Firebase Manager", GUILayout.Width( 200.0f ) );
                    ShowToggleLeft( ref settings.billing_manager_debug, "Billing Manager", GUILayout.Width( 200.0f ) );
                    ShowToggleLeft( ref settings.analytics_manager_debug, "Analytics Manager", GUILayout.Width( 200.0f ) );
                    EditorGUILayout.EndHorizontal();
                }

                using (Box.Scope())
                {
                    GUILayout.Label( "Editor visualization", EditorStyles.boldLabel );

                    if (ShowToggleLeft( ref editor_settings.project_visualisation, "Project visualization" ))
                    {
                        VisualizationProject.enabled = editor_settings.project_visualisation;
                    }
                    if (editor_settings.project_visualisation)
                    {
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label( "Path to game content:", GUILayout.ExpandWidth( false ) );
                        ShowPathFieldAssets( ref editor_settings.path_game_folder );
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.Space();
                        EditorGUILayout.LabelField( "Project view indicators:" );
                        EditorGUILayout.LabelField( "New File/Folder - read the new features", VisualizationProject.Visualization.styleBackgroundNew );
                        EditorGUILayout.LabelField( "Obsolete File/Folder - remove it", VisualizationProject.Visualization.styleBackgroundRemoved );
                    }
                    else
                    {
                        EditorGUILayout.HelpBox( "Editor visualization is now disabled.", MessageType.Info );
                    }
                }
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        private static bool OrientationIsPortrait()
        {
            var orientation = PlayerSettings.defaultInterfaceOrientation;
            if (orientation == UIOrientation.Portrait || orientation == UIOrientation.PortraitUpsideDown)
            {
                return true;
            }
            else if (orientation == UIOrientation.AutoRotation)
            {
                if (PlayerSettings.allowedAutorotateToPortrait
                    && !PlayerSettings.allowedAutorotateToLandscapeRight
                    && !PlayerSettings.allowedAutorotateToLandscapeLeft)
                {
                    return true;
                }
            }
            return false;
        }
    }

    [InitializeOnLoad]
    public class VisualizationProject
    {
        public static Visualization[] elements;

        static VisualizationProject()
        {
            enabled = enabled;
        }

        public static bool enabled
        {
            get { return !Application.isPlaying && EditorPSVSettings.settings != null && EditorPSVSettings.settings.project_visualisation; }
            set
            {
                if (value)
                {
                    var obsoleteAssets = AssetDatabase.FindAssets( "l:Obsolete" );
                    elements = new Visualization[6 + obsoleteAssets.Length];
                    int i = 0;
                    elements[i++] = new Visualization( "Assets/PSV", VisualisationType.ChangeIcon, false, "scripts_icon_" );
                    elements[i++] = new Visualization( "Assets/Resources", VisualisationType.ChangeIcon, false, "green_icon_" );
                    elements[i++] = new Visualization( EditorPSVSettings.settings.path_game_folder, VisualisationType.ChangeIcon, false, "scenes_icon_" );
                    elements[i++] = new Visualization( "Assets/Plugins", VisualisationType.ChangeIcon, false, "plugins_icon_" );
                    while (i < obsoleteAssets.Length + 4)
                    {
                        elements[i] = new Visualization( obsoleteAssets[i - 4], VisualisationType.RemovedItem, true );
                        i++;
                    }

                    // New items Prototype 4.3
                    elements[i++] = new Visualization( "Assets/PSV/Scripts/NativeNotification", VisualisationType.NewItem, false );
                    elements[i++] = new Visualization( "Assets/Game/Scripts/CustomSettings", VisualisationType.NewItem, false );

                    EditorApplication.projectWindowItemOnGUI += ProjectWindowItemOnGUI;
                }
                else if (elements != null)
                {
                    elements = null;
                    EditorApplication.projectWindowItemOnGUI -= ProjectWindowItemOnGUI;
                }
            }
        }

        private static void ProjectWindowItemOnGUI( string GUID, Rect rect )
        {
            if (Event.current.type == EventType.Repaint)
            {
                for (int i = 0; i < elements.Length; i++)
                {
                    if (elements[i].Show( GUID, rect ))
                        return;
                }
            }
        }

        public enum VisualisationType : byte
        {
            Empty, ChangeIcon, NewItem, RemovedItem
        }

        public class Visualization
        {
            public readonly string guid;
            public VisualisationType type;

            private Texture2D smallContent;
            private Texture2D largeContent;

            private static GUIStyle backgroundNew;
            private static GUIStyle backgroundRemoved;

            public static GUIStyle styleBackgroundNew
            {
                get
                {
                    if (backgroundNew == null)
                        backgroundNew = CreateStyle( new Color( 0.0f, 1.0f, 0.0f, 0.1f ) );
                    return backgroundNew;
                }
            }
            public static GUIStyle styleBackgroundRemoved
            {
                get
                {
                    if (backgroundRemoved == null)
                        backgroundRemoved = CreateStyle( new Color( 1.0f, 0.0f, 0.0f, 0.2f ) );
                    return backgroundRemoved;
                }
            }

            /// <summary>
            /// </summary>
            /// <param name="name">Supported path or name file</param>
            /// <param name="content">Icon name in project</param>
            public Visualization( string name, VisualisationType type, bool isGuid, string content = "" )
            {
                this.type = type;

                if (isGuid)
                {
                    guid = name;
                }
                else if (name.LastIndexOf( '/' ) < 0)
                {
                    string[] search = AssetDatabase.FindAssets( name );
                    if (search.Length == 0)
                    {
                        this.type = VisualisationType.Empty;
                        return;
                    }
                    guid = search[0];
                }
                else
                {
                    if (!File.Exists( name ) && !Directory.Exists( name ))
                    {
                        this.type = VisualisationType.Empty;
                        return;
                    }
                    guid = AssetDatabase.AssetPathToGUID( name );
                }
                if (type == VisualisationType.ChangeIcon)
                {
                    smallContent = InitContent( content + "16" );
                    largeContent = InitContent( content + "64" );
                }
            }

            public bool Show( string GUID, Rect rect )
            {
                if (type == VisualisationType.Empty)
                    return false;
                if (GUID.Equals( guid ))
                {
                    switch (type)
                    {
                        case VisualisationType.ChangeIcon:
                        SetFolderTexture( rect );
                        break;
                        case VisualisationType.NewItem:
                        SetColorBackground( rect, styleBackgroundNew );
                        break;
                        case VisualisationType.RemovedItem:
                        SetColorBackground( rect, styleBackgroundRemoved );
                        break;
                    }
                    return true;
                }
                return false;
            }

            public void SetFolderTexture( Rect rect )
            {
                bool isSmall = rect.width > rect.height;

                if (isSmall)
                    rect.width = rect.height;
                else
                    rect.height = rect.width;

                if (rect.width > 64.0f)
                {
                    // center the icon if it is zoomed
                    var offset = ( rect.width - 64.0f ) / 2f;
                    rect = new Rect( rect.x + offset, rect.y + offset, 64.0f, 64.0f );
                }

                // unity shifted small icons a bit in 5.5
#if UNITY_5_5
                if (isSmall)
                    rect = new Rect( rect.x + 3, rect.y, rect.width, rect.height );
#elif UNITY_5_6_OR_NEWER
                if (isSmall && !IsTreeView( rect ))
                    rect = new Rect( rect.x + 3, rect.y, rect.width, rect.height );
#endif
                Texture icon = isSmall ? smallContent : largeContent;

                if (icon != null)
                    GUI.DrawTexture( rect, icon );
            }
            public void SetColorBackground( Rect rect, GUIStyle style )
            {
                GUI.Box( rect, GUIContent.none, style );
            }

            private Texture2D InitContent( string contentName )
            {
                var selectIcons = AssetDatabase.FindAssets( contentName );
                if (selectIcons.Length != 0)
                    return AssetDatabase.LoadAssetAtPath<Texture2D>( AssetDatabase.GUIDToAssetPath( selectIcons[0] ) );
                return null;
            }

            public static GUIStyle CreateStyle( Color colorBackground )
            {
                Texture2D texture = new Texture2D( 1, 1 );
                texture.SetPixel( 0, 0, colorBackground );
                texture.Apply();
                var style = new GUIStyle( GUI.skin.box );
                style.normal.background = texture;
                return style;
            }

            private static bool IsTreeView( Rect rect )
            {
                return ( rect.x - 16 ) % 14 == 0;
            }
        }
    }

}