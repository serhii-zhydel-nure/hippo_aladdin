﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;
using UnityEditor;
using System.Collections.Generic;

namespace PSV.SettingsEditor
{
    public class PSVSceneManagementWindow : PSVWindowElement
    {
        protected const string name_homeadsbox_module = "HomeAdsModule";
        protected const string name_rateme_module = "RateMeModue";
        protected const string name_purchaseme_module = "PurchaseDialogue";
        protected const string name_scene_settings = "SceneLoaderSettings";

        protected List<OfferGUI> allOffers;
        protected string requiredScenesInBuild;

        public PSVSceneManagementWindow()
        {
            allOffers = new List<OfferGUI>
            {
                OfferGUI.New( "SleeplessSceneOffer", "PSV", ShowToggleScreenOff, "The screen will not be turned off at the specified scenes." ),
                OfferGUI.New( "InterstitialAdsOffer", "PSV", null, "Interstitial Ads will not be shown after the specified scenes" ),
            };
            if (AssetsHelper.IsExistFolderInAssets( name_homeadsbox_module ))
                allOffers.Add( OfferGUI.New( "HomeAdsOffer", "PSV.HomeAds", null, "Home Ads box will be shown after the specified scenes" ) );
            if (AssetsHelper.IsExistFolderInAssets( name_rateme_module ))
                allOffers.Add( OfferGUI.New( "RateMeOffer", "PSV", null, "Rate me scene will be shown after the specified scenes" ) );
            if (AssetsHelper.IsExistFolderInAssets( name_purchaseme_module ))
                allOffers.Add( OfferGUI.New( "PurchaseOffer", "PSV", null, "Purchase sceme will be shown after the specified scenes" ) );
            CheckBuildScenesList();
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Scene Management Settings", true ))
            {
                using (Box.Scope())
                {
                    ShowBtnSelectScript( "For add new scenes need to modify the script", name_scene_settings );

                    GUILayout.BeginHorizontal();
                    GUILayout.Label( "Load scene on InitScene:", GUILayout.ExpandWidth( false ) );
                    ShowSceneElement( ref settings.first_scene );
                    ShowToggleLeft( ref editor_settings.splash_transition, new GUIContent( "Enabled(Editor Only)",
                        "Is need do switch to first scene on current scene id equal 0.\nLeave all scenes in Hierarchy without switching to splashes" ),
                        GUILayout.Width( 135.0f ) );
                    GUILayout.EndHorizontal();
                }

                using (Box.Scope())
                {
                    EditorGUILayout.LabelField( "Transition default:", EditorStyles.boldLabel, GUILayout.ExpandWidth( false ) );
                    GUILayout.BeginHorizontal();
                    GUILayout.Label( "Method:", GUILayout.ExpandWidth( false ) );
                    settings.transition_method = ( SceneLoader.TransitionMethod )EditorGUILayout.EnumPopup( settings.transition_method );
                    if (settings.transition_method == SceneLoader.TransitionMethod.Default)
                        settings.transition_method = SceneLoader.TransitionMethod.Tween;

                    GUILayout.Label( "Duration:", GUILayout.ExpandWidth( false ) );
                    settings.transition_duration = EditorGUILayout.FloatField( settings.transition_duration );
                    GUILayout.EndHorizontal();
                    ShowToggleLeft( ref settings.async_load, "Load scenes asynchronously." );
                }

                int offerIndex = 0;
                if (settings.never_sleep_screen)
                {
                    using (Box.Scope())
                    {
                        offerIndex = 1; // Skip show SleeplessSceneOffer
                        ShowToggleScreenOff();
                        EditorGUILayout.HelpBox( "Recommended allow the screen to sleep. And to define scenes on which the screen should not sleep. For example, such as cutscenes.",
                            MessageType.Warning );
                    }
                }

                for (; offerIndex < allOffers.Count; offerIndex++)
                {
                    allOffers[offerIndex].ShowOfferSelect();
                }

                EditorGUILayout.Space();

                using (Box.Scope())
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField( new GUIContent( "Splash scenes list:",
                        "List here all scenes that should be shown at the start of the game with some short lifetime" ), EditorStyles.boldLabel );
                    GUILayout.Label( "Splash Duration:", GUILayout.ExpandWidth( false ) );
                    settings.splash_duration = EditorGUILayout.FloatField( settings.splash_duration );
                    GUILayout.EndHorizontal();
                    EdditArray( ref settings.splash_scenes, ShowSceneElement );
                }
            }
            if (Event.current.isMouse)
                CheckBuildScenesList();
            else if (requiredScenesInBuild.Length > 0)
                EditorGUILayout.HelpBox( "Please add scenes to build: " + requiredScenesInBuild, MessageType.Error );
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        private void CheckBuildScenesList()
        {
            requiredScenesInBuild = string.Empty;
            EditorBuildSettingsScene[] buildScenes = EditorBuildSettings.scenes;
            for (int offerIndex = 0; offerIndex < allOffers.Count; offerIndex++)
            {
                Scenes offerScene = allOffers[offerIndex].offer.OfferScene();
                if (offerScene != Scenes.None)
                {
                    bool sceneExist = false;
                    for (int sceneIndex = 0; sceneIndex < buildScenes.Length; sceneIndex++)
                    {
                        if (buildScenes[sceneIndex].enabled && buildScenes[sceneIndex].path.EndsWith( offerScene.ToString() + ".unity" ))
                        {
                            sceneExist = true;
                            break;
                        }
                    }
                    if (!sceneExist)
                        requiredScenesInBuild += offerScene.ToString() + ", ";
                }
            }
        }
        private void ShowSceneElement( ref Scenes scene, int index = 0 )
        {
            scene = ( Scenes )EditorGUILayout.EnumPopup( scene, GUILayout.MaxWidth( 200.0f ) );
        }
        private bool ShowSceneElementNotNone( ref Scenes scene )
        {
            GUILayout.Label( "Offer scene:", EditorStyles.miniLabel, GUILayout.ExpandWidth( false ) );
            scene = ( Scenes )EditorGUILayout.EnumPopup( scene, GUILayout.MaxWidth( 200.0f ) );
            return scene != Scenes.None;
        }
        private void ShowToggleScreenOff()
        {
            ShowToggleLeft( ref settings.never_sleep_screen, "Never sleep screen", GUILayout.Width( 130.0f ) );
        }

        public class OfferGUI
        {
            public string nameScript;
            public ISceneOffer offer;
            public Action selectElement;
            public string description;

            public static OfferGUI New( string nameScript, string nameSpace, Action selectElement = null, string description = null )
            {
                var result = new OfferGUI
                {
                    nameScript = nameScript,
                    selectElement = selectElement,
                    description = description
                };

                Type typeOffer = typeof( OffersManager ).Assembly.GetType( nameSpace + "." + nameScript );
                if (typeOffer != null)
                {
                    result.offer = ( ISceneOffer )Activator.CreateInstance( typeOffer );
                }
                return result;
            }

            public void ShowOfferSelect()
            {
                if (string.IsNullOrEmpty( nameScript ) || offer == null)
                    return;

                using (Box.Scope( false ))
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField( nameScript + ":", EditorStyles.boldLabel, GUILayout.MinWidth( 130.0f ) );
                    if (offer.OfferScene() != Scenes.None)
                    {
                        GUILayout.Label( "Offer scene: " + offer.OfferScene().ToString(), GUILayout.ExpandWidth( false ) );
                    }
                    if (selectElement != null)
                        selectElement();
                    ShowBtnSelectScript( nameScript );
                    EditorGUILayout.EndHorizontal();


                    var scenes = offer.ScenesList();
                    string allScenes = "Scenes: ";
                    if (scenes.Length == 0)
                    {
                        allScenes += "not selected";
                    }
                    else
                    {
                        for (int i = 0; i < scenes.Length; i++)
                        {
                            allScenes += scenes[i] + "; ";
                        }
                    }
                    GUILayout.Label( allScenes, EditorStyles.wordWrappedLabel );

                    if (!string.IsNullOrEmpty( description ))
                    {
                        EditorGUILayout.LabelField( description, EditorStyles.centeredGreyMiniLabel );
                    }
                }
            }
        }
    }
}