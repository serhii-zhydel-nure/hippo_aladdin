﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PSV.SettingsEditor
{
    public class InternalIconViewer : PSVWindowElement
    {
        public const string psv_editor_file = "PSVSettingsWindow";
        public static bool developer_mode_enabled = false;

        public List<InternalIconContent> icons;

        public void FindIcons()
        {
            icons = new List<InternalIconContent>();
            Texture2D[] allTextures = Resources.FindObjectsOfTypeAll<Texture2D>();

#if UNITY_2017_1_OR_NEWER
            Debug.unityLogger.logEnabled = false;
#else
            Debug.logger.logEnabled = false;
#endif
            for (int i = 0; i < allTextures.Length; i++)
            {
                if (allTextures[i].name.Length == 0)
                    continue;
                //if (texture.hideFlags != HideFlags.HideAndDontSave && texture.hideFlags != ( HideFlags.HideInInspector | HideFlags.HideAndDontSave ))
                //    continue;
                if (!EditorUtility.IsPersistent( allTextures[i] ))
                    continue;

                GUIContent icon = EditorGUIUtility.IconContent( allTextures[i].name );
                if (icon == null || icon.image == null)
                    continue;
                icons.Add( new InternalIconContent( icon, allTextures[i].name ) );
            }
#if UNITY_2017_1_OR_NEWER
            Debug.unityLogger.logEnabled = true;
#else
            Debug.logger.logEnabled = true;
#endif
            icons.Sort();
            Resources.UnloadUnusedAssets();
            //System.GC.Collect();
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Internal Editor Icons" ))
            {
                if (icons == null)
                    FindIcons();

                GUILayoutOption heaghtElem = GUILayout.MaxHeight( EditorGUIUtility.singleLineHeight * 2.0f );
                for (int i = 0; i < icons.Count; i++)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField( icons[i].icon, heaghtElem );
                    EditorGUILayout.SelectableLabel( icons[i].name );
                    EditorGUILayout.EndHorizontal();
                }
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        public class InternalIconContent
        {
            public GUIContent icon;
            public string name;

            public InternalIconContent( GUIContent icon, string name )
            {
                this.icon = icon;
                this.name = name;
            }
        }
    }
}