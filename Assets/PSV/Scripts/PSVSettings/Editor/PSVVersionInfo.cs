﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using System.IO;

namespace PSV.SettingsEditor
{
    public class PSVVersionInfo : PSVWindowElement
    {
        public const string changelogDocumentURL = "https://docs.google.com/document/d/1hj_C2f5HOVSt_eNmqfhj8bRvV5FuMlMufjRQdJINnmI/edit?usp=sharing";
        public const string prototypeInfoURL = "https://stratos227.000webhostapp.com/PrototypeInfo.json";

        public ProtoypeInfoObject serverInfo;

        public bool isNeedUptade = false;

        public PSVVersionInfo()
        {
            CheckVersion();
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            EditorGUILayout.HelpBox( "New version prototype is available: " + serverInfo.info.version, MessageType.Warning );
            EditorGUILayout.BeginHorizontal();
            ShowBtnInfoURL( "Open change log.", changelogDocumentURL );
            GUILayout.FlexibleSpace();
            ShowToggleLeft( ref editor_settings.check_version_prototype, "Check version." );
            EditorGUILayout.EndHorizontal();
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        public override bool IsEnabled()
        {
            return editor_settings.check_version_prototype && isNeedUptade;
        }

        public void CheckVersion()
        {
            if (editor_settings.check_version_prototype)
            {
                if (!serverInfo)
                {
                    var allInfo = Resources.FindObjectsOfTypeAll<ProtoypeInfoObject>();
                    if (allInfo.Length == 0)
                    {
                        if (Application.internetReachability != NetworkReachability.NotReachable)
                        {
                            serverInfo = ScriptableObject.CreateInstance<ProtoypeInfoObject>();
                            serverInfo.hideFlags = HideFlags.HideAndDontSave;
                            EditorCoroutine.Start( serverInfo.DownloadLastVersion( CheckVersion ) );
                        }
                        return;
                    }
                    else
                    {
                        serverInfo = allInfo[0];
                    }
                }
                if (serverInfo.info != null)
                {
                    isNeedUptade = !string.IsNullOrEmpty( serverInfo.info.version ) && ConstSettings.prototypeVersion != serverInfo.info.version;
                    if (isNeedUptade)
                        Debug.LogWarning( "New version PROTOTYPE is available: " + serverInfo.info.version );
                }
            }
        }

        public void ToggleVersionChecked()
        {
            editor_settings.check_version_prototype = !editor_settings.check_version_prototype;
            CheckVersion();
        }

        public static void CreateCurrentInfoFile()
        {
            var info = new ProtoypeInfo
            {
                version = ConstSettings.prototypeVersion
            };
            string json = JsonUtility.ToJson( info );
            File.WriteAllText( "PrototypeInfo.json", json );
        }

    }
}