﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using PSV.ADS;

namespace PSV.SettingsEditor
{
    public class PSVAdsWindow : PSVWindowElement
    {
        private const string purchased_ads_pref = "AdmobDisabled";

        private bool required_specify_ads_settings = false;
        private ProviderParams providerParams;
        private bool editorAdsEnabled;

        public PSVAdsWindow() : base()
        {
            providerParams = new ProviderParams();
            PrepareProviders( ref settings.android_provider_settings );
            PrepareProviders( ref settings.ios_provider_settings );
            editorAdsEnabled = PlayerPrefs.GetInt( purchased_ads_pref, 0 ) == 0;
        }

        private void PrepareProviders( ref ProjectSettingsContainer.ProviderSettings[] elements )
        {
            for (int i = 0; i < elements.Length; i++)
            {
                if (elements[i] == null)
                {
                    required_specify_ads_settings = true;
                }
                else
                {
                    elements[i].RestoreObsolete( providerParams );
                    if (elements[i].value.Length == 0)
                    {
                        elements[i].value = AdsSettings.DEMO_ADS_PARAM;
                    }
                }
            }
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "ADS Settings", true ))
            {
                using (Box.Scope())
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label( "Delay time seconds:" );

                    if (ShowToggleLeft( ref editorAdsEnabled, "Editor Ads enabled", GUILayout.Width( 130.0f ) ))
                    {
                        GUI.changed = false;
                        if (editorAdsEnabled)
                            ManagerGoogle.EnableAdmob();
                        else
                            ManagerGoogle.DisableAdmob();
                        PSVPurchaiseWindow.SetPurchaisedEditor( IAP.Products.SKU_ADMOB, !editorAdsEnabled );
                    }
                    EditorGUILayout.EndHorizontal();
                    ShowDelayAds( ref settings.ads_interstitial_delay, "Interstitial:", "30" );
                    ShowDelayAds( ref settings.ads_rewarded_vidoe_delay, "Rewarded video:", " 0 " );
                }

                required_specify_ads_settings = false;
                switch (settingsTarget)
                {
                    case BuildTargetGroup.Android:
                    ShowAdsProviderList( IconContent( BuildTargetGroup.Android, "Android providers" ),
                                           ref settings.android_provider_settings,
                                           ref settings.android_providers_order );
                    break;
                    case BuildTargetGroup.iOS:
                    ShowAdsProviderList( IconContent( BuildTargetGroup.iOS, "IOS providers" ),
                                           ref settings.ios_provider_settings,
                                           ref settings.ios_providers_order );
                    break;
                }
            }
            if (required_specify_ads_settings)
            {
                EditorGUILayout.HelpBox( "Please specify ADS Provider settings or remove empty fields.", MessageType.Error );
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        private void ShowDelayAds( ref float delay, string label, string defVal )
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( label, GUILayout.Width( 110.0f ) );
            delay = Math.Max( 0.0f, EditorGUILayout.FloatField( delay ) );
            GUILayout.Label( "Default:" + defVal, GUILayout.ExpandWidth( false ) );
            EditorGUILayout.EndHorizontal();
        }

        private void ShowProviderSettingsElement( ref ProjectSettingsContainer.ProviderSettings element, int index )
        {
            if (element == null)
                element = new ProjectSettingsContainer.ProviderSettings( string.Empty, AdsSettings.DEMO_ADS_PARAM );

            int selected_index = EditorGUILayout.Popup( element.paramIndex, providerParams, GUILayout.Width( 18.0f ) );
            if (selected_index != element.paramIndex)
            {
                element.param = providerParams[selected_index];
                element.paramIndex = selected_index;
            }
            ShowField( ref element.param, GUILayout.MaxWidth( 210.0f ) );
            ShowField( ref element.value );
            if (element.param.Length == 0)
            {
                required_specify_ads_settings = true;
            }
        }

        private void ShowAdNetworkElement( ref AdNetwork provider, int index )
        {
            provider = ( AdNetwork )EditorGUILayout.EnumPopup( provider );
        }

        private void ShowAdsProviderList( GUIContent label, ref ProjectSettingsContainer.ProviderSettings[] settingList, ref AdNetwork[] networkList )
        {
            GUILayout.Label( label, EditorStyles.boldLabel );
            using (Box.Scope())
            {
                GUILayout.Label( "Settings" );
                EdditArray( ref settingList, ShowProviderSettingsElement );
            }
            using (Box.Scope())
            {
                GUILayout.Label( "Order" );
                EdditArray( ref networkList, ShowAdNetworkElement );
            }
        }
    }
}