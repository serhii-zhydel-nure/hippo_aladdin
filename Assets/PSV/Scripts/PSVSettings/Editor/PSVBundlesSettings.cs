﻿#if ASSET_BUNDLES_STORAGE
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.Collections;
using PSV.AssetBundles;

namespace PSV.SettingsEditor
{
    public class PSVBundleSettingsWindow : PSVWindowElement
    {
        private const string asset_bundle_usage_patterns_info = "https://unity3d.com/ru/learn/tutorials/topics/best-practices/assetbundle-usage-patterns";

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Asset Bundles Settings" ))
            {
                using (Box.Scope())
                {
                    GUILayout.BeginHorizontal();
                    EditorGUILayout.LabelField( "Cache asset bundles", EditorStyles.boldLabel );
                    if (GUILayout.Button( "Clear Editor Cache", EditorStyles.miniButton, GUILayout.ExpandWidth( false ) ))
                    {
                        GUI.changed = false;
                        ClearCacheBundles();
                    }
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    GUILayout.Label( "Global version:", GUILayout.ExpandWidth( false ) );
                    bool is_default = settings.custom_global_assets_version == 0;
                    if (is_default)
                    {
                        GUILayout.Label( settings.global_assets_version.ToString() );
                    }
                    else
                    {
                        settings.custom_global_assets_version = ( uint )EditorGUILayout.IntField( ( int )settings.custom_global_assets_version );
                    }
                    if (ShowToggleLeft( ref is_default, "Equal App version", GUILayout.Width( 130.0f ) ))
                    {
                        settings.custom_global_assets_version = ( uint )( is_default ? 0 : 1 );
                    }
                    GUILayout.EndHorizontal();
                    
                }

                using (Box.Scope())
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label( "LocalizationManager:", EditorStyles.boldLabel, GUILayout.ExpandWidth( false ) );
                    GUILayout.Label( "(Managed asset bundles)", EditorStyles.miniLabel );
                    ShowToggleLeft( ref editor_settings.always_local_url_in_editor, "Always local for editor", GUILayout.Width( 145.0f ) );
                    EditorGUILayout.EndHorizontal();
                    GUILayout.Space( 5.0f );

                    using (Box.Scope())
                    {
                        GUILayout.Label( "Source directory(Assets)" );
                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label( "Directory: ", GUILayout.ExpandWidth( false ) );
                        ShowPathFieldAssets( ref editor_settings.path_reses_assetbundles );
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label( "MultiLanguage:", GUILayout.ExpandWidth( false ) );
                        ShowField( ref editor_settings.name_multilanguage_sounds_folder );

                        GUILayout.Label( "NonLanguage:", GUILayout.ExpandWidth( false ) );
                        ShowField( ref editor_settings.name_nonlanguage_sounds_folder );
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        GUILayout.Label( "Set asset bundle labels for source directory:", GUILayout.ExpandWidth( false ) );
                        if (GUILayout.Button( "Set labels", EditorStyles.miniButton, GUILayout.Width( 80.0f ) ))
                        {
                            GUI.changed = false;
                            PSVBundleLabels.SubscribeFilesToBundle();
                        }
                        EditorGUILayout.EndHorizontal();
                    }

                    using (Box.Scope())
                    {
                        GUILayout.Label( "Multi-Language path to AssetBundles:" );
                        EditorGUILayout.BeginHorizontal();
                        ShowAssetBundlePath( ref settings.asssetbundle_pathes[0], Languages.Language.Russian );
                        EditorGUILayout.EndHorizontal();

                        GUILayout.Label( "Includes all available languages in project.\nOn runtime 'CurrentLang' replace to current language name.",
                            EditorStyles.centeredGreyMiniLabel );
                    }
                    using (Box.Scope( false ))
                    {
                        GUILayout.Label( "Non-Languages path to AssetBundles:" );
                        EditorGUILayout.BeginHorizontal();
                        ShowAssetBundlePath( ref settings.asssetbundle_pathes[1], 1 );
                        EditorGUILayout.EndHorizontal();
                    }
                }
                EditorGUILayout.Space();
                using (Box.Scope())
                {
                    EditorGUILayout.LabelField( "Unmanaged asset bundle path:", EditorStyles.boldLabel );
                    if (settings.asssetbundle_pathes.Length > 2)
                    {
                        ShowBtnInfoURL( "Before using Unmanaged asset bundle, please read usage patterns!", asset_bundle_usage_patterns_info );
                        GUILayout.Label( "For get path use PSVSettings.settings.asssetbundle_pathes[n]", EditorStyles.centeredGreyMiniLabel );
                    }
                    EdditArray( ref settings.asssetbundle_pathes, ShowAssetBundlePath, null, 2 );
                }
                GUILayout.Label( "* Pathways support multiple platform. On runtime 'Platform' replace\nto build platform name. For details, see also class PathToBundle.", EditorStyles.centeredGreyMiniLabel );

            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        private void ShowAssetBundlePathWithIndex(ref PathToBundle path, int index )
        {
            GUILayout.Label( index + ":", GUILayout.ExpandWidth( false ) );
            ShowAssetBundlePath( ref path, Languages.Language.None );
        }
        private void ShowAssetBundlePath( ref PathToBundle path, int index )
        {
            ShowAssetBundlePath( ref path, Languages.Language.None );
        }
        private void ShowAssetBundlePath( ref PathToBundle path, Languages.Language lang )
        {
            if (path == null)
                path = new PathToBundle( string.Empty );

            bool isUrl = !string.IsNullOrEmpty( path.prefix );
            string pathPlatformVersion = "[Platform*]/";
            
            if (isUrl != GUILayout.Toggle( isUrl, "URL:", EditorStyles.miniButton, GUILayout.ExpandWidth( false ) ))
            {
                isUrl = !isUrl;
                path.prefix = isUrl ? "https://" : string.Empty;
            }
            if (isUrl)
            {
                ShowField( ref path.prefix );
                pathPlatformVersion += settings.global_assets_version + "/";
            }
            else
            {
                pathPlatformVersion = "StreamingAssets/" + pathPlatformVersion;
            }
            if (lang != Languages.Language.None)
                pathPlatformVersion += "[CurrentLang]";

            GUILayout.Label( pathPlatformVersion, GUILayout.ExpandWidth( false ) );
            ShowField( ref path.postfix );
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (isUrl && !path.prefix.StartsWith( "http" ))
            {
                EditorGUILayout.HelpBox( "URL should be start with protocol: http / https", MessageType.Error );
            }
            else
            {
                string full_path = path.Path( lang, PathToBundle.GetPlatformFolderName( RuntimePlatform.Android ),
                    settings.global_assets_version.ToString(), false );
                if (!isUrl)
                    full_path = full_path.Substring( full_path.IndexOf( "Assets/" ) );
                EditorGUILayout.HelpBox( "Example: " + full_path, MessageType.None );
            }
        }

        public static void ClearCacheBundles()
        {
#if UNITY_2017_1_OR_NEWER
            Caching.ClearCache();
#else
            Caching.CleanCache();
#endif
        }
    }
}
#endif