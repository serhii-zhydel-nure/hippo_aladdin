﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

namespace PSV.SettingsEditor
{
    public class PSVDefineWindow : PSVWindowElement
    {
        private List<DirectiveStatus> status_directives = new List<DirectiveStatus>
        {
            new DirectiveStatus( "PUSH_NEWS_PRESENT", false, false ),
            new DirectiveStatus( "NEATPLUG_PRESENT", false, false ),
            new DirectiveStatus( "GOOGLE_ADS_PRESENT", false, false ),
            //new DirectiveStatus( "KIDOZ_PRESENT", false, false ),
            //new DirectiveStatus( "MOBFOX_PRESENT", false, false ),
            //new DirectiveStatus( "APPODEAL_PRESENT", false, false ), 
            //new DirectiveStatus( "USE_VUNGLE_EXTRAS", false, false ),
            new DirectiveStatus( "USE_UNITY_IAP", false, false ),
            new DirectiveStatus( "GOOGLE_PLAY_GAMES_PRESENT", false, false ),
            new DirectiveStatus( ASSET_BUNDLES_STORAGE, false, true ),
            new DirectiveStatus( "NO_RUNTIME_INITIALIZE", false, true, true ),
            new DirectiveStatus( "NO_POPULARITY_MODULE", false, true, true ),
        };

        private const int maxWidthDefine = 180;
        private const char define_separator = ';';
        private const string ASSET_BUNDLES_STORAGE = "ASSET_BUNDLES_STORAGE";

        private bool isHidedInFoldout = false;

        public PSVDefineWindow()
        {
            changeTargetPlatform += SetCurrentDefine;
            SetCurrentDefine();
        }
        public void SetCurrentDefine()
        {
            string curr_enabled_define_str = PlayerSettings.GetScriptingDefineSymbolsForGroup( settingsTarget );
            string[] enabled_directives = curr_enabled_define_str.Split( define_separator );
            for (int i = 0; i < enabled_directives.Length; i++)
            {
                SetEnabledOrAddDefine( enabled_directives[i] );
            }
        }
        private void SetEnabledOrAddDefine( string key )
        {
            if (string.IsNullOrEmpty( key ))
                return;
            for (int i = 0; i < status_directives.Count; i++)
            {
                if (status_directives[i].key == key)
                {
                    status_directives[i].isEnabled = true;
                    return;
                }
            }
            status_directives.Add( new DirectiveStatus( key, true,
                EditorUserBuildSettings.selectedBuildTargetGroup == BuildTargetGroup.Standalone ) );
        }

        public override void OnGUI( PSVSettingsWindow editor )
        {
            int countInLine = ( int )editor.position.width / maxWidthDefine;
            bool isVisible;
            if (status_directives.Count / countInLine < 2)
            {
                if (isHidedInFoldout)
                {
                    spoiler_open--;
                    isHidedInFoldout = false;
                }
                isVisible = true;
                GUILayout.Label( "Scripting Define Symbols switches" );
            }
            else
            {
                if (!isHidedInFoldout)
                {
                    spoiler_open++;
                    isHidedInFoldout = true;
                }
                isVisible = InFoldout( "Scripting Define Symbols switches" );
            }
            if (isVisible)
            {
                GUILayout.BeginHorizontal();
                for (int i = 0; i < status_directives.Count; i++)
                {
                    if (status_directives[i].Show( editor.validPlatforms ))
                    {
                        if (status_directives[i].key == ASSET_BUNDLES_STORAGE)
                        {
                            PSVResourcesSettings.MoveSoundsFolder( status_directives[i].isEnabled );
                        }
                    }
                    if (i + 1 < status_directives.Count && ( ( i + 1 ) % countInLine ) == 0)
                    {
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();
                    }
                }
                GUILayout.EndHorizontal();
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        public class DirectiveStatus
        {
            public readonly string key;
            public readonly bool isInverted;
            public readonly bool isSetForStandalone;
            private bool is_enabled;

            public bool isEnabled
            {
                get
                {
                    return is_enabled;
                }
                set
                {
                    is_enabled = isInverted ? !value : value;
                }
            }

            public DirectiveStatus( string key, bool isEnabled, bool isSetForStandalone = false, bool isInverted = false )
            {
                this.key = key;
                this.isInverted = isInverted;
                this.isEnabled = isEnabled;
                this.isSetForStandalone = isSetForStandalone;
            }

            public bool Show( BuildTargetGroup[] groups )
            {
                string label = isInverted ? key.Substring( 3 ) : key;
                if (isEnabled != GUILayout.Toggle( isEnabled, label, EditorStyles.miniButton ))
                {
                    GUI.changed = false;
                    is_enabled = !is_enabled;
                    for (int i = 0; i < groups.Length; i++)
                    {
                        SetForGroup( groups[i], isInverted ? !isEnabled : isEnabled );
                    }
                    if (isSetForStandalone)
                    {
                        SetForGroup( BuildTargetGroup.Standalone, isInverted ? !isEnabled : isEnabled );
                    }
                    return true;
                }
                return false;
            }

            public string SetForGroup( BuildTargetGroup group, bool isEnable )
            {
                string define_list = PlayerSettings.GetScriptingDefineSymbolsForGroup( group );

                if (isEnable)
                {
                    int dir_ndex = define_list.IndexOf( key );
                    if (dir_ndex >= 0)
                        return define_list;

                    if (define_list.Length > 0)
                    {
                        define_list += define_separator;
                    }
                    define_list += key;
                }
                else
                {
                    int dir_ndex = define_list.IndexOf( key );
                    if (dir_ndex < 0)
                        return define_list;

                    int len = key.Length;
                    int last_symbol = dir_ndex + len;
                    //if there are some other defines we will include to len separator symbol
                    if (define_list.Length > last_symbol && define_list[last_symbol] == define_separator)
                    {
                        len++;
                    }
                    define_list = define_list.Remove( dir_ndex, len );
                }
                PlayerSettings.SetScriptingDefineSymbolsForGroup( group, define_list );
                return define_list;
            }

        }
    }
}