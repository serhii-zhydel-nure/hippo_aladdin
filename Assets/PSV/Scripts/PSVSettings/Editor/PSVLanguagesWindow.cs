﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace PSV.SettingsEditor
{
    public class PSVLanguagesWindow : PSVWindowElement
    {
        private bool required_create_language_folders = false;

        public PSVLanguagesWindow()
        {
            for (int i = 0; i < settings.available_languages.Length; i++)
            {
                string folder_path = GetPathToMultilanguageFolder() + settings.available_languages[i].ToString();
                if (!Directory.Exists( folder_path ))
                {
                    required_create_language_folders = true;
                    break;
                }
            }
        }

        public override void OnGUI( PSVSettingsWindow window )
        {
            if (InFoldout( "Languages Settings" ))
            {
                required_create_language_folders = settings.available_languages.Length == 0;
                EdditArray( ref settings.available_languages, ShowLanguageElement, OnChangeLanguages );

                if (required_create_language_folders)
                    EditorGUILayout.HelpBox( "Use button 'Create folder' for create localized sounds folder in the resources.", MessageType.Info );

                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label( "Specify available languages by localized sounds: ", GUILayout.ExpandWidth( false ) );
                if (GUILayout.Button( "Specify", EditorStyles.miniButton, GUILayout.ExpandWidth( false ) ))
                {
                    GUI.changed = false;
                    SpecifyTheCorrectLanguagesBySoundFolders();
                }
                EditorGUILayout.EndHorizontal();
            }
            if (required_create_language_folders)
            {
                EditorGUILayout.HelpBox( "Available languages do not match the localized sounds in the resources.", MessageType.Error );
            }
        }

        public override string EditorFileName()
        {
            return base.EditorFileName();
        }

        private void ShowLanguageElement( ref Languages.Language lang, int index )
        {
            var changed_lang = ( Languages.Language )EditorGUILayout.EnumPopup( lang );
            if (lang != changed_lang)
            {
                OnChangeLanguages( false, lang );
                OnChangeLanguages( true, changed_lang );
                lang = changed_lang;
            }
            string folder_path = GetPathToMultilanguageFolder() + lang.ToString();
            if (Directory.Exists( folder_path ))
            {
                ShowBtnSelectAsset( folder_path );
            }
            else
            {
                required_create_language_folders = true;
                if (GUILayout.Button( "Create folder", EditorStyles.miniButton, GUILayout.Width( 80.0f ) ))
                {
                    GUI.changed = false;
                    Directory.CreateDirectory( folder_path );
                    AssetDatabase.ImportAsset( folder_path );
                }
            }
        }

        private void SpecifyTheCorrectLanguagesBySoundFolders()
        {
            string multilanguage_folder = GetPathToMultilanguageFolder();
            string[] folders_in = Directory.GetDirectories( multilanguage_folder );
            List<Languages.Language> correct_languages = new List<Languages.Language>();
            for (int i = 0; i < folders_in.Length; i++)
            {
                Debug.Log( folders_in[i] );
                if (!folders_in[i].EndsWith( "_Removed" ))
                {
                    string[] split_path = folders_in[i].Split( '/' );
                    try
                    {
                        correct_languages.Add( split_path[split_path.Length - 1].ToEnum<Languages.Language>() );
                    }
                    catch { }
                }
            }
            settings.available_languages = correct_languages.ToArray();
        }

        private string GetPathToMultilanguageFolder()
        {
#if ASSET_BUNDLES_STORAGE
            string path = editor_settings.path_reses_assetbundles;
#else
            string path = EditorPSVSettings.path_sounds_in_resources;
#endif
            return path + "/" + editor_settings.name_multilanguage_sounds_folder + "/";
        }

        private bool OnChangeLanguages( bool isAdd, int index )
        {
            OnChangeLanguages( isAdd, settings.available_languages[index] );
            return true;
        }
        private void OnChangeLanguages( bool isAdd, Languages.Language lang )
        {
            string lang_path = GetPathToMultilanguageFolder() + lang.ToString();
            if (isAdd)
            {
                if (AssetsHelper.TryMoveFolder( lang_path + "_Removed", lang_path ))
                {
#if ASSET_BUNDLES_STORAGE
                    PSVBundleLabels.SetBundleNameForFolder( lang_path, lang.ToString().ToLower() + settings.language_bundle_path.postfix );
#endif
                }
                else
                {
                    Directory.CreateDirectory( lang_path );
                    AssetDatabase.ImportAsset( lang_path );
                }
            }
            else if (AssetsHelper.TryMoveFolder( lang_path, lang_path + "_Removed" ))
            {
#if ASSET_BUNDLES_STORAGE
                PSVBundleLabels.SetBundleNameForFolder( lang_path + "_Removed", string.Empty );
#endif
            }
        }
    }
}