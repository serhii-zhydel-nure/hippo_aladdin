﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV.SettingsEditor
{
    public class ProtoypeInfoObject : ScriptableObject
    {
        public ProtoypeInfo info;
        
        public IEnumerator DownloadLastVersion( Action onDownload )
        {
            using (WWW loader = new WWW( PSVVersionInfo.prototypeInfoURL ))
            {
                while (!loader.isDone)
                    yield return null;
                if (string.IsNullOrEmpty( loader.error ))
                {
                    try
                    {
                        var infoTemp = JsonUtility.FromJson<ProtoypeInfo>( loader.text );
                        if (infoTemp != null)
                        {
                            info = infoTemp;
                            if (onDownload != null)
                                onDownload();
                        }
                    }
                    catch { }
                }
            }
        }
    }

    [System.Serializable]
    public class ProtoypeInfo
    {
        public string version;
    }
}