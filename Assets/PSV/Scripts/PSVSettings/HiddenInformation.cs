﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using PSV.ADS;

namespace PSV
{
    [RequireComponent( typeof( Button ) )]
    public partial class HiddenInformation : MonoBehaviour
    {
        public HiddenWindow openWindow;

        private int countClicks = 0;

        private void Awake()
        {
            var button = GetComponent<Button>();
            button.onClick.AddListener( OnButtonClick );
        }

        protected virtual void OnButtonClick()
        {
            if (countClicks == 0)
                Invoke( "ClearCountClicks", 8.0f );
            countClicks++;
            if (countClicks > 7)
            {
                CancelInvoke();
                countClicks = 0;

                var settingsPanel = FindObjectOfType<SettingsPanel>();
                if (settingsPanel)
                    this.Invoke( settingsPanel.OpenSettingsPannel, 1.0f, true, true );
                openWindow = new HiddenWindow( OnClose );
                KeyListener.AddKey( KeyCode.Escape, KeyListener_OnKeyPressed );
            }
        }

        private void KeyListener_OnKeyPressed( KeyCode key )
        {
            if (key == KeyCode.Escape)
                OnClose();
        }

        private void OnClose()
        {
            KeyListener.OnKeyPressed -= KeyListener_OnKeyPressed;
            Destroy( openWindow.baseObject );
            openWindow = null;
        }

        /// <summary>
        /// Invoke method : <see cref="OnButtonClick"/>
        /// </summary>
        private void ClearCountClicks()
        {
            countClicks = 0;
        }


        public class HiddenWindow : CanvasWindow
        {
            public Toggle homeOrCommercial;
            public Toggle bannerOrNative;
            public RectTransform mainContentViewRect;

            public HiddenWindow( UnityEngine.Events.UnityAction onClose )
            {
                Image background = baseObject.AddComponent<Image>();
                background.color = new Color32( 115, 200, 215, 255 );
                var mainContentTran = AddRectView( baseTran, 800.0f, out mainContentViewRect );
                mainContentViewRect.sizeDelta = Vector2.zero;
                mainContentViewRect.anchorMin = Vector2.zero;
                mainContentViewRect.anchorMax = Vector2.one;

                RectTransform textInfoViewRect;
                var textInfoContentTran = AddRectView( mainContentTran, 800.0f, out textInfoViewRect );
                textInfoViewRect.sizeDelta = new Vector2( 0.0f, 250.0f );
                textInfoViewRect.anchorMin = Vector2.up;
                textInfoViewRect.anchorMax = Vector2.one;
                Text mainInfoText = AddText( textInfoContentTran, Vector2.zero, Vector2.one );
                mainInfoText.alignment = TextAnchor.UpperCenter;

                var infoBuilder = new System.Text.StringBuilder();
                infoBuilder.Append( "Game v: " );
                infoBuilder.Append( Application.version );
                infoBuilder.Append( "\nUnity v: " );
                infoBuilder.Append( Application.unityVersion );
                infoBuilder.Append( " Prototype v: " );
                infoBuilder.Append( ConstSettings.prototypeVersion );

                var secondContentTran = AddVertical( mainContentTran );
                secondContentTran.pivot = Vector2.up;
                secondContentTran.sizeDelta = new Vector2( 0.0f, 500.0f );
                secondContentTran.anchorMin = Vector2.up;
                secondContentTran.anchorMax = Vector2.one;
                secondContentTran.anchoredPosition = new Vector2( 0.0f, -250.0f );

                var horizontalUtils = AddHorizontal( secondContentTran );
                {
                    bool isMuted = AudioController.GetGroup( StreamGroup.MUSIC ).IsMuted();
                    AddToggle( horizontalUtils, isMuted ? "Вкл Музыку" : "Выкл Музыку", !isMuted, ToggleMusic );

#if !NO_POPULARITY_MODULE
                    infoBuilder.Append( "\nRating:" );
                    using (var popularityRating = PopularNotify.PopularityRating.Builder())
                    {
                        int lastPosition = popularityRating.GetLastRatingPosition();
                        if (lastPosition < 1 || lastPosition > 7)
                            infoBuilder.Append( "Not popular\n" );
                        else
                            infoBuilder.Append( lastPosition + "\n" );
                    }
                    infoBuilder.Append( NativeNotifications.CallNative<string>( "getAllSavedNotificationsString" ) );

                    AddButton( horizontalUtils, "Test Push", CallNotification );
#endif
                    AddButton( horizontalUtils, "Close", onClose );
                }

                var horizontalAdsToggles = AddHorizontal( secondContentTran );
                {
                    homeOrCommercial = AddToggle( horizontalAdsToggles, "Home/Commercial", false, null );
                    bannerOrNative = AddToggle( horizontalAdsToggles, "Banner/Native", true, null );
                }

                var horizontalAdsShowFullscreen = AddHorizontal( secondContentTran );
                {
                    AddButton( horizontalAdsShowFullscreen, "Show Interstitial", OnShowInterstitial );
                    AddButton( horizontalAdsShowFullscreen, "Show Rewarded", OnShowRewarded );
                }

                var horizontalAdsShowBanner = AddHorizontal( secondContentTran );
                {
                    AddButton( horizontalAdsShowBanner, "Show Ad", OnShowAdBanner );
                    AddButton( horizontalAdsShowBanner, "Hide Ad", OnHideAdBanner );
                }

                var horizontalMoveBanner = AddHorizontal( secondContentTran );
                {
                    AddButton( horizontalMoveBanner, "To left", MoveBannerLeft );
                    AddButton( horizontalMoveBanner, "To center", MoveBannerCenter );
                    AddButton( horizontalMoveBanner, "To right", MoveBannerRight );
                }

                //var horizontalAdsProviders = AddHorizontal( secondContentTran );
                //{
                //    AddButton( horizontalMoveBanner, "To left", MoveBannerLeft );
                //    AddButton( horizontalMoveBanner, "To center", MoveBannerCenter );
                //    AddButton( horizontalMoveBanner, "To right", MoveBannerRight );
                //}

                mainInfoText.text = infoBuilder.ToString();
            }

            #region Utils
            private void ToggleMusic( ChangeToggle toggle, bool isEnabled )
            {
                toggle.label.text = isEnabled ? "Выкл Музыку" : "Вкл Музыку";
                AudioController.GetGroup( StreamGroup.MUSIC ).Mute( !isEnabled, false );
            }

#if !NO_POPULARITY_MODULE
            private void CallNotification()
            {
                new NativeNotifications
                    .Builder( "Test Title", "Test message" )
                    .DelaySeconds( 5 )
                    .OpenIntent( HomeAds.HomeAdsBaseOffer.ACTION_OPEN_NOTIFY )
                    .StyleType( PopularNotify.PopularityRating.default_notify_style )
                    .Background( PopularNotify.PopularityRating.default_notify_background_res )
                    .Build();
                this.Invoke( Application.Quit, 7, true, true );
            }
#endif
            #endregion

            #region Ads manager
            private void OnShowAdBanner()
            {
                if (bannerOrNative.isOn)
                {
                    ManagerGoogle.ShowSmallBanner( homeOrCommercial.isOn );
                }
                else
                {
                    ManagerGoogle.ShowNativeBanner( homeOrCommercial.isOn );
                }
                mainContentViewRect.sizeDelta = new Vector2( 0.0f, -100.0f );
            }
            private void OnHideAdBanner()
            {
                if (bannerOrNative.isOn)
                {
                    ManagerGoogle.HideSmallBanner();
                }
                else
                {
                    ManagerGoogle.HideNativeBanner();
                }
                mainContentViewRect.sizeDelta = Vector2.zero;
            }

            private void OnShowRewarded()
            {
                ManagerGoogle.ShowRewardedVideoAd( homeOrCommercial.isOn, true );
            }
            private void OnShowInterstitial()
            {
                ManagerGoogle.ShowInterstitial( homeOrCommercial.isOn, true );
            }

            private void MoveBannerRight()
            {
                MoveBannerTo( AdPosition.Bottom_Right );
            }
            private void MoveBannerCenter()
            {
                MoveBannerTo( AdPosition.Bottom_Centered );
            }
            private void MoveBannerLeft()
            {
                MoveBannerTo( AdPosition.Bottom_Left );
            }
            private void MoveBannerTo( AdPosition pos )
            {
                if (bannerOrNative.isOn)
                {
                    ManagerGoogle.RefreshSmallBanner( pos, AdSize.Standard_banner_320x50 );
                }
                else
                {
                    ManagerGoogle.RefreshNativeBanner( pos, AdSize.Native_banner_minimal_320x80 );
                }
            }

            #endregion
        }
    }

    public class CanvasWindow
    {
        public GameObject baseObject;
        public RectTransform baseTran;

        public CanvasWindow()
        {
            baseObject = AddObject( "HiddenInfo", ServiceContainer.canvas, out baseTran );
            baseTran.sizeDelta = Vector2.zero;
            baseTran.anchorMin = Vector2.zero;
            baseTran.anchorMax = Vector2.one;
        }

        public static RectTransform AddHorizontal( RectTransform parent )
        {
            RectTransform result;
            GameObject obj = AddObject( "Horizontal", parent, out result );
            var layout = obj.AddComponent<HorizontalLayoutGroup>();
            layout.spacing = 7.0f;
            layout.padding = new RectOffset( 3, 3, 3, 3 );
            return result;
        }

        public static RectTransform AddVertical( RectTransform parent )
        {
            RectTransform result;
            GameObject obj = AddObject( "Vertical", parent, out result );
            var layout = obj.AddComponent<VerticalLayoutGroup>();
            layout.spacing = 7.0f;
            layout.padding = new RectOffset( 3, 3, 3, 3 );
            return result;
        }

        public static GameObject AddObject( string name, RectTransform parent, out RectTransform rectTran )
        {
            GameObject obj = new GameObject( name );
            rectTran = obj.AddComponent<RectTransform>();
            rectTran.SetParent( parent, false );
            return obj;
        }

        public static Text AddText( RectTransform parent, Vector2 anchorMin, Vector2 anchorMax, float height = 0.0f )
        {
            RectTransform rectTran;
            GameObject obj = AddObject( "Text", parent, out rectTran );
            rectTran.sizeDelta = new Vector2( 0.0f, height );
            rectTran.anchorMin = anchorMin;
            rectTran.anchorMax = anchorMax;
            Text textField = obj.AddComponent<Text>();
            Font ArialFont = ( Font )Resources.GetBuiltinResource( typeof( Font ), "Arial.ttf" );
            textField.font = ArialFont;
            textField.resizeTextForBestFit = true;
            textField.color = Color.white;
            textField.alignment = TextAnchor.MiddleCenter;
            obj.AddComponent<Outline>();
            return textField;
        }

        public static Button AddButton( RectTransform parent, string label, UnityEngine.Events.UnityAction onClick )
        {
            RectTransform rectTran;
            GameObject obj = AddObject( "Button", parent, out rectTran );
            Image renderer = obj.AddComponent<Image>();
            renderer.color = new Color32( 165, 165, 165, 255 );
            Button button = obj.AddComponent<Button>();
            var text = AddText( rectTran, Vector2.zero, Vector2.one );
            text.text = label;
            button.onClick.AddListener( onClick );
            return button;
        }

        public static Toggle AddToggle( RectTransform parent, string label, bool isOn, Action<ChangeToggle, bool> onChange )
        {
            RectTransform rectTran;
            GameObject obj = AddObject( "Toggle", parent, out rectTran );
            Toggle toggle = obj.AddComponent<Toggle>();
            toggle.toggleTransition = Toggle.ToggleTransition.None;
            ChangeToggle eventHandler = new ChangeToggle
            {
                background = obj.AddComponent<Image>(),
                label = AddText( rectTran, Vector2.zero, Vector2.one )
            };
            toggle.targetGraphic = eventHandler.background;
            eventHandler.label.text = label;
            toggle.isOn = isOn;
            toggle.onValueChanged.AddListener( eventHandler.OnChange );
            eventHandler.OnChange( isOn );
            eventHandler.OnChangeEvent += onChange;
            return toggle;
        }

        public static RectTransform AddRectView( RectTransform parent, float contentHeight, out RectTransform viewTran )
        {
            var viewObj = AddObject( "RectView", parent, out viewTran );
            viewTran.pivot = new Vector2( 0.5f, 1.0f );
            var scrollRect = viewObj.AddComponent<ScrollRect>();
            scrollRect.horizontal = false;
            scrollRect.viewport = viewTran;
            var background = viewObj.AddComponent<Image>();
            background.color = new Color( 0.8f, 0.8f, 0.8f, 0.3f );
            viewObj.AddComponent<Mask>();

            RectTransform contentTran;
            AddObject( "Content", viewTran, out contentTran );
            contentTran.pivot = new Vector2( 0.5f, 1.0f );
            contentTran.sizeDelta = new Vector2( 0.0f, contentHeight );
            contentTran.anchorMin = Vector2.up;
            contentTran.anchorMax = Vector2.one;
            scrollRect.content = contentTran;
            return contentTran;
        }

        public class ChangeToggle
        {
            public Image background;
            public Text label;
            public event Action<ChangeToggle, bool> OnChangeEvent;

            public void OnChange( bool isOn )
            {
                background.color = isOn ? new Color32( 150, 220, 160, 255 ) : new Color32( 165, 165, 165, 255 );
                if (OnChangeEvent != null)
                    OnChangeEvent( this, isOn );
            }
        }
    }
}