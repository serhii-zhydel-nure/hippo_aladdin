﻿#if ASSET_BUNDLES_STORAGE
using UnityEngine;
using System.Collections;
using PSV.AssetBundles;
#if UNITY_5_4_OR_NEWER
using UnityEngine.Networking;
#else
using UnityEngine.Experimental.Networking;
#endif
using System;

namespace PSV
{
    public partial class LoadAssetBundle : LoadableObject
    {
        #region Fields
        public Hash128 bundle_hash;
        public event Action<AssetBundle> OnLoadDone;

        public bool is_wait_connection = true;
        private string last_error = string.Empty;
        public string bundle_url { get { return identifier; } }

        protected AsyncOperation load_operation;

        #endregion

        /// <summary>
        /// Create Loader for download AssetBundle. For Start Load call <see cref="LoadAsync"/> or <see cref="Preload"/>
        /// </summary>
        /// <param name="bundle_url">Local | Global path to AssetBundle.</param>
        public LoadAssetBundle( string bundle_url ) : base( bundle_url ) { }

        /// <summary>
        /// Instantly load from local directory.
        /// Equal <see cref="AssetBundle.LoadFromFile(string)"/> logic.
        /// </summary>
        public AssetBundle Load()
        {
            is_prepared = true;
            var bundle = AssetBundle.LoadFromFile( identifier );
            if (OnLoadDone != null)
            {
                OnLoadDone( bundle );
            }
            return bundle;
        }

        /// <summary>
        /// Select between <see cref="Preload"/> and <see cref="LoadAsync"/> with <see cref="BackgroundProcess.SetNecessaryAll()"/>.
        /// If current scene is selected then add to Preload list for next scene
        /// Else load in background.
        /// </summary>
        public LoadableObject PreloadOnScene( Scenes scene )
        {
            if (SceneLoader.IsSceneActive( scene.ToString() ))
            {
                Preload();
            }
            else
            {
                SetNecessaryAll().LoadAsync();
            }
            return this;
        }

        #region Params

        /// <summary>
        /// Callback on download done.
        /// </summary>
        public LoadAssetBundle OnComplete( Action<AssetBundle> callback )
        {
            OnLoadDone += callback;
            return this;
        }

        /// <summary>
        /// Set global app version for determining the cached bundle version. 
        /// </summary>
        public LoadAssetBundle SetGlobalVersion()
        {
            bundle_hash = new Hash128( 0U, 0U, 0U, PSVSettings.settings.global_assets_version );
            return this;
        }

        /// <summary>
        /// Set bundle version for determining the cached version. 
        /// </summary>
        public LoadAssetBundle SetVersion( uint version )
        {
            bundle_hash = new Hash128( 0U, 0U, 0U, version );
            return this;
        }

        /// <summary>
        /// Set bundle Hash code for determining the cached version. 
        /// </summary>
        public LoadAssetBundle SetHash( Hash128 hash )
        {
            bundle_hash = hash;
            return this;
        }

        /// <summary>
        /// For False: skip download on <see cref="NetworkReachability.NotReachable"/>.
        /// </summary>
        public LoadAssetBundle SetWaitNetworkReachable( bool is_wait_connection = true )
        {
            this.is_wait_connection = is_wait_connection;
            return this;
        }

        #endregion

        public override float progress
        {
            get
            {
                return load_operation == null ? 0.0f : load_operation.progress;
            }

        }
        public bool IsCached()
        {
            return Caching.IsVersionCached( bundle_url, bundle_hash );
        }

        protected override IEnumerator GetInstruction()
        {
            if (bundle_hash.Equals( default( Hash128 ) ))
                SetGlobalVersion();
            // UnityWebRequest not supported Streaming Assets directory storage and need use LoadAsyncLocalInstruction
            if (bundle_url.StartsWith( "http" ))
            {
                if (!Uri.IsWellFormedUriString( bundle_url, UriKind.Absolute ))
                {
                    Debug.LogError( "LoadAssetBundle: Invalid URI! - " + bundle_url );
                    yield break;
                }

                bool is_cached_version = IsCached();
                if (!is_cached_version)
                {
                    AnalyticsManager.LogEvent( AnalyticsEvents.DownloadFromServer, "Download Started: AssetBundle" );
                    yield return NetworkReachabilityHandler.WaitAllowLoading( StillWaitReachable );
                    ApplicationEventAgent.OnPause += ApplicationPauseAgent_Event;
                }
                do // Repeats until it is downloaded without server reachability errors
                {
                    if (isDone)
                        break; // Abort load

#if UNITY_2018_1_OR_NEWER
                    using (UnityWebRequest webRequest = UnityWebRequestAssetBundle.GetAssetBundle( bundle_url, bundle_hash, 0 ))
#else
                    using (UnityWebRequest webRequest = UnityWebRequest.GetAssetBundle( bundle_url, bundle_hash, 0 ))
#endif
                    {
#if UNITY_2017_1_OR_NEWER
                        load_operation = webRequest.SendWebRequest();
#else
                        load_operation = webRequest.Send();
#endif
                        while (!( load_operation.isDone || isDone ))
                        {
                            yield return null; // Download progress async
                            if (content_length == 0L)
                            {
                                long.TryParse( webRequest.GetResponseHeader( "Content-Length" ), out content_length );
                            }
                            HandleNotReachableMessage();
                        }
                        if (isDone)
                        {
                            webRequest.Abort();
                            break; // Abort load
                        }
#if UNITY_2017_1_OR_NEWER
                        if (webRequest.isNetworkError || webRequest.isHttpError)
#else
                        if (webRequest.isError)
#endif
                        {
                            HandleErrorMessage( webRequest.error + "|Code:" + webRequest.responseCode );

                            if (!is_wait_connection
                                || Application.isEditor
                                || Application.platform == RuntimePlatform.WindowsPlayer
                                || Application.platform == RuntimePlatform.OSXPlayer) // Is skip download on error. Always for Editor and Standalone
                            {
                                OnLoadDoneReturn( null );
                                break; // << - skip download asset bundle
                            }
                        }
                        else // On Download Complete
                        {
                            AssetBundle loaddedBundle = DownloadHandlerAssetBundle.GetContent( webRequest );
                            if (loaddedBundle) // Download Done
                            {
                                WarningPanel.Hide();
                                ApplicationEventAgent.OnPause -= ApplicationPauseAgent_Event;
                                OnLoadDoneReturn( loaddedBundle );
                                if (!is_cached_version)
                                {
#if UNITY_2017_1_OR_NEWER
                                    Caching.ClearOtherCachedVersions( PathToBundle.GetBundleName( bundle_url ), bundle_hash );
#endif
                                    AnalyticsManager.LogEvent( AnalyticsEvents.DownloadFromServer, "Download Done: AssetBundle" );
                                }
                                break; // << - Download DONE and skip loop
                            }
                            else if (webRequest.responseCode == ( long )System.Net.HttpStatusCode.NotFound)
                            {
                                ApplicationEventAgent.OnPause -= ApplicationPauseAgent_Event;
                                ServiceMessage.Hide();
                                if (!Application.isEditor && Application.platform != RuntimePlatform.WindowsPlayer
                                    && Application.platform != RuntimePlatform.OSXPlayer) // Is skip update version for Editor and Standalone
                                {
                                    WarningPanel.ShowUpdateApplication(); // Requred update Application in market
                                    AnalyticsManager.LogEvent( AnalyticsEvents.DownloadFromServer,
                                        "Download Canceled: AssetBundle not found version " + PSVSettings.settings.global_assets_version );

                                    while (true) // Pause load other. Wait update application
                                        yield return null;
                                }
                            }
                            else
                            {
                                HandleErrorMessage( "Impossible unzip bundle |Code:" + webRequest.responseCode );
                            }
                        }
                    }
                    yield return new WaitForSeconds( 1.0f );
                } while (is_wait_connection);
            }
            else // Load from local file
            {
                load_operation = AssetBundle.LoadFromFileAsync( bundle_url );
                while (!load_operation.isDone)
                    yield return null;

                OnLoadDoneReturn( ( ( AssetBundleCreateRequest )load_operation ).assetBundle );
            }
        }

        private void OnLoadDoneReturn( AssetBundle assetBundle )
        {
            if (OnLoadDone == null)
            {
                Debug.LogError( "LoadAssetBundle: Download '" + assetBundle.name
                    + "' has no receiver for OnComplete callback. Is left in memory permanently. It is strongly recommended to manage the downloaded AssetBundles for free resources." );
            }
            else
            {
                OnLoadDone( assetBundle );
            }
        }

        private void ApplicationPauseAgent_Event( bool is_pause )
        {
            string message = "Download " + ( is_pause ? "paused: " : "unpaused: " );

            if (string.IsNullOrEmpty( last_error ))
                message += "no errors.";
            else
                message += last_error;
            AnalyticsManager.LogEvent( AnalyticsEvents.DownloadFromServer, message );
        }

        private bool StillWaitReachable()
        {
            return is_wait_connection && !isDone;
        }

        private void HandleNotReachableMessage()
        {
            if (string.IsNullOrEmpty( last_error ))
            {
                if (WarningPanel.IsShown)
                {
                    if (Application.internetReachability != NetworkReachability.NotReachable)
                        WarningPanel.Hide();
                }
                else if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    WarningPanel.Show( NetworkReachabilityHandler.LangTextWaitNetworcConnection(), true );
                }
            }
        }

        private void HandleErrorMessage( string new_error )
        {
            Debug.LogError( "LoadAssetBundle: " + new_error + ". URL: " + bundle_url );
            if (string.IsNullOrEmpty( last_error ))
                AnalyticsManager.LogEvent( AnalyticsEvents.DownloadFromServer, "Download EROR: AssetBundle " + new_error );
            else
                WarningPanel.ShowWarningTryLater().OnAccept( WarningPanel.ApplicationQuit );
            last_error = new_error;
        }


    }
}
#endif