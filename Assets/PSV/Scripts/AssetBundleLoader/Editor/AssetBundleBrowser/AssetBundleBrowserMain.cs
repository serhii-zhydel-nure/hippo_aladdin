#if ASSET_BUNDLES_STORAGE && UNITY_5_6_OR_NEWER
using System.Collections.Generic;
using UnityEditor;

namespace UnityEngine.AssetBundles
{

    public class AssetBundleBrowserMain : EditorWindow/*, IHasCustomMenu*/
    {
        [SerializeField]
        public AssetBundleManageTab m_ManageTab;
        

        private Texture2D m_RefreshTexture;

        const float k_ToolbarPadding = 15;
        const float k_MenubarPadding = 32;

        [MenuItem("Window/AssetBundle Browser", priority = 2050)]
        public static void ShowWindow()
        {
            var window = GetWindow<AssetBundleBrowserMain>();
            window.titleContent = new GUIContent("AssetBundles");
            window.Show();
        }

        //[SerializeField]
        //public bool multiDataSource = false;
        //public virtual void AddItemsToMenu(GenericMenu menu)
        //{
        //    //menu.AddSeparator(string.Empty);
        //    menu.AddItem(new GUIContent("Custom Sources"), multiDataSource, FlipDataSource);
        //}
        //public void FlipDataSource()
        //{
        //    multiDataSource = !multiDataSource;
        //}

        private void OnEnable()
        {

            Rect subPos = GetSubWindowArea();
            if(m_ManageTab == null)
                m_ManageTab = new AssetBundleManageTab();
            m_ManageTab.OnEnable(subPos, this);

            m_RefreshTexture = EditorGUIUtility.FindTexture("Refresh");


            //determine if we are "multi source" or not...
            //multiDataSource = false;
            //List<System.Type> types = AssetBundleDataSource.ABDataSourceProviderUtility.CustomABDataSourceTypes;
            //if (types.Count > 1)
            //    multiDataSource = true;
        }

        private Rect GetSubWindowArea()
        {
            float padding = k_MenubarPadding;
            //if (multiDataSource)
            //    padding += k_MenubarPadding * 0.5f;
            Rect subPos = new Rect(0, padding, position.width, position.height - padding);
            return subPos;
        }

        private void Update()
        {
            m_ManageTab.Update();
        }

        private void OnGUI()
        {
            ModeToggle();
            m_ManageTab.OnGUI(GetSubWindowArea());
        }

        void ModeToggle()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(k_ToolbarPadding);

                bool clicked = GUILayout.Button(m_RefreshTexture);
                if (clicked)
                    m_ManageTab.ForceReloadData();
           
            //float toolbarWidth = position.width - k_ToolbarPadding * 4 - m_RefreshTexture.width;
            
            GUILayout.EndHorizontal();
            //if(multiDataSource)
            //{
            //    //GUILayout.BeginArea(r);
            //    GUILayout.BeginHorizontal();

            //    using (new EditorGUILayout.HorizontalScope(EditorStyles.toolbar))
            //    {
            //        GUILayout.Label("Bundle Data Source:");
            //        GUILayout.FlexibleSpace();
            //        var c = new GUIContent(string.Format("{0} ({1})", "Default", "Built-in" ), "Select Asset Bundle Set");
            //        if (GUILayout.Button(c , EditorStyles.toolbarPopup) )
            //        {
            //            GenericMenu menu = new GenericMenu();
            //            bool firstItem = true;

            //            foreach (var info in AssetBundleDataSource.ABDataSourceProviderUtility.CustomABDataSourceTypes)
            //            {
            //                List<AssetBundleDataSource.ABDataSource> dataSourceList = null;
            //                dataSourceList = info.GetMethod("CreateDataSources").Invoke(null, null) as List<AssetBundleDataSource.ABDataSource>;
                        

            //                if (dataSourceList == null)
            //                    continue;

            //                if (!firstItem)
            //                {
            //                    menu.AddSeparator("");
            //                }

            //                foreach (var ds in dataSourceList)
            //                {
            //                    menu.AddItem(new GUIContent(string.Format("{0} ({1})", "Default", "Built-in" ) ), false,
            //                        () =>
            //                        {
            //                            var thisDataSource = ds;
            //                            AssetBundleModel.Model.DataSource = thisDataSource;
            //                            m_ManageTab.ForceReloadData();
            //                        }
            //                    );
            //                }

            //                firstItem = false;
            //            }

            //            menu.ShowAsContext();
            //        }

            //        GUILayout.FlexibleSpace();
            //    }

            //    GUILayout.EndHorizontal();
            //    //GUILayout.EndArea();
            //}
        }


    }
}
#endif