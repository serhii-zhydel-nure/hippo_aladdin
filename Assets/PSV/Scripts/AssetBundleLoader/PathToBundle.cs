﻿#if ASSET_BUNDLES_STORAGE
using UnityEngine;

namespace PSV.AssetBundles
{
    /// <summary>
    /// Construct paths like:
    /// [ prefix | streamingAssetsPath ] + PlatformFolder*/ + [VersionFolder**/] + [ language_name ] + <see cref="postfix"/> |
    /// * PlatformFolder substituted for the current running platform.
    /// ** VersionFolder folder for version bundle defoult get PSVSettings.settings.global_assets_version
    /// </summary>
    [System.Serializable]
    public class PathToBundle
    {
        /// <summary>
        /// Postfix after platform name for folder asset bundles. 
        /// </summary>
        /// <example> .../AndroidBundles/... </example>
        public const string postfix_bundles = "Bundles";
        /// <summary>
        /// If <see cref="Application.version"/> ends with a letter then test folder added to URL after prefix.
        /// </summary>
        private const char slash = '/';
        private const string osx_standalone_unified = "StandaloneOSX";
        private const string windows_standalone_unified = "StandaloneWindows";
        private const string android_unified = "Android";
        private const string ios_unified = "iOS";

        public string prefix;
        public string postfix;

        /// <summary>
        /// Create Local path to asset bundle.
        /// Prefix equal streamingAssetsPath.
        /// </summary>
        /// <param name="postfix">Right part after platform folder and language name.</param>
        public PathToBundle( string postfix )
        {
            this.prefix = string.Empty;
            this.postfix = postfix;
        }

        /// <summary>
        /// Create URL to asset bundle.
        /// </summary>
        /// <param name="prefix">Left part path. If equal Empty then substituted to streamingAssetsPath.</param>
        /// <param name="postfix">Right part after platform folder and language name.</param>
        public PathToBundle( string prefix, string postfix )
        {
            this.prefix = prefix;
            this.postfix = postfix;
        }

        /// <summary>
        /// Construct path with current platform folder and language name.
        /// </summary>
        /// <param name="lang">Selected language for path. Set 0 for skip.</param>
        public string Path( Languages.Language lang = 0 )
        {
            return Path( lang, GetPlatformFolderName(), PSVSettings.settings.global_assets_version.ToString() );
        }

        /// <summary>
        /// Construct path with current platform folder and language name.
        /// </summary>
        /// <param name="version_folder">AssetBundles Version folder name. string.Empty for skip.</param>
        /// <param name="lang">Selected language for path. Set 0 for skip.</param>
        public string Path( string version_folder, Languages.Language lang = 0 )
        {
            return Path( lang, GetPlatformFolderName(), version_folder );
        }

        /// <summary>
        /// Construct path with specific platform folder and language name.
        /// </summary>
        /// <param name="lang">Selected language for path. Set 0 for skip.</param>
        /// <param name="platform_folder">Platform folder get from: <see cref="GetPlatformFolderName()"/></param>
        /// <param name="version_folder">AssetBundles Version folder name. string.Empty for skip.</param>
        /// <param name="editor_local">On True: for Editor in any case construct local path.</param>
        public string Path( Languages.Language lang, string platform_folder, string version_folder, bool editor_local = true )
        {
            string result;

#if UNITY_EDITOR
            if (editor_local && EditorPSVSettings.settings.always_local_url_in_editor)
            {
                result = compilingFolder + platform_folder + slash;
            }
            else
#endif
            if (string.IsNullOrEmpty( prefix ))
            {
                result = streamingFolder + platform_folder + slash;
            }
            else
            {
                result = prefix + platform_folder + slash;
                if (!string.IsNullOrEmpty( version_folder ))
                    result += version_folder + slash;
            }
            if (lang != 0)
                result += lang.ToString().ToLower();
            result += postfix;
            return result;
        }

        public static string GetBundleName( string full_path )
        {
            string[] splitted = full_path.Split( new char[] { slash } );
            return splitted[splitted.Length - 1];
        }

        /// <summary>
        /// Get unified runtime platform folder name with <see cref="postfix_bundles"/>.
        /// </summary>
        public static string GetPlatformFolderName()
        {
            return GetPlatformFolderName( Application.platform );
            //#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
            //            return osx_standalone;
            //#elif UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            //            return windows_standalone;
            //#elif UNITY_ANDROID
            //            return android;
            //#elif UNITY_IOS
            //            return ios;
            //#else
            //            throw new System.NotImplementedException();
            //#endif
        }

        /// <summary>
        /// Get unified platform folder name with <see cref="postfix_bundles"/>.
        /// </summary>
        public static string GetPlatformFolderName( RuntimePlatform platform )
        {
            string unified_name;
            switch (platform)
            {
                case RuntimePlatform.Android:
                unified_name = android_unified;
                break;
                case RuntimePlatform.IPhonePlayer:
                unified_name = ios_unified;
                break;
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer:
                unified_name = osx_standalone_unified;
                break;
                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                unified_name = windows_standalone_unified;
                break;
                default:
                unified_name = platform.ToString();
                break;
            }
            return unified_name + postfix_bundles;
        }

        /// <summary>
        /// <see cref="Application.streamingAssetsPath"/> + "/"
        /// </summary>
        public static string streamingFolder
        {
            get
            {
                return Application.streamingAssetsPath + slash;
            }
        }

#if UNITY_EDITOR

        /// <summary>
        /// Allow Editor only.
        /// <see cref="EditorPSVSettings.settings.path_buffer_bundles"/> + "/"
        /// </summary>
        public static string compilingFolder
        {
            get
            {
                return EditorPSVSettings.settings.path_buffer_bundles + slash;
            }
        }

        /// <summary>
        /// Allow Editor only.
        /// Get unified platform folder name with <see cref="postfix_bundles"/>.
        /// </summary>
        public static string GetPlatformFolderName( UnityEditor.BuildTarget platform )
        {
            string unified_name;
            switch (platform)
            {
                case UnityEditor.BuildTarget.Android:
                unified_name = android_unified;
                break;
                case UnityEditor.BuildTarget.iOS:
                unified_name = ios_unified;
                break;
#if UNITY_2017_3_OR_NEWER
                case UnityEditor.BuildTarget.StandaloneOSX:
#else
                case UnityEditor.BuildTarget.StandaloneOSXUniversal:
                case UnityEditor.BuildTarget.StandaloneOSXIntel64:
                case UnityEditor.BuildTarget.StandaloneOSXIntel:
#endif
                unified_name = osx_standalone_unified;
                break;
                case UnityEditor.BuildTarget.StandaloneWindows:
                case UnityEditor.BuildTarget.StandaloneWindows64:
                unified_name = windows_standalone_unified;
                break;
                default:
                unified_name = platform.ToString();
                break;
            }
            return unified_name + postfix_bundles;
        }
#endif
    }
}
#endif