﻿//use only one of billing providers. If Unity IAP enabled - remove OpenIAB SDK including JAR files

#if UNITY_PURCHASING
//comment this define before updating Unity IAP plugin from AssetStore
#define USE_UNITY_IAP

#endif

using UnityEngine;

#if USE_UNITY_IAP
using UnityEngine.Purchasing;
#endif


namespace PSV.IAP
{

    public class UnityIAPProvider : IBillingProvider
#if USE_UNITY_IAP
, IStoreListener
#endif
    {
#pragma warning disable 0067
        public event System.Action<string>
            OnPurchaseSucceded,
            OnPurchaseCancelled;
        public event System.Action
            OnInitializationComplete,
            OnPurchaseFailure;
        public event System.Action<string, bool>
            OnMessageLogged;
#pragma warning restore 0067

        public bool IsInitialized()
        {
#if USE_UNITY_IAP
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
#else
			return false;
#endif
        }


        private void DebugLog( string s, bool error = false )
        {
            if (OnMessageLogged != null)
            {
                OnMessageLogged( s, error );
            }
        }


        public void Init( string public_key )
        {
#if USE_UNITY_IAP
            InitializePurchasing();
#else
			DebugLog( "Set up UnityIAP", true );
#endif
        }


        public void Purchase( ProductProperties prod )
        {
            string sku = prod.sku;
            DebugLog( "Trying to purchase " + sku );
#if USE_UNITY_IAP
            BuyProductID( sku );
#else
			PurchaseFailed();
#endif
        }


        public void Restore()
        {
#if USE_UNITY_IAP
            RestoreProducts();
#endif
        }


        private void PurchaseSucceded( string sku )
        {
            if (OnPurchaseSucceded != null)
            {
                OnPurchaseSucceded( sku );
            }
        }

        private void PurchaseCancelled( string sku )
        {
            if (OnPurchaseCancelled != null)
            {
                OnPurchaseCancelled( sku );
            }
        }

        private void PurchaseFailed()
        {
            if (OnPurchaseFailure != null)
            {
                OnPurchaseFailure();
            }
        }

#if USE_UNITY_IAP


        private UnityEngine.Purchasing.ProductType ConvertType( IAP.ProductType type )
        {
            return (UnityEngine.Purchasing.ProductType) type;
        }

        private void InitializePurchasing()
        {

            if (!IsInitialized())
            {
                var builder = ConfigurationBuilder.Instance( StandardPurchasingModule.Instance() );

                ProductProperties[] properties = BillingManager.Products_SKU;
                DebugLog( "Products enqueued: " + properties.ConvertToString() );

                for (int i = 0; i < properties.Length; i++)
                {
                    string sku = properties[i].sku;
                    ProductType prod_type = properties[i].type;
                    DebugLog( "Adding product " + sku + " as " + prod_type );
                    builder.AddProduct( sku, ConvertType( prod_type ) );
                }

                // Expect a response either in OnInitialized or OnInitializeFailed.
                UnityPurchasing.Initialize( this, builder );
            }

        }

        void BuyProductID( string productId )
        {
            // If Purchasing has been initialized ...
            if (IsInitialized())
            {
                // ... look up the Product reference with the general product identifier and the Purchasing 
                // system's products collection.
                Product product = m_StoreController.products.WithID( productId );

                // If the look up found a product for this device's store and that product is ready to be sold ... 
                if (product != null && product.availableToPurchase)
                {
                    DebugLog( string.Format( "Purchasing product asynchronously: '{0}'", product.definition.id ) );
                    // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    // asynchronously.
                    m_StoreController.InitiatePurchase( product );
                }
                // Otherwise ...
                else
                {
                    // ... report the product look-up failure situation  
#if !UNITY_EDITOR
                    DebugLog( "BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase", true );
#endif
                }
            }
            // Otherwise ...
            else
            {
                // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
                // retrying initialization.
                DebugLog( "BuyProductID FAIL. Not initialized.", true );
            }
        }


        public void RestoreProducts()
        {
            // If Purchasing has not yet been set up ...
            if (!IsInitialized())
            {
                // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
                DebugLog( "RestorePurchases FAIL. Not initialized.", true );
                return;
            }

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer ||
                Application.platform == RuntimePlatform.OSXPlayer)
            {
                // ... begin restoring purchases
                DebugLog( "RestorePurchases started ..." );

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions( ( result ) =>
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    DebugLog( "RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore." );
                } );
            }
            // Otherwise ...
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
#if !UNITY_EDITOR
                DebugLog( "RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform, true );
#endif
            }
        }


        //private void CheckRefunds( Product product )
        //{
        //    bool is_owned = false;
        //    if (product.hasReceipt)
        //    {
        //        //this works for Android Only!!!!!!!!!!
        //        //TODO: parse Apple receipt
        //        DebugLog( "parsing item " + product.definition.id );
        //        GooglePurchaseData p_data = new GooglePurchaseData( product.receipt );
        //        DebugLog( "parsed item: " + product.definition.id + "\n" + p_data.ToString() );
        //        is_owned = p_data.IsPurchased();
        //    }
        //    if (!is_owned)
        //    {
        //        PurchaseCancelled( product.definition.id ); //TODO: check if we should use storeSpecificIdid !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //    }
        //}


        //  
        // --- IStoreListener
        //

        private static IStoreController m_StoreController;          // The Unity Purchasing system.
        private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        public void OnInitialized( IStoreController controller, IExtensionProvider extensions )
        {
            // Purchasing has succeeded initializing. Collect our Purchasing references.
            DebugLog( "OnInitialized: PASS" );

            // Overall Purchasing system, configured with products for this application.
            m_StoreController = controller;
            // Store specific subsystem, for accessing device-specific store features.
            m_StoreExtensionProvider = extensions;

            //foreach (var item in m_StoreController.products.all)
            //{
            //    DebugLog( "item receipt: " + item.receipt );
            //    CheckRefunds( item );
            //}

            if (OnInitializationComplete != null)
            {
                OnInitializationComplete();
            }
        }


        public void OnInitializeFailed( InitializationFailureReason error )
        {
            // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
            DebugLog( "OnInitializeFailed InitializationFailureReason:" + error, true );
        }


        public PurchaseProcessingResult ProcessPurchase( PurchaseEventArgs args )
        {
            PurchaseSucceded( args.purchasedProduct.definition.id );

            //TODO: send complete only in case when there was a listener to the event

            // Return a flag indicating whether this product has completely been received, or if the application needs 
            // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
            // saving purchased products to the cloud, and when that save is delayed. 
            return PurchaseProcessingResult.Complete;
        }


        public void OnPurchaseFailed( Product product, PurchaseFailureReason failureReason )
        {
            // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
            // this reason with the user to guide their troubleshooting actions.
            DebugLog( string.Format( "OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason ), true );
            PurchaseFailed();
        }

#endif


        private class GooglePurchaseData
        {
            // INAPP_PURCHASE_DATA
            public string inAppPurchaseData;
            // INAPP_DATA_SIGNATURE
            public string inAppDataSignature;
            public GooglePurchaseJson json;
            [System.Serializable]
            private struct GooglePurchaseReceipt
            {
                public string Payload;
            }
            [System.Serializable]
            private struct GooglePurchasePayload
            {
                public string json;
                public string signature;
            }
            [System.Serializable]
            public struct GooglePurchaseJson
            {
                public string autoRenewing;
                public string orderId;
                public string packageName;
                public string productId;
                public string purchaseTime;
                public string purchaseState;
                public string developerPayload;
                public string purchaseToken;
            }
            public GooglePurchaseData( string receipt )
            {
                try
                {
                    var purchaseReceipt = JsonUtility.FromJson<GooglePurchaseReceipt>( receipt );
                    var purchasePayload = JsonUtility.FromJson<GooglePurchasePayload>( purchaseReceipt.Payload );
                    var inAppJsonData = JsonUtility.FromJson<GooglePurchaseJson>( purchasePayload.json );
                    inAppPurchaseData = purchasePayload.json;
                    inAppDataSignature = purchasePayload.signature;
                    json = inAppJsonData;
                }
                catch
                {
                    Debug.LogWarning( "Could not parse receipt: " + receipt );
                    inAppPurchaseData = "";
                    inAppDataSignature = "";
                }
            }


            public new string ToString()
            {
                return "productId: " + json.productId
                     + "\npurchaseState: " + json.purchaseState
                     + "\nautoRenewing:  " + json.autoRenewing
                     + "\npurchaseTime:  " + json.purchaseTime
                     + "\npackageName:  " + json.packageName;
            }


            public bool IsPurchased()
            {
                int purchase_state = -1; //0 is purchased, 1 is canceled, 2 is refunded
                return !string.IsNullOrEmpty( inAppPurchaseData ) && !string.IsNullOrEmpty( inAppDataSignature ) && int.TryParse( json.purchaseState, out purchase_state ) && purchase_state == 0;
            }

        }
    }
}
