﻿#if UNITY_PURCHASING
//comment this define before updating Unity IAP plugin from AssetStore
#define USE_UNITY_IAP
#else
#define USE_OPEN_IAB
#endif

using UnityEngine;
using System.Collections.Generic;
using System.Collections;

/// ChangeLog
/// v2.0
/// - added ProjectSettings interaction (asset which holds settings for current project)
/// - added purchase restore
/// v2.1
/// - removed unsupported platforms from SDK
/// v3.0
/// - changed MapSKU method, now it process' array of productIdentifiers
/// - to add new SKU declare it as const and add it to array "productIdentifiers"
/// - implemented EasyStoreKit interaction to support IAP on IOS
/// - implemented array of consumable purchases for Android
/// - using debug trigger from ProjectSettingsContainer
/// v 4.0
/// - fixed restoring for android (process only received items)
/// - implemented sku as dictionary value for Products (can be assigned to different value under other platforms
/// - added purchase events with Products enum argument
/// v 5.0
/// - added UnityPurchase support (IOS)
/// - EasyStore deprecated
/// v5.1
/// - fixed GetProduct (if (item.Value.sku.Equals ( sku_alias ))
/// v5.2
/// - implemented storage of product purchase state (non consumable only)
/// v5.3
/// - replaced purchase event with own delegate type
/// - will immediately save integer set after purchase succeed
/// v6
/// - implemented IAP provider interface
/// - moved specific IAP methods to separate providers
/// - moved definition of Products to separate file (BillingManagerProducts) - here you should list all your products that can be purchased in game (define their type - consumable or not)
/// - moved methods for handling purchase success and failure to separate file (BillingManagerEventHadlers) - here you can define some static methods to process purchase state for your product (to guarantee saving of its state)
/// - refactored code
/// - if you've updated your project, with UnityIAP imported, and getting errors - try to ReimportAll or delete Library folder in Project's root
/// v7
/// - fixed IsProductPurchased
/// - moved ads disabling to main class file, should disable ads according to array of SKU that are meant to do it
/// - made some methods static (TODO: leave instance only for internal methods, move public methods to static)
/// - left in EventHandlers only custom space (no built-in logic is present there)



///Depends On:
///- ProjectSettingsContainer
///- ManagerGoogle
///- StoreKit (XCode framework)


namespace PSV.IAP
{

    public static partial class BillingManager
    {

        /// <summary>
        /// WIll trigger after valid purchase receipt is received
        /// </summary>
        static public PurchaseCallback
            OnPurchaseSucceded;

        /// <summary>
        /// WIll trigger after invalid purchase receipt is received or product has no receipt (works for products owned before).
        /// Is obsolete (spelling error), use <see cref="OnPurchaseCanceled"/>
        /// </summary>
        [System.Obsolete( "Use OnPurchaseCanceled instead" )]
        static public event PurchaseCallback
            OnPurchaseCancelled
        {
            add { OnPurchaseCanceled += value; }
            remove { OnPurchaseCanceled -= value; }
        }


        /// <summary>
        /// WIll trigger after invalid purchase receipt is received or product has no receipt( works for products owned before)
        /// </summary>
        static public PurchaseCallback
            OnPurchaseCanceled;


        /// <summary>
        /// WIll trigger after system purchase dialog is closed/canceled
        /// </summary>
        static public System.Action
            OnPurchaseFailure;

        private const string
            prefs_prefix = "BillingManager:",
            first_run_pref = prefs_prefix + "FirstRun",
            product_pref = prefs_prefix + "Product:";

        private readonly static IBillingProvider iap_provider =
#if USE_UNITY_IAP
            new UnityIAPProvider();
#elif USE_OPEN_IAB
            new OpenIABProvider();
#else
			null;
#endif



        public static bool IsInitialized
        {
            get
            {
                return iap_provider != null && iap_provider.IsInitialized();
            }
        }

        #region Find Product

        public static ProductProperties[] Products_SKU
        {
            get
            {
                return PSVSettings.settings.products_SKU;
            }
        }

        public static Products[] ads_related_products
        {
            get
            {
                return PSVSettings.settings.ads_related_products;
            }
        }

        [System.Obsolete( "Now you can get Products_SKU like array properties." )]
        public static ProductProperties[] GetProductIdentifiers()
        {
            return Products_SKU;
        }


        public static ProductProperties GetProperty( Products product )
        {
            for (int i = 0; i < Products_SKU.Length; i++)
            {
                if (Products_SKU[i].product == product)
                {
                    return Products_SKU[i];
                }
            }
            DebugLog( "Purchase " + product + " failed - no identifier found", true );
            return ProductProperties.empty;
        }

        public static ProductType GetProductType( Products prod )
        {
            return GetProperty( prod ).type;
        }

        public static string GetProductSku( Products prod )
        {
            return GetProperty( prod ).sku;
        }

        public static ProductProperties GetProperty( string sku_alias )
        {
            for (int i = 0; i < Products_SKU.Length; i++)
            {
                if (Products_SKU[i].sku.Equals( sku_alias ))
                {
                    return Products_SKU[i];
                }
            }
            DebugLog( "Purchase " + sku_alias + " failed - no identifier found", true );
            return ProductProperties.empty;
        }

        public static ProductType GetProductType( string sku_alias )
        {
            return GetProperty( sku_alias ).type;
        }

        public static Products GetProduct( string sku_alias )
        {
            return GetProperty( sku_alias ).product;
        }

        #endregion

        private static void DebugLog( string s, bool error = false )
        {
            string message = "BillingManager: " + s;
            if (error)
            {
                Debug.LogError( message );
            }
            else if (PSVSettings.settings.billing_manager_debug)
            {
                Debug.Log( message );
            }
        }

        [AwakeStatic]
        public static void AwakeStatic()
        {
            AwakeStaticAttribute.Done( typeof( BillingManager ) );
            DebugLog( "Initializing" );

            if (iap_provider != null)
            {
                iap_provider.OnMessageLogged += DebugLog;
                iap_provider.OnPurchaseCancelled += PurchaseCancelled;
                iap_provider.OnPurchaseFailure += PurchaseFailed;
                iap_provider.OnPurchaseSucceded += PurchaseSucceded;
                iap_provider.OnInitializationComplete += OnInitializationComplete;

                CoroutineHandler.Start( InitProviderKeyDelay() );
            }
        }

        private static IEnumerator InitProviderKeyDelay()
        {
            yield return new WaitForEndOfFrame();
            iap_provider.Init( PSVSettings.settings.public_key );
        }

        //Will trigger old purchases to process PurchaseSucceded event
        private static void OnInitializationComplete()
        {
            if (( Application.isEditor || Application.platform == RuntimePlatform.Android ) && !PlayerPrefs.HasKey( first_run_pref ))
            {
                PlayerPrefs.SetString( first_run_pref, "1" );
            }
        }

        public static void RestoreProducts()
        {
            if (iap_provider != null)
            {
                iap_provider.Restore();
            }
        }

        public static void Purchase( Products product )
        {
            DebugLog( "Trying to purchase " + product );
            ProductProperties prop = GetProperty( product );
            if (IsInitialized)
            {
                if (prop.product != Products.Empty)
                {
                    iap_provider.Purchase( prop );
                }

                DebugLog( "Purchase " + prop.ToString() );
            }
            else if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
            {
                PurchaseSucceded( prop.sku );
                DebugLog( "Does not have purchasing providers. Purchase Succeed only for Editor." );
            }
            else
            {
                DebugLog( product + " purchase failed because there are no purchasing providers.", true );
            }
        }

        [System.Obsolete( "Use Purchase(Products product) instead." )]
        public static void PurchaseSubscription( Products product )
        {
            Purchase( product );
        }

        public static bool IsProductPurchased( Products prod )
        {
            //TODO: check if needed to scan inventory too (UnityIAPProvider.m_StoreController.products.all)
            string pref = GetProductPref( prod.ToString() );
            bool res = PlayerPrefs.GetInt( pref, 0 ) == 1;
            DebugLog( "IsProductPurchased: " + pref + " - " + res );
            return res;

        }

        public static bool IsSKUPurchased( string sku )
        {
            ProductProperties prop = GetProperty( sku );
            Products product = prop.product;
            return IsProductPurchased( product );
        }

        //will save only consumable products, non consumable are meant to be used once and take effect only this moment (add coins, that are stored separately)
        private static void SavePurchaseState( Products prod, bool is_purchased )
        {
            DebugLog( "SavePurchaseState: " + prod.ToString() + " - " + is_purchased );
            PlayerPrefs.SetInt( GetProductPref( prod.ToString() ), is_purchased ? 1 : 0 );
            NextFrameCall.SavePrefs();
        }

        private static string GetProductPref( string prod )
        {
            return product_pref + prod;
        }

        private static bool CanDisableAds( Products product )
        {
            for (int i = 0; i < ads_related_products.Length; i++)
            {
                if (ads_related_products[i] == product)
                {
                    return true;
                }
            }
            return false;
        }


        private static bool ShouldCancelAdmob( Products product )
        {
            for (int i = 0; i < ads_related_products.Length; i++)
            {
                //check if other products were owned
                if (ads_related_products[i] != product && IsProductPurchased( ads_related_products[i] ))
                {
                    //will not cancel admob if there are other owned products that should disable ads
                    return false;
                }
            }
            return true;
        }


        private static void PurchaseSucceded( string sku )
        {
            DebugLog( "purchaseSuccededEvent: '" + sku + "'" );

            ProductProperties prop = GetProperty( sku );
            Products product = prop.product;

            SavePurchaseState( product, true );

            if (CanDisableAds( product ))
            {
                ManagerGoogle.DisableAdmob();
            }

            //custom callbacks
            PurchaseSucceded( product );

            if (OnPurchaseSucceded != null)
            {
                OnPurchaseSucceded( prop );
            }
        }

        private static void PurchaseCancelled( string sku )
        {
            ProductProperties prop = GetProperty( sku );
            Products product = prop.product;

            DebugLog( "purchaseCanceledEvent: '" + product + "'" );
            if (IsProductPurchased( product ))
            {
                DebugLog( "purchaseCancellation: '" + product + "' was purchased" );
                SavePurchaseState( product, false );

                if (CanDisableAds( product ) && ShouldCancelAdmob( product ))
                {
                    ManagerGoogle.EnableAdmob();
                }

                //custom callbacks
                PurchaseCancelled( product );

                if (OnPurchaseCanceled != null)
                {
                    OnPurchaseCanceled( prop );
                }
            }
        }


        private static void PurchaseFailed()
        {
            if (OnPurchaseFailure != null)
            {
                OnPurchaseFailure();
            }
        }
    }
}
