﻿using UnityEngine;
using System.Collections;

namespace PSV.IAP
{
    public class RestorePurchaseButton : ButtonClickHandler
    {
        void Start()
        {
            if (Application.platform != RuntimePlatform.IPhonePlayer)
                this.gameObject.SetActive( false );
        }

        protected override void OnButtonClick()
        {
            BillingManager.RestoreProducts();
        }
    }
}