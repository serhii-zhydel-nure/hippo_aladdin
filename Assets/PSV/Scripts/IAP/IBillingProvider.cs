﻿using UnityEngine;
using System;

namespace PSV.IAP
{

    public interface IBillingProvider
    {
        event Action<string>
            OnPurchaseSucceded,
            OnPurchaseCancelled;
        event Action
            OnInitializationComplete,
            OnPurchaseFailure;
        event Action<string, bool>
            OnMessageLogged;


        bool IsInitialized();


        void Init(string public_key);


        void Purchase(ProductProperties prod);


        void Restore();

    }
}