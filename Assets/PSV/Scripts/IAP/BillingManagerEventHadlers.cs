﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// implement here handling purchase state using own callbacks without events
/// </summary>

namespace PSV.IAP
{

    public static partial class BillingManager
    {

        private static void PurchaseSucceded( Products product )
        {
            //list here your custom callbacks
            switch (product)
            {
                //case Products.Custom_SKU:
                //{
                //    CustomCallback();
                //    break;
                //}
                default:
                {
                    return;
                }
            }
        }

        private static void PurchaseCancelled( Products product )
        {
            switch (product)
            {
                //list here your custom callbacks
                //case Products.Custom_SKU:
                //{
                //    CustomCallback();
                //    break;
                //}
                default:
                {
                    return;
                }
            }
        }

    }
}