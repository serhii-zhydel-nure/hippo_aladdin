﻿using UnityEngine;
using System.Collections;

namespace PSV.IAP
{

    public class PurchaseAdsButton : ButtonClickHandler
    {

        private new void Awake()
        {
            base.Awake();
            ManagerGoogle.OnAdsEnabled += EnableButton;
        }

        private void Start()
        {
            bool is_enabled = false;
            if (Application.isEditor || Application.isMobilePlatform)
                is_enabled = ManagerGoogle.IsAdmobEnabled();
            EnableButton( is_enabled );
        }

        private void OnDestroy()
        {
            ManagerGoogle.OnAdsEnabled -= EnableButton;
        }

        private void EnableButton( bool enabled )
        {
            gameObject.SetActive( enabled );
        }

        void Action()
        {
            if (Application.isEditor || Application.isMobilePlatform)
                BillingManager.Purchase( Products.SKU_ADMOB );
        }

        protected override void OnButtonClick()
        {
            Action();
        }
    }
}