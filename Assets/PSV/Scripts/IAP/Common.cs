﻿namespace PSV.IAP
{

	public delegate void PurchaseCallback( ProductProperties prop );


	public enum ProductType
	{
        /// <summary>
        /// Can be purchased repeatedly. Suitable for consumable products such as virtual
        /// currencies.
        /// </summary>
        Consumable = 0,

        /// <summary>
        /// Can only be purchased once. Suitable for one-off purchases such as extra levels.
        /// </summary>
        NonConsumable = 1,

        /// <summary>
        /// Can be purchased repeatedly and restored. Durable but with a finite duration
        /// of validity.
        /// </summary>
        Subscription = 2
	}

    [System.Serializable]
	public struct ProductProperties
    {
        public Products product;
        public string sku;
        public ProductType type;

        public ProductProperties( Products product, string sku, ProductType type )
        {
            this.product = product;
            this.sku = sku;
            this.type = type;
        }

        public ProductProperties( string sku, ProductType type )
        {
            this.product = Products.Empty;
            this.sku = sku;
            this.type = type;
        }

        /// <summary>
        /// Not equal default(ProductProperties) because default(ProductType) not is "NonConsumable"
        /// </summary>
        public static ProductProperties empty
        {
            get
            {
                return new ProductProperties( Products.Empty, string.Empty, ProductType.NonConsumable );
            }
        }

        override public string ToString()
        {
            return product + " - " + sku + " [" + type + "]";
        }
    }

}
