﻿using UnityEngine;
using System.Collections;

namespace PSV.IAP
{

    public class PurchaseButton : ButtonClickHandler
    {
        /// ChangeLog
        /// v2.0
        /// added support fro any product (just select enum item)


        public Products product = Products.SKU_ADMOB;

        private new void Awake()
        {
            base.Awake();
            BillingManager.OnPurchaseSucceded += OnPurchaseSucceded;
            BillingManager.OnPurchaseCanceled += OnPurchaseCancelled;
        }

        private void Start()
        {
            bool is_enabled = false;
            if (Application.isEditor || Application.isMobilePlatform)
                is_enabled = !BillingManager.IsProductPurchased( product );
            EnableButton( BillingManager.GetProperty( product ), is_enabled );
        }

        private void OnDestroy()
        {
            BillingManager.OnPurchaseSucceded -= OnPurchaseSucceded;
            BillingManager.OnPurchaseCanceled -= OnPurchaseCancelled;
        }

        void Action()
        {
            if (Application.isEditor || Application.isMobilePlatform)
                BillingManager.Purchase( product );
        }


        private void OnPurchaseSucceded( ProductProperties prop )
        {
            EnableButton( prop, false );
        }

        private void OnPurchaseCancelled( ProductProperties prop )
        {
            EnableButton( prop, true );
        }


        void EnableButton( ProductProperties prop, bool enable )
        {
            if (prop.product == product && prop.type == ProductType.NonConsumable)
            {
                this.gameObject.SetActive( enable );
            }
        }

        protected override void OnButtonClick()
        {
            Action();
        }
    }
}