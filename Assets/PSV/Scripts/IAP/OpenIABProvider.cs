﻿//use only one of billing providers. If Unity IAP enabled - remove OpenIAB SDK including JAR files
#if !UNITY_PURCHASING

//#define USE_OPEN_IAB

#endif


using UnityEngine;
using System.Collections.Generic;


#if USE_OPEN_IAB
using OnePF;
#endif


namespace PSV.IAP
{

    public class OpenIABProvider : IBillingProvider
    {
#pragma warning disable 0067
        public event System.Action<string>
            OnPurchaseSucceded,
            OnPurchaseCancelled;
        public event System.Action
            OnInitializationComplete,
            OnPurchaseFailure;
        public event System.Action<string, bool>
            OnMessageLogged;
#pragma warning restore 0067

        bool _isInitialized = false;

#if USE_OPEN_IAB
#pragma warning disable 414
        Inventory _inventory = null;
#pragma warning restore 414
#endif

        public bool IsInitialized()
        {
            return _isInitialized;
        }


        private void DebugLog( string s, bool error = false )
        {
            if (OnMessageLogged != null)
            {
                OnMessageLogged( s, error );
            }
        }


        public void Init( string public_key )
        {
#if USE_OPEN_IAB
            if (GameObject.FindObjectOfType<OpenIABEventManager>() == null)
            {
                GameObject e_man = new GameObject( "OpenIABEventManager" );
                e_man.AddComponent<OpenIABEventManager>();
            }
            OpenIABEventManager.billingSupportedEvent += BillingSupportedEvent;
            OpenIABEventManager.billingNotSupportedEvent += BillingNotSupportedEvent;
            OpenIABEventManager.queryInventorySucceededEvent += QueryInventorySucceededEvent;
            OpenIABEventManager.queryInventoryFailedEvent += QueryInventoryFailedEvent;
            OpenIABEventManager.purchaseSucceededEvent += PurchaseSucceededEvent;
            OpenIABEventManager.purchaseFailedEvent += PurchaseFailedEvent;
            OpenIABEventManager.consumePurchaseSucceededEvent += ConsumePurchaseSucceededEvent;
            OpenIABEventManager.consumePurchaseFailedEvent += ConsumePurchaseFailedEvent;

            if (string.IsNullOrEmpty( public_key ))
            {
                DebugLog( "Set proper PUBLIC_KEY for your project" );
            }
            else
            {
                MapSKU();
                var options = new Options
                {
                    checkInventoryTimeoutMs = Options.INVENTORY_CHECK_TIMEOUT_MS * 2,
                    discoveryTimeoutMs = Options.DISCOVER_TIMEOUT_MS * 2,
                    checkInventory = false,
                    verifyMode = OptionsVerifyMode.VERIFY_SKIP,
                    storeKeys = new Dictionary<string, string> { { OpenIAB_Android.STORE_GOOGLE, public_key } },
                    storeSearchStrategy = SearchStrategy.INSTALLER_THEN_BEST_FIT
                };

                OpenIAB.init( options );
            }
#else
			DebugLog( "Set up OpenIAB" );
#endif
        }


        public void Purchase( ProductProperties prod )
        {
            DebugLog( "Trying to purchase " + prod.sku );
#if USE_OPEN_IAB
            if (prod.type == ProductType.Subscription)
            {
                OpenIAB.purchaseSubscription( prod.sku );
            }
            else
            {
                OpenIAB.purchaseProduct( prod.sku );
            }
#else
			PurchaseFailed();
#endif
        }


        public void Restore()
        {
#if USE_OPEN_IAB
            RestoreProducts();
#endif
        }

        private void PurchaseSucceded( string sku )
        {
            if (OnPurchaseSucceded != null)
            {
                OnPurchaseSucceded( sku );
            }
        }

        private void PurchaseCancelled( string sku )
        {
            if (OnPurchaseCancelled != null)
            {
                OnPurchaseCancelled( sku );
            }
        }

        private void PurchaseFailed()
        {
            if (OnPurchaseFailure != null)
            {
                OnPurchaseFailure();
            }
        }

#if USE_OPEN_IAB

        private void MapSKU()
        {
            foreach (var item in BillingManager.Products_SKU)
            {
                string sku = item.sku;
                OpenIAB.mapSku( sku, OpenIAB_Android.STORE_GOOGLE, sku );
            }
        }

        private void BillingSupportedEvent()
        {
            _isInitialized = true;
            RestoreProducts();
            DebugLog( "billingSupportedEvent" );
            if (OnInitializationComplete != null)
            {
                OnInitializationComplete();
            }
        }

        //Fetching already purchased products (does not support IOS, use UnityIAP)
        private void RestoreProducts()
        {
            if (_isInitialized)
            {
                OpenIAB.queryInventory();
            }
        }


        private void BillingNotSupportedEvent( string error )
        {
            DebugLog( "billingNotSupportedEvent: " + error, true );
        }

        private void QueryInventorySucceededEvent( Inventory inventory )
        {
            DebugLog( "queryInventorySucceededEvent: " + inventory );
            ProcessRestoredProducts( inventory );
        }

        //restoring purchases
        private void ProcessRestoredProducts( Inventory inventory )
        {
            if (inventory != null)
            {
                _inventory = inventory;
                //check all purchases
                List<Purchase> purchases = inventory.GetAllPurchases();
                for (int i = 0; i < purchases.Count; i++)
                {
                    Purchase p = purchases[i];
                    if (p.PurchaseState == 0)
                    {
                        PurchaseSucceededEvent( p );
                    }
                    else
                    {
                        PurchaseCancelled( p.Sku );
                    }
                }
            }
        }

        private void QueryInventoryFailedEvent( string error )
        {
            DebugLog( "queryInventoryFailedEvent: " + error, true );
        }

        private void PurchaseSucceededEvent( Purchase purchase )
        {
            DebugLog( "purchaseSucceededEvent: " + purchase );
            PurchaseSucceded( purchase.Sku );

            ProductType type = BillingManager.GetProductType( purchase.Sku );
            if (type == ProductType.Consumable)
            {
                OpenIAB.consumeProduct( purchase );
            }
        }

        private void PurchaseFailedEvent( int errorCode, string errorMessage )
        {
            DebugLog( "purchaseFailedEvent: " + errorMessage, true );
            PurchaseFailed();
        }

        private void ConsumePurchaseSucceededEvent( Purchase purchase )
        {
            DebugLog( "consumePurchaseSucceededEvent: " + purchase );
        }

        private void ConsumePurchaseFailedEvent( string error )
        {
            DebugLog( "consumePurchaseFailedEvent: " + error, true );
        }


#endif
    }
}