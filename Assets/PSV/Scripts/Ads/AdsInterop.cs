﻿using System;


namespace PSV.ADS
{
    public enum AdPosition
    {
        Top_Centered,
        Top_Left,
        Top_Right,
        Bottom_Centered,
        Bottom_Left,
        Bottom_Right,
        Middle_Centered,
        Middle_Left,
        Middle_Right,
        Undefined
    }

    public static class AdsInterop
	{
		//events for Game
        public static event Action OnAdsReady;
		public static event Action<bool, bool, bool, bool> OnInitialiseAds;
		public static event Action<bool> OnShowInterstitialAd;
		public static event Action<bool> OnShowRewarded;
		public static event Action<bool> OnShowBannerAd;
		public static event Action<bool> OnShowNativeAd;
		public static event Action OnHideBannerAd;
		public static event Action OnHideNativeAd;
		public static event Action<AdPosition, AdSize> OnRefreshBannerAd;
		public static event Action<AdPosition, AdSize> OnRefreshNativeAd;
		//public static event Action<AdSize> OnResizeBannerAd;
		//public static event Action<AdSize> OnResizeNativeAd;
		public static event Action OnDisableAds;
		public static event Action OnEnableAds;

        //events for AdsMAnager
        public static event Action OnInterstitialShown;
		public static event Action OnInterstitialClosed;
		public static event Action OnRewardedShown;
		public static event Action OnRewardedCompleted;
		public static event Action OnRewardedClosed;

		//debug events for AdsManager
		public static event Action<string, bool> OnBannerEvent;
		public static event Action<string, bool> OnInterstitialEvent;
		public static event Action<string, bool> OnRewardedEvent;
		public static event Action<string, bool> OnNativeEvent;
        
        public static bool interstitial_shown = false;
        public static bool rewarded_shown = false;
        public static bool ads_ready = false;

        #region Methods for Game

        public static void InitialiseAds (bool ads_enabled, bool children_tagged, bool for_families, bool non_personalized)
        {
            if (OnInitialiseAds != null)
            {
                OnInitialiseAds(ads_enabled, children_tagged, for_families, non_personalized);
            }
        }

        public static void ShowBannerAd (bool show_home_ads = false)
		{
			if (OnShowBannerAd != null)
			{
				OnShowBannerAd ( show_home_ads );
			}
		}

		public static void HideBannerAd ()
		{
			if (OnHideBannerAd != null)
			{
				OnHideBannerAd ( );
			}
		}

		public static void RefreshBannerAd (AdPosition ad_pos, AdSize ad_size)
		{
			if (OnRefreshBannerAd != null)
			{
				OnRefreshBannerAd ( ad_pos, ad_size );
			}
		}

		public static void ShowNativeAd (bool show_home_ads = false)
		{
			if (OnShowNativeAd != null)
			{
				OnShowNativeAd ( show_home_ads );
			}
		}

		public static void HideNativeAd ()
		{
			if (OnHideNativeAd != null)
			{
				OnHideNativeAd ( );
			}
		}

		public static void RefreshNativeAd (AdPosition ad_pos, AdSize ad_size)
		{
			if (OnRefreshNativeAd != null)
			{
				OnRefreshNativeAd ( ad_pos, ad_size );
			}
		}

		public static bool ShowInterstitialAd (bool show_home_ads = false)
		{
			if (OnShowInterstitialAd != null)
			{
				OnShowInterstitialAd ( show_home_ads );
				return true;
			}
			return false;
		}

		public static bool ShowRewarded (bool show_home_ads = false)
		{
			if (OnShowRewarded != null)
			{
				OnShowRewarded ( show_home_ads );
				return true;
			}
			return false;
		}

		public static void DisableAds ()
		{
			if (OnDisableAds != null)
			{
				OnDisableAds ( );
			}
		}

        public static void EnableAds ()
        {
            if (OnEnableAds != null)
            {
                OnEnableAds();
            }
        }

        #endregion

        #region  methods for AdsManager

        /// <summary>
        /// Called when corresponding ads manager is ready to initialize.
        /// </summary>
        public static void AdsReady()
        {
            UnityMainThreadDispatcher.Enqueue( AdsReadyMainThread );
        }


        private static void AdsReadyMainThread()
        {
            ads_ready = true;
            if (OnAdsReady != null)
            {
                OnAdsReady();
            }
        }


        public static void InterstitialShown ()
		{
			UnityMainThreadDispatcher.Enqueue ( InterstitialShownMainThread );
		}

        private static void InterstitialShownMainThread()
        {
            interstitial_shown = true;
            if (OnInterstitialShown != null)
            {
                OnInterstitialShown();
            }
        }
        
        public static void InterstitialClosed ()
		{
			UnityMainThreadDispatcher.Enqueue ( InterstitialClosedMainThread );
		}

        private static void InterstitialClosedMainThread()
        {
            if (OnInterstitialClosed != null)
            {
                OnInterstitialClosed();
            }
            interstitial_shown = false;
        }

        public static void RewardedShown ()
		{
			UnityMainThreadDispatcher.Enqueue ( RewardedShownMainThread );
		}

        private static void RewardedShownMainThread()
        {
            rewarded_shown = true;
            if (OnRewardedShown != null)
            {
                OnRewardedShown();
            }
        }

        public static void RewardedCompleted ()
		{
			UnityMainThreadDispatcher.Enqueue ( RewardedCompletedMainThread );
		}

        private static void RewardedCompletedMainThread()
        {
            if (OnRewardedCompleted != null)
            {
                OnRewardedCompleted();
            }
        }

        public static void RewardedClosed ()
		{
			UnityMainThreadDispatcher.Enqueue ( RewardedClosedMainThread );
		}

        private static void RewardedClosedMainThread()
        {
            if (OnRewardedClosed != null)
            {
                OnRewardedClosed();
            }
            rewarded_shown = false;
        }


        #endregion

        #region Debug methods for AdsManager

        public static void BannerEvent (string net, bool available)
		{
			if (OnBannerEvent != null)
			{
				OnBannerEvent ( net, available );
			}
		}

		public static void NativeEvent (string net, bool available)
		{
			if (OnNativeEvent != null)
			{
				OnNativeEvent ( net, available );
			}
		}

		public static void RewardedEvent (string net, bool available)
		{
			if (OnRewardedEvent != null)
			{
				OnRewardedEvent ( net, available );
			}
		}

		public static void InterstitialEvent (string net, bool available)
		{
			if (OnInterstitialEvent != null)
			{
				OnInterstitialEvent ( net, available );
			}
		}
		#endregion
	}
}
