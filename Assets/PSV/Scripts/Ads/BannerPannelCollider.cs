﻿#if !UNITY_IOS
using UnityEngine;
using System.Collections;

namespace PSV.ADS
{

	[RequireComponent ( typeof ( BoxCollider2D ) )]
	public class BannerPannelCollider :BannerListener
	{
		public float margin = 0.2f;

		private BoxCollider2D
			collider2d;

		private Transform tr;

		private void Awake ()
		{
			collider2d = GetComponent<BoxCollider2D> ( );
			tr = transform;
		}


		override protected void BannerUpdated (Vector2 sz, Vector2 anchor)
		{
			if (collider2d != null)
			{
				Vector2 coll_size = Vector2.zero;
				Vector2 pos = Vector2.zero;
				Camera cam = Camera.main;


				if (cam == null)
				{
					cam = FindObjectOfType<Camera> ( );
				}
				if (cam != null)
				{
					Vector2 top_right = cam.ScreenToWorldPoint ( sz );
					Vector2 bottom_left = cam.ScreenToWorldPoint ( Vector2.zero );
					coll_size = top_right - bottom_left;
					pos = new Vector2 ( Screen.width * anchor.x, Screen.height * anchor.y );
					pos = cam.ScreenToWorldPoint ( pos );
				}
				//saving unscaled collider size
				Vector2 offset = coll_size * 0.5f;
				//applying scale to collider's size
				coll_size.x += coll_size.y * margin;
				coll_size.y += coll_size.y * margin;
				collider2d.size = coll_size;
				//normalizing anchor from (0 .. 0.5 .. 1) to (-1 .. 0 .. 1)
				anchor *= -2;
				offset.x *= anchor.x + 1;
				offset.y *= anchor.y + 1;
				collider2d.offset = offset;
				tr.position = pos;
			}
		}

	}
}
#endif