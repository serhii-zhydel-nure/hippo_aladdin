﻿using UnityEngine;
using AdmobType = AdmobAd.BannerAdType;
using AdmobAgetType = AdmobAdAgent.BannerAdType;

namespace PSV.ADS
{
    public class AdSize
    {
        public readonly int
                w,
                h;

        public readonly static AdSize
            Smart_banner = new AdSize( 1, 1 ),
            Standard_banner_320x50 = new AdSize( 320, 50 ),
            Large_banner_320x100 = new AdSize( 320, 100 ),
            IAB_medium_rectangle_300x250 = new AdSize( 300, 250 ),
            IAB_fullsize_banner_468x60 = new AdSize( 468, 60 ),
            IAB_leaderboard_728x90 = new AdSize( 728, 90 ),
            IAB_SkyScraper_160x600 = new AdSize( 160, 600 ),
            Native_banner_minimal_320x80 = new AdSize( 320, 80 );


        public AdSize( int w, int h )
        {
            this.w = w;
            this.h = h;
        }


        public static bool operator ==( AdSize s1, AdSize s2 )
        {
            if (ReferenceEquals( s1, s2 ))
            {
                return true;
            }
            else if (!ReferenceEquals( s1, null ) && !ReferenceEquals( s2, null ))
            {
                return s1.w == s2.w && s1.h == s2.h;
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=( AdSize s1, AdSize s2 )
        {
            return !(s1 == s2);
        }
        
        #region Converters

        public Vector2 ConvertToPixels()
        {
            Vector2 b_size = new Vector2( w, h );
            float coef = (Screen.dpi / 160f);
            return b_size * coef;
        }

        public static Vector2 ConvertToPixels( AdSize size )
        {
            if (ReferenceEquals( size, null ))
                return Vector2.zero;
            else
                return size.ConvertToPixels();
        }

        public Vector2 ToVector2()
        {
            return new Vector2( w, h );
        }

        public static explicit operator Vector2( AdSize size )
        {
            return new Vector2( size.w, size.h );
        }

        public static explicit operator GoogleMobileAds.Api.AdSize( AdSize ad_size )
        {
            if (ad_size == Smart_banner)
            {
                return GoogleMobileAds.Api.AdSize.SmartBanner;
            }
            else
            {
                return new GoogleMobileAds.Api.AdSize( ad_size.w, ad_size.h );
            }
        }

        public static explicit operator AdmobType( AdSize size )
        {
            if      (size == IAB_fullsize_banner_468x60)     return AdmobType.Tablets_IAB_Banner_468x60;
            else if (size == IAB_leaderboard_728x90)         return AdmobType.Tablets_IAB_LeaderBoard_728x90;
            else if (size == IAB_medium_rectangle_300x250)   return AdmobType.Tablets_IAB_MRect_300x250;
            else if (size == IAB_SkyScraper_160x600)         return AdmobType.Tablets_IAB_SkyScraper_160x600;
            else if (size == Standard_banner_320x50)         return AdmobType.Universal_Banner_320x50;
            else if (size == Smart_banner)                   return AdmobType.Universal_SmartBanner;
            else                                             return AdmobType.Universal_Banner_320x50;
        }

        public static explicit operator AdmobAgetType( AdSize size )
        {
            //return (AdmobAgetType)((AdmobType)size);
            if      (size == IAB_fullsize_banner_468x60)     return AdmobAgetType.Tablets_IAB_Banner_468x60;
            else if (size == IAB_leaderboard_728x90)         return AdmobAgetType.Tablets_IAB_LeaderBoard_728x90;
            else if (size == IAB_medium_rectangle_300x250)   return AdmobAgetType.Tablets_IAB_MRect_300x250;
            else if (size == IAB_SkyScraper_160x600)         return AdmobAgetType.Tablets_IAB_SkyScraper_160x600;
            else if (size == Standard_banner_320x50)         return AdmobAgetType.Universal_Banner_320x50;
            else if (size == Smart_banner)                   return AdmobAgetType.Universal_SmartBanner;
            else                                             return AdmobAgetType.Universal_Banner_320x50;
        }
        
        #endregion


        public override string ToString()
        {
            return "(" + this.w + " : " + this.h + ")";
        }
        
        public override bool Equals( object obj )
        {
            if (!(obj is AdSize))
            {
                return false;
            }

            var size = (AdSize)obj;
            return w == size.w &&
                   h == size.h;
        }

        public override int GetHashCode()
        {
            var hashCode = -1930607363;
            hashCode = hashCode * -1521134295 + w.GetHashCode();
            hashCode = hashCode * -1521134295 + h.GetHashCode();
            return hashCode;
        }
    }
}