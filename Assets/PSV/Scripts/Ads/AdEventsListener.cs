﻿using UnityEngine;
using System.Collections.Generic;

namespace PSV.ADS
{
    public static class AdEventsListener
    {
        public static System.Action<string, EventSource, bool> OnAdEvent;
        
        public enum EventSource
        {
            Banner,
            Interstitial,
            Rewarded,
            Native,
        }
        
        private static Dictionary<string, bool>
            banner_ready = new Dictionary<string, bool> ( ),
            interstitial_ready = new Dictionary<string, bool> ( ),
            native_ready = new Dictionary<string, bool> ( ),
            rewarded_ready = new Dictionary<string, bool> ( );

        [AwakeStatic]
        public static void AwakeStatic()
        {
            AwakeStaticAttribute.Done( typeof( AdEventsListener ) );
            AdsInterop.OnBannerEvent += AdsInterop_OnBannerEvent;
            AdsInterop.OnInterstitialEvent += AdsInterop_OnInterEvent;
            AdsInterop.OnNativeEvent += AdsInterop_OnNativeEvent;
            AdsInterop.OnRewardedEvent += AdsInterop_OnRewardedEvent;
        }
        
        private static void AdsInterop_OnRewardedEvent (string net, bool available)
        {
            AdEvent ( net, EventSource.Rewarded, available );
        }

        private static void AdsInterop_OnNativeEvent (string net, bool available)
        {
            AdEvent ( net, EventSource.Native, available );
        }

        private static void AdsInterop_OnInterEvent (string net, bool available)
        {
            AdEvent ( net, EventSource.Interstitial, available );
        }

        private static void AdsInterop_OnBannerEvent (string net, bool available)
        {
            AdEvent ( net, EventSource.Banner, available );
        }


        private static void AdEvent (string net, EventSource src, bool available)
        {
            Dictionary<string, bool> dic = GetAdReady ( src );
            if (!dic.ContainsKey ( net ))
            {
                dic.Add ( net, available );
            }
            else
            {
                dic [net] = available;
            }

            if (OnAdEvent != null)
            {
                OnAdEvent ( net, src, available );
            }
        }


        public static bool GetSrcReady (EventSource src)
        {
            bool res = false;
            Dictionary<string, bool> dic = GetAdReady ( src );
            foreach (var item in dic)
            {
                if (item.Value)
                {
                    res = true;
                    break;
                }
            }
            return res;
        }


        public static Dictionary<string, bool> GetAdReady (EventSource src)
        {
            switch (src)
            {
                case EventSource.Banner:
                    return banner_ready;
                case EventSource.Interstitial:
                    return interstitial_ready;
                case EventSource.Native:
                    return native_ready;
                case EventSource.Rewarded:
                    return rewarded_ready;
                default:
                    return new Dictionary<string, bool>();
            }
        }

       

    }
}