﻿#if UNITY_ANDROID
using UnityEngine;
using System.Collections;

namespace PSV.ADS
{
    public static class ImmersiveModeEnabler
    {
        static AndroidJavaObject unityActivity;
        static AndroidJavaObject javaObj;
        static AndroidJavaClass javaClass;
        static bool paused;

        [AwakeStatic]
        public static void AwakeStatic()
        {
            AwakeStaticAttribute.Done( typeof( ImmersiveModeEnabler ) );
            if (!Application.isEditor)
                HideNavigationBar();
            ApplicationEventAgent.OnPause += OnApplicationPause;
            ApplicationEventAgent.OnFocus += OnApplicationFocus;
        }

        static void HideNavigationBar()
        {
            //lock(this) //TODO: check it
            {
                using (javaClass = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ))
                {
                    unityActivity = javaClass.GetStatic<AndroidJavaObject>( "currentActivity" );
                }

                if (unityActivity == null)
                {
                    return;
                }

                using (javaClass = new AndroidJavaClass( "com.rak24.androidimmersivemode.Main" ))
                {
                    if (javaClass == null || javaClass.GetRawClass() == System.IntPtr.Zero)
                    {
                        return;
                    }
                    else
                    {
                        javaObj = javaClass.CallStatic<AndroidJavaObject>( "instance" );
                        if (javaObj == null)
                            return;
                        unityActivity.Call( "runOnUiThread", new AndroidJavaRunnable( () =>
                            {
                                javaObj.Call( "EnableImmersiveMode", unityActivity );
                            } ) );
                    }
                }
            }
        }

        static void OnApplicationPause( bool pausedState )
        {
            paused = pausedState;
        }

        static void OnApplicationFocus( bool hasFocus )
        {
            if (hasFocus)
            {
                if (javaObj != null && paused != true)
                {
                    unityActivity.Call( "runOnUiThread", new AndroidJavaRunnable( () =>
                        {
                            javaObj.CallStatic( "ImmersiveModeFromCache", unityActivity );
                        } ) );
                }
            }

        }

        public static void PinThisApp() // Above android 5.0 - App Pinning
        {
            if (javaObj != null)
            {
                javaObj.CallStatic( "EnableAppPin", unityActivity );
            }
        }

        public static void UnPinThisApp() // Unpin the app
        {
            if (javaObj != null)
            {
                javaObj.CallStatic( "DisableAppPin", unityActivity );
            }
        }
    }
}
#endif