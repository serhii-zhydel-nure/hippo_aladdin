﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PSV.ADS
{

    public static class AdUtils
    {
        /// <summary>
        /// Calculates Screen rect position for banner of certain size and at certain position 
        /// </summary>
        /// <param name="ad_position"></param>
        /// <param name="ad_size_px"></param>
        /// <returns></returns>
        public static Vector2 GetBannerTopLeftCorner( this AdPosition ad_position, Vector2 ad_size_px )
        {
            Vector2 res = Vector2.zero;
            switch (ad_position)
            {
                case AdPosition.Bottom_Centered:
                    res.y = Screen.height - ad_size_px.y;
                    res.x = Screen.width * 0.5f - ad_size_px.x * 0.5f;
                    break;
                case AdPosition.Top_Centered:
                    res.y = 0;
                    res.x = Screen.width * 0.5f - ad_size_px.x * 0.5f;
                    break;
                case AdPosition.Bottom_Left:
                    res.y = Screen.height - ad_size_px.y;
                    res.x = 0;
                    break;
                case AdPosition.Bottom_Right:
                    res.y = Screen.height - ad_size_px.y;
                    res.x = Screen.width - ad_size_px.x;
                    break;
                case AdPosition.Top_Left:
                    res.y = 0;
                    res.x = 0;
                    break;
                case AdPosition.Top_Right:
                    res.y = 0;
                    res.x = Screen.width - ad_size_px.x;
                    break;
                case AdPosition.Middle_Centered:
                    res.y = Screen.height * 0.5f - ad_size_px.y * 0.5f;
                    res.x = Screen.width * 0.5f - ad_size_px.x * 0.5f;
                    break;
                case AdPosition.Middle_Left:
                    res.y = Screen.height * 0.5f - ad_size_px.y * 0.5f;
                    res.x = 0;
                    break;
                case AdPosition.Middle_Right:
                    res.y = Screen.height * 0.5f - ad_size_px.y * 0.5f;
                    res.x = Screen.width - ad_size_px.x;
                    break;
            }
            return res;
        }


        //should be ordered by aspect to keep search correct
        private static List<BannerProp> banner_aspects = new List<BannerProp>()
            {
                new BannerProp(new Vector2(640, 100),  new Vector2(2048,1536)),      //(4x3 Xiaomi MiPad2)
                new BannerProp(new Vector2(480,75),    new Vector2(800,480)),        //(16.5/10 Samsung J1)
                new BannerProp(new Vector2(960, 150)*1.1f,  new Vector2(1920,1080)),      //(16x9 FullHD devices)
                new BannerProp(new Vector2(640,100),   new Vector2(1280,640)),       //(18x9 Doogee)
                new BannerProp(new Vector2(480,75)*1.1f,    new Vector2(1110,540)),       //(18.5x9 Samsung S8)
            };



        private static BannerProp GetBannerProp( float aspect )
        {
            int res = -1;
            for (int i = 0; i < banner_aspects.Count; i++)
            {
                res = i;
                if (aspect <= banner_aspects[res].aspect)
                {
                    //check if previous aspect value was closer to current
                    if (res > 0 && (aspect - banner_aspects[res - 1].aspect) < (banner_aspects[res].aspect - aspect))
                    {
                        res--;
                    }
                    break;
                }
            }
            return res >= 0 ? banner_aspects[res] : null;
        }

        public static Vector2 GetAdSizeForScreen( this AdSize ad_size, float screen_width, float screen_height )
        {
            float aspect = (float) Mathf.Max( screen_width, screen_height ) / (float) Mathf.Min( screen_width, screen_height );
            BannerProp banner_prop = GetBannerProp( aspect );
            if (banner_prop != null)
            {
                return banner_prop.GetAdSizeOnScreen( ad_size, screen_width, screen_height );
            }
            return Vector2.zero;
        }


        public static Vector2 GetAdSizeForCurrentScreen( this AdSize ad_size )
        {
            return GetAdSizeForScreen( ad_size, Screen.width, Screen.height );
        }


        public static void DrawBannerRectGUI( AdSize banner_size, AdPosition banner_pos, float aspect = 0, Color color = default( Color ), string label = "" )
        {
            GUI.Button( GetBannerRect( banner_size, banner_pos, aspect, color ), label + banner_size.ToString() + "\nDebug content overlap at\n800x480 and 2960x1440 resolutions" );
        }

        public static Rect GetBannerRect( AdSize banner_size, AdPosition banner_pos, float aspect, Color color )
        {
            Vector2 banner_size_px = Vector2.zero;
            Vector2 pos;
            if (aspect == 0)
            {
                banner_size_px = banner_size.GetAdSizeForCurrentScreen();

            }
            else
            {
                BannerProp b_prop = GetBannerProp( aspect );
                if (b_prop != null)
                {
                    banner_size_px = b_prop.GetAdSizeOnScreen( banner_size, Screen.width, Screen.height );
                }
            }
            pos = banner_pos.GetBannerTopLeftCorner( banner_size_px );
            if (color == default( Color )) color = Color.white;
            color.a = 0.5f;
            GUI.color = color;

            return new Rect( pos.x, pos.y, banner_size_px.x, banner_size_px.y );
        }

        private class BannerProp
        {
            public float aspect;
            public float banner_scale;
            public Vector2 resolution;

            public BannerProp( Vector2 banner_size, Vector2 resolution )
            {
                this.aspect = resolution.x / resolution.y;
                this.resolution = resolution;
                this.banner_scale = banner_size.x / 320f;
            }

            public float GetScreenScale( float width, float height )
            {
                return Mathf.Max( width, height ) / Mathf.Max( resolution.x, resolution.y );
            }

            public Vector2 GetAdSizeOnScreen( AdSize ad_size, float screen_width, float screen_height )
            {
                float screen_scale = GetScreenScale( screen_width, screen_height );
                float scale = banner_scale * screen_scale;
                if (ad_size == AdSize.Smart_banner)
                {
                    //smart banner (will show at maximum height)
                    return new Vector2( Screen.width, 40 * scale );
                }
                else
                {
                    //fixed size banner                
                    return ad_size.ToVector2() * scale;
                }
            }
        }
    }



}