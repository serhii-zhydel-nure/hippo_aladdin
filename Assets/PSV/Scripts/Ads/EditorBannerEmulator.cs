﻿#if UNITY_EDITOR
using UnityEngine;


namespace PSV.ADS
{

    [ExecuteInEditMode]
    public class EditorBannerEmulator : MonoBehaviour
    {

        [UnityEditor.MenuItem( "HelpTools/ADS/Create Banner emulator for Editor" )]
        [UnityEditor.MenuItem( "GameObject/Create Banner emulator for Editor", false, 0 )]
        public static void CreateInstance()
        {
            new GameObject( "EditorBannerEmulator", typeof( EditorBannerEmulator ) );
        }

        void OnGUI()
        {
            if (!Application.isPlaying)
            {
                EmulateBanner();
                //EmulateBanner(4f/3f, Color.cyan);
            }
        }


        static void EmulateBanner()
        {
            AdSize banner_size = GetCurrentAdSize();
            AdUtils.DrawBannerRectGUI( banner_size, GetCurrentAdPosition(), label: "BANNER_AD " );
       }

        static void EmulateBanner( float aspect, Color color )
        {
            AdUtils.DrawBannerRectGUI( GetCurrentAdSize(), GetCurrentAdPosition(), aspect, color, "BANNER_AD " );
        }

        static AdPosition GetCurrentAdPosition()
        {
            Scenes active_scene;
            GetActiveScene( out active_scene );
            return SceneLoaderSettings.small_banner_position_override.ContainsKey( active_scene ) ?
                SceneLoaderSettings.small_banner_position_override[active_scene] :
                SceneLoaderSettings.small_banner_default_pos;
        }


        static AdSize GetCurrentAdSize()
        {
            Scenes active_scene;
            if (GetActiveScene( out active_scene ))
            {
                if (SceneLoaderSettings.not_allowed_small_banner.Contains( active_scene ))
                {
                    return new AdSize( 0, 0 );
                }
                if (SceneLoaderSettings.small_banner_size_override.ContainsKey( active_scene ))
                {
                    return SceneLoaderSettings.small_banner_size_override[active_scene];
                }
            }
            return SceneLoaderSettings.small_banner_default_size;
        }

        static bool GetActiveScene( out Scenes scene )
        {
            object obj = null;
            try { System.Enum.Parse( typeof( Scenes ), UnityEngine.SceneManagement.SceneManager.GetActiveScene().name ); } catch { }
            if (obj != null)
            {
                scene = (Scenes) obj;
                return true;
            }
            scene = default( Scenes );
            return false;
        }
    }
}
#endif