﻿#define BRAND_NEW_PROTOTYPE
#if !UNITY_IOS // Deleted for IOS
using UnityEngine;

namespace PSV.ADS
{
	public class BannerListener :MonoBehaviour
	{

		[ContextMenu( "UpdateBanner")]
		protected void UpdateBannerPanel ()
		{
		    BannerUpdated ( ManagerGoogle.GetBannerSizeInPX ( ), GetAnchor ( ManagerGoogle.GetAdPos ( ) ) );
		}


		virtual protected void BannerUpdated (Vector2 sz, Vector2 anchor)
		{

		}



#if BRAND_NEW_PROTOTYPE
		virtual protected void OnEnable ()
		{
			UpdateBannerPanel ( );
			ManagerGoogle.OnAdsEnabled += (bool param) => UpdateBannerPanel ( );
			AdsInterop.OnHideBannerAd += UpdateBannerPanel;
			AdsInterop.OnShowBannerAd += (bool param) => UpdateBannerPanel ( );
			AdsInterop.OnRefreshBannerAd += (AdPosition pos, AdSize size) => UpdateBannerPanel ( );
		}

		virtual protected void OnDisable ()
		{
			ManagerGoogle.OnAdsEnabled -= (bool param) => UpdateBannerPanel ( );
			AdsInterop.OnHideBannerAd -= UpdateBannerPanel;
			AdsInterop.OnShowBannerAd -= (bool param) => UpdateBannerPanel ( );
			AdsInterop.OnRefreshBannerAd -= (AdPosition pos, AdSize size) => UpdateBannerPanel ( );
		}


		private Vector2 GetAnchor (AdPosition ad_position)
		{
			Vector2 res = Vector2.zero;
			switch (ad_position)
			{
				case AdPosition.Bottom_Centered:
					res.y = 0;
					res.x = 0.5f;
					break;
				case AdPosition.Top_Centered:
					res.y = 1;
					res.x = 0.5f;
					break;
				case AdPosition.Bottom_Left:
					res.y = 0;
					res.x = 0;
					break;
				case AdPosition.Bottom_Right:
					res.y = 0;
					res.x = 1;
					break;
				case AdPosition.Top_Left:
					res.y = 1;
					res.x = 0;
					break;
				case AdPosition.Top_Right:
					res.y = 1;
					res.x = 1;
					break;
				case AdPosition.Middle_Centered:
					res.y = 0.5f;
					res.x = 0.5f;
					break;
				case AdPosition.Middle_Left:
					res.y = 0.5f;
					res.x = 0;
					break;
				case AdPosition.Middle_Right:
					res.y = 0.5f;
					res.x = 1;
					break;
			}
			return res;
		}

#else
	virtual protected void OnEnable ()
	{
		ManagerGoogle.OnAdsDisabled += UpdateBannerPanel;
		ManagerGoogle.OnRefreshBannerAd += UpdateBannerPanel;
	}

	virtual protected void OnDisable ()
	{
		ManagerGoogle.OnAdsDisabled -= UpdateBannerPanel;
		ManagerGoogle.OnRefreshBannerAd -= UpdateBannerPanel;
	}

	private Vector2 GetAnchor (AdmobAd.AdLayout ad_position)
	{
		Vector2 res = Vector2.zero;
		switch (ad_position)
		{
			case AdmobAd.AdLayout.Bottom_Centered:
				res.y = 0;
				res.x = 0.5f;
				break;
			case AdmobAd.AdLayout.Top_Centered:
				res.y = 1;
				res.x = 0.5f;
				break;
			case AdmobAd.AdLayout.Bottom_Left:
				res.y = 0;
				res.x = 0;
				break;
			case AdmobAd.AdLayout.Bottom_Right:
				res.y = 0;
				res.x = 1;
				break;
			case AdmobAd.AdLayout.Top_Left:
				res.y = 1;
				res.x = 0;
				break;
			case AdmobAd.AdLayout.Top_Right:
				res.y = 1;
				res.x = 1;
				break;
			case AdmobAd.AdLayout.Middle_Centered:
				res.y = 0.5f;
				res.x = 0.5f;
				break;
			case AdmobAd.AdLayout.Middle_Left:
				res.y = 0.5f;
				res.x = 0;
				break;
			case AdmobAd.AdLayout.Middle_Right:
				res.y = 0.5f;
				res.x = 1;
				break;
		}
		return res;
	}

	//insert in ManagerGoogle
	//public static event Action OnAdsDisabled;
	//public static event Action OnRefreshBannerAd;

	//public bool GetBannerVisible ()
	//{
	//	return banner_visible;
	//}


	//public Vector2 GetBannerSizeInPX ()
	//{

	//	Vector2 b_size = banner_size != null ? new Vector2 ( banner_size.x, banner_size.y ) : Vector2.zero;
	//	float coef = (Screen.dpi / 160f);
	//	return b_size * coef * (GetBannerVisible ( ) && !IsAdmobDisabled ( ) ? 1 : 0);
	//}



	//public AdmobAd.AdLayout GetAdPos ()
	//{
	//	return AdmobManager.Instance.BannerAdPosition;
	//}

#endif

	}
}
#endif