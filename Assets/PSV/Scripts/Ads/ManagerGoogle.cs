﻿using System;
using UnityEngine;
using PSV;
using PSV.ADS;
using System.Collections;

/// ChangeLog:
/// - Removed initialization of BillingManager - it is now performed in its StaticAwake method
/// 


public static partial class ManagerGoogle
{
    #region Events
    public static event Action<bool>
        OnAdsEnabled;

    public static event Action OnInterstitialShown
    {
        add { AdsInterop.OnInterstitialShown += value; }
        remove { AdsInterop.OnInterstitialShown -= value; }
    }
    public static event Action OnInterstitialClosed
    {
        add { AdsInterop.OnInterstitialClosed += value; }
        remove { AdsInterop.OnInterstitialClosed -= value; }
    }

    public static event Action OnRewardedShown
    {
        add { AdsInterop.OnRewardedShown += value; }
        remove { AdsInterop.OnRewardedShown -= value; }
    }
    public static event Action OnRewardedClosed
    {
        add { AdsInterop.OnRewardedClosed += value; }
        remove { AdsInterop.OnRewardedClosed -= value; }
    }
    public static event Action OnRewardedComplete
    {
        add { AdsInterop.OnRewardedCompleted += value; }
        remove { AdsInterop.OnRewardedCompleted -= value; }
    }


    private static Action on_rewarded_successful;
    private static Action on_rewarded_fail;

    #endregion

    #region Fields

    private static float
        last_interstitial_time = 0, //0
        last_rewarded_time = 0;

    private static float interstitial_time { get { return PSVSettings.settings.ads_interstitial_delay; } }
    private static float rewarded_time { get { return PSVSettings.settings.ads_rewarded_vidoe_delay; } }

    private const bool
        children_tagged = true, //this flag forces sdk to filter ads with violence, drugs, etc
        for_families = false, //this flag forces SDK to request on family ads (poor fill rate)
        non_personalized = false, //GDPR Consent SDK Implementation
        single_ad_mode = true; //only one banner ad can be visible (banner ad or Native ad)

    private const string
        //children_tagged_param = "CHILDREN_TAGGED",
        //for_families_param = "FOR_FAMILIES",
        //interstitial_interval_param = "INTERSTITIAL_DELAY",
        //first_interstitial_param = "FIRST_INTERSTITIAL_DELAY",
        purchased_data_pref = "AdmobDisabled";

    private static bool
        banner_use_home_ads = false,
        native_use_home_ads = false,
        external_link_present = false,
        banner_visible = false,
        native_visible = false,
        ads_enabled = true;

    private static AdPosition
        banner_pos = AdPosition.Bottom_Centered,
        native_pos = AdPosition.Bottom_Centered;

    private static AdSize
        banner_size = AdSize.Standard_banner_320x50,
        native_size = AdSize.Native_banner_minimal_320x80;

    #endregion

    #region Editor tools
#if UNITY_EDITOR
    [UnityEditor.MenuItem( "HelpTools/ADS/ResetInterstitilaTime" )]
    private static void MenuResetInterstitilaTime()
    {
        last_interstitial_time = 0;
    }

    [UnityEditor.MenuItem( "HelpTools/ADS/ShowFullscreenBanner" )]
    private static void MenuShowInterstitial()
    {
        ShowInterstitial();
    }

    [UnityEditor.MenuItem( "HelpTools/ADS/DisableAdmob" )]
    private static void MenuDisableAdmob()
    {
        SetAdsEnabled( false );
    }

    [UnityEditor.MenuItem( "HelpTools/ADS/EnableAdmob" )]
    private static void MenuEnableAdmob()
    {
        SetAdsEnabled( true );
    }
#endif
    #endregion

    [AwakeStatic]
    public static void AwakeStatic()
    {
        AwakeStaticAttribute.Done( typeof( ManagerGoogle ) );
        Subscribe();
        if (AdsInterop.ads_ready)
            Init();
    }

    private static void Init()
    {
        LogMessage( "Initializing" );

        if (Application.isEditor || Application.isMobilePlatform)
        {
            //if using AdsInterop there can be no admob manager as it is about ad providers
            //so we will perform initialization of purchased data here

            if (PlayerPrefs.HasKey( purchased_data_pref ))
            {
                ads_enabled = PlayerPrefs.GetInt( purchased_data_pref ) == 0;
            }
            else
            {
                ads_enabled = true;
            }
        }

        AdsInterop.InitialiseAds( IsAdmobEnabled(), children_tagged, for_families, non_personalized ); //sending ads_enabled to leave active rewarded video
        
        RefreshSmallBanner( banner_pos, banner_size );
        RefreshNativeBanner( native_pos, native_size );
    }
    

    private static void LogMessage( string message )
    {
        if (PSVSettings.settings.manager_google_debug)
        {
            Debug.Log( "ManagerGoogle: " + message );
        }
    }


    #region EventHandlers	

    private static void Subscribe()
    {
        AdsInterop.OnAdsReady += Init;
        AdsInterop.OnInterstitialClosed += InterstitialClosed;
        AdsInterop.OnRewardedClosed += RewardedClosed;
        AdsInterop.OnRewardedCompleted += RewardedComplete;
        ExternalLinksManager.OnExternalLinksVisible += ExternalLinkPresent;
    }


    private static void Unsubscribe()
    {
        AdsInterop.OnAdsReady -= Init;
        AdsInterop.OnInterstitialClosed -= InterstitialClosed;
        AdsInterop.OnRewardedClosed -= RewardedClosed;
        AdsInterop.OnRewardedCompleted -= RewardedComplete;
        ExternalLinksManager.OnExternalLinksVisible -= ExternalLinkPresent;
    }

    private static void ExternalLinkPresent( bool param )
    {
        external_link_present = param;
        if (param)
        {
            //hide ads
            HideNativeBanner( false );
            HideSmallBanner( false );
        }
        else
        {
            //show ads if were any
            if (native_visible)
            {
                ShowNativeBanner( native_use_home_ads );
            }
            else if (banner_visible)
            {
                ShowSmallBanner( banner_use_home_ads );
            }
        }
    }

    private static void InterstitialClosed()
    {
        LogMessage( "InterstitialClosed" );
        if (AdsInterop.interstitial_shown)
        {
            ResetLastInterstitialTime();
        }
    }

    private static void RewardedComplete()
    {
        LogMessage( "RewardedVideoComplete" );
        if (on_rewarded_successful != null)
        {
            on_rewarded_successful();
            on_rewarded_successful = null;
        }
        on_rewarded_fail = null;
    }

    private static void RewardedClosed()
    {
        LogMessage( "RewardedVideoClosed" );
        if (AdsInterop.rewarded_shown)
        {
            ResetLastRewardedTime();
        }
        if (on_rewarded_fail != null)
        {
            on_rewarded_fail();
            on_rewarded_fail = null;
        }
        on_rewarded_successful = null;
    }
    #endregion

    #region General Methods


    private static void SetAdsEnabled( bool param, bool save = true, bool announce = true )
    {
        ads_enabled = param;
        if (save)
        {
            PlayerPrefs.SetInt( purchased_data_pref, ads_enabled ? 0 : 1 );
            NextFrameCall.SavePrefs();
        }
        if (announce)
        {
            if (ads_enabled)
            {
                AdsInterop.EnableAds();
            }
            else
            {
                AdsInterop.DisableAds();
            }
            if (OnAdsEnabled != null)
            {
                OnAdsEnabled( ads_enabled );
            }
        }
    }

    public static bool GetBannerVisible()
    {
        return banner_visible && !external_link_present;
    }

    public static void DisableAdmob()
    {
        SetAdsEnabled( false );
    }

    public static void EnableAdmob()
    {
        SetAdsEnabled( true );
    }

    public static Vector2 GetBannerSizeInPX()
    {
        if (GetBannerVisible() && IsAdmobEnabled())
            return AdSize.ConvertToPixels( banner_size );
        else
            return Vector2.zero;
    }

    public static AdSize GetAdSize()
    {
        return banner_size;
    }

    public static AdPosition GetAdPos()
    {
        return banner_pos;
    }


    public static AdSize GetNativeSize()
    {
        return native_size;
    }

    public static AdPosition GetNativePos()
    {
        return native_pos;
    }

    [Obsolete( "Now this setting open in PSV Settings Windows - ADS", true )]
    public static void SetInterstitialInterval( float val ) { }
    [Obsolete( "Now this setting open in PSV Settings Windows - ADS", true )]
    private static void UpdateRewardedTime( float val ) { }

    public static void ResetLastInterstitialTime()
    {
        last_interstitial_time = Time.time;
    }

    public static void ResetLastRewardedTime()
    {
        last_rewarded_time = Time.time;
    }

    public static bool IsAdmobEnabled()
    {
        return ads_enabled;
    }  

    private static bool GetSrcReady( AdEventsListener.EventSource src )
    {
        return Application.isEditor || AdEventsListener.GetSrcReady( src );
    }
    #endregion

    #region Banner

    public static bool IsBannerReady()
    {
        return GetSrcReady( AdEventsListener.EventSource.Banner );
    }

    public static void HideSmallBanner( bool save_state = true )
    {
        LogMessage( "Hide banner" );
        if (save_state)
        {
            banner_visible = false;
        }
        AdsInterop.HideBannerAd();
    }

    public static void ShowSmallBanner( bool show_home_ads = false, AdPosition ad_pos = AdPosition.Undefined )
    {

        banner_use_home_ads = show_home_ads;
        if (single_ad_mode && native_visible)
        {
            LogMessage( "Single Ad Mode: Hiding NativeBanner" );
            HideNativeBanner();
        }
        if (IsAdmobEnabled())
        {
            banner_visible = true;
            if (!external_link_present)
            {
                LogMessage( "Show banner, external_link_present = " + external_link_present );
                if (ad_pos != AdPosition.Undefined)
                {
                    RefreshSmallBanner( ad_pos, banner_size );
                }
                AdsInterop.ShowBannerAd( show_home_ads );
            }
        }
    }

    public static void RefreshSmallBanner( AdPosition ad_pos, AdSize ad_size )
    {
        banner_pos = ad_pos;
        banner_size = ad_size;

        //check if banner in needed position to avoid its unnecessary reload
        if (IsAdmobEnabled())
        {
            AdsInterop.RefreshBannerAd( banner_pos, banner_size );
        }
    }


    #endregion

    #region Native
    public static bool IsNativeReady()
    {
        return GetSrcReady( AdEventsListener.EventSource.Native );
    }

    public static void HideNativeBanner( bool save_state = true )
    {
        LogMessage( "Hide native" );
        if (save_state)
            native_visible = false;
        AdsInterop.HideNativeAd();
    }


    public static void ShowNativeBanner( bool show_home_ads = false )
    {
        native_use_home_ads = show_home_ads;
        if (single_ad_mode && banner_visible)
        {
            LogMessage( "Single Ad Mode: Hiding SmallBanner" );
            HideSmallBanner();
        }
        if (IsAdmobEnabled())
        {
            native_visible = true;
            if (!external_link_present)
            {
                LogMessage( "Show native" );
                AdsInterop.ShowNativeAd( show_home_ads );

            }
        }
    }

    public static void RefreshNativeBanner( AdPosition ad_pos, AdSize ad_size )
    {
        native_pos = ad_pos;
        native_size = ad_size;
        //check if banner in needed position to avoid its unnecessary reload
        if (IsAdmobEnabled())
        {
            AdsInterop.RefreshNativeAd( native_pos, native_size );
        }
    }


    #endregion

    #region Interstitial

    /// <summary>
    /// Full-screen banner cached and ready to show.
    /// </summary>
    public static bool IsInterstitialReady()
    {
        return GetSrcReady( AdEventsListener.EventSource.Interstitial );
    }

    /// <summary>
    /// The time delay after the last interstitial was passed.
    /// </summary>
    public static bool IsInterDelayPassed()
    {
        return Time.time - last_interstitial_time >= interstitial_time;
    }


    public static void ShowInterstitial()
    {
        ShowInterstitial( false, false );
    }

    /// <summary>
    /// Show Interstitial Ads banner.
    /// Return value indicates whether show the ads succeeded.
    /// </summary>
    public static bool ShowInterstitial( bool show_home_ads, bool force_show = false )
    {
        if (IsAdmobEnabled())
        {
            if (force_show || IsInterDelayPassed())
            {
                LogMessage( "Show interstitial show_home_ads=" + show_home_ads );
                if (AdsInterop.ShowInterstitialAd( show_home_ads ))
                {
                    return true;
                }
            }
            else
            {
                LogMessage( "It not time to show interstitial, now=" + Time.time + " last=" + last_interstitial_time );
            }
        }
        AdsInterop.InterstitialClosed();
        return false;
    }

    [Obsolete( "Renamed to ShowInterstitial" )]
    public static void ShowFullscreenBanner( bool show_home_ads = false, bool force_show = false )
    {
        ShowInterstitial( show_home_ads, force_show );
    }

    /// <summary>
    /// Is displayed right now fullscreen banner interstitial.
    /// </summary>
    public static bool IsDisplayedInterstitial()
    {
        return AdsInterop.interstitial_shown;
    }
    #endregion

    #region Rewarded
    /// <summary>
    /// Rewarded video ad cached and ready to show.
    /// </summary>
    public static bool IsRewardedReady()
    {
        return GetSrcReady( AdEventsListener.EventSource.Rewarded );
    }

    /// <summary>
    /// The time delay after the last rewarded video ad was passed.
    /// </summary>
    public static bool IsRewardedDelayPassed()
    {
        return Time.time - last_rewarded_time >= rewarded_time;
    }

    /// <summary>
    /// Show rewarded video ad.
    /// Before calling this method, subscribe to events like <see cref="OnRewardedComplete"/>.
    /// After done or close rewarded, unsubscribe from events.
    /// </summary>
    /// <param name="show_home_ads">Is force show home rewarded ad.</param>
    /// <param name="ignore_delay">Is ignore time delay after the last shown.</param>
    public static void ShowRewardedVideoAd( bool show_home_ads = false, bool ignore_delay = false )
    {
        if (ignore_delay || IsRewardedDelayPassed())
        {
            AdsInterop.ShowRewarded( show_home_ads );
        }
    }

    /// <summary>
    /// Show rewarded video ad. 
    /// One-time use actions without needed to unsubscribe.
    /// Return value indicates whether show the video succeeded.
    /// </summary>
    /// <param name="successful">Call on complete for reward.</param>
    /// <param name="fail">Call on closed and refused the award.</param>
    /// <param name="show_home_ads">Is force show home rewarded ad.</param>
    /// <param name="ignore_delay">Is ignore time delay after the last shown.</param>
    public static bool TryShowRewardedVideoAd( Action successful, Action fail, bool show_home_ads = false, bool ignore_delay = false )
    {
        if (IsRewardedReady() && (ignore_delay || IsRewardedDelayPassed()))
        {
            if (AdsInterop.ShowRewarded( show_home_ads ))
            {
                on_rewarded_successful += successful;
                on_rewarded_fail += fail;
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Is displayed right now rewarded ad video.
    /// </summary>
    public static bool IsDisplayedRewardedVideo()
    {
        return AdsInterop.rewarded_shown;
    }
    #endregion

}
