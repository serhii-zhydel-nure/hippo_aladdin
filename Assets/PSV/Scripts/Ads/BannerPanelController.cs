﻿#if false // Deleted
using UnityEngine;
using PSV;

namespace PSV.ADS
{
    [RequireComponent( typeof( RectTransform ) )]
    public class BannerPanelController : BannerListener
    {


        private RectTransform
            panel;

        private void Awake()
        {
            panel = GetComponent<RectTransform>();
        }


        protected float margin = 0.2f;  //  10px/50px

        public float Margin
        {
            set
            {
                margin = value;
                UpdateBannerPanel();
            }
            get
            {
                return margin;
            }
        }



        override protected void BannerUpdated( Vector2 sz, Vector2 anchor )
        {
            if (panel != null)
            {
                float m = sz.y * margin;
                sz.x += m;
                sz.y += m;
                panel.sizeDelta = sz;

                panel.pivot = anchor;
                panel.anchorMax = anchor;
                panel.anchorMin = anchor;
                panel.anchoredPosition = GetPos( m, anchor );
            }
        }


        private Vector2 GetPos( float delta_size, Vector2 pivot )
        {
            delta_size *= 0.5f;
            return new Vector2(
                GetAxisOffset( delta_size, pivot.x ),
                GetAxisOffset( delta_size, pivot.y )
                );
        }



        private float GetAxisOffset( float offset, float pivot )
        {
            float res = 0;
            if (pivot == 0)
            {
                res -= offset;
            }
            if (pivot == 1)
            {
                res += offset;
            }
            return res;

        }

    }
}
#endif