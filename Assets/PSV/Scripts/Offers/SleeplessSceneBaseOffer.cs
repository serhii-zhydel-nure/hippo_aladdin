﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public abstract class SleeplessSceneBaseOffer : ISceneOffer
    {
        public int priority { get { return int.MaxValue; } }

        public virtual bool IsEmpty()
        {
            return ScenesList().Length == 0 || PSVSettings.settings.never_sleep_screen;
        }

        public virtual OfferReply TryShow( Scenes current_scene, Scenes target_scene )
        {
            if (target_scene != current_scene)
            {
                if (IsSleplessScene( target_scene ))
                    Screen.sleepTimeout = SleepTimeout.NeverSleep;
                else
                    Screen.sleepTimeout = SleepTimeout.SystemSetting;
            }
            return OfferReply.Skipped;
        }


        public bool IsSleplessScene( Scenes scene )
        {
            var sceneList = ScenesList();
            for (int i = 0; i < sceneList.Length; i++)
            {
                if (scene == sceneList[i])
                {
                    return true;
                }
            }
            return false;
        }

        public abstract Scenes[] ScenesList();

        public Scenes OfferScene()
        {
            return Scenes.None;
        }
    }
}