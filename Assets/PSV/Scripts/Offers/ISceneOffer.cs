﻿
namespace PSV
{
    public interface ISceneOffer
    {
        /// <summary>
        /// On Show skip other offers.
        /// </summary>
        OfferReply TryShow( Scenes current_scene, Scenes target_scene );

        /// <summary>
        /// On Empty be removed offer from list.
        /// Check before <see cref="TryShow(Scenes, Scenes)"/>
        /// </summary>
        bool IsEmpty();

        /// <summary>
        /// The higher the priority, the lower the probability of showing.
        /// </summary>
        int priority { get; }

        Scenes[] ScenesList();
        Scenes OfferScene();
    }

    public enum OfferReply : byte
    {
        /// <summary>
        /// Try show next.
        /// If this is last offer then will be called method ClosedInterstitial
        /// </summary>
        Skipped,
        /// <summary>
        /// Stop ask other offers.
        /// This offer is not ads and need call method ClosedInterstitial.
        /// </summary>
        ShownNoAds,
        /// <summary>
        /// Stop ask other offers.
        /// This offer is ads and do not call method ClosedInterstitial.
        /// </summary>
        ShowWithAds
    }
    
}
