﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
using PSV.ADS;

/// <summary>
/// Manager to show offers in scene transitions. Generally it will override target scene.
/// Define here partial methods for your custom offers, your method should 
/// v1
/// - implemented RateMe and PurchaseMe offers
/// - integrated with SceneLoader
/// </summary>

namespace PSV
{
    public static partial class OffersManager
    {
        public static Scenes suspendedScene = Scenes.None;

#if false
        static OffersManager()
        {
            offers = new List<ISceneOffer>( 4 );

            var offerType = typeof( ISceneOffer );
            //var allTypes = AppDomain.CurrentDomain.GetAssemblies();
            var allTypes = offerType.Assembly.GetTypes();
            for (int i = 0; i < allTypes.Length; i++)
            {
                if (!allTypes[i].IsInterface && allTypes[i].IsAssignableFrom( offerType ))
                {
                    AddOffer( ( ISceneOffer )Activator.CreateInstance( allTypes[i] ) );
                }
            }

            //PurchaseOffer purchase = new PurchaseOffer();
            //if (!purchase.IsEmpty())
            //    offers.Add( purchase );
            //RateMeOffer rate_me = new RateMeOffer();
            //if (!rate_me.IsEmpty())
            //    offers.Add( rate_me );
        }
#endif

        public static void AddOffer( ISceneOffer offer )
        {
            if (!offers.Contains( offer ))
            {
                // * Now will be sorted on Try Show
                //int priority = offer.priority;
                //for (int i = 0; i < offers.Count; i++)
                //{
                //    if (priority < offers[i].priority)
                //    {
                //        offers.Insert( i, offer );
                //        return;
                //    }
                //}
                offers.Add( offer );
            }
        }

        public static void RemoveOffer( ISceneOffer offer )
        {
            offers.Remove( offer );
        }

        /// <summary>
        /// Remove all Offers from list equal T
        /// </summary>
        public static void RemoveOffer<T>() where T : ISceneOffer
        {
            for (int i = offers.Count - 1; i >= 0; i--)
            {
                if (offers[i] is T)
                    offers.RemoveAt( i );
            }
        }

        public static T Find<T>() where T : ISceneOffer
        {
            for (int i = 0; i < offers.Count; i++)
            {
                if (offers[i] is T)
                    return ( T )offers[i];
            }
            return default( T );
        }
        
        public static OfferReply ShowOffers( Scenes current_scene, Scenes target_scene )
        {
            if (offers.Count > 0)
            {
                offers.Sort( SortByPriority );
                for (int i = offers.Count - 1; i >= 0; i--)
                {
                    if (offers[i].IsEmpty())
                    {
                        offers.RemoveAt( i );
                    }
                    else
                    {
                        OfferReply reply = offers[i].TryShow( current_scene, target_scene );
                        switch (reply)
                        {
                            case OfferReply.ShowWithAds:
                            suspendedScene = Scenes.None;
                            return reply;
                            case OfferReply.ShownNoAds:
                            suspendedScene = target_scene;
                            return reply;
                        }
                    }
                }
            }
            suspendedScene = Scenes.None;
            return OfferReply.Skipped;
        }

        private static int SortByPriority( ISceneOffer x, ISceneOffer y )
        {
            return x.priority.CompareTo( y.priority );
        }
    }
}
