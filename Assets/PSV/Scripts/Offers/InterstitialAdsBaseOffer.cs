﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public abstract class InterstitialAdsBaseOffer : ISceneOffer
    {
        public int priority { get { return -300; } }

        public bool IsEmpty()
        {
            return false;
        }
        
        public abstract Scenes[] ScenesList();

        public virtual OfferReply TryShow( Scenes current_scene, Scenes target_scene )
        {
            var sceneList = ScenesList();
            for (int i = 0; i < sceneList.Length; i++)
            {
                if (sceneList[i] == current_scene)
                {
                    return OfferReply.Skipped;
                }
            }
            bool isShown = ManagerGoogle.ShowInterstitial( false, false );
            if (isShown)
            {
                OnShowDialog( current_scene, target_scene );
                return OfferReply.ShowWithAds;
            }
            return OfferReply.Skipped;
        }

        protected abstract void OnShowDialog( Scenes current_scene, Scenes target_scene );
        
        public Scenes OfferScene()
        {
            return Scenes.None;
        }
    }
}