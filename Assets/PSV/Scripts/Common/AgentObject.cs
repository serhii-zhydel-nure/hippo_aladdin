﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public abstract class AgentObject<T> : MonoBehaviour where T : AgentObject<T>
    {
        protected static T agent;

        protected void Awake()
        {
            if (agent)
            {
                Object.Destroy( this );
            }
            else
            {
                agent = ( T )this;
            }
        }

        protected static void CheckAgent()
        {
            if (!agent)
            {
                ServiceContainer.AddComponent<T>();
            }
        }

        /// <summary>
        /// Check exist agent Object. 
        /// Try instantiate object from prefab.
        /// Return result exist agent;
        /// </summary>
        /// <param name="prefab_path">Path to prefab in Resource folder.</param>
        protected static bool CheckAgent( string prefab_path )
        {
            if (!agent)
            {
                GameObject prefab = Resources.Load<GameObject>( prefab_path );
                if (prefab)
                {
                    ServiceContainer.Instantiate( prefab );
                }
                else
                {
                    Debug.LogError( typeof( T ).ToString() + ": Prefab not found to path " + prefab_path );
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Check exist agent UI Object. 
        /// Try instantiate object from prefab.
        /// Return result exist agent;
        /// </summary>
        /// <param name="prefab_path">Path to prefab in Resource folder.</param>
        /// <param name="add_beginning">Add object to beginning canvas list.</param>
        protected static bool CheckUIAgent( string prefab_path, bool add_beginning = false )
        {
            if (!agent)
            {
                GameObject prefab = Resources.Load<GameObject>( prefab_path );
                if (prefab)
                {
                    ServiceContainer.InstantiateUI( prefab, add_beginning );
                }
                else
                {
                    Debug.LogError( typeof( T ).ToString() + ": Prefab not found to path " + prefab_path );
                    return false;
                }
            }
            return true;
        }

        public static new void Destroy( Object obj )
        {
            Destroy( obj, 0.0f );
        }
        public static new void Destroy( Object obj, float delay = 0.0f )
        {
            if (obj is GameObject && ReferenceEquals( obj, ServiceContainer.gameObject ))
            {
                Debug.LogError( "It is impossible to destroy a service container!" );
                return;
            }
            Object.Destroy( obj, delay );
        }
    }
}