﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public class CoroutineHandler : AgentObject<CoroutineHandler>
    {
        private static ForThisScene coroutinesOnScene;

        /// <summary>
        /// Start unmanaged coroutine.
        /// </summary>
        public static Coroutine Start( IEnumerator routine )
        {
            CheckAgent();
            return agent.StartCoroutine( routine );
        }

        /// <summary>
        /// Start managed coroutine. Destroy on switch scene.
        /// </summary>
        public static Coroutine StartOnScene( IEnumerator routine )
        {
            if (!coroutinesOnScene)
            {
                GameObject container =  new GameObject( "CoroutineOnScene" );
                coroutinesOnScene = container.AddComponent<ForThisScene>();
            }
            return coroutinesOnScene.StartCoroutine( routine );
        }

        public static void Stop( Coroutine coroutine )
        {
            if (coroutine != null)
            {
                if (agent)
                    agent.StopCoroutine( coroutine );
                if (coroutinesOnScene)
                    coroutinesOnScene.StopCoroutine( coroutine );
            }
        }

        public static void StopAllOnScene()
        {
            if (coroutinesOnScene)
                coroutinesOnScene.StopAllCoroutines();
        }

#if !UNITY_2017_1_OR_NEWER
        private void OnDestroy()
        {
            StopAllCoroutines();
        }
#endif

        public class ForThisScene : MonoBehaviour
        {
            private void OnDestroy()
            {
                coroutinesOnScene = null;
#if !UNITY_2017_1_OR_NEWER
                StopAllCoroutines();
#endif
            }
        }
    }
}