﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace PSV
{
    /// <summary>
    /// A thread-safe class which holds a queue with actions to execute on the next Update() method. It can be used to make calls to the main thread for
    /// things such as UI Manipulation in Unity. It was developed for use in combination with the Firebase Unity plugin, which uses separate threads for event handling
    /// </summary>
    public class UnityMainThreadDispatcher : AgentObject<UnityMainThreadDispatcher>
    {
        private static Queue<Action> _executionQueue = new Queue<Action>( 15 ); // Default new Queue() size 32

        public void Update()
        {
            lock (_executionQueue)
            {
                while (_executionQueue.Count > 0)
                {
                    _executionQueue.Dequeue()();
                }
            }
        }

        /// <summary>
        /// Locks the queue and adds the Action to the queue
        /// </summary>
        /// <param name="action">function that will be executed from the main thread.</param>
        public static void Enqueue( Action action )
        {
            CheckAgent();
            lock (_executionQueue)
            {
                _executionQueue.Enqueue( action );
            }
        }

        public static bool Contains( Action action )
        {
            return _executionQueue.Contains( action );
        }
    }

    public class NextFrameCall
    {
        public static void Add( Action action )
        {
            UnityMainThreadDispatcher.Enqueue( action );
        }

        public static void AddOneTimeCall( Action action )
        {
            if (!UnityMainThreadDispatcher.Contains( action ))
                UnityMainThreadDispatcher.Enqueue( action );
        }

        public static void SavePrefs()
        {
            AddOneTimeCall( PlayerPrefs.Save );
        }
    }
}