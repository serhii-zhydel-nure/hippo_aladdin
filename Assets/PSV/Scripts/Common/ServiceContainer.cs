﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace PSV
{
    public static partial class ServiceContainer
    {
        private static GameObject container;
        private static RectTransform canvas_container;

        public static GameObject gameObject
        {
            get
            {
                CheckContainer();
                return container;
            }
        }

        public static RectTransform canvas
        {
            get
            {
                CheckCanvas();
                return canvas_container;
            }
        }

        public static T AddComponent<T>() where T : MonoBehaviour
        {
            CheckContainer();
            return container.AddComponent<T>();
        }

        public static T AddComponentUI<T>() where T : MonoBehaviour
        {
            CheckCanvas();
            return canvas_container.gameObject.AddComponent<T>();
        }

        public static GameObject Instantiate( GameObject prefab )
        {
            CheckContainer();
#if UNITY_5_4_OR_NEWER
            GameObject result = (GameObject)Object.Instantiate( prefab, container.transform, false );
#else
            GameObject result = (GameObject)Object.Instantiate( prefab );
            Transform instance_t = result.GetComponent<Transform>();
            instance_t.SetParent( container.transform, false );
#endif
            return result;
        }

        public static GameObject InstantiateUI( GameObject prefab, bool add_beginning = false )
        {
            CheckCanvas();
#if UNITY_5_4_OR_NEWER
            GameObject result = (GameObject)Object.Instantiate( prefab, canvas_container, false );
            RectTransform instance_t = result.GetComponent<RectTransform>();
#else
            GameObject result = (GameObject)Object.Instantiate( prefab );
            RectTransform instance_t = result.GetComponent<RectTransform>();
            instance_t.SetParent( canvas_container, false );
#endif
            instance_t.anchoredPosition = prefab.GetComponent<RectTransform>().anchoredPosition;
            if (add_beginning)
            {
                instance_t.SetAsFirstSibling();
            }
            return result;
        }

        private static void CheckContainer()
        {
            if (!container)
            {
                container = new GameObject( "ServiceContainer" );
                Object.DontDestroyOnLoad( container );

                GameObject eventSystem = new GameObject( "EventSystem", typeof( EventSystem ) );
                eventSystem.GetComponent<Transform>().SetParent( container.transform, false );
                var inputModule = eventSystem.AddComponent<StandaloneInputModule>();
                inputModule.forceModuleActive = true;
            }
        }

        private static void CheckCanvas()
        {
            CheckContainer();
            if (!canvas_container)
            {
                GameObject canvas_obj = new GameObject( "ServiceCanvas" );
                canvas_container = canvas_obj.AddComponent<RectTransform>();
                canvas_container.SetParent( container.transform, false );

                Canvas canvas = canvas_obj.AddComponent<Canvas>();
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
                canvas.sortingOrder = System.Int16.MaxValue;

                CanvasScaler canvas_scaler = canvas_obj.AddComponent<CanvasScaler>();
                canvas_scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
                canvas_scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;

                canvas_obj.AddComponent<GraphicRaycaster>();
            }
        }
    }
}