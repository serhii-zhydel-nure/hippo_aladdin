﻿//#define CustomLoadMethod
using UnityEngine;

namespace PSV
{
    /// <summary>
    /// Call static method once on Initialize first scene.
    /// Warrning! For use this on device should select InitScene(Any empty scene) index 0.
    /// </summary>
    public class AwakeStaticAttribute
#if NO_RUNTIME_INITIALIZE || CustomLoadMethod
        : System.Attribute
#else
        : RuntimeInitializeOnLoadMethodAttribute
#endif
    {
#if NO_RUNTIME_INITIALIZE || CustomLoadMethod
        public AwakeStaticAttribute() { }

#if CustomLoadMethod
#if UNITY_EDITOR
        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.BeforeSceneLoad )]
#else
        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.AfterSceneLoad )]
#endif
        private static void StaticAwake()
        {
            CheckCallStaticDelayed();
        }
#endif
#else
        public AwakeStaticAttribute()
            : base( Application.isEditor ? RuntimeInitializeLoadType.BeforeSceneLoad : RuntimeInitializeLoadType.AfterSceneLoad ) { }

#if UNITY_EDITOR
        private static System.Collections.Generic.List<System.Type> doneCalls = new System.Collections.Generic.List<System.Type>();

        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.AfterSceneLoad )]
        private static void StaticAwake()
        {
            NextFrameCall.Add( CheckCallStaticDelayed );
        }
#endif
#endif

        public static void Done( System.Type target )
        {
#if UNITY_EDITOR
#if CustomLoadMethod
            Debug.Log( "Awake " + target.Name );
#else
            doneCalls.Add( target );
#endif
#endif
        }

#if UNITY_EDITOR || CustomLoadMethod
        private static void CheckCallStaticDelayed()
        {
            var types = typeof( AwakeStaticAttribute ).Assembly.GetTypes();
            for (int i = 0; i < types.Length; i++)
            {
                FindAttribute( types[i] );
            }
        }

        private static void FindAttribute( System.Type type )
        {
#if !CustomLoadMethod
            if (doneCalls.Contains( type ))
                return;
#endif
            var methods = type.GetMethods( System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic );
            for (int m = 0; m < methods.Length; m++)
            {
                var attributes = methods[m].GetCustomAttributes( typeof( AwakeStaticAttribute ), true );
                if (attributes.Length > 0)
                {
#if CustomLoadMethod
                    methods[m].Invoke( null, null );
#else
                    Debug.LogError( "Awake Static are skipped in class " + type.Name );
#endif
                    return;
                }
            }
        }
#endif
    }
}