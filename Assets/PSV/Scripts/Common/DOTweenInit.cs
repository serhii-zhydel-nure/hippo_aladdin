﻿using UnityEngine;
using System.Collections;

namespace PSV
{
	public static class DOTweenInit 
	{
        [AwakeStatic]
        public static void InitDefault()
        {
            AwakeStaticAttribute.Done( typeof( DOTweenInit ) );
            DG.Tweening.DOTween.Init( false, true, DG.Tweening.LogBehaviour.Verbose ).SetCapacity( 200, 10 );
        }
	}
}