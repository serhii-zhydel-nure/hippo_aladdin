﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace PSV
{
    public interface ILoadable : IEnumerable, IDisposable
    {
        /// <summary>
        /// Unique load identifie.
        /// </summary>
        string identifier { get; }
        bool isDone { get; }
        /// <summary>
        /// Progress percent value 0.0f - 1.0f
        /// </summary>
        float progress { get; }
        /// <summary>
        /// String progress loaded bytes. 
        /// Can be return string.Empty
        /// </summary>
        string GetProgressBytesString();
    }

    public abstract class LoadableObject : ILoadable, ISceneOffer
    {
        private static List<LoadableObject> async_load_objects = new List<LoadableObject>();

        public string identifier { get; protected set; }
        public bool isDone { get; private set; }

        public abstract float progress { get; }

        protected bool is_prepared;
        protected Coroutine async_load_coroutine;
        protected Scenes[] necessary_for_scenes;
        protected long content_length = 0;

        public LoadableObject( string identifier )
        {
            this.identifier = identifier;
        }
        

        /// <summary>
        /// Add load asset bundle progress to preloading list. List is called on next switch scene.
        /// Load control is passed to the <see cref="SceneLoader"/> handler.
        /// Identifier for handler is bundle_url.
        /// </summary>
        public LoadableObject Preload()
        {
            SceneLoader.AddLoadable( this );
            return this;
        }

        /// <summary>
        /// Async load asset bundle in background.
        /// Load control is passed to the <see cref="BackgroundProcess"/> handler. 
        /// Identifier for handler is bundle_url.
        /// </summary>
        public LoadableObject LoadAsync()
        {
            if (!isDone && !is_prepared)
            {
                for (int i = 0; i < async_load_objects.Count; i++) // Skip duplicates
                {
                    if (async_load_objects[i].identifier == identifier)
                        return this;
                }
                async_load_objects.Add( this );
                async_load_coroutine = CoroutineHandler.Start( GetEnumerator() );
            }
            return this;
        }

        /// <summary>
        /// Return string format (loaded bytes)/(total bytes) [Kb|Mb|Gb].
        /// Return value only after start load else return string.Empty.
        /// </summary>
        public string GetProgressBytesString()
        {
            if (content_length == 0L)
                return string.Empty;
            return BytesToString( ( long )( content_length * progress ), content_length );
        }

        /// <summary>
        /// Get Instructions for start load object resources.
        /// </summary>
        public IEnumerator GetEnumerator()
        {
            if (is_prepared)
            {
                while (!isDone)
                {
                    yield return null;
                }
            }
            else
            {
                isDone = false;
                is_prepared = true;
                yield return GetInstruction();
                isDone = true;

                for (int i = 0; i < async_load_objects.Count; i++)
                {
                    if (async_load_objects[i].identifier == identifier)
                    {
                        async_load_objects.RemoveAt( i );
                        break;
                    }
                }
            }
        }
        protected abstract IEnumerator GetInstruction();

        public void SetAllowLoadAgain()
        {
            is_prepared = false;
        }

        /// <summary>
        /// Abort loadian.
        /// </summary>
        public virtual void Dispose()
        {
            isDone = true;
        }

        /// <summary>
        /// Warranty that the process will be completed before loading any next scene.
        /// </summary>
        public LoadableObject SetNecessaryAll()
        {
            if (!isDone)
            {
                if (this.necessary_for_scenes == null)
                    OffersManager.AddOffer( this );
                this.necessary_for_scenes = new Scenes[0];
            }
            return this;
        }

        /// <summary>
        /// Warranty that the process will be completed before loading the specified scenes.
        /// </summary>
        public LoadableObject SetNecessaryFor( Scenes[] necessary_for_scenes )
        {
            if (!isDone)
            {
                Scenes targetScene = SceneLoader.GetTargetScene();
                if (targetScene != SceneLoader.GetCurrentScene())
                {
                    bool is_load_now = necessary_for_scenes.Length == 0;
                    for (int i = 0; i < necessary_for_scenes.Length; i++)
                    {
                        if (necessary_for_scenes[i] == targetScene)
                        {
                            is_load_now = true;
                            break;
                        }
                    }
                    if (is_load_now)
                    {
                        SceneLoader.AddLoadable( this );
                        if (this.necessary_for_scenes != null)
                            OffersManager.RemoveOffer( this );
                        this.necessary_for_scenes = necessary_for_scenes;
                        return this;
                    }
                }

                if (this.necessary_for_scenes == null)
                {
                    OffersManager.AddOffer( this );
                }
                this.necessary_for_scenes = necessary_for_scenes;
            }
            return this;
        }

        /// <summary>
        /// Remove warranty that the process will be completed before loading the specified scenes.
        /// </summary>
        public LoadableObject ClearNecessaryAll()
        {
            if (!isDone)
            {
                OffersManager.RemoveOffer( this );
                this.necessary_for_scenes = null;
            }
            return this;
        }

        #region Scene offer
        int ISceneOffer.priority { get { return 0; } }

        bool ISceneOffer.IsEmpty()
        {
            return isDone || necessary_for_scenes == null;
        }

        OfferReply ISceneOffer.TryShow( Scenes current_scene, Scenes target_scene )
        {
            if (necessary_for_scenes != null && current_scene != target_scene)
            {
                if (necessary_for_scenes.Length == 0)
                {
                    SceneLoader.AddLoadable( this );
                }
                else
                {
                    for (int i = 0; i < necessary_for_scenes.Length; i++)
                    {
                        if (necessary_for_scenes[i] == target_scene)
                        {
                            SceneLoader.AddLoadable( this );
                            break;
                        }
                    }
                }
            }
            return OfferReply.Skipped; // In any case skipped.
        }

        Scenes[] ISceneOffer.ScenesList()
        {
            return necessary_for_scenes;
        }

        Scenes ISceneOffer.OfferScene()
        {
            return Scenes.None;
        }
        #endregion

        public static void AbortLoad( string identifier )
        {
            if (string.IsNullOrEmpty( identifier ))
                return;
            for (int i = 0; i < async_load_objects.Count; i++)
            {
                if (async_load_objects[i].identifier == identifier)
                {
                    async_load_objects[i].Dispose();
                    CoroutineHandler.Stop( async_load_objects[i].async_load_coroutine );
                    async_load_objects[i].async_load_coroutine = null;
                    async_load_objects.RemoveAt( i );
                    break;
                }
            }
            SceneLoader.RemoveLoadable( identifier );
        }

        public static string BytesToString( long current, long total )
        {
            int mag = Mathf.Max( 0, Mathf.FloorToInt( Mathf.Log( total, 1024 ) ) );
            double pow = Math.Pow( 1024, mag );
            double currentSize = Math.Round( current / pow, 2 );
            double totalSize = Math.Round( total / pow, 2 );
            string magStr;
            switch (mag)
            {
                default:
                magStr = "b";
                break;
                case 1:
                magStr = "Kb";
                break;
                case 2:
                magStr = "Mb";
                break;
                case 3:
                magStr = "Gb";
                break;
            }
            return string.Format( "{0}/{1} {2}", currentSize, totalSize, magStr );
        }
        
        /// <summary>
        /// Create enumerator process with LoadableObject logic.
        /// </summary>
        public class LoadableEnumerator : LoadableObject
        {
            private IEnumerator instruction;

            public LoadableEnumerator( IEnumerator instruction, string identifier ) : base( identifier )
            {
                this.instruction = instruction;
            }

            public override float progress
            {
                get
                {
                    return Mathf.Repeat( Time.time, 1.0f );
                }
            }

            protected override IEnumerator GetInstruction()
            {
                return instruction;
            }
        }
    }
}