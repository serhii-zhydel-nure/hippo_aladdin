﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV
{
	public class ApplicationEventAgent : AgentObject<ApplicationEventAgent> 
	{
        private static Action<bool> applicationPause;
        private static Action<bool> applicationFocus;
        private static Action applicationQuit;
        

        public static event Action<bool> OnPause
        {
            add
            {
                CheckAgent();
                applicationPause += value;
            }
            remove
            {
                applicationPause -= value;
            }
        }

        public static event Action<bool> OnFocus
        {
            add
            {
                CheckAgent();
                applicationFocus += value;
            }
            remove
            {
                applicationFocus -= value;
            }
        }

        public static event Action OnQuit
        {
            add
            {
                CheckAgent();
                applicationQuit += value;
            }
            remove
            {
                applicationQuit -= value;
            }
        }

        private void OnApplicationPause( bool pause )
        {
            if (applicationPause != null)
                applicationPause( pause );
        }

        private void OnApplicationFocus( bool focus )
        {
            if (applicationFocus != null)
                applicationFocus( focus );
        }

        private void OnApplicationQuit()
        {
            AnalyticsManager.LogEvent( AnalyticsEvents.CloseApplication );
            if (applicationQuit != null)
                applicationQuit();
        }
    }
}