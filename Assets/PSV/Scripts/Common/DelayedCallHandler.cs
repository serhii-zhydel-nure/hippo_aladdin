﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public static class DelayedCallHandler
    {
        /// <summary>
        /// Delayed Invoke for <see cref="Time.unscaledTime"/>
        /// </summary>
        public static Coroutine DelayedCall( System.Action callback, float t, bool undestroyable = true )
        {
            if (undestroyable)
                return CoroutineHandler.Start( WaitAndExecute( callback, t ) );
            else
                return CoroutineHandler.StartOnScene( WaitAndExecute( callback, t ) );
        }

        private static IEnumerator WaitAndExecute( System.Action callback, float t )
        {
            t += Time.unscaledTime;
            while (Time.unscaledTime < t)
            {
                yield return null;
            }
            callback();
        }

        /// <summary>
        /// Delayed Invoke for <see cref="Time.time"/>
        /// </summary>
        public static Coroutine DelayedCallScaled( System.Action callback, float t, bool undestroyable = true )
        {
            if (undestroyable)
                return CoroutineHandler.Start( WaitAndExecuteScaled( callback, t ) );
            else
                return CoroutineHandler.StartOnScene( WaitAndExecuteScaled( callback, t ) );
        }

        private static IEnumerator WaitAndExecuteScaled( System.Action callback, float t )
        {
            yield return new WaitForSeconds( t );
            callback();
        }


        /// <summary>
        /// Abort delayed call. Redirect to <see cref="CoroutineHandler.Stop(Coroutine)"/>
        /// </summary>
        public static void StopDelayedCall( Coroutine call )
        {
            CoroutineHandler.Stop( call );
        }

        /// <summary>
        /// Compact version of the methods <see cref="DelayedCall(System.Action, float)"/> and <see cref="DelayedCallScaled(System.Action, float)"/>
        /// </summary>
        /// <param name="delay">Delay seconds.</param>
        /// <param name="ignore_time_scale">For true select Time.unstaledTime</param>
        /// <param name="undestroyable">Is dont destroy on load</param>
        public static Coroutine Invoke( this object empty, System.Action callback, float delay, bool ignore_time_scale = false, bool undestroyable = false )
        {
            IEnumerator waitAction;
            if (ignore_time_scale)
                waitAction = WaitAndExecute( callback, delay );
            else
                waitAction = WaitAndExecuteScaled( callback, delay );

            if (undestroyable)
                return CoroutineHandler.Start( waitAction );
            else
                return CoroutineHandler.StartOnScene( waitAction );
        }
    }
}
