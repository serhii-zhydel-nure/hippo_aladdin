﻿/* 
 * Dependence plugins: 
 * com.psvplugins.notification:1.2
 * com.android.support:support-v4:26.1.0+
 * 
 * Build SDK Tools Version '27.0.3+'
 * */

using System;
using UnityEngine;

namespace PSV
{
    public static class NativeNotifications
    {
        private const string native_android_class_name = "com.psvplugins.notification.Notification";
        private static bool valid_class = true;

        [AwakeStatic]
        public static void StaticAwake()
        {
            AwakeStaticAttribute.Done( typeof( NativeNotifications ) );
            //SetBlockInRuntime( true );
            NextFrameCall.Add( RestorePausedNotifications );
        }

        /// <summary>
        /// Initialize platform notification.
        /// Time in MILLISECONDS: 1 sec = 1000 millisec
        /// </summary>
        /// <param name="title">Set the first line of text in the platform notification template.</param>
        /// <param name="message">Set the second line of text in the platform notification template.</param>
        /// <param name="delay">Delay from the current time in MILLISECONDS</param>
        /// <param name="repeatRate">Repeat delay from first show in MILISECONDS. Non-repetitive for 0 value. The minimum repetition rate is 60 sec.</param>
        /// <param name="intentInfo">Intent action on open content notification. You can get the current action with GetLaunchIntent()</param>
        /// <param name="sound">Is need play sound.</param>
        /// <param name="vibrate">Is need enable vibration.</param>
        /// <param name="url">For url = Empty - open current application. You can specify any link.</param>
        /// <param name="styleType">Set notification style by constants DEFAULT_STYLE = 0, WITH_BACKGROUND_STYLE = 1, WITH_HIPPO_BG_STYLE = 2</param>
        /// <param name="backgroundName">Resource background image name for notification. Image must be in Plugins/Android/res/drawable/
        /// For default(Empty string) background get from native storage.</param>
        [Obsolete( "Better use NativeNOtifications.Builder() intead" )]
        public static void Notify( string title, string message, long delay, long repeatRate = 0,
            string intentInfo = "", bool sound = true, bool vibrate = true, string url = "", int styleType = 0, string backgroundName = "" )
        {
            new Builder( title, message )
                .DelayMillis( delay )
                .RepeatMillis( repeatRate )
                .OpenIntent( intentInfo )
                .Sound( sound )
                .OpenUrl( url )
                .StyleType( styleType )
                .Background( backgroundName )
                .Build();
        }


        /// <summary>
        /// Cancel all notifications with equal message.
        /// </summary>
        public static void CancelByMessage( string message )
        {
            CallNative( "cancelNotification", message );
        }

        /// <summary>
        /// Cancel all notifications with equal title.
        /// </summary>
        public static void CancelByTitle( string title )
        {
            CallNative( "cancelNotificationByTitle", title );
        }

        /// <summary>
        /// Notifications can be stopped by the system if there is a deficiency of RAM, so they need to be restore.
        /// Restored automatically on start application.
        /// </summary>
        public static void RestorePausedNotifications()
        {
            CallNative( "restorePausedNotifications" );
        }

        /// <summary>
        /// Return launch intent string or Null.
        /// </summary>
        public static string GetLaunchIntent()
        {
            return CallNative<string>( "getLastIntentContent" );
        }

        /// <summary>
        /// Block the notification of this application during the runtime.
        /// </summary>
        public static void SetBlockInRuntime( bool isBlock = true )
        {
            CallNative( "setBlockShow", Convert.ToByte( isBlock ) );
        }

        public static void CallNative( string nameMethod, params object[] args )
        {
            if (!valid_class)
                return;
            if (Application.platform == RuntimePlatform.Android)
            {
                try
                {
                    using (AndroidJavaClass native_android_class = new AndroidJavaClass( native_android_class_name ))
                    {
                        if (native_android_class_name != null && native_android_class.GetRawClass() != IntPtr.Zero)
                            native_android_class.CallStatic( nameMethod, args );
                        else
                            valid_class = false;
                    }
                }
                catch
                {
                    Debug.LogError( "NativeNotifications: " + native_android_class_name + " class not found!" );
                    valid_class = false;
                }
            }
        }

        public static ReturnType CallNative<ReturnType>( string nameMethod, params object[] args )
        {
            if (!valid_class)
                return default( ReturnType );
            if (Application.platform == RuntimePlatform.Android)
            {
                try
                {
                    using (AndroidJavaClass native_android_class = new AndroidJavaClass( native_android_class_name ))
                        if (native_android_class_name != null && native_android_class.GetRawClass() != IntPtr.Zero)
                            return native_android_class.CallStatic<ReturnType>( nameMethod, args );
                }
                catch
                {
                    Debug.LogError( "NativeNotifications: " + native_android_class_name + " class not found!" );
                }
            }
            valid_class = false;
            return default( ReturnType );
        }

        public class Builder
        {
            #region Fields
            private string title;
            private string message;
            private long targetTime;
            private long repeatRate = 0;
            private string intentInfo = string.Empty;
            private bool sound = true;
            private bool visibleIcon = true;
            private string url = string.Empty;
            private int style = 0;
            private string backgroundName = string.Empty;
            private string channelName = string.Empty;
            #endregion

            public Builder( string title, string message )
            {
                this.title = title;
                this.message = message;
            }

            public void Build()
            {
                if (targetTime == 0)
                {
                    Debug.LogError( "NativeNotifications: Unable to build a notification without target time." );
                    return;
                }
                if (!string.IsNullOrEmpty( channelName ))
                    CallNative( "setNotifyChannelName", channelName );
                CallNative( "initNotificationToTime", title, message, targetTime, repeatRate,
                    intentInfo, Convert.ToByte( sound ), Convert.ToByte( visibleIcon ), url, style, backgroundName );
            }

            #region Parrams
            /// <summary>
            /// Set the first line of text in the platform notification template.
            /// </summary>
            public Builder Title( string title ) { this.title = title; return this; }

            /// <summary>
            /// Set the second line of text in the platform notification template.
            /// </summary>
            public Builder Message( string message ) { this.message = message; return this; }

            /// <summary>
            /// Delay from the current time in MILLISECONDS
            /// </summary>
            public Builder DelayMillis( long delay )
            {
                DateTime Jan1st1970 = new DateTime( 1970, 1, 1, 0, 0, 0, DateTimeKind.Utc );
                targetTime = ( long )( DateTime.UtcNow - Jan1st1970 ).TotalMilliseconds + delay;
                return this;
            }

            /// <summary>
            /// Delay from the current time in Seconds
            /// </summary>
            public Builder DelaySeconds( long delay ) { return DelayMillis( delay * 1000L ); }

            /// <summary>
            /// Set target time for show notification.
            /// </summary>
            /// <param name="time">Recomended use UTC format.</param>
            public Builder TargetTime( DateTime time )
            {
                time = time.ToUniversalTime();
                DateTime Jan1st1970 = new DateTime( 1970, 1, 1, 0, 0, 0, DateTimeKind.Utc );
                targetTime = ( long )( time - Jan1st1970 ).TotalMilliseconds;
                return this;
            }

            /// <summary>
            /// Repeat delay from first show in MILISECONDS. Non-repetitive for 0 value. The minimum repetition rate is 60 sec.
            /// </summary>
            public Builder RepeatMillis( long repeatRate ) { this.repeatRate = repeatRate; return this; }

            /// <summary>
            /// Repeat delay from first show in MILISECONDS. Non-repetitive for 0 value. The minimum repetition rate is 60 sec.
            /// </summary>
            public Builder RepeatSeconds( long repeatRate ) { return RepeatMillis( repeatRate * 1000L ); }

            /// <summary>
            /// Intent action on open content notification. You can get the current action with GetLaunchIntent()
            /// </summary>
            public Builder OpenIntent( string intentInfo ) { this.intentInfo = intentInfo; return this; }

            /// <summary>
            /// Is need play sound and vibration.
            /// </summary>
            public Builder Sound( bool enabled ) { this.sound = enabled; return this; }

            /// <summary>
            /// Is need Visible application icon.
            /// </summary>
            public Builder VisibleIcon( bool isVisible ) { this.visibleIcon = isVisible; return this; }

            /// <summary>
            /// For url = Empty - open current application. 
            /// You can specify any link.
            /// </summary>
            public Builder OpenUrl( string url ) { this.url = url; return this; }

            /// <summary>
            /// Set notification style:
            /// DEFAULT_STYLE = 0;
            /// WITH_DEFAULT_BG_STYLE = 1;
            /// WITH_HIPPO_BG_STYLE = 2;
            /// </summary>
            public Builder StyleType( int type ) { this.style = type; return this; }

            /// <summary>
            /// Resource background image name for notification. 
            /// Image must be in Plugins/Android/res/drawable/
            /// For default(Empty string) background get from native storage.
            /// </summary>
            public Builder Background( string resName ) { this.backgroundName = resName; return this; }

            /// <summary>
            /// Change the name of the notification group. Default: Kids Games
            /// </summary>
            public Builder SetChannelName( string channel )
            {
                this.channelName = channel;
                return this;
            }
        }
        #endregion
    }
}