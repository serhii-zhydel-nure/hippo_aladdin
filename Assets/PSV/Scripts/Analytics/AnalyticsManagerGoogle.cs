﻿
namespace PSV
{


	public static partial class AnalyticsManager
	{

        static partial void LogGoogleAnalyticsEvent(AnalyticsEvents _event, string _message)
        {
            string
                titleCat = null,
                titleAction = null,
                titleLabel = "";
            int value = 0;

            switch (_event)
            {
                case AnalyticsEvents.LogScreen:
                    GoogleAnalytics.LogScreen(_message);
                    return;
                case AnalyticsEvents.BannerClicked:
                case AnalyticsEvents.InterstitialClicked:
                case AnalyticsEvents.NeatPlugError:
                case AnalyticsEvents.HomeAdsError:
                    return;
                case AnalyticsEvents.Custom:
                default:
                    titleCat = _event.ToString();
                    titleAction = _message;
                    break;
                case AnalyticsEvents.CloseApplication:
                case AnalyticsEvents.StartApplication:
                    titleCat = _event.ToString();
                    titleAction = _event.ToString();
                    break;
                case AnalyticsEvents.OpenPub:
                    titleCat = "Promo";
                    titleAction = "OpenURL " + UnityEngine.Application.platform.ToString() + ": MoreGames";
                    break;
                case AnalyticsEvents.OpenPromo:
                    titleCat = "Promo";
                    titleAction = "OpenURL " + UnityEngine.Application.platform.ToString() + ": " + _message;
                    break;
                case AnalyticsEvents.LevelComplete:
                    titleCat = "LevelComplete";
                    titleAction = _message;
                    break;
            }

            if (!string.IsNullOrEmpty(titleCat) && !string.IsNullOrEmpty(titleAction))
            {
                GoogleAnalytics.LogEvent(titleCat, titleAction, titleLabel, value);

            }
            else
            {
                LogMessage( "LogGoogleAnalyticsEvent : not enough args", true );
            }
        }


    }
}
