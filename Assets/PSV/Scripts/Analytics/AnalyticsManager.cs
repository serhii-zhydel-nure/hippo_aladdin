﻿
/// <summary>
/// v2
/// - Divided class into separate files, each for one analytics provider. They can be easily removed from project without errors.
/// 
/// </summary>

namespace PSV
{
	public static partial class AnalyticsManager
	{
		private const int event_name_limit = 40;

		public static void LogEvent (AnalyticsEvents _event, string _message = "")
		{
			if (_event == AnalyticsEvents.LogScreen)
			{
				_message += GetGameMode ( );
			}
			LogMessage ( "LogEvent ( " + _event + ", " + _message + " )" );

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_WP8_1
			LogGoogleAnalyticsEvent ( _event, _message );
			LogUnityEvent ( _event, _message );
#endif
		}


        static partial void LogGoogleAnalyticsEvent(AnalyticsEvents _event, string _message);


        static partial void LogUnityEvent(AnalyticsEvents _event, string _message);


		public static string GetGameMode ()
		{
			string res = "";
			//put here processing of current selected game_mode for scenes that support it
			int current_game_mode = 0; //{2+, 5+, 0}


			if (current_game_mode > 0)
			{
				res = "_" + current_game_mode;
			}
			return res;
		}

		private static void LogMessage (string message, bool error = false)
		{
			message = "AnalyticsManager :: " + message;
			if (error) 
			{
				UnityEngine.Debug.LogError ( message );
			}
			else if (PSVSettings.settings.analytics_manager_debug)
			{
				UnityEngine.Debug.Log ( message );
			}
		}

	}
}
