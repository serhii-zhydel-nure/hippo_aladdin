﻿#if UNITY_ANALYTICS
#define USE_UNITY_ANALYTICS
#endif

using System.Collections.Generic;

#if USE_UNITY_ANALYTICS
using UnityEngine.Analytics;
#endif


namespace PSV
{


	public static partial class AnalyticsManager
	{


        static partial void LogUnityEvent(AnalyticsEvents _event, string _message)
        {
#if USE_UNITY_ANALYTICS
			Dictionary<string, object> parameters = new Dictionary<string, object> ( );
			string eventName = null;
			switch (_event)
			{
				case AnalyticsEvents.LogScreen:
					eventName = _event.ToString ( );
					parameters.Add ( "screen", _message );
					break;
                case AnalyticsEvents.BannerClicked:
                case AnalyticsEvents.InterstitialClicked:
                case AnalyticsEvents.NeatPlugError:
                case AnalyticsEvents.HomeAdsError:
                    return;
                case AnalyticsEvents.Custom:
                default:
					eventName = _event.ToString ( );
					parameters.Add ( "message", _message );
					break;
				case AnalyticsEvents.CloseApplication:
				case AnalyticsEvents.StartApplication:
					eventName = _event.ToString ( );
					break;
				case AnalyticsEvents.OpenPub:
					eventName = "Promo";
					parameters.Add ( "MoreGames " + UnityEngine.Application.platform.ToString(), _message );
					break;
				case AnalyticsEvents.OpenPromo:
					eventName = "Promo";
					parameters.Add ( "OpenURL " + UnityEngine.Application.platform.ToString(), _message );
					break;
				case AnalyticsEvents.LevelComplete:
					eventName = _event.ToString ( );
					string [] p_arr = _message.Split ( ';' );
					for (int i = 0; i < p_arr.Length; i++)
					{
						string item = p_arr [i];
						if (!string.IsNullOrEmpty ( item ))
						{
							int separator = item.IndexOf ( ':' );
							if (separator > 0 && separator < item.Length - 1)
							{
								string p_name = item.Substring ( 0, separator );
								string p_value = item.Substring ( separator + 1 );
								object val;
								float float_val = 0;
								int int_val = 0;
								if (float.TryParse ( p_value, out float_val ))
								{
									val = float_val;
									LogMessage ( eventName + " param " + p_name + " is float" );
								}
								else
								{
									if (int.TryParse ( p_value, out int_val ))
									{
										val = int_val;
										LogMessage ( eventName + " param " + p_name + " is int" );
									}
									else
									{
										val = p_value;
										LogMessage ( eventName + " param " + p_name + " is string" );
									}
								}
								parameters.Add ( p_name, val );
							}
						}
					}
					break;
			}

			if (!string.IsNullOrEmpty ( eventName ))
			{
				Analytics.CustomEvent ( eventName, parameters );
			}
#endif
        }


    }
}
