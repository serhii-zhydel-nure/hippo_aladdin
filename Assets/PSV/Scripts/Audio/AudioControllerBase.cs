﻿using UnityEngine;
using System.Collections.Generic;


namespace PSV
{
    using Audio;

    /// <summary>
    /// A class to play sounds and sound sequences. Implements dividing sounds into groups (channels).
    /// Depends on DelayedCallHandler, DoTween
    /// </summary>
    /// 
    /// ChangeLog:
    /// - refactored
    /// - created new class AudioStream
    /// - made it possible to divide streams into groups and operate them separately
    /// - implemented fluent syntax for sound playback (AudioHandler)
    /// - fixed bugs in intervals count for sequence of sounds (last sound length is taken as last array item)
    /// - implemented safe interfase for AudioStream - PlayingAudio.


    public static partial class AudioController
	{
        
		//stream containers for groups
        private static AudioStreamContainer[] groups = null;

		//for pause insert while playing audio by name in a sequence
		public const string
			PAUSE = "||";
        
		//Defines whether you'll see verbose logging or error messages only
		public static bool debug_mode = false;


		/// <summary>
		/// Manages console messages for current class
		/// </summary>
		/// <param name="msg"></param>
		/// <param name="error"></param>
		private static void LogMessage( string msg, bool error = false )
		{
			msg = "AudioController: " + msg;
			if (error)
			{
				Debug.LogError( msg );
			}
			else if (debug_mode)
			{
				Debug.Log( msg );
			}
		}


        #region Basic Logic

        [AwakeStatic]
        public static void AwakeStatic()
        {
            AwakeStaticAttribute.Done( typeof( AudioController ) );
            Pause( false );

            System.Array streamTypes = System.Enum.GetValues( typeof( StreamGroup ) );
            groups = new AudioStreamContainer[streamTypes.Length];
            for (int i = 0; i < streamTypes.Length; i++)
            {
                StreamGroup currGroup = ( StreamGroup )streamTypes.GetValue( i );

                groups[i] = new AudioStreamContainer( currGroup );
            }
        }

        /// <summary>
        /// Creates AudioStream object to play sound using your own AudioSource.
        /// </summary>
        /// <param name="src">Your AudioSource that is created on the scene</param>
        /// <returns></returns>
		public static AudioStream GetCustomStream( AudioSource src)
		{
			return new AudioStream( src );
		}

        #endregion


        #region Groups management

        /// <summary>
        /// Will change main volume.
        /// Redirect to <see cref="AudioListener.volume"/>
        /// </summary>
        /// <param name="volume">This will override main volume</param>
        public static void SetVolume( float volume )
		{
			AudioListener.volume = volume;
		}


		/// <summary>
		/// Will change volume on own and user sources, not listed in music_like_streams list.
		/// Is deprecated. sounds have their own overrides per each group. Only changing background sounds volume makes sense (like music or ambient).
		/// </summary>
		/// <param name="volume">This will override main volume</param>
		public static void SetSoundsVolume( float volume )
		{
            for (int i = 0; i < groups.Length; i++)
            {
                if (!Common.IsMusicLikeGroup( groups[i].stream_type ))
                {
                    groups[i].Volume = volume;
                }
            }
        }


		/// <summary>
		/// Will change volume on own and user sources, listed in music_like_streams list.
		/// </summary>
		/// <param name="volume">This will override volume from GameSettings</param>
		public static void SetMusicVolume( float volume )
		{
            for (int i = 0; i < groups.Length; i++)
            {
                if (Common.IsMusicLikeGroup( groups[i].stream_type ))
                {
                    groups[i].Volume = volume;
                }
            }
        }


        /// <summary>
        /// Will change own and user sources volume (individual group values are counted. Set of groups will depend on is_music param (music_like_streams list defines whether group will be managed as music or as sound)
        /// </summary>
        /// <param name="volume">This will override main volume</param>
        /// <param name="is_music">tells what groups will be changed (See Common.music_like_streams)</param>
        private static void SetStreamsVolume( float volume, bool is_music )
		{
			for (int i = 0; i < groups.Length; i++)
			{
                if (is_music == Common.IsMusicLikeGroup( groups[i].stream_type ))
                {
                    groups[i].Volume = volume;
                }
			}
		}


		/// <summary>
		/// Will change volume override for streams of the group. It will immediately apply to all streams from that group.
		/// </summary>
		/// <param name="group">A group of streams to change volume</param>
		/// <param name="volume"></param>
		public static void SetStreamsVolume( StreamGroup group, float volume )
		{
            AudioStreamContainer container = GetGroup(group);
            container.Volume = volume;
		}

        /// <summary>
        /// Will mute/unmute streams in the group.
        /// Muting background sounds (music or ambient) without muting other sounds is not allowed.
        /// </summary>
        /// <param name="group">a group of streams to mute</param>
        /// <param name="param">true - mute, false - unmute</param>
        /// <param name="save_settings">Is Save value to PlayerPrefs</param>
        public static void MuteStreams( StreamGroup group, bool param, bool save_settings = false )
		{
			AudioStreamContainer container = GetGroup( group );
			container.Mute( param, save_settings );
		}

        /// <summary>
        /// Will mute/unmute all streams.
        /// </summary>
        /// <param name="param">true - mute, false - unmute</param>
        /// <param name="save_settings">Is Save value to PlayerPrefs</param>
        public static void MuteAllStreams( bool param, bool save_settings = false )
        {
			for (int i = 0; i < groups.Length; i++)
			{
				groups[i].Mute( param, save_settings );
			}
		}

        /// <summary>
        /// Will unmute/mute all streams. Redirect to inverse <see cref="MuteAllStreams(bool)"/>
        /// </summary>
        /// <param name="param">true - unmute, false - mute</param>
        /// <param name="save_settings">Is Save value to PlayerPrefs</param>
        public static void EnableSounds( bool param, bool save_settings = false )
        {
            MuteAllStreams( !param, save_settings );
        }

        /// <summary>
        /// Pauses playback of all streams that does not ignore AudioListener's settings.
        /// Redirect to <see cref="AudioListener.pause"/>
        /// </summary>
        /// <param name="param">Pause/Play param</param>
        public static void Pause( bool param )
		{
			AudioListener.pause = param;
		}

		/// <summary>
		/// Stops streams in all available groups that are playing the sound with name snd_name.  Returns count of streams that were playing that sound
		/// </summary>
		/// <param name="snd_name">AudioCLip name</param>
		/// <returns></returns>
		public static int StopSound( string snd_name )
		{
			int res = 0;

			for (int i = 0; i < groups.Length; i++)
			{
				res += groups[i].StopSound( snd_name );
			}
			return res;
		}
        
		/// <summary>
		/// Stops streams in all available groups that are playing the sound with name snd_name.  Returns count of streams that were playing that sound
		/// </summary>
		/// <param name="snd_name">AudioCLip name</param>
		/// <param name="group">StreamGroup the clip is related to</param>
		/// <returns></returns>
		public static int StopSound( string snd_name, StreamGroup group )
		{
			AudioStreamContainer container = GetGroup( group );
			return container.StopSound( snd_name );
		}


		/// <summary>
		/// Stops all streams.
		/// </summary>
		/// <param name="leave_music">False - all streams will be stopped, True - only streams not related to music will stop</param>
		public static void Release( bool leave_music = false )
		{
            for (int i = 0; i < groups.Length; i++)
            {
                if (!(leave_music && Common.IsMusicLikeGroup( groups[i].stream_type)))
                {
                    groups[i].Release();
                }
            }
		}

		/// <summary>
		/// Stops streams except those, who are in do_not_release container
		/// </summary>
		/// <param name="do_not_release">List here stream groups that should continue playback</param>
		public static void Release( List<StreamGroup> do_not_release )
		{
            for (int i = 0; i < groups.Length; i++)
			{
				if (do_not_release == null || !do_not_release.Contains( groups[i].stream_type ))
				{
                    groups[i].Release();
				}
			}
		}

        /// <summary>
		/// Stops all streams for selected streams group.
		/// </summary>
        /// <param name="group">Released streams group</param>
        public static void Release( StreamGroup group )
        {
            GetGroup( group ).Release();
        }


		#endregion


		#region Getters

		/// <summary>
		/// Returns an AudioStream pool, assigned to the group.
		/// </summary>
		/// <param name="group">StreamGroup to select</param>
		public static AudioStreamContainer GetGroup( StreamGroup group )
		{
            return groups[( int )group];
        }



		/// <summary>
		/// Will return AudioClip according to its name and current language
        /// Redirect to <see cref="LocalizationManager.GetSound(string)"/>
		/// </summary>
		private static AudioClip GetSound( string snd_name )
		{
			return LocalizationManager.GetSound( snd_name );
		}


		/// <summary>
		/// Returns volume for the group multiplied by volume_override.
		/// </summary>
		/// <param name="group">StreamGroup to select</param>
		/// <returns>Volume level</returns>
		public static float GetGroupVolume( StreamGroup group )
		{
            AudioStreamContainer container = GetGroup( group );
            return container.Volume;
		}

		/// <summary>
		/// Tells if group not allowed to use multi-stream mode
		/// </summary>
		/// <param name="group"></param>
		/// <returns>Group mode</returns>
		public static bool GetGroupSoloMode( StreamGroup group )
		{
            AudioStreamContainer container = GetGroup( group );
            return container.Solo;

		}
        
        /// <summary>
        /// Will tell true if at least one stream plays the sound
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <returns>Whether at least one stream in any group plays the sound</returns>
        public static bool IsSoundPlaying( string snd_name )
		{
			for (int i = 0; i < groups.Length; i++)
			{
				if ( groups[i].IsSoundPlaying( snd_name ))
				{
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Will tell true if at least one stream plays the sound
		/// </summary>
		/// <param name="snd_name">AudioClip name</param>
		/// <param name="group">StreamGroup the clip is related to</param>
		/// <returns>Whether at least one stream in group plays the sound</returns>
		public static bool IsSoundPlaying( string snd_name, StreamGroup group )
		{
			AudioStreamContainer container = GetGroup( group );
			return container.IsSoundPlaying( snd_name );
		}

		/// <summary>
		/// Tells if AudioListener is paused
        /// Redirect to <see cref="AudioListener.pause"/>
		/// </summary>
		public static bool IsPaused()
		{
			return AudioListener.pause;
		}

		#endregion

	}

}
