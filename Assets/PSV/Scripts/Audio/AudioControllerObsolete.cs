﻿using UnityEngine;
using System;

namespace PSV
{
    using Audio;
    using UnityEngine.Audio;

    public static partial class AudioController
    {
        /* 
         * Obsolete methods can be removed.
         */

        #region Base
        /// <summary>
        /// Will change volume override for streams of the group. It will immediately apply to all streams from that group.
        /// Redirect to <see cref="SetStreamsVolume(StreamGroup, float)"/>
        /// </summary>
        /// <param name="volume"></param>
        /// <param name="group">A group of streams to change volume</param>
        [Obsolete]
        public static void SetStreamsVolume( float volume, StreamGroup group )
        {
            SetStreamsVolume( group, volume );

        }
        
        /// <summary>
        /// Will unmute/mute sources, listed in music_like_streams list.
        /// Muting background sounds (music or ambient) without muting other sounds is not allowed.
        /// </summary>
        /// <param name="param">true - unmute, false - mute</param>
        [Obsolete( "Is deprecated. Use MuteStreams for specific group." )]
        public static void EnableMusic( bool param )
        {
            GetGroup( StreamGroup.MUSIC ).Mute( !param, true );
        }

        /// <summary>
        /// Returns volume for the group multiplied by volume_override.
        /// </summary>
        /// <param name="group">StreamGroup to select</param>
        /// <returns>Volume level</returns>
        [Obsolete( "Renamed to GetGroupVolume" )]
        public static float GetStreamVolume( StreamGroup group )
        {
            return GetGroupVolume( group );
        }

        #endregion

        #region Methods to play sound as AudioClip

        /// <summary>
        /// Plays sound using own stream from given group. Stores used AudioStream in stream variable. Stream may be reused.
        /// </summary>
        /// <param name="clip">AudioClip</param>
        /// <param name="stream">AudioStream from a pool that will play the clip.</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip clip, out AudioStream stream, StreamGroup group, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clip )
                            .Group( group )
                            .Volume( volume_override )
                            .Loop( loop )
                            .Pitch( pitch )
                            .OnComplete( on_complete )
                            .Start()
                            .GetStream( out stream );
        }


        /// <summary>
        /// Plays sound on custom stream with settings for the group.
        /// </summary>
        /// <param name="clip">AudioClip</param>
        /// <param name="custom_stream">Custom AudioStream that will play the clip. Won't be reused</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip clip, AudioStream custom_stream, StreamGroup group, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clip )
                        .Stream( custom_stream )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start();
        }


        /// <summary>
        /// Plays sound on custom AudioSource. Use PlaySound with custom AudioStream to be able to reuse it in future. Having stream itself will let you manage it manually.
        /// </summary>
        /// <param name="clip">AudioClip to play</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip clip, AudioSource custom_src, StreamGroup group, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clip )
                        .Source( custom_src )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start();
        }


        /// <summary>
        /// Plays sound on custom AudioSource
        /// </summary>
        /// <param name="clip">AudioClip</param>
        /// <param name="custom_src">Custom AudioSource to play sound with</param>
        /// <param name="custom_stream">AudioStream from a pool that will play the clip.</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip clip, AudioSource custom_src, out AudioStream custom_stream, StreamGroup group, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clip )
                             .Group( group )
                             .Source( custom_src )
                             .Volume( volume_override )
                             .Loop( loop )
                             .Pitch( pitch )
                             .OnComplete( on_complete )
                             .Start()
                             .GetStream( out custom_stream );
        }

        #endregion


        #region Methods to play sound represented as its name

        /// <summary>
        /// Plays sound using own stream from given group. Stores used AudioStream in stream variable. Stream may be reused.
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <param name="stream">AudioStream from a pool that will play the clip.</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string snd_name, out AudioStream stream, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( snd_name )
                            .Group( group )
                            .Volume( volume_override )
                            .Loop( loop )
                            .Pitch( pitch )
                            .OnComplete( on_complete )
                            .Start()
                            .GetStream( out stream );
        }


        /// <summary>
        /// Plays sound on custom stream with settings for the group.
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <param name="custom_stream">Custom AudioStream that will play the clip. Won't be reused</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string snd_name, AudioStream custom_stream, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( snd_name )
                        .Stream( custom_stream )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start();
        }


        /// <summary>
        /// Plays sound on custom AudioSource. Use PlaySound with custom AudioStream to be able to reuse it in future. Having stream itself will let you manage it manually.
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string snd_name, AudioSource custom_src, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( snd_name )
                        .Source( custom_src )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start();
        }


        /// <summary>
        /// Plays sound on custom AudioSource
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <param name="custom_src">Custom AudioSource to play sound with</param>
        /// <param name="custom_stream">AudioStream from a pool that will play the clip.</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <param name="locker">Set here a tween that will use this stream</param>
        /// <returns>Length of sound</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string snd_name, AudioSource custom_src, out AudioStream custom_stream, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( snd_name )
                        .Group( group )
                        .Source( custom_src )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start()
                        .GetStream( out custom_stream );
        }
        #endregion


        #region Methods to play a set of AudioCLip

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases. Stores used AudioStream in stream variable. Stream may be reused.
        /// </summary>
        /// <param name="clips">sounds to play in a row</param>
        /// <param name="stream">AudioStream from a pool that will play the clip.</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="intervals">Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip[] clips, out AudioStream stream, StreamGroup group = StreamGroup.FX, float[] intervals = null, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clips )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .Intervals( intervals )
                        .OnComplete( on_complete )
                        .Start()
                        .GetStream( out stream );
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="clips">sounds to play in a row</param>
        /// <param name="custom_stream">Custom AudioStream that will play the clip. Won't be reused</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="intervals">Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip[] clips, AudioStream custom_stream, StreamGroup group = StreamGroup.FX, float[] intervals = null, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clips )
                        .Stream( custom_stream )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Intervals( intervals )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start();
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="clips">sounds to play in a row</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="intervals">Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip[] clips, AudioSource custom_src, StreamGroup group = StreamGroup.FX, float[] intervals = null, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clips )
                        .Source( custom_src )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Intervals( intervals )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start();
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="clips">sounds to play in a row</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="custom_stream">AudioStream from a pool that will play the clip.</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="intervals">Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( AudioClip[] clips, AudioSource custom_src, out AudioStream custom_stream, StreamGroup group = StreamGroup.FX, float[] intervals = null, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            return Play( clips )
                        .Group( group )
                        .Source( custom_src )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Intervals( intervals )
                        .Pitch( pitch )
                        .OnComplete( on_complete )
                        .Start()
                        .GetStream( out custom_stream );
        }

        #endregion


        #region Methods to play a set of sounds represented by their names

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases. Stores used AudioStream in stream variable. Stream may be reused.
        /// </summary>
        /// <param name="snd_name">AudioClip names, value "||2.2" gives extra pause between phrases</param>
        /// <param name="stream">AudioStream from a pool that will play the clip.</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="interval">Pause before next AudioClip will be played</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string[] snd_name, out AudioStream stream, StreamGroup group = StreamGroup.FX, float interval = 0, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            var handler = Play( snd_name )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete );
            if (interval > 0)
            {
                float[] intervals = new float[snd_name.Length];
                for (int i = 0; i < intervals.Length; i++)
                    intervals[i] = interval;

                handler.AddIntervals( intervals );
            }
            return handler.Start().GetStream( out stream );
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="snd_name">AudioClip names, value "||2.2" gives extra pause between phrases</param>
        /// <param name="custom_stream">Custom AudioStream that will play the clip. Won't be reused</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="interval">Pause before next AudioClip will be played</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string[] snd_name, AudioStream custom_stream, StreamGroup group = StreamGroup.FX, float interval = 0, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            var handler = Play( snd_name )
                        .Stream( custom_stream )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete );
            if (interval > 0)
            {
                float[] intervals = new float[snd_name.Length];
                for (int i = 0; i < intervals.Length; i++)
                    intervals[i] = interval;

                handler.AddIntervals( intervals );
            }
            return handler.Start();
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="snd_name">AudioClip names, value "||2.2" gives extra pause between phrases</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="interval">Pause before next AudioClip will be played</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string[] snd_name, AudioSource custom_src, StreamGroup group = StreamGroup.FX, float interval = 0, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            var handler = Play( snd_name )
                        .Source( custom_src )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete );
            if (interval > 0)
            {
                float[] intervals = new float[snd_name.Length];
                for (int i = 0; i < intervals.Length; i++)
                    intervals[i] = interval;

                handler.AddIntervals( intervals );
            }
            return handler.Start();
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="snd_name">AudioClip names, value "||2.2" gives extra pause between phrases</param>
        /// <param name="custom_src">Custom AudioSource to play sound</param>
        /// <param name="custom_stream">AudioStream from a pool that will play the clip.</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="interval">Pause before next AudioClip will be played</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <param name="pitch">Pitch value for sound</param>
        /// <param name="on_complete">Will be called when Stream will stop</param>
        /// <returns>Stack length</returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlaySound( string[] snd_name, AudioSource custom_src, out AudioStream custom_stream, StreamGroup group = StreamGroup.FX, float interval = 0, float volume_override = 1, bool loop = false, float pitch = 1, Action on_complete = null )
        {
            var handler = Play( snd_name )
                        .Source( custom_src )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Pitch( pitch )
                        .OnComplete( on_complete );
            if (interval > 0)
            {
                float[] intervals = new float[snd_name.Length];
                for (int i = 0; i < intervals.Length; i++)
                    intervals[i] = interval;

                handler.AddIntervals( intervals );
            }
            return handler.Start().GetStream( out custom_stream );
        }

        #endregion


        #region Methods from older AudioController

        /// <summary>
        /// Play's sound on Voice group using loop argument
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <returns></returns>
        [Obsolete( "Better use Play() method with AudioHandler" )]
        public static float PlayVoice( string snd_name, float volume_override = 1, bool loop = false )
        {
            return Play( snd_name )
                    .Group( StreamGroup.VOICE )
                    .Volume( volume_override )
                    .Loop( loop )
                    .Start();
        }

        #endregion
    }

    namespace Audio
    {
        public partial class AudioHandler
        {
            /// <summary>
            /// Is playback has been already started
            /// </summary>
            [Obsolete( "Renamed to isSoundStarted" )]
            public bool isSoundStart { get { return isSoundStarted; } }

            /// <summary>
            /// Set StreamGroup the clip is related to
            /// </summary>
            [Obsolete( "Renamed to Group" )]
            public AudioHandler SetGroup( StreamGroup group = StreamGroup.FX )
            {
                return Group( group );
            }

            /// <summary>
            /// This will override volume from GameSettings for the group
            /// </summary>
            [Obsolete( "Renamed to Volume" )]
            public AudioHandler SetVolume( float volume = 1.0f )
            {
                return Volume( volume );
            }

            /// <summary>
            /// This will loop the sound.
            ///  Warning! Loop is not supported for Sequence playing.
            /// </summary>
            [Obsolete( "Renamed to Loop" )]
            public AudioHandler SetLoop( bool loop = false )
            {
                return Loop( loop );
            }

            /// <summary>
            /// Pitch value for sound
            /// </summary>
            [Obsolete( "Renamed to Pitch" )]
            public AudioHandler SetPitch( float pitch = 1.0f )
            {
                return Pitch( pitch );
            }

            /// <summary>
            /// Will ignore pause and play on unscaled time.
            /// </summary>
            [Obsolete( "Renamed to IgnorePause" )]
            public AudioHandler SetIgnorePause( bool ignore_pause = false )
            {
                return IgnorePause( ignore_pause );
            }

            /// <summary>
            /// Custom AudioSource to play sound on.
            /// </summary>
            [Obsolete( "Renamed to Source" )]
            public AudioHandler SetSource( AudioSource custom_source )
            {
                return Source( custom_source );
            }

            /// <summary>
            /// Custom AudioSource to play sound on.
            /// Equal constructor <see cref="AudioStream"/> or <see cref="AudioController.GetCustomStream(AudioSource)"/>
            /// </summary>
            [Obsolete( "It is not safe to set AudioSource that can be destroyed on switch between scenes. Better buffer PlayingSound." )]
            public AudioHandler Source( AudioSource custom_source )
            {
                if (CheckAllowSetBefore( "Source" ))
                    result_stream = new AudioStream( custom_source );
                return this;
            }
            
            /// <summary>
            /// Custom AudioStream that will play the clip. Won't be reused.
            /// </summary>
            [Obsolete( "Renamed to Stream" )]
            public AudioHandler SetStream( AudioStream custom_stream )
            {
                return Stream( custom_stream );
            }

            /// <summary>
            /// Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause
            /// </summary>
            [Obsolete( "Renamed to Intervals" )]
            public AudioHandler SetIntervals( float[] intervals )
            {
                return Intervals( intervals );
            }

            /// <summary>
            /// Set Unity Audio Mixer Group for <see cref="AudioMixer"/>.
            /// </summary>
            [Obsolete( "Renamed to MixerGroup" )]
            public AudioHandler SetMixerGroup( AudioMixerGroup mixer_group )
            {
                return MixerGroup( mixer_group );
            }
        }
    }
}
