﻿using UnityEngine;

namespace PSV
{
    using Audio;
    using System.Collections.Generic;

    public static partial class AudioController
    {
        #region Main methods to play sounds

        /// <summary>
        /// Plays clip using own stream from the group.
        /// Return AudioHandler for set more params.
        /// WARNING! Must be called init method like <see cref="AudioHandler.Start()"/>.
        /// </summary>
        /// <param name="clip">AudioClip</param>
        public static AudioHandler Play( AudioClip clip )
        {
            if (clip)
                return new AudioHandler( new AudioClip[] { clip } );
            LogMessage( "ArgumentNull - AudioClip can not be empty!", true );
            return new AudioHandler( null );
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// Return AudioHandler for set more params.
        /// WARNING! Must be called init method like <see cref="AudioHandler.Start()"/>.
        /// </summary>
        /// <param name="clips">sounds to play in a row</param>
        public static AudioHandler Play( AudioClip[] clips )
        {
            if (clips == null || clips.Length == 0)
            {
                LogMessage( "ArgumentNull - AudioClips can not be empty!", true );
                return new AudioHandler( null );
            }
            return new AudioHandler( clips );
        }

        /// <summary>
        /// Plays sound by AudioClip's name
        /// Return AudioHandler for set more params.
        /// WARNING! Must be called init method like <see cref="AudioHandler.Start()"/>.
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        public static AudioHandler Play( string snd_name )
        {
            AudioClip snd_clip = LocalizationManager.GetSound( snd_name );
            if (snd_clip)
            {
                return new AudioHandler( new AudioClip[] { snd_clip } );
            }
            return new AudioHandler( null );
        }

        /// /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.Return AudioHandler for set more params.
        /// Return AudioHandler for set more params.
        /// WARNING! Must be called init method like <see cref="AudioHandler.Start()"/>.
        /// </summary>
        /// <param name="snd_name">AudioClip names, value "||2.2" gives extra pause between phrases</param>
        public static AudioHandler Play( string[] snd_names )
        {
            if (snd_names == null || snd_names.Length == 0)
            {
                LogMessage( "ArgumentNull - Audio clip names can not be empty!", true );
                return new AudioHandler( null );
            }
            List<AudioClip> tempClips = new List<AudioClip>();
            List<float> tempIntervals = new List<float>();

            float delay = 0;
            for (int i = 0; i < snd_names.Length; i++)
            {
                if (snd_names[i].Substring( 0, 2 ).Equals( PAUSE ))
                {
                    //if snd_name contains pause constant extra interval is added before next sound
                    float extra_delay;
                    if (float.TryParse( snd_names[i].Substring( 2 ), out extra_delay ))
                    {
                        //delay will be accumulated while pause constant will be met
                        delay += extra_delay;
                    }
                }
                else
                {
                    AudioClip snd_clip = LocalizationManager.GetSound( snd_names[i] );
                    if (snd_clip)
                    {
                        tempIntervals.Add( delay ); //as sound name was met - delay is saved to intervals to be applied before the sound will be played
                        delay = 0; //resetting delay to accumulate next interval

                        tempClips.Add( snd_clip );
                    }
                }
            }
            if (tempClips.Count == 0)
            {
                LogMessage( "No sound was found for a queue! Args: " + snd_names.ToString(), true );
                return new AudioHandler( null );
            }
            else
            {
                return new AudioHandler( tempClips.ToArray() ).Intervals( tempIntervals.ToArray() );
            }
        }

        #endregion

        /// <summary>
        /// Play's sound on Music group using loop argument
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <returns></returns>
        public static float PlayMusic( string snd_name, float volume_override = 1, bool loop = true )
        {
            return Play( snd_name )
                    .Group( StreamGroup.MUSIC )
                    .Volume( volume_override )
                    .AllowDuplicate( false )
                    .Loop( loop )
                    .Start();
        }

        #region PlaySound methods abstraction
        /// <summary>
        /// Plays clip using own stream from the group.
        /// </summary>
        /// <param name="clip">AudioClip</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <returns>Length of sound</returns>
        public static float PlaySound( AudioClip clip, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false )
        {
            return Play( clip )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Start()
                        .Length();
        }

        /// <summary>
        /// Plays sound by AudioClip's name and returns it's length
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sound</param>
        /// <returns>Length of sound</returns>
        public static float PlaySound( string snd_name, StreamGroup group = StreamGroup.FX, float volume_override = 1, bool loop = false )
        {
            return Play( snd_name )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Start()
                        .Length();
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="clips">sounds to play in a row</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="intervals">Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <returns>Stack length</returns>
        public static float PlaySound( AudioClip[] clips, StreamGroup group = StreamGroup.FX, float[] intervals = null, float volume_override = 1, bool loop = false )
        {
            return Play( clips )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop )
                        .Intervals( intervals )
                        .Start()
                        .Length();
        }

        /// <summary>
        /// Plays a set of sounds placed in a queue. Used for dialogs containing separate phrases.
        /// </summary>
        /// <param name="snd_name">AudioClip names, value "||2.2" gives extra pause between phrases</param>
        /// <param name="group">StreamGroup the clip is related to</param>
        /// <param name="interval">Pause before next AudioClip will be played</param>
        /// <param name="volume_override">This will override volume from GameSettings for the group</param>
        /// <param name="loop">This will loop the sequence</param>
        /// <returns>Stack length</returns>
        public static float PlaySound( string[] snd_name, StreamGroup group = StreamGroup.FX, float interval = 0, float volume_override = 1, bool loop = false )
        {
            var handler = Play( snd_name )
                        .Group( group )
                        .Volume( volume_override )
                        .Loop( loop );
            if (interval > 0)
            {
                float[] intervals = new float[snd_name.Length];
                for (int i = 0; i < intervals.Length; i++)
                    intervals[i] = interval;

                handler.AddIntervals( intervals );
            }
            return handler.Start().Length();
        }

        #endregion

    }
}
