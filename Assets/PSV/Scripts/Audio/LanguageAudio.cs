﻿using UnityEngine;
using System.Collections;

namespace PSV.Audio
{
    [System.Obsolete( "This logic move to LocalizationManager." )]
    public static partial class LanguageAudio
    {
        public static void Init()
        {
            LocalizationManager.InitNonLanguage();
        }

        public static AudioClip GetSoundByName( string name, bool multilanguage = true )
        {
            return LocalizationManager.GetSound( name );
        }

        public static void LoadSoundByName( string name, bool multilanguage = true, string folder = "" )
        {
            LocalizationManager.LoadSoundByName( name, multilanguage, folder );
        }

        public static void LoadSounds( bool multilanguage = true )
        {
            if (multilanguage)
                LocalizationManager.InitLanguage();
            else
                LocalizationManager.InitNonLanguage();
        }
    }
}