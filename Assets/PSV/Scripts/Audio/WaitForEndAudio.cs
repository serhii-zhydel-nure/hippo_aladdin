﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV.Audio
{
    public class WaitForEndAudio : CustomYieldInstruction
    {
        protected bool isPlaying;

        /// <summary>
        /// Waint for end playing Audio Stream.
        /// </summary>
        public WaitForEndAudio( AudioStream stream )
        {
            if (stream != null && stream.IsPlaying())
            {
                if (stream.loop)
                {
                    Debug.LogError( "AudioController: Can not wait for end looped AudioStream [" + stream.GetClipName() + "]! Waiting is skipped." );
                    return;
                }
                isPlaying = true;
                stream.OnComplete += OnStopStream;
            }
        }

        protected void OnStopStream()
        {
            isPlaying = false;
        }

        public override bool keepWaiting
        {
            get
            {
                return isPlaying;
            }
        }
    }

    public class WaitForEndAudioClip : WaitForEndAudio
    {
        protected AudioStream stream;
        protected float timeEnd;
        protected bool ignorePause;
        protected Action callback;

        /// <summary>
        /// Wait for end playing single Audio Clip.
        /// </summary>
        public WaitForEndAudioClip( AudioStream stream, Action callback, float delay_after = 0.0f ) : base( stream )
        {
            this.stream = stream;
            //delay = stream.timeLeft + delay_after;
            ignorePause = stream.ignoreListenerPause;
            timeEnd = ( ignorePause ? Time.unscaledTime : Time.time ) + stream.timeLeft;
            this.callback = callback;
        }

        public override bool keepWaiting
        {
            get
            {
                if (!isPlaying)
                    return false;
                //if (!stream.IsPaused())
                //    delay -= stream.ignoreListenerPause ? Time.unscaledDeltaTime : Time.deltaTime;
                if (timeEnd < ( ignorePause ? Time.unscaledTime : Time.time ))
                {
                    stream.OnComplete -= base.OnStopStream;
                    if (callback != null)
                        callback();
                    return false;
                }
                return true;
            }
        }
    }
}