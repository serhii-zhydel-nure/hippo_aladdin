﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Audio;

namespace PSV.Audio
{
    public partial class PlayingSound : IDisposable
    {
        private const string log_prefix = "AudioController: ";

        private AudioStream stream;
        private float audio_length;
        private bool isPlaying;

        internal PlayingSound() { isPlaying = false; }
        internal PlayingSound( AudioStream stream, float audio_length )
        {
            if (stream.IsPlaying())
            {
                this.stream = stream;
                this.audio_length = audio_length;
                isPlaying = true;
                stream.OnComplete += Dispose;
            }
        }

        #region Getters
        public bool IsPlaying() { return isPlaying; }

        public bool IsSequence() { return isPlaying && stream.IsSequence(); }

        public bool IsPaused() { return CheckAllow() && stream.IsPaused(); }

        /// <summary>
        /// Get current volume
        /// </summary>
        public float Volume() { return CheckAllow() ? stream.volume : 1.0f; }

        /// <summary>
        /// Is muted now.
        /// </summary>
        public bool Mute() { return CheckAllow() && stream.mute; }

        /// <summary>
        /// Is loop now.
        /// </summary>
        public bool Loop() { return CheckAllow() && stream.loop; }

        /// <summary>
        /// Will ignore general pause through AudioListener. Personal pause method works independently.
        /// </summary>
        public bool IgnoreListenerPause() { return CheckAllow() && stream.ignoreListenerPause; }

        /// <summary>
        /// Current plying AudioClip.
        /// </summary>
        public AudioClip Clip() { return isPlaying ? stream.GetClip() : null; }

        /// <summary>
        /// Length of AudioClip or Stack with all delays
        /// </summary>
        public float Length() { return audio_length; }

        /// <summary>
        /// Get time left playback. Return 0 on playback complete.
        /// For multiple sound playback return length. 
        /// </summary>
        public float TimeLeft()
        {
            if (CheckAllow())
            {
                if (IsSequence())
                    return audio_length;
                else
                    return stream.timeLeft;
            }
            return 0.0f;
        }

        /// <summary>
        /// Get final result <see cref="AudioStream"/>.
        /// </summary>
        [Obsolete( "It is not safe to store a link to AudioStream. Better buffer PlayingSound container." )]
        public AudioStream GetStream() { return isPlaying ? stream : null; }

        /// <summary>
        /// Get final result <see cref="AudioStream"/>.
        /// </summary>
        [Obsolete( "It is not safe to store a link to AudioStream. Better buffer PlayingSound container." )]
        public PlayingSound GetStream( out AudioStream result )
        {
            result = GetStream();
            return this;
        }

        #endregion

        #region Setters
        /// <summary>
        /// This will override volume from GameSettings for the group
        /// </summary>
        public PlayingSound Volume( float volume )
        {
            if (volume < 0.0f)
                volume = 1.0f;
            if (CheckAllow())
                stream.volume = volume;
            return this;
        }

        /// <summary>
        /// This will loop the sound.
        /// Warning! Loop is not supported for Sequence playing.
        /// </summary>
        public PlayingSound Loop( bool loop )
        {
            if (CheckAllow())
                stream.loop = loop;
            return this;
        }

        /// <summary>
        /// This will mute the sound.
        /// </summary>
        public PlayingSound Mute( bool mute )
        {
            if (CheckAllow())
                stream.mute = mute;
            return this;
        }

        /// <summary>
        /// Pitch value for sound
        /// </summaryPlayingSound
        public PlayingSound Pitch( float pitch )
        {
            if (CheckAllow())
                stream.pitch = pitch;
            return this;
        }

        /// <summary>
        /// Will be called when Stream stops
        /// </summary>
        public PlayingSound OnComplete( Action complete_action )
        {
            if (CheckAllow())
            {
                this.stream.OnComplete += complete_action;
            }
            else
            {
                if (complete_action != null)
                    complete_action();
            }
            return this;
        }

        /// <summary>
        /// Set Unity Audio Mixer Group for <see cref="AudioMixer"/>.
        /// </summary>
        public PlayingSound MixerGroup( AudioMixerGroup mixer_group )
        {
            if (CheckAllow())
            {
                stream.outputMixerGroup = mixer_group;
            }
            return this;
        }

        #endregion

        /// <summary>
        /// Return YieldInstruction for wait playback complete. If necessary auto call <see cref="Start()"/>.
        /// </summary>
        public WaitForEndAudio Wait()
        {
            if (CheckAllow())
            {
                return new WaitForEndAudio( stream );
            }
            return null;
        }

        /// <summary>
        /// Stops current stream and disables all other processes that are performed with it (locker, SoundStopMonitor). After stream stopped events will be generated.
        /// </summary>
        public PlayingSound Stop()
        {
            if (CheckAllow())
                stream.Stop();
            return this;
        }

        /// <summary>
        /// Fade volume and Stops current stream and disables all other processes that are performed with it (locker, SoundStopMonitor). After stream stopped events will be generated.
        /// </summary>
        public PlayingSound Stop( float fade_duration )
        {
            if (CheckAllow())
                stream.Stop( fade_duration );
            return this;
        }

        /// <summary>
        /// Pausing stream (Will kill and resume SoundStopMonitor if it is needed and generate corresponding event). 
        /// Returns true if stream is in correct state.
        /// </summary>
        public PlayingSound Pause( bool paused )
        {
            if (CheckAllow())
                stream.Pause( paused );
            return this;
        }

        /// <summary>
        /// Return <see cref="timeLeft"/>.
        /// </summary>
        /// Support obsolete style AudioController.
        public static implicit operator float( PlayingSound handler ) { return handler.TimeLeft(); }

        /// <summary>
        /// Close accses to AudioStream and free memory.
        /// </summary>
        public void Dispose()
        {
            stream.OnComplete -= Dispose;
            isPlaying = false;
            stream = null;
        }

        private bool CheckAllow()
        {
            if (isPlaying)
                return true;
#if UNITY_EDITOR
            Debug.LogWarning( log_prefix + "Can not access the stream because it is already completed!" );
#endif
            return false;
        }
        
    }
}