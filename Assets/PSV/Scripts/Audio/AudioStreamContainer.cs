﻿using UnityEngine;
using System.Collections.Generic;
using System;


namespace PSV.Audio
{

    /// <summary>
    /// Manages streams that belong to a certain StreamGroup. Holds a pool of AudioStream objects and re-uses them for playback. Uses group specific settings.
    /// </summary>
	public partial class AudioStreamContainer
    {
        #region Fields
        private const string settings_volume_pref = "volume";
        private const string settings_muted_pref = "muted";

        private readonly GameObject
            src_parent;
        public readonly StreamGroup
            stream_type;
        private float
            volume;
        private bool
            mute = false;
        private bool
            solo_playing = false;
        private Queue<AudioStream>
            container = new Queue<AudioStream>();
        #endregion

        /// <summary>
        /// used to store in counter a number of positive subscribers in terms of event conditions
        /// </summary>
        /// <param name="snd_name">sound name</param>
        /// <param name="counter">positive event results</param>
        public delegate void CounterCallback( string snd_name, ref int counter );

        public event CounterCallback
            OnSoundPlayingRequest,
            OnStopSound;

        /// <summary>
        /// Params (float newGroupVolume)
        /// </summary>
		public event Action<float>
            OnGroupVolumeChanged;

        /// <summary>
        /// Params (bool mute_value)
        /// </summary>
        public event Action<bool>
            OnGroupMuted;


        /// <summary>
        /// Constructor for container
        /// </summary>
        public AudioStreamContainer( StreamGroup group )
        {
            this.stream_type = group;
            solo_playing = Common.IsSoloGroup( group );
            volume = Common.GetGroupVolume( group );
            volume *= PlayerPrefs.GetFloat( settings_volume_pref + group.ToString(), 1.0f );
            mute = PlayerPrefs.GetInt( settings_muted_pref, 0 ) == 1;
            src_parent = new GameObject( "[" + group + "] AudioSources" );
            UnityEngine.Object.DontDestroyOnLoad( src_parent );
        }


        /// <summary>
        /// Creates AudioStream. If src is set AudioStream will not be reused after playback complete. 
        /// You can use it to play sounds on custom AudioSources (eg: you have to use distance to AudioListener to make sound fade in and out when src reaches camera and leaves it)
        /// </summary>
        /// <returns>Stream attached to this container</returns>
        public AudioStream CreateStream()
        {
            LogMessage( "Creating new stream on group " + stream_type );

            AudioStream stream = new AudioStream( src_parent.AddComponent<AudioSource>() );
            return stream;
        }

        /// <summary>
        /// Stores stream to the queue to use it for further playback
        /// </summary>
        /// <param name="stream"></param>
        internal void EnqueueStream( AudioStream stream )
        {
            container.Enqueue( stream );
        }
        
        /// <summary>
        /// Returns free stream and removes it from queue
        /// </summary>
        /// <returns></returns>
        private AudioStream DequeueStream()
        {
            AudioStream stream = null;
            if (container.Count > 0)
            {
                stream = container.Dequeue();
            }
            return stream;
        }


        /// <summary>
        /// Stops streams in group only that are playing  the sound with name snd_name  (the rest of the streams in other groups will continue playing the sound, if the were).
        /// </summary>
        /// <param name="snd_name">AudioCLip name</param>
        /// <returns></returns>
        public int StopSound( string snd_name )
        {
            LogMessage( "StopSound " + snd_name );
            int counter = 0;
            if (!string.IsNullOrEmpty( snd_name ) && OnStopSound != null)
            {
                OnStopSound( snd_name, ref counter );
            }
            return counter;
        }

        /// <summary>
        /// Will mute/unmute streams in the group.
        /// </summary>
        /// <param name="param">true - mute, false - unmute</param>
        public void Mute( bool param, bool save_settings )
        {
            mute = param;
            if (save_settings)
            {
                PlayerPrefs.SetInt( settings_muted_pref + stream_type.ToString(), param ? 1 : 0 );
                NextFrameCall.SavePrefs();
            }
            if (OnGroupMuted != null)
            {
                OnGroupMuted( param );
            }
        }


        /// <summary>
        /// Indicates whether streams in group are muted
        /// </summary>
        /// <returns></returns>
        public bool IsMuted()
        {
            return mute;
        }

        /// <summary>
        /// Stops streams at the container
        /// </summary>
        public void Release()
        {
            int counter = 0;
            if (OnStopSound != null)
            {
                OnStopSound( null, ref counter );
            }
        }


        /// <summary>
        /// Stream Group Volume
        /// </summary>
        public float Volume
        {
            get
            {
                return volume;
            }
            set
            {
                PlayerPrefs.SetFloat( settings_volume_pref + stream_type.ToString(), value / Common.GetGroupVolume( stream_type ) );
                NextFrameCall.SavePrefs();
                if (OnGroupVolumeChanged != null)
                {
                    OnGroupVolumeChanged( value ); // First set for streams in group
                }
                volume = value; // Second store group value

            }
        }

        /// <summary>
        /// Is will reuse same stream in group.
        /// Redirection to <see cref="Common.IsSoloGroup(StreamGroup)"/>
        /// </summary>
        public bool Solo
        {
            get
            {
                return solo_playing;
            }
            set
            {
                solo_playing = value;
            }
        }

        /// <summary>
        /// Will tell true if stream plays the sound
        /// </summary>
        /// <param name="snd_name">AudioClip name</param>
        /// <returns>Whether the sound is currently playing</returns>
        public bool IsSoundPlaying( string snd_name )
        {
            int counter = 0;
            if (OnSoundPlayingRequest != null)
            {
                OnSoundPlayingRequest( snd_name, ref counter );
            }
            return counter > 0;
        }

        public bool IsSoundPlayingAny()
        {
            int counter = 0;
            if (OnSoundPlayingRequest != null)
            {
                OnSoundPlayingRequest( null, ref counter );
            }
            return counter > 0;
        }

        /// <summary>
        /// Select all Playing sounds names and volume.
        /// </summary>
        public Dictionary<string, float> GetPlayingNames()
        {
            var sources = src_parent.GetComponents<AudioSource>();
            Dictionary<string, float> resultList = new Dictionary<string, float>();
            for (int i = 0; i < sources.Length; i++)
            {
                if (sources[i].isPlaying)
                {
                    resultList.Add( sources[i].clip.name, sources[i].volume / volume );
                }
            }
            return resultList;
        }


        /// <summary>
        /// Returns AudioStream from group's pool.
        /// If Group has no available AudioStream it will be created and attached to the group automatically.
        /// If group uses single stream mode - stream in container will be re-used.
        /// </summary>
        /// <returns>Stream from group's pool</returns>
        internal AudioStream GetStream()
        {
            AudioStream stream = DequeueStream();
            if (!stream)
            {
                stream = CreateStream();
            }

            return stream;
        }



        /// <summary>
        /// Manages console messages for current class
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="error"></param>
        private void LogMessage( string msg, bool error = false )
        {
            msg = "AudioStreamContainer<" + stream_type + ">: " + msg;
            if (error)
            {
                Debug.LogError( msg );
            }
            else if (AudioController.debug_mode)
            {
                Debug.Log( msg );
            }
        }

    }
}
