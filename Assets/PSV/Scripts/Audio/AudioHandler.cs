﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Audio;

namespace PSV.Audio
{
    /// <summary>
    /// Handler play sounds. 
    /// For applying all parameters and initializing the playback of sound need call <see cref="Start()"/> or analog.
    /// Can implicit converting to <see cref="float"/>(time left).
    /// </summary>
    /// <example>
    /// Support constructions:
    /// 1) AudioController.Play( Sound )[.Params()].Start();
    /// 2) yield return AudioController.Play( Sound )[.Params()].Wait();
    /// 3) yield return AudioController.Play( Sound )[.Params()].StartPhrase( animation_action );
    /// 4) float timeLeft = AudioController.Play( Sound )[.Params()];
    /// </example>
    public partial class AudioHandler
    {
        #region Fields
        private const string log_prefix = "AudioController: ";

        private StreamGroup stream_group = StreamGroup.FX;
        private bool loop = false;
        private bool ignore_pause = false;
        private bool duplicates_allowed = true;
        private float volume_override = 1.0f;
        private float pitch = 1.0f;
        private AudioMixerGroup mixer_group;
        private float[] intervals = null;

        private Action complete_action;
        private AudioClip[] audio_clips = null;
        private AudioStream result_stream;
        private PlayingSound playingSound = null;

#if UNITY_EDITOR
        private string start_call_stack;
#endif
        #endregion

        /// /// <summary>
        /// Handle only NULL or array audio clips
        /// </summary>
        public AudioHandler( AudioClip[] clips )
        {
            audio_clips = clips;
#if UNITY_EDITOR
            start_call_stack = new System.Diagnostics.StackTrace( false ).ToString();
#endif
        }

#if UNITY_EDITOR
        ~AudioHandler()
        {
            if (!isSoundStarted && audio_clips != null && audio_clips.Length != 0)
            {
                Debug.LogError( log_prefix + "Sound won't be played because you did not call Start()\n" + start_call_stack );
            }
        }
#endif

        /// <summary>
        /// Applying all parameters and initializing the playback of sound.
        /// This method or analog should always be called!
        /// </summary>
        public PlayingSound Start()
        {
            if (isSoundStarted)
            {
                Debug.LogError( log_prefix + "Sound [" + GetFirstClipName() + "] playback has been already started" );
                return playingSound;
            }
            float currDelay = 0.0f;
            float audio_length;
            AudioStreamContainer container = AudioController.GetGroup( stream_group );

            if (audio_clips == null || audio_clips.Length == 0)
            {
                if (complete_action != null)
                    complete_action();
                return playingSound = new PlayingSound();
            }

            if (!duplicates_allowed && container.IsSoundPlaying( audio_clips[0].name ))
            {
                audio_length = audio_clips[0].length;
                if (complete_action != null)
                    complete_action();
                // TODO Get current playing source.
                return playingSound = new PlayingSound();  // Skip new sound, because it already playing
            }

            if (container.Solo)
            {
                //to save single stream mode on the group other streams will be stopped before playing new sound
                //after this method competes free stream will be enqueued
                container.Release();
            }

            if (result_stream == null)
            {
                result_stream = container.GetStream();
            }

            if (audio_clips.Length > 1)
            {
                for (int i = 0; i < audio_clips.Length; i++)
                {
                    float delay = 0;
                    if (intervals != null && intervals.Length > i)
                        delay = intervals[i];

                    if (audio_clips[i])
                        currDelay += audio_clips[i].length + delay + Time.deltaTime; // + Time.deltaTime for have time to stop the previous Audio stream
                    else
                        Debug.LogError( log_prefix + "Sound " + i + " is null" );
                }
                result_stream.loop = false;
                result_stream.Play( audio_clips, complete_action, ignore_pause, intervals, container );
            }
            else
            {
                result_stream.loop = loop;
                result_stream.Play( audio_clips[0], complete_action, ignore_pause, container );
                currDelay = audio_clips[0].length;
            }
            result_stream.volume = volume_override;
            result_stream.pitch = pitch;
            result_stream.outputMixerGroup = mixer_group;
            audio_length = Mathf.Clamp( currDelay + Time.deltaTime, 0.0f, float.MaxValue ); // + Time.deltaTime for have time to stop the previous Audio stream
            playingSound = new PlayingSound( result_stream, audio_length );
            result_stream = null;
            complete_action = null;
            return playingSound;
        }

        /// <summary>
        /// Analogue to the method <see cref="Start()"/> for phrases.
        /// Auto-set <see cref="StreamGroup.VOICE"/> and skip phrase when muted.
        /// </summary>
        /// <!--It is better to skip the phrases and animations of the conversation if the sound is turned off.-->
        /// <param name="animation_action">Callback True on start, and False on complete.</param>
        public WaitForEndAudio StartPhrase( Action<bool> animation_action )
        {
            if (isSoundStarted)
            {
                Debug.LogError( log_prefix + "Sound [" + GetFirstClipName() + "] playback has been already started. Replace Start() method or use Wait()." );
                return playingSound.Wait();
            }
            else if (AudioController.GetGroup( StreamGroup.VOICE ).IsMuted()) // Skip phrase when Voice group muted
            {
                return null;
            }
            Group( StreamGroup.VOICE );
            if (animation_action != null)
            {
                animation_action( true ); // Play animation
                OnComplete( () => animation_action( false ) ); // Stop animation on complete sound
            }
            return Wait();
        }

        /// <summary>
        /// Return YieldInstruction for wait playback complete. If necessary auto call <see cref="Start()"/>.
        /// </summary>
        public WaitForEndAudio Wait()
        {
            if (!isSoundStarted)
                Start();
            return playingSound.Wait();
        }

        /// <summary>
        /// Is playback has been already started
        /// </summary>
        public bool isSoundStarted { get { return playingSound != null; } }

        #region AudioStream Properties

        /// <summary>
        /// Set StreamGroup the clip is related to
        /// </summary>
        public AudioHandler Group( StreamGroup group = StreamGroup.FX )
        {
            if (CheckAllowSetBefore( "Group" ))
            {
                this.stream_group = group;
            }
            return this;
        }

        /// <summary>
        /// This will override volume from GameSettings for the group
        /// </summary>
        public AudioHandler Volume( float volume = 1.0f )
        {
            if (volume < 0.0f)
                volume = 1.0f;
            if (isSoundStarted)
                playingSound.Volume( volume );
            else
                this.volume_override = volume;
            return this;
        }

        /// <summary>
        /// This will loop the sound.
        ///  Warning! Loop is not supported for Sequence playing.
        /// </summary>
        public AudioHandler Loop( bool loop = false )
        {
            if (isSoundStarted)
                playingSound.Loop( loop );
            else
                this.loop = loop;
            return this;
        }

        /// <summary>
        /// Pitch value for sound
        /// </summary>
        public AudioHandler Pitch( float pitch = 1.0f )
        {
            if (isSoundStarted)
                playingSound.Pitch( pitch );
            else
                this.pitch = pitch;
            return this;
        }

        /// <summary>
        /// Will be called when Stream stops
        /// </summary>
        public AudioHandler OnComplete( Action complete_action )
        {
            if (isSoundStarted)
                playingSound.OnComplete( complete_action );
            else
                this.complete_action += complete_action;
            return this;
        }

        /// <summary>
        /// Will ignore pause and play on unscaled time.
        /// </summary>
        public AudioHandler IgnorePause( bool ignore_pause = false )
        {
            if (CheckAllowSetBefore( "Ignore Pause" ))
                this.ignore_pause = ignore_pause;
            return this;
        }

        /// <summary>
        /// Custom AudioStream that will play the clip. Won't be reused.
        /// </summary>
        public AudioHandler Stream( AudioStream custom_stream )
        {
            if (CheckAllowSetBefore( "Stream" ))
                result_stream = custom_stream;
            return this;
        }

        /// <summary>
        /// Add Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause
        /// </summary>
        public AudioHandler AddIntervals( float[] intervals )
        {
            if (intervals == null || intervals.Length == 0)
                return this;

            if (CheckAllowSetBefore( "Intervals" ))
            {
                if (this.intervals == null || this.intervals.Length == 0)
                {
                    this.intervals = intervals;
                }
                else
                {
                    for (int i = 0; i < this.intervals.Length; i++)
                    {
                        if (intervals.Length > i)
                            this.intervals[i] += intervals[i];
                        else
                            break;
                    }
                }
            }
            return this;
        }

        /// <summary>
        /// Pause before next AudioClip will be played, should be of the same length as clips or null to make no pause
        /// </summary>
        public AudioHandler Intervals( float[] intervals )
        {
            if (CheckAllowSetBefore( "Intervals" ))
                this.intervals = intervals;
            return this;
        }

        /// <summary>
        /// Allow playback with the same sound clip that is already running. (Default True)
        /// Recommended for <see cref="StreamGroup.MUSIC"/> setting False 
        /// </summary>
        /// <!--If music playback called with same audioclip -->
        /// <!--this will make stream to play clip from the beginning or continue its playback-->
        public AudioHandler AllowDuplicate( bool duplicates_allowed = true )
        {
            if (CheckAllowSetBefore( "AllowDuplicate" ))
                this.duplicates_allowed = duplicates_allowed;
            return this;
        }

        /// <summary>
        /// Set Unity Audio Mixer Group for <see cref="AudioMixer"/>.
        /// </summary>
        public AudioHandler MixerGroup( AudioMixerGroup mixer_group )
        {
            if (isSoundStarted)
                playingSound.MixerGroup( mixer_group );
            else
                this.mixer_group = mixer_group;
            return this;
        }

        #endregion

        /// <summary>
        /// Return <see cref="timeLeft"/>. If necessary auto call <see cref="Start()"/>.
        /// </summary>
        /// Support obsolete style AudioController.
        public static implicit operator float( AudioHandler handler )
        {
            if (handler.isSoundStarted)
                return handler.playingSound.TimeLeft();
            else
                return handler.Start().Length();
        }

        #region Log handlers
        /// <summary>
        /// Whether property change allowed before playback started
        /// </summary>
        /// <param name="variable_name">Changed variable debug name</param>
        /// <returns></returns>
        protected bool CheckAllowSetBefore( string variable_name )
        {
            if (isSoundStarted)
            {
                Debug.LogWarning( log_prefix + "Can't be changed " + variable_name + " after start" );
                return false;
            }
            return true;
        }

        protected string GetFirstClipName()
        {
            if (audio_clips != null && audio_clips.Length > 0 && audio_clips[0] != null)
            {
                return audio_clips[0].name;
            }
            return "NULL";
        }
        #endregion

    }
}