﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Audio;

namespace PSV.Audio
{

    /// <summary>
    /// Class-wrapper for AudioSource to handle AudioClips playback end. 
    /// It manages AudioSources major set of parameters and methods.
    /// </summary>
    public partial class AudioStream
    {
        #region Fields
        private AudioSource src = null;
        private AudioStreamContainer container = null;
        private bool is_paused = false;
        private bool is_busy = false;

        /// <summary>
        /// Call once per playback. 
        /// </summary>
        public event Action OnComplete;

        private Coroutine sound_stop_monitor = null;
        private Coroutine sequence_playing = null;
        private Coroutine smooth_voume_change_action;
        #endregion

        /// <summary>
        /// AudioStream constructor.
        /// </summary>
        /// <param name="src">AudioSource to use for playback</param>
        public AudioStream( AudioSource src )
        {
            if (src)
            {
                this.src = src;
                this.src.playOnAwake = false;
            }
            else
            {
                throw new Exception( "AudioStream is created with null AudioSource" );
            }
        }

        /// <summary>
        /// Plays AudioClip on a stream and will execute callback on stream stopped.
        /// </summary>
        /// <param name="clip">Sound that will be played on the stream</param>
        /// <param name="on_stop_callback">This delegate will be called as stream will be stopped. 
        /// This can happen by manual call of Stop() method, as sound or a sequence of sounds will end. 
        /// Looped sounds will stop in manual mode ar on release.</param>
        /// <param name="ignore_pause">Will ignore pause and play on unscaled time.</param>
        public void Play( AudioClip clip, Action on_stop_callback, bool ignore_pause, AudioStreamContainer container )
        {
            if (!clip)
            {
                LogMessage( "Play is called null AudioClip", true );
                return;
            }
            if (!src)
            {
                LogMessage( "Play is called on null AudioSource", true );
            }
            LogMessage( "Playing " + clip.name );

            is_busy = true;
            OnComplete = on_stop_callback;
            src.ignoreListenerPause = ignore_pause;
            src.mute = container.IsMuted();
            src.clip = clip;
            src.Play();
            if (!src.loop)
            {
                //scheduling Stream stop if it is not looped
                StartSoundStopMonitor();
            }

            Subscribe( container );
        }

        /// <summary>
        /// Plays AudioClips sequence on a stream and will execute callback on sequence stopped.
        /// </summary>
        /// <param name="clips">Sounds that will be played on the stream</param>
        /// <param name="on_stop_callback">This delegate will be called as stream will be stopped. 
        /// This can happen by manual call of Stop() method, as sound or a sequence of sounds will end. 
        /// Looped sounds will stop in manual mode ar on release.</param>
        /// <param name="ignore_pause">Will ignore pause and play on unscaled time.</param>
        public void Play( AudioClip[] clips, Action on_stop_callback, bool ignore_pause, float[] intervals, AudioStreamContainer container )
        {
            if (clips == null || clips.Length == 0)
            {
                LogMessage( "Play is called null AudioClip", true );
                return;
            }
            if (!src)
            {
                LogMessage( "Play is called on null AudioSource", true );
            }

            OnComplete = on_stop_callback;
            src.ignoreListenerPause = ignore_pause;

            LogMessage( "Playing sequence " + clips.ConvertToString() );
            is_busy = true;
            Subscribe( container );
            sequence_playing = CoroutineHandler.Start( PlaySequence( clips, intervals ) );

        }

        private IEnumerator PlaySequence( AudioClip[] clips, float[] intervals )
        {
            AudioStream _stream = this; //caching stream for anonymous methods

            int clipsCount = clips.Length;
            for (int i = 0; i < clipsCount; i++)
            {
                float delay = 0;
                if (intervals != null && intervals.Length > i)
                    delay = intervals[i];

                if (clips[i])
                {
                    src.clip = clips[i];
                    src.Play();
                    yield return new WaitForEndAudioClip( _stream, null, delay + Time.deltaTime );
                }
            }
            _stream.Stop();
        }

        #region Source properties
        /// <summary>
        /// Will ignore general pause through AudioListener. Personal pause method works independently.
        /// </summary>
        public bool ignoreListenerPause
        {
            get
            {
                return src.ignoreListenerPause;
            }
        }

        /// <summary>
        /// Get time left playback current iteration. Return 0 on playback complete.
        /// </summary>
        public float timeLeft
        {
            get
            {
                if (!is_busy)
                    return 0.0f;
                return src.clip.length - src.time;
            }
        }

        /// <summary>
        /// Get/Set personal volume without considering Group volume.
        /// </summary>
        public float volume
        {
            get
            {
                if (container == null)
                    return src.volume;
                else
                    return src.volume / container.Volume;
            }
            set
            {
                if (container == null)
                    src.volume = value;
                else
                    src.volume = container.Volume * value;
            }
        }

        /// <summary>
        /// Get/Set pitch value for stream.
        /// </summary>
        public float pitch
        {
            get
            {
                return src.pitch;
            }
            set
            {
                src.pitch = value;
            }
        }

        /// <summary>
        /// Get/Set loop value for stream. Will not take effect for locked stream.
        /// Warning! Loop is not supported for Sequence playing.
        /// </summary>
        public bool loop
        {
            get
            {
                return src.loop;
            }
            set
            {
                if (IsPlaying())
                {
                    if (!IsSequence())
                    {
                        src.loop = value;
                        if (src.loop)
                            KillSoundStopMonitor();
                        else
                            ResumeSoundStopMonitor();
                    }
                }
                else
                {
                    src.loop = value;
                }
            }
        }

        /// <summary>
        /// Get/Set mute value for stream.
        /// </summary>
        public bool mute
        {
            get
            {
                return src.mute;
            }
            set
            {
                if (container != null)
                    src.mute = container.IsMuted() || value;
                else
                    src.mute = value;
            }
        }

        public AudioMixerGroup outputMixerGroup
        {
            get
            {
                return src.outputAudioMixerGroup;
            }
            set
            {
                src.outputAudioMixerGroup = value;
            }
        }
        #endregion

        #region Check methods

        public bool IsSequence() { return sequence_playing != null; }

        public bool IsPlaying()
        {
            return is_busy;
        }

        /// <summary>
        /// Returns true if Clip set to audioSource.
        /// </summary>
        /// <param name="snd_name">Name of the sound that will be played on the stream</param>
        /// <returns></returns>
        public bool IsPlaying( string snd_name )
        {
            return ( GetClipName().Equals( snd_name ) );
        }

        /// <summary>
        /// Returns true if Clip set to audioSource.
        /// </summary>
        /// <param name="snd_name">Name of the sound that will be played on the stream</param>
        /// <param name="counter">Counter that indicates how much streams are playing the sound</param>
        private void IsPlaying( string snd_name, ref int counter )
        {
            if (IsPlaying() && ( string.IsNullOrEmpty( snd_name ) || IsPlaying( snd_name ) ))
            {
                counter++;
            }
        }

        public bool IsPaused()
        {
            return is_paused;
        }
        #endregion

        /// <summary>
        /// Subscribe to container's events that will take effect for the entire group. Is needed only while playback is active.
        /// </summary>
        /// <param name="container">Container that stream belongs to</param>
        private void Subscribe( AudioStreamContainer container )
        {
            Unsubscribe();
            LogMessage( "Subscribe stream " + GetName() + " [" + GetClipName() + "]" );
            this.container = container;
            container.OnStopSound += StopByName;
            container.OnGroupVolumeChanged += OnGroupVolumeChanged;
            container.OnGroupMuted += OnGroupMuted;
            container.OnSoundPlayingRequest += IsPlaying;
        }

        /// <summary>
        /// Unsubscribe from container's events that will take effect for the entire group. Is needed only while playback is active.
        /// </summary>
        /// <param name="container">Container that stream belongs to</param>
        private void Unsubscribe()
        {
            if (container != null)
            {
                LogMessage( "Unsubscribe stream " + GetName() + " [" + GetClipName() + "]" );
                container.OnStopSound -= StopByName;
                container.OnGroupVolumeChanged -= OnGroupVolumeChanged;
                container.OnGroupMuted -= OnGroupMuted;
                container.OnSoundPlayingRequest -= IsPlaying;
                container.EnqueueStream( this );
                container = null;
            }
        }

        /// <summary>
        /// Stops coroutine monitoring playback end.
        /// </summary>
        private void KillSoundStopMonitor()
        {
            CoroutineHandler.Stop( sound_stop_monitor );
            sound_stop_monitor = null;
        }

        /// <summary>
        /// Starts coroutine that is waiting for snd_length and stops stream (you should take in account current playback position)
        /// </summary>
        private void ResumeSoundStopMonitor()
        {
            if (!IsSequence() && IsPlaying() && src.clip != null && !src.loop)
            {
                StartSoundStopMonitor();
            }
        }

        /// <summary>
        /// Starts coroutine that is waiting for snd_length and stops stream (you should take in account current playback position).
        /// </summary>
        /// <param name="snd_length"></param>
        private void StartSoundStopMonitor()
        {
            if (sound_stop_monitor == null)
            {
                sound_stop_monitor = CoroutineHandler.Start( new WaitForEndAudioClip( this, Stop ) );
            }
        }

        /// <summary>
        /// Event handler for volume change on entire group the stream belongs to.
        /// </summary>
        /// <param name="newGroupVolume"></param>
        private void OnGroupVolumeChanged( float newGroupVolume )
        {
            float selfVolume = volume; // Calculate self volume not considering group volume
            src.volume = selfVolume * newGroupVolume;
        }

        /// <summary>
        /// Event handler for muting entire group the stream belongs to.
        /// </summary>
        /// <param name="mute">Tells whether stream should be muted</param>
        private void OnGroupMuted( bool mute )
        {
            src.mute = mute;
        }


        /// <summary>
        /// Fade volume and Stops current stream and disables all other processes that are performed with it (locker, SoundStopMonitor). After stream stopped events will be generated.
        /// </summary>
        public void Stop( float fade_duration )
        {
            if (fade_duration <= 0.0f)
                Stop();
            else if (loop || fade_duration < timeLeft)
                smooth_voume_change_action = CoroutineHandler.Start( SmoothVolumeAction( fade_duration, 0.0f, Stop ) );
        }

        /// <summary>
        /// Stops current stream and disables all other processes that are performed with it (locker, SoundStopMonitor). After stream stopped events will be generated.
        /// </summary>
        public void Stop()
        {
            if (src)
            {
                if (is_busy)
                {
                    CoroutineHandler.Stop( smooth_voume_change_action );
                    CoroutineHandler.Stop( sequence_playing );
                    smooth_voume_change_action = null;
                    sequence_playing = null;

                    Unsubscribe();
                    KillSoundStopMonitor();
                    LogMessage( "Stop for sound '" + GetClipName() + "' is called" );

                    is_busy = false;
                    is_paused = false;
                    src.Stop();
                    src.clip = null;

                    if (OnComplete != null)
                    {
                        OnComplete();
                        OnComplete = null;
                    }
                }
            }
            else
            {
                LogMessage( "Stop is called on null AudioSource" );
            }
        }

        /// <summary>
        /// Will stop stream if it play's clip with a given name.
        /// </summary>
        /// <param name="snd_name">Name of the sound that will be played on the stream</param>
        private void StopByName( string snd_name, ref int counter )
        {

            if (string.IsNullOrEmpty( snd_name ) || string.Equals( GetClipName(), snd_name, StringComparison.OrdinalIgnoreCase ))
            {
                Stop();
                counter++;
            }
        }

        /// <summary>
        /// Pausing stream (Will kill and resume SoundStopMonitor if it is needed and generate corresponding event). 
        /// Returns true if stream is in correct state.
        /// </summary>
        public void Pause( bool pause )
        {
            if (src)
            {
                if (is_busy)
                {
                    is_paused = pause;

                    if (pause)
                        src.Pause();
                    else
                        src.Play();

                    if (is_paused)
                    {
                        KillSoundStopMonitor();
                    }
                    else
                    {
                        ResumeSoundStopMonitor();
                    }
                }
                else
                {
                    LogMessage( "Pause is called on non-playing AudioStream", true );
                }
            }
            else
            {
                LogMessage( "Pause is called on null AudioSource", true );
            }
        }


        /// <summary>
        /// WIll return sound that is playing currently.
        /// </summary>
        /// <returns>Sound playing now</returns>
        public AudioClip GetClip()
        {
            AudioClip res = null;
            if (src)
            {
                res = src.clip;
            }
            return res;
        }

        /// <summary>
        /// WIll return name of the sound that is playing currently.
        /// </summary>
        /// <returns>Name of the sound playing now</returns>
        public string GetClipName()
        {
            string res = "";
            AudioClip clip = GetClip();
            if (clip)
            {
                res = clip.name;
            }
            return res;
        }

        /// <summary>
        /// WIll return name of gameObject the stream's AudioSource is attached to.
        /// </summary>
        /// <returns>Name of the stream</returns>
        public string GetName()
        {
            return src ? src.gameObject.name : "AudioStream is broken: src is missing";
        }


        private IEnumerator SmoothVolumeAction( float duration, float target, Action callback )
        {
            if (duration > 0.0f)
            {
                target = Mathf.Clamp01( target );
                float multiplier = ( 1.0f / duration ) * Mathf.Abs( src.volume - target );

                while (src.volume != target)
                {
                    src.volume = Mathf.MoveTowards( src.volume, target, ( ignoreListenerPause ? Time.unscaledDeltaTime : Time.deltaTime ) * multiplier );
                    yield return null;
                }
            }
            if (callback != null)
                callback();
            smooth_voume_change_action = null;
        }

        /// <summary>
        /// Logs message to console.
        /// </summary>
        /// <param name="msg">Message text</param>
        /// <param name="error">Defines Log level: error/verbose</param>
        private static void LogMessage( string msg, bool error = false )
        {
            msg = "AudioStream: " + msg;
            if (error)
            {
                Debug.LogError( msg );
            }
            else if (AudioController.debug_mode)
            {
                Debug.Log( msg );
            }
        }
        

        public static implicit operator bool( AudioStream stream )
        {
            return stream != null && stream.src;
        }
    }
}