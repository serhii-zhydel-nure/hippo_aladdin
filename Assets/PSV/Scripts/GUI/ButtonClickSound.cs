﻿#define BRAND_NEW_PROTOTYPE

using UnityEngine;
using UnityEngine.UI;
using PSV.Audio;

namespace PSV
{
	public class ButtonClickSound :MonoBehaviour
	{
#if BRAND_NEW_PROTOTYPE
        //* Moved to ConstSettings script.
		//private const string ClickSound = "Button - Click";
#else
	    public AudioClip ClickSound;
#endif
		[HideInInspector]
		public new bool enabled = true;


		void Awake ()
		{
			Button button = GetComponent<Button> ( );
			if (button)
			{
				button.onClick.AddListener ( PlayOnCLick );
			}
			else
			{
				Toggle toggle = GetComponent<Toggle> ( );
				if (toggle)
				{
					toggle.onValueChanged.AddListener ( (bool param) => PlayOnCLick ( ) );
				}
			}
		}

		void PlayOnCLick ()
		{
			if (enabled)
			{
				AudioController.Play ( ConstSettings.click_btn_sound ).IgnorePause(true).Start();
				Vibration.Vibrate ( 20 );
			}
		}
	}
}
