﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace PSV
{

	public class DialoguePanel :MonoBehaviour, IExternalLink
	{

		public bool
			contains_external_links = false;

		public float
			anim_duration = 0.2f;

		public Ease
			easing = Ease.InCubic;

		public RectTransform
			pannel;

		public GameObject
			blocker;


		protected bool
			pannel_shown = false;

		protected virtual void OnEnable ()
		{
			ShowPannel ( false, false );
			CanvasBlocker.OnClick += OnBlockerClick;
		}

		protected virtual void OnDisable ()
		{
			CanvasBlocker.OnClick -= OnBlockerClick;
			if (pannel_shown)
			{
				DestroyLink ( );
			}
		}


		protected virtual void OnBlockerClick ()
		{
			//hide panel
			ShowPannel ( false );
		}


		protected virtual void ShowPannel (bool param, bool use_tween = true)
		{
			pannel_shown = param;

			AnimatePanel ( param, use_tween );

			if (pannel_shown)
			{
				CreateLink ( );
			}
			else
			{
				DestroyLink ( );
			}

		}


		protected virtual void AnimatePanel (bool show, bool use_tween)
		{
			if (use_tween)
			{
				if (show)
				{
					ActivatePanelObjects ( show );
				}

                TweenParams tParams = new TweenParams().SetEase( easing ).SetUpdate( true );
                if(!show)
                    tParams.OnComplete ( DeactivatePanelObjects );

				TweenAnimation ( show, tParams );
			}
			else
			{
				NoTweenAction ( show );
				ActivatePanelObjects ( show );
			}
		}


		protected virtual Tweener TweenAnimation (bool show, TweenParams tParams)
		{
			return pannel.DOScale ( show ? 1 : 0, anim_duration ).SetAs ( tParams );
		}

		protected virtual void NoTweenAction (bool show)
		{
			pannel.localScale = show ? Vector3.one : Vector3.zero;
		}





		protected virtual void ActivatePanelObjects (bool param)
		{
			pannel.gameObject.SetActive ( param );
			blocker.SetActive ( param );
		}
        protected void DeactivatePanelObjects()
        {
            ActivatePanelObjects( false );
        }



		#region External link implementation
		public bool IsStatic
		{
			get
			{
				return false;
			}
		}

		void IExternalLinkView.Show( bool param)
		{
			if (!param)
			{
				ShowPannel ( false, false );
			}
		}

		public void CreateLink ()
		{
			if (contains_external_links)
			{
				ExternalLinksManager.AddLink ( this );
			}
		}

		public void DestroyLink ()
		{
			if (contains_external_links)
			{
                ExternalLinksManager.DeleteLink ( this );
			}
		}

		#endregion
	}
}
