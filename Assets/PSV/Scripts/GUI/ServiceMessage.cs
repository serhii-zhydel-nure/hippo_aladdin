﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace PSV
{
    public partial class ServiceMessage : AgentObject<ServiceMessage>
    {
        private const string prefab_path = "ServiceMessage";

        [SerializeField]
        private Text message;
        private bool is_empty_prefab = false;

        /// <summary>
        /// Show system message
        /// </summary>
        /// <param name="duration">For 0 duration need call manually Hide</param>
        public static ServiceMessage Show( string message, float duration = 0.0f )
        {
            if (CheckUIAgent( prefab_path ))
            {
                agent.gameObject.SetActive( true );
                agent.message.text = message;
                agent.CancelInvoke();
                if (duration > 0.0f)
                    agent.HideDelay( duration );
                return agent;
            }
            else
            {
                CheckAgent();
                agent.is_empty_prefab = true;
                return agent;
            }
        }

        public static void Hide( float delay = 0.0f )
        {
            if (agent && !agent.is_empty_prefab)
            {
                if (delay > 0.0f)
                {
                    agent.HideDelay( delay );
                }
                else
                {
                    agent.HideMessage();
                }
            }
        }

        private void HideDelay( float delay )
        {
            Invoke( "HideMessage", delay );
        }

        private void HideMessage()
        {
            gameObject.SetActive( false );
        }

        public void SetPosition( TextAnchor anchor )
        {
            if (is_empty_prefab)
                return;
            Vector2 value = new Vector2( ( ( int )anchor % 3 ) * 0.5f, 1.0f - ( ( int )anchor / 3 ) * 0.5f );
            RectTransform rect_tran = GetComponent<RectTransform>();
            rect_tran.pivot = value;
            rect_tran.anchorMax = value;
            rect_tran.anchorMin = value;
        }

        public void SetAlligment( TextAnchor anchor )
        {
            if (is_empty_prefab)
                return;
            message.alignment = anchor;
        }

        public void SetMessage( string message )
        {
            if (!is_empty_prefab)
                this.message.text = message;
        }
    }
}