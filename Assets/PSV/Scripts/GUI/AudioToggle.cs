﻿#define BRAND_NEW_PROTOTYPE
#define Prototype_4_2_OR_NEWER

using UnityEngine;
using System.Collections;

namespace PSV
{
    public class AudioToggle : ToggleButton
    {
        public enum Toggles
        {
            Music,
            Sounds,
            Vibro,
        }

        public Toggles ToggleType;

        private bool active = false;

        override protected void Awake()
        {
            base.Awake();
            if (ToggleType == Toggles.Music)
            {
                gameObject.SetActive( false );
            }
#if UNITY_IPHONE
		    if (ToggleType == Toggles.Vibro)
		    {
			    gameObject.SetActive ( false );
		    }
#endif
        }

        void OnEnable()
        {
            SetValues();
        }

        void SetValues()
        {
            active = false;
            bool param = false;
            switch (ToggleType)
            {
                case Toggles.Music:
                {
#if BRAND_NEW_PROTOTYPE
#if Prototype_4_2_OR_NEWER
                    param = AudioController.GetGroup( StreamGroup.MUSIC ).IsMuted();
#else
                    param = !GameSettings.IsMusicEnabled();
#endif
#else
					param =	!AudioController.IsMusicEnabled();
#endif
                    break;
                }
                case Toggles.Sounds:
                {
#if BRAND_NEW_PROTOTYPE
#if Prototype_4_2_OR_NEWER
                    param = AudioController.GetGroup( StreamGroup.FX ).IsMuted();
#else
                    param = !GameSettings.IsSoundsEnabled();
#endif
#else
					param =	!AudioController.IsSoundsEnabled();
#endif
                    break;
                }
                case Toggles.Vibro:
                {
#if BRAND_NEW_PROTOTYPE
#if Prototype_4_2_OR_NEWER
                    param = !Vibration.IsEnabled();
#else
                    param = !GameSettings.IsVibroEnabled();
#endif
#endif
                    break;
                }
            }


            //this stuff is done to avoid listeners from catching init changes events
            SetToggle( param );
            active = true;
        }

        override protected void OnValueChanged( bool param )
        {
            //Debug.Log("Toggle action = " + ToggleType.ToString());
            if (active)
            {
                switch (ToggleType)
                {
//                    case Toggles.Music:
//                    {
//#if BRAND_NEW_PROTOTYPE
//                        GameSettings.EnableMusic( !param );
//#else
//						AudioController.EnableMusic ( !param );
//#endif
//                        break;
//                    }
                    case Toggles.Sounds:
                    {
#if BRAND_NEW_PROTOTYPE
#if Prototype_4_2_OR_NEWER
                        AudioController.MuteAllStreams( param );
#else
                        GameSettings.EnableSounds( !param );
#endif
#else
						AudioController.EnableSounds ( !param );
#endif
                        break;
                    }
                    case Toggles.Vibro:
                    {
#if BRAND_NEW_PROTOTYPE
#if Prototype_4_2_OR_NEWER
                        Vibration.SetEnabled( !param );
#else
                        GameSettings.EnableVibro( !param );
#endif
#endif
                        break;
                    }
                }
            }
        }

    }
}