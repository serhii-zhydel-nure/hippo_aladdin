﻿using UnityEngine;
using UnityEngine.UI;
using PSV;

namespace PSV
{
    [RequireComponent( typeof( Button ) )]
    public class GoToSceneButton : ButtonClickHandler
    {
        public Scenes target;

        protected override void OnButtonClick()
        {
            SceneLoader.SwitchToScene( target );
        }

    }
}