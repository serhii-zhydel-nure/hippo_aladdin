﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public class EndlessRotation : MonoBehaviour
    {
        [SerializeField]
        private Vector3 current_speed = Vector3.forward;

        private Quaternion rotate_per_frame;

        private void Awake()
        {
            speed = current_speed;
        }

        private void Update()
        {
            transform.localRotation *= rotate_per_frame;
        }

        public Vector3 speed
        {
            get
            {
                return current_speed;
            }
            set
            {
                current_speed = value;
                rotate_per_frame = Quaternion.Euler( current_speed );
            }
        }
    }
}