﻿
namespace PSV
{

	public class PauseToggleButton :ButtonClickHandler
	{

		protected override void OnButtonClick ()
		{
			PauseManager.TogglePause ( );
		}
	}
}
