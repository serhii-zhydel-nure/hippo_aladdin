﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

namespace PSV
{
    [RequireComponent( typeof( RectTransform ) )]
    public partial class WarningPanel : AgentObject<WarningPanel>, IExternalLinkView, IPointerClickHandler
    {
        private const string prefab_path = "WarningPanel";

        [SerializeField]
        private RectTransform panel;
        [SerializeField]
        private Text tigle_text;
        [SerializeField]
        private Text warning_text;
        [SerializeField]
        private Toggle toggle;
        [SerializeField]
        private Text toggle_text;

        [SerializeField]
        private Button accept_button;
        [SerializeField]
        private Button cancel_button;

        private bool is_empty_prefab = false; // service flag for prefab not found

        private System.Action click_on_fade;

        public static bool IsShown
        {
            get
            {
                return agent;
            }
        }

        /// <summary>
        /// Show warning message panel.
        /// </summary>
        /// <param name="layer_above_fade">For True - panel show above fade screen.</param>
        public static WarningPanel Show( string message, bool layer_above_fade )
        {
            if (CheckUIAgent( prefab_path, !layer_above_fade ))
            {
                agent.SetMessage( message );
                agent.toggle.gameObject.SetActive( false );

                agent.panel.DOAnchorPos( Vector2.zero, 0.5f ).SetEase( Ease.OutElastic ).SetUpdate( true );
                agent.accept_button.onClick.RemoveAllListeners();
                agent.cancel_button.onClick.RemoveAllListeners();
                ExternalLinksManager.AddLink( agent );
                return agent;
            }
            else
            {
                CheckAgent();
                agent.is_empty_prefab = true;
                return agent;
            }
        }

        public static WarningPanel ShowUpdateApplication()
        {
            string message;
            string title;
            if (Languages.GetLanguage() == Languages.Language.Russian)
            {
                message = "Пожалуйста\nобновите игру!";
                title = "Вышло важное обновление!";
            }
            else
            {
                message = "Please update!";
                title = "Update is available!";
            }
            return Show( message, true )
                .SetTitle( title )
                .OnAccept( OnAcceptUpdate, false );
        }

        public static WarningPanel ShowWarningTryLater( bool layer_above_fade = true )
        {
            if (agent)
                return agent;
            string message;
            if (Languages.GetLanguage() == Languages.Language.Russian)
                message = "Ой, что-то сломалось. Пожалуйста, повторите попытку позже.";
            else
                message = "Oh, something broke. Please try again later.";
            return Show( message, layer_above_fade );
        }

        public static void Hide()
        {
            if (agent)
            {
                agent.HideMessage();
            }
        }

        public static bool TryGetToggleValue( ref bool value )
        {
            if (agent && !agent.is_empty_prefab)
            {
                value = agent.toggle.isOn;
                return true;
            }
            return false;
        }

        #region Params
        public WarningPanel SetTitle( string message )
        {
            if (!is_empty_prefab)
            {
                tigle_text.gameObject.SetActive( true );
                tigle_text.text = message;
            }
            return this;
        }
        public WarningPanel SetMessage( string message )
        {
            if (!is_empty_prefab)
                warning_text.text = message;
            return this;
        }

        public WarningPanel SetToggle( string toggle_message, UnityAction<bool> on_toggle = null, bool def_value = true )
        {
            if (is_empty_prefab)
                return this;
            if (string.IsNullOrEmpty( toggle_message ))
            {
                toggle.gameObject.SetActive( false );
            }
            else
            {
                toggle.gameObject.SetActive( true );
                toggle_text.text = toggle_message;
                toggle.onValueChanged.RemoveAllListeners();
                toggle.isOn = def_value;
                if (on_toggle != null)
                    toggle.onValueChanged.AddListener( on_toggle );
            }
            return this;
        }

        public WarningPanel OnAccept( UnityAction on_accept, bool close_on_accept = true )
        {
            if (is_empty_prefab)
                return this;
            accept_button.gameObject.SetActive( true );
            if (on_accept != null)
                accept_button.onClick.AddListener( on_accept );
            if (close_on_accept)
                accept_button.onClick.AddListener( HideMessage );
            return this;
        }

        public WarningPanel OnCancel( UnityAction on_cancel )
        {
            if (is_empty_prefab)
                return this;
            cancel_button.gameObject.SetActive( true );
            if (on_cancel != null)
                cancel_button.onClick.AddListener( on_cancel );
            cancel_button.onClick.AddListener( HideMessage );
            return this;
        }

        public WarningPanel OnFadeClick( System.Action on_fade_click, bool close_on_click )
        {
            if (click_on_fade != null)
                click_on_fade -= Hide;
            click_on_fade += on_fade_click;
            if (close_on_click)
                click_on_fade += Hide;
            return this;
        }
        #endregion

        public void HideMessage()
        {
            ExternalLinksManager.DeleteLink( this );
            if (is_empty_prefab)
                Object.Destroy( this );
            else
                Object.Destroy( gameObject );
        }


        /// <summary>
        /// Allow Android platform only. For other simple hide panel.
        /// </summary>
        private static void OnAcceptUpdate()
        {
            if (Application.platform == RuntimePlatform.Android)
                Application.OpenURL( ConstSettings.GetApplicationMarketURL() );
            else
                Hide();
        }

        public static void ApplicationQuit()
        {
            Application.Quit();
        }


        public bool IsStatic { get { return false; } }

        public void Show( bool param )
        {
            gameObject.SetActive( param );
        }

        public void OnPointerClick( PointerEventData eventData )
        {
            if (click_on_fade != null)
                click_on_fade();
        }
    }
}