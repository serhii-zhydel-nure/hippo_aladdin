﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace PSV
{
    [RequireComponent( typeof( Graphic ) )]
    [RequireComponent( typeof( CanvasGroup ) )]
    public partial class ServiceFade : AgentObject<ServiceFade>
    {
        private const string prefab_path = "FadeScreen";

        private CanvasGroup fade_group;     //Image component of fade_screen
        private GameObject[] children;

        private Color default_Color;
        private Graphic background;

        [Obsolete("Better use static methods with out Instance.")]
        private static ServiceFade Instance
        {
            get
            {
                CheckUIAgent( prefab_path, true );
                return agent;
            }
        }

        private new void Awake()
        {
            if (agent)
            {
                UnityEngine.Object.Destroy( this );
                return;
            }

            agent = this;

            children = new GameObject[transform.childCount];
            for (int i = 0; i < children.Length; i++)
            {
                children[i] = transform.GetChild( i ).gameObject;
            }

            fade_group = GetComponent<CanvasGroup>();
            background = GetComponent<Graphic>();
            default_Color = background.color;

            gameObject.SetActive( false );
        }


        private void OnEnable()
        {
            EnableChildren( false );
            SceneLoader.OnLoadProgressChange += OnLoadProgressChange; // Unsubscribe on first call
        }

        public static void FadeScreen( float target_alpha, float duration, Action callback )
        {
            if (CheckUIAgent( prefab_path, true ))
            {
                agent.background.color = agent.default_Color;
                CoroutineHandler.Start( agent.fade_group.FadeAction( target_alpha, duration, callback ) );
            }
            else if (callback != null)
            {
                callback();
            }
        }

        public static void FadeScreen( float target_alpha, float duration, Action callback, Color background_color)
        {
            if (CheckUIAgent( prefab_path, true ))
            {
                agent.background.color = background_color;
                CoroutineHandler.Start( agent.fade_group.FadeAction( target_alpha, duration, callback ) );
            }
            else if (callback != null)
            {
                callback();
            }
        }

        /// <summary>
        /// Set fade animation like Scene transition
        /// </summary>
        public static void SceneTransition( bool open, Action callback, float fade_time )
        {
            FadeScreen( open ? 0.0f : 1.0f, fade_time, callback );
        }

        /// <summary>
        /// Set fade animation like Scene transition
        /// </summary>
        [Obsolete( "Now use extension method for CanvasGroup.FadeAction", true )]
        public static void SceneTransition( bool open, Action callback, CanvasGroup fade_group, float fade_time ) { }

        private void OnLoadProgressChange( float progress )
        {
            if (progress > 0.0f && children.Length > 0 && !children[0].activeSelf)
            {
                EnableChildren( true );
                SceneLoader.OnLoadProgressChange -= OnLoadProgressChange;
            }
        }

        private void EnableChildren( bool param )
        {
            for (int i = 0; i < children.Length; i++)
            {
                children[i].SetActive( param );
            }
        }
    }
}