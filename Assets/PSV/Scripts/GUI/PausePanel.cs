﻿using UnityEngine;

namespace PSV
{
    public class PausePanel : DialoguePanel
    {
        public bool show_on_esc = true;

        private bool ignore_application_pause = true;




        protected override void OnEnable()
        {
            //Debug.Log ( "PausePanel. OnEnable" );

            base.OnEnable();

            ShowPannel( PauseManager.IsPaused() );

            PauseManager.OnPause += OnPause;
            if (show_on_esc)
            {
                KeyListener.AddKey( KeyCode.Escape );
                KeyListener.OnKeyPressed += KeyListener_OnKeyPressed;
            }
        }


        protected override void OnDisable()
        {
            base.OnDisable();
            PauseManager.OnPause -= OnPause;
            if (PauseManager.IsPaused())
            {
                PauseManager.SetPause( false );
            }
            if (show_on_esc)
            {
                KeyListener.OnKeyPressed -= KeyListener_OnKeyPressed;
            }
        }


        private void KeyListener_OnKeyPressed( KeyCode key )
        {
            if (key == KeyCode.Escape)
            {
                PauseManager.TogglePause();
            }
        }


        private void OnPause( bool pause )
        {
            //Debug.Log ( "PausePanel. OnPause " + pause );
            ShowPannel( pause );
        }


        protected override void OnBlockerClick()
        {
            //base.OnBlockerClick ( );
            PauseManager.SetPause( false );
        }


        private void OnApplicationPause( bool pause )
        {
            if (!ignore_application_pause && pause)
            {
                PauseManager.SetPause( true );
            }
        }

        public void IgnoreApplicationPause( bool param )
        {
            ignore_application_pause = param;
        }

    }
}