﻿using UnityEngine;
using System.Collections;

namespace PSV
{
	public class OpenURL :ButtonClickHandler
	{

		public string
			URL = "http://www.psvgamestudio.com/";


		void Action ()
		{

#if UNITY_WEBGL
        Application.ExternalEval ( "window.open(\"" + URL + "\")" );
#else
			Application.OpenURL ( URL );
#endif
		}

		protected override void OnButtonClick ()
		{
			Action ( );
		}

	}
}