﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ChangeLog
/// - fixed centering pivot
/// - rectTransform component is stored too
/// </summary>
/// 
namespace PSV
{
    [RequireComponent(typeof( Toggle ) )]
    [RequireComponent( typeof( ButtonClickSound ) )]
    public class ToggleButton :MonoBehaviour
	{

		protected ButtonClickSound
			clicker;
		protected Toggle
			toggle;
		protected RectTransform
			rectTransform;

		public bool
			center_pivot = true;

		virtual protected void Awake ()
		{
			rectTransform = GetComponent<RectTransform> ( );
			toggle = GetComponent<Toggle> ( );
			toggle.onValueChanged.AddListener ( OnValueChanged );
			clicker = GetComponent<ButtonClickSound> ( );
			Animator anim = GetComponent<Animator> ( );
			if (anim != null)
			{
				anim.updateMode = AnimatorUpdateMode.UnscaledTime;
			}
			if (center_pivot)
			{
				rectTransform.CenterPivot ( );
			}
		}

		virtual protected void OnValueChanged (bool val)
		{

		}

		protected void SetToggle (bool param)
		{
			EnableClicker ( false );
			toggle.isOn = param;
			EnableClicker ( true );
		}

		void EnableClicker (bool param)
		{
			if (clicker != null)
			{
				clicker.enabled = param;
			}
		}
	}
}