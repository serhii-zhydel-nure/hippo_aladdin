﻿
namespace PSV
{

	public class PauseButton :ButtonClickHandler
	{

		public bool pause = false;

		protected override void OnButtonClick ()
		{
			PauseManager.SetPause ( pause );
		}

	}
}
