﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace PSV.Localization
{
#if ASSET_BUNDLES_STORAGE
    [RequireComponent( typeof( Image ) )]
    public class LanguageImage : MonoBehaviour
    {
        public string sprite_name;

        private void Awake()
        {
            GetComponent<Image>().sprite = LocalizationManager.GetSprite( sprite_name );
            Destroy( this );
        }
    }
#endif
}