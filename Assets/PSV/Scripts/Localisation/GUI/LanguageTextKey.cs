﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace PSV.Localization
{
    [RequireComponent(typeof(Text))]
	public class LanguageTextKey : MonoBehaviour 
	{
        public string text_key;

        private void OnEnable()
        {
            GetComponent<Text>().text = LocalizationManager.GetString( text_key );
        }
    }
}