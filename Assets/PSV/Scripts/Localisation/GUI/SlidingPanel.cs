﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

namespace PSV.Localization
{
	public class SlidingPanel :MonoBehaviour
	{
		public bool
			default_hidden = true;

		protected bool
			sliderShown = false,
			isMoving = false;

		protected RectTransform
			Slider;
		public RectTransform
			Rotator;

		private ScrollRect
		   scroller;


		[Tooltip ( "Set y = 1 to slide up or y = -1 to slide down" )]
		public Vector2
			slide_direction;

		public float
			sliderDeuration = 0.5f;

		private Sequence
			seq = null;

		public LangToggle []
			children;


		void Awake ()
		{
			Slider = GetComponent<RectTransform> ( );
			scroller = GetComponentInChildren<ScrollRect> ( );

		}


		virtual public void Init ()
		{
			if (scroller != null)
			{
				children = scroller.content.GetComponentsInChildren<LangToggle> ( false );
			}
			else
			{
				Debug.Log ( "Scroller is null, not lang panel" );
			}
			if (Rotator != null)
			{
				Rotator.localRotation = Quaternion.identity;
			}
			isMoving = false;
			sliderShown = !default_hidden;

			if (Slider != null && default_hidden)
			{
				Slider.localPosition = GetSlidingTarget ( sliderShown, Slider );
			}

		}


		void Start ()
		{
			Init ( );
		}


		public void ShowMenu ()
		{
			if (seq != null && isMoving)
			{
				seq.Kill ( );
			}
			sliderShown = !sliderShown;
			if (sliderShown)
			{
				if (scroller != null && children != null && children.Length > 0) //scrolls down to selected language
				{
					Languages.Language current_lng = Languages.GetLanguage ( );
					int active_button = -1;
					for (int i = 0; i < children.Length; i++)
					{
						if (children [i].lang == current_lng)
						{
							active_button = i;
							break;
						}
					}
					if (active_button >= 0)
					{
						StartCoroutine ( ScrollToActive ( GetScrollPos ( active_button ) ) );
					}
				}
			}
			isMoving = true;
			seq = DOTween.Sequence ( );
			if (Rotator != null)
			{
				seq.Join ( Rotator.DOLocalRotate ( sliderShown ? new Vector3 ( 0, 0, 180f ) : Vector3.zero, sliderDeuration ) );
			}
			if (Slider != null)
			{
				//				Vector2 sliderPos = sliderShown ? Vector2.zero : sliderOffset;
				Slider.DOLocalMove ( GetSlidingTarget ( sliderShown, Slider ), sliderDeuration, true ).SetEase ( Ease.OutSine );
			}
			seq.AppendCallback ( () =>
			{
				isMoving = false;
			} );
		}

		Vector2 GetSlidingTarget (bool show, RectTransform t)
		{
			return new Vector2 ( show ? t.rect.width * slide_direction.x : 0, show ? t.rect.height * slide_direction.y : 0 );
		}

		float GetScrollPos (int active_item)
		{
			float res;
			if (children.Length <= 1)
			{
				res = 1;
			}
			else
			{
				res = active_item / ((float) children.Length - 1f);
			}
			return res;
		}


		IEnumerator ScrollToActive (float pos)
		{
			yield return new WaitForEndOfFrame ( );
			//vertical scroll means by lowest item 0, horizontal scroll means by right end 1 
			if (scroller.vertical)
			{
				scroller.verticalNormalizedPosition = 1 - pos;
			}
			if (scroller.horizontal)
			{
				scroller.horizontalNormalizedPosition = pos;
			}
		}


		//public bool OnEsc ()
		//{
		//    if (sliderShown)
		//    {
		//        ShowMenu ( );
		//        return true;
		//    }

		//    return false;
		//}
	}
}