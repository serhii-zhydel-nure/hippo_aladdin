﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

namespace PSV.Localization
{
	[RequireComponent ( typeof ( Image ) )]
	public class LangButton :MonoBehaviour
	{
		public Sprite []
			lang_array;    //set here lang sprites in strong order of their index in lang_order


		private Dictionary<Languages.Language, int> lang_icons;


		private Image
			img;

		public Transform
			toggles_parrent;
		public GameObject
			toggle_prefab;



		[ContextMenu ( "SpawnAllToggles" )]
		void SpawnToggles ()
		{
			if (toggles_parrent != null && toggle_prefab != null)
			{
				if (lang_icons == null)
				{
					ParseIcons ( );
				}

				for (int i = toggles_parrent.childCount - 1; i >= 0; i--)
				{
#if UNITY_EDITOR
					DestroyImmediate ( toggles_parrent.GetChild ( i ).gameObject );
#else
				Destroy ( toggles_parrent.GetChild ( i ).gameObject );
#endif
				}

				ToggleGroup _tg = toggles_parrent.GetComponent<ToggleGroup> ( );

				foreach (var item in lang_icons)
				{
					GameObject toggle = Instantiate ( toggle_prefab );
					toggle.name = item.Key.ToString ( );
					toggle.transform.SetParent ( toggles_parrent );
					toggle.transform.localScale = Vector3.one;
					Toggle _t = toggle.GetComponent<Toggle> ( );
					if (_t != null)
					{
						Image _img = _t.targetGraphic as Image;
						if (_img != null)
						{
							_img.sprite = GetLangSprite ( item.Key );
						}
						LangToggle _lt = toggle.GetComponent<LangToggle> ( );
						if (_lt != null)
						{
							_lt.lang = item.Key;
						}
						if (_tg != null)
						{
							_t.group = _tg;
						}
					}
				}
			}
		}



		void Awake ()
		{
			img = GetComponent<Image> ( );

			ParseIcons ( );
		}


		void ParseIcons ()
		{
			lang_icons = new Dictionary<Languages.Language, int> ( );
			for (int i = 0; i < lang_array.Length; i++)
			{
				if (lang_array [i] != null)
				{
					Languages.Language icon_lng = (Languages.Language) System.Enum.Parse ( typeof ( Languages.Language ), lang_array [i].name );
					if (!lang_icons.ContainsKey ( icon_lng ))
					{
						lang_icons.Add ( icon_lng, i );
					}
					else
					{
						Debug.Log ( "Key duplicate " + icon_lng.ToString ( ) + " icon name : " + lang_array [i].name );
					}
				}
			}
		}


		void OnEnable ()
		{
			Languages.Language current_lang = Languages.GetLanguage ( );
			SetSprite ( current_lang );
			Languages.OnLangChange += SetSprite;
		}


		void OnDisable ()
		{
			Languages.OnLangChange -= SetSprite;
		}



		void SetSprite (Languages.Language lang)
		{
			if (lang_icons.ContainsKey ( lang ))
			{
				img.sprite = GetLangSprite ( lang );
			}
		}

		Sprite GetLangSprite (Languages.Language lang)
		{
			Sprite res = null;
			if (lang_icons.ContainsKey ( lang ))
			{
				res = lang_array [lang_icons [lang]];
			}
			if (res == null)
			{
				Debug.Log ( "No lang icon for " + lang );
			}
			return res;
		}

	}
}