﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace PSV.Localization
{
    [RequireComponent( typeof( Text ) )]
    public class LanguageText : MonoBehaviour
    {
        public enum AliasNames
        {
            play_btn,
            disable_ads_btn,
            more_games_btn,
            rate_btn,
            game_label,
            exit_label,
            best_score,
            score,
            achievements,
            loc_complete,
            loc_achievements,
            loc_more_games,
            loc_supermarket,
            loc_aircontroller,
            loc_online_games,
            loc_luntik,
            loc_ivan,
            loc_soccer,
        }

        public AliasNames alias;

        private void OnEnable()
        {
            GetComponent<Text>().text = LocalizationManager.GetString( alias.ToString() );
        }
    }
}
