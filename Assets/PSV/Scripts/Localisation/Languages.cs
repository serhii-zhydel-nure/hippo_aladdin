﻿#define BRAND_NEW_PROTOTYPE

using UnityEngine;
using System.Collections.Generic;
using PSV;
using System;

/* ChangeLog:
 * CleanUp
 * Next Language now gives next available language in dictionary
 */

public static class Languages
{
    private const string settings_lang_pref = "CurrentLanguage";
    private static Language locale_lang;

#if ASSET_BUNDLES_STORAGE
    public static event Action<Language> OnLangChange
    {
        add { LocalizationManager.OnLanguageLoaded += value; }
        remove { LocalizationManager.OnLanguageLoaded -= value; }
    }
#else
    public static event Action<Language> OnLangChange;
#endif

    public enum Language
    {
        None = 0,
        Arabic = SystemLanguage.Arabic,
        Chinese = SystemLanguage.Chinese,
        Czech = SystemLanguage.Czech,
        Danish = SystemLanguage.Danish,
        Dutch = SystemLanguage.Dutch,
        English = SystemLanguage.English,
        Finnish = SystemLanguage.Finnish,
        French = SystemLanguage.French,
        German = SystemLanguage.German,
        Greek = SystemLanguage.Greek,
        Italian = SystemLanguage.Italian,
        Japanese = SystemLanguage.Japanese,
        Norwegian = SystemLanguage.Norwegian,
        Polish = SystemLanguage.Polish,
        Portuguese = SystemLanguage.Portuguese,
        Romanian = SystemLanguage.Romanian,
        Russian = SystemLanguage.Russian,
        Spanish = SystemLanguage.Spanish,
        Swedish = SystemLanguage.Swedish,
        Turkish = SystemLanguage.Turkish,
        Hindi = 50,
        Hebrew = SystemLanguage.Hebrew,
        Indonesian = SystemLanguage.Indonesian,
        Korean = SystemLanguage.Korean,
        Thai = SystemLanguage.Thai,
        Ukrainian = SystemLanguage.Ukrainian,
        Catalan = SystemLanguage.Catalan,
        Belarusian = SystemLanguage.Belarusian,
    };
    
    public static Language GetOverrideLanguage(Language target )
    {
        switch (target)
        {
            case Language.Ukrainian:
            case Language.Belarusian:
            return Language.Russian;
            case Language.Catalan:
            return Language.Spanish;
            default:
            return target;
        }
    }

    public static Language[] languages
    {
        get { return PSVSettings.settings.available_languages; }
    }

    public static bool IsAvailable( Language lang )
    {
        foreach (Language ready_lang in languages)
        {
            if (ready_lang == lang)
                return true;
        }
        return false;
    }
    
    public static Language DetectLanguage()
    {
        Language settingsLang = ( Language )PlayerPrefs.GetInt( settings_lang_pref, ( int )Language.None );
        if (settingsLang != Language.None)
        {
            return settingsLang;
        }
        else
        {
            Language lang = Utils.GetSystemLanguage();
            if (!IsAvailable( lang ))
            {
                lang = GetOverrideLanguage( lang );
            }
            if (IsAvailable( lang ))
            {
                return lang;
            }
            else
            {
                return Language.English;
            }
        }
    }

    public static void SetLanguageByName( string lname )
    {
        foreach (Language ready_lang in languages)
        {
            if (ready_lang.ToString() == lname)
            {
                SetLanguage( ready_lang );
                break;
            }
        }
    }

    public static void SetLanguage( Language lang )
    {
        Debug.Log( "Languages: SetLanguage to " + lang.ToString() );

        if (IsAvailable( lang ))
        {
            locale_lang = lang;
#if BRAND_NEW_PROTOTYPE
            PlayerPrefs.SetInt( settings_lang_pref, ( int )locale_lang );
            LocalizationManager.InitLanguage();
            NextFrameCall.SavePrefs();
#else
            PlayerPrefs.SetInt ( "CurrentLanguage", (int) locale_lang );
#endif
#if !ASSET_BUNDLES_STORAGE
            if (OnLangChange != null)
            {
                OnLangChange( locale_lang );
            }
#endif
        }
        else
        {
            Debug.LogError( "Couldn't set language " + lang.ToString() );
        }
    }

    public static Language GetNextLanguage()
    {
        Language
            curr,
            first,
            res;
        res = first = curr = GetLanguage();
        int next = -1;
        int curr_ind = -1;
        foreach (Language ready_lang in languages)
        {
            curr_ind++;
            if (curr_ind == 0)
            {
                first = ready_lang;
            }
            if (ready_lang == curr)
            {
                next = curr_ind + 1;
                if (next >= languages.Length)
                {
                    res = first;
                    break;
                }
            }
            if (next == curr_ind)
            {
                res = ready_lang;
                break;
            }
        }

        return res;
    }

    public static string GetLanguageName()
    {
        return GetLanguage().ToString();
    }

    public static Language GetLanguage()
    {
        if (locale_lang == Language.None)
        {
            SetLanguage( DetectLanguage() );
        }
        return locale_lang;
    }

}
