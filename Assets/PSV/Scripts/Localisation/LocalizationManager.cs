﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV.Localization;
using Lang = Languages.Language;

namespace PSV
{
    public static partial class LocalizationManager
    {
        #region Fields

        private static Dictionary<string, AudioClip> nonlanguage_sounds = new Dictionary<string, AudioClip>();
        private static Dictionary<string, AudioClip> language_sounds = new Dictionary<string, AudioClip>();

        public static Lang loaded_text_lang;
        public static LocalizationText localization_text;

#if ASSET_BUNDLES_STORAGE
        public static System.Action<Lang> OnLanguageLoaded;

        private static AssetBundle language_bundle;
        private static Lang loaded_lang;
        private static Lang target_lang;

        private static AssetBundle nonlanguage_bundle;

        public static bool is_ready_lang
        {
            get
            {
                return loaded_lang == Languages.GetLanguage();
            }
        }
#else
        private const string multilanguage_path = "Sounds/MultiLanguage/";
        private const string nonlanguage_path = "Sounds/NonLanguage/";
#endif

        #endregion

        [AwakeStatic]
        public static void StaticAwake()
        {
            AwakeStaticAttribute.Done( typeof( LocalizationManager ) );
            Languages.GetLanguage();
            InitNonLanguage();
        }

        /// <summary>
        /// Initialization for language resources.
        /// Background loading of asset bundles.
        /// </summary>
        public static void InitLanguage()
        {
            Lang required_lang = Languages.GetLanguage();
            InitLanguageText( required_lang );
            InitLanguage( required_lang );
        }

#if ASSET_BUNDLES_STORAGE

        /// <summary>
        /// Initialization for language resources.
        /// Background loading of asset bundles.
        /// </summary>
        public static void InitLanguage( Lang required_lang, Scenes preload_scene = Scenes.None )
        {
            if (target_lang == required_lang)
                return;

            LoadAssetBundle.AbortLoad( GetLangBundlePath( target_lang ) );
            target_lang = required_lang;

            if (loaded_lang != required_lang)
            {
                new LoadAssetBundle( GetLangBundlePath( required_lang ) )
                    .OnComplete( InitLanguage )
                    .PreloadOnScene( preload_scene );
            }
        }

        /// <summary>
        /// Initialization for language resources. 
        /// </summary>
        public static void InitLanguage( AssetBundle loaded_bundle )
        {
            if (!loaded_bundle)
            {
                LogError( "InitLanguage failure - AssetBundle is null!" );
                if (loaded_lang == Lang.None)
                    loaded_lang = Languages.DetectLanguage();
                Languages.SetLanguage( loaded_lang );
                return;
            }

            // Unload previous resources 
            language_sounds.Clear();
            if (language_bundle)
                language_bundle.Unload( true );

            language_bundle = loaded_bundle;
            loaded_lang = target_lang;

            BufferAudio( language_bundle, true );

            if (OnLanguageLoaded != null)
                OnLanguageLoaded( loaded_lang );
        }
        
        /// <summary>
        /// Free memory. All language audio & Sprites clear. For restore sounds call <see cref="InitLanguage(Lang, Scenes)"/>
        /// </summary>
        public static void LanguageBundleFreeMemory()
        {
            language_sounds.Clear();
            language_bundle.Unload( true );
        }

        /// <summary>
        /// Initialization for non language resources.
        /// Background loading of asset bundles.
        /// Should be called once!
        /// </summary>
        public static void InitNonLanguage( Scenes preload_scene = Scenes.None )
        {
            if (nonlanguage_bundle == null)
            {
                new LoadAssetBundle( PSVSettings.settings.non_language_bundle_path.Path() )
                    .OnComplete( InitNonLanguage )
                    .PreloadOnScene( preload_scene );
            }
        }

        /// <summary>
        /// Initialization for non language resources.
        /// </summary>
        public static void InitNonLanguage( AssetBundle loaded_bundle )
        {
            if (!loaded_bundle)
            {
                LogError( "InitNonLanguage failure - AssetBundle is null!" );
                return;
            }

            // Unload previous resources 
            nonlanguage_sounds.Clear();
            if (nonlanguage_bundle)
                nonlanguage_bundle.Unload( true );

            nonlanguage_bundle = loaded_bundle;
            BufferAudio( nonlanguage_bundle, false );
        }

        /// <summary>
        /// Get sprite from language and non language bundles.
        /// Combining functions <see cref="TryGetSpriteLang(string, out Sprite)"/> 
        /// and <see cref="TryGetSpriteNonLang(string, out Sprite)"/>.
        /// Return found Sprite or null.
        /// </summary>
        public static Sprite GetSprite( string sprite_name )
        {
            Sprite result;
            if (TryGetSpriteLang( sprite_name, out result ))
            {
                return result;
            }
            else if (TryGetSpriteNonLang( sprite_name, out result ))
            {
                return result;
            }
            else
            {
                LogError( "Sprite [" + sprite_name + "] not found!" );
                return null;
            }
        }

        public static bool TryGetSpriteLang( string sprite_name, out Sprite result )
        {
            if (language_bundle == null)
            {
                Debug.LogWarning( "LocalizationManager: TryGetSpriteLang can't find sprite because Language bundle are not loaded." );
                result = null;
                return false;
            }
            result = language_bundle.LoadAsset<Sprite>( sprite_name );
            return true;
        }

        public static bool TryGetSpriteNonLang( string sprite_name, out Sprite result )
        {
            if (nonlanguage_bundle == null)
            {
                Debug.LogWarning( "LocalizationManager: TryGetSpriteNonLang can't find sprite because NON Language bundle are not loaded." );
                result = null;
                return false;
            }
            result = nonlanguage_bundle.LoadAsset<Sprite>( sprite_name );
            return true;
        }

        public static T LoadAssetLang<T>( string name ) where T : UnityEngine.Object
        {
            return language_bundle.LoadAsset<T>( name );
        }

        public static T LoadAssetNonLang<T>( string name ) where T : UnityEngine.Object
        {
            return nonlanguage_bundle.LoadAsset<T>( name );
        }

        public static AssetBundle GetLangBundle()
        {
            return language_bundle;
        }

        public static AssetBundle GetNonLangBundle()
        {
            return nonlanguage_bundle;
        }

        /// <summary>
        /// URL/Path to asset bundle with selected language resources.
        /// </summary>
        /// <example> https://URL.lang.resources.folder/platform/bundle.extension </example>
        /// For local storage AssetBundles use <see cref="PathToBundle.GetLocalPath()"/>
        private static string GetLangBundlePath( Lang language )
        {
            if (!Languages.IsAvailable( language ))
            {
                return string.Empty;
            }

            switch (language)
            {
                //Example
                //case Lang.Russian:
                //    return "https://URL.lang.resources.folder/platform/bundle.extension";
                default:
                return PSVSettings.settings.language_bundle_path.Path( language );
            }
        }
        
#endif

        #region Localization Text

        public static void InitLanguageText( Lang lang )
        {
            if (PSVSettings.settings.locale_text_format == TextFormat.none)
                return;
            if (loaded_text_lang == lang)
                return;
            if (!Languages.IsAvailable( lang ))
                return;

            new LoadableLangText( PSVSettings.settings.locale_text_path, PSVSettings.settings.locale_text_format, ResultLoadTextHandler, lang )
                .Load( Scenes.None );
        }

        private static void ResultLoadTextHandler( LocalizationText result, bool successfully, Lang lang )
        {
            if (successfully)
            {
                loaded_text_lang = lang;
                localization_text = result;
            }
            else if (loaded_text_lang == 0 && lang != Lang.English) // && is Correct operator. If have not lang text then try load English
            {
                InitLanguageText( Lang.English );
            }
            else
            {
                Log( "Text file for " + lang.ToString() + " not found !" );
            }
        }

        /// <summary>
        /// Get localization string from language bundle.
        /// </summary>
        public static string GetString( string key )
        {
            if (loaded_text_lang == 0)
            {
                if (PSVSettings.settings.locale_text_format == TextFormat.none)
                {
                    LogError( "Localization Text format is not selected! You can select format in Window/PSVSettings/Resource. Impossible to get a string [" + key + "]." );
                }
                else
                {
                    LogError( "Impossible to get a string [" + key + "]. LocalizationText is empty now!" );
                }
                return LocalizationText.empty;
            }
            return localization_text[key];
        }
        #endregion

        #region Localization Audio
#if ASSET_BUNDLES_STORAGE
        private static void BufferAudio( AssetBundle loaded_bundle, bool multilanguage )
        {
            AudioClip[] res = loaded_bundle.LoadAllAssets<AudioClip>();
            if (res == null || res.Length == 0)
            {
                LogError( "AssetBundle[" + loaded_bundle.name + "] have no AudioClips" );
            }
            else
            {
                for (int i = 0; i < res.Length; i++)
                {
                    AddSound( res[i], multilanguage );
                }
            }
        }
#else
        public static void InitLanguage( Lang required_lang )
        {
            language_sounds.Clear();
            string path = multilanguage_path + required_lang.ToString() + "/";
            Object[] res = Resources.LoadAll( path );
            Log( "Loading MultiLanguage from " + path );
            for (int i = 0; i < res.Length; i++)
            {
                AudioClip sound = res[i] as AudioClip;
                AddSound( sound, true );
            }
        }

        public static void InitNonLanguage()
        {
            nonlanguage_sounds.Clear();
            Object[] res = Resources.LoadAll( nonlanguage_path );
            for (int i = 0; i < res.Length; i++)
            {
                AudioClip sound = res[i] as AudioClip;
                AddSound( sound, false );
            }
        }
#endif

        public static void AddSound( AudioClip clip, bool multilanguage )
        {
            Dictionary<string, AudioClip> list;
            if (multilanguage)
                list = language_sounds;
            else
                list = nonlanguage_sounds;
            try
            {
                list.Add( clip.name.ToLower(), clip );
            }
            catch (System.ArgumentException e)
            {
                LogError( "Can't add sound [" + clip.name + "]\n" + e );
            }
        }

        /// <summary>
        /// Get audio clip from language and non language bundles.
        /// Combining functions <see cref="TryGetSoundLang(string, out AudioClip)"/> 
        /// and <see cref="TryGetSoundNonLang(string, out AudioClip)"/>.
        /// Return found AudiopClip or null.
        /// </summary>
        public static AudioClip GetSound( string name )
        {
            name = name.ToLower();
            AudioClip result;
            if (language_sounds.TryGetValue( name, out result ))
            {
                return result;
            }
            else if (nonlanguage_sounds.TryGetValue( name, out result ))
            {
                return result;
            }
            else
            {
                LogError( "Sound [" + name + "] not found!" );
                return null;
            }
        }

        public static bool TryGetSoundLang( string name, out AudioClip result )
        {
            name = name.ToLower();
            return language_sounds.TryGetValue( name, out result );
        }

        public static bool TryGetSoundNonLang( string name, out AudioClip result )
        {
            name = name.ToLower();
            return nonlanguage_sounds.TryGetValue( name, out result );
        }

        /// <summary>
        /// Load Sound to audio list from Resources folder.
        /// </summary>
        [System.Obsolete( "Not recommended for use." )]
        public static void LoadSoundByName( string name, bool multilanguage, string folder, string path = "Sounds/" )
        {
            AudioClip sound;
            if (multilanguage)
            {
                path += "MultiLanguage/" + Languages.GetLanguage().ToString() + "/";
                if (!string.IsNullOrEmpty( folder ))
                {
                    path += folder + "/";
                }
                path += name;
                sound = Resources.Load<AudioClip>( path );
            }
            else
            {
                path += "NonLanguage/";
                if (!string.IsNullOrEmpty( folder ))
                {
                    path += folder + "/";
                }
                path += name;
                sound = Resources.Load<AudioClip>( path );
            }
            AddSound( sound, multilanguage );
        }

        #endregion


        public static void Log( string message )
        {
            Debug.Log( "LocalizationManager: " + message );
        }

        public static void LogError( string message )
        {
            Debug.LogError( "LocalizationManager: " + message );
        }
    }

}