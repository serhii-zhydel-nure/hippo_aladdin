﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using Lang = Languages.Language;

namespace PSV.Localization
{
    [Serializable]
    public enum TextFormat : byte
    {
        none, json, xml, Obsolete
    }

    [Serializable]
    public class LocalTextData
    {
        [XmlAttribute()]
        public string key;
        [XmlAttribute()]
        public string text;
    }

    [Serializable]
    public class LocalizationText
    {
        public const string empty = "####";
        
        [XmlArrayItem( "String" )]
        public LocalTextData[] data;
        
        public string this[string key]
        {
            get
            {
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i].key == key)
                        return data[i].text;
                }

                LocalizationManager.LogError( "Text with key '" + key + "'  does not exist ." );
                return empty;
            }
        }
        
        public static bool TryParse(string content, TextFormat format, out LocalizationText result, string lang_key = "")
        {
            switch (format)
            {
                case TextFormat.json:
                result = JsonUtility.FromJson<LocalizationText>( content );
                return true;
                case TextFormat.xml:
                var serializer = new XmlSerializer( typeof( LocalizationText ) );
                using (var reader = new System.IO.StringReader( content ))
                {
                    result = serializer.Deserialize( reader ) as LocalizationText;
                    return true;
                }
                case TextFormat.Obsolete:
                List<Dictionary<string, object>> data = CSVReader.Read( content );
                LocalTextData[] text_for_lang = new LocalTextData[data.Count];
                if (data[0].ContainsKey( lang_key ))
                {
                    for (int i = 0; i < data.Count; i++)
                    {
                        text_for_lang[i] = new LocalTextData()
                        {
                            key = data[i]["Keys"].ToString().Trim(),
                            text = data[i][lang_key].ToString().Trim()
                        };
                    }
                    result = new LocalizationText() { data = text_for_lang };
                    return true;
                }
                break;
            }
            result = null;
            return false;
        }
    }
    
}