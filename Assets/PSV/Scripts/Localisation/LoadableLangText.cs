﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;

namespace PSV.Localization
{
    public class LoadableLangText : LoadableObject
    {
        public const string json_format = ".json";
        public const string xml_format = ".xml";

        public delegate void LoadResult( LocalizationText result, bool successfully, Languages.Language lang );

        private TextFormat format;
        private Languages.Language target_lang;
        private LoadResult onDone;
        private bool in_resources;

        public override float progress
        {
            get
            {
                return Mathf.Repeat( Time.time, 1.0f );
            }
        }

        /// <summary>
        /// Load text file from Resource folder or URL.
        /// To end path added Language name.
        /// In path don't specify file format.
        /// </summary>
        public LoadableLangText( string path, TextFormat formatFile, LoadResult onDone, Languages.Language target_lang = 0 ) : base( path )
        {
            if (target_lang != 0 && format != TextFormat.Obsolete)
                path += target_lang.ToString();

            in_resources = !path.StartsWith( "http" );
            if (!in_resources)
            {
                switch (formatFile)
                {
                    case TextFormat.json:
                    path += json_format;
                    break;
                    case TextFormat.xml:
                    path += xml_format;
                    break;
                }
            }
            this.identifier = path;
            this.target_lang = target_lang;
            this.format = formatFile;
            this.onDone = onDone;
        }

        protected override IEnumerator GetInstruction()
        {
            using (WWW downloader = new WWW( identifier ))
            {
                yield return downloader;
                while (!downloader.isDone)
                    yield return null;
                LocalizationText result = null;
                bool successfully = false;
                if (string.IsNullOrEmpty( downloader.error ))
                {
                    string content = downloader.text;
                    if (!string.IsNullOrEmpty( content ))
                    {
                        successfully = LocalizationText.TryParse( content, format, out result, target_lang.ToString() );
                    }
                }
                onDone( result, successfully, target_lang );
            }
        }

        public void Load( Scenes preload_scene )
        {
            if (!in_resources && SceneLoader.IsSceneActive( preload_scene.ToString() ))
            {
                SceneLoader.AddLoadable( this );
                base.LoadAsync();
            }
            else
            {
                this.LoadAsync();
            }
        }

        public new void LoadAsync()
        {
            if (in_resources)
            {
                LocalizationText result = null;
                bool successfully = false;
                TextAsset localeAsset = Resources.Load<TextAsset>( identifier );
                if (localeAsset)
                {
                    successfully = LocalizationText.TryParse( localeAsset.text, format, out result, target_lang.ToString() );
                }
                onDone( result, successfully, target_lang );
            }
            else
            {
                base.SetNecessaryAll().LoadAsync();
            }
        }
    }
}