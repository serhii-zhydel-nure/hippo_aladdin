﻿#if UNITY_EDITOR || UNITY_STANDALONE
using System;
using System.Collections;
using System.Text.RegularExpressions;
#endif
using UnityEngine;

namespace PSV
{
    public class ScreenShotTaker : MonoBehaviour
    {
        private void Awake()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            DontDestroyOnLoad( gameObject );
#else
            Destroy( this );
#endif
        }

#if UNITY_EDITOR || UNITY_STANDALONE
        [Header( "Hotkey to save screenshot in Assets folder" )]
        [SerializeField]
        private KeyCode defaultCaptureHotkey = KeyCode.F11;
        [SerializeField]
        private KeyCode resizedCaptureHotkey = KeyCode.F12;
        [Space( 10.0f )]
        public Resolution[] resolutions = { new Resolution( 1280, 720, 1 ), new Resolution( 1104, 621, 2 ), new Resolution( 1366, 1024, 2 ) };

        [Header( "Allow only for similar aspect ratios" )]
        public bool lockingAspects = false;

        private bool canGetShot = true;

        private void Start()
        {
            KeyListener.AddKey( resizedCaptureHotkey );
            KeyListener.OnKeyPressed += TakeScreenShot;
            KeyListener.AddKey( defaultCaptureHotkey );
            KeyListener.OnKeyPressed += TakeDefaultCreenShot;
        }

        private void OnDestroy()
        {
            KeyListener.RemoveKey( resizedCaptureHotkey );
            KeyListener.OnKeyPressed -= TakeScreenShot;
            KeyListener.RemoveKey( defaultCaptureHotkey );
            KeyListener.OnKeyPressed -= TakeDefaultCreenShot;
        }

        public void TakeScreenShot( KeyCode key )
        {
            if (key == resizedCaptureHotkey && canGetShot)
            {
                canGetShot = false;
                StartCoroutine( ScreenCaptureAction() );
            }
        }

        private void TakeDefaultCreenShot( KeyCode key )
        {
            if (key == defaultCaptureHotkey)
            {
                string shotName = "Sccreenshot_" + DateTime.Now.ToString( "HH-mm-ss" );
                Debug.Log( "Screenshot taken: " + shotName );
                string resolshot = shotName + string.Format( "({0}x{1})", Screen.width, Screen.height ) + ".png";
                ApplyScreenshot( resolshot, 1 );
            }
        }

        private IEnumerator ScreenCaptureAction()
        {
            string shotName = "Sccreenshot_" + DateTime.Now.ToString( "HH-mm-ss" );
            Debug.Log( "Screenshot taken: " + shotName );
#if UNITY_EDITOR
            int defaultResolution = Resolution.GetResolutionIndex();
#else
            ResolutionCheckerStandalone.change_size = false;
            Resolution defaultResolution = Resolution.ScreenResolution();
#endif
            float defaultAspect = Resolution.ScreenAscpect();
            bool isHorizontal = defaultAspect > 1.0f;
            yield return new WaitForEndOfFrame();
            for (int i = 0; i < resolutions.Length; i++)
            {
                if (resolutions[i].SetScreen( defaultAspect, isHorizontal, lockingAspects ))
                {
                    string resolshot = shotName + resolutions[i].ToStringMult( isHorizontal ) + ".png";
                    yield return new WaitForEndOfFrame();
                    ApplyScreenshot( resolshot, resolutions[i].scale );
                    yield return new WaitForEndOfFrame();
                }
            }
#if UNITY_EDITOR
            Resolution.SetScreenResolution( defaultResolution );
#else
            Screen.SetResolution( defaultResolution.width, defaultResolution.height, false );
            ResolutionCheckerStandalone.change_size = true;
#endif
            canGetShot = true;
        }

        private void ApplyScreenshot( string name, int scale )
        {
#if UNITY_2017_2_OR_NEWER
            ScreenCapture.CaptureScreenshot( name, scale );
#else
            Application.CaptureScreenshot( name, scale );
#endif
        }

        [Serializable]
        public struct Resolution
        {
            public int width;
            public int height;
            public int scale;

            public Resolution( int width, int height, int scale )
            {
                this.width = width;
                this.height = height;
                this.scale = scale;
            }

            public override string ToString()
            {
                return string.Format( "({0}x{1})", width, height );
            }
            public string ToString( int multiple )
            {
                return string.Format( "({0}x{1})", width * multiple, height * multiple );
            }
            public string ToStringMult( bool isHorizontal )
            {
                if (isHorizontal)
                    return string.Format( "({0}x{1})", width * scale, height * scale );
                else
                    return string.Format( "({0}x{1})", height * scale, width * scale );
            }

            public static float ScreenAscpect()
            {
                return ( float )Screen.width / Screen.height;
            }

            public static Resolution ScreenResolution()
            {
                return new Resolution( Screen.width, Screen.height, 1 );
            }

            public bool SetScreen( float screenAspect, bool isHorizontal, bool lock_aspects )
            {
                int orientWidth = isHorizontal ? width : height;
                int orientHeight = isHorizontal ? height : width;

                if (lock_aspects)
                {
                    float requiredAspect = ( float )orientWidth / orientHeight;
                    if (Math.Abs( requiredAspect - screenAspect ) > 0.1f)
                    {
                        Debug.LogWarning( string.Format(
                            "The current aspect ratio[{0}] is too different from the required ({1}x{2})[{3}]",
                            screenAspect, orientWidth, orientHeight, requiredAspect ) );
                        return false;
                    }
                }
#if UNITY_EDITOR
                var sizesType = typeof( UnityEditor.Editor ).Assembly.GetType( "UnityEditor.GameViewSizes" );
                var singleType = typeof( UnityEditor.ScriptableSingleton<> ).MakeGenericType( sizesType );
                var instanceProp = singleType.GetProperty( "instance" );
                object gameViewSizesInstance = instanceProp.GetValue( null, null );

                System.Reflection.MethodInfo getGroup = sizesType.GetMethod( "GetGroup" );

                UnityEditor.GameViewSizeGroupType targetView;
#if UNITY_ANDROID
                targetView = UnityEditor.GameViewSizeGroupType.Android;
#elif UNITY_IOS
                targetView = UnityEditor.GameViewSizeGroupType.iOS;
#elif UNITY_STANDALONE
                targetView = UnityEditor.GameViewSizeGroupType.Standalone;
#endif

                var group = getGroup.Invoke( gameViewSizesInstance, new object[] { ( int )targetView } );
                var getDisplayTexts = group.GetType().GetMethod( "GetDisplayTexts" );
                var displayTexts = getDisplayTexts.Invoke( group, null ) as string[];

                Regex r = new Regex( orientWidth + @"\D" + orientHeight );
                for (int i = 0; i < displayTexts.Length; i++)
                {
                    if (r.Match( displayTexts[i] ).Success)
                    {
                        SetScreenResolution( i );
                        return true;
                    }
                }

                int addedSizeId = displayTexts.Length;
                var addCustomSize = getGroup.ReturnType.GetMethod( "AddCustomSize" ); // or group.GetType().
                var gvsType = typeof( UnityEditor.Editor ).Assembly.GetType( "UnityEditor.GameViewSize" );
                var ctor = gvsType.GetConstructor( new Type[] { typeof( int ), typeof( int ), typeof( int ), typeof( string ) } );
                var newSize = ctor.Invoke( new object[] { 1, orientWidth, orientHeight, "For ScreenCapture" } ); // 0 = AspectRatio, 1 = FixedResolution
                addCustomSize.Invoke( group, new object[] { newSize } );

                SetScreenResolution( addedSizeId );
#else
                Screen.SetResolution( orientWidth, orientHeight, false );
#endif
                return true;
            }
#if UNITY_EDITOR
            public static void SetScreenResolution( int index )
            {
                var gvWndType = typeof( UnityEditor.Editor ).Assembly.GetType( "UnityEditor.GameView" );
                var gvWnd = UnityEditor.EditorWindow.GetWindow( gvWndType );
                var SizeSelectionCallback = gvWndType.GetMethod( "SizeSelectionCallback",
                        System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic );
                SizeSelectionCallback.Invoke( gvWnd, new object[] { index, null } );
            }

            public static int GetResolutionIndex()
            {
                var gvWndType = Type.GetType( "UnityEditor.GameView,UnityEditor" );
                var gvWnd = UnityEditor.EditorWindow.GetWindow( gvWndType );
                var get_selectedSizeIndex = gvWndType.GetMethod( "get_selectedSizeIndex",
                        System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic );
                return ( int )get_selectedSizeIndex.Invoke( gvWnd, null );
            }
#endif
        }

#endif
    }
}
