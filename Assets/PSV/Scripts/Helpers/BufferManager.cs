﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PSV
{
    public static partial class BufferManager
    {
        public enum Vars
        {
            Scores,
            HighScore,
        }

        private static Dictionary<string, object> buffer = new Dictionary<string, object>();


        public static object GetVar( Vars key )
        {
            if (buffer.ContainsKey( key.ToString() ))
                return buffer[key.ToString()];
            else
                return null;
        }

        public static T GetVar<T>( string key )
        {
            object result;
            if (buffer.TryGetValue( key, out result ) && result is T)
                return (T)result;
            else
                return default( T );
        }

        public static void SetVar<T>( Vars key, T value )
        {
            SetVar( key.ToString(), value );
        }

        public static void SetVar<T>( string key, T value )
        {
            if (buffer.ContainsKey( key ))
                buffer[key] = value;
            else
                buffer.Add( key, value );
        }

        public static void RemoveVar( Vars key )
        {
            buffer.Remove( key.ToString() );
        }

        public static void RemoveVar( string key )
        {
            buffer.Remove( key );
        }

        public static void Clear()
        {
            buffer.Clear();
        }
    }
}