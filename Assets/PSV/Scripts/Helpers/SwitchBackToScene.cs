﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public class SwitchBackToScene : MonoBehaviour
    {
        public Scenes
            target_scene;
        
        void OnEnable()
        {
            KeyListener.AddKey( KeyCode.Escape );
            KeyListener.OnKeyPressed += ProcessKey;
        }

        void OnDisable()
        {
            KeyListener.OnKeyPressed -= ProcessKey;
        }


        public void ProcessKey( KeyCode key )
        {
            if (key == KeyCode.Escape)
            {
                Action();
            }
        }

        public void Action()
        {
            SceneLoader.SwitchToScene( target_scene );
        }
    }
}