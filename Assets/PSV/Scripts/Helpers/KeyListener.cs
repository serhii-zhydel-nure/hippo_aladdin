﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace PSV
{
    public class KeyListener : AgentObject<KeyListener>
    {
        private static event Action<KeyCode> on_key_pressed;

        public static event Action<KeyCode> OnKeyPressed
        {
            add
            {
                CheckAgent();
                on_key_pressed += value;
                if (agent)
                    agent.enabled = true;
            }
            remove
            {
                on_key_pressed -= value;
                if (on_key_pressed == null && agent)
                    agent.enabled = false;
            }
        }

        private static List<KeyCode> keys_to_check = new List<KeyCode>
        {
            KeyCode.Escape,
        };

        public static void AddKey( KeyCode key, Action<KeyCode> on_pressed = null )
        {
            if (keys_to_check.Count == 0)
            {
                if (agent && ( on_pressed != null || on_key_pressed != null ) )
                    agent.enabled = true;
            }
            if (!keys_to_check.Contains( key ))
                keys_to_check.Add( key );
            if (on_pressed != null)
                OnKeyPressed += on_pressed;
        }

        public static void RemoveKey( KeyCode key )
        {
            keys_to_check.Remove( key );
            if (keys_to_check.Count == 0 && agent)
                agent.enabled = false;

        }

        private new void Awake()
        {
            base.Awake();
            if (on_key_pressed == null)
                enabled = false;
        }

        private void Update()
        {
            for (int i = 0; i < keys_to_check.Count; i++)
            {
                if (Input.GetKeyDown( keys_to_check[i] ))
                {
                    on_key_pressed( keys_to_check[i] );
                    //Debug.Log("Key pressed : " + keys_to_check [i].ToString());
                }
            }
        }
    }
}