﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;

namespace PSV
{

    public static class PauseManager
    {
        public static event Action<bool> OnPause;

        private static bool
            debug_mode = false,
            game_paused = false,
            paused = false;

        private static List<Tween>
            paused_tweens = null;

        [AwakeStatic]
        public static void Subscribe()
        {
            AwakeStaticAttribute.Done( typeof( PauseManager ) );
            ManagerGoogle.OnInterstitialShown += InterstitialShown;
            ManagerGoogle.OnRewardedShown += InterstitialShown;
            ManagerGoogle.OnInterstitialClosed += InterstitialClosed;
            ManagerGoogle.OnRewardedClosed += InterstitialClosed;
            ApplicationEventAgent.OnPause += OnApplicationPause;
        }

        private static IEnumerator UpdateTimeScale()
        {
            WaitForFixedUpdate fixedDelay = new WaitForFixedUpdate();

            while (paused)
            {
                Time.timeScale = 0;
                yield return fixedDelay;
            }
        }

        private static void OnApplicationPause( bool pause )
        {
            DebugLog( "PauseManager: OnApplicationPause " + pause );

            if (!game_paused)
            {
                SetPause( pause, false );
            }
        }

        private static void InterstitialShown()
        {
            if (!game_paused)
            {
                SetPause( true, false );
            }
        }

        private static void InterstitialClosed()
        {
            if (!game_paused)
            {
                SetPause( false, false );
            }
        }

        public static bool IsPaused()
        {
            return paused;
        }

        public static void TogglePause()
        {
            SetPause( !IsPaused() );
        }

        public static void SetPause( bool pause )
        {
            SetPause( pause, true );
        }

        private static void SetPause( bool pause, bool save_state )
        {
            if (pause != paused)
            {
                paused = pause;

                if (save_state)
                {
                    game_paused = pause;
                }

                Time.timeScale = pause ? 0.0f : 1.0f;

                DebugLog( "setPause " + pause + " timescale == " + (pause ? 0.0f : 1.0f) );

                AudioController.Pause( pause );

                if (pause)
                {
                    DOTween.PauseAll();
                    paused_tweens = DOTween.PausedTweens();
                    CoroutineHandler.Start( UpdateTimeScale() );
                }
                else if (paused_tweens != null)
                {
                    for (int i = 0; i < paused_tweens.Count; i++)
                    {
                        paused_tweens[i].Play();
                    }
                    paused_tweens = null;
                }

                if (OnPause != null)
                {
                    OnPause( pause );
                }
            }
        }

        private static void DebugLog( string message )
        {
            if (debug_mode)
            {
                Debug.Log( "PauseManager: " + message );
            }
        }
    }
}
