/* 
 * Dependence plugins: 
 * com.psvplugins.nativevibration:1.0
 * 
 * Build SDK Tools Version '27.0.3+'
 * */
 
using UnityEngine;
using System.Collections;
using System;

namespace PSV
{
    public static class Vibration
    {
        private const string native_android_class_name = "com.psvplugins.nativevibration.Vibration";
        private const string settings_enabled = "vibroEnabled";

        private const int code_invalid = -5;
        private const int code_unknown = -1;
        private const int code_disabled = 0;
        private const int code_enabled = 1;

        private static AndroidJavaClass native_android_class = null;
        private static int valid_state = code_enabled;

        public static event Action<bool> OnVibroEnabled;


        public static void SetEnabled(bool enabled, bool save_settings = true )
        {
            if (save_settings)
            {
                PlayerPrefs.SetInt( settings_enabled, enabled ? code_enabled : code_disabled );
                NextFrameCall.SavePrefs();
            }
            if(valid_state != code_invalid)
                valid_state = enabled ? code_enabled : code_disabled;
            if (OnVibroEnabled != null)
                OnVibroEnabled( enabled );
        }

        public static bool IsEnabled()
        {
            return valid_state == code_enabled;
        }

        /// <summary>
        /// Vibrate constantly for the specified period of time.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to vibrate.</param>
        public static void Vibrate( long milliseconds )
        {
            CallNative( "Vibrate", milliseconds );
        }

        /// <summary>
        /// Vibrate with a given pattern.
        /// 
        /// Pass in an array of ints that are the durations for which to turn on or off
        /// the vibrator in milliseconds.The first value indicates the number of milliseconds
        /// to wait before turning the vibrator on.The next value indicates the number of milliseconds
        /// for which to keep the vibrator on before turning it off.Subsequent values alternate
        /// between durations in milliseconds to turn the vibrator off or to turn the vibrator on.
        /// 
        /// FirstElement    0 : Initial delay 0 ms
        /// SecondElement 400 : Vibrate for 400 ms
        /// ThirdElement  800 : Pause for 800 ms
        /// FourthElement 600 : Vibrate for 600 ms
        /// FifthElement  800 : Pause for 800 ms
        /// SixthElement  800 : Vibrate for 800 ms
        /// SeventElement 800 : Pause for 800 ms
        /// EightElement 1000 : Vibrate for 1000 ms
        /// ...
        /// </summary>
        /// <param name="pattern">Minimum lenth 4: Initial; OneShot; delay; OneShot.</param>
        public static void VibratePattern( long[] pattern )
        {
            CallNative( "VibratePattern", pattern );
        }

        /// <summary>
        /// Turn the vibrator off.
        /// </summary>
        public static void Cancel()
        {
            CallNative( "Stop" );
        }

        private static void CallNative( string nameMethod, params object[] args )
        {
            if (valid_state == code_unknown)
                valid_state = PlayerPrefs.GetInt( settings_enabled, code_enabled );
            if (valid_state != code_enabled)
                return;
            if (Application.platform == RuntimePlatform.Android)
            {
                try
                {
                    if (native_android_class == null)
                    {
                        native_android_class = new AndroidJavaClass( native_android_class_name );
                        if (native_android_class == null || native_android_class.GetRawClass() == IntPtr.Zero)
                        {
                            valid_state = code_invalid;
                            return;
                        }
                    }
                    native_android_class.CallStatic( nameMethod, args );
                }
                catch
                {
                    Debug.LogError( "Vibration: " + native_android_class_name + " class not found!" );
                    valid_state = code_invalid;
                }
            }
        }

    }


#if false && UNITY_ANDROID && UNITY_EDITOR
    [UnityEditor.InitializeOnLoad]
    class VibrationManifestProcessor
    {
        private static string[] necessary_permissions = new string[] {
            "android.permission.VIBRATE",
        };


        static VibrationManifestProcessor()
        {
            CheckPermissions();
        }

        [UnityEditor.MenuItem( "HelpTools/Manifest/Check Vibrate permission" )]
        private static void CheckPermissions()
        {
            var outputFile = System.IO.Path.Combine( Application.dataPath, "Plugins/Android/AndroidManifest.xml" );
            if (!System.IO.File.Exists( outputFile ))
            {
                Debug.LogError( "There is no Manifest file found at Assets/Plugins/Android/" );
                return;
            }

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.Load( outputFile );

            if (doc == null)
            {
                Debug.LogError( "Couldn't load " + outputFile );
                return;
            }

            System.Xml.XmlNode manNode = FindChildNode( doc, "manifest" );
            string ns = manNode.GetNamespaceOfPrefix( "android" );

            for (int i = 0; i < necessary_permissions.Length; i++)
            {
                System.Xml.XmlElement permission = FindElementWithAndroidName( "uses-permission", "name", ns, necessary_permissions[i], manNode );
                if (permission == null)
                {
                    permission = doc.CreateElement( "uses-permission" );
                    permission.SetAttribute( "name", ns, necessary_permissions[i] );
                    manNode.AppendChild( permission );
                    Debug.Log( "Vibration: added missing permission " + permission );
                }
            }
            doc.Save( outputFile );
        }

        private static System.Xml.XmlNode FindChildNode( System.Xml.XmlNode parent, string name )
        {
            System.Xml.XmlNode curr = parent.FirstChild;
            while (curr != null)
            {
                if (curr.Name.Equals( name ))
                {
                    return curr;
                }
                curr = curr.NextSibling;
            }
            return null;
        }

        private static System.Xml.XmlElement FindElementWithAndroidName( string name, string androidName, string ns, string value, System.Xml.XmlNode parent )
        {
            var curr = parent.FirstChild;
            while (curr != null)
            {
                if (curr.Name.Equals( name ) && curr is System.Xml.XmlElement && ( ( System.Xml.XmlElement )curr ).GetAttribute( androidName, ns ) == value)
                {
                    return curr as System.Xml.XmlElement;
                }
                curr = curr.NextSibling;
            }
            return null;
        }
    }
#endif
}