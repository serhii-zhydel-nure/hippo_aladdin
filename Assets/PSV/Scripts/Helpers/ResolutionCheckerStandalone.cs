﻿#if UNITY_STANDALONE
using UnityEngine;
using System.Collections;

namespace PSV
{
    public class ResolutionCheckerStandalone : AgentObject<ResolutionCheckerStandalone>
    {
        private const float targetAspect = 9.0f / 16.0f;

        public static bool
            change_size = true;

        private int
            saved_height = 0,
            saved_width = 0;

        [RuntimeInitializeOnLoadMethod( RuntimeInitializeLoadType.AfterSceneLoad )]
        private static void CreateChecker()
        {
            if (PSVSettings.settings.portrait_orientation_only)
                CheckAgent();
        }

        private void Start()
        {
            if (PSVSettings.settings.portrait_orientation_only)
                InvokeRepeating( "CheckResolution", 0.0f, 1.0f );
            else
                Destroy( this );
        }

        /// <summary>
        /// Invoke Repeating method : <see cref="Start()"/>
        /// </summary>
        private void CheckResolution()
        {
            if (change_size && (Screen.height != saved_height || Screen.width != saved_width))
            {
                saved_height = Screen.height;
                saved_width = (int)((float)saved_height * targetAspect);
                Debug.Log( "CheckResolution " + saved_width + "x" + saved_height );
                Screen.SetResolution( saved_width, saved_height, false );
            }
        }
    }
}
#endif