﻿//#define PUSH_NEWS_PRESENT

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using PSV.ADS;

/* ChangeLog:
 * Updated Spine package (http://ru.esotericsoftware.com/forum/Noteworthy-Spine-Unity-Topics-5924) (use Spine.Unity and Spine.Unity.Modules  (http://ru.esotericsoftware.com/forum/Spine-Unity-namespaces-6025))
 * New GoogleAdsSDK (fixed bugs)
 * Scenes are loaded by string. Its not necessary now to keep same order of scenes in enum
 * Banners wont be repositioned until splashes will end
 * Added extra check before banner reposition to avoid its reload call (ManagerGoogle)
 * Added push scene to no-ads list
 * Added platform directives to support different platforms
 * Removed Transition kit with shaders
 * 1.1.3
 * Added mediation networks: Chartboost, UnityAds, InMobi, MobFox
 * Added RateMePlugin
 * Added Promo.v11
 * Updated ManagerGoogle
 * Updated AdmobManager
 * Added PlayServicesResolver
 * Added EditorTools for Textures and manifest
 * Modified Audio toggle, LangPannel scripts
 * Added hide promo OnSettingsVisible
 * 1.1.4
 * Fixed reposition of banner (didn't turned back to previous position): modified ManagerGoogle and AdmobManager 
 * 1.1.5
 * Moved Reposition small banner to event OnInterClosed
 * Moved SceneLoader to namespace PSV
 * 1.1.6
 * Fixed not calling OnInterClosed when Interstitial is not allowed to show
 * 1.1.7
 * Added support of FirebaseAnalytics
 * 1.1.8
 * Fixed Firebase LogEvent(SelectContent)
 * 1.2.0
 * Modified PauseStreams and GetSRC methods in AudioController 
 * Added AdsInterop and AdsManager with IAdProvider interface for ad plugins
 * Added resize banner interface
 * Added support of Rewarded Video Ads
 * Added support of Native Banners
 * Removed AdmobManager
 * Modified ManagerGoogle and SceneLoader classes to match AdsInterop's interface
 * Implemented ExternalLinksManager to show one view per screen (thanks to Dmytro Vyshnevskyi)
 * Modified 
 * - PurchaseButton
 * - AudioController
 * - PromoModule
 * - RateUs
 * - ManagerGoogle
 * - Utils
 * - BillingManager
 * - SceneLoader
 * 1.2.2
 * - Fixes for RateMeModule.PauseAudio
 * 1.2.3
 * - RateMeModule.PauseAudio deprecated
 * - CanUseHome changed to work with interstitial count, not time (so it is possible to show home ads depending )
 * - moved use_home reset to InterstitialShown event listener (this will save triggers state if interstitial wasn't shown, but InterstitialClosed event occurred)
 * 1.2.4 (17.01.2017)
 * - Implemented ad request interval to avoid crashes in off-line mode
 * 1.2.5 (26.01.2017)
 * - shortened log event message for ad errors from providers
 * - AdsManager now implements home_ad_interval remote param to select home provider for showing own ads once per interval (used for all ad types)
 * - RateMe module fixed to give correct value on IsVisible method called
 * - FirebaseManager minor fixes
 * - Updated SDKs for mediation networks
 * 1.2.6 (20.02.2017)
 * - Made changes to some scripts by adding define to support faster import into older prototype projects
 * - SpineRuntime Last updated: UTC - 2017 February 18
 * 1.2.7 (02.03.2017)
 * - updated FirebaseManager to support one-shot database connection (minimizing traffic)
 * - implemented DB_connections limit in FirebaseManager managed by RemoteConfig, limits connection based on db_url
 * - updated PromoModule to support RemoteConfig and RealtimeDatabase
 * - moved ManagerGoogle's interstitial_time to RemoteConfig
 * - fixed AdsProviders
 * - updated HomeAdsSettings
 * - SpineRuntime Last updated: UTC - 2017 February 27
 * 1.2.8 (09.03.2017)
 * - Updated FirebaseManager to avoid crashes in libraries
 * - Implemented SimulateTransition method to make in-scene transitions easier (when switching between small episodes)
 * 1.2.9 (21.03.2017)
 * - Ads DummyProvider updated (fixed behaviour for AdsDisabled)
 * - AdsManager is modified to exclude HomeAdsProvider as standalone to enable its disabling
 * - ManagerGoogle now can change interval for the time interstitial will be shown at first
 * - LanguagePannel and lang toggle were modified to match set of languages defined as a Dictionary in Languages.cs
 * - RateMe module changed behavior for sound playback (now it will be played as panel will become enabled
 * - SpineRuntime Last updated: UTC - 2017 March 16
 * 1.3.0 (22.03.2017)
 * - GoogleAnalytics - added platform as a prefix to AppVersion (detailed change-log in script)
 * - PromoModule, HomeAdsSettings, FirebaseManager, AnalyticsManager are modified to support non-Firebase Implementation (to exclude firebase from project just comment all occasions of "define FIREBASE_PRESENT" directive)
 * 2.0.0 (30.03.2017)
 * - Fixed RateMe (PauseAudio)
 * - Excluded GoogleAdsProviders
 * - NeatPlug can show Rewarded as Interstitial (Rewarded events still work)
 * 2.0.0 (30.03.2017)
 * - Added events to SceneLoader (OnSCreenObscured - when the screen is Faded)
 * - Added events to GameSettings (On<group>VolumeChanged, On<group>Enabled)
 * - Added Latest Mediation SDKs and new networks
 * 3.0.0 (12.05.2017)
 * - Added AdColony
 * - Added Leadbolt
 * - Reverted old partners plugins to same as in Traumatologist
 * - Added mediation for IOS
 * - Added Resolver for IOS
 * - to build IOS project use HelpTol/Build IOS
 * - import Latest Firebase and GoogleAds (exclude PlayServicesResolver)
 * - use PlayServices resolver from Firebase only
 * - ProjectSettingsContainer was updated (debug enabler for ads managers)
 * - Now ManagerGoogle supports remote params for CHILDREN_TAGGED and FOR_FAMILIES (bool true/false)
 * - Now Promo takes care of primary set to be loaded any way (only if project was launched under certain platform)
 * - banner_visible_on_complete when simulating transition can ignore in scene rules for banner (reposition can be called before simulating with flag set true)
 * - now Showing RateUs will ignore calling interstitial
 * 3.0.1 (07.06.2017)
 * - refactored AnalyticsManager
 * - now scene loading will start after ScreenObscured callback complete
 * - added event to monitor asyncSceneLoad progress
 * 3.0.2 (09.06.2017)
 * - patched AudioControllers PauseStreams method to clear list of paused sources on unpause
 * - patched SceneLoader to pause Audio when interstitial is shown and unpause it whet it is closed (IOS feature not to pause game activity completely when it is interrupted by ad)
 * - added AudioControler.OnPause event to support iteration with custom controllers
 * 3.0.3 (15.06.2017)
 * - implemented scene load mode
 * - AudioToggle for music now disabled to avoid copying of game sounds
 * - GameSettings MusicVolume now inherits SoundsVolume
 * - GameSettings will change simultaneously sounds and music settings
 * 3.0.4 (30.06.2017)
 * - added pause manager and GUI items for it (pause buttons and pause panel)
 * - made base class for dialogs
 * - refactored settings panel
 * - refactored AudioController
 * - updated UI graphics
 * 3.0.5
 * - implemented PauseManager and GUI part to manage pause in game (panel and pause buttons)
 * 3.0.6
 * - fixed AudioControler.GetPlayingSources (AudioSource.isPLaying returns false always when application loose focus)
 * - fixed AudioControler's PauseStreams and PauseMusics to avoid bugs when set to pause second time
 * - fixed BannerPannelController - Centered image (texture sticked to edge had double offset)
 * 3.0.7
 * - added FadeContentEnabler to avoid any visible content before interstitial was shown (Screen stays black till Interstitial will be closed, then all other stuff is enabled (loading label, progress bar))
 * - FadeContentEnabler will automatically be added to fadeScreen gameObject
 * - moved Banner and Native refresh to CompleteTransition method
 * - implemented overrides of banner and native ad settings for transition (scene load and simulation)
 * - moved LogScreeen analytics event to WaitForTransitionComplete coroutine (will take in account targetScene overrides by RateMeModule)
 * - fixed rate me resetting last time (was set after scene transition so if we use no effect transition, last time wont be reset and rate me will be shown again)
 * 3.1.0
 * - implemented Unity services for AnalyticsManager and BillingManager
 * - moved LogScreen event in waitForTransition after scene became current to keep calls to CurrentScene in this event actual
 * - added scripting define symbols helper to generate GOOGLE_ADS_PRESENT, FIREBASE_PRESENT, USE_OPEN_IAB for enabling some functionality that use these plugins
 * - refactored editor help tools (texture import settings)
 * - updated push plugin
 * 3.1.1
 * - fixed CanShowRateUs(): returned true after RateUs scene too
 * - added template in rateUs methods to show dialogged every X scene
 * - there can be troubles after updating project with Unity IAP: 
 *   solution is to comment scripts that use UnityIAP/Analytics (commenting defines will be enough)
 *   and re-import UnityIAP from asset-store or Re-import assets locally
 * - Promo module now will not work with local (primary) set; will be shown only if data from server received; to check it wait for a while and reload scene where PromoUI is located
 * 3.2
 * - fixed SimulateTransition (will not work if transition is in progress, and will lock transition by itself)
 * - Rate me will be shown till closed 3 times
 * 3.3
 * - refactored scripts, implemented OffersManager, moved RateMe and PurchaseMe dialogs to it
 * - refactored AnalyticsManager, splitted main class
 * - refactored BillingManager, implemented IAP provider, splitted main class
 * 
 * 
 * Current version 4.2+ and dispay in Window/PSV Project Settings.
 */

namespace PSV
{
    public static partial class SceneLoader
    {
        public delegate void OnSceneLoadedDelegate( Scenes from, Scenes to );
        public static event OnSceneLoadedDelegate
            OnSceneLoaded;
        public static event Action
            OnScreenObscured;

        public static event Action<float>
            OnLoadProgressChange;

        public enum TransitionMethod
        {
            Default,    //do not select this (service item)
            Tween,      //using animation of Canvas Image
            None,       //using simple SceneManager.LoadScene()
        }

        private static Scenes
            _target_scene;

        public static bool async_load { get { return PSVSettings.settings.async_load; } }

        private static bool
            interstitial_closed = false,
            in_transition = false;      //determines if we can start another transition (switching between few scenes in a row not allowed)

        public static Scenes[] splash_scenes { get { return PSVSettings.settings.splash_scenes; } }

        public static Scenes first_scene { get { return PSVSettings.settings.first_scene; } }   //scene to load after splashes (usually MainMenu)
#if PUSH_NEWS_PRESENT
        public static Scenes push_scene { get { return PSVSettings.settings.push_scene; } } //scene where push service is set
#endif
        public static float splash_duration { get { return PSVSettings.settings.splash_duration; } }    //splash screen lifetime
        public static float transition_duration { get { return PSVSettings.settings.transition_duration; } }   //transition animation duration

        public static TransitionMethod transition_method { get { return PSVSettings.settings.transition_method; } }    //how to switch between scenes


        private static Action
            complete_simulate_transition = null;


        #region ad_overrides

        private static bool?
            override_banner_visible = null,
            override_native_visible = null;


        private static AdPosition
            override_banner_pos = AdPosition.Undefined,
            override_native_pos = AdPosition.Undefined;

        private static AdSize
            override_banner_size = null,
            override_native_size = null;

        #endregion

        [AwakeStatic]
        public static void StartStatic()
        {
            AwakeStaticAttribute.Done( typeof( SceneLoader ) );
            SetTargetScene( first_scene );
            ManagerGoogle.OnInterstitialClosed += InterstitialClosed;
            ManagerGoogle.OnInterstitialShown += InterstitialShown;
            LoadSplash();
        }

        public static void LoadSplash()
        {
#if UNITY_EDITOR
            if (EditorPSVSettings.settings.splash_transition)
#endif
                if (GetStringCurrentScene() == "InitScene")
                {
                    CoroutineHandler.Start( SplashSequence() );
                }

            AnalyticsManager.LogEvent( AnalyticsEvents.StartApplication );
        }

        #region EventListeners

        private static void ScreenObscured()    //occurs when screen is frozen (hidden)
        {
            Scenes current_scene = GetCurrentScene();
            Scenes target_scene = GetTargetScene();
            if (OnScreenObscured != null)
            {
                OnScreenObscured();
            }
            //AudioController.ReleaseStreams ( ); //used for old AudioController
            AudioController.Release( true );

            //call before other Scene dependent methods - can override target scene if using scene mode
            var offers_reply = OffersManager.ShowOffers( current_scene, target_scene );
            if (offers_reply != OfferReply.ShowWithAds)
            {
                InterstitialClosed();
            }
        }

        private static void InterstitialShown()               //occurs when user closes interstitial
        {
            //PauseManager.SetPause ( true, false );
        }

        private static void InterstitialClosed()               //occurs when user closes interstitial
        {
            //PauseManager.Instance.SetPause ( false, false );
            interstitial_closed = true;
            CompleteSimulatingTransinion();
        }

        private static void CompleteTransition()
        {
            Scenes current_scene = GetCurrentScene();
            RefreshBanner( current_scene );
            RefreshNative( current_scene );
            in_transition = false;
        }
        
        #endregion

        #region ServiceMethods

        public static bool IsSceneLoaded( string scene_name )
        {
            //search scene among active
            for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                Scene s = SceneManager.GetSceneAt( i );
                if (s.name == scene_name && s.isLoaded)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsSceneActive( string scene_name )
        {
            var activeScene = SceneManager.GetActiveScene();
            return activeScene.name.Equals( scene_name ) || ( activeScene.buildIndex == 0 && Scenes.None.ToString().Equals( scene_name ));
        }

        public static string GetStringCurrentScene()
        {
            return SceneManager.GetActiveScene().name;
        }

        public static Scenes GetCurrentScene()
        {
            try
            {
                return GetStringCurrentScene().ToEnum<Scenes>();
            }
            catch
            {
                return Scenes.None;
            }
        }

        public static Scenes GetTargetScene()
        {
            return _target_scene;
        }

        public static void SetTargetScene( Scenes scene )
        {
            _target_scene = scene;
        }

        private static bool CanContinueTransition(/*string level*/)
        {
            return !SceneLoaderSettings.transition_after_ad || interstitial_closed/* && GetStringCurrentScene ( ) == level*/;
        }

        public static bool IsLoadingLevel()
        {
            return in_transition;
        }

        public static void SwitchToScene( Scenes target_scene, TransitionMethod method = TransitionMethod.Default, float override_duration = -1, LoadSceneMode load_mode = LoadSceneMode.Single )
        {
            if (!IsLoadingLevel())
            {
                if (target_scene == Scenes.None)
                {
                    Debug.LogError( "SceneLoader: Switch to None scene is impossible!" );
                    return;
                }
                SetTargetScene( target_scene );

                in_transition = true;

                interstitial_closed = false;

                //will be enabled after transition complete
                HideBannerAds();

                if (method == TransitionMethod.Default)
                    method = transition_method;

                float duration = override_duration < 0 ? transition_duration : override_duration;

                switch (method)
                {
                    case TransitionMethod.None:
                    {
                        LoadScene( load_mode, CompleteTransition );
                        break;
                    }
                    case TransitionMethod.Tween:
                    {
                        TweenLoadScene( duration, load_mode );
                        break;
                    }
                }
            }
            else
            {
                Debug.LogError( "SceneLoader: There is still a transition to the scene, so the next transition is impossible!" );
            }
        }

        private static IEnumerator SplashSequence()   //coroutine for showing splash scenes with certain delay
        {
            yield return new WaitForEndOfFrame();
            for (int i = 0; i < splash_scenes.Length; i++)
            {
                SwitchToScene( splash_scenes[i] );
                yield return new WaitForSeconds( splash_duration );
            }
#if PUSH_NEWS_PRESENT
            if (Utils.IsMobilePlatform())
            {
                SwitchToScene( PsvPushService.AreNewsAvailable() ? push_scene : first_scene );
            }
            else
#endif
            {
                SwitchToScene( first_scene );
            }
        }

        #endregion

        #region ADS

        private static void HideBannerAds()
        {
            ManagerGoogle.HideSmallBanner();
            ManagerGoogle.HideNativeBanner();
        }

        private static bool RefreshBanner( Scenes target_scene )
        {
            bool show_banner = false;
            if (Application.isEditor || Application.isMobilePlatform)
            {
                show_banner = override_banner_visible != null ? override_banner_visible == true : !SceneLoaderSettings.not_allowed_small_banner.Contains( target_scene );

                if (show_banner)
                {
                    AdPosition target_pos;
                    if (override_banner_pos != AdPosition.Undefined)
                    {
                        target_pos = override_banner_pos;

                    }
                    else if (!SceneLoaderSettings.small_banner_position_override.TryGetValue( target_scene, out target_pos ))
                    {
                        target_pos = SceneLoaderSettings.small_banner_default_pos;
                    }

                    AdSize target_size;
                    if (override_banner_size != null)
                    {
                        target_size = override_banner_size;
                    }
                    else if (!SceneLoaderSettings.small_banner_size_override.TryGetValue( target_scene, out target_size ))
                    {
                        target_size = SceneLoaderSettings.small_banner_default_size;
                    }

                    ManagerGoogle.RefreshSmallBanner( target_pos, target_size );
                    ManagerGoogle.ShowSmallBanner();
                }
                else
                {
                    ManagerGoogle.HideSmallBanner();
                }
            }
            OverrideBannerSettings();
            return show_banner;
        }

        private static bool RefreshNative( Scenes target_scene )
        {
            bool show_native = false;
            if (Application.isEditor || Application.isMobilePlatform)
            {
                show_native = override_native_visible != null ? override_native_visible == true : SceneLoaderSettings.native_allowed.Contains( target_scene );

                if (show_native)
                {
                    AdPosition target_pos;
                    if (override_native_pos != AdPosition.Undefined)
                    {
                        target_pos = override_native_pos;

                    }
                    else if (!SceneLoaderSettings.native_position_override.TryGetValue( target_scene, out target_pos ))
                    {
                        target_pos = SceneLoaderSettings.native_default_pos;
                    }

                    AdSize target_size;
                    if (override_native_size != null)
                    {
                        target_size = override_native_size;
                    }
                    else if (!SceneLoaderSettings.native_size_override.TryGetValue( target_scene, out target_size ))
                    {
                        target_size = SceneLoaderSettings.native_default_size;
                    }

                    ManagerGoogle.RefreshNativeBanner( target_pos, target_size );
                    ManagerGoogle.ShowNativeBanner();
                }
                else
                {
                    ManagerGoogle.HideNativeBanner();
                }
            }
            OverrideNativeSettings();
            return show_native;
        }

        /// <summary>
        /// Call before Loading scene or simulating transition. Will override settings only for current transition. Call without arguments to reset overrides.
        /// </summary>
        /// <param name="visible"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        public static void OverrideBannerSettings( bool? visible = null, AdPosition pos = AdPosition.Undefined, AdSize size = null )
        {
            override_banner_visible = visible;
            override_banner_pos = pos;
            override_banner_size = size;
        }

        /// <summary>
        /// Call before Loading scene or simulating transition. Will override settings only for current transition. Call without arguments to reset overrides.
        /// </summary>
        /// <param name="visible"></param>
        /// <param name="pos"></param>
        /// <param name="size"></param>
        public static void OverrideNativeSettings( bool? visible = null, AdPosition pos = AdPosition.Undefined, AdSize size = null )
        {
            override_native_visible = visible;
            override_native_pos = pos;
            override_native_size = size;
        }

#endregion

#region TweenTransition

        public static void SimulateTransition( Action on_screen_obscured, bool show_interstitial = true, float override_duration = -1 )
        {
            if (!IsLoadingLevel())
            {
                in_transition = true;
                HideBannerAds();
                float duration = override_duration < 0 ? transition_duration : override_duration;
                complete_simulate_transition = () =>
                {
                    if (on_screen_obscured != null)
                    {
                        on_screen_obscured();
                    }

                    CoroutineHandler.Start( SimulatingLoadResources( duration ) );
                };
                Action on_fade_complite;
                if (show_interstitial)
                    on_fade_complite = ManagerGoogle.ShowInterstitial;
                else
                    on_fade_complite = CompleteSimulatingTransinion;
                ServiceFade.SceneTransition( false, on_fade_complite, duration );
            }
            else
            {
                Debug.LogError( "SceneLoader: There is still a transition to the scene, so the next transition is impossible!" );
            }
        }

        private static void CompleteSimulatingTransinion()
        {
            if (complete_simulate_transition != null)
            {
                complete_simulate_transition();
                complete_simulate_transition = null;
            }
        }

        private static void TweenLoadScene( float duration, LoadSceneMode load_mode )
        {
            ServiceFade.SceneTransition( false, () => TweenCompleteTransition( duration, load_mode ), duration );
        }

        private static void TweenCompleteTransition( float duration, LoadSceneMode load_mode )
        {
            LoadScene( load_mode, () =>
            {
                ServiceFade.SceneTransition( true, CompleteTransition, duration );
            } );
        }

        [Obsolete( "Use extension method CanvasGroup.FadeAction", true )]
        public static void SceneTransition( bool open, Action callback, CanvasGroup fade_image, float fadeTime, bool use_tween = false ) { }

#endregion

#region NoEffectTransition
        private static void LoadScene( LoadSceneMode load_mode, Action load_complete_callback = null )   //generic way of loading scene without animation
        {
            ScreenObscured();
            CoroutineHandler.Start( SceneTransitionAction( load_mode, load_complete_callback ) );
        }

        private static IEnumerator SceneTransitionAction( LoadSceneMode load_mode, Action callback )
        {
            while (!CanContinueTransition())
            {
                //Debug.Log ( "Interstitial not closed" );
                yield return null;
            }

            if (OnLoadProgressChange != null)
                OnLoadProgressChange( 0.01f );
            yield return LoadResourcesAction();

            Scenes current_level = GetCurrentScene();
            Scenes target_level = GetTargetScene();
            string level = target_level.ToString();

            if (async_load)
            {
                AsyncOperation a_op = SceneManager.LoadSceneAsync( level, load_mode );

                //this method enables Scene after it is loaded immediately
                while (!a_op.isDone)
                {
                    if (OnLoadProgressChange != null)
                        OnLoadProgressChange( a_op.progress );
                    //Debug.Log ( "Scene " + level + " loading:  " + a_op.progress + " %" );
                    yield return null;
                }
            }
            else
            {
                SceneManager.LoadScene( level, load_mode );
            }


            while (!IsSceneActive( level ))
            {
                //Debug.Log ( "Can't continue transition" );
                yield return null;
            }

            AnalyticsManager.LogEvent( AnalyticsEvents.LogScreen, level );

            if (callback != null)
            {
                callback();
            }

            if (OnSceneLoaded != null)
            {
                try
                {
                    OnSceneLoaded( current_level, target_level );
                }
                catch (Exception e)
                {
                    Debug.LogError( "Exception occurred while executing OnSceneLoaded(): " + e.Message );
                }
            }
        }
        
#endregion
        
    }
}