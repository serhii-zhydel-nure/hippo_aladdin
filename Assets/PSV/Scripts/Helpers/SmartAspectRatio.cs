﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    [RequireComponent( typeof( Camera ) )]
    public class SmartAspectRatio : MonoBehaviour
    {
        private const float defaultAspect = 16.0f / 9.0f;
        public enum IncreaseType
        {
            Width, Half, Height
        }

        public Aspect[] aspects =
        {
            new Aspect( 18, 9, IncreaseType.Half ),
            new Aspect( 16, 9, IncreaseType.Width ),
            new Aspect( 17, 10, IncreaseType.Height ),
            new Aspect( 16, 10, IncreaseType.Height ),
            new Aspect( 3, 2, IncreaseType.Half ),
            new Aspect( 4, 3, IncreaseType.Half )
        };

        private void Awake()
        {
            Camera mainCamera = GetComponent<Camera>();
            float currAspect = mainCamera.aspect;
            IncreaseType type = IncreaseType.Width;
            for (int i = 0; i < aspects.Length; i++)
            {
                if (Mathf.Approximately( currAspect, ( float )aspects[i].x / aspects[i].y ))
                {
                    type = aspects[i].type;
                }
            }
            mainCamera.orthographicSize = SizeForAspect( mainCamera.orthographicSize, mainCamera.aspect, type );
#if !UNITY_EDITOR
            Destroy( this );
#endif
        }

        public static float SizeForAspect( float size, float aspect, IncreaseType type )
        {
            switch (type)
            {
                default:
                return size;
                case IncreaseType.Height:
                return size * defaultAspect / aspect;
                case IncreaseType.Half:
                float halfAspect = aspect + ( defaultAspect - aspect ) * 0.5f;
                return size * defaultAspect / halfAspect;
            }
        }

        [System.Serializable]
        public struct Aspect
        {
            public int x;
            public int y;
            public IncreaseType type;

            public Aspect( int x, int y, IncreaseType type )
            {
                this.x = x;
                this.y = y;
                this.type = type;
            }
        }

#if UNITY_EDITOR
        [UnityEditor.CustomEditor( typeof( SmartAspectRatio ) )]
        public class Inspector : UnityEditor.Editor
        {
            private Camera camera;
            private UnityEditor.SerializedProperty aspects;
            private static Color[] colors =
            {
                new Color32( 0, 255, 255, 255 ),
                new Color32( 102, 102, 255, 255 ),
                new Color32( 255, 51, 204, 255 ),
                new Color32( 255, 102, 0, 255 ),
                new Color32( 153, 255, 102, 255 ),
                new Color32( 0, 153, 51, 255 ),
            };

            private void OnEnable()
            {
                camera = ( ( Component )target ).GetComponent<Camera>();
                aspects = serializedObject.FindProperty( "aspects" );
            }
            public override void OnInspectorGUI()
            {
                float size = camera.orthographicSize;
                if (UnityEditor.EditorApplication.isPlaying)
                {
                    UnityEditor.EditorGUILayout.HelpBox( "Original camera orthographic size = "
                        + ( size * camera.aspect / defaultAspect ), UnityEditor.MessageType.Info );
                    return;
                }
                UnityEditor.EditorGUILayout.HelpBox( "Camera orthographic size be changed! The standard of aspect ratio is 16:9", UnityEditor.MessageType.Info );

                int count = aspects.arraySize;
                for (int i = 0; i < count; i++)
                {
                    var element = aspects.GetArrayElementAtIndex( i );
                    var type = element.FindPropertyRelative( "type" );
                    int x = element.FindPropertyRelative( "x" ).intValue;
                    int y = element.FindPropertyRelative( "y" ).intValue;
                    UnityEditor.EditorGUILayout.BeginHorizontal();
                    UnityEditor.EditorGUILayout.PropertyField( type, new GUIContent( x + ":" + y + "\tincrease:" ) );
                    colors[i] = UnityEditor.EditorGUILayout.ColorField( colors[i], GUILayout.Width( 50.0f ) );
                    UnityEditor.EditorGUILayout.EndHorizontal();
                }
                serializedObject.ApplyModifiedProperties();
            }

            void OnSceneGUI()
            {
                if (UnityEditor.EditorApplication.isPlaying)
                    return;
                float defaultCameraSize = camera.orthographicSize;
                Vector3 centerPoint = camera.transform.position;
                int count = aspects.arraySize;
                for (int i = 0; i < count; i++)
                {
                    var element = aspects.GetArrayElementAtIndex( i );
                    var type = element.FindPropertyRelative( "type" );
                    int x = element.FindPropertyRelative( "x" ).intValue;
                    int y = element.FindPropertyRelative( "y" ).intValue;

                    float drawAspect = ( float )x / y;
                    if (defaultAspect == drawAspect)
                        continue;
                    float aspectSize = SizeForAspect( defaultCameraSize, drawAspect, ( IncreaseType )type.enumValueIndex );
                    float cameraHeight = aspectSize * 2;
                    Vector3 extents = new Vector3( cameraHeight * drawAspect, cameraHeight, 0 ) * 0.5f;

                    Vector3 leftBot = centerPoint - extents;
                    Vector3 rightTop = centerPoint + extents;

                    Vector3 leftTop = new Vector3( leftBot.x, rightTop.y, rightTop.z );
                    Vector3 RightBot = new Vector3( rightTop.x, leftBot.y, leftBot.z );

                    UnityEditor.Handles.color = colors[i];
                    UnityEditor.Handles.DrawLine( leftBot, leftTop );
                    UnityEditor.Handles.DrawLine( leftTop, rightTop );
                    UnityEditor.Handles.DrawLine( rightTop, RightBot );
                    UnityEditor.Handles.DrawLine( RightBot, leftBot );
                }
            }
        }
#endif
    }
}