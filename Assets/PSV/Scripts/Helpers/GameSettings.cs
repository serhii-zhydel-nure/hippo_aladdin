﻿using UnityEngine;
using System;
using PSV;

public static partial class GameSettings
{
    [Obsolete( "Better use AudioController.GetGroup( StreamGroup.MUSIC ).OnGroupVolumeChanged instead" )]
    public static event Action<float> OnMusicVolumeChanged
    {
        add { AudioController.GetGroup( StreamGroup.MUSIC ).OnGroupVolumeChanged += value; }
        remove { AudioController.GetGroup( StreamGroup.MUSIC ).OnGroupVolumeChanged -= value; }
    }
    [Obsolete( "Better use AudioController.GetGroup( StreamGroup.FX ).OnGroupVolumeChanged instead" )]
    public static event Action<float> OnSoundsVolumeChanged
    {
        add { AudioController.GetGroup( StreamGroup.FX ).OnGroupVolumeChanged += value; }
        remove { AudioController.GetGroup( StreamGroup.MUSIC ).OnGroupVolumeChanged -= value; }
    }
    [Obsolete( "Better use  AudioController.GetGroup( StreamGroup.FX ).OnGroupMuted instead", true )]
    public static event Action<bool> OnSoundsEnabled
    {
        add { /*AudioController.GetGroup( StreamGroup.FX ).OnGroupMuted += ( param ) => { value( !param ); };*/ }
        remove { /*AudioController.GetGroup( StreamGroup.MUSIC ).OnGroupMuted -= ( param ) => { value( !param ); };*/ }
    }
    [Obsolete( "Better use Vibration.OnVibroEnabled instead" )]
    public static event Action<bool> OnVibroEnabled
    {
        add { Vibration.OnVibroEnabled += value; }
        remove { Vibration.OnVibroEnabled -= value; }
    }
    [Obsolete( "Better use AudioController.GetGroup( StreamGroup.MUSIC ).OnGroupMuted instead", true )]
    public static event Action<bool> OnMusicEnabled
    {
        add { /*AudioController.GetGroup( StreamGroup.MUSIC ).OnGroupMuted += ( param ) => { value( !param ); };*/ }
        remove { /*AudioController.GetGroup( StreamGroup.MUSIC ).OnGroupMuted -= ( param ) => { value( !param ); };*/ }
    }


    [Obsolete( "Better use Vibration.IsEnabled() instead" )]
    public static bool IsVibroEnabled()
    {
        return Vibration.IsEnabled();
    }

    [Obsolete( "Better use Vibration.SetEnabled() instead" )]
    public static void EnableVibro( bool param )
    {
        Vibration.SetEnabled( param, true );
    }

    /// <summary>
    /// Not recomended use Enable music.
    /// </summary>
    [Obsolete( "Better use AudioController.EnableSounds(bool, bool) instead" )]
    public static bool IsMusicEnabled()
    {
        return IsSoundsEnabled();
    }

    /// <summary>
    /// Not recomended use Enable music.
    /// </summary>
    [Obsolete( "Better use AudioController.EnableSounds(bool, bool) instead" )]
    public static void EnableMusic( bool param )
    {
        EnableSounds( param );
    }

    [Obsolete( "Better use AudioController.GetGroup(StreamGroup.FX).IsMuted() instead" )]
    public static bool IsSoundsEnabled()
    {
        return !AudioController.GetGroup( StreamGroup.FX ).IsMuted();
    }

    [Obsolete( "Better use AudioController.GetGroup(StreamGroup.FX).IsMuted() instead" )]
    public static void EnableSounds( bool param )
    {
        AudioController.MuteAllStreams( !param );
    }

    [Obsolete( "Better use AudioController.GetGroup(StreamGroup.MUSIC).Volume instead" )]
    public static float GetMusicVol()
    {
        return AudioController.GetGroup( StreamGroup.MUSIC ).Volume;
    }

    [Obsolete( "Better use AudioController.GetGroup(StreamGroup.MUSIC).Volume instead" )]
    public static void SetMusicVol( float param )
    {
        AudioController.GetGroup( StreamGroup.MUSIC ).Volume = param;
    }

    [Obsolete( "Better use AudioController.GetGroup(StreamGroup.FX).Volume instead" )]
    public static float GetSoundsVol()
    {
        return AudioController.GetGroup( StreamGroup.FX ).Volume;
    }

    [Obsolete( "Better use AudioController.GetGroup(StreamGroup.FX).Volume instead" )]
    public static void SetSoundsVol( float param )
    {
        AudioController.GetGroup( StreamGroup.FX ).Volume = param;

    }

    [Obsolete( "Better use Languages.GetLanguage() instead" )]
    public static int GetCurrentLang()
    {
        return ( int )Languages.GetLanguage();
    }

    [Obsolete( "Logic moved to Languages.SetLanguage", true )]
    public static void SetCurrentLang( Languages.Language lang ) { }

}
