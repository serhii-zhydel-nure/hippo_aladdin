﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PSV
{
    public static partial class SceneLoader
    {
        private static List<ILoadable> loaddable_list = new List<ILoadable>();

        /// <summary>
        /// Add resource bundle to preload list.
        /// </summary>
        public static void AddLoadable( ILoadable loadable )
        {
            for (int i = 0; i < loaddable_list.Count; i++)
            {
                if (loaddable_list[i].identifier == loadable.identifier)
                    return;
            }
            loaddable_list.Add( loadable );
        }

        /// <summary>
        /// Remove resource bundle from preload list.
        /// </summary>
        public static void RemoveLoadable( ILoadable loadable )
        {
            if (loadable == null || loadable.isDone)
                return;
            RemoveLoadable( loadable.identifier );
        }

        /// <summary>
        /// Remove resource bundle from preload list.
        /// </summary>
        public static void RemoveLoadable( string identifier )
        {
            if (string.IsNullOrEmpty( identifier ))
                return;
            for (int i = 0; i < loaddable_list.Count; i++)
            {
                if (identifier == loaddable_list[i].identifier)
                {
                    loaddable_list[i].Dispose();
                    if (!IsLoadingLevel())
                        loaddable_list.RemoveAt( i );
                    return;
                }
            }
        }
        
        private static IEnumerator SimulatingLoadResources( float duration )
        {
            yield return LoadResourcesAction();
            ServiceFade.SceneTransition( true, CompleteTransition, duration );
        }

        private static IEnumerator LoadResourcesAction()
        {
            if (loaddable_list.Count > 0)
            {
                string loading_message = null;
                ServiceMessage service_mes = null;

                float load_percent = 0.0f;
                for (int loading_part = 0; loading_part < loaddable_list.Count; loading_part++)
                {
                    ILoadable temp_loadable = loaddable_list[loading_part];
                    float done_percent = loading_part * ( 1.0f / loaddable_list.Count );
                    
                    Stack<IEnumerator> instructions = new Stack<IEnumerator>( 1 );
                    instructions.Push( temp_loadable.GetEnumerator() );
                    while (instructions.Count > 0)
                    {
                        IEnumerator target_instruction = instructions.Peek();
                        if (target_instruction.MoveNext())
                        {
                            float next_percent = done_percent + temp_loadable.progress / loaddable_list.Count;
                            load_percent = Mathf.MoveTowards( load_percent, next_percent, Time.deltaTime );
                            if (OnLoadProgressChange != null)
                                OnLoadProgressChange( load_percent );
                            object currAct = target_instruction.Current;
                            yield return currAct;
                            string progressBytes = temp_loadable.GetProgressBytesString();
                            if (!string.IsNullOrEmpty( progressBytes ))
                            {
                                if (!service_mes)
                                    service_mes = CreateUpdateMessage( out loading_message );
                                service_mes.SetMessage( progressBytes + loading_message );
                            }
                            if (currAct != null && currAct is IEnumerator)
                                instructions.Push( ( IEnumerator )currAct );
                        }
                        else
                        {
                            instructions.Pop();
                        }
                    }
                }
                if (service_mes)
                    ServiceMessage.Hide();
                loaddable_list.Clear();
            }
        }

        private static ServiceMessage CreateUpdateMessage( out string message )
        {
            if (Languages.GetLanguage() == Languages.Language.Russian)
                message = " Пожалуйста, подождите, скачивается обновление.";
            else
                message = " Please wait, the update is downloading.";
            var service_mes = ServiceMessage.Show( message );
            service_mes.SetPosition( TextAnchor.LowerCenter );
            service_mes.SetAlligment( TextAnchor.UpperRight );
            return service_mes;
        }
    }
}