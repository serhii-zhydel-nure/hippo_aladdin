﻿using UnityEngine;
using System.Collections;
using PSV;


/// ChangeLog
/// v2.0
/// - added ProjectSettings interaction (asset which holds settings for current project)
/// - added Firebase interaction to get ID as a remote param
/// - added platform prefix to AppVersion
/// - replaced AppName with AppBundle
/// - removed all items for manual setting


///Depends On:
///- FirebaseManager
///- ProjectSettingsContainer
///- CoroutineHandler

public static class GoogleAnalytics
{
	private const string ANALYTICS_ID_PARAM = "GOOGLE_ANALYTICS_ID";
	private static string PropertyID = "";

	private const string AndroidPropertyID = "UA-64271377-75";
	private const string IOSPropertyID = "UA-64271377-75";
	private const string WPPropertyID = "UA-64271377-75";

	private static string BundleID = "com.PSVStudio.name";
	private static string AppVersion = "1.0";
    
	private static string screenResolution;
	private static string clientID;

	private static string userLanguage;

    [AwakeStatic]
    public static void AwakeStatic()
    {
        AwakeStaticAttribute.Done( typeof( GoogleAnalytics ) );
        PropertyID = GetAnalyticsID();

        Debug.Log( "GoogleAnalytics: started with id=" + PropertyID );

        BundleID = ConstSettings.GetApplicationAlias();

        AppVersion = Application.version;
        //Debug.Log ( "GoogleAnalytics started with ID=" + PropertyID + " and Bundle=" + BundleID );

        // Get the device resolution
        screenResolution = Screen.width + "x" + Screen.height;

        // Get a unique identifier for the device http://docs.unity3d.com/Documentation/ScriptReference/SystemInfo-deviceUniqueIdentifier.html
        string UID = PSV.Utils.GetUID();
        //string UID = System.Guid.NewGuid ( ).ToString ( );
        //if (PlayerPrefs.HasKey ( "UID" ))
        //{
        //	UID = PlayerPrefs.GetString ( "UID" );
        //}
        //else
        //{
        //	PlayerPrefs.SetString ( "UID", UID );
        //}
        clientID = WWW.EscapeURL( UID );

        AppVersion = GetPlatform() + BundleID + "_" + AppVersion;

        // HTMLEscape our variables so it doesn't break the URL request
        //AppName = WWW.EscapeURL ( AppName );
        PropertyID = WWW.EscapeURL( PropertyID );
        BundleID = WWW.EscapeURL( BundleID );
        AppVersion = WWW.EscapeURL( AppVersion );

        // Lets get some extra information about this user
        userLanguage = Application.systemLanguage.ToString().ToLower();
    }


	private static string GetAnalyticsID ()
	{
		return GetPropertyID().Trim();
    }

	private static string GetPropertyID ()
	{
		ProjectSettingsContainer settings = PSVSettings.settings;

		string res = "UA-64271377-75"; //using shared id by default
#if UNITY_ANDROID
		string id = settings.android_analytics_id;
		if (string.IsNullOrEmpty ( id ))
		{
			id = AndroidPropertyID;
		}
		res = id;
#elif UNITY_IPHONE
		string id = settings.ios_analytics_id;
		if (string.IsNullOrEmpty ( id ))
		{
			id = IOSPropertyID;
		}
		res = id;
#elif UNITY_WSA
		string id = settings.windows_analytics_id;
		if (string.IsNullOrEmpty ( id ))
		{
			id = WPPropertyID;
		}
		res = id;
#endif
		return res;
	}

	private static string GetPlatform ()
	{
		string res = "";
		switch (Application.platform)
		{
			case RuntimePlatform.Android:
				res = "a";
				break;
			case RuntimePlatform.IPhonePlayer:
				res = "i";
				break;
			case RuntimePlatform.WSAPlayerARM:
			case RuntimePlatform.WSAPlayerX64:
			case RuntimePlatform.WSAPlayerX86:
			//case RuntimePlatform.WP8Player:
				res = "w";
				break;
			case RuntimePlatform.OSXEditor:
			case RuntimePlatform.WindowsEditor:
				res = "editor";
				break;
			//case RuntimePlatform.OSXWebPlayer:
			case RuntimePlatform.WebGLPlayer:
			//case RuntimePlatform.WindowsWebPlayer:
				res = "web";
				break;
			case RuntimePlatform.WindowsPlayer:
			case RuntimePlatform.OSXPlayer:
			case RuntimePlatform.LinuxPlayer:
				res = "PC";
				break;
		}

		if (!string.IsNullOrEmpty ( res ))
		{
			res += "_";
		}
		return res;
	}
    

	public static void LogScreen (string title)
	{
		// Get the html chars escaped title of the screen so it doesn't break the URL request
		title = AppVersion + "_" + title;
		title = WWW.EscapeURL ( title );

		// URL which will be pinged to log the requested screen and include details about the user
		var url = "http://www.google-analytics.com/collect?v=1&ul=" + userLanguage + "&t=appview&sr=" + screenResolution + "&an=" + BundleID + "&tid=" + PropertyID + "&aid=" + BundleID + "&cid=" + clientID + "&_u=.sB&av=" + AppVersion + "&_v=ma1b3&cd=" + title + "&qt=2500&z=185";

        // Process the URL
        CoroutineHandler.Start( Process( url ) );
    }

	public static void LogEvent (string titleCat, string titleAction, string titleLabel = "", int value = 0)
	{
		// Get the html chars escaped category and action of the event so it doesn't break the URL request
		titleCat = WWW.EscapeURL ( titleCat );
		titleAction = WWW.EscapeURL ( titleAction );

		// If we sent the action as a string of CLIENT_ID then replace it with the actual client ID
		titleLabel = (titleLabel == "CLIENT_ID" ? clientID : titleLabel);

		// URL which will be pinged to log the event and include details about the user
		var url = "http://www.google-analytics.com/collect?v=1&ul=" + userLanguage + "&t=event&sr=" + screenResolution + "&an=" + BundleID + "&tid=" + PropertyID + "&aid=" + BundleID + "&cid=" + clientID + "&_u=.sB&av=" + AppVersion + "&_v=ma1b3&ec=" + titleCat + "&ea=" + titleAction + "&ev=" + value + "&el=" + titleLabel + "&qt=2500&z=185";

        // Process the URL
        CoroutineHandler.Start( Process( url ) );
    }

	public static void LogError (string description, bool isFatal)
	{
		// Get the html chars escaped description so it doesn't break the URL request
		description = WWW.EscapeURL ( description );

		int fatal = (isFatal ? 1 : 0);

		// URL which will be pinged to log the requested screen and include details about the user
		var url = "http://www.google-analytics.com/collect?v=1&ul=" + userLanguage + "&t=exception&sr=" + screenResolution + "&an=" + BundleID + "&tid=" + PropertyID + "&aid=" + BundleID + "&cid=" + clientID + "&_u=.sB&av=" + AppVersion + "&_v=ma1b3&exd=" + description + "&exf=" + fatal + "&qt=2500&z=185";

		// Process the URL
		CoroutineHandler.Start ( Process (  url ) );
	}

	private static IEnumerator Process (string url)
	{
        using(WWW www = new WWW(url))
        {
            // Wait for the URL to be processed
            yield return www;
            if (!string.IsNullOrEmpty( www.error ))
            {
                Debug.LogError( "GoogleAnalytics: error sending event - " + www.error );
            }
        }
	}

}