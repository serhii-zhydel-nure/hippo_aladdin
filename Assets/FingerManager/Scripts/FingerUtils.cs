﻿using UnityEngine;
using System.Collections.Generic;
using Spine.Unity;
using DG.Tweening;

using System.Linq;

namespace PSV_Tutorials
{
	public enum Directions
	{
		Up,
		Down,
		Left,
		Right,
	}
	static public class FingerUtils
	{
		public static float SWIPE_ANIM_LEENGTH;
		public static float TAP_ANIM_LENGTH;
		public static float TAP_DOWN_ANIM_LENGTH;
		public static float TAP_UP_ANIM_LENGTH;
		public static float SHAKE_ANIM_LENGTH;
		public static float ZOOM_ANIM_LENGTH;
		public static float DOUBLE_TAP_ANIM_LENGTH;
		public static float TWIST_ANIM_LENGTH;
		public static float POINT_ANIM_LENGTH;
		public static float WIPE_ANIM_LENGTH;
		public static float INFINITY = 1000;

		static public void Init (SkeletonDataAsset finger)
		{
			SWIPE_ANIM_LEENGTH = finger.GetAnimLength ("Swipe_Up");
			TAP_ANIM_LENGTH = finger.GetAnimLength ("Tap");
			TAP_DOWN_ANIM_LENGTH = finger.GetAnimLength ("Tap_Down");
			TAP_UP_ANIM_LENGTH = finger.GetAnimLength ("Tap_Up");
			SHAKE_ANIM_LENGTH = finger.GetAnimLength ("Shake");
			ZOOM_ANIM_LENGTH = finger.GetAnimLength ("Zoom_In");
			DOUBLE_TAP_ANIM_LENGTH = finger.GetAnimLength ("Double_Tap");
			TWIST_ANIM_LENGTH = finger.GetAnimLength ("Twist_ClockWise");
			POINT_ANIM_LENGTH = finger.GetAnimLength ("Point_Up");
			WIPE_ANIM_LENGTH = finger.GetAnimLength ("Wipe");
		}

		static public float GetAnimLength (this SkeletonDataAsset asset, string name)
		{
			if (asset == null)
			{
				Debug.LogError ("Finger Manager Is Not Initialised. Call  \"FingerManager.Init()\" in the main thread once");
			}
			return asset.GetSkeletonData (true).FindAnimation (name).Duration;
		}

		static public void SetSpineAnimation (this SkeletonAnimation anim, string clip1, string clip2 = "", bool delay = false, float delayFrom = 0, float delayTo = 0, bool loop = false)
		{
			if (anim != null)
			{
				if (delay)
				{
					DG.Tweening.DOVirtual.DelayedCall (Random.Range (delayFrom, delayTo), () => SetSpineAnimation (anim, clip1, clip2, false));
				}
				else
				{
					if (clip1 != "")
					{
						anim.state.SetAnimation (0, clip1, (clip2 == "" ? loop : false));
						if (clip2 != "")
						{
							anim.state.AddAnimation (0, clip2, loop, 0);
						}
					}			
				}
			}
			else
			{
				Debug.Log ("SetAnimation: Anim is null");
			}
		}

		public static void KillAndClear (this Tween tween)
		{
			if (tween != null && tween.IsActive())
			{
				tween.Kill ();
				tween = null;
			}
		}

		public static void KillAndClear (this Sequence seq)
		{
			if (seq != null && seq.IsActive())
			{
				seq.Kill (true);
				seq = null;
			}
		}

		public static T[] Copy<T> (T[] array)
		{
			T[] res = new T[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				res [i] = array [i];
			}
			return res;
		}

		public static List<T> Copy<T> (List<T> list)
		{
			List<T> res = new List<T> ();
			for (int i = 0; i < list.Count; i++)
			{
				res.Add (list [i]);
			}
			return res;
		}

		public static Spine.TrackEntry SetAnimationAtRandomFrame (this Spine.AnimationState state, int trackIndex, string animationName, bool loop)
		{
			Spine.TrackEntry trackEntry = state.SetAnimation (trackIndex, animationName, loop);
			trackEntry.TrackTime = Random.Range (0, trackEntry.Animation.Duration);

			return trackEntry;
		}

		static public List<T> Shuffle<T> (this List<T> list)
		{
			int n = list.Count;
			List<T> new_list = list.CopyList ();
			while (n > 0)
			{
				int k = Random.Range (0, n);
				T item = new_list [n - 1];
				new_list [n - 1] = new_list [k];
				new_list [k] = item;
				n--;
			}
			return new_list;
		}

		static public List<T> CopyList<T> (this List<T> list)
		{
			int n = list.Count;
			List<T> new_list = new List<T> ();
			for (int i = 0; i < n; i++)
			{
				new_list.Add (list [i]);
			}
			return new_list;
		}

		static public T[] CopyArray<T> (this T[] array)
		{
			int n = array.Length;
			T[] new_array = new T[n];
			for (int i = 0; i < n; i++)
			{
				new_array [i] = array [i];
			}
			return new_array;
		}

		static public T[] Shuffle<T> (this T[] array)
		{
			int n = array.Length;        
			T[] new_array = array.CopyArray ();
			while (n > 0)
			{
				int k = Random.Range (0, n);
				T item = new_array [n - 1];
				new_array [n - 1] = new_array [k];
				new_array [k] = item;
				n--;
			}
			return new_array;
		}
		//	public static DG.Tweening.Core.TweenerCore<Vector3,
		//		DG.Tweening.Plugins.Core.PathCore.Path,
		//		DG.Tweening.Plugins.Options.PathOptions>
		//
		//		SetCustomSpeedBased <DG.Tweening.Core.TweenerCore<Vector3,
		//
		//		DG.Tweening.Plugins.Core.PathCore.Path,
		//		DG.Tweening.Plugins.Options.PathOptions>>
		//
		//		(this DG.Tweening.Core.TweenerCore<Vector3,
		//			  DG.Tweening.Plugins.Core.PathCore.Path,
		//			  DG.Tweening.Plugins.Options.PathOptions> path,
		//		 bool speedBased)
		//	{
		//		if (speedBased)
		//		{
		//			return path.SetSpeedBased ();
		//		}
		//		return path;
		//	}
		//	public static Tween SetCustomSpeedBased(this Tween path, bool speedBased)
		//	{
		//		if (speedBased)
		//		{
		//			return this.SetSpeedBased ();
		//		}
		//		return this;
		//	}
	}
}