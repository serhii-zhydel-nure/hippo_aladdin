﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PSV_Tutorials;

public class Test : MonoBehaviour
{
	// sets of transforms
	public Transform[] array;
	public Transform[] array2;
	public Transform[] array3;

	//Creating a link
	FingerTutorial finger; 

	void Awake ()
	{
		// ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! //
		// OPTIONAL: FingerManager.Init ()  MUST be called ONCE on the Application start   //
		FingerManager.Init ();															   //
		// ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! //

		// Creating a finger tutorial sequence of finger gestures, where each starts after the previous is complete.
		// FingerTutorial will Create one Finger GameObject on the scene that will follow the instructions.
		// In orer to show more fingers simultaneously create more FingerTutorial objects to operate them saperately.
		// FingerTutorial implements most of the DOTween interface. 

		//Constructor
		finger = new FingerTutorial (
			// new ... Drag, DragNDrop, Swipe, Twist, Shake, Point, Wipe, DoubleTap  // Finger gestures

			// CAN USE  
			//
			// OnComlete() 
			// OnUpdate() 
			// OnStepComplete() 
			// SetLoops()
			// OnStart() 
			// SetParent(Transform)(for stationaty only)  Makes the Finger object follow the transform (as if it was a transform's parent)
			// OnWayPointChange() 
			// SetDelay() 
			// SetEase()  
			//
			// FOR EACH GESTURE IN THE SEQUENCE
			
			new Drag (array, 2).SetLoops (-1),   //inits and adds the Drag gesture to the sequence and loops the gesture integer times (-1 for infinity) ),
			new Drag (array, 2).SetLoops (-1, LoopType.Yoyo),  //loops the gesture wih Yoyo type.
			new Drag (array, 2, PathType.CatmullRom).SetLoops (3),// Interpolated path type
			new DragNDrop (array, 2, PathType.Linear).SetLoops (-1),
			new DragNDrop (array, 2, PathType.CatmullRom).SetLoops (-1),//different path type
			new Swipe (array2[0].position, array[array.Length - 1].position).SetLoops (-1),//Swipe gesture Point-to-Point type
			new Tap (array [1].position).SetLoops (-1),
			new Tap (array [1].position,true).SetLoops (-1),//Tap with scaling finger animation
			new Swipe (array2[0].position, array[array.Length - 1].position).SetLoops (-1),
			new Swipe (array2 [0], Directions.Left).SetLoops (-1),// Swipe gesture in the positions (center of the swipe)
			new Swipe (array2 [0], Directions.Right).SetLoops (-1),
			new Swipe (array2 [0], Directions.Up).SetLoops (-1),
			new Swipe (array2 [0], Directions.Down).SetLoops (-1),
			new Twist (array [0].position, RotationType.ClockWise).SetLoops (-1),
			new Shake (array [0]).SetLoops (-1).SetParent(array [0]),
			new Twist (array [0].position, RotationType.CounterClockWise).SetLoops (-1),
			new Point (array [0].position, Directions.Right).SetLoops (-1),//Pointer Finger Pointing the position from different directions
			new Point (array [0].position, Directions.Left).SetLoops (-1),
			new Point (array [0].position, Directions.Up).SetLoops (-1),
			new Point (array [0].position, Directions.Down).SetLoops (-1),
			new Wipe (array [0]).SetLoops (5),
			new DoubleTap (array [0]).SetLoops (-1)
		
		).SetLoops(1000).OnUpdate (() => Check ());
		// Some of the DOTween methods is implemented for the whole tutorial
		// OnUpdate()
		// OnComplete()
		// OnStepComplete()
		// OnStart()
		// SetLoops()

		//Plays the tutorial
		finger.Play ();
		// Hide()
		// Show()
		// Pause()
		// Play()
		// Stop()
		// IsPlaying {get;}
	}

	// My Custon test method to setp into the next gesture in the tutorial sequence and hide/show each
	void Check ()
	{
		if (Input.GetMouseButtonDown (0) || Input.touchCount > 0)
		{
			if (finger.IsPlaying)
			{
				finger.Hide ();
			}
			else
			{
				finger.Show ();
			}
		}
		if (Input.GetMouseButtonDown (1) || Input.touchCount > 0)
		{
			finger.NextStep ();
		}
	}

}
