﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

namespace PSV_Tutorials
{
	public enum FingerAnimations
	{
		Tap_Down,
		Tap,
		Tap_Up,
		Tap_Down_Idle,

		Idle,

		Zoom_In,
		Zoom_Out,

		Double_Tap,
		Tap_Scaling,

		Point_Up,
		Point_Down,
		Point_Left,
		Point_Right,

		Swipe_Up,
		Swipe_Down,
		Swipe_Left,
		Swipe_Right,

		Swipe_Up_Stationary,
		Swipe_Down_Stationary,
		Swipe_Left_Stationary,
		Swipe_Right_Stationary,

		Twist_ClockWise,
		Twist_AntiClockWise,

		Shake,

		Clean,
		Wipe,

		None,
	}

	public class Finger : MonoBehaviour
	{
		[SerializeField] private SkeletonAnimation skeleton;
		private bool avaliable = true;

		public bool isAvaliable
		{
			get
			{
				return avaliable;
			}
		}
			
		public void Pause ()
		{
			skeleton.timeScale = 0;
		}

		public void Hide ()
		{
			gameObject.SetActive (false);
		}

		public void Show ()
		{
			gameObject.SetActive (true);
		}

		public void Resume ()
		{
			skeleton.timeScale = 1;
		}

		public void Release ()
		{
			avaliable = true;
			gameObject.SetActive (false);
		}

		public void Ocupy ()
		{
			avaliable = false;
		}

		public void SetAnimation (FingerAnimations anim, FingerAnimations anim2 = FingerAnimations.None, bool loop = false)
		{
			skeleton.SetSpineAnimation (anim.ToString (), (anim2 != FingerAnimations.None ? anim2.ToString () : ""), loop: loop);
		}

		public void SetAnimation (string anim, string anim2 = "", bool loop = false)
		{
			skeleton.SetSpineAnimation (anim, anim2, loop: loop);
		}
	}
}