﻿using UnityEngine;
using System.Collections;
using PSV_Tutorials;
using DG.Tweening;

public enum ZoomType
{
	In,
	Out,
}

public class Zoom : TutorialPrimitiveBase
{
	private Tween tween;
	private Vector3 tapPosition;
	private ZoomType type;

	/// <summary>
	/// Creates a Zoom gesture in the position due to given type.   	
	/// </summary>
	/// <param name="tapPosition"> Position of the Zoom gesture</param>
	/// <param name="zoomType"> Type of the zoom (in/out)</param>
	public Zoom (Vector3 tapPosition, ZoomType zoomType)
	{
		this.type = zoomType;
		this.tapPosition = tapPosition;
	}

	/// <summary>
	/// Creates a Zoom gesture in the position due to given type.   	
	/// </summary>
	/// <param name="tapTransform"> Transform of the Zoom gesture</param>
	/// <param name="zoomType"> Type of the zoom (in/out)</param>
	public Zoom (Transform tapTransform, ZoomType zoomType)
		: this (tapTransform.position, zoomType)
	{
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		finger.transform.position = tapPosition;
		Kill ();
		onStepCompleteCallbacks.Add (() => Debug.Log ("Step Complete"));
		tween = DOVirtual.Float (0, 1, FingerUtils.ZOOM_ANIM_LENGTH, delegate
			{
			})
			.SetAs (tParams ());
	}
	 
	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		finger.SetAnimation ("Zoom_" + type.ToString (), loop: true);
	}

	public override void Pause ()
	{
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
		tween = null;
	}

	public  Zoom OnComplete (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public Zoom SetDelay (float delay)
	{
		return base.SetDelay<Zoom> (delay);
	}

	public  Zoom SetParent (Transform parent)
	{
		finger.transform.parent = parent;
		return this;
	}

	public  Zoom SetLoops (int cycles)
	{
		return base.SetLoops<Zoom> (cycles);
	}

	public  Zoom OnPause (TweenCallback callback)
	{
		return base.OnPause<Zoom> (callback);
	}

	public  Zoom OnStart (TweenCallback callback)
	{
		return base.OnStart<Zoom> (callback);
	}

	public  Zoom OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public  Zoom OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<Zoom> (callback);
	}
}
