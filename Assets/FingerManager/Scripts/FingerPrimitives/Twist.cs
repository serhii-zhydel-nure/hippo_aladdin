﻿using UnityEngine;
using System.Collections;
using PSV_Tutorials;
using DG.Tweening;

public enum RotationType
{
	ClockWise,
	CounterClockWise,
}

public class Twist : TutorialPrimitiveBase
{

	private Tween tween;
	private Vector3 twistPosition;
	private RotationType rotationType;

	/// <summary>
	/// Creates a Twist gesture in the position due to given type.   	
	/// </summary>
	/// <param name="tapPosition"> Position of the Twist gesture</param>
	/// <param name="zoomType"> Type of the Twist (Clockwise/Counter)</param>
	public Twist (Vector3 twistPosition, RotationType rotationType)
	{
		this.rotationType = rotationType;
		this.twistPosition = twistPosition;
	}

	/// <summary>
	/// Creates a Twist gesture in the Transform due to given type.   	
	/// </summary>
	/// <param name="twistTransform"> Transform of the Twist gesture</param>
	/// <param name="zoomType"> Type of the Twist (Clockwise/Counter)</param>
	public Twist (Transform twistTransform, RotationType rotationType)
		: this (twistTransform.position, rotationType)
	{
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		finger.transform.position = twistPosition;
		Kill ();
		tween = DOVirtual.Float (0, 1, FingerUtils.TWIST_ANIM_LENGTH, delegate
			{
			})
			.SetAs (tParams ());
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		finger.SetAnimation ("Twist_" + rotationType.ToString (), loop: true);
	}

	public override void Pause ()
	{
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
	}

	public  Twist OnComplete (TweenCallback callback)
	{
		return base.OnComplete<Twist> (callback);
	}

	public Twist SetDelay (float delay)
	{
		return base.SetDelay<Twist> (delay);
	}
	/// <summary>
	/// Makes the gesture follow parent position.   	
	/// </summary>
	public  Twist SetParent (Transform parent)
	{
		finger.transform.parent = parent;
		onUpdateCallbacks.Add (() => finger.transform.position = parent.position);
		return this;
	}

	public  Twist SetLoops (int cycles)
	{
		return base.SetLoops<Twist> (cycles);
	}

	public  Twist OnPause (TweenCallback callback)
	{
		return base.OnPause<Twist> (callback);
	}

	public  Twist OnStart (TweenCallback callback)
	{
		return base.OnStart<Twist> (callback);
	}

	public  Twist OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public  Twist OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<Twist> (callback);
	}
}