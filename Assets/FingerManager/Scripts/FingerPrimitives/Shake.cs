﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PSV_Tutorials;

public class Shake : TutorialPrimitiveBase
{
	private Tween tween;
	private Vector3 shakePosition;
	private ZoomType type;
	private Transform parent;

	/// <summary>
	/// Creates a Shake gesture in the position.   	
	/// </summary>
	/// <param name="shakePosition"> Position of the Shake gesture</param>
	public Shake (Vector3 shakePosition)
	{
		this.shakePosition = shakePosition;
	}

	/// <summary>
	/// Creates a Shake gesture in the Transform's position.   	
	/// </summary>
	/// <param name="shakeTransform"> Transform of the Shake gesture</param>
	public Shake (Transform shakeTransform)
		: this (shakeTransform.position)
	{
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		finger.transform.position = shakePosition;
		Kill ();
		tween = DOVirtual.Float (0, 1, FingerUtils.SHAKE_ANIM_LENGTH, delegate
			{
			})
			.SetAs (tParams ());
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		finger.SetAnimation (FingerAnimations.Shake, loop: true);
	}

	public override void Pause ()
	{
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
		tween = null;
	}

	public  Shake OnComplete (TweenCallback callback)
	{
		return base.OnComplete<Shake> (callback);
	}
	/// <summary>
	/// A deley before the gesture starts   	
	/// </summary>
	public Shake SetDelay (float delay)
	{
		return base.SetDelay<Shake> (delay);
	}
	/// <summary>
	/// Makes the gesture follow parent position.   	
	/// </summary>
	public  Shake SetParent (Transform parent)
	{
		this.parent = parent;
		onUpdateCallbacks.Add (() => finger.transform.position = this.parent.position);
		return this;
	}

	public  Shake SetLoops (int cycles)
	{
		return base.SetLoops<Shake> (cycles);
	}

	public  Shake OnPause (TweenCallback callback)
	{
		return base.OnPause<Shake> (callback);
	}

	public  Shake OnStart (TweenCallback callback)
	{
		return base.OnStart<Shake> (callback);
	}

	public  Shake OnUpdate (TweenCallback callback)
	{
		return base.OnUpdate<Shake> (callback);
	}

	public  Shake OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<Shake> (callback);
	}
}
