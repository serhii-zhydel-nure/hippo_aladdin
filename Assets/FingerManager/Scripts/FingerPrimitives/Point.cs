﻿using UnityEngine;
using System.Collections;
using PSV_Tutorials;
using DG.Tweening;

public class Point : TutorialPrimitiveBase
{
	private Tween tween;
	private Vector3 pointPosition;
	private Transform parent;
	private Directions direction;

	/// <summary>
	/// Creates a Point gesture in the position due to the direction.   	
	/// </summary>
	/// <param name="tapPosition"> Position of the TapDown </param>
	/// <param name="pointDirection"> Direction the finger appears </param>
	public Point (Vector3 pointPosition, Directions pointDirection = Directions.Right)
	{
		this.pointPosition = pointPosition;
		this.direction = pointDirection;
	}

	/// <summary>
	/// Creates a Point gesture in the Transform's position.   	
	/// </summary>
	/// <param name="tapTransform"> Transform of the TapDown </param>
	/// <param name="pointDirection"> Direction the finger appears </param>
	public Point (Transform pointTransform, Directions direction = Directions.Right)
		: this (pointTransform.position, direction)
	{
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		Kill ();
		finger.transform.position = pointPosition;
		tween = DOVirtual.Float (0, 1, FingerUtils.POINT_ANIM_LENGTH, delegate
			{
			})
			.SetAs (tParams ());
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		finger.SetAnimation ("Point_"+direction.ToString(), loop: true);
	}

	public override void Pause ()
	{
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
	}

	public  Point OnComplete (TweenCallback callback)
	{
		return base.OnComplete<Point> (callback);
	}

	public Point SetDelay (float delay)
	{
		return base.SetDelay<Point> (delay);
	}
	/// <summary>
	/// Makes the gesture follow parent position.   	
	/// </summary>
	public  Point SetParent (Transform parent)
	{
		this.parent = parent;
		onUpdateCallbacks.Add (() => finger.transform.position = this.parent.position);
		return this;
	}

	public  Point SetLoops (int cycles)
	{
		return base.SetLoops<Point> (cycles);
	}

	public  Point OnPause (TweenCallback callback)
	{
		return base.OnPause<Point> (callback);
	}

	public  Point OnStart (TweenCallback callback)
	{
		return base.OnStart<Point> (callback);
	}

	public  Point OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public  Point OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<Point> (callback);
	}
}
