﻿using UnityEngine;
using System.Collections;
using PSV_Tutorials;
using DG.Tweening;

public class DoubleTap : TutorialPrimitiveBase
{
	private Tween tween;
	private Vector3 tapPosition;
	private Transform parent;

	/// <summary>
	/// Creates a Tap gesture in the position.   	
	/// </summary>
	/// <param name="tapPosition"> Position of the TapDown </param>
	public DoubleTap (Vector3 tapPosition, bool scaleWhileTap = false)
	{
		this.tapPosition = tapPosition;
	}

	/// <summary>
	/// Creates a Tap gesture in the Transform's position.   	
	/// </summary>
	/// <param name="tapTransform"> Transform of the TapDown </param>
	public DoubleTap (Transform tapTransform, bool scaleWhileTap = false)
		: this (tapTransform.position, scaleWhileTap)
	{
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		Kill ();
		finger.transform.position = tapPosition;
		tween = DOVirtual.Float (0, 1, FingerUtils.DOUBLE_TAP_ANIM_LENGTH, delegate
			{
			})
			.SetAs (tParams ());
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		finger.SetAnimation (FingerAnimations.Double_Tap, loop: true);
	}

	public override void Pause ()
	{
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
	}

	public  DoubleTap OnComplete (TweenCallback callback)
	{
		return base.OnComplete<DoubleTap> (callback);
	}

	public DoubleTap SetDelay (float delay)
	{
		return base.SetDelay<DoubleTap> (delay);
	}

	/// <summary>
	/// Makes the gesture follow parent position.   	
	/// </summary>
	public  DoubleTap SetParent (Transform parent)
	{
		this.parent = parent;
		onUpdateCallbacks.Add (() => finger.transform.position = this.parent.position);
		return this;
	}

	public  DoubleTap SetLoops (int cycles)
	{
		return base.SetLoops<DoubleTap> (cycles);
	}

	public  DoubleTap OnPause (TweenCallback callback)
	{
		return base.OnPause<DoubleTap> (callback);
	}

	public  DoubleTap OnStart (TweenCallback callback)
	{
		return base.OnStart<DoubleTap> (callback);
	}

	public  DoubleTap OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public  DoubleTap OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<DoubleTap> (callback);
	}
}
