﻿using UnityEngine;
using System.Collections;
using System.Linq;
using PSV_Tutorials;
using DG.Tweening;

public class Drag : TutorialPrimitiveBase
{
	private Tween tween;
	private Vector3[] dragPositions;
	private PathType type;
	private PathMode mode;

	/// <summary>
	/// Creates a Drag gesture through given points.   	
	/// </summary>
	/// <param name="positions"> Positions for the Drag gesture </param>
	/// <param name="duration"> Duration of the Drag gesture</param>
	public Drag (Vector3[] positions, float duration, PathType pathType = PathType.Linear, PathMode pathMode = PathMode.Full3D)
	{
		this.duration = duration;
		this.dragPositions = FingerUtils.Copy (positions);
		this.mode = pathMode;
		this.type = pathType;
	}

	/// <summary>
	/// Creates a Drag gesture through given points.   	
	/// </summary>
	/// <param name="startTransform"> start Transform of the Drag gesture </param>
	/// <param name="endTransform"> end Transform or the Drag gesture </param>
	/// <param name="duration"> Duration of the Drag gesture</param>
	public Drag (Transform startTransform, Transform endTransform, float duration, PathType pathType = PathType.Linear, PathMode pathMode = PathMode.Full3D)
		: this (new Transform[]{ startTransform, endTransform }, duration, pathType, pathMode)
	{
	}

	/// <summary>
	/// Creates a Drag gesture through given points.   	
	/// </summary>
	/// <param name="startPosition"> start Position of the Drag gesture </param>
	/// <param name="endPosition"> End Position of the Drag gesture </param>
	public Drag (Vector3 startPosition, Vector3 endPosition, float duration, PathType pathType = PathType.Linear, PathMode pathMode = PathMode.Full3D)
		: this (new Vector3[]{ startPosition, endPosition }, duration, pathType, pathMode)
	{
	}

	/// <summary>
	/// Creates a Drag gesture through given points.   	
	/// </summary>
	/// <param name="positions"> Positions or the Drag gesture </param>
	/// <param name="duration"> Duration of the Drag gesture</param>
	public Drag (Transform[] positions, float duration, PathType pathType = PathType.Linear, PathMode pathMode = PathMode.Full3D)
		: this (positions.Select (tr => tr.position).ToArray (), duration, pathType, pathMode)
	{
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		finger.transform.position = dragPositions [0];
		Kill ();
		tween = finger.transform.DOPath (dragPositions, duration, type, mode)
			.SetAs (tParams ());
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		finger.SetAnimation (FingerAnimations.Tap_Down_Idle, loop: true);
	}

	public override void Pause ()
	{
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
		tween = null;
	}

	public  Drag OnComplete (TweenCallback callback)
	{
		return base.OnComplete<Drag> (callback);
	}

	public Drag SetDelay (float delay)
	{
		return base.SetDelay<Drag> (delay);
	}

	public  Drag SetParent (Transform parent)
	{
		finger.transform.parent = parent;
		return this;
	}

	public  Drag SetLoops (int cycles, LoopType type = LoopType.Restart)
	{
		return base.SetLoops<Drag> (cycles, type: type);
	}

	public  Drag OnPause (TweenCallback callback)
	{
		return base.OnPause<Drag> (callback);
	}

	public  Drag OnStart (TweenCallback callback)
	{
		return base.OnStart<Drag> (callback);
	}

	public  Drag OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public  Drag OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<Drag> (callback);
	}
}
