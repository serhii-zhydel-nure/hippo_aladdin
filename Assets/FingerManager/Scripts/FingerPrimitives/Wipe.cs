﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PSV_Tutorials;

public class Wipe : TutorialPrimitiveBase
{
	private Tween tween;
	private Vector3 cleanPosition;
	private Transform parent;

	/// <summary>
	/// Creates a Clean gesture in the position.   	
	/// </summary>
	/// <param name="tapPosition"> Position of the Clean gesture </param>
	public Wipe (Vector3 cleanPosition)
	{
		this.cleanPosition = cleanPosition;
	}

	/// <summary>
	/// Creates a Clean gesture in the Transform's position.   	
	/// </summary> 
	/// <param name="tapTransform"> Transform of the Clean gesture </param>
	public Wipe (Transform cleanTransform)
		: this (cleanTransform.position)
	{
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		finger.transform.position = cleanPosition;
		Kill ();
		tween = DOVirtual.Float (0, 1, FingerUtils.WIPE_ANIM_LENGTH, delegate
			{
			})
			.SetAs (tParams ());
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		finger.SetAnimation (FingerAnimations.Wipe, loop: true);
	}

	public override void Pause ()
	{
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
	}

	public  Wipe OnComplete (TweenCallback callback)
	{
		return base.OnComplete<Wipe> (callback);
	}

	public Wipe SetDelay (float delay)
	{
		return base.SetDelay<Wipe> (delay);
	}

	public  Wipe SetParent (Transform parent)
	{
		this.parent = parent;
		onUpdateCallbacks.Add (() => finger.transform.position = this.parent.position);
		return this;
	}

	public  Wipe SetLoops (int cycles)
	{
		return base.SetLoops<Wipe> (cycles);
	}

	public  Wipe OnPause (TweenCallback callback)
	{
		return base.OnPause<Wipe> (callback);
	}

	public  Wipe OnStart (TweenCallback callback)
	{
		return base.OnStart<Wipe> (callback);
	}

	public  Wipe OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public  Wipe OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<Wipe> (callback);
	}
}
