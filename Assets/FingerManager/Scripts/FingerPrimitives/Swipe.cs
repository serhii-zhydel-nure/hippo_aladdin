﻿using UnityEngine;
using System.Collections;
using PSV_Tutorials;
using Spine.Unity;
using DG.Tweening;

public class Swipe : TutorialPrimitiveBase
{
	private enum SwipeType
	{
		Position,
		Interval,
	}

	private Tween tween;
	private Vector3 position;
	private Vector3 startPosition;
	private Vector3 endPosition;
	private Directions direction;
	private SwipeType swipeType;
	private FingerAnimations swipeAnimation;

	/// <summary>
	/// Creates a Swipe gesture in the position due to direction.   	
	/// </summary>
	/// <param name="swipePosition"> Center of the Swipe gesture center</param>
	/// <param name="swipeDirection"> Direction of the Swipe gesture</param>
	public Swipe (Vector3 swipePosition, Directions swipeDirection)
	{
		direction = swipeDirection;
		position = swipePosition;
		swipeType = SwipeType.Position;
	}

	/// <summary>
	/// Creates a Swipe gesture in the position due to Direction.   	
	/// </summary>
	/// <param name="swipeTransform"> Transform of the Swipe gesture center gesture</param>
	/// <param name="swipeDirection"> Direction of the Swipe gesture</param>
	public Swipe (Transform swipeTransform, Directions swipeDirection)
		: this (swipeTransform.position, swipeDirection)
	{
	}

	/// <summary>
	/// Creates a Swipe gesture between two positions.   	
	/// </summary>
	/// <param name="start"> Start position of the Swipe gesture</param>
	/// <param name="end"> End position of the Swipe gesture</param>
	public Swipe (Vector3 start, Vector3 end)
	{
		startPosition = start;
		endPosition = end;
		swipeType = SwipeType.Interval;
	}

	/// <summary>
	/// Creates a Swipe gesture between two Transform.   	
	/// </summary>
	/// <param name="start"> Start Transform of the Swipe gesture</param>
	/// <param name="end"> End Transform of the Swipe gesture</param>
	public Swipe (Transform start, Transform end)
		: this (start.position, end.position)
	{
	}

	private void CreateIntervalSwipe ()
	{
		DefineAnimation ();
		finger.SetAnimation (swipeAnimation, loop: true);
		finger.transform.position = startPosition;
		tween = finger.transform.DOMove (endPosition, FingerUtils.SWIPE_ANIM_LEENGTH)
			.SetAs (tParams ());
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		if (swipeType == SwipeType.Interval)
		{
		
			if (Mathf.Abs (startPosition.y - endPosition.y) > Mathf.Abs (startPosition.x - endPosition.x))
			{
				if (startPosition.y > endPosition.y)
				{
					swipeAnimation = FingerAnimations.Swipe_Down_Stationary;
				}
				else
				{
					swipeAnimation = FingerAnimations.Swipe_Up_Stationary;
				}
			}
			else
			{
				if (startPosition.x > endPosition.x)
				{
					swipeAnimation = FingerAnimations.Swipe_Left_Stationary;
				}
				else
				{
					swipeAnimation = FingerAnimations.Swipe_Right_Stationary;
				}
			}
		}
		else
		{
			finger.SetAnimation (("Swipe_" + direction.ToString ()), loop: true);
		}
	}

	private void CreatePositionSwipe ()
	{
		tween.KillAndClear ();
		tween = DOVirtual.Float (0, 1, FingerUtils.SWIPE_ANIM_LEENGTH, delegate
			{
			})
			.SetAs (tParams ());
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		finger.transform.position = position;
		Kill ();
		if (swipeType == SwipeType.Interval)
		{
			CreateIntervalSwipe ();
		}
		else
		{
			CreatePositionSwipe ();
		}
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	public override void Pause ()
	{
		tween.Pause ();
		finger.Pause ();
	}
		
	public Swipe SetLoops (int loops)
	{
		return base.SetLoops<Swipe> (loops);
	}

	public Swipe OnComplete (TweenCallback callback)
	{
		return base.OnComplete<Swipe> (callback);
	}

	public Swipe OnStart (TweenCallback callback)
	{
		return base.OnStart<Swipe> (callback);
	}

	public Swipe SetUpdate (bool unscaledTime)
	{
		return base.SetUpdate<Swipe> (unscaledTime);
	}

	public Swipe OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public Swipe OnPause (TweenCallback callback)
	{
		return base.OnPause<Swipe> (callback);
	}
}
