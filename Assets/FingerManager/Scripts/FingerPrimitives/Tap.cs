﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using PSV_Tutorials;

public class Tap : TutorialPrimitiveBase
{
	private Tween tween;
	private Vector3 tapPosition;
	private Transform parent;
	private bool scaleWhileTap = false;

	/// <summary>
	/// Creates a Tap gesture in the position.   	
	/// </summary>
	/// <param name="tapPosition"> Position of the TapDown </param>
	/// <param name="scaleWhileTap"> Use scale animation of the Tap</param>
	public Tap (Vector3 tapPosition, bool scaleWhileTap = false)
	{
		this.tapPosition = tapPosition;
		this.scaleWhileTap = scaleWhileTap;
	}

	/// <summary>
	/// Creates a Tap gesture in the Transform's position.   	
	/// </summary>
	/// <param name="tapTransform"> Transform of the TapDown </param>
	/// <param name="scaleWhileTap"> Use scale animation of the Tap</param>
	public Tap (Transform tapTransform, bool scaleWhileTap = false)
		: this (tapTransform.position, scaleWhileTap)
	{
	}

	public override void Kill ()
	{
		tween.KillAndClear ();
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		Kill ();
		finger.transform.position = tapPosition;
		tween = DOVirtual.Float (0, 1, FingerUtils.TAP_ANIM_LENGTH, delegate
			{
			})
			.SetAs (tParams ());
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		if (scaleWhileTap)
		{
			finger.SetAnimation (FingerAnimations.Tap_Scaling, loop: true);
		}
		else
		{
			finger.SetAnimation (FingerAnimations.Tap, loop: true);
		}
		
	}

	public override void Pause ()
	{
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill ();
		StartCallbacks (onCompleteCallbacks);
	}

	public  Tap OnComplete (TweenCallback callback)
	{
		return base.OnComplete<Tap> (callback);
	}

	public Tap SetDelay (float delay)
	{
		return base.SetDelay<Tap> (delay);
	}
	/// <summary>
	/// Makes the gesture follow parent position.   	
	/// </summary>
	public  Tap SetParent (Transform parent)
	{
		this.parent = parent;
		onUpdateCallbacks.Add (() => finger.transform.position = this.parent.position);
		return this;
	}

	public  Tap SetLoops (int cycles)
	{
		return base.SetLoops<Tap> (cycles);
	}

	public  Tap OnPause (TweenCallback callback)
	{
		return base.OnPause<Tap> (callback);
	}

	public  Tap OnStart (TweenCallback callback)
	{
		return base.OnStart<Tap> (callback);
	}

	public  Tap OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}

	public  Tap OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<Tap> (callback);
	}
}
