﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using DG.Tweening;
using PSV_Tutorials;

public class DragNDrop : TutorialPrimitiveBase
{
	private Vector3[] wayPoints;
	private PathType type;
	private PathMode mode;
	Sequence drag = DOTween.Sequence ();
	Tween tapDownTween;
	Tween dragTween;
	Tween tapUpTween;

	/// <summary>
	/// Creates a DragAndDrop from startPpoint to the endPoint.   	
	/// </summary>
	/// <param name="startPpoint"> start point of the DragAndDrop gesture </param>
	/// <param name="endPoint"> end point of the DragAndDrop gesture </param>
	/// <param name="duration"> duration of the DragAndDrop gesture </param>
	public DragNDrop (Vector3 startPpoint, Vector3 endPoint, float duration)
		: this (new Vector3[] { startPpoint, endPoint }, duration)
	{
	}

	/// <summary>
	/// Creates a DragAndDrop from startPpoint to the endPoint.   	
	/// </summary>
	/// <param name="startPoint"> start Transform of the DragAndDrop gesture </param>
	/// <param name="endPoint"> end Transform of the DragAndDrop gesture </param>
	/// <param name="duration"> duration of the DragAndDrop gesture </param>
	/// <param name="type">path type of the DragAndDrop gesture </param>
	/// <param name="mode"> path mode of the DragAndDrop gesture </param>
	public DragNDrop (Transform startPoint, Transform endPoint, float duration, PathType type = PathType.CatmullRom, PathMode mode = PathMode.Full3D)
		: this (new Vector3[] { startPoint.position, endPoint.position }, duration, type, mode)
	{
	}

	/// <summary>
	///	 Creates a DragAndDrop thruogh the given Transforms.
	/// </summary>
	/// <param name="transforms"> Transform of the DragAndDrop gesture </param>
	/// <param name="duration"> duration of the DragAndDrop gesture </param>
	/// <param name="type">path type of the DragAndDrop gesture </param>
	/// <param name="mode"> path mode of the DragAndDrop gesture </param>
	public DragNDrop (Transform[] transforms, float duration, PathType type = PathType.CatmullRom, PathMode mode = PathMode.Full3D)
		: this (transforms.Select (pos => pos.position).ToArray (), duration, type, mode)
	{
	}

	/// <summary>
	/// Creates a DragAndDrop thruogh the given positions.   	
	/// </summary>
	/// <param name="points"> positions of the DragAndDrop gesture </param>
	/// <param name="duration"> duration of the DragAndDrop gesture </param>
	/// <param name="type">path type of the DragAndDrop gesture </param>
	/// <param name="mode"> path mode of the DragAndDrop gesture </param>
	public DragNDrop (Vector3[] points, float duration, PathType type = PathType.CatmullRom, PathMode mode = PathMode.Full3D)
	{
		onStepCompleteCallbacks.Add (() => finger.SetAnimation (FingerAnimations.Tap_Down, FingerAnimations.Tap_Down_Idle));

		wayPoints = FingerUtils.Copy (points);
		this.duration = duration;
		this.mode = mode;
		this.type = type;
	}

	public override void Kill ()
	{
		
		drag.KillAndClear ();
	}

	public override void Pause ()
	{
		drag.Pause ();
		finger.Pause ();
	}

	public override void Complete ()
	{
		Kill();
		StartCallbacks (onCompleteCallbacks);
	}

	protected override void CreateTweens ()
	{
		base.CreateTweens ();
		finger.transform.position = wayPoints [0];
		Kill ();
		drag = DOTween.Sequence ();
		drag.Append (DOVirtual.Float (0, 1, FingerUtils.TAP_DOWN_ANIM_LENGTH, delegate
				{
				})
						.OnUpdate (() => StartCallbacks (onUpdateCallbacks))
						.OnStart (() => StartCallbacks (onStartCallbacks)
			)
		);
	
		drag.Append (finger.transform.DOPath (wayPoints, duration, type, mode)
			.OnStart (() => StartCallbacks (onStartCallbacks))
			.OnPause (() => StartCallbacks (onPauseCallbacks))
			.OnUpdate (() => StartCallbacks (onUpdateCallbacks))
			.SetSpeedBased (speedBased)
			.SetEase (ease)
			.SetUpdate (unscaledTime)
			.OnComplete (() => finger.SetAnimation (FingerAnimations.Tap_Up, FingerAnimations.Idle))
			.OnWaypointChange (delegate
				{
					StartCallbacks (onUpdateCallbacks);
				})
		);
		drag.Append (DOVirtual.Float (0, 1, FingerUtils.TAP_DOWN_ANIM_LENGTH, delegate
				{
				})
			.OnUpdate (() => StartCallbacks (onUpdateCallbacks)));
		drag.OnStepComplete (() => StartCallbacks (onStepCompleteCallbacks));
		drag.SetLoops (loops, loopType);
		drag.OnComplete (() => StartCallbacks (onCompleteCallbacks));
	}

	protected override void DefineAnimation ()
	{
		base.DefineAnimation ();
		finger.SetAnimation (FingerAnimations.Tap_Down, FingerAnimations.Tap_Down_Idle);
	}

	public  DragNDrop OnComplete (TweenCallback callback)
	{
		return base.OnComplete<DragNDrop> (callback);
	}

	public  DragNDrop OnPause (TweenCallback callback)
	{
		return base.OnPause<DragNDrop> (callback);
	}

	public  DragNDrop OnStepComplete (TweenCallback callback)
	{
		return base.OnStepComplete<DragNDrop> (callback);
	}

	public  DragNDrop OnWayPointChange (TweenCallback callback)
	{
		return base.OnStepComplete<DragNDrop> (callback);
	}

	public  DragNDrop SetPathMode (PathMode mode)
	{
		this.mode = mode;
		return  this;
	}

	public  DragNDrop SetPathType (PathType type)
	{
		this.type = type;
		return  this;
	}

	public DragNDrop OnStart (TweenCallback callback)
	{
		return base.OnStart<DragNDrop> (callback);
	}

	public DragNDrop SetDelay (float delay)
	{
		return base.SetDelay<DragNDrop> (delay);
	}

	public  DragNDrop SetEase (Ease ease)
	{
		return base.SetEase<DragNDrop> (ease);
	}

	public  DragNDrop SetLoops (int cycles, LoopType type = LoopType.Restart)
	{
		return base.SetLoops<DragNDrop> (cycles, type);
	}

	public  DragNDrop SetSpeedBase ()
	{
		return base.SetSpeedBase<DragNDrop> ();
	}

	public  DragNDrop SetUpdate (bool unscaledTime)
	{
		return base.SetUpdate<DragNDrop> (unscaledTime);
	}

	public  DragNDrop OnUpdate (TweenCallback callback)
	{
		onUpdateCallbacks.Add (callback);
		return this;
	}
		
}
