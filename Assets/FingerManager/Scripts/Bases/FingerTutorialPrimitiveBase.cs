﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;

namespace PSV_Tutorials
{
	public abstract class TutorialPrimitiveBase
	{
		protected List<TweenCallback> onStartCallbacks = new List<TweenCallback> ();
		protected List<TweenCallback> onCompleteCallbacks = new List<TweenCallback> ();
		protected List<TweenCallback> onStepCompleteCallbacks = new List<TweenCallback> ();
		protected List<TweenCallback> onPauseCallbacks = new List<TweenCallback> ();
		protected List<TweenCallback> onUpdateCallbacks = new List<TweenCallback> ();
		protected List<TweenCallback> onWaypointChangeCallbacks = new List<TweenCallback> ();

		protected int loops = 1;
		protected LoopType loopType = LoopType.Restart;
		protected float duration = 1;
		protected float delay = 0;
		protected Ease ease = Ease.Linear;
		protected bool speedBased = false;
		protected bool unscaledTime = false;
		protected Finger finger;

		/// <summary>
		/// Kills the gesture.   	
		/// </summary>
		public abstract void Kill ();

		/// <summary>
		/// Pauses the gesture.   	
		/// </summary>
		public abstract void Pause ();

		/// <summary>
		/// Complete the gesture (OnComplete() is called).   	
		/// </summary>
		public abstract void Complete ();

		public void SetFinger (Finger finger)
		{
			this.finger = finger;
		}

		protected TweenParams tParams ()
		{
			return new TweenParams ()
				.OnStart (() => StartCallbacks (onStartCallbacks))
				.OnUpdate (() => StartCallbacks (onUpdateCallbacks))
				.OnStepComplete (() => StartCallbacks (onStepCompleteCallbacks))
				.OnWaypointChange (delegate
				{
					StartCallbacks (onWaypointChangeCallbacks);
				})
				.OnComplete (() => StartCallbacks (onCompleteCallbacks))
				.SetLoops (loops, loopType)
				.SetEase (ease)
				.SetUpdate (unscaledTime)
				.SetDelay (delay)
				.SetSpeedBased (speedBased);
		}

		protected virtual void DefineAnimation ()
		{
		
		}

		protected virtual void CreateTweens ()
		{
		
		}

		public void Play ()
		{
			finger.Show ();
			DefineAnimation ();
			CreateTweens ();
		}

		protected void StartCallbacks (List<TweenCallback> callbacksList)
		{
			foreach (TweenCallback callback in callbacksList)
			{
				callback ();
			}	
		}

		public T OnStart<T> (TweenCallback callback) where T : TutorialPrimitiveBase
		{
			onStartCallbacks.Add (callback);
			return this as T;
		}

		public T OnComplete<T> (TweenCallback callback)where T : TutorialPrimitiveBase
		{
			onCompleteCallbacks.Add (callback);
			return this as T;
		}

		public T OnPause<T> (TweenCallback callback)where T : TutorialPrimitiveBase
		{
			onPauseCallbacks.Add (callback);
			return this as T;
		}

		public T OnStepComplete<T> (TweenCallback callback)where T : TutorialPrimitiveBase
		{
			onStepCompleteCallbacks.Add (callback);
			return this as T;
		}

		public T SetDelay<T> (float delay)where T : TutorialPrimitiveBase
		{
			this.delay = delay;
			return this as T;
		}

		public T  OnUpdate<T> (TweenCallback callback)where T : TutorialPrimitiveBase
		{
			onUpdateCallbacks.Add (callback);
			return this as T;
		}

		public T OnWayPointChange<T> (TweenCallback callback)where T : TutorialPrimitiveBase
		{
			onWaypointChangeCallbacks.Add (callback);
			return this as T;
		}

		public T SetLoops<T> (int cycles, LoopType type = LoopType.Restart)where T : TutorialPrimitiveBase
		{
			loops = cycles;
			loopType = type; 
			return this as T;
		}

		public T SetSpeedBase<T> ()where T : TutorialPrimitiveBase
		{
			speedBased = true;
			return this as T;
		}

		public virtual T SetEase<T> (Ease ease)where T : TutorialPrimitiveBase
		{
			this.ease = ease;
			return this as T;
		}

		public T SetUpdate<T> (bool unscaledTime)where T : TutorialPrimitiveBase
		{
			this.unscaledTime = unscaledTime;
			return this as T;
		}

	}

	//	public interface ITutorialPrimitive<T>
	//	{
	//		T Play ();
	//
	//		T Kill ();
	//
	//		T OnStart (TweenCallback callback);
	//
	//		T OnComplete (TweenCallback callback);
	//
	//		T OnCycleEnd (TweenCallback callback);
	//
	//		T SetDelay (float delay);
	//
	//		T Pause ();
	//
	//		T SetLoop (int cycles);
	//
	//		T SetSpeedBase ();
	//
	//		T SetEase (Ease ease);
	//
	//		T SetUpdate (bool unscaledTime);
	//	}

	[System.Serializable]

	public class FingerTutorial
	{
		private List<TweenCallback> OnCompleteCallbacks;
		private int loops = 1;
		private Finger finger;
		private TutorialPrimitiveBase activePrimitive;
		private TutorialPrimitiveBase firstPrimitive;
		private TutorialPrimitiveBase lastPrimitive;
		private bool isPlaying;
		private Tween hideFingerTween;
		private TweenCallback onUpdateCallback;

		private  List<TutorialPrimitiveBase> primitiveList = new List<TutorialPrimitiveBase> ();

		public FingerTutorial ()
		{
			primitiveList = new List<TutorialPrimitiveBase> ();
		}

		public bool IsPlaying
		{
			get
			{
				return isPlaying;
			}
		}

		public FingerTutorial (TutorialPrimitiveBase primitive) : this (new TutorialPrimitiveBase[]{ primitive })
		{
			
		}

		public FingerTutorial (params TutorialPrimitiveBase[] array)
		{
			OnCompleteCallbacks = new List<TweenCallback> ();
			primitiveList = FingerUtils.Copy (array).ToList ();
			firstPrimitive = primitiveList [0];
			finger = FingerManager.GetAvaliableFinger ();
			foreach (TutorialPrimitiveBase primitive in array)
			{
				primitive.SetFinger (finger);
			}
			if (primitiveList.Count > 1)
			{
				for (int i = 0; i < primitiveList.Count - 1; i++)
				{
					TutorialPrimitiveBase nextPrimitive = primitiveList [i + 1];
					primitiveList [i].OnComplete<TutorialPrimitiveBase> (() => PlayPrimitive (nextPrimitive));
				}
				lastPrimitive = primitiveList [primitiveList.Count - 1];
				lastPrimitive.OnComplete<TutorialPrimitiveBase> (() => AddLoop ());
			}
			else
			{
				firstPrimitive.OnComplete<TutorialPrimitiveBase> ( () => Stop ());
			}
		}

		private void StartCallbacks (List<TweenCallback> callbacksList)
		{
			foreach (TweenCallback callback in callbacksList)
			{
				callback ();
			}	
		}

		private void PlayPrimitive (TutorialPrimitiveBase primitive)
		{
			activePrimitive = primitive;
			activePrimitive.Play ();
		}

		private void AddLoop ()
		{
			loops--;
			if (loops > 0)
			{
				PlayPrimitive (firstPrimitive);
			}
			else
			{
				activePrimitive.Kill ();
				Complete ();
			}
		}

		private void Complete ()
		{
			hideFingerTween.KillAndClear ();
			finger.Release ();
			StartCallbacks (OnCompleteCallbacks);
			isPlaying = false;
		}

		/// <summary>
		/// Shows the finger if was hidden.   	
		/// </summary>
		public void Show ()
		{
			hideFingerTween.KillAndClear ();
			finger.Show ();
			activePrimitive.Play ();
			isPlaying = true;
		}

		/// <summary>
		/// Makes the finger invisible.   	
		/// </summary>
		public void Hide ()
		{
			finger.Hide ();
			activePrimitive.Kill ();
			hideFingerTween = DOVirtual.Float (0, 1, FingerUtils.INFINITY,delegate {}).OnUpdate (onUpdateCallback).SetLoops(-1);
			isPlaying = false;
		}

		/// <summary>
		/// Plays the tutorisl from the very beginning.   	
		/// </summary>
		public void Play ()
		{
			hideFingerTween.KillAndClear ();
			if (activePrimitive != null)
			{
				activePrimitive.Kill ();
			}
			PlayPrimitive (firstPrimitive);
			finger.Resume ();
			isPlaying = true;
		}

		/// <summary>
		/// Stops the finger tutorial.   	
		/// </summary>
		public void Stop ()
		{
			hideFingerTween.KillAndClear ();
			foreach (TutorialPrimitiveBase primitive in primitiveList)
			{
				primitive.Kill ();
			}
			finger.Release ();
			isPlaying = false;
		}

		/// <summary>
		/// Freezes the finger tutorial.   	
		/// </summary>
		public void Pause ()
		{
			hideFingerTween.KillAndClear ();
			activePrimitive.Pause ();
			finger.Pause ();
			isPlaying = false;
		}

		/// <summary>
		/// Ads a callback after each cycle of the looped tutorial.   	
		/// </summary>
		public FingerTutorial OnStepComplete (TweenCallback callback)
		{
			return this;
		}

		/// <summary>
		/// Plays the next finger gesture in the tutorial sequence.   	
		/// </summary>
		public void NextStep ()
		{
			hideFingerTween.KillAndClear ();
			activePrimitive.Complete ();
		}

		/// <summary>
		/// Ads a callback after the whole tutorial is complete.   	
		/// </summary>
		public FingerTutorial OnComplete (TweenCallback callback)
		{
			OnCompleteCallbacks.Add (callback);
			return this;
		}

		/// <summary>
		/// Ads a callback while the tutorial is playing (IsPlaying = true).   	
		/// </summary>
		public FingerTutorial OnUpdate (TweenCallback callback)
		{
			foreach (TutorialPrimitiveBase primitive in primitiveList)
			{
				(primitive as TutorialPrimitiveBase).OnUpdate <TutorialPrimitiveBase> (callback);
			}
			onUpdateCallback = callback;
			return this;
		}

		/// <summary>
		/// Sets the anount of cycles the tutorial will repeat.   	
		/// </summary>
		public FingerTutorial SetLoops (int loops)
		{
			this.loops = loops;
			return this;
		}

	}
}