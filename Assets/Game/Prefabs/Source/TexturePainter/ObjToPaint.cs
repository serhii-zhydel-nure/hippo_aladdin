﻿using UnityEngine;
using System.Collections;

public class ObjToPaint : MonoBehaviour
{
    public bool isDefined;
    public MeshCollider currentMeshCollider;
    public Renderer currentSkin;
    public Color defaultColor;

    public Material mat;
    private void Update()
    {
      
    }
    private void Awake()
    {
        if (GetComponent<SkinnedMeshRenderer>())
        {
            currentSkin = GetComponent<SkinnedMeshRenderer>();
        }
        else if (true)
        {
            currentSkin = GetComponent<MeshRenderer>();
        }
        if (transform.parent)
            foreach (var item in transform.parent.GetComponentsInChildren<Transform>(true))
            {
                if (item.name == name + "_Coll")
                {
                    currentMeshCollider = item.GetComponent<MeshCollider>();
                }
                else if (item.name == name + " (1)")
                {
                    currentMeshCollider = item.GetComponent<MeshCollider>();
                }
            }
        if (!currentMeshCollider)
        {
            gameObject.AddComponent<MeshCollider>();
            currentMeshCollider = GetComponent<MeshCollider>();
        }
        //if (currentSkin)
        //    if (currentSkin.GetComponent<Renderer>().material)

        //        defaultColor = currentSkin.GetComponent<Renderer>().material.color;
        //if (!PlayerPrefs.HasKey(transform.parent.name + name + "_defaultColor_r"))
        //{
        //    SaveDefaultColor();
        //}
    }

    public void SetColorWhite()
    {
        currentSkin.GetComponent<Renderer>().material.color = Color.white;

    }
    public void ReturnDefaultColor()
    {
        currentSkin.GetComponent<Renderer>().material.color = LoadDefaultColor();
    }
    public void SetCurrentSavedColor()
    {
        if (PlayerPrefs.HasKey(transform.parent.name + name + "_savedColor_r"))
        {
            currentSkin.GetComponent<Renderer>().material.color = LoadCurrentSavedColor();

        }

    }

    public void SaveCurrentColor()
    {
        float r, g, b, a = 0;
        r = currentSkin.GetComponent<Renderer>().material.color.r;
        g = currentSkin.GetComponent<Renderer>().material.color.g;
        b = currentSkin.GetComponent<Renderer>().material.color.b;
        a = currentSkin.GetComponent<Renderer>().material.color.a;

        PlayerPrefs.SetFloat(transform.parent.name + name + "_savedColor_r", r);
        PlayerPrefs.SetFloat(transform.parent.name + name + "_savedColor_g", g);
        PlayerPrefs.SetFloat(transform.parent.name + name + "_savedColor_b", b);
        PlayerPrefs.SetFloat(transform.parent.name + name + "_savedColor_a", a);

    }

    Color LoadCurrentSavedColor()
    {
        return new Color
   (
       PlayerPrefs.GetFloat(transform.parent.name + name + "_savedColor_r"),
       PlayerPrefs.GetFloat(transform.parent.name + name + "_savedColor_g"),
       PlayerPrefs.GetFloat(transform.parent.name + name + "_savedColor_b"),
       PlayerPrefs.GetFloat(transform.parent.name + name + "_savedColor_a")

    );
    }
    void SaveDefaultColor()
    {
        float r, g, b, a = 0;
        r = currentSkin.GetComponent<Renderer>().material.color.r;
        g = currentSkin.GetComponent<Renderer>().material.color.g;
        b = currentSkin.GetComponent<Renderer>().material.color.b;
        a = currentSkin.GetComponent<Renderer>().material.color.a;

        PlayerPrefs.SetFloat(transform.parent.name + name + "_defaultColor_r", r);
        PlayerPrefs.SetFloat(transform.parent.name + name + "_defaultColor_g", g);
        PlayerPrefs.SetFloat(transform.parent.name + name + "_defaultColor_b", b);
        PlayerPrefs.SetFloat(transform.parent.name + name + "_defaultColor_a", a);

    }
    Color LoadDefaultColor()
    {
        return new Color
        (
            PlayerPrefs.GetFloat(transform.parent.name + name + "_defaultColor_r"),
            PlayerPrefs.GetFloat(transform.parent.name + name + "_defaultColor_g"),
            PlayerPrefs.GetFloat(transform.parent.name + name + "_defaultColor_b"),
            PlayerPrefs.GetFloat(transform.parent.name + name + "_defaultColor_a")

         );
    }

    public void LoadSavedTexture()
    {
        Debug.Log("LoadSavedTexture");
        // Create a texture. Texture size does not matter, since
        // LoadImage will replace with with incoming image size.
        string fullPath = Application.persistentDataPath + "/";
        string fileName = currentMeshCollider.transform.parent.name + currentMeshCollider.name + ".png";

        Texture2D tex = new Texture2D(2, 2);
       
        try
        {
            tex.LoadImage(System.IO.File.ReadAllBytes(fullPath + fileName));
            //GetComponent<Renderer>().material.mainTexture = tex;

            mat.mainTexture = tex;
            currentSkin.material = mat;
        }
        catch (System.Exception)
        {

            Debug.Log("Текстура не существует\n " + fullPath + fileName);
        }
    
    }
 
}
