﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterToPaint : MonoBehaviour
{

    public List<ObjToPaint> list_ObjToPaint = new List<ObjToPaint>();
    public float minDistance = 3;
    public bool isThisActivePlayer;

    private void Awake()
    {
        list_ObjToPaint.AddRange(GetComponentsInChildren<ObjToPaint>(true));
    }

    public void SetWhiteColor()
    {
        foreach (var item in list_ObjToPaint)
        {
            item.SetColorWhite();
        }
    }
    public void ReturnDefaultColor()
    {
        foreach (var item in list_ObjToPaint)
        {
            item.ReturnDefaultColor();
        }
    }
    public void SetCurrentSavedColor()
    {
        foreach (var item in list_ObjToPaint)
        {
            item.SetCurrentSavedColor();
        }
    }
    public void SaveCurrentColor()
    {
        foreach (var item in list_ObjToPaint)
        {
            item.SaveCurrentColor();
        }
    }
    public void LoadSavedTexture()
    {
        foreach (var item in list_ObjToPaint)
        {
            Debug.Log("LoadSavedTexture");
            item.LoadSavedTexture();
            item.SetCurrentSavedColor();
        }
    }
   

    public void InitMeToPaint()
    {
       if(GetComponent<Animator>()) GetComponent<Animator>().SetBool("paint", true);
        //GetComponent<Rigidbody>().isKinematic = true;
        //GetComponent<PlayerController>().currentControlPype = PlayerController.ControlPype.none;
        foreach (var item in list_ObjToPaint)
        {
            item.currentMeshCollider.gameObject.SetActive(true);
        }
        isThisActivePlayer = true;
    }
}
