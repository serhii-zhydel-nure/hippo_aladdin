﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PainterButtonWhiteColor : MonoBehaviour {
    public static PainterButtonWhiteColor instance;

    Image imageLogo;
    public Sprite imgColor, imgWhite;

    private void Awake()
    {
        instance = this;
        foreach (var item in GetComponentsInChildren<Transform>())
        {
            if (item.name == "Image")
            {
                imageLogo = item.GetComponent<Image>();
            }
        }
    }
 
    public void SetIsWhiteNow(bool _isWhiteNow)
    {
        if (_isWhiteNow)
        {
            imageLogo.sprite = imgWhite;
        }
        else imageLogo.sprite = imgColor;
    }

}
