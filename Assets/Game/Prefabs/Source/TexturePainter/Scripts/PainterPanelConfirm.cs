﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PainterPanelConfirm : MonoBehaviour {

    public static PainterPanelConfirm instance;

    Button ButtonNextScene, ButtonAgain;
    GameObject PanelConfirmObj;

    private void Awake()
    {
        instance = this;
        foreach (var item in GetComponentsInChildren<Transform>(true))
        {
            if (item.name == "PanelConfirmObj")
            {
                PanelConfirmObj = item.gameObject;
            }
            else if (item.name == "ButtonNextScene")
            {
                ButtonNextScene = item.GetComponent<Button>();
                ButtonNextScene.onClick.AddListener(() => OnButtonNextScene());
            }
            else if (item.name == "ButtonAgain")
            {
                ButtonAgain = item.GetComponent<Button>();
                ButtonAgain.onClick.AddListener(() => OnButtonAgain());

            }
        }
    }

    private void OnButtonNextScene()
    {
        //StartCoroutine(StaticParams.AutoFade("GameSpinner", true));
    }

    private void OnButtonAgain()
    {
        Debug.Log("OnButtonAgain");
        ShowHide(false);
    }

    public void ShowHide(bool show)
    {
             PanelConfirmObj.SetActive(show);
     }
}
