﻿using UnityEngine;
using System.Collections;

public class ModelViewControls : MonoBehaviour
{
    public static ModelViewControls instance;
    public bool isDragNow;
    private int yMinLimit = 0, yMaxLimit = 80;
    private Quaternion currentRotation, desiredRotation, rotation;
    private float yDeg = 15, xDeg = 0.0f;
    public float currentDistance, desiredDistance = 3.0f, maxDistance = 6.0f, minDistance = 9.0f;
    private Vector3 position;
    public GameObject camPivo, camObject;
    Transform trTarget;
    float sensitivity = 1.25f;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        currentDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
    }

    // Update is called once per frame
    void Update()
    {
        CameraControlUpdate();
    }
    float clickTimer;

    void CameraControlUpdate()
    {

#if UNITY_EDITOR
        yDeg += Input.GetAxis("Vertical") * sensitivity;
        xDeg -= Input.GetAxis("Horizontal") * sensitivity;
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        foreach (var item in PainterController.instance.list_TexturePainter)
        {
            if (item.isPaintingNow)
            {
                isDragNow = false;
                clickTimer = 0;
                return;
            }
        }
        if (Input.touchCount != 0)
        {
            clickTimer += Time.deltaTime;
            if (clickTimer > .5f)
            {
                isDragNow = true;
                   yDeg += Input.touches[0].deltaPosition.y * sensitivity;
                xDeg += Input.touches[0].deltaPosition.x * sensitivity;

            }

        }
        else
        {
            isDragNow = false;
               clickTimer = 0;
        }
#endif
        yDeg = ClampAngle(yDeg, yMinLimit, yMaxLimit);
        desiredRotation = Quaternion.Euler(yDeg, xDeg, 0);
        rotation = Quaternion.Lerp(camPivo.transform.rotation, desiredRotation, 0.05f);
        camPivo.transform.rotation = desiredRotation;
        desiredDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
        currentDistance = Mathf.Lerp(currentDistance, desiredDistance, 0.05f);
        position = camPivo.transform.position - (rotation * Vector3.forward * currentDistance);
        Vector3 lerpedPos = Vector3.Lerp(camObject.transform.position, position, 0.05f);
        camObject.transform.position = lerpedPos;

       if(trTarget) camPivo.transform.position = Vector3.Lerp(camPivo.transform.position,
            trTarget.position, Time.deltaTime * 2);
    }
    private static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }

    public void SetTarget(Transform _targetObject, float _minDistance)
    {
        trTarget = _targetObject;
        minDistance = _minDistance;
        maxDistance = _minDistance;
    }
}
