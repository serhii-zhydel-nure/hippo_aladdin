﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Shevchenko;

public class PainterController : MonoBehaviour
{
    public static PainterController instance;
    public static string level;

    [Header("Пока перетаскиваем ручками")]
    public GameObject goObjectToPaint;
    [HideInInspector]
    public CharacterToPaint characterToPaint;
    public List<CharacterToPaint> list_AllCharacterToPaint;
    public List<CharacterToPaint> list_ActiveCharactersToPaint = new List<CharacterToPaint>();

    public bool isWhiteNow;

    public List<TexturePainter> list_TexturePainter = new List<TexturePainter>();
    public GameObject firstTexturePainter;

    TexturePainter CreateTexturePainter()
    {
        if (!firstTexturePainter)
        {
            firstTexturePainter = GameObject.Find("Painter");
            list_TexturePainter.Add(firstTexturePainter.GetComponentInChildren<TexturePainter>());
            return list_TexturePainter[0];
        }

        GameObject go = (GameObject)Instantiate(firstTexturePainter);
        RenderTexture src = firstTexturePainter.GetComponentInChildren<TexturePainter>().canvasTexture;
        RenderTexture rt = new RenderTexture(src.width, src.height, src.depth);
        TexturePainter tp = go.GetComponentInChildren<TexturePainter>();
        tp.canvasTexture = rt;
        tp.canvasCam.targetTexture = tp.canvasTexture;
        go.transform.position = new Vector3(list_TexturePainter[list_TexturePainter.Count - 1].transform.root.transform.position.x + 2, 0, 0);
        list_TexturePainter.Add(tp);
        return tp;
    }
    private void Awake()
    {
        //CreateTexturePainter();

        instance = this;
        //list_TexturePainter.AddRange(FindObjectsOfType<TexturePainter>());
        if (GameObject.Find("ButtonSetWhiteColor"))
        {
            GameObject.Find("ButtonSetWhiteColor").GetComponent<Button>()
                .onClick.AddListener(() => SetWhiteColor());
        }
        //if (GameObject.Find("ButtonSave"))
        //{
        //    GameObject.Find("ButtonSave").GetComponent<Button>()
        //        .onClick.AddListener(() => SaveAllToFile());
        //}
        if (GameObject.Find("ButtonLoadTexture"))
        {
            GameObject.Find("ButtonLoadTexture").GetComponent<Button>()
                .onClick.AddListener(() => ButtonLoadTexture());
        }
        if (GameObject.Find("ButtonRestartScene"))
        {
            GameObject.Find("ButtonRestartScene").GetComponent<Button>()
                .onClick.AddListener(() => { Application.LoadLevel(Application.loadedLevel); });
        }
        if (GameObject.Find("ButtonClearTexture"))
        {
            GameObject.Find("ButtonClearTexture").GetComponent<Button>()
                .onClick.AddListener(() => { ButtonClearTexture(); });
        }
        if (GameObject.Find("ButtonNextcene"))
        {
            GameObject.Find("ButtonNextcene").GetComponent<Button>()
                .onClick.AddListener(() => { ButtonNextcene(); });
        }
        if (GameObject.Find("ButtonNextObj"))
        {
            GameObject.Find("ButtonNextObj").GetComponent<Button>()
                .onClick.AddListener(() => { ButtonNextObj(); });
        }
        if (GameObject.Find("ButtonPrevObj"))
        {
            GameObject.Find("ButtonPrevObj").GetComponent<Button>()
                .onClick.AddListener(() => { ButtonPrevObj(); });
        }

        if (GameObject.Find("Scroller"))
        {
            Scroller = GameObject.Find("Scroller").GetComponent<ScrollRect>();
        }
    }
    void Start()
    {
        list_AllCharacterToPaint = new List<CharacterToPaint>(FindObjectsOfType<CharacterToPaint>());

        //InitPainter();
        //if (level == null)
        //{
        //    level = "Level (1)";
        //}
        //InitOtherItems(GameObject.Find(level).transform);
        InitAllItemsOnScene();
        foreach (var item in list_TexturePainter)
        {
            if (!item.isPainterDefinedBusy)
            {
                Destroy(item.gameObject);
            }
        }
    }

    ScrollRect Scroller;
    private void OnEnable()
    {
        //DragGame.OnDropStartedOnePar += DragGame_OnDropStartedOnePar;
        //DragGame.OnDropCorrect += DragGame_OnDropCorrect;
        //DragGame.OnDropWrong += DragGame_OnDropWrong;
        //DragGame.OnDropWrongOnePar += DragGame_OnDropWrongOnePar;

    }



    private void OnDisable()
    {
        //DragGame.OnDragStartedOnePar -= DragGame_OnDropStartedOnePar;
        //DragGame.OnDropCorrect -= DragGame_OnDropCorrect;
        //DragGame.OnDropWrong -= DragGame_OnDropWrong;
        //DragGame.OnDropWrongOnePar -= DragGame_OnDropWrongOnePar;
        
    }

    private void DragGame_OnDropWrongOnePar(DragGame_Child element)
    {
        Scroller.enabled = true;

        Debug.Log("DragGame_OnDropWrong");
    }

    private void DragGame_OnDropWrong(DragGame_Child element, DragGame_Parent parent)
    {
        Debug.Log("DragGame_OnDropWrong");
        Scroller.enabled = true;
    }
    private void DragGame_OnDropCorrect(DragGame_Child element, DragGame_Parent parent)
    {
        Debug.Log("DragGame_OnDropCorrect");
        Scroller.enabled = true;
    }
    private void DragGame_OnDropStartedOnePar(DragGame_Child element)
    {
        Debug.Log("DragGame_OnDropStartedOnePar");
        Scroller.StopMovement();
        Scroller.enabled = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {

            Debug.Log("KeyCode.R-- Load Saved");
            foreach (var item in list_ActiveCharactersToPaint)
            {
                item.LoadSavedTexture();
            }
        }
    }
    int counter;
    private void ButtonPrevObj()
    {
        counter++;
        if (counter >= list_ActiveCharactersToPaint.Count)
        {
            counter = 0;
        }
        ModelViewControls.instance.SetTarget(list_ActiveCharactersToPaint[counter].transform,
            list_ActiveCharactersToPaint[counter].minDistance);
    }

    private void ButtonNextObj()
    {
        counter--;
        if (counter < 0)
        {
            counter = list_ActiveCharactersToPaint.Count - 1;
        }
        ModelViewControls.instance.SetTarget(list_ActiveCharactersToPaint[counter].transform,
            list_ActiveCharactersToPaint[counter].minDistance);

    }

    private void ButtonNextcene()
    {
        //StartCoroutine(StaticParams.AutoFade("GameSpinner", false));
    }

    private void ButtonClearTexture()
    {
        foreach (var item in list_TexturePainter)
        {
            item.ClearTexture();
        }
    }

  public  void SaveAllToFile()
    {

        StartCoroutine(IeSaveAllToFile());

    }
    IEnumerator IeSaveAllToFile()
    {
        foreach (var item in GameObject.Find("DragsParent").GetComponentsInChildren<Transform>(true))
        {
            item.gameObject.layer = 0;
        }
        yield return new WaitForEndOfFrame();
        foreach (var item in list_TexturePainter)
        {
            if (item.currentMeshCollider)
            {
                item.SaveTextureToFile();

            }
        }
        if (characterToPaint) characterToPaint.SaveCurrentColor();
        //PainterPanelConfirm.instance.ShowHide(true);
    }
    void SetWhiteColor()
    {
        //foreach (var item in list_TexturePainter)
        //{
        //    item.SetWhiteColor();
        //}
        if (!isWhiteNow)
        {
            characterToPaint.SetWhiteColor();

        }
        else
        {
            characterToPaint.ReturnDefaultColor();
        }
        isWhiteNow = !isWhiteNow;
        PainterButtonWhiteColor.instance.SetIsWhiteNow(isWhiteNow);

    }

    void InitPainter()
    {
        string srcPlayerName = "player0";
        if (PlayerPrefs.HasKey("SelectedPlayer"))
        {
            srcPlayerName = PlayerPrefs.GetString("SelectedPlayer");
        }
        GameObject go = (GameObject)Instantiate(Resources.Load<GameObject>("Players/" + srcPlayerName));
        go.transform.position = Vector3.zero;
        goObjectToPaint = go;

        characterToPaint = goObjectToPaint.GetComponent<CharacterToPaint>();
        characterToPaint.InitMeToPaint();
        ObjToPaint[] mas_ObjToPaint = goObjectToPaint.GetComponentsInChildren<ObjToPaint>();

        //Debug.Log("ВНИМАНИЕ!! Выбран объект= "+ goObjectToPaint.name);
        for (int i = 0; i < mas_ObjToPaint.Length; i++)
        {
            TexturePainter tp = CreateTexturePainter();
            tp.currentMeshCollider = mas_ObjToPaint[i].currentMeshCollider;
            tp.currentSkin = (SkinnedMeshRenderer)mas_ObjToPaint[i].currentSkin;
            mas_ObjToPaint[i].currentSkin.material.mainTexture = tp.canvasTexture;
            tp.isPainterDefinedBusy = true;
            mas_ObjToPaint[i].isDefined = true;
        }
        list_ActiveCharactersToPaint.Add(characterToPaint);
    }

    void InitOtherItems(Transform _activeHolder)
    {
        foreach (var item in _activeHolder.GetComponentsInChildren<ObjToPaint>())
        {
            if (!item.isDefined)
            {
                TexturePainter tp = CreateTexturePainter();
                if (!tp)
                {
                    Debug.Log("Нет свободной TexturePainter");
                    return;
                }
                tp.InitRecieveParams(item);
            }
        }

        foreach (var item in _activeHolder.GetComponentsInChildren<CharacterToPaint>(true))
        {
            item.InitMeToPaint();
            list_ActiveCharactersToPaint.Add(item);
        }
        Debug.Log("111");
        foreach (var item in list_AllCharacterToPaint)
        {
            if (item.isThisActivePlayer)
            {
                item.LoadSavedTexture();
            }
        }
    }
    void InitAllItemsOnScene()
    {
        foreach (var item in FindObjectsOfType<ObjToPaint>())
        {
            if (!item.isDefined)
            {
                TexturePainter tp = CreateTexturePainter();
                if (!tp)
                {
                    Debug.Log("Нет свободной TexturePainter");
                    return;
                }
                tp.InitRecieveParams(item);
            }
        }

        foreach (var item in FindObjectsOfType<CharacterToPaint>())
        {
            item.InitMeToPaint();
            list_ActiveCharactersToPaint.Add(item);
        }
        Debug.Log("111");
        //foreach (var item in list_AllCharacterToPaint)
        //{
        //    if (item.isThisActivePlayer)
        //    {
        //        item.LoadSavedTexture();
        //    }
        //}
    }

    void ButtonLoadTexture()
    {
        characterToPaint.LoadSavedTexture();
    }
}
