﻿using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Cleaning
{
  public class EraserSound:MonoBehaviour
  {
    private AudioSource _source;

    void Awake()
    {
      _source = gameObject.AddComponent<AudioSource>();
    }

    void OnEnable()
    {
			StartCoroutine(_source.PlayClipLoop(/*ResourcesFolder.GetTracks("трут мочалкой")*/ Resources.Load<AudioClip>("Sounds/NonLanguage/трут мочалкой")));
    }

    void OnDisable()
    {
      StopAllCoroutines();
      _source.Stop();
    }
  }
}