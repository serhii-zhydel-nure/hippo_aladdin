﻿using UnityEngine;

namespace Cleaning
{
  public class BaseController : MonoBehaviour
  {
    public GameObject self;

    virtual public void Init() { }

    virtual public void Process() { }

    virtual public void Release() { }
  }
}

