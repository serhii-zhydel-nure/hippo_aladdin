﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Cleaning
{
  public class Cleaning : BaseController
  {
    private bool _finish;
    //public AudioSource walking;

    public float ErasePersents = 95;
    private bool _animating;
    static bool _load;
    static int _partForLoad;

    float _time;
    Vector3 _firstPos;
    bool _bContinue = true, _bEnd = false;

    public void Work()
    {
      InputManager.Instance.Init();
      Init();
      if (_load)
      {
        f_Load();
      }
    }

    public void f_Load()
    {
      PlayerPrefs.SetInt("CleaningLevelsOpen", PlayerPrefs.GetInt("CleaningLevelsOpen") + _partForLoad);
      _load = false;
    }

    override public void Init()
    {
      Release();
      MaskCamera.Instance.Init();
      MaskCamera.Instance.SetPercentToShow(ErasePersents * 0.01f);
      MaskCamera.Instance.ErasingDone += () => Debug.Log("ErasingDone");
      _animating = false;
      _finish = false;
      _bEnd = false;
    }

    public static void f_SetLoadParams(int b)
    {
      _partForLoad = b;
      _load = true;
    }


    override public void Release()
    {
      MaskCamera.Instance.Release();


    }

    void Update()
    {
      InputManager.Instance.Process();
      Process();
    }

    private void Animating()
    {
      _animating = true;
      _bEnd = true;

    }


    override public void Process()
    {
      if (Input.GetMouseButtonDown(0))
      {
        _time = Time.time;
        _firstPos = Input.mousePosition;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        if (!Physics.Raycast(ray, out raycastHit, 500)) return;
      }

      if (Input.GetMouseButton(0) && !_bEnd)
      {
        if (Time.time - _time > 0.2f)
        {
          CheckPos(_firstPos, Input.mousePosition, 0.5f);
          _time = Time.time;
          _firstPos = Input.mousePosition;
        }
      }

      if (Input.GetMouseButtonUp(0))
      {
        StopAllCoroutines();
      }

      if (MaskCamera.Instance.IsImageShown && !_animating)
      {
        Animating();
      }

      if (!_bEnd)
      {
        MaskCamera.Instance.Process();
      }
    }


    void CheckPos(Vector3 newpos, Vector3 lastpos, float delta)
    {
      if (Vector3.Distance(lastpos, newpos) < delta)
      {
        StopAllCoroutines();
        _bContinue = false;
      }
      else
      {
        if (!_bContinue)
        {
          _bContinue = true;
        }

      }

    }
  }
}
