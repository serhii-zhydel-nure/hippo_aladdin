﻿using System.Collections;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Cleaning
{
  public static class CleanLauncher
  {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="cleaningSprite"></param>
    /// <param name="persents">         80                                </param>
    /// <param name="eraserPrefab">     PrefabManager.GetPrefab("wisp")   </param>
    /// <param name="eraserSize">       new Vector3(0.6f, 0.6f, 0.6f)     </param>
    /// <returns></returns>
		public static IEnumerator CleanSprite(SpriteRenderer cleaningSprite, float persents, GameObject eraserPrefab, Vector3 eraserSize, string soundName = "")
    {
      GameObject cleanerObj = GameObject.Instantiate(ResourcesFolder.GetPrefab("cleaner"));

      bool cleaningDone = false;
      cleanerObj.AddComponentWithInit<CleanerController>((script) =>
      {
        script.SpriteToClean = cleaningSprite;
        script.PersentsToDone = persents;
					script.soundName = soundName;
        script.EraserPrefab = eraserPrefab;
        script.EraserSize = eraserSize;
        script.CleaningDone += () => cleaningDone = true;
      });

      yield return new WaitUntil(() => cleaningDone);
      yield return cleaningSprite.gameObject.TransparentOverSeconds(0, 0.8f).Append(()=> cleaningSprite.gameObject.SetActive(false));

      GameObject.Destroy(cleanerObj);
    }
  }
}