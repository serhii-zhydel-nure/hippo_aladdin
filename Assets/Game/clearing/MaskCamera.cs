﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using PSV;

namespace Cleaning
{
	public class MaskCamera : InputBase
	{
		public event Action ErasingDone;
		public event Action<float> ErasedPersentsChanged;

		public static MaskCamera Instance;
		public GameObject dust;
		public Material eraser_material;
		private bool clear_gl;
		private Rect screen_rect;
		private Vector2? newHolePosition;
		public static float scale = 5f;
		public string soundName = "";

		private int _eraserWidth = 60;
		private int _eraserHeight = 60;

		private float percent_to_show = 0.8f;
		private bool image_shown;
		private bool image_showing;

		private float showing_time = 1f;
		private float cur_time;


		private int count = 0;

		public bool IsImageShown
		{
			get { return image_shown; }
		}

		private void Awake()
		{

			Instance = this;
		}

		public void Init()
		{
			Bounds bounds = dust.GetComponent<SpriteRenderer>().bounds;

			screen_rect.x = bounds.min.x;
			screen_rect.y = dust.GetComponent<Renderer>().bounds.min.y;
			screen_rect.width = dust.GetComponent<Renderer>().bounds.size.x;
			screen_rect.height = dust.GetComponent<Renderer>().bounds.size.y;

			dust.GetComponent<Renderer>().material.SetFloat("_Alpha", 1f);

			image_showing = false;
			image_shown = false;
			clear_gl = true;
			newHolePosition = null;
			dust.SetActive(true);
			count = 0;
		}

		public void Release()
		{
			image_showing = false;
			image_shown = false;
		}

		public void SetPercentToShow(float value)
		{
			percent_to_show = value;
		}

		public void ShowImage()
		{
			image_showing = true;
			image_shown = false;

			cur_time = 0;
		}

		private void Update()
		{

		}

		public void Process()
		{
			newHolePosition = null;
			if (!image_showing && !image_shown)
			{
				if (Input.GetMouseButton(0))
				{

					Vector2 v = InputManager.Instance.GetTouchPosition();
					Rect worldRect = screen_rect;
					if (worldRect.Contains(v))
					{
						if (soundName != "" && !AudioController.IsSoundPlaying(soundName))
						{
							AudioController.PlaySound(soundName);
						}
            					
						newHolePosition = new Vector2(_eraserWidth * (v.x - worldRect.xMin) / worldRect.width,
							_eraserHeight * (v.y - worldRect.yMin) / worldRect.height);

					}
					else
					{
						if (soundName != "" && AudioController.IsSoundPlaying(soundName))
						{
							AudioController.StopSound(soundName);
						}
					}
				}
				else
				{
					if (soundName != "" && AudioController.IsSoundPlaying(soundName))
					{
						AudioController.StopSound(soundName);
					}
				}
			}
			else
			{
				if (image_showing)
				{
					cur_time += Time.deltaTime;
					//dust.GetComponent<Renderer>().material.SetFloat("_Alpha", Mathf.Lerp(0.5f, 0.0f, cur_time / showing_time));
					if (cur_time >= showing_time)
					{
						image_showing = false;
						image_shown = true;
					}
				}
			}
		}

		private float CleaningPercent(Texture2D texture)
		{
			float p = 0;
			//print(texture);
			for (int i = 0; i < _eraserWidth; i++)
			{
				for (int j = 0; j < _eraserHeight; j++)
				{
					if (texture.GetPixel(i, j).a != 0)
					{
						p++;

					}

				}
			}
			//print(p / (float)(_eraserWidth * _eraserHeight));
			return p / (float)(_eraserWidth * _eraserHeight);
		}

		public void OnPostRender()
		{
			if (!image_shown && !image_showing)
			{
				if (clear_gl)
				{
					clear_gl = false;
					GL.Clear(false, true, new Color(0f, 0f, 0f, 0f));
				}

				if (newHolePosition != null)
				{
					var tex = CutHole(newHolePosition.Value);
					if (CleaningPercent(tex) >= percent_to_show)
					{
						ShowImage();
						if (ErasingDone != null)
							ErasingDone.Invoke();
					}
					else
					{
						if (ErasedPersentsChanged != null)
							ErasedPersentsChanged.Invoke(CleaningPercent(tex));
					}

					Destroy(tex);
				}
			}
		}

		private Texture2D CutHole(Vector2 imageLocalPosition)
		{

			Rect textureRect = new Rect(0f, 0f, 1f, 1f);
			Rect positionRect = new Rect(
				                       (imageLocalPosition.x - 0.5f * (eraser_material.mainTexture.width / scale)) / (_eraserWidth),
				                       (imageLocalPosition.y - 0.5f * (eraser_material.mainTexture.height / scale)) / (_eraserHeight),
				                       eraser_material.mainTexture.width / (_eraserWidth * scale),
				                       eraser_material.mainTexture.height / (_eraserHeight * scale)
			                       );

			GL.PushMatrix();
			GL.LoadOrtho();

			for (int i = 0; i < eraser_material.passCount; i++)
			{
				eraser_material.SetPass(i);
				GL.Begin(GL.QUADS);
				GL.Color(Color.white);
				GL.TexCoord2(textureRect.xMin, textureRect.yMax);
				GL.Vertex3(positionRect.xMin, positionRect.yMax, 0.0f);
				GL.TexCoord2(textureRect.xMax, textureRect.yMax);
				GL.Vertex3(positionRect.xMax, positionRect.yMax, 0.0f);
				GL.TexCoord2(textureRect.xMax, textureRect.yMin);
				GL.Vertex3(positionRect.xMax, positionRect.yMin, 0.0f);
				GL.TexCoord2(textureRect.xMin, textureRect.yMin);
				GL.Vertex3(positionRect.xMin, positionRect.yMin, 0.0f);

				GL.End();
			}
			GL.PopMatrix();
			//return (Texture2D)dust.GetComponent<SpriteRenderer>().material.mainTexture;
			Texture2D newTexture = new Texture2D(_eraserWidth, _eraserHeight);

			newTexture.ReadPixels(new Rect(0, 0, _eraserWidth, _eraserHeight), 0, 0);

			bool applyMipsmaps = false;
			newTexture.Apply(applyMipsmaps);
			bool highQuality = false;
			newTexture.Compress(highQuality);

			RenderTexture.active = null;

			return newTexture;
		}
	}
}