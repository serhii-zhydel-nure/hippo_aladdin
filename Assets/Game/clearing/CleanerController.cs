﻿using System;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.InputManagement.Global;
using UnityEngine;

namespace Cleaning
{
  public class CleanerController : MonoBehaviour
  {
    public SpriteRenderer SpriteToClean;
    public GameObject EraserPrefab;
		public string soundName;

    public float PersentsToDone = 60;
    public event Action CleaningDone;
    public static event Action<float> CleanedPersents;

    public Vector3 EraserSize = Vector3.one;

    private GameObject _eraserForm;

    private void Awake()
    {
      if (EraserPrefab != null)
      {
        _eraserForm = gameObject.CreateChild(EraserPrefab);
        _eraserForm.transform.localScale = EraserSize;
        _eraserForm.AddComponent<UserFingerFolower>();
        _eraserForm.SetActive(false);
      }
    }

    // Use this for initialization
    void Start()
    {
      SetSpriteEraseMaterial(SpriteToClean);
      MaskCamera maskCamera = GetComponentInChildren<MaskCamera>();
      maskCamera.dust = SpriteToClean.gameObject;
      maskCamera.ErasingDone += CleaningDone;
      maskCamera.ErasedPersentsChanged += CleanedPersents;
			maskCamera.soundName = soundName;
      Cleaning cleaner = GetComponent<Cleaning>();
      cleaner.ErasePersents = PersentsToDone;
      cleaner.Work();
    }

    private void SetSpriteEraseMaterial(SpriteRenderer sprite)
    {
      sprite.SetMaterialFromResourses("Dust");
    }
  }
}
