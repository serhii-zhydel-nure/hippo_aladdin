﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Cleaning
{
  public class InputManager : MonoBehaviour
  {
    public static InputManager Instance;

    public LayerMask touchInputMask;
    private List<InputBase> focused;
    private Vector2 touchPosition;
    private Vector2 prevTouchPosition;
    private Vector2 deltaPosition;
    private bool lockTouchListener = false;

    private void Awake()
    {
      Instance = this;
    }

    public void Init()
    {
      focused = new List<InputBase>();
      lockTouchListener = false;

      touchPosition = new Vector2();
      prevTouchPosition = new Vector2();
      deltaPosition = new Vector2();
    }

    public void Release()
    {
      focused.Clear();
      lockTouchListener = false;

    }

    public void LockListener(bool _lock)
    {
      lockTouchListener = _lock;
    }

    public bool IsListenerLocked()
    {
      return lockTouchListener;
    }

    public Vector2 GetTouchPosition()
    {
      return touchPosition;
    }

    public Vector2 GetDeltaPosition()
    {
      return deltaPosition;
    }

    public bool HasFocusedObjects()
    {
      return focused.Count > 0;
    }

    public void SetFocus(InputBase _focus)
    {
      focused.Add(_focus);
    }

    public void RemoveFocus(InputBase _focus)
    {
      focused.Remove(_focus);
    }

    public void ClearFocus()
    {
      focused.Clear();
    }

    private void CheckTouch(TouchPhase phase, Vector3 position)
    {
      Vector3 ray = Camera.main.ScreenToWorldPoint(position);

      RaycastHit2D hit = Physics2D.Raycast(ray, Vector2.zero, 20, touchInputMask);
      if (hit.collider != null)
      {
        GameObject recipient = hit.transform.gameObject;
        InputBase input = recipient.GetComponent<InputBase>();
        if (input)
        {
          Touch(input, phase, hit.point);
        }
      }
    }

    private bool Touch(InputBase input, TouchPhase phase, Vector2 point, bool focus = false)
    {
      bool breaking = false;

      if (input != null && input.IsFocused() == focus)
      {
        if (phase == TouchPhase.Began)
        {
          breaking = input.OnTouchDown(point);
        }
        if (phase == TouchPhase.Ended)
        {
          breaking = input.OnTouchUp(point);
        }
        if (phase == TouchPhase.Stationary || phase == TouchPhase.Moved)
        {
          breaking = input.OnTouchMove(point);
        }
        if (phase == TouchPhase.Canceled)
        {
          breaking = input.OnTouchExit(point);
        }
      }
      return breaking;
    }

    private void FocusedItems()
    {
      TouchPhase phase = TouchPhase.Began;
      Vector3 position = new Vector3();
#if UNITY_EDITOR
      if (Input.GetMouseButtonUp(0) || Input.GetMouseButton(0))
      {
        if (Input.GetMouseButtonUp(0))
        {
          phase = TouchPhase.Ended;
        }
        else if (Input.GetMouseButton(0))
        {
          phase = TouchPhase.Moved;
        }
        position = Input.mousePosition;
      }
#endif
      //#elif
      if (Input.touchCount > 0)
      {
        Touch touch = Input.GetTouch(0);
        phase = touch.phase;
        position = touch.position;
      }
      //#endif

      //		for (int i = focused.Count - 1; i >= 0; i--) {
      //			if (focused[i].focus == false) {
      //				focused.RemoveAt (i);
      //			}
      //		}

      position = Camera.main.ScreenToWorldPoint(position);
      if (phase != TouchPhase.Began)
      {
        for (int i = focused.Count - 1; i >= 0; i--)
        {
          InputBase input = focused[i];
          Touch(input, phase, position, true);
        }
        //			foreach (InputBase input in focused) {
        ////				print (input + " " + phase + " " + position);
        //				Touch (input, phase, position);
        //			}
      }
    }

    public void Process()
    {
      prevTouchPosition = touchPosition;
      if (lockTouchListener) return;
      FocusedItems();
#if UNITY_EDITOR
      if (Input.GetMouseButtonDown(0))
      {
        CheckTouch(TouchPhase.Began, Input.mousePosition);
      }
      else if (Input.GetMouseButtonUp(0))
      {
        CheckTouch(TouchPhase.Ended, Input.mousePosition);
      }
      else if (Input.GetMouseButton(0))
      {
        CheckTouch(TouchPhase.Moved, Input.mousePosition);
      }
      touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
#endif
      //#elif
      if (Input.touchCount > 0)
      {
        Touch touch = Input.GetTouch(0);
        CheckTouch(touch.phase, touch.position);
        touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
        if (touch.phase == TouchPhase.Began)
        {
          prevTouchPosition = touchPosition;
        }
        //			foreach (Touch touch in Input.touches){
        //				CheckTouch (touch.phase, touch.position);
        //			}
      }
      //#endif
      deltaPosition = touchPosition - prevTouchPosition;
      //		FocusedItems ();
    }
  }
}