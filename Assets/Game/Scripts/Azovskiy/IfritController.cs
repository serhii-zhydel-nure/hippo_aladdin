﻿using UnityEngine;
using System.Collections;
using Shevchenko;
using PSV;

public class IfritController : MonoBehaviour
{
	public GameObject particle;
	PlayerController ifrit;
	public Transform[] ifritWalkPoints;

	bool running;
	// Use this for initialization
	void Start()
	{
		ifrit = GetComponentInChildren<PlayerController>();
		ifrit.animPlayer.SetInteger("ActionType", 13);
		ifrit.animPlayer.SetBool("bAction", true);

	}
    public IEnumerator Freeze()
    {
         yield return new WaitForSeconds(1);
        ifrit.animPlayer.Play("Ifrit_freezing");
        //Oledenenie_zamorozka
        AudioController.PlaySound("Oledenenie_zamorozka");
    }
    public IEnumerator Fire()
	{
        yield return new WaitForSeconds(1);
        //zvuk-ognya-zvuk-ognya
        AudioController.PlaySound("zvuk-ognya-zvuk-ognya");

        particle.SetActive(true);
		yield return new WaitForSeconds(1.4f);
		gameObject.SetActive(false);
	}
		
	public void StartRun()
	{
		StartCoroutine(Run());
	}
	public void StopRun()
	{
		running = false;
	}
	public void Jump()
	{
		ifrit.animPlayer.Play("Scare_1");
	}
	public IEnumerator Run()
	{
        yield return new WaitForSeconds(1);

		Talker.instance.TalkQueue("uv-hp-98");

        running = true;

        ifrit.walkSpeed = 4;
		ifrit.animPlayer.SetBool("bAction", false);
		while (running)
		{
			ifrit.Walk(ifritWalkPoints[0].position);
			yield return new WaitWhile(() => ifrit.IsWalkingNow());
			ifrit.Walk(ifritWalkPoints[1].position);
			yield return new WaitWhile(() => ifrit.IsWalkingNow());
		}
	}

}
