﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DestroyWrongStuff : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{
		StartCoroutine(DestroyDelayed());
	}

	IEnumerator DestroyDelayed()
	{
		yield return new WaitForSeconds(10);
		transform.DOScale(0, 0.4f).OnComplete(() => Destroy(gameObject, 0.2f));
	}
}
