﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV;
using Scripts_SergeyZhidel;
using Shevchenko;

public class MainMenuLamp : MonoBehaviour
{
    public Animator _tutorialFingerAnimator;
    private HelpByTimeIfUserDontDoRightAction _helpByTimeIfUserDontTakeRightStuff;
    public List<Entity> list_Entities = new List<Entity>();
    PlayerController pepa, george;

    void Start()
    {
        AudioController.Release();
        AudioController.PlaySound("Loop 1 song",  StreamGroup.MUSIC, 0.3f, true);

        foreach (var item in FindObjectsOfType<PlayerController>())
        {
            if (item.playerType == PlayerType.pEpa)
                pepa = item;
            else if (item.playerType == PlayerType.george)
                george = item;
        }
        foreach (var item in list_Entities)
        {
            Entity en = Instantiate(item);

            if (item.forGeorge)
            {
                en.transform.position = george.transform.position;
                george.GetComponent<EntityPlayer>().PlaceCorrectEntityImmediate(en);
            }
            else
            {
                en.transform.position = pepa.transform.position;
                pepa.GetComponent<EntityPlayer>().PlaceCorrectEntityImmediate(en);
            }
        }
        StartCoroutine(Process());
    }

    IEnumerator Process()
    {

        yield return new WaitForSeconds(1);
        if (Kover.instance)
        {
            Kover.instance.SetFly(true);

            Kover.instance.FlyUp();
        }
        foreach (var item in FindObjectsOfType<PlayerController>())
        {
            item.SetKoverSit();
            item.SetKoverSit();

        }
        SFX.PlayLoop("Дует ветер когда они летят на ковре");

        //Нас ждут сказки Востока!
        Talker.instance.TalkQueue("hp-1");
        yield return new WaitWhile(() => Talker.instance.isTalkingNow);
        _tutorialFingerAnimator.SetBool("Scrub", true);
        isSmallGame = true;
        yield return new WaitForSeconds(1);
        Talker.instance.TalkQueueRandom(new string[] { "hp-86", "hp-85" });
        yield return new WaitWhile(() => Talker.instance.isTalkingNow);


    }

    Vector2 lastPos, delta;
    float holdTimer;
    bool isSmallGame, isFin;


    void Update()
    {
        if (isSmallGame)
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

            }
            if (Input.GetMouseButton(0))
            {

                delta = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition) - lastPos;
                lastPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                if (/*delta.magnitude != 0*/ true)
                {
                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                    if (hits != null)
                    {
                        foreach (var item in hits)
                        {
                            if (item.collider.name == "Lampa")
                            {
                                SFX.PlayLoop("трут лампу");
                                if (holdTimer == 0)
                                {
                                    SFX.Play("дым из лампы");
                                }
                                holdTimer += Time.deltaTime;
                                LampEffect.instance.SetScale(holdTimer * 1.8f);
                                _tutorialFingerAnimator.SetBool("Scrub", false);
                                if (holdTimer > 1.5f && holdTimer < 4)
                                {
                                    isSmallGame = false;

                                    StartCoroutine(Fin());
                                }
                                break;
                            }
                        }
                    }
                }
            }

        }
        if (Input.GetMouseButtonUp(0))
        {
            SFX.StopLoop("трут лампу");

       
                Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                if (hits != null)
                {
                    foreach (var item in hits)
                    {
                        if (item.collider.name == "Lampa")
                        {
                            StartCoroutine(IeClick());

                        }
                    }
                }
        


        }
    }

    IEnumerator IeClick()
    {
        isSmallGame = false;
        do
        {
            holdTimer += Time.deltaTime;
            LampEffect.instance.SetScale(holdTimer * 1.8f);

            yield return null;
        } while (holdTimer < 1.5f);

        StartCoroutine(Fin());
    }
    IEnumerator Fin()
    {
        LampEffect.instance.LauchSecondSmoke();

        yield return new WaitForSeconds(3);
        GameSettings.Difficulty.Value = GameDifficulty.TwoYears;
        SceneLoader.SwitchToScene(Scenes.Map);

    }
}
