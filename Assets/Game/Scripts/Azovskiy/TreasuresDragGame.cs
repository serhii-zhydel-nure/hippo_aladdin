﻿using UnityEngine;
using System.Collections;
using Shevchenko;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using System.Collections.Generic;
using PSV;
using DG.Tweening;

public class TreasuresDragGame : DragGame
{

    public bool game2plus;

    public GameObject chestClosed;
    public GameObject chestOpened;

    void Start()
    {
//		AudioController.PlayMusic("Loop 11 song");
//		if (!AudioController.IsSoundPlaying("Loop 11 song"))
//		{
		AudioController.Release();
			AudioController.PlaySound("Loop 11 song",  StreamGroup.MUSIC, 0.3f, true);
//		}
         StartCoroutine(Process());
    }
    void OnEnable()
    {
        DragGame.OnGameFinished += GameFinished;
        DragGame.OnDropCorrect += OnDropItem;
        DragGame.OnHelperFirst += DragGame_OnHelperFirst;
        DragGame.OnHelperSecond += DragGame_OnHelperSecond;
        DragGame.OnDropWrongNoPar += DragGame_OnDropWrongNoPar;
        //		DragGame.OnItemTaken += DragGame_OnItemTaken;
    }

    void OnDisable()
    {
        DragGame.OnGameFinished -= GameFinished;
        DragGame.OnDropCorrect -= OnDropItem;
        DragGame.OnHelperFirst -= DragGame_OnHelperFirst;
        DragGame.OnHelperSecond -= DragGame_OnHelperSecond;
        DragGame.OnDropWrongNoPar -= DragGame_OnDropWrongNoPar;
        //		DragGame.OnItemTaken -= DragGame_OnItemTaken;
    }

    void GameFinished()
    {
        Debug.Log("Finished!!");
        game = false;
    }
    void OnDropItem(DragGame_Child _DragGame_Child, DragGame_Parent _DragGame_Parent)
    {
        AudioController.PlaySound("Throw");

        Transform movingObj = _DragGame_Child.transform;
        StartCoroutine(MoveToCorrectPlace(_DragGame_Child.transform, _DragGame_Parent.transform));
        //		Transform targetObj = _DragGame_Parent.GetComponent<Package>().hand;
        //		_DragGame_Parent.GetComponent<Package>().StartCoroutine(MoveToCorrectPlace(movingObj, targetObj));

        //		if (!gameFinished)
        //		{
        //			_DragGame_Child.transform.SetParent(_DragGame_Parent.transform);
        //		}
    }

    void DragGame_OnHelperSecond()
    {
        //		Debug.Log("DragGame_OnHelperSecond");
        //SoundEffectsController.StopAnd_Play_By_Name_Delayed("hp-3", true);

    }

    void DragGame_OnHelperFirst()
    {
        //		Debug.Log("DragGame_OnHelperFirst");
        //		TalkDelayed("Put the thing on the table to control check", false);
        //		XRayItem[] items = FindObjectsOfType <XRayItem>();
        //		foreach (var i in items)
        //		{
        //			if (i.item == targetItem)
        //			{
        //				//				i.Shake();
        //			}
        //		}
    }

    private void DragGame_OnDropWrongNoPar()
    {

    }
    void DragGame_OnItemTaken(DragGame_Child child)
    {
        //		AudioController.PlaySound("knopka 1");
        //		_userSelectedRightStuff.Value.InvokeSafe();
    }
    public override bool CheckIsDropCorrect(DragGame_Parent hit2, DragGame_Child hit)
    {
        //        if (hit2.number == hit.GetComponent<DragGame_Child>().number)
        if (hit != null)
        {
            if (hit2 != null)
            {
				if (hit == childs[index])
				{
					if (hit.GetComponent<DragGame_Child>().objValue == hit2.number)
					{
						return true;
					}
					return false;
				}
                return false;
            }
            return false;
        }

        //		dragItemState = DragItemState.none;
        Debug.Log("false");
        return false;
    }

    public override IEnumerator MoveToCorrectPlace(Transform _from, Transform _to, bool useTempParentPos = false)
    {
        Debug.Log("222");
		yield return StartCoroutine(StopHelp());
        //		yield return new WaitForSeconds(0.7f);
        //		if (currentConsumer.GetComponent<Hero>().HeroType == HeroType.RacoonSon)
        //		{
        //			yield return new WaitForSeconds(0.5f);
        //		}
        Vector2 speed = Vector2.zero;
        do
        {
            Debug.Log("moving...");

            //			_from.position = Vector2.SmoothDamp(_from.position, _to.position, ref speed, 0.14f);
            _from.position = Vector2.Lerp(_from.position, _to.position, Time.deltaTime * 6);
            yield return new WaitForEndOfFrame();
        } while (Vector2.Distance(_from.position, _to.position) > 0.03f);
        Debug.Log("move to correct place finished");
		AudioController.PlaySound("Вставляет ключи в сундук");
    }

    public override void Update()
    {
        base.Update();
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // Retrieve all raycast hits from the click position and store them in an array called "hits"
            RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);

            if (hits != null)
            {
                if (hits.Length > 0)
                {
                    foreach (var item in hits)
                    {
                        if (item.collider)
                        {
                            if (GameSettings.Difficulty.Value == GameDifficulty.TwoYears)
                            {
                                if (item.collider.GetComponent<DragGame_Child>())
                                {
                                    Debug.Log("has drag game child");
                                    foreach (var parent in FindObjectsOfType<DragGame_Parent>())
                                    {
										if (item.collider.GetComponent<DragGame_Child>() == childs[index])
										{
											if (item.collider.GetComponent<DragGame_Child>().objValue == parent.number)
											{
												if (game2plus)
												{
													StartCoroutine(MoveToCorrectPlace(item.transform, parent.transform));
													//item.collider.GetComponent<DragGame_Child>().DropCorret(parent);
													item.collider.GetComponent<DragGame_Child>().SetState(DragGame_Child_State.dropCorrect, parent);

													item.collider.enabled = false;
													parent.GetComponent<Collider2D>().enabled = false;
													game2plus = false;
												}
											}
										}
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
		if (highlight)
		{
			spriteToHighlight.color = new Color(0, 1, 0, Mathf.PingPong(Time.time, 1));
		}
    }

	int index;
	List<DragGame_Child> childs;
	bool highlight;
	SpriteRenderer spriteToHighlight;

    IEnumerator Process()
    {
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(Narrator.Talk("hp-72"));

		childs = new List<DragGame_Child>();
		DragGame_Child[] temp = FindObjectsOfType<DragGame_Child>();

		foreach (var item in temp)
		{
			if (item.objValue != 5 && item.objValue != 6)
			{
				childs.Add(item);
			}
		}
		for (int i = 0; i < childs.Count; i++)
        {
            if (GameSettings.Difficulty.Value == GameDifficulty.TwoYears)
            {
				index = i;
				//TODO подсказка
				StartHelp(childs[i].gameObject);

                game2plus = true;
                yield return new WaitUntil(() => !game2plus);
            }
            else
            {
				index = i;
				StartHelp(childs[i].gameObject);
                base.OverrideLevelParams(1, new List<DragGame_Child>(FindObjectsOfType<DragGame_Child>()),
                                            new List<DragGame_Parent>(FindObjectsOfType<DragGame_Parent>()));
 
                while (base.game)
                    yield return new WaitForEndOfFrame();
            }
            Debug.Log("Game end");
        }
        Debug.Log("Game end");
        yield return new WaitForSeconds(1);
        //появляется лампа
        //переход на сцену 10

        foreach (var item in FindObjectsOfType<DragGame_Parent>())
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in FindObjectsOfType<DragGame_Child>())
        {
            item.gameObject.SetActive(false);
        }
        chestClosed.SetActive(false);
        chestOpened.SetActive(true);
		AudioController.PlaySound("Звук ковра в меню игры");
        yield return new WaitForSeconds(4.2f);
 
             SceneLoader.SwitchToScene( Scenes._10Lampa);

     }
	Coroutine pulse;
	GameObject pulsingObject;

	void StartHelp(GameObject obj)
	{
		foreach (var item in FindObjectsOfType<DragGame_Parent>())
		{
			if (item.number == childs[index].objValue)
			{
				spriteToHighlight = item.gameObject.FindChild("sprite_light").GetComponent<SpriteRenderer>();
				highlight = true;
				return;
			}
		}

//		pulse = StartCoroutine(TransformInstruments.PulseEternally(obj));
//		pulsingObject = obj;
//		return;
	}
	IEnumerator StopHelp()
	{
		spriteToHighlight.DOFade(0, 0.5f);
		highlight = false;
		Debug.Log("StopHelp");
		if (pulse != null)
		{
			StopCoroutine(pulse);
		}
		pulse = null;
		if (pulsingObject != null)
		{
			yield return StartCoroutine(pulsingObject.transform.ScaleOverSeconds(Vector3.one, 0.5f));
		}
		pulsingObject = null;
	}
}
