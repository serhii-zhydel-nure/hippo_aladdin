﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TelegaController : MonoBehaviour
{
	public Animator telegaAnimator, camelAnimator;
	public GameObject water;

	public static TelegaController instance;
	// Use this for initialization
	void Start()
	{
		instance = this;
//		telegaAnimator = GetComponentInChildren<Animator>();
	}
	
	public void Move(bool move)
	{
		telegaAnimator.SetBool("move", move);
		camelAnimator.SetBool("Walk", move);
//		camelAnimator.SetBool("Drink", !move);
//		Debug.Log("Move:" + move);
	}

	public IEnumerator MoveToDrink()
	{
		yield return new WaitForSeconds(1);
		bool wait = true;
//		telegaAnimator.SetBool("move", true);
		camelAnimator.SetBool("Walk", true);
		Vector3 pos = GameObject.Find("CamelDrinkPoint").transform.position;
		camelAnimator.transform.DOMove(pos, 2).OnComplete(() => wait = false);
		yield return new WaitWhile(() => wait);
		camelAnimator.SetBool("Walk", false);
		camelAnimator.SetBool("Drink", false);
		yield return new WaitForSeconds(0.5f);
		Drink();
		yield return new WaitForSeconds(0.5f);
		water.SetActive(true);
	}
	public void Drink()
	{
		camelAnimator.SetBool("Walk", false);
		camelAnimator.SetBool("Drink", true);
	}
}
