﻿using UnityEngine;
using System.Collections;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.InputManagement.Local;
using Cleaning;
using Shevchenko;
using DG.Tweening;
using PSV_Tutorials;
using PSV;

public class OasisController : MonoBehaviour
{
    Hero cat;
    private Trigger2D _catEnemyTrigger;
    public GameObject[] obstacles;
    private float _persentsToClean;
    private SmoothFollow _cameraSmoothFollowForPeppa;
    private Camera _camera;
    //public GameObject lamp;
    GameObject cleaningDust;

    //	public Animator camelAnimator;

    bool chooseSpell;

    enum Spell
    {
        none,
        freeze,
        fire,
        shrink,
    }
    FingerTutorial finger;

    Spell spell = Spell.none;
    // Use this for initialization
    private void Awake()
    {
        FingerManager.Init();                                                             //

    }
    void Start()
    {
        cat = FindObjectOfType<Hero>();
        _catEnemyTrigger = GameObject.Find("EnemyTrigger").GetComponent<Trigger2D>();
        _persentsToClean = GameSettings.Difficulty.Value == GameDifficulty.TwoYears ? 50 : 60;
        _camera = Camera.main;
        cleaningDust = GameObject.Find("Scene11Smoke");
        cleaningDust.SetActive(false);
        StartCoroutine(Process());
        //		if (!AudioController.IsSoundPlaying("Loop 11 song"))
        //		{
        AudioController.Release();
        AudioController.PlaySound("Loop 11 song",  StreamGroup.MUSIC, 0.3f, true);
        //		}
         //		PanelAlign.ShowPanel("Panel");
    }

    public IEnumerator Process()
    {
        yield return new WaitForSeconds(0.5f);
        //Этот караванщик заплутал в пустыни. Давай поможем выйти ему к оазису.
//        yield return StartCoroutine(Narrator.Talk("hp-77"));
		Talker.instance.TalkQueue("hp-77");
		yield return new WaitWhile(() => Talker.instance.isTalkingNow);
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < obstacles.Length; i++)
        {
            TelegaController.instance.Move(true);
            yield return StartCoroutine(cat.Walking.Walk(obstacles[i].FindChild("TargetPoint").transform.position));
            TelegaController.instance.Move(false);
            if (obstacles[i].name.Contains("Mirage"))
            {
                yield return StartCoroutine(MirageMiniGame(obstacles[i].GetComponentInChildren<SpriteRenderer>()));
            }
            if (obstacles[i].name.Contains("Ifrit"))
            {
                yield return StartCoroutine(IfritMiniGame(obstacles[i]));
            }
        }
        //		Мы помогли караванщику.
        StartCoroutine(TelegaController.instance.MoveToDrink());
        cat.Animator.SetBool("Loop", true);
        cat.Animator.Play("JoyJumping");

//		Talker.instance.TalkQueue("ep-hp-27");
//		yield return new WaitForSeconds(0.5f);
		AudioController.PlaySound("ep-hp-27");
//        yield return StartCoroutine(Narrator.Talk("hp-84"));

		Talker.instance.TalkQueue("hp-84");
		yield return new WaitWhile(() => Talker.instance.isTalkingNow);
        cat.Animator.SetBool("Loop", false);
		AudioController.PlaySound("ep-hp-27");
        yield return new WaitForSeconds(5);
         SceneLoader.SwitchToScene( Scenes.Map);
        //натыкаемся на препятствие и тормозим
        //запускаем миниигру
    }

    private void CatTriggerHandler(Collider2D otherCollider)
    {
        if (otherCollider.name.Contains("Mirage"))
        {
            cat.Walking.StopWalking();
        }
    }

    bool miragePhrase;

    bool cleaning;

	SpriteRenderer mirageSprite;

    IEnumerator MirageMiniGame(SpriteRenderer sprite)
    {
        _2dxFX_Liquify lf = sprite.gameObject.GetComponent<_2dxFX_Liquify>();

        GameObject goWater = sprite.transform.root.GetComponentInChildren<Water2DScript>(true).gameObject;
        goWater.SetActive(true);
        GameObject LightRotator = null;
        foreach (var item in sprite.transform.root.GetComponentsInChildren<Transform>(true))
        {
            if (item.name == "LightRotator")
            {
                LightRotator = item.gameObject;
            }
        }
        StaticParams.SetColor(LightRotator, 0);
        AudioController.PlaySound("дым из лампы");
        lf.TurnToLiquid = 1;
        while (lf.TurnToLiquid > 0.005f)
        {
            lf.TurnToLiquid = Mathf.Lerp(lf.TurnToLiquid, 0, Time.deltaTime);
            yield return null;

        }
        while (lf.TurnToLiquid > 0.0001f)
        {
            lf.TurnToLiquid = Mathf.Lerp(lf.TurnToLiquid, 0, Time.deltaTime * 10);
            yield return null;

        }
        Destroy(lf);

        LightRotator.SetActive(true);
        LightRotator.transform.LerpColor(1);

		mirageSprite = sprite;
        if (!Input.GetMouseButton(0))
        {


            finger = new FingerTutorial(
                new Wipe(sprite.transform.position).SetLoops(5)

                        ).SetLoops(1000).OnUpdate(() => Check());
            finger.Play();
            GameObject fin = GameObject.Find("FingerPrefab(Clone)");
            fin.GetComponent<Renderer>().sortingOrder = 100;
            fin.layer = 4;
        }


        if (!miragePhrase)
        {
            //		Это мираж! Давай сотрем его.
//            yield return StartCoroutine(Narrator.Talk("hp-78"));

			Talker.instance.TalkQueue("hp-78");
			yield return new WaitWhile(() => Talker.instance.isTalkingNow);
            miragePhrase = true;
        }

        //yield return new WaitForSeconds(0.1f);
        //		Води пальцем по миражу.
//        StartCoroutine(Narrator.Talk("hp-79"));
		Talker.instance.TalkQueue("hp-79");
        Vector3 eraserSize = new Vector3(0.3f, 0.3f, 0.3f);
        cleaning = true;
		cleaningTimerRunning = true;
        yield return StartCoroutine(CleanLauncher.CleanSprite(sprite, _persentsToClean, null, eraserSize, "стерает мираж"));
        cleaning = false;
		cleaningTimerRunning = false;
        cleaningDust.GetComponent<ParticleSystem>().Stop();
        yield return new WaitForSeconds(1);
        sprite.gameObject.SetActive(false);
        goWater.SetActive(false);
        //LightRotator.SetActive(false);
		GameObject finDestr = GameObject.Find("FingerPrefab(Clone)");
		GameObject.Destroy(finDestr);

        LightRotator.transform.LerpColor(0);
        Instantiate(Resources.Load<GameObject>("Sahara/Effects/heal_LV3"), sprite.transform.position, Quaternion.identity);
        AudioController.PlaySound("Звук ковра в меню игры");
		cat.Animator.SetBool("Loop", true);
		cat.Animator.Play("JoyJumping");
//		yield return new WaitForSeconds(5.5f);
//		Talker.instance.TalkQueue("ep-hp-27");
//		AudioController.PlaySound("ep-hp-27");
		yield return new WaitForSeconds(2);
		cat.Animator.SetBool("Loop", false);
		AudioController.PlaySound("ep-hp-27");
        yield return new WaitForSeconds(1.5f);

    }
    void Check()
    {
        if (Input.GetMouseButtonDown(0) || Input.touchCount > 0)
        {
            if (finger.IsPlaying)
            {
                finger.Hide();
            }
        }

    }

    GameObject ifrit;

    bool ifritPhrase;


    IEnumerator IfritMiniGame(GameObject obj)
    {
        if (!ifritPhrase)
        {
            //		Это злобный ифрит - дух пустыни.
//            yield return StartCoroutine(Narrator.Talk("hp-80"));
			Talker.instance.TalkQueue("hp-80");
			yield return new WaitWhile(() => Talker.instance.isTalkingNow);
            yield return new WaitForSeconds(0.3f);
            //		Давай прогоним его!
//            yield return StartCoroutine(Narrator.Talk("hp-81"));
			Talker.instance.TalkQueue("hp-81");
			yield return new WaitWhile(() => Talker.instance.isTalkingNow);
            ifritPhrase = true;
        }
        else yield return new WaitForSeconds(1.5f);
        //появляется лампа
        Oasis_Lamp.instance.SetEnableDisable(true);


        //			Води пальцем по лампе, чтобы вызвать ряд заклинаний.
//        StartCoroutine(Narrator.Talk("hp-82"));
		Talker.instance.TalkQueue("hp-82");
        //		}
        //запустить шкряболку лампы
        isSmallGame = true;
        yield return new WaitWhile(() => isSmallGame);
        //появляется панель с заклинаниями
        yield return new WaitForSeconds(0.7f);
//        StartCoroutine(Narrator.Talk("hp-83"));
		Talker.instance.TalkQueue("hp-83");
        chooseSpell = true;
        ifrit = obj;
        yield return new WaitWhile(() => chooseSpell);
		yield return new WaitForSeconds(1);
		cat.Animator.SetBool("Loop", true);
		cat.Animator.Play("JoyJumping");
//		Talker.instance.TalkQueue("ep-hp-27");
//		yield return new WaitForSeconds(0.5f);
//		
		yield return new WaitForSeconds(2);
		cat.Animator.SetBool("Loop", false);
		AudioController.PlaySound("ep-hp-27");
		yield return new WaitForSeconds(1.5f);
    }

    Vector2 lastPos, delta;
    float holdTimer;
    bool isSmallGame;

    IEnumerator CastSpell()
    {
        yield return new WaitForSeconds(2);
        chooseSpell = false;
    }

    bool tapMiniGame;
    bool coolDown;
    float tapCoolDownTimer;
	bool tapHelp;
	float tapHelpTimer;

	bool cleaningTimerRunning;
	float cleaningTimer;

    private void Update()
    {
        if (isSmallGame)
        {
            if (Input.GetMouseButtonDown(0))
            {
                lastPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

            }
            if (Input.GetMouseButton(0))
            {

                delta = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition) - lastPos;
                lastPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                if (delta.magnitude != 0)
                {

                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                    if (hits != null)
                    {
                        foreach (var item in hits)
                        {
                            if (item.collider.name == "Lamp")
                            {
                                if (!AudioController.IsSoundPlaying("Вытирает что-то"))
                                {
                                    AudioController.PlaySound("Вытирает что-то");
                                }
                                holdTimer += Time.deltaTime;
                                LampEffect.instance.SetScale(holdTimer * 1f);
                                if (holdTimer > 3)
                                {
                                    if (AudioController.IsSoundPlaying("Вытирает что-то"))
                                    {
                                        AudioController.StopSound("Вытирает что-то");
                                    }
                                    isSmallGame = false;
                                    //									holdTimer = 0;
                                    LampEffect.instance.SetScale(holdTimer * 1f);
                                    //									lamp.SetActive(false);
                                    PanelAlign.ShowPanel("Panel");

                                }
                                //								PanelAlign.ShowPanel("Panel");
                            }
                            else
                            {
                                if (AudioController.IsSoundPlaying("Вытирает что-то"))
                                {
                                    AudioController.StopSound("Вытирает что-то");
                                }
                            }
                        }
                    }
                    else
                    {
                        if (AudioController.IsSoundPlaying("Вытирает что-то"))
                        {
                            AudioController.StopSound("Вытирает что-то");
                        }
                    }
                }
            }
            else
            {
                if (AudioController.IsSoundPlaying("Вытирает что-то"))
                {
                    AudioController.StopSound("Вытирает что-то");
                }
            }
            //DirectionAngleInput();
            //ProgressBarBase.instance.SetProgress(progressBarValue);
        }
        if (chooseSpell)
        {
            if (Input.GetMouseButtonUp(0))
            {
                Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                if (hits != null)
                {
                    foreach (var item in hits)
                    {
                        if (item.collider.name == "Spell1")
                        {
                            //freeze
                            //							chooseSpell = false;
                            StartCoroutine(CastSpell());
                            spell = Spell.freeze;
                            //TODO effects
                            holdTimer = 0;
                            LampEffect.instance.SetScale(holdTimer * 1f);
                            //lamp.SetActive(false);
                            Oasis_Lamp.instance.SetEnableDisable(false);

                            StartCoroutine(ifrit.GetComponent<IfritController>().Freeze());
                            Oasis_Lamp.instance.LaunchEffect(ifrit.transform.Find("TargetHitPoint"));
                            //							ifrit.transform.DOScale(0, 1);
                            //							ifrit.DissapearObjectOverSeconds(1, true, true);
                            PanelAlign.HideAllPanels();
                        }
                        else if (item.collider.name == "Spell2")
                        {
                            //fire
                            //							chooseSpell = false;
                            StartCoroutine(CastSpell());
                            spell = Spell.fire;
                            //TODO effects
                            //							ifrit.transform.DOScale(0, 1);
                            holdTimer = 0;
                            LampEffect.instance.SetScale(holdTimer * 1f);
                            //lamp.SetActive(false);
                            Oasis_Lamp.instance.SetEnableDisable(false);
                            Oasis_Lamp.instance.LaunchEffect(ifrit.transform.Find("TargetHitPoint"));

                            StartCoroutine(ifrit.GetComponent<IfritController>().Fire());
                            //							ifrit.DissapearObjectOverSeconds(1, true, true);
                            PanelAlign.HideAllPanels();
                        }
                        else if (item.collider.name == "Spell3")
                        {
                            //size
                            tapMiniGame = true;
                            //							StartCoroutine(CastSpell());
                            spell = Spell.shrink;
                            ifrit.GetComponent<IfritController>().StartRun();
                            Oasis_Lamp.instance.LaunchEffect(ifrit.transform.Find("TargetHitPoint"));

                            //TODO effects
                            //							ifrit.transform.DOScale(0, 1);
                            //							ifrit.DissapearObjectOverSeconds(1, true, true);
                            PanelAlign.HideAllPanels();
                        }
                        else
                        {
                            spell = Spell.none;
                        }
                    }
                }
            }
        }
        if (cleaning)
        {
			if (cleaningTimerRunning)
			{
				cleaningTimer -= Time.deltaTime;
				if (cleaningTimer < 0)
				{
					cleaningTimerRunning = false;
					cleaningTimer = 5;
					if (finger == null)
					{
						finger = new FingerTutorial(
							new Wipe(mirageSprite.transform.position).SetLoops(5)

						).SetLoops(1000).OnUpdate(() => Check());
						finger.Play();
						GameObject fin = GameObject.Find("FingerPrefab(Clone)");
						fin.GetComponent<Renderer>().sortingOrder = 100;
						fin.layer = 4;
					}
					else
					{
						finger.Play();
					}

				}
			}

            if (Input.GetMouseButtonDown(0))
            {
                cleaningDust.SetActive(true);
                cleaningDust.GetComponent<ParticleSystem>().Play();
            }
            if (Input.GetMouseButton(0))
            {
				cleaningTimer = 5;
				cleaningTimerRunning = true;
                cleaningDust.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 4);
            }
            if (Input.GetMouseButtonUp(0))
            {
                cleaningDust.GetComponent<ParticleSystem>().Stop();
            }
        }
        if (tapMiniGame)
        {
            tapCoolDownTimer -= Time.deltaTime;
            if (tapCoolDownTimer < 0)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                    if (hits != null)
                    {
                        foreach (var item in hits)
                        {
                            if (item.collider.name.Contains("fox_papa"))
                            {
                                tapCoolDownTimer = 0.5f;
                                ifrit.transform.DOScale(ifrit.transform.localScale.x - 0.25f, 0.4f);
                                ifrit.GetComponent<IfritController>().Jump();
                                AudioController.PlaySound("Firework_01");

                                Destroy(Instantiate(Resources.Load<GameObject>("Sahara/Effects/2D_Firework_01"), ifrit.transform.position, Quaternion.identity), 5);

								tapHelpTimer = 5;

                                if (ifrit.transform.localScale.x <= 0.3f)
                                {
                                    ifrit.transform.DOScale(0, 0.3f);
                                    tapMiniGame = false;
                                    holdTimer = 0;
                                    LampEffect.instance.SetScale(holdTimer * 1f);
                                    //lamp.SetActive(false);
                                    Oasis_Lamp.instance.SetEnableDisable(false);

                                    StartCoroutine(CastSpell());
                                }
                            }
                        }
                    }
                }
            }
			tapHelpTimer -= Time.deltaTime;
			if (tapHelpTimer < 0)
			{
				tapHelpTimer = 5;
				Talker.instance.TalkQueue("uv-hp-97");
			}
        }
    }
}
