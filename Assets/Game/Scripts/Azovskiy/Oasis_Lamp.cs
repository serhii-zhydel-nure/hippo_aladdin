﻿using UnityEngine;
using System.Collections;
using PSV;

public class Oasis_Lamp : MonoBehaviour {
    public static Oasis_Lamp instance;
    _2dxFX_DestroyedFX fx;
    float targetMove;
    ThrowSimulator ts;

    private void Awake()
    {
        instance = this;
        fx = GetComponentInChildren<_2dxFX_DestroyedFX>();
        fx.Destroyed = 1;
        ts = GetComponent<ThrowSimulator>();
        ts.startShotPos = transform.Find("StartShotPos");
    }

    public void SetEnableDisable(bool _enable)
    {
        if (_enable)
        {
            //Звук ковра в меню игры
            AudioController.PlaySound("Звук ковра в меню игры");

            targetMove = 0;

        }else
        {
            targetMove =  1;
        }
        if (ieMover !=null)
        {
            StopCoroutine(ieMover);
        }
        ieMover = IeMover();
        StartCoroutine(ieMover);
    }
    IEnumerator ieMover;
    IEnumerator IeMover()
    {
        while (fx.Destroyed!= targetMove)
        {
            fx.Destroyed = Mathf.MoveTowards(fx.Destroyed, targetMove, Time.deltaTime);
            yield return null;
        }
    }
    public void LaunchEffect(Transform _target)
    {
        AudioController.PlaySound("331621__hykenfreak__flame-ignition");
        ts.DoThrow(_target);
    }
    private void OnEnable()
    {
        ThrowSimulator.OnThrowStart += ThrowSimulator_OnThrowStart;
        ThrowSimulator.OnThrowSuccess += ThrowSimulator_OnThrowSuccess;

    }
    private void OnDisable()
    {
        ThrowSimulator.OnThrowStart -= ThrowSimulator_OnThrowStart;
        ThrowSimulator.OnThrowSuccess -= ThrowSimulator_OnThrowSuccess;
    }

    private void ThrowSimulator_OnThrowSuccess(Transform _bullet)
    {
        _bullet.Find("Explosion").gameObject.SetActive(true);
    }

    private void ThrowSimulator_OnThrowStart(Transform _bullet)
    {
         
    }
}
