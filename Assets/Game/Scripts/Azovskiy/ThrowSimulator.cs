﻿using UnityEngine;
using System.Collections;

public class ThrowSimulator : MonoBehaviour {

    public delegate void ThrowStart(Transform _bullet);
    public static event ThrowStart OnThrowStart;
    public delegate void Throw();
    public static event ThrowStart OnThrowCancel;
    public static event ThrowStart OnThrowSuccess;

    //public Transform Target;
    public float firingAngle = 45.0f;
    public float gravity = 9.8f;
    public float delayBeforeDrop = 2;

    Transform bullet;
    GameObject src_bullet, src_DelayTimer;
    public Transform startShotPos;

     float rateDelayTimer;

    void Awake()
    {
          src_bullet = Resources.Load<GameObject>("Sahara/Frostbolt");
        src_DelayTimer = Resources.Load<GameObject>("DelayTimer");
        rateDelayTimer = 1 / delayBeforeDrop;
    }

     public Transform targ;

    void Update()
    {
         if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("KeyCode.F");
            bullet = Instantiate(src_bullet).transform;
            StartCoroutine(SimulateThrow(targ.position));
        }
     }

    public void DoThrow(Transform _target)
    {
        bullet = Instantiate(src_bullet).transform;
        targ = _target;
        StartCoroutine(SimulateThrow(targ.position));

    }
    IEnumerator SimulateThrow(Vector3 _target)
    {
         // Move projectile to the position of throwing object + add some offset if needed.
        bullet.position = startShotPos.position + new Vector3(0, 0.0f, 0);
        if (OnThrowStart!=null)
        {
            OnThrowStart(bullet);

        }

        // Calculate distance to target
        float target_Distance = Vector3.Distance(bullet.position, _target);

        // Calculate the velocity needed to throw the object to the target at specified angle.
        float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = target_Distance / Vx;

        // Rotate projectile to face the target.
        bullet.rotation = Quaternion.LookRotation(_target - bullet.position);

        float elapse_time = 0;

        while (elapse_time < flightDuration)
        {
            bullet.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

            elapse_time += Time.deltaTime;

            yield return null;
        }
        if (OnThrowSuccess!=null)
        {
            OnThrowSuccess(bullet);
        }
        //bell.GetComponent<BellController>().BellFallAction();
    }
}
