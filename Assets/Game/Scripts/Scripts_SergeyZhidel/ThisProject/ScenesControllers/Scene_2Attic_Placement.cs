﻿using System;
using System.Collections;
using DG.Tweening;
using PSV;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.InputManagement.Local;
using UnityEngine;

namespace Scripts_SergeyZhidel.ScenesManagment
{
	public class Scene_2Attic_Placement : SceneController
	{
		private Hero _pepa;
		private Hero _gim;
		private Camera _camera;
		private float _cameraStartSize;
		private Vector3 _cameraStartPos;

		private string[] _wrongDrag =
			{
				"hp-21", //Этому тут не место.
				"hp-20" //Это не то, что мы ищем.
			};

		private HelpByTimeIfUserDontDoRightAction _helpByTimeIfUserDontTakeRightStuff;
		private TalkPhraseHelp _phraseHelp;

		public Scene_2Attic_Placement()
			: base(Scenes._2Attic_Placement)
		{
		}

		public override void PrepareSceneToScenario()
		{
			base.PrepareSceneToScenario();

			_pepa = HeroesOnScene[HeroType.HippoPepa];
			_gim = HeroesOnScene[HeroType.HippoGeorge];
			_camera = Camera.main;
			_cameraStartSize = _camera.orthographicSize;
			_cameraStartPos = _camera.transform.position;

			_phraseHelp = _camera.gameObject.AddComponentWithInit<TalkPhraseHelp>(component => component.Speaker = _pepa.SoundBehavior);
			_helpByTimeIfUserDontTakeRightStuff = HelpByTimeIfUserDontDoRightAction.GetNewInstance();
			_helpByTimeIfUserDontTakeRightStuff.AddUserHelpers(_phraseHelp);
//			AudioController.PlayMusic("Loop 1 song");
//			if (!AudioController.IsSoundPlaying("Loop 1 song"))
//			{
			AudioController.Release();
				AudioController.PlaySound("Loop 1 song",  StreamGroup.MUSIC, 0.3f, true);
//			}    
		}

		public override IEnumerator RunScenario()
		{
			//Расставь вещи по местам
			yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-10"));

			//Найди предметы и поставь их на место
			yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-11"));


			string phrase = "hp-12";//Найди велосипед
			yield return StartCoroutine(_pepa.SoundBehavior.Talk(phrase));
			GameObject parrent = this.Find("Drag_bicycle");
			GameObject draggableObj = parrent.FindChild("bicycle");
			GameObject target = parrent.FindChild("bicycle_target");
			yield return StartCoroutine(PlayDragGame(draggableObj, target, phrase));


			phrase = "hp-13";//Где же мяч?
			yield return StartCoroutine(_pepa.SoundBehavior.Talk(phrase));
			parrent = this.Find("Drag_ball");
			draggableObj = parrent.FindChild("ball");
			target = parrent.FindChild("ball_target");
			yield return StartCoroutine(PlayDragGame(draggableObj, target, phrase));


			phrase = "hp-14";//Куда пропал сачок?
			yield return StartCoroutine(_pepa.SoundBehavior.Talk(phrase));
			parrent = this.Find("Drag_Nettle");
			draggableObj = parrent.FindChild("Nettle");
			target = parrent.FindChild("Nettle_target");
			yield return StartCoroutine(PlayDragGame(draggableObj, target, phrase));


			phrase = "hp-15"; //Где же лестница?
			yield return StartCoroutine(_pepa.SoundBehavior.Talk(phrase));
			parrent = this.Find("Drag_ladder");
			draggableObj = parrent.FindChild("ladder");
			target = parrent.FindChild("ladder_target");
			yield return StartCoroutine(PlayDragGame(draggableObj, target, phrase));


			phrase = "hp-16"; //Найди метлу.
			yield return StartCoroutine(_pepa.SoundBehavior.Talk(phrase));
			parrent = this.Find("Drag_broom");
			draggableObj = parrent.FindChild("broom");
			target = parrent.FindChild("broom_target");
			yield return StartCoroutine(PlayDragGame(draggableObj, target, phrase));


			phrase = "hp-17";  //Где же ракетка?
			yield return StartCoroutine(_pepa.SoundBehavior.Talk(phrase));
			parrent = this.Find("Drag_racquet");
			draggableObj = parrent.FindChild("racquet");
			target = parrent.FindChild("racquet_target");
			yield return StartCoroutine(PlayDragGame(draggableObj, target, phrase));

			//Вот мы и справились.
			yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-22"));
			MapProgressController.SetSceneCompleted();
			LoadNextScene(Scenes.Map);
		}

		private IEnumerator PlayDragGame(GameObject draggableObj, GameObject target, string phraseNothingHappens)
		{
			_phraseHelp.PhrasesToTalk.Add(phraseNothingHappens);

			target.SetActive(true);
			yield return StartCoroutine(DragStuff(draggableObj, target.GetComponent<Collider2D>(),
					draggedObjRight =>
					{
						OnObjDraggedSuccess(draggedObjRight, target);
					},
					draggedObjWrong =>
					{
						StartCoroutine(_pepa.SoundBehavior.Talk(_wrongDrag.GetRandomElement()));
					}));

			_phraseHelp.PhrasesToTalk.Remove(phraseNothingHappens);
			AudioController.PlaySound("Собирает элементы коврика, убирает вещи на чердаке, находит Джи на рынке");
			yield return StartCoroutine(_pepa.SoundBehavior.Talk(Common.RightPhrases.GetRandomElement()));

			yield return new WaitForSeconds(0.5f);//чтоб не так резко начиналась новая игра
		}

		private IEnumerator DragStuff(GameObject dragableObj, Collider2D target, Action<GameObject> finishSuccess, Action<GameObject> finishFail)
		{
			bool draggingFinished = false;

			DragGameController dragControll = new DragGameController(dragableObj, target);

			dragControll.StartDragging += obj => obj.AddComponentIfNotExist<GroupSorter>().GroupAdditionalOrder = 1000;

			dragControll.FinishedSuccessfully += finishSuccess;
			dragControll.FinishedUnSuccessfully += finishFail;

			//слои при перетаскивании
			dragControll.FinishedUnSuccessfully += obj => obj.GetComponent<GroupSorter>().GroupAdditionalOrder -= 1000;
			dragControll.FinishedSuccessfully += obj => obj.GetComponent<GroupSorter>().GroupAdditionalOrder -= 1000;
			;

			//настройка подсказок
			dragControll.StartDragging += obj => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
			dragControll.FinishedSuccessfully += obj => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
			dragControll.FinishedUnSuccessfully += obj => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidWrongAction(); 

			dragControll.FinishedSuccessfully += obj => draggingFinished = true;

			_helpByTimeIfUserDontTakeRightStuff.StartWork();
			yield return new WaitUntil(() => draggingFinished);
			_helpByTimeIfUserDontTakeRightStuff.Stop();
			dragControll.Destroy();
		}

		private void OnObjDraggedSuccess(GameObject obj, GameObject target)
		{
			target.SetActive(false);
			obj.transform.DOShakeScale(0.5f, 0.15f, 5);
			obj.transform.DOMove(target.transform.position, 0.4f);
			obj.transform.DORotate(target.transform.rotation.eulerAngles, 0.4f);
		}


	}
}