﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Scripts_SergeyZhidel.Treasures
{
  public class BoxStuffGenerator : MonoBehaviour
  {
    public RightStuffPanel Panel;

    private Stack<GameObject> RightBoxStuff;
    private GameObject[] WrongBoxStuff;
//		public List<GameObject> RightStuff;

    private int _maxWrongStuffGeneratedInSequence = 3;
    private int _wrongStuffGeneratedInSequenceCounter;

    private bool PanelAllowRightStuff { get { return Panel.AddedRightStuff.Count < Panel.DesiredRigthStuff.Count(); } }

    void Start()
    {
      GameObject[] allBoxStuff = ResourcesFolder.GetAllPrefabs("Box Suff");
//			RightStuff = new List<GameObject>();
//			RightStuff.AddRange(allBoxStuff
//				.Where(stuff =>
//					Panel.DesiredRigthStuff.Any(desiredStuff => desiredStuff.name == stuff.name))//есть ли обьект с таким именем на панеле
//				.ToArray());
//			RightBoxStuff = RightStuff.Shuffle().ToStack();
//      RightBoxStuff = allBoxStuff
//        .Where(stuff =>
//        Panel.DesiredRigthStuff.Any(desiredStuff => desiredStuff.name == stuff.name))//есть ли обьект с таким именем на панеле
//       .ToArray().Shuffle().ToStack();
      WrongBoxStuff = allBoxStuff.Where(stuff => stuff.name.Contains("Wrong")).ToArray();
			RightBoxStuff = allBoxStuff.Where(stuff => stuff.name.Contains("Right")).ToArray()./*Shuffle().*/ToStack();

			Debug.Log(String.Format("<color=blue>RightBoxStuff: {0}</color>", RightBoxStuff.ToStringAll()));
    }

    public GameObject GenerateNewStuffForBox()
    {
      WrongBoxStuff.Shuffle();

      GameObject result;
      if (Random.Range(0, 4) == 0 && PanelAllowRightStuff && RightBoxStuff.Count > 0)
      {
        result = Instantiate(RightBoxStuff.Pop());
        _wrongStuffGeneratedInSequenceCounter = 0;
      }
      else
      {
        if (_wrongStuffGeneratedInSequenceCounter > _maxWrongStuffGeneratedInSequence && PanelAllowRightStuff &&
            RightBoxStuff.Count > 0)
        {
          result = Instantiate(RightBoxStuff.Pop());
          _wrongStuffGeneratedInSequenceCounter = 0;
        }
        else
        {
          result = Instantiate(WrongBoxStuff.GetRandomItem());
          _wrongStuffGeneratedInSequenceCounter++;
        }
      }

      return result;
    }


//		GameObject Pop()
//		{
//			GameObject result = null;
//			result = RightStuff.First();
//			Debug.Log(result.name);
//			RightStuff.RemoveAt(0);
//			return result;
//		}

  }
}