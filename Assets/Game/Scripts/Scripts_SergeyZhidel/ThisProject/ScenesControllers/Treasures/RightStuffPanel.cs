﻿using System.Collections.Generic;
using System.Linq;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

public class RightStuffPanel : MonoBehaviour {

	public GameObject[] DesiredRigthStuff;
  public List<GameObject> AddedRightStuff = new List<GameObject>();

  void Awake()
  {
    DesiredRigthStuff = gameObject.GetChildrenFirstLevel().Where(child => child.name.Contains("Right")).ToArray();
  }
}
