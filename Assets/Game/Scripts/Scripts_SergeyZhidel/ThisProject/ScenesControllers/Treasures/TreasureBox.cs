﻿using System;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.Treasures;
using UnityEngine;
using Random = UnityEngine.Random;
using PSV;

public class TreasureBox : MonoBehaviour
{
  public float GeneratedSuffAngleMin = -25;
  public float GeneratedSuffAngleMax = 25;

  public float MinForce = 1;
  public float MaxForce = 100;

  [SerializeField]
  private ParticleSystem _cointParticles;

  [SerializeField]
  private GameObject _placeForNewStuff;

  public BoxStuffGenerator StuffGenerator;

  public event Action<GameObject> StuffRunAway;

  // Use this for initialization
  void Awake()
  {
    if (_cointParticles == null)
      _cointParticles = gameObject.FindChild("CiontsParticles").GetComponent<ParticleSystem>();


    if (_placeForNewStuff == null)
      _placeForNewStuff = gameObject.FindChild("NewStuffPlace");

    if (StuffGenerator == null)
      StuffGenerator = FindObjectOfType<BoxStuffGenerator>();
  }


  public void Open()
  {
    if (!_cointParticles.isPlaying)
    {
      _cointParticles.Play();
      GameObject generatedStuff = StuffGenerator.GenerateNewStuffForBox();

      generatedStuff.transform.SetOn2DPosition(_placeForNewStuff);
      Quaternion calculatedRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(GeneratedSuffAngleMin, GeneratedSuffAngleMax)));
      generatedStuff.transform.rotation = calculatedRotation;

      Vector2 calculatedForce = generatedStuff.transform.localRotation * Vector2.up;
      calculatedForce.Normalize();
      calculatedForce *= Random.Range(MinForce, MaxForce);
      //stuffBody.AddRelativeForce(new Vector3(moveHorizontal, 0, moveVertical) * speed); = calculatedForce;
      //float power = UnityEngine.Random.Range(MinForce, MaxForce);

      Rigidbody2D stuffBody = generatedStuff.AddComponent<Rigidbody2D>();
      stuffBody.velocity = calculatedForce;

      StuffRunAway.InvokeSafe(generatedStuff);
    }
  }

	void Update()
	{
		if (_cointParticles.isPlaying)
		{
			if (!AudioController.IsSoundPlaying("Монеты с сундука"))
			{
				AudioController.PlaySound("Монеты с сундука");
			}
		}
	}
}
