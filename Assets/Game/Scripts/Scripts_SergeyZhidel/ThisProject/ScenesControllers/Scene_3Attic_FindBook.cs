﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PSV;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts_SergeyZhidel.ScenesManagment
{
  class Scene_3Attic_FindBook : SceneController
  {
    private Hero _pepa;
    private Hero _gim;
    private GameObject _bookInGimHand;
    private Camera _camera;
    private float _cameraStartSize;
    private Vector3 _cameraStartPos;

    public Scene_3Attic_FindBook() : base(Scenes._3Attic_Book)
    {
    }

    public override void PrepareSceneToScenario()
    {
      base.PrepareSceneToScenario();

      _pepa = HeroesOnScene[HeroType.HippoPepa];
      _gim = HeroesOnScene[HeroType.HippoGeorge];
      _camera = Camera.main;
      _cameraStartSize = _camera.orthographicSize;
      _cameraStartPos = _camera.transform.position;
      _bookInGimHand = _gim.gameObject.FindChild("Book");
//			AudioController.PlayMusic("Loop 1 song");
//			if (!AudioController.IsSoundPlaying("Loop 1 song"))
//			{
			AudioController.Release();
				AudioController.PlaySound("Loop 1 song",  StreamGroup.MUSIC, 0.3f, true);
//			}
    }

    public override IEnumerator RunScenario()
    {
      yield return new WaitForSeconds(1);

      _gim.CharacterAnimations.IdleType = IdleTypes.InBoxLegsUp;//подготовка к телепанию после прыжка

      //разгон
      _gim.Walking.WalkSpeed = 8;
      _gim.Walking.AllowAutoSetOpinionDirectionOnFinishMove = false;
      yield return StartCoroutine(_gim.Walking.Walk("JumpInBoxPos"));

      //прыжок
      this.StartWithDelay(0.1f, _gim.transform.MoveOverSeconds(this.Find("InBox"), 0.5f));
      this.StartWithDelay(0.2f, () => _gim.GetComponent<LayerSorter>().AdditionalOrder -= 100);
      yield return StartCoroutine(_gim.Animator.PlayFullAnimation("JumpInBox", true));

      //телепание(автовозврат в Idle)
			AudioController.PlaySound("Джи роется в коробке");

      yield return new WaitForSeconds(1);
			AudioController.PlaySound("Джи смеется 2");
			yield return new WaitForSeconds(2);

      //выпрыгивание
      this.StartWithDelay(0.3f, () => _bookInGimHand.SetActive(true));
      this.StartWithDelay(0.2f, () => _gim.GetComponent<LayerSorter>().AdditionalOrder += 100);
      this.StartWithDelay(0.2f, _gim.transform.MoveOverSeconds(this.Find("NearToBox"), 0.4f));
      _gim.Walking.WalkSpeed = 5;
      _gim.CharacterAnimations.IdleType = IdleTypes.StayWithStuffInBackHand;
      _gim.CharacterAnimations.WalkingType = WalkingTypes.RunWithStuffInBackHand;
      yield return StartCoroutine(_gim.Animator.PlayFullAnimation("JumpOutOfBox"));
      //Книга-книга!
      StartCoroutine(_gim.SoundBehavior.Talk("ji-1"));
      yield return StartCoroutine(_gim.Animator.PlayFullAnimation("JoyStanding", true));
      yield return StartCoroutine(_gim.Animator.PlayFullAnimation("JoyStanding", true));
      yield return new WaitForSeconds(1.2f);

      //бежать к пепе
      yield return StartCoroutine(_gim.Walking.Walk("NearToPepa"));
      yield return new WaitForSeconds(0.5f);
      //Это же восточные сказки!
      yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-23"));
      //Давай почитаем.
      yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-24"));
      LoadNextScene(Scenes._4Book);
    }
  }
}
