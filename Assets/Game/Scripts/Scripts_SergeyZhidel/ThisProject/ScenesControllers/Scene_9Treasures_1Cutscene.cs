﻿using System.Collections;
using PSV;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.ScenesManagment
{
  public class Scene_9Treasures_1Cutscene:SceneController
  {
    private Hero _pepa;
    private Hero _gim;
    private Camera _camera;
    private float _cameraStartSize;
    private Vector3 _cameraStartPos;

    public Scene_9Treasures_1Cutscene() : base(Scenes._9Treasures_1Cutscene)
    {
    }

    public override void PrepareSceneToScenario()
    {
      base.PrepareSceneToScenario();

      _pepa = HeroesOnScene[HeroType.HippoPepa];
      _gim = HeroesOnScene[HeroType.HippoGeorge];
      _camera = Camera.main;
      _cameraStartSize = _camera.orthographicSize;
      _cameraStartPos = _camera.transform.position;
//			if (!AudioController.IsSoundPlaying("Loop 11 song"))
//			{
			AudioController.Release();
				AudioController.PlaySound("Loop 11 song",  StreamGroup.MUSIC, 0.3f, true);
//			}
     }

    public override IEnumerator RunScenario()
    {
      _pepa.Walking.WalkSpeed = 3;
      _gim.Walking.WalkSpeed = 4;
     CoroutineTracker pepaWalk = _pepa.Walking.Walk("FirstCameOnScenePepa").GetTrack();
     CoroutineTracker gimWalk = _gim.Walking.Walk("FirstCameOnSceneGim").GetTrack();

      StartCoroutine(pepaWalk.CoroutineTarget);
      StartCoroutine(gimWalk.CoroutineTarget);

      yield return new WaitUntil(()=> pepaWalk.IsCoroutineFinished && gimWalk.IsCoroutineFinished);

      //О-о-о
			Debug.Log("О-о-о");
      yield return StartCoroutine(_gim.SoundBehavior.Talk("ji-7"));
			Debug.Log("Как ты думаешь, что там?");
      //Как ты думаешь, что там?
      yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-69"));
			yield return new WaitForSeconds(0.7f);
			Debug.Log("Смотри, на сундуке есть пустые места для каких-то предметов. Наверное это такой замок. Найдём эти предметы и установим на свои места.");
      //Смотри, на сундуке есть пустые места для каких-то предметов. Наверное это такой замок. Найдём эти предметы и установим на свои места.
      yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-70"));
			yield return new WaitForSeconds(0.5f);
			SceneLoader.SwitchToScene(Scenes._9Treasures_2FindHidenStuff);
    }
  }
}