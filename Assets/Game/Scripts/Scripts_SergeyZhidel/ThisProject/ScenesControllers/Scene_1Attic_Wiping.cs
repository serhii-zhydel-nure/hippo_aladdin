﻿using System.Collections;
using Cleaning;
using PSV;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using Shevchenko;

namespace Scripts_SergeyZhidel.ScenesManagment
{
    public class Scene_1Attic_Wiping : SceneController
    {
        private GameObject[] _spiderwebs;
        private Hero _pepa;
        private Camera _camera;
        private float _cameraStartSize;
        private Vector3 _cameraStartPos;

        private HelpByTimeIfUserDontDoRightAction _helpByTimeIfUserDontTakeRightStuff;
        private TalkPhraseHelp _phraseHelp;


        private float _persentsToClean;
        public Scene_1Attic_Wiping() : base(Scenes._1Attic_Wiping)
        {
        }


        public override void PrepareSceneToScenario()
        {
            base.PrepareSceneToScenario();

            _spiderwebs = this.Find("spiderweb").GetChildrenFirstLevel();

            _persentsToClean = GameSettings.Difficulty.Value == GameDifficulty.TwoYears ? 50 : 60; // 70 : 80

            _pepa = HeroesOnScene[HeroType.HippoPepa];
            _camera = Camera.main;
            _cameraStartSize = _camera.orthographicSize;
            _cameraStartPos = _camera.transform.position;

            _phraseHelp = _camera.gameObject.AddComponentWithInit<TalkPhraseHelp>(component => component.Speaker = _pepa.SoundBehavior);
            _helpByTimeIfUserDontTakeRightStuff = HelpByTimeIfUserDontDoRightAction.GetNewInstance();
            _helpByTimeIfUserDontTakeRightStuff.AddUserHelpers(_phraseHelp);
//			AudioController.PlayMusic("Loop 1 song");
//			if (!AudioController.IsSoundPlaying("Loop 1 song"))
//			{
			AudioController.Release();
				AudioController.PlaySound("Loop 1 song", StreamGroup.MUSIC, 0.3f, true);
//			}
        }

        public override IEnumerator RunScenario()
        {
            SpriteRenderer clampSprite = this.Find("CameraBordersTransparent").GetComponent<SpriteRenderer>();
            GameObject window = this.Find("window");
            SpriteRenderer dustOnWindow = window.FindChild("dustOnWindow").GetComponent<SpriteRenderer>();
            GameObject eraserPrefab = ResourcesFolder.GetPrefab("wisp");
            Vector3 eraserSize = new Vector3(0.3f, 0.3f, 0.3f);

            //Ой, как здесь грязно и некрасиво. Наведем на чердаке порядок
            yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-3"));

            //Вытрем пыль с окошка
            yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-4"));
            //yield return StartCoroutine(Camera.main.ClampedMoveToObjectAndScaleOverSeconds(window, clampSprite, 2, 2));
            CameraController.instance.SetTarget(window.transform).AllowMove(true).SetScale(2.02f).SetSpeedMove(4).SetSpeedScale(4);
            yield return new WaitWhile(() => CameraController.instance.IsCameraInMoveNow());

            string phraseToRepeat = "hp-5";//Проведи пальцем по окну
            yield return StartCoroutine(_pepa.SoundBehavior.Talk(phraseToRepeat));
            _phraseHelp.PhrasesToTalk.Add(phraseToRepeat);
            _helpByTimeIfUserDontTakeRightStuff.StartWork();
            CleanerController.CleanedPersents += p => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
			yield return StartCoroutine(CleanLauncher.CleanSprite(dustOnWindow, _persentsToClean, eraserPrefab, eraserSize, "Вытирает что-то"));
            _helpByTimeIfUserDontTakeRightStuff.Stop();
            _phraseHelp.PhrasesToTalk.Remove(phraseToRepeat);

            //yield return StartCoroutine(_camera.MoveAndScaleOverSeconds(_cameraStartPos, _cameraStartSize, 2f));
            CameraController.instance.SetTarget(_cameraStartPos).AllowMove(true)
                .SetScale(_cameraStartSize).SetSpeedMove(4).SetSpeedScale(4);
            yield return new WaitWhile(() => CameraController.instance.IsCameraInMoveNow());

            //Так держать!
            yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-6"));
            //Продолжаем уборку!
            yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-7"));

            //Давай уберем паутину
            yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-8"));

            //Проведи пальцем по паутине
            phraseToRepeat = "hp-9";
            yield return StartCoroutine(_pepa.SoundBehavior.Talk(phraseToRepeat));

            foreach (GameObject spiderweb in _spiderwebs)
            {
                //yield return StartCoroutine(Camera.main.ClampedMoveToObjectAndScaleOverSeconds(spiderweb, clampSprite, 2, 2));
                CameraController.instance.SetTarget(spiderweb.transform).AllowMove(true).SetScale(2.02f).SetSpeedMove(4).SetSpeedScale(4);
                yield return new WaitWhile(() => CameraController.instance.IsCameraInMoveNow());

                _phraseHelp.PhrasesToTalk.Add(phraseToRepeat);
                _helpByTimeIfUserDontTakeRightStuff.StartWork();
                CleanerController.CleanedPersents += p => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
				yield return StartCoroutine(CleanLauncher.CleanSprite(spiderweb.GetComponent<SpriteRenderer>(), _persentsToClean, eraserPrefab, eraserSize, "снятие паутины"));
                _helpByTimeIfUserDontTakeRightStuff.Stop();
                _phraseHelp.PhrasesToTalk.Remove(phraseToRepeat);
            }

            //yield return StartCoroutine(_camera.MoveAndScaleOverSeconds(_cameraStartPos, _cameraStartSize, 2f));
            CameraController.instance.SetTarget(_cameraStartPos).AllowMove(true)
                .SetScale(_cameraStartSize).SetSpeedMove(4).SetSpeedScale(4);
            yield return new WaitWhile(() => CameraController.instance.IsCameraInMoveNow());

            //Так держать!
            yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-6"));
            //Продолжаем уборку!
            yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-7"));

            LoadNextScene(Scenes._2Attic_Placement);
        }


    }
}