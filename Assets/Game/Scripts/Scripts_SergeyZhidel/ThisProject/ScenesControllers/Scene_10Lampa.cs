﻿using System;
using System.Collections;
using System.Diagnostics;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.InputManagement.Local;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using PSV;

namespace Scripts_SergeyZhidel.ScenesManagment
{
	class Scene_10Lampa:SceneController
	{
		private Hero _pepa;
		private Hero _gim;
		private Hero _mamaDjin;
		private Camera _camera;
		private float _cameraStartSize;
		private Vector3 _cameraStartPos;

		private Animator _tutorialFingerAnimator;

		private GameObject _lampa;
		private Image _progressBar;
		private GameObject _progressBarObject;

		private HelpByTimeIfUserDontDoRightAction _helpByTimeIfUserDontTakeRightStuff;
		private TalkPhraseHelp _phraseHelp;

		public GameObject particle;

		public Scene_10Lampa() : base( Scenes._10Lampa)
		{
		}


		public override void PrepareSceneToScenario()
		{
			base.PrepareSceneToScenario();
			_pepa = HeroesOnScene[HeroType.HippoPepa];
			_gim = HeroesOnScene[HeroType.HippoGeorge];
			_mamaDjin = HeroesOnScene[HeroType.HippoMama];

			_camera = Camera.main;
			_cameraStartSize = _camera.orthographicSize;
			_cameraStartPos = _camera.transform.position;
			_tutorialFingerAnimator = this.Find("Tutorial").GetComponent<Animator>();

			_lampa = this.Find("Lampa");
			_progressBar = this.Find("Progressbar").FindChild("ProgressbarObj").GetComponent<Image>();

			_phraseHelp = CommonScriptsContainer.Instance.AddComponentWithInit<TalkPhraseHelp>(component => component.Speaker = _pepa.SoundBehavior);
			_helpByTimeIfUserDontTakeRightStuff = HelpByTimeIfUserDontDoRightAction.GetNewInstance();
			_helpByTimeIfUserDontTakeRightStuff.AddUserHelpers(_phraseHelp);

			_helpByTimeIfUserDontTakeRightStuff.OnGiveUserHelp += () => _tutorialFingerAnimator.SetBool("Scrub", true);
			_helpByTimeIfUserDontTakeRightStuff.OnTakeHelpBackFromUser += () => _tutorialFingerAnimator.SetBool("Scrub", false);
			_progressBarObject = this.Find("Progressbar").gameObject;
			_progressBarObject.SetActive(false);
            particle = LampEffect.instance.gameObject;
//			AudioController.PlayMusic("Loop 11 song");
//			if (!AudioController.IsSoundPlaying("Loop 11 song"))
//			{
			AudioController.Release();
				AudioController.PlaySound("Loop 11 song",  StreamGroup.MUSIC, 0.3f, true);
//			}
 //			particle.SetActive(false);
		}


		public override IEnumerator RunScenario()
		{
			bool scrubbingFinished = false;
			yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-73"));//Мы нашли волшебную лампу!
			yield return new WaitForSeconds(0.5f);
			string pharaseHelp = "hp-74";//Потри лампу, чтобы вызвать джинна.
			yield return StartCoroutine(_pepa.SoundBehavior.Talk(pharaseHelp));
			_progressBarObject.SetActive(true);
			_phraseHelp.PhrasesToTalk.Add(pharaseHelp);
			_helpByTimeIfUserDontTakeRightStuff.StartWork();
//			WhileMovingInColliderTimeRecorder whileMovingInCollderTimeRecorder = _lampa.AddComponent<WhileMovingInColliderTimeRecorder>();
////      whileMovingInCollderTimeRecorder.PersentChanged += persent => _progressBar.fillAmount = (float)persent / 100;
//			whileMovingInCollderTimeRecorder.PersentChanged += persent => _progressBar.transform.localScale = new Vector3((float)persent / 100, 1, 1);
//			whileMovingInCollderTimeRecorder.PersentChanged += persent => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
//			whileMovingInCollderTimeRecorder.WaitingFinished += param => scrubbingFinished = true;
			isSmallGame = true;
			yield return new WaitUntil(() => !isSmallGame);
			_phraseHelp.PhrasesToTalk.Remove(pharaseHelp);
			_helpByTimeIfUserDontTakeRightStuff.Stop();
			yield return new WaitForSeconds(0.5f);
			_progressBarObject.SetActive(false);
//			particle.SetActive(true);
			holdTimer = 1;
			scaleDown = true;

            yield return new WaitForSeconds(1);
            LampEffect.instance.SetScale(0);

            yield return StartCoroutine(_lampa.FindChild("MamaDjin").transform.ScaleOverSeconds(Vector3.one, 2));
            yield return StartCoroutine(Shevchenko.StaticParams.MoveToCorrectPlace(_lampa.FindChild("MamaDjin").transform, GameObject.Find("GinPosFin").transform, 2));
			//О владыки лампы, я - великий джинн Абдалазиз - к вашим услугам! Чего желаете, о повелители?
			yield return StartCoroutine(_mamaDjin.SoundBehavior.Talk("mh-1"));
			Instantiate(ResourcesFolder.GetPrefab("2D_Bubble_Lamp"));
			yield return new WaitForSeconds(0.5f);
			//Мы хотим, мы хотим…
			yield return StartCoroutine(_pepa.SoundBehavior.Talk("hp-75"));
			yield return new WaitForSeconds(0.5f);
			LoadNextScene( Scenes.FinalCutScene);
		}


		Vector2 lastPos, delta;
		float holdTimer;
		bool isSmallGame;
		bool scaleDown;

		void Update()
		{
			if (isSmallGame)
			{
				if (Input.GetMouseButtonDown(0))
				{
					lastPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

				}
				if (Input.GetMouseButton(0))
				{

					delta = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition) - lastPos;
					lastPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

					if (delta.magnitude != 0)
					{

						Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
						RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
						if (hits != null)
						{
							foreach (var item in hits)
							{
								if (item.collider.name == "Lampa")
								{
									if (!AudioController.IsSoundPlaying("трут лампу"))
									{
										AudioController.PlaySound("трут лампу");
									}
									holdTimer += Time.deltaTime;
									LampEffect.instance.SetScale(holdTimer * 1f);
									if (holdTimer > 3)
									{
										isSmallGame = false;
										if (AudioController.IsSoundPlaying("трут лампу"))
										{
											AudioController.StopSound("трут лампу");
										}
										AudioController.PlaySound("дым из лампы");
//										lamp.SetActive(false);
//										PanelAlign.ShowPanel("Panel");
//										holdTimer = 0;
									}
									//								PanelAlign.ShowPanel("Panel");
								}
								else
								{
									if (AudioController.IsSoundPlaying("трут лампу"))
									{
										AudioController.StopSound("трут лампу");
									}
								}
							}
						}
						else
						{
							if (AudioController.IsSoundPlaying("трут лампу"))
							{
								AudioController.StopSound("трут лампу");
							}
						}
					}
				}
				else
				{
					if (AudioController.IsSoundPlaying("трут лампу"))
					{
						AudioController.StopSound("трут лампу");
					}
				}
				Debug.Log("holdTimer " + holdTimer);
				_progressBar.transform.localScale = new Vector3(holdTimer/3, 1, 1);
				//DirectionAngleInput();
				//ProgressBarBase.instance.SetProgress(progressBarValue);
			}
			if (scaleDown)
			{
				holdTimer -= Time.deltaTime;
                LampEffect.instance.LauchSecondSmoke();

                if (holdTimer < 0)
				{
					scaleDown = false;
					holdTimer = 0;
                    LampEffect.instance.StopSecondSmoke();
                }
            }
		}
	}
}
