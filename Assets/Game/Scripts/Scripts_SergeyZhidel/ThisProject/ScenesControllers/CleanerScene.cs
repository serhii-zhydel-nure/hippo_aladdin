﻿using System.Collections;
using Cleaning;
using PSV;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.ScenesManagment
{
  public class CleanerScene:SceneController
  {
    public CleanerScene() : base(Scenes.Cleaner)
    {
    }

    public override IEnumerator RunScenario()
    {
      GameObject cleanerObj = Instantiate(ResourcesFolder.GetPrefab("cleaner"));

      SpriteRenderer cleaningSprite =this.Find("Sprite").GetComponent<SpriteRenderer>();
      bool cleaningDone = false;
      cleanerObj.AddComponentWithInit<CleanerController>(script =>
      {
        script.EraserSize = new Vector3(0.6f, 0.6f, 0.6f);
        script.EraserPrefab = ResourcesFolder.GetPrefab("wisp");
        script.PersentsToDone = 80;
        script.SpriteToClean = cleaningSprite;
        script.CleaningDone += () => cleaningDone = true;
      });

      yield return new WaitUntil(() => cleaningDone);
      Destroy(cleanerObj);
      cleaningSprite.gameObject.SetActive(false);
    }
  }
}