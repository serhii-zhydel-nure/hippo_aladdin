﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  ////Общее для всех сцен (например фразы)
  /// </summary>
  public static class Common
  {

    public static List<String> RightPhrases = new List<string>
    {
      "hp-18",//Правильно!  
      "hp-19", //Отлично!      
      "hp-6"//Так держать!
    };

    public static List<String> WrongPhrases = new List<string> { };

  }
}