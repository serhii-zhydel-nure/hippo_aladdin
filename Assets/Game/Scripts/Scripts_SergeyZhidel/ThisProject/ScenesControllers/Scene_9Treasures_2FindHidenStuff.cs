﻿using System;
using System.Collections;
using System.Linq;
using DG.Tweening;
using PSV;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.InputManagement.Local;
using UnityEngine;

namespace Scripts_SergeyZhidel.ScenesManagment
{
  public class Scene_9Treasures_2FindHidenStuff : SceneController
  {
    private Camera _camera;
    private float _cameraStartSize;
    private Vector3 _cameraStartPos;

    private RightStuffPanel _panel;
    private TreasureBox[] _allTreasureBoxs;

    private int _rightStuffAdditionalOrded = 100;
    private int _wronfStuffAdditionalOrded = 50;

    private HelpByTimeIfUserDontDoRightAction _helpByTimeIfUserDontTakeRightStuff;
    private TalkPhraseHelp _phraseHelp;

    public Scene_9Treasures_2FindHidenStuff() : base(Scenes._9Treasures_2FindHidenStuff)
    {
    }

    public override void PrepareSceneToScenario()
    {
      base.PrepareSceneToScenario();
      _camera = Camera.main;
      _cameraStartSize = _camera.orthographicSize;
      _cameraStartPos = _camera.transform.position;

      _phraseHelp = _camera.gameObject.AddComponentWithInit<TalkPhraseHelp>(component => component.Speaker = Narrator.Instance);
      _helpByTimeIfUserDontTakeRightStuff = HelpByTimeIfUserDontDoRightAction.GetNewInstance();
      _helpByTimeIfUserDontTakeRightStuff.AddUserHelpers(_phraseHelp);

      _allTreasureBoxs = FindObjectsOfType<TreasureBox>();
      _panel =this.Find("Panel").GetComponent<RightStuffPanel>();
//			AudioController.PlayMusic("Loop 11 song");
//			if (!AudioController.IsSoundPlaying("Loop 11 song"))
//			{
			AudioController.Release();
				AudioController.PlaySound("Loop 11 song",  StreamGroup.MUSIC, 0.3f, true);
//			}
     }

    public override IEnumerator RunScenario()
    {
      //Жми на сундуки и горы золота, чтобы отыскать предметы
      string phraseNothingHappens = "hp-71";
      StartCoroutine(Narrator.Talk(phraseNothingHappens));
      _helpByTimeIfUserDontTakeRightStuff.StartWork();


      foreach (TreasureBox treasureBox in _allTreasureBoxs)
      {
        treasureBox.gameObject.AddComponent<SelectableBehaviour>().Selected +=
          param =>
          {
            param.Object.GetComponent<TreasureBox>().Open();
            _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
          };

        treasureBox.StuffRunAway += newStuff =>
        {
          newStuff.AddComponent<BoxCollider2D>();
          //newStuff.AddComponentIfNotExist<GroupSorter>().GroupAdditionalOrder = GetOrderForStuff(newStuff);
          if (newStuff.name.Contains("Right"))
          {
            GameObject dragTarget = _panel.DesiredRigthStuff.First(desire => newStuff.name.Contains(desire.name));//потому что инстанцированный обьект будет с частичкой "(Clone)"
            StartCoroutine(PlayDragGame(newStuff, dragTarget));
          }
          else
          {
            newStuff.AddComponent<SelectableBehaviour>().Selected += param => SelectedWrongStuff(param.Object);
          }
        };

      }
      _phraseHelp.PhrasesToTalk.Add(phraseNothingHappens);
      _helpByTimeIfUserDontTakeRightStuff.StartWork();

      //ждать пока количество нужных предметов не уровняется с количеством добавленых
      yield return new WaitUntil(() => _panel.AddedRightStuff.Count >= _panel.DesiredRigthStuff.Count());

      _helpByTimeIfUserDontTakeRightStuff.Stop();
      _phraseHelp.PhrasesToTalk.Remove(phraseNothingHappens);

			LoadNextScene(Scenes._9Treasures_3DragGame);
    }

    private IEnumerator PlayDragGame(GameObject draggableObj, GameObject target)
    {

      yield return StartCoroutine(DragStuff(draggableObj, target.AddComponentIfNotExist<BoxCollider2D>(),
        draggedObjRight => { OnObjDraggedSuccess(draggedObjRight, target); },
        draggedObjWrong => { }));


      //yield return StartCoroutine(_pepa.SoundBehavior.Talk(Common.RightPhrases.GetRandomElement()));
      yield return new WaitForSeconds(0.5f);//чтоб не так резко начиналась новая игра
    }

    private IEnumerator DragStuff(GameObject dragableObj, Collider2D target, Action<GameObject> finishSuccess, Action<GameObject> finishFail)
    {
      bool draggingFinished = false;

      DragGameController dragControll = new DragGameController(dragableObj, target);
      dragControll.MoveBackIfFinishedNotCorrect = false;

      dragControll.StartDragging += obj => obj.AddComponentIfNotExist<GroupSorter>().GroupAdditionalOrder += 1000;

      dragControll.FinishedSuccessfully += finishSuccess;
      dragControll.FinishedUnSuccessfully += finishFail;

      dragControll.FinishedSuccessfully += obj => StartCoroutine(Narrator.Talk(Common.RightPhrases.RandomOne()));

      //слои при перетаскивании
      dragControll.FinishedUnSuccessfully += obj => obj.GetComponent<GroupSorter>().GroupAdditionalOrder -= 1000;
      dragControll.FinishedSuccessfully += obj => obj.GetComponent<GroupSorter>().GroupAdditionalOrder -= 1000; ;

      //настройка подсказок
      dragControll.StartDragging += obj => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
      dragControll.FinishedSuccessfully += obj => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
      dragControll.FinishedUnSuccessfully += obj => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidWrongAction();

      //убрать физику при перетаскивании
      dragControll.StartDragging += obj => obj.GetComponent<Rigidbody2D>().isKinematic = true;
      dragControll.FinishedUnSuccessfully += obj => obj.GetComponent<Rigidbody2D>().isKinematic = false;

      dragControll.FinishedSuccessfully += obj => draggingFinished = true;

      yield return new WaitUntil(() => draggingFinished);

      _panel.AddedRightStuff.Add(dragableObj);
      Destroy(dragableObj.GetComponent<Rigidbody2D>());
      dragControll.Destroy();
    }

    private void OnObjDraggedSuccess(GameObject obj, GameObject target)
    {
      target.SetActive(false);
      obj.transform.DOShakeScale(0.5f, 0.15f, 5);
      obj.transform.DOMove(target.transform.position, 0.4f);
      obj.transform.DORotate(target.transform.rotation.eulerAngles, 0.4f)
        .OnComplete(() =>
        {
          obj.CreateChild(ResourcesFolder.GetPrefab("OkMark"));
          obj.GetComponent<GroupSorter>().UpdateLayer();
        });

    }

    private void SelectedWrongStuff(GameObject obj)
    {
      _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidWrongAction();
      obj.transform.DOScale(0, 0.4f).OnComplete(() => Destroy(obj, 0.2f));
    }

  }
}