﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cleaning;
using Scripts_SergeyZhidel;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.ScenesManagment;
using UnityEngine;
using PSV;

namespace Assets.Game.Scripts.Scripts_SergeyZhidel.Scenes
{
	class Scene_4Book : SceneController
	{
		private MegaBookBuilder _bookBuilder;

		private Camera _mainCamera;

		private HelpByTimeIfUserDontDoRightAction _helpByTimeIfUserDontTakeRightStuff;
		private TalkPhraseHelp _phraseHelp;

		private Hero _georgeTramp1;
		private Hero _georgeTramp2;
		private Hero _georgePrince;
		private Hero _papaTradesman1;
		private Hero _papaTradesman2;

		private float _persentsToClean;

		private GameObject cleaningDust;

		public Scene_4Book()
			: base(PSV .Scenes._4Book)
		{
		}

        public void Start()
        {
            PrepareSceneToScenario();
            StartCoroutine(RunScenario());
        }
        public override void PrepareSceneToScenario()
		{
			base.PrepareSceneToScenario();

			_bookBuilder = FindObjectOfType<MegaBookBuilder>();

			_mainCamera = this.Find("MainCamera").GetComponent<Camera>();

			_persentsToClean = GameSettings.Difficulty.Value == GameDifficulty.TwoYears ? 80 : 90;

			_phraseHelp = _mainCamera.gameObject.AddComponentWithInit<TalkPhraseHelp>(component => component.Speaker = Narrator.Instance);
			_helpByTimeIfUserDontTakeRightStuff = HelpByTimeIfUserDontDoRightAction.GetNewInstance();
			_helpByTimeIfUserDontTakeRightStuff.AddUserHelpers(_phraseHelp);

			_georgeTramp1 = this.Find("George_tramp1").GetComponent<Hero>();
			_georgeTramp2 = this.Find("George_tramp2").GetComponent<Hero>();
			_georgePrince = this.Find("George_prince").GetComponent<Hero>();
			_papaTradesman1 = this.Find("Hippo_papa_tradesman1").GetComponent<Hero>();
			_papaTradesman2 = this.Find("Hippo_papa_tradesman2").GetComponent<Hero>();
			cleaningDust = GameObject.Find("Scene4Smoke");
			cleaningDust.SetActive(false);
			cleaning = false;
//			AudioController.PlayMusic("Loop 1 song");
//			if (!AudioController.IsSoundPlaying("Loop 1 song"))
//			{
			AudioController.Release();
				AudioController.PlaySound("Loop 15 song",  StreamGroup.MUSIC, 0.3f, true);
//			}
		}

		bool cleaning;
		public override IEnumerator RunScenario()
		{
			//обложка
			//yield return StartCoroutine(_mainCamera.MoveAndScaleOverSeconds(this.Find("page2Pos"), 0.45f, 1f));
			//yield return StartCoroutine(CleanPage(this.Find("FrontCover").GetComponent<SpriteRenderer>()));
			yield return new WaitForSeconds(0.3f);
			StartCoroutine(TurnPage().Append(() =>
					{
						this.Find("PagesDust").FindChild("page1").SetActive(true);
						this.Find("Page_1").FindChild("Dust").SetActive(false);
					}));
			yield return StartCoroutine(_mainCamera.MoveAndScaleOverSeconds(this.Find("CameraBookCenter"), 0.75f, 3f));

			//страница 1

			yield return StartCoroutine(CleanPage(this.Find("PagesDust").FindChild("page1").GetComponent<SpriteRenderer>(), "Effects/Scene4Smoke"));
			//В одном персидском городе жил когда-то бедный юноша. Звали его Алладин
			this.StartWithDelay(1.5f, _mainCamera.MoveAndScaleOverSeconds(this.Find("page2Pos"), 0.5f, 2f));
			yield return StartCoroutine(Narrator.Instance.Talk("hp-27"));
			yield return StartCoroutine(_georgeTramp1.Animator.PlayFullAnimation("JoyJumping"));

			StartCoroutine(TurnPage().Append(() =>
					{
						this.Find("PagesDust").FindChild("page2").SetActive(true);
						this.Find("Page_2").FindChild("Dust").SetActive(false);
					}));
			yield return StartCoroutine(_mainCamera.MoveAndScaleOverSeconds(this.Find("CameraBookCenter"), 0.75f, 2f));


			//страница 2
			yield return StartCoroutine(_mainCamera.MoveAndScaleOverSeconds(this.Find("page1Pos"), 0.55f, 2f));
			yield return StartCoroutine(CleanPage(this.Find("PagesDust").FindChild("page2").GetComponent<SpriteRenderer>(), "Effects/Scene4Smoke"));

			//Однажды к нему обратился богатый купец
			yield return StartCoroutine(Narrator.Instance.Talk("hp-28"));
			yield return StartCoroutine(_papaTradesman1.Walking.Walk("papaPos1"));
			//Знай, о дитя мое, что твой отец - мой брат. Я пришел озолотить тебя в память о брате моем
			yield return StartCoroutine(_papaTradesman1.SoundBehavior.Talk("hp-29"));
			_georgeTramp2.CharacterAnimations.ScareType = ScareTypes.Panic;
			_georgeTramp2.CharacterAnimations.Scared = true;
			StartCoroutine(_georgeTramp2.Walking.WalkAllNonStop(new[]
					{
						"georgePos1", "georgePos2",
						"georgePos1", "georgePos2",
						"georgePos1", "georgePos2",
						"georgePos1", "georgePos2",
					}));
			yield return new WaitForSeconds(1.5f);

			//страница 3
			this.Find("PagesDust").FindChild("page3").SetActive(true);
			this.Find("Page_3").FindChild("Dust").SetActive(false);
			StartCoroutine(_mainCamera.MoveAndScaleOverSeconds(this.Find("page2Pos"), 0.55f, 2f));
			yield return StartCoroutine(CleanPage(this.Find("PagesDust").FindChild("page3").GetComponent<SpriteRenderer>(), "Effects/Scene4Smoke"));

			//На следующий день купец повел Алладина на базар и купил ему красивую одежду
			yield return StartCoroutine(Narrator.Instance.Talk("hp-30"));
			GameObject bubles = _mainCamera.gameObject.CreateChild(ResourcesFolder.GetPrefab("2D_Bubble_01"));
			bubles.transform.localPosition = new Vector3(0, -1.55f, 3.74f);
			yield return StartCoroutine(_georgePrince.Animator.PlayFullAnimation("JoyJumping"));

			yield return new WaitForSeconds(1f);
			MapProgressController.SetSceneCompleted();
			LoadNextScene(PSV .Scenes.Map);

			yield break;
		}

		private IEnumerator TurnPage()
		{
			AudioController.PlaySound("Открывает книгу");
			_bookBuilder.NextPage();
			yield return new WaitUntil(() => _bookBuilder.page == _bookBuilder.Flip);
		}

		private IEnumerator CleanPage(SpriteRenderer spriteToClean, string cleanerPrefab = "Prefabs/wisp")
		{
//			GameObject eraserPrefab = ResourcesFolder.GetPrefab(cleanerPrefab);
//			GameObject eraserPrefab = Resources.Load<GameObject>(cleanerPrefab);
			cleaning = true;
			GameObject eraserPrefab = null;
			Vector3 eraserSize = new Vector3(0.3f, 0.3f, 0.3f);

			string phraseToRepeat = "hp-26";//Проведи пальцем по странице
			yield return StartCoroutine(Narrator.Instance.Talk(phraseToRepeat));
			_phraseHelp.PhrasesToTalk.Add(phraseToRepeat);
			_helpByTimeIfUserDontTakeRightStuff.StartWork();
			MaskCamera.scale = 4;
			CleanerController.CleanedPersents += p => _helpByTimeIfUserDontTakeRightStuff.NotifyUserDidRightAction();
			//Дым когда протираешь книгу
			yield return StartCoroutine(CleanLauncher.CleanSprite(spriteToClean, _persentsToClean, eraserPrefab, eraserSize, "Открывает книгу"));
			_helpByTimeIfUserDontTakeRightStuff.Stop();
			_phraseHelp.PhrasesToTalk.Remove(phraseToRepeat);

			//Так держать!
			cleaning = false;
			cleaningDust.GetComponent<ParticleSystem>().Stop();
//			cleaningDust.SetActive(false);
//			yield return StartCoroutine(Narrator.Instance.Talk("hp-6"));

		}

		void Update()
		{
			if (cleaning)
			{
				if (Input.GetMouseButtonDown(0))
				{
					cleaningDust.SetActive(true);
					cleaningDust.GetComponent<ParticleSystem>().Play();
				}
				if (Input.GetMouseButton(0))
				{
					cleaningDust.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 4);
				}
				if (Input.GetMouseButtonUp(0))
				{
					cleaningDust.GetComponent<ParticleSystem>().Stop();
				}
			}
		}
	}
}
