﻿#if PSV_PROTOTYPE
using Scripts_SergeyZhidel;
using Language = Languages.Language;
#endif


#if PSV_PROTOTYPE
public enum GameDifficulty
{
  TwoYears = 2,
  FiveYears = 5
}
#else
namespace Scripts_SergeyZhidel
{
   public enum Scenes  
	{
		EmptySceneMode,
		MainMenu,
  }
}
#endif

public static partial class GameSettings
{
  public static PrefsEnum<Language> CurrentLanguage = new PrefsEnum<Language>("CurrentLanguage",
  Localizator.DefineLanguageForThisDevice());

#if PSV_PROTOTYPE
  public static PrefsEnum<GameDifficulty> Difficulty = new PrefsEnum<GameDifficulty>("Difficulty",
    GameDifficulty.TwoYears);
#endif

  public static PrefsBool IsMusic = new PrefsBool("music_on", true);
  public static PrefsFloat MusicVol = new PrefsFloat("music_vol", 0.3f);

  public static PrefsBool IsSound = new PrefsBool("sounds_on", true);
  public static PrefsFloat SoundsVol = new PrefsFloat("sounds_vol", 1f);

  public static PrefsBool IsVibro = new PrefsBool("vibro_on", true);
}
