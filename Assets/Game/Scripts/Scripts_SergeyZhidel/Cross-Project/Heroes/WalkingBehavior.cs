﻿using System;
using System.Collections;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class WalkingBehavior : MonoBehaviour
  {
    public Action<bool> PlayWalkingAnimation;
    public Action<float> SetWalkSpeedParameter;

    [SerializeField]
    private float _walkSpeed = 5;

    /// <summary>
    /// Поумолчанию: "true"
    /// </summary>
    public bool AllowAutoSetOpinionDirectionOnFinishMove
    {
      set
      {
        if (value)
          FinishedWalk += SetOpinionDirectionAuto;
        else
          ActionFunkExtender.UnsubscribeSafe(ref FinishedWalk, SetOpinionDirectionAuto);
      }
    }


    public float WalkSpeed
    {
      get { return _walkSpeed; }
      set
      {
        _walkSpeed = value;
        SetWalkSpeedParameter.InvokeSafe(value);
      }
    }

    public bool IsWalking { get; private set; }

    public event Action<Vector3> StartedWalk;
    public event Action FinishedWalk;

    private string _status
    {
      get { return Status; }
      set
      {
        Status = value;
      }
    }

    public string Status = "Stay";

    private void Awake()
    {
      IsWalking = false;
      StartedWalk += OnStartMove;
      FinishedWalk += OnFinishMove;
      AllowAutoSetOpinionDirectionOnFinishMove = true;
    }

    private void OnStartMove(Vector3 direction)
    {
      PlayWalkingAnimation.InvokeSafe(true);
      if (direction.magnitude == 1)
        //direction is directionVector
        SetOpinionDirection(Vector3.zero.GetAxisDirection(direction, Axis3D.X));
      else
        //direction is distanationPoint
        SetOpinionDirection(direction);
    }

    public void OnFinishMove()
    {
      _status = "Stay";
      PlayWalkingAnimation.InvokeSafe(false);
    }

    private void SetOpinionDirectionAuto()
    {
      if (transform.position.x > Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, 0, 0)).x)//center of screeen
        SetOpinionDirection(Direction.Left);
      else
        SetOpinionDirection(Direction.Right);
    }

    public void SetOpinionDirection(Vector3 targetPosition)
    {
//      Debug.Log(String.Format("<color=blue>transform.position.x: {0}</color>", transform.position.x));
//      Debug.Log(String.Format("<color=blue>targetPosition.x: {0}</color>", targetPosition.x));
      if (transform.position.x > targetPosition.x)
        SetOpinionDirection(Direction.Left);
      else
        SetOpinionDirection(Direction.Right);

    }

    public void SetOpinionDirection(Direction direction)
    {
      if (direction == Direction.Left)
        transform.rotation = Quaternion.Euler(0, 180, 0);
      else
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    #region WalkCode

    private IEnumerator WalkToDestination(Vector2 destination)
    {
      _status = String.Format("WalkToDestination: {0}", destination);

      if (transform.position.ToVector2() == destination)
        yield break;

      IsWalking = true;
      StartedWalk.InvokeSafe(destination);

      yield return transform.MoveWithSpeed(destination, WalkSpeed);

      IsWalking = false;
      FinishedWalk.InvokeSafe();
    }


    public IEnumerator Walk(Vector2 targetPos)
    {
      StopAllCoroutines();
      _status = String.Format("Walk: {0}", targetPos);
      yield return WalkToDestination(targetPos);
    }

    public IEnumerator Walk(string distanationObjName)
    {
      StopAllCoroutines();
      _status = String.Format("Walk: {0}", distanationObjName);
      yield return WalkToDestination(GameObject.Find(distanationObjName).transform.position);
    }

    public IEnumerator WalkAll(string[] distanationObjsName)
    {
      StopAllCoroutines();
      _status = String.Format("WalkAll: {0}", distanationObjsName.ToStringAll());

      foreach (string distanationObjName in distanationObjsName)
      {
        yield return Walk(distanationObjName);
      }
    }

    public IEnumerator WalkAllNonStop(params string[] distanationObjsName)
    {
      StopAllCoroutines();
      IsWalking = true;
      _status = String.Format("WalkAllNonStop: {0}", distanationObjsName.ToStringAll());

      foreach (string distanationObjName in distanationObjsName)
      {
        Vector2 destination = GameObject.Find(distanationObjName).transform.position.ToVector2();

        if (transform.position.ToVector2() == destination)
          continue;

        StartedWalk.Invoke(destination);

        yield return transform.MoveWithSpeed(destination, WalkSpeed);
      }

      FinishedWalk.InvokeSafe();
      IsWalking = false;
    }

    public IEnumerator WalkToObjByAxisWithSpeed(GameObject objToWalk, Axis3D axis, float intend = 0)
    {
      _status = String.Format("WalkToObjByAxisWithSpeed. obj: {0} axis:{1}", objToWalk.name, axis);

      IsWalking = true;

      StartedWalk.Invoke(gameObject.transform.position.GetDirectionVectorToPoint(objToWalk).normalized);

      yield return gameObject.transform.MoveToObjByAxisWithSpeed(objToWalk, axis, WalkSpeed, intend);

      FinishedWalk.InvokeSafe();
      IsWalking = false;
    }


    public IEnumerator WalkOnDirectionWhile(Vector3 direction, Func<bool> condition)
    {
      _status = "WalkOnDirectionWhile " + direction;

      direction.Normalize();

      IsWalking = true;
      StartedWalk.Invoke(direction);

      while (condition.Invoke())
      {
        transform.position += direction * WalkSpeed * Time.deltaTime;
        yield return null;//то же самое что и Update()
      }

      FinishedWalk.InvokeSafe();
      IsWalking = false;
    }

    public void StopWalking()
    {
      StopAllCoroutines();
      IsWalking = false;
      FinishedWalk.InvokeSafe();
    }

    public void StopMovingButNotAnimation()
    {
      StopAllCoroutines();
    }

    #endregion
  }
}