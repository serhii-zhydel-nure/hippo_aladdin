﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  [RequireComponent(typeof(Animator))]
  public partial class CharacterAnimationParameters : MonoBehaviour
  {
    [SerializeField]
    private Animator _animator;

    public bool Walking
    {
      get { return _animator.GetBool("Walking"); }
      set { _animator.SetBool("Walking", value); }
    }

    public bool Final
    {
      get { return _animator.GetBool("Final"); }
      set { _animator.SetBool("Final", value); }
    }

    public bool Flying
    {
      get { return _animator.GetBool("Flying"); }
      set { _animator.SetBool("Flying", value); }
    }

    public bool MakeFlip
    {
      get { return _animator.GetBool("MakeFlip"); }
      set { _animator.SetBool("MakeFlip", value); }
    }

    public bool Laughing
    {
      get { return _animator.GetBool("Laughing"); }
      set { _animator.SetBool("Laughing", value); }
    }

    public bool Sad
    {
      get { return _animator.GetBool("Sad"); }
      set { _animator.SetBool("Sad", value); }
    }

    public bool Joing
    {
      get { return _animator.GetBool("Joing"); }
      set { _animator.SetBool("Joing", value); }
    }

    public bool Sitting
    {
      get { return _animator.GetBool("Sitting"); }
      set { _animator.SetBool("Sitting", value); }
    }

    public bool Scared
    {
      get { return _animator.GetBool("Scared"); }
      set { _animator.SetBool("Scared", value); }
    }

    public bool Talking
    {
      get { return _animator.GetBool("Talking"); }
      set { _animator.SetBool("Talking", value); }
    }


    public IdleTypes IdleType
    {
      get { return (IdleTypes)Enum.Parse(typeof(IdleTypes), _animator.GetInteger("IdleType").ToString()); }
      set { _animator.SetInteger("IdleType", (int)value); }
    }

    public HideTypes Hidetype
    {
      get { return (HideTypes)Enum.Parse(typeof(HideTypes), _animator.GetInteger("HideType").ToString()); }
      set { _animator.SetInteger("HideType", (int)value); }
    }

    public WalkingTypes WalkingType
    {
      get { return (WalkingTypes)_animator.GetInteger("WalkType"); }
      set { _animator.SetInteger("WalkType", (int)value); }
    }

    public FlyingTypes FlyState
    {
      get { return (FlyingTypes)_animator.GetInteger("FlyState"); }
      set { _animator.SetInteger("FlyState", (int)value); }
    }

    public FlipTypes FlipType
    {
      get { return (FlipTypes)_animator.GetInteger("FlipType"); }
      set { _animator.SetInteger("FlipType", (int)value); }
    }

    public LaughingTypes LaughType
    {
      get { return (LaughingTypes)_animator.GetInteger("LaughType"); }
      set { _animator.SetInteger("LaughType", (int)value); }
    }

    public SadTypes SadType
    {
      get { return (SadTypes)_animator.GetInteger("SadType"); }
      set { _animator.SetInteger("SadType", (int)value); }
    }

    public ScareTypes ScareType
    {
      get { return (ScareTypes)_animator.GetInteger("ScareType"); }
      set { _animator.SetInteger("ScareType", (int)value); }
    }

    public SittingTypes SitType
    {
      get { return (SittingTypes)_animator.GetInteger("SitType"); }
      set { _animator.SetInteger("SitType", (int)value); }
    }

    public float WalkSpeed
    {
      get { return _animator.GetFloat("WalkSpeed"); }
      set { _animator.SetFloat("WalkSpeed", value); }
    }

    private void Awake()
    {
      _animator = GetComponent<Animator>();

      if (_animator == null)
        _animator = GetComponentInChildren<Animator>();
    }

  }


  public enum IdleTypes
  {
    Idle0 = 0,
    Breath = 1,
    InBoxLegsUp = 2,
    StayWithStuffInBackHand = 3,
  }


  public enum WalkingTypes
  {
    Walk = 0,
    RunJumping = 1,
    WalkWithCart = 2,
    RunWithStuffInBackHand = 3,
  }

  public enum FlyingTypes
  {
    StartJump = 0,
    FlyUp = 1,
    TopPeak = 2,
    FlyDown = 3
  }

  public enum FlipTypes
  {
    Bump = 0,
    Swallow = 1,
    Grimase = 2,
    Flip = 3,
    Rastopyrka = 4,
    Superman = 5,
    Dudka = 6,
    Clouds = 7,
  }

  public enum FinalTypes
  {
    PepaFinal = 100,
    GeorgeFinal = 200,
    PapaFinal = 300,
    PapaFinal1 = 301,
    MamaFinal = 400
  }

  public enum LaughingTypes
  {
    Laugh0 = 0
  }

  public enum HideTypes
  {
    Hide0 = 0,
    Hide1 = 1,
    Hide2 = 2
  }

  public enum SadTypes
  {
    Sad0 = 0
  }

  public enum ScareTypes
  {
    Panic = 0,
    VeryScared = 1
  }

  public enum SittingTypes
  {
    Sit = 0
  }
}