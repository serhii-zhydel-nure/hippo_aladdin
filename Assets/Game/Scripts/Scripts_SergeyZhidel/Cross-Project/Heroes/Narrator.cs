﻿using System.Collections;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts_SergeyZhidel
{
  public class Narrator
  {
    public static SoundBehavior Instance
    {
      get
      {
        if (_instance == null)
        {
          _instance = GameManager.Instance.gameObject.CreateChild("Narrator").AddComponent<SoundBehavior>();
          SceneManager.sceneLoaded += (scene, mode) =>
          {
            Debug.Log("<color=blue>Narrator StopTalking</color>");
            StopTalking();
          };
        }
        return _instance;
      }
    }

    private static SoundBehavior _instance = null;


    public static bool IsTalking { get { return Instance.IsTalking; } }


    public static IEnumerator Talk(string clipName)
    {
      yield return Instance.TalkWithNoAnimation(clipName);
    }

    public static IEnumerator Talk(params string[] clipsNames)
    {
      yield return Instance.Talk(clipsNames);
    }

    public static void StopTalking()
    {
      Instance.StopTalking();
    }
  }
}