﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts_SergeyZhidel.Debuging;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class SoundBehavior : MonoBehaviour
  {
    public bool IsTalking { get { return Source.isPlaying; } }

    public Action<bool> PlayTalkingAnimation;

    public AudioSource Source;

    void Awake()
    {
      Source = gameObject.AddComponentIfNotExist<AudioSource>();
      Source.volume = GameSettings.SoundsVol.Value;
    }

    void OnEnable()
    {
#if UNITY_EDITOR || DEBUG
      SpeachSkipper.SpeachSourses.Add(this);
#endif
      GameSettings.SoundsVol.OnValueChanged += ChangeVolume;
    }

    void ChangeVolume(float newVolume)
    {
      Source.volume = newVolume;
    }

    void OnDisable()
    {
      GameSettings.SoundsVol.OnValueChanged -= ChangeVolume;
#if UNITY_EDITOR || DEBUG
      SpeachSkipper.SpeachSourses.Remove(this);
#endif
    }

    public IEnumerator Talk(string clipName)
    {
      PlayTalkingAnimation.InvokeSafe(true);
      yield return Source.PlayClipFull(ResourcesFolder.GetSpeech(clipName));
      PlayTalkingAnimation.InvokeSafe(false);
    }

    public IEnumerator TalkWithNoAnimation(string clipName)
    {
      yield return Source.PlayClipFull(ResourcesFolder.GetSpeech(clipName));
    }

    public IEnumerator Talk(params string[] clipsNames)
    {
      foreach (string clipName in clipsNames)
      {
        yield return Talk(clipName);
      }
    }

    public void StopTalking()
    {
      StopAllCoroutines();
      Source.Stop();
      PlayTalkingAnimation.InvokeSafe(false);
    }
  }
}