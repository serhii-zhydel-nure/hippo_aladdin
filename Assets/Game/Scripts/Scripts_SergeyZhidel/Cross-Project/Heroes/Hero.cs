﻿using System;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.InputManagement.Local;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class Hero : MonoBehaviour
  {
    [NonSerialized]
    public Animator Animator;
    public CharacterAnimationParameters CharacterAnimations { get; protected set; }

    [NonSerialized]
    public AudioSource AudioSource;
    [NonSerialized]
    public SoundBehavior SoundBehavior;

    [NonSerialized]
    public WalkingBehavior Walking;

    //set form outside
    public HeroType HeroType;

    protected virtual void Awake()
    {
      Animator = GetComponentInChildren<Animator>();
      CharacterAnimations = transform.GetChild(0).gameObject.AddComponent<CharacterAnimationParameters>();

      AudioSource = gameObject.AddComponentIfNotExist<AudioSource>();
      SoundBehavior = gameObject.AddComponentIfNotExist<SoundBehavior>();
      SoundBehavior.PlayTalkingAnimation += play => CharacterAnimations.Talking = play;

      Walking = gameObject.AddComponentIfNotExist<WalkingBehavior>();
      Walking.PlayWalkingAnimation += play => CharacterAnimations.Walking = play;
      Walking.SetWalkSpeedParameter += speedParam => CharacterAnimations.WalkSpeed = speedParam;

      Animator.PlayAnimationFromRandomTime("Idle0(IdleType==0)");
    }

    /// <summary>
    //// прикрепить чтото, например положить в руку
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="childObjName"></param>
    /// <param name="rotation"></param>
    /// <param name="position"></param>
    public void PutOnSubject(GameObject subject, string childObjName, Quaternion rotation, Vector3 position)
    {
      subject.transform.SetParent(gameObject.FindChild(childObjName).transform);
      subject.transform.localRotation = rotation;
      subject.transform.localPosition = position;
    }

    public bool CanMakeFlip()
    {
      return CharacterAnimations.Flying && !CharacterAnimations.MakeFlip &&
             (Animator.IsPlayingAnimation("FlyUp(FlyState==1)") ||
              Animator.IsPlayingAnimation(@"top\peak(FlyState==2)") ||
              Animator.IsPlayingAnimation("FlyDown(FlyState==3)") ||
              Animator.IsPlayingAnimation("FlyScared"));
    }

    public virtual SelectableBehaviour AddSelectableBehaviour()
    {
      return gameObject.AddComponentIfNotExist<SelectableBehaviour>();
    }

    public override string ToString()
    {
      return HeroType.ToString();
    }
  }



  public enum HeroType
  {
    HippoPepa,
    HippoGeorge,
    HippoMama,
    HippoPapa,
    DogSon,
    PigSon,
    GiraffeMama,
    GiraffeDaughter,
    GiraffeSon,
    RacoonSon,
    RacoonPapa,
    Dragon,
    Other,
  }
}