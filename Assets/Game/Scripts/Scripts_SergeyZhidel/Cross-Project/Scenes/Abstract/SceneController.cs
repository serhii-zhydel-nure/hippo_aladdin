﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
#if PSV_PROTOTYPE
using PSV;
#endif
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.ScenesManagment
{
  /// <summary>
  /// Паттерн State
  /// </summary>
  public abstract class SceneController : MonoBehaviour
  {
    public readonly Scenes NameOfScene;

#if PSV_PROTOTYPE
    public Dictionary<HeroType, Hero> HeroesOnScene;
#endif

    protected SceneController(Scenes scene)
    {
      NameOfScene = scene;
    }

    #region StateMethods 
    /// <summary>
    /// After OnLevelWasLoaded, before PrepareSceneToScenario, only for first launch of this scene
    /// То, что делается только когда сцена загружается первый раз в этой сессии (не вообще на телефоне)
    /// </summary>
    public virtual void OnFirstLaunch()
    {

    }

    /// <summary>
    /// Initialization (Configuration) of scene
    /// Для инициализации ссылок
    /// </summary>
    public virtual void PrepareSceneToScenario()
    {
#if PSV_PROTOTYPE
      //update heroes from this scene
      HeroesOnScene = new Dictionary<HeroType, Hero>();
      foreach (Hero hero in FindObjectsOfType<Hero>())
        if (hero.HeroType != HeroType.Other)
        {
          if (HeroesOnScene.Keys.Contains(hero.HeroType))
          {
            Debug.Log(String.Format("<color=blue>This hero already in the scene: {0}</color>", hero.HeroType), hero.gameObject);
            Debug.Log(String.Format("<color=blue>In Dictionary: {0}</color>", HeroesOnScene[hero.HeroType]), HeroesOnScene[hero.HeroType].gameObject);
          }
          else
            HeroesOnScene.Add(hero.HeroType, hero);
        }
#endif
    }

    /// <summary>
    /// After scene loaded
    /// Сам сценарий того, что будет происходить на сцене
    /// </summary>
    public abstract IEnumerator RunScenario();


    /// <summary>
    /// Before next scene loading
    /// То, что обязательно доделать по выходу со сцены.
    /// Освобождение ресурсов, осписка.
    /// </summary>
    public virtual void ReleaseScene()
    {
      StopAllCoroutines();
    }
    #endregion

    protected virtual void LoadNextScene(Scenes nextScene = Scenes.EmptySceneMode)
    {
      if (nextScene == Scenes.EmptySceneMode)
      {
        var message = Camera.main.gameObject.CreateChild(ResourcesFolder.GetPrefab("EndSceneMessage"));
        message.transform.position = message.transform.position.SetValueOfAxis(1, Axis3D.Z);
      }
      else
      {
        ScenesFSM.LoadScene(nextScene);
      }
    }
  }
}