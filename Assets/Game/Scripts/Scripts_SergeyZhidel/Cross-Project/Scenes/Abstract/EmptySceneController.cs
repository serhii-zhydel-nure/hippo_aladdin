﻿using System.Collections;
#if PSV_PROTOTYPE
using PSV;
#endif

namespace Scripts_SergeyZhidel.ScenesManagment
{
  public class EmptySceneController : SceneController
  {
    public EmptySceneController() : base(Scenes.EmptySceneMode) { }

    public override IEnumerator RunScenario()
    {
      yield break;
    }
  }
}