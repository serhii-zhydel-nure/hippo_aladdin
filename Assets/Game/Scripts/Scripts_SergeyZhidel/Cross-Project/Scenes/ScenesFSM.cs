﻿using System;
using System.Collections.Generic;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using UnityEngine.SceneManagement;
#if PSV_PROTOTYPE
using PSV;
#endif

namespace Scripts_SergeyZhidel.ScenesManagment
{
  /// <summary>
  /// Порядок загрузки\выгрузки сцен. Переключение сцен.
  /// Паттерн состояние (State).
  /// </summary>
  public class ScenesFSM : MonoBehaviour
  {
    private static ScenesFSM _instance;

    public static event Action<Scenes> TransitToScene;
    public static event Action<Scenes> BeforeRunScenario;

    //need for returning on previous scene, for store scenesController instances
    public static readonly Dictionary<Scenes, SceneController> ScenesControllers =
      new Dictionary<Scenes, SceneController>();

    public static SceneController CurrentSceneController;

    private static List<SceneController> _firstTimeLoadedScenes = new List<SceneController>();

    //starts with the beginning of the game
    static ScenesFSM()
    {
      var scenesContainer = GameManager.Instance.gameObject.CreateChild("ScenesControllers");

      _instance = scenesContainer.AddComponent<ScenesFSM>();

      //create all children of NotEmptySceneController
      foreach (Type gameScene in Reflection.GetInheritorsTypes<SceneController>())
      {
        var sceneController = (scenesContainer.AddComponent(gameScene) as SceneController);
        ScenesControllers.Add(sceneController.NameOfScene, sceneController);
      }
    }

    //для перехода на другую сцену
    public static void LoadScene(Scenes sceneName)
    {
      Debug.Log(String.Format("<color=green>Load scene: \"{0}\"</color>", sceneName));
      CurrentSceneController.ReleaseScene();

      TransitToScene.InvokeSafe(sceneName);

#if PSV_PROTOTYPE
      //if (SceneLoader.Instance == null)
        _instance.StartCoroutine(ScreenDarker.FadeScreenCor(1f)
        .Append(() => SceneManager.LoadScene(sceneName.ToString())));
      //else
      //  SceneLoader.SwitchToScene(sceneName, SceneLoader.TransitionMethod.Tween);
#else
       _instance.StartCoroutine(ScreenDarker.FadeScreenCor(1f)
        .Append(() => SceneManager.LoadScene(sceneName.ToString())));
#endif
    }


    /// <summary>
    /// Служебный, для уведомления SceneFSM что уровень загрузился
    /// </summary>
    public static void LevelWasLoaded(string sceneName)
    {
      Debug.Log(String.Format("<color=green>Trying to capture managing of the loading scene: \"{0}\"</color>", sceneName));

      Scenes loadedScene = Scenes.EmptySceneMode;
      if (sceneName.TryParceToEnum(ref loadedScene) && ScenesControllers.ContainsKey(loadedScene))
      {
        Debug.Log("<color=green>Successful. Continue scene's scenario.</color>");
        CurrentSceneController = ScenesControllers.TryToGetValueUnChecked(loadedScene);
      }
      else
      {
        Debug.Log(String.Format("<color=blue>No SceneController for Scene: \"{0}\"</color>", sceneName));
        CurrentSceneController = ScenesControllers.TryToGetValueUnChecked(Scenes.EmptySceneMode);
      }

      if (!_firstTimeLoadedScenes.Contains(CurrentSceneController))
      {
        CurrentSceneController.OnFirstLaunch();
        _firstTimeLoadedScenes.Add(CurrentSceneController);
      }

      CurrentSceneController.PrepareSceneToScenario();

      BeforeRunScenario.InvokeSafe(CurrentSceneController.NameOfScene);
      _instance.StartCoroutine(ScreenDarker.RiseScreenCor(1f));

      Debug.Log(String.Format("<color=green>Run Scenario: \"{0}\"</color>", CurrentSceneController.NameOfScene));
      CurrentSceneController.StartCoroutine(CurrentSceneController.RunScenario());
    }

    public static void ForceStopScene()
    {
      CurrentSceneController.ReleaseScene();
    }
  }
}