﻿using UnityEngine;

namespace Scripts_SergeyZhidel.Debuging
{
  class PlayerPrefsCleaner:MonoBehaviour
  {
    void OnGUI()
    {
      if (GUI.Button(
        new Rect(
        Screen.width * 0.01f, 
        Screen.height * 0.9f,
        Screen.width * 0.07f, 
        Screen.height * 0.05f), 
        "ClearPlPr"))
      {
        PlayerPrefs.DeleteAll();
      }
    }
  }
}
