﻿using System;
#if PSV_PROTOTYPE
using PSV;
#endif
using Scripts_SergeyZhidel.ScenesManagment;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts_SergeyZhidel.Debuging
{
  public class SceneButtonsShower : MonoBehaviour
  {
    private bool _showButtons;

    void OnGUI()
    {
      if (GUI.Button(
         new Rect(
        Screen.width * 0.75f,
        Screen.height * 0.01f,
        Screen.width * 0.07f,
        Screen.height * 0.05f),
         @"S\H ScenesControllers"))
      {
        _showButtons = !_showButtons;
      }

      if (_showButtons)
      {
        Scenes[] scenesControllers = ((Scenes[])Enum.GetValues(typeof(Scenes)));

        float verticalIntend = 3;
        float ButtonWidth = 0.15f;//persents
        float previousPos = 0;

        float buttonHeight = (Screen.height - ((scenesControllers.Length - 1) * verticalIntend)) / scenesControllers.Length;
        for (int i = 0; i < scenesControllers.Length; i++)
        {
          float x = Screen.width - Screen.width * ButtonWidth;
          float y = previousPos;
          float w = Screen.width * ButtonWidth;
          float h = buttonHeight;
          if (GUI.Button(new Rect(x, y, w, h),
            scenesControllers[i].ToString()))
          {
            ScenesFSM.ForceStopScene();
            SceneManager.LoadScene(scenesControllers[i].ToString());
          }
          previousPos += buttonHeight + verticalIntend;
        }
      }

    }

  }
}
