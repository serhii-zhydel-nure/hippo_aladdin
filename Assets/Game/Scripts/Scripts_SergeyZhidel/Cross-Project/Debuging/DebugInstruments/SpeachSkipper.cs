﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scripts_SergeyZhidel.Debuging
{
  public class SpeachSkipper : MonoBehaviour
  {
    public static List<SoundBehavior> SpeachSourses = new List<SoundBehavior>();

#if DEVELOPMENT_BUILD
    void OnGUI()
    {
      if (GUI.Button(
        new Rect(
        Screen.width * 0.01f,
        Screen.height * 0.2f,
        Screen.width * 0.07f,
        Screen.height * 0.05f),
        "Skip"))
      {
        SkipSpeach();
      }
    }
#endif

    void Update()
    {
      if(Input.GetKeyDown(KeyCode.S))
        SkipSpeach();
    }

    public void SkipSpeach()
    {
      foreach (SoundBehavior player in SpeachSourses)
      {
        player.StopTalking();
      }
    }

    public static bool IsAnybodySpeaking()
    {
      return SpeachSourses.Any(hero => hero.IsTalking);
    }
  }
}