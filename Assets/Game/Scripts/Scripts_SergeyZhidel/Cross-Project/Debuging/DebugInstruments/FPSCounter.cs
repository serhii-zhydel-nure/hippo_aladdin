﻿using UnityEngine;

namespace Scripts_SergeyZhidel.Debuging
{
  public class FPSCounter : MonoBehaviour
  {
    private string _curFps;
    GUIStyle _curFpsStyle = new GUIStyle();
    private string _minFps;
    GUIStyle _minFpsStyle = new GUIStyle();
    private float _accum;
    private int _frames;
    private float _maxFrame;
    private float _timeleft;
    public float UpdateInterval = 0.5f;

    private void Awake()
    {
      _timeleft = UpdateInterval;
    }


    private void Update()
    {
      _timeleft -= Time.deltaTime;
      _accum += Time.timeScale / Time.deltaTime;
      _frames++;
      _maxFrame = Mathf.Max(_maxFrame, Time.deltaTime);
      if (_timeleft <= 0.0)
      {
        float num = _accum / _frames;
        Color color = (num < 30f) ? ((num <= 10f) ? Color.yellow : Color.red) : Color.green;
        _curFps = string.Format("FPS: {0:F1}", num);
        _curFpsStyle.normal.textColor = color;
        _timeleft = UpdateInterval;
        _accum = 0f;
        _frames = 0;

        float num2 = 1f / _maxFrame;
        Color color2 = (num2 < 30f) ? ((num2 <= 10f) ? Color.yellow : Color.red) : Color.green;
        _minFps = string.Format("MIN: {0:F1}", num2);
        _minFpsStyle.normal.textColor = color2;
        _maxFrame = 0f;
      }
    }

    private void OnGUI()
    {
      GUI.Label(new Rect(
     Screen.width * 0.05f,
     Screen.height * 0.01f,
     Screen.width * 0.07f,
     Screen.height * 0.05f), _curFps, _curFpsStyle);

      GUI.Label(new Rect(
     Screen.width * 0.20f,
     Screen.height * 0.01f,
     Screen.width * 0.07f,
     Screen.height * 0.05f), _minFps, _minFpsStyle);
    }
  }
}

