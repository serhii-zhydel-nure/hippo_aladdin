﻿using PSV;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.Debuging
{
    public class DebuggingConfigurations
    {
        public static GameObject DebugContainer;


        public static void LoadDebugingConfigurations()
        {
#if (UNITY_EDITOR || DEVELOPMENT_BUILD)
            DebugContainer = GameManager.Instance.gameObject.CreateChild("Debug");
            Debug.unityLogger.logEnabled = true;
            DebugContainer.AddComponent<SpeachSkipper>();
            DebugContainer.AddComponent<CustomConsole>();
            GameSettings.StartConfigurations();
#endif
#if !UNITY_EDITOR && DEVELOPMENT_BUILD//device bild
      DebugContainer.AddComponent<SpeachSkipper>();
      //DebugContainer.AddComponent<SceneButtonsShower>();
      DebugContainer.AddComponent<FPSCounter>();
      DebugContainer.AddComponent<CustomConsole>();
      DebugContainer.AddComponent<PlayerPrefsCleaner>();
      //SetGameConfigurations();
#endif
#if !UNITY_EDITOR && !DEVELOPMENT_BUILD//production build
      //Debug.logger.logEnabled = false;
#endif
        }
    }
}