﻿using System;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class Timer : MonoBehaviour
  {
    public static Timer GiveNewInstance(float interval, string name, bool isCyclical = true)
    {
      var timer = CommonScriptsContainer.Instance.AddComponent<Timer>();
      timer.Cyclical = isCyclical;
      timer.Interval = interval;
      timer.Name = name;
      return timer;
    }

    /// <summary>
    /// in seconds
    /// </summary>
    public float Interval = 10;

    public string Name = "Timer";

    public event Action OnTick;

    public bool Cyclical = true;

    public bool IsWorking;

    [SerializeField]
    private float _curTime;

    public void StartWork()
    {
      _curTime = 0;
      IsWorking = true;
    }


    public void Stop()
    {
      IsWorking = false;
    }

    public void Restart()
    {
      Stop();
      StartWork();
    }


    void Update()
    {
      if (IsWorking)
      {
        _curTime += Time.deltaTime;

        if (_curTime >= Interval)
        {
          _curTime = 0;
          OnTick.InvokeSafe();
          if (!Cyclical)
            Stop();
        }
      }
    }
  }
}