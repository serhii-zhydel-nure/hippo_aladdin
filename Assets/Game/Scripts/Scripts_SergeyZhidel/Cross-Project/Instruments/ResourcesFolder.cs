﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Scripts_SergeyZhidel
{
 
  public static class ResourcesFolder
  {
    private static readonly string PrefabFolderPath = "Prefabs";
    private static readonly string TrackFolderPath = Path.Combine("Sounds", "NonLanguage");
    private static readonly string SpeechFolderPath = Path.Combine("Sounds", "MultiLanguage");

    private static Dictionary<string, AudioClip> _tracks = new Dictionary<string, AudioClip>();
    private static Dictionary<string, AudioClip> _speeches = new Dictionary<string, AudioClip>();
    private static Dictionary<string, GameObject> _prefabs = new Dictionary<string, GameObject>();

    static ResourcesFolder()
    {
      LoadPrefabs();
      LoadTracks();
      LoadSpeeches(GameSettings.CurrentLanguage.ToString());
      GameSettings.CurrentLanguage.OnValueChanged += language => LoadSpeeches(language.ToString());
    }

    private static void LoadSpeeches(string languageName)
    {
      _speeches = new Dictionary<string, AudioClip>();
      foreach (var item in Resources.LoadAll<AudioClip>(Path.Combine(SpeechFolderPath, languageName)))
        _speeches.Add(item.name, item);
    }
    private static void LoadTracks()
    {
      _tracks = new Dictionary<string, AudioClip>();
      foreach (var item in Resources.LoadAll<AudioClip>(TrackFolderPath))
        _tracks.Add(item.name, item);
    }
    private static void LoadPrefabs()
    {
      var result = Resources.LoadAll<GameObject>(PrefabFolderPath);
      if (result != null)
      {
        foreach (GameObject prefab in result)
        {
          _prefabs.Add(prefab.name, prefab);
        }
      }
    }

    public static AudioClip GetSpeech(string speechCode)
    {
      if (!_speeches.ContainsKey(speechCode))
        Debug.LogWarning(String.Format("<color=red>No such speech: {0}</color>", speechCode));

      return _speeches[speechCode];
    }

    public static AudioClip GetTracks(string trackCode)
    {
      if (!_tracks.ContainsKey(trackCode))
        Debug.LogWarning(String.Format("<color=red>No such track: {0}</color>", trackCode));

      return _tracks[trackCode];
    }

    public static GameObject GetPrefab(string prefabName)
    {
      if (!_prefabs.ContainsKey(prefabName))
        Debug.LogWarning(String.Format("<color=red>No such prefab: " + prefabName));

      return _prefabs[prefabName];
    }

    public static GameObject GetPrefab(string subFolder, string prefabName)
    {
      var result = Resources.LoadAll<GameObject>(Path.Combine(PrefabFolderPath, subFolder))
        .FirstOrDefault(pref => pref.name == prefabName);

      if (result == null)
        Debug.LogWarning(String.Format("<color=red>No such prefab: " + prefabName));

      return result;
    }

    public static GameObject[] GetAllPrefabs(string subFolder)
    {
      var result = Resources.LoadAll<GameObject>(Path.Combine(PrefabFolderPath, subFolder)).ToArray();

      if (result.Length == 0)
        Debug.LogWarning("No prefabs there: " + Path.Combine(PrefabFolderPath, subFolder));

      return result;
    }

    public static GameObject[] FindAllPrefabInstances(GameObject prefab)
    {
      GameObject[] allObjects = (GameObject[])Object.FindObjectsOfType(typeof(GameObject));
      return allObjects.Where(obj => obj.name.Contains(prefab.name)).ToArray();
    }
  }
}