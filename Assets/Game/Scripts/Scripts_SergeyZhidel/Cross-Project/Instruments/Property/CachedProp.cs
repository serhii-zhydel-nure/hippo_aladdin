﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  [Serializable]
  public class CachedProp<T>
  {
    public T Value
    {
      get
      {
        return _cachedValue;
      }
      set
      {
        _setMethod.Invoke(value);
        _cachedValue = value;
      }
    }

    [HideInInspector]
    [SerializeField]
    private T _cachedValue;

    private readonly Action<T> _setMethod;

    public CachedProp(T startValue, Action<T> setMethod)
    {
      _setMethod = setMethod;
      Value = startValue;
    }

    public static implicit operator T(CachedProp<T> prop)
    {
      return prop.Value;
    }


    public static bool operator ==(CachedProp<T> propA, CachedProp<T> propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(CachedProp<T> propA, CachedProp<T> propB)
    {
      return !(propA == propB);
    }

    public bool Equals(CachedProp<T> other)
    {
      return Value.Equals(other.Value);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((CachedProp<T>)obj);
    }

    public override int GetHashCode()
    {
      return Value.GetHashCode() + _setMethod.GetHashCode();
    }
  }
}