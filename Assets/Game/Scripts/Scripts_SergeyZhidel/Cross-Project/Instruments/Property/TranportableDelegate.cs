﻿using System;
using Scripts_SergeyZhidel.Extenders;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  /// Обертка, позволяющая передавать "ссылку на ссылку обьекта". 
  /// Если даже будет назначен новый обьект, то все кто имеет ссылку на эту обертку тоже получат новый обьект. 
  /// Таким образом можно использовать один и тот же обьект сразу в нескольих местах, чтоб иметь самую последнюю версию.
  /// </summary>
  /// <typeparam name="T"></typeparam>
  public class TransferableProp<T>
  {
    public event Action<T> OnValueChanged;

    private T _value;

    public T Value
    {
      get { return _value; }
      set
      {
        _value = value;
        OnValueChanged.InvokeSafe(value);
      }
    }


    public TransferableProp(T startValue)
    {
      Value = startValue;
    }

    public TransferableProp()
    {
      Value = default(T);
    }

    public static implicit operator T(TransferableProp<T> prop)
    {
      return prop.Value;
    }


    public static bool operator ==(TransferableProp<T> propA, TransferableProp<T> propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(TransferableProp<T> propA, TransferableProp<T> propB)
    {
      return !(propA == propB);
    }

    public bool Equals(TransferableProp<T> other)
    {
      return Value.Equals(other.Value);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((CachedProp<T>)obj);
    }

    public override int GetHashCode()
    {
      return Value.GetHashCode();
    }
  }
}