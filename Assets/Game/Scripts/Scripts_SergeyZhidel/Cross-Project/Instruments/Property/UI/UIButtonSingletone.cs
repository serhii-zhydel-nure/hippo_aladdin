﻿using System;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts_SergeyZhidel
{
  public abstract class UIButtonSingletone<TInheritor> : MonoBehaviour
      where TInheritor : UIButtonSingletone<TInheritor>
  {
    public static TInheritor Instance;

    public static event Action<Button> StaticOnClick
    {
      add
      {
        _staticOnClick += value;
        if (Instance != null)
          Instance.SetMethod();
      }
      remove
      {
        _staticOnClick -= value;
        if (Instance != null)
          Instance.SetMethod();
      }
    }

    private static Action<Button> _staticOnClick;


    protected virtual void SetMethod()
    {
      Button pressedButton = GetComponent<Button>();
      pressedButton.onClick.RemoveAllListeners();
      pressedButton.onClick.AddListener(() => _staticOnClick.InvokeSafe(pressedButton));
    }


    protected virtual void Awake()
    {
      Instance = (TInheritor)this;
      SetMethod();
    }
  }
}