﻿using System;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  /// Чтобы удобно управлять значением UI элемента, даже если он на другой сцене. 
  /// По загрузке, значение само измениться на то, что в StaticValue.
  /// </summary>
  /// <typeparam name="TInheritor"></typeparam>
  /// <typeparam name="TValue"></typeparam>
  public abstract class UITextSingletone<TInheritor, TValue> : MonoBehaviour where TInheritor : UITextSingletone<TInheritor, TValue>
  {
    protected static TInheritor _instance;

    public static event Action<TValue> OnValueChanged;


    public static TValue StaticValue
    {
      get { return _staticValue; }
      set
      {
        _staticValue = value;
        if (_instance != null && !_instance.Value.Equals(value))
          _instance.Value = value;
        OnValueChanged.InvokeSafe(value);
      }
    }

    private static TValue _staticValue;



    public TValue Value
    {
      get
      {
        return _value;
      }
      set
      {
        SetMethod(value);
      }
    }

    [SerializeField]
    private TValue _value;

    protected virtual void SetMethod(TValue newValue)
    {
      _value = newValue;
      if (!_staticValue.Equals(newValue))
        _staticValue = newValue;
    }

    protected virtual void Awake()
    {
      _instance = (TInheritor)this;
      Value = StaticValue;
    }
  }
}