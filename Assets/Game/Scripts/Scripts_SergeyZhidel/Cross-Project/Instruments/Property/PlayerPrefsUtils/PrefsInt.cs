﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class PrefsInt:APlayerPrefProperty<int>
  {
    

    public PrefsInt(string key, int value) : base(key, value)
    {
    }

    protected override int GetValue()
    {
      return PlayerPrefs.GetInt(PrefsKey);
    }

    protected override void SetValue(int listToSet)
    {
      PlayerPrefs.SetInt(PrefsKey, listToSet);
    }

    public static implicit operator int(PrefsInt prop)
    {
      return prop.ToValueType();
    }


    public static bool operator ==(PrefsInt propA, PrefsInt propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(PrefsInt propA, PrefsInt propB)
    {
      return !(propA == propB);
    }

    protected bool Equals(PrefsInt other)
    {
      return Value.Equals(other.Value) && PrefsKey.Equals(other.PrefsKey);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((PrefsInt)obj);
    }

    public override int GetHashCode()
    {
      return PrefsKey.GetHashCode();
    }
  }
}