﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class PrefsFloat : APlayerPrefProperty<float>
  {
    protected bool Equals(PrefsFloat other)
    {
      return Value.Equals(other.Value) && PrefsKey.Equals(other.PrefsKey);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((PrefsFloat) obj);
    }

    public override int GetHashCode()
    {
      return PrefsKey.GetHashCode();
    }

    public PrefsFloat(string key, float value) : base(key, value)
    {
    }

    protected override float GetValue()
    {
      return PlayerPrefs.GetFloat(PrefsKey);
    }

    protected override void SetValue(float listToSet)
    {
      PlayerPrefs.SetFloat(PrefsKey, listToSet);
    }

    public static implicit operator float(PrefsFloat prop)
    {
      return prop.ToValueType();
    }


    public static bool operator ==(PrefsFloat propA, PrefsFloat propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(PrefsFloat propA, PrefsFloat propB)
    {
      return !(propA == propB);
    }
  }
}