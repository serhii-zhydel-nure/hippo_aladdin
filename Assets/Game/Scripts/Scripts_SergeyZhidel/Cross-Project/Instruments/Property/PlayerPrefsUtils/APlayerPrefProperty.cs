﻿using System;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public abstract class APlayerPrefProperty<T>
  {
    public event Action<T> OnValueChanged;

    public readonly string PrefsKey;

    public T Value
    {
      get
      {
        return GetValue();
      }
      set
      {
        SetValue(value);
        OnValueChanged.InvokeSafe(value);
      }
    }

    /// <summary>
    /// Value = value if PlayerPrefs doesn't have value
    /// </summary>
    /// <param name="key">PlayerPrefs Key</param>
    /// <param name="value">sets if PlayerPrefs doesn't have value</param>
    protected APlayerPrefProperty(string key, T value)
    {
      PrefsKey = key;
      if (!HasValue())
        Value = value;
    }

    protected abstract T GetValue();

    protected abstract void SetValue(T value);

    public bool HasValue()
    {
      return PlayerPrefs.HasKey(PrefsKey);
    }

    public override string ToString()
    {
      return Value.ToString();
    }

    /// <summary>
    /// Must be overridden as implicit conversion operator from APlayerPrefProperty<T> to T
    /// </summary>
    /// <returns></returns>
    protected T ToValueType()
    {
      return Value;
    }

  }
}