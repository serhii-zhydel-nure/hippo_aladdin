﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class PrefsString : APlayerPrefProperty<string>
  {
    public PrefsString(string key, string value) : base(key, value)
    {
    }

    protected bool Equals(PrefsString other)
    {
      return Value.Equals(other.Value) && PrefsKey.Equals(other.PrefsKey);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((PrefsString) obj);
    }

    public override int GetHashCode()
    {
      return PrefsKey.GetHashCode();
    }



    protected override string GetValue()
    {
      return PlayerPrefs.GetString(PrefsKey);
    }

    protected override void SetValue(string listToSet)
    {
      PlayerPrefs.SetString(PrefsKey, listToSet);
    }

    public static implicit operator string(PrefsString prop)
    {
      return prop.ToValueType();
    }

    public static bool operator ==(PrefsString propA, PrefsString propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(PrefsString propA, PrefsString propB)
    {
      return !(propA == propB);
    }
  }
}