﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class PrefsEnum<T> : APlayerPrefProperty<T> where T : struct, IComparable, IConvertible, IFormattable
  {
    protected bool Equals(PrefsEnum<T> other)
    {
      return Value.Equals(other.Value) && PrefsKey.Equals(other.PrefsKey);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((PrefsEnum<T>) obj);
    }

    public override int GetHashCode()
    {
      return PrefsKey.GetHashCode();
    }

    public PrefsEnum(string key, T value) : base(key, value)
    {
    }

    protected override T GetValue()
    {
      return (T)Enum.ToObject(typeof (T), PlayerPrefs.GetInt(PrefsKey));
    }

    protected override void SetValue(T listToSet)
    {
      PlayerPrefs.SetInt(PrefsKey, Convert.ToInt32(listToSet)); ;
    }

    public static implicit operator T(PrefsEnum<T> prefsEnumProp)  // implicit PrefsEnum<T> to T conversion operator
    {
      return prefsEnumProp.ToValueType();
    }

    public static bool operator ==(PrefsEnum<T> propA, PrefsEnum<T> propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(PrefsEnum<T> propA, PrefsEnum<T> propB)
    {
      return !(propA == propB);
    }
  }
}