﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class PlayerPrefsDateTime : APlayerPrefProperty<DateTime>
  {
    private string _yearKey;
    private string _monthKey;
    private string _dayKey;
    private string _hourKey;
    private string _minutesKey;
    private string _secondsKey;
    private string _milliSecondsKey;

    public PlayerPrefsDateTime(string key, DateTime value) : base(key, value)
    {
      _yearKey = PrefsKey + "_year";
      _monthKey = PrefsKey + "_month";
      _dayKey = PrefsKey + "_day";
      _hourKey = PrefsKey + "_hour";
      _minutesKey = PrefsKey + "_minutes";
      _secondsKey = PrefsKey + "_seconds";
      _milliSecondsKey = PrefsKey + "_milliSeconds";
    }

    protected override DateTime GetValue()
    {
      return new DateTime(PlayerPrefs.GetInt(_yearKey), PlayerPrefs.GetInt(_monthKey),
        PlayerPrefs.GetInt(_dayKey), PlayerPrefs.GetInt(_hourKey),
        PlayerPrefs.GetInt(_minutesKey), PlayerPrefs.GetInt(_secondsKey),
        PlayerPrefs.GetInt(_milliSecondsKey));
    }

    protected override void SetValue(DateTime listToSet)
    {
      PlayerPrefs.SetInt(_yearKey, listToSet.Year);
      PlayerPrefs.SetInt(_monthKey, listToSet.Month);
      PlayerPrefs.SetInt(_dayKey, listToSet.Day);
      PlayerPrefs.SetInt(_hourKey, listToSet.Hour);
      PlayerPrefs.SetInt(_minutesKey, listToSet.Minute);
      PlayerPrefs.SetInt(_secondsKey, listToSet.Second);
      PlayerPrefs.SetInt(_milliSecondsKey, listToSet.Millisecond);
    }

    protected bool Equals(PlayerPrefsDateTime other)
    {
      return Value.Equals(other) && PrefsKey.Equals(other.PrefsKey);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((PlayerPrefsDateTime)obj);
    }

    public override int GetHashCode()
    {
      return PrefsKey.GetHashCode();
    }


    public static implicit operator DateTime(PlayerPrefsDateTime prop)
    {
      return prop.ToValueType();
    }


    public static bool operator ==(PlayerPrefsDateTime propA, PlayerPrefsDateTime propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(PlayerPrefsDateTime propA, PlayerPrefsDateTime propB)
    {
      return !(propA == propB);
    }
  }
}