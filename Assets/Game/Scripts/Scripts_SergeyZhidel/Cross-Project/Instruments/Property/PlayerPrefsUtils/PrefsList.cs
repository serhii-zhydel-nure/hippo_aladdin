﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class PrefsList<TPrefProp, TParam> : APlayerPrefProperty<IList<TParam>>, IList<TParam>
    where TPrefProp : APlayerPrefProperty<TParam>
  {
    /// <summary>
    /// Quantity of items and PrefsList's main pref("HasKey" check this value)
    /// </summary>
    private PrefsInt _itemCount;
    public List<TPrefProp> ListOfProperties;

    static PrefsList()
    {
      if (typeof(TPrefProp).IsAbstract)
        throw new TypeLoadException(typeof(TPrefProp).Name + "is Abstract. You cannot create it's instances.");
    }

    public PrefsList(string key, List<TParam> value) : base(key, value)
    {
      if (_itemCount == null)
      {
        _itemCount = new PrefsInt(PrefsKey, 0);
        ListOfProperties = new List<TPrefProp>(_itemCount);
        for (int i = 0; i < _itemCount; i++)
        {
          string nextItemKey = String.Format("{0} [{1}]", PrefsKey, i);
          ListOfProperties.Add(Reflection.CreateInstance<TPrefProp, string, TParam>(nextItemKey, default(TParam)));
        }
      }
    }

    protected override IList<TParam> GetValue()
    {
      return ListOfProperties.Select(itemProp => itemProp.Value).ToList();
    }

    protected override void SetValue(IList<TParam> listToSet)
    {
      if (_itemCount == null)
      {
        _itemCount = new PrefsInt(PrefsKey, 0);
        ListOfProperties = new List<TPrefProp>(_itemCount);
        for (int i = 0; i < _itemCount; i++)
        {
          string nextItemKey = String.Format("{0} [{1}]", PrefsKey, i);
          ListOfProperties.Add(Reflection.CreateInstance<TPrefProp, string, TParam>(nextItemKey, default(TParam)));
        }
      }

      foreach (TPrefProp itemProp in ListOfProperties)
          PlayerPrefs.DeleteKey(itemProp.PrefsKey);

        ListOfProperties = new List<TPrefProp>(listToSet.Count);
      

      for (int i = 0; i < listToSet.Count; i++)
      {
        string nextItemKey = String.Format("{0} [{1}]", PrefsKey, i);
        ListOfProperties.Add(Reflection.CreateInstance<TPrefProp, string, TParam>(nextItemKey, listToSet[i]));
      }

      _itemCount.Value = ListOfProperties.Count;
    }

    public IEnumerator<TParam> GetEnumerator()
    {
      return GetValue().GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    public void Add(TParam item)
    {
      string nextItemKey = String.Format("{0} [{1}]", PrefsKey, _itemCount.Value++);
      ListOfProperties.Add(Reflection.CreateInstance<TPrefProp, string, TParam>(nextItemKey, item));
    }

    public void Clear()
    {
      ListOfProperties = new List<TPrefProp>(0);
      _itemCount = new PrefsInt(PrefsKey, 0);
    }

    public bool Contains(TParam item)
    {
      return ListOfProperties.Any(itemProp => itemProp.Value.Equals(item));
    }

    public void CopyTo(TParam[] array, int arrayIndex)
    {
      GetValue().CopyTo(array, arrayIndex);
    }

    public bool Remove(TParam itemToRemove)
    {
      TPrefProp propItem = ListOfProperties.FirstOrDefault(item => item.Equals(itemToRemove));

      if (propItem == null)
        return false;

      RemoveAt(IndexOf(itemToRemove));

      return true;
    }

    public int Count { get { return _itemCount.Value; } }

    public bool IsReadOnly { get { return false; } }

    public int IndexOf(TParam itemToSearch)
    {
      return ListOfProperties.FindIndex(item => item.Equals(itemToSearch));
    }

    public void Insert(int index, TParam item)
    {
      if (index < 0 || index > ListOfProperties.Count)
        throw new IndexOutOfRangeException(index.ToString());

      Add(item);

      for (int i = ListOfProperties.Count; i > index; i--)
      {
        ListOfProperties[i].Value = ListOfProperties[i - 1].Value;
      }
      ListOfProperties[index].Value = item;
    }

    public void RemoveAt(int index)
    {
      for (int i = index; i < ListOfProperties.Count - 1; i++)
      {
        ListOfProperties[i].Value = ListOfProperties[i + 1].Value;
      }
      ListOfProperties.RemoveAt(ListOfProperties.Count);
    }

    public TParam this[int index]
    {
      get { return ListOfProperties[index].Value; }
      set { ListOfProperties[index].Value = value; }
    }

    public static implicit operator List<TParam>(PrefsList<TPrefProp, TParam> prefsListProp)  // implicit PrefsEnum<T> to T conversion operator
    {
      return prefsListProp.GetValue() as List<TParam>;
    }

    public static bool operator ==(PrefsList<TPrefProp, TParam> propA, PrefsList<TPrefProp, TParam> propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(PrefsList<TPrefProp, TParam> propA, PrefsList<TPrefProp, TParam> propB)
    {
      return !(propA == propB);
    }

    protected bool Equals(PrefsList<TPrefProp, TParam> other)
    {
      return Value.Equals(other.Value) && PrefsKey.Equals(other.PrefsKey);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((PrefsList<TPrefProp, TParam>)obj);
    }

    public override int GetHashCode()
    {
      return PrefsKey.GetHashCode();
    }
  }
}