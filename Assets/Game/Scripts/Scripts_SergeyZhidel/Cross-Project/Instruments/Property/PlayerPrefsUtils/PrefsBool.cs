﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class PrefsBool:APlayerPrefProperty<bool>
  {
    protected bool Equals(PrefsBool other)
    {
      return Value.Equals(other.Value) && PrefsKey.Equals(other.PrefsKey);
    }

    public override bool Equals(object obj)
    {
      if (ReferenceEquals(null, obj)) return false;
      if (ReferenceEquals(this, obj)) return true;
      if (obj.GetType() != GetType()) return false;
      return Equals((PrefsBool) obj);
    }

    public override int GetHashCode()
    {
      return PrefsKey.GetHashCode();
    }

    public PrefsBool(string key, bool value) : base(key, value)
    {
    }

    protected override bool GetValue()
    {
      return PlayerPrefs.GetInt(PrefsKey) == 1;
    }

    protected override void SetValue(bool listToSet)
    {
      PlayerPrefs.SetInt(PrefsKey, listToSet ? 1 : 0);
    }

    public static implicit operator bool(PrefsBool prop)
    {
      return prop.ToValueType();
    }

    public static bool operator ==(PrefsBool propA, PrefsBool propB)
    {
      // If both are null, or both are same instance, return true.
      if (ReferenceEquals(propA, propB))
      {
        return true;
      }

      // If one is null, but not both, return false.
      if (((object)propA == null) || ((object)propB == null))
      {
        return false;
      }

      // Return true if the fields match:
      return propA.Value.Equals(propB.Value);
    }

    public static bool operator !=(PrefsBool propA, PrefsBool propB)
    {
      return !(propA == propB);
    }
  }
}