﻿using System;
using Scripts_SergeyZhidel.InputManagement.Local;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  /// Контроллер-обертка для удобного управления перетаскиванием обьектов.
  /// По дефолту:
  /// для сложности 2+ - тапнул и обьект полетел на цель,
  /// для 5+ - надо перетащить обьект на цель.
  /// Если нужно другое поведения при перетаскивания - есть поля twoYearsDragType и fiveYearsDragType.
  /// </summary>
  class DragGameController : IDragableObj, IDisposable
  {
    public event Action<GameObject> StartDragging
    {
      add { _dragControll.StartDragging += value; }
      remove { _dragControll.StartDragging -= value; }
    }

    public event Action<GameObject> FinishedSuccessfully
    {
      add { _dragControll.FinishedSuccessfully += value; }
      remove { _dragControll.FinishedSuccessfully -= value; }
    }

    public event Action<GameObject> FinishedUnSuccessfully
    {
      add { _dragControll.FinishedUnSuccessfully += value; }
      remove { _dragControll.FinishedUnSuccessfully -= value; }
    }

    public Collider2D TargetCollider2D
    {
      get { return _dragControll.TargetCollider2D; }
      set { _dragControll.TargetCollider2D = value; }
    }

    public bool CanFinishDraggingSuccessful
    {
      get { return _dragControll.CanFinishDraggingSuccessful; }
      set { _dragControll.CanFinishDraggingSuccessful = value; }
    }

    public bool MoveBackIfFinishedNotCorrect
    {
      get
      {
        if (_dragControll is DragToCollider)
          return (_dragControll as DragToCollider).MoveBackAtItsPlace;
        return false;
      }
      set
      {
        if (_dragControll is DragToCollider)
          (_dragControll as DragToCollider).MoveBackAtItsPlace = value;
      }
    }

    private readonly IDragableObj _dragControll;

    /// <summary>
    /// Контроллер-обертка для удобного управления перетаскиванием обьектов.
    /// По стандарту 
    /// для сложности 2+ - тапнул и обьект полетел на цель,
    /// для 5+ - надо перетащить обьект на цель.
    /// Если нужно другое поведения при перетаскивания - есть поля twoYearsDragType и fiveYearsDragType.
    /// </summary>
    /// <param name="dragableObj"></param>
    /// <param name="target">Можно задать вручную, после создания обьекта</param>
    /// <param name="twoYearsDragType">Тип перетаскивания, для сложности 2+. Должен быть "MonoBehaviour,IDragableObj".</param>
    /// <param name="fiveYearsDragType">Тип перетаскивания, для сложности 5+. Должен быть "MonoBehaviour,IDragableObj".</param>
    public DragGameController(GameObject dragableObj, Collider2D target = null, Type twoYearsDragType = null, Type fiveYearsDragType = null)
    {
      if (GameSettings.Difficulty.Value == GameDifficulty.TwoYears)
      {
        if (twoYearsDragType == null)
          _dragControll = dragableObj.AddComponent<TapDragToCollider>();//default type
        else
          _dragControll = (IDragableObj)dragableObj.AddComponent(twoYearsDragType);
      }
      if (GameSettings.Difficulty.Value == GameDifficulty.FiveYears)
      {
        if (fiveYearsDragType == null)
          _dragControll = dragableObj.AddComponent<DragToCollider>();//default type
        else
          _dragControll = (IDragableObj)dragableObj.AddComponent(fiveYearsDragType);
      }

      _dragControll.TargetCollider2D = target;
    }

    public void Dispose()
    {
      Destroy();
    }

    public void Destroy()
    {
      GameObject.Destroy(_dragControll as MonoBehaviour);
    }
  }
}
