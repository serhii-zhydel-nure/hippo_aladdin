﻿using System;
using System.Collections.Generic;
using UnityEngine;
#if PSV_PROTOTYPE
using Language = Languages.Language;
#endif

namespace Scripts_SergeyZhidel
{
#if !PSV_PROTOTYPE
  public enum Language
  {
    Arabic = SystemLanguage.Arabic,
    Chinese = SystemLanguage.Chinese,
    Czech = SystemLanguage.Czech,
    Danish = SystemLanguage.Danish,
    Dutch = SystemLanguage.Dutch,
    English = SystemLanguage.English,
    Finnish = SystemLanguage.Finnish,
    French = SystemLanguage.French,
    German = SystemLanguage.German,
    Greek = SystemLanguage.Greek,
    Italian = SystemLanguage.Italian,
    Japanese = SystemLanguage.Japanese,
    Norwegian = SystemLanguage.Norwegian,
    Polish = SystemLanguage.Polish,
    Portuguese = SystemLanguage.Portuguese,
    Romanian = SystemLanguage.Romanian,
    Russian = SystemLanguage.Russian,
    Spanish = SystemLanguage.Spanish,
    Swedish = SystemLanguage.Swedish,
    Turkish = SystemLanguage.Turkish,
    Hindi = 50,
    Hebrew = SystemLanguage.Hebrew,
    Indonesian = SystemLanguage.Indonesian,
    Korean = SystemLanguage.Korean,
    Thai = SystemLanguage.Thai,
    Ukrainian = SystemLanguage.Ukrainian,
    Catalan = SystemLanguage.Catalan,
    Belarusian = SystemLanguage.Belarusian
  };
#endif

  public static class Localizator
  {
    public static Language DefineLanguageForThisDevice()
    {
      Language curLanguage = Language.English;
#if UNITY_ANDROID
      if (GetAndroidDisplayLanguage() != "Unknown")
        curLanguage = (Language)Enum.Parse(typeof(Language), Application.systemLanguage.ToString(), true);
#endif

      if (_overridableLanguages.ContainsKey(curLanguage))
        //change by more common language
        curLanguage = _overridableLanguages[curLanguage];

      if (_availableInBuildLanguages.Contains(curLanguage))
        return curLanguage;
      return _availableInBuildLanguages.Contains(Language.English) ? Language.English : Language.Russian;
    }

    private static string GetAndroidDisplayLanguage()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
      return Application.systemLanguage.ToString();
#elif UNITY_ANDROID
        AndroidJavaClass localeClass = new AndroidJavaClass ( "java/util/Locale" );
        AndroidJavaObject defaultLocale = localeClass.CallStatic<AndroidJavaObject> ( "getDefault" );
        AndroidJavaObject usLocale = localeClass.GetStatic<AndroidJavaObject> ( "US" );
        string systemLanguage = defaultLocale.Call<string> ( "getDisplayLanguage", usLocale );
        Debug.Log ( "Android language is " + systemLanguage + " detected as " + systemLanguage );
        return systemLanguage;
#endif
    }


    private static Dictionary<Language, Language> _overridableLanguages = new Dictionary<Language, Language>
    {
      {Language.Belarusian, Language.Russian},
      {Language.Ukrainian, Language.Russian},
      {Language.Catalan, Language.Spanish}
    };

    private static List<Language> _availableInBuildLanguages = new List<Language>
    {
      Language.Russian
    };
  }



}
