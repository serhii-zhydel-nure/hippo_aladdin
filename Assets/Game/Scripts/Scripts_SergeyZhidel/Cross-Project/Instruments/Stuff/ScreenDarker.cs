﻿using System.Collections;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts_SergeyZhidel
{
  public class ScreenDarker
  {
    public static ScreenDarker Instance
    {
      get
      {
        if (_instance == null)
        {
          _instance = new ScreenDarker();
        }
        return _instance;
      }
    }
    private static ScreenDarker _instance;

    private GameObject _source;
    private Canvas _souceCanvas;
    private CanvasScaler _scaler;
    private Image _darkImage;

    protected ScreenDarker()
    {
      _source = new GameObject("ScreenDarker");
      _source.transform.SetParent(GameManager.Instance.gameObject.transform);
      _souceCanvas = _source.AddComponent<Canvas>();
      _souceCanvas.sortingLayerName = "UI";
      _souceCanvas.sortingOrder = 10000;
      _souceCanvas.renderMode = RenderMode.ScreenSpaceCamera;
      _souceCanvas.worldCamera = Camera.main;
      _scaler = _source.AddComponent<CanvasScaler>();
      _scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
      _source.AddComponent<GraphicRaycaster>();

      _darkImage = _source.AddComponent<Image>();
      _darkImage.color = new Color(0,0,0,0);
      _source.SetActive(false);
    }

    public static IEnumerator FadeScreenCor(float seconds)
    {
      Instance._source.SetActive(true);
      yield return Instance._darkImage.TransparencyOverSeconds(1, seconds);
      Instance._source.SetActive(false);
    }

    public static IEnumerator RiseScreenCor(float seconds)
    {
      Instance._source.SetActive(true);
      yield return Instance._darkImage.TransparencyOverSeconds(0, seconds);
      Instance._source.SetActive(false);
    }

    public static void FadeScreen(float seconds)
    {
      GameManager.Instance.StartCoroutine(FadeScreenCor(seconds));
    }

    public static void RiseScreen(float seconds)
    {
      GameManager.Instance.StartCoroutine(RiseScreenCor(seconds));
    }
  }
}