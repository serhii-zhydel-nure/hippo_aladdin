﻿using System;
using System.Collections.Generic;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.ScenesManagment
{
  public class EndlessRoad : MonoBehaviour
  {
    [Tooltip("Задний триггер")]
    public Trigger2D BackTrigger;
    [Tooltip("Пердний триггер")]
    public Trigger2D ForwardTrigger;

    [Tooltip("Смещение тригеров от краев выстроенных в ряд ячеек (в процентах от длины ряда ячеек)")]
    [Range(0, 100)]
    public int TrigerIntendPersents = 0;
    private float _triggerBorderIntend = 0;

    [Tooltip("Принудительное смещение триггеров по выбраной оси")]
    public float ForcelLocalTrigerShift = 0;

    [Tooltip("Нахлест ячеек друг на друга. Минус - нахлест справа на лево.")]
    [Range(-3,3)]
    public float CellsOverlapping = 0;

    [Tooltip("Ось по которой будет двигаться дорога.")]
    public Axis3D DirectionAxis;
    public event Action<GameObject> CellChangedPosition; 

    public GameObject[] Cells;
    private Dictionary<GameObject, Vector3> cellsAndTheirSizes = new Dictionary<GameObject, Vector3>(); 
    //private Dictionary<GameObject, Vector3> cellsAndTheirCenters = new Dictionary<GameObject, Vector3>(); 

    public GameObject Voyager;


    private void Awake()
    {
      if (Cells == null)//not seted
        throw new NullReferenceException("Cells aren't setted.");

      if (Cells.Length <= 1)
        throw new ArgumentException("Must be at least 2 parts of road.");

      foreach (var cell in Cells)
      {
        if(cell == null)
          throw new NullReferenceException("Cell isn't setted.");
      }

      if(Cells.HasDuplicates())
        throw new ArgumentException("Duplicated cells");

      ReadCellsSizes();

      if (TrigerIntendPersents != 0)
      _triggerBorderIntend = 
          ((GetAllCellsLenght(DirectionAxis) - CellsOverlapping * Cells.Length) 
          * TrigerIntendPersents / 100);
      
      if (Voyager == null)
        throw new NullReferenceException("Voyager is NULL, nobody need your road.");

      if (BackTrigger == null)
        throw new NullReferenceException("BackTrigger is NULL, Voyager cannot move this way.");

      if (ForwardTrigger == null)
        throw new NullReferenceException("ForwardTrigger is NULL, Voyager cannot move this way.");

      MakeRoad();

      BackTrigger.TriggerEnter.AddListener((otherCollider) =>
      {
        if (IsVoyager(otherCollider.gameObject) &&
            otherCollider.gameObject.transform.position.GetValueOfAxis(DirectionAxis) > //comes from forward to back
            BackTrigger.gameObject.transform.position.GetValueOfAxis(DirectionAxis))
        {
          GoBack();
        }
      });

      ForwardTrigger.TriggerEnter.AddListener((otherCollider) =>
      {
        if (IsVoyager(otherCollider.gameObject) &&
            otherCollider.gameObject.transform.position.GetValueOfAxis(DirectionAxis) < //comes from back to forward 
            ForwardTrigger.gameObject.transform.position.GetValueOfAxis(DirectionAxis))
        {
          GoForward();
        }
      });
    }

    private void ReadCellsSizes()
    {
      foreach (GameObject cell in Cells)
      {
        cellsAndTheirSizes.Add(cell,cell.GetBounds().size);
        //cellsAndTheirCenters.Add(cell, cell.GetGeometricalCenter());
        //Debug.Log(String.Format("<color=blue>cell size: {0}</color>", cell.GetSize()), cell);

        //new GameObject("Geometric center: " + cell.name).transform.position = cell.GetGeometricalCenter();

      }
    }

    public float GetAllCellsLenght(Axis3D lineUpAxis)
    {
      float result = 0;
      foreach (GameObject cell in Cells)
      {
        result += cellsAndTheirSizes[cell].GetValueOfAxis(lineUpAxis);
      }
      return result;
    }

    private bool IsVoyager(GameObject obj)
    {
      return obj.GetInstanceID() == Voyager.gameObject.GetInstanceID();
    }

    private void MakeRoad()
    {
      Vector3 prevCellSize = cellsAndTheirSizes[Cells[0]];
      Vector3 prevCellPos = Cells[0].transform.position;

      for (int i = 1; i < Cells.Length; i++)
      {
        Vector3 curCellSize = cellsAndTheirSizes[Cells[i - 1]];
        //к позиции предыдущей ячейки добавить половину размера той же предыдущей ячейки
        Vector3 curCellPos = prevCellPos.AddToAxis(prevCellSize.GetValueOfAxis(DirectionAxis)/2, DirectionAxis);//конец предыдущей ячейки
        //к концу предыдущей ячейки добавить половину размера теперешней ячейки
        curCellPos = curCellPos.AddToAxis(curCellSize.GetValueOfAxis(DirectionAxis)/2, DirectionAxis);
        curCellPos = curCellPos.AddToAxis(CellsOverlapping, DirectionAxis);//добавить нахлест
        Cells[i].transform.position = curCellPos;
        prevCellSize = curCellSize;
        prevCellPos = curCellPos;
      }

      BackTrigger.transform.position = CalculateBackTriggerPos(Cells[0]);
      ForwardTrigger.transform.position = CalculateForwardTriggerPos(Cells[Cells.Length - 1]);
    }


    private Vector3 CalculateBackTriggerPos(GameObject firstCellFromBack)
    {
      Vector3 firstCellSize = cellsAndTheirSizes[firstCellFromBack];
      Vector3 backTriggerPos = firstCellFromBack.transform.position;
    
      backTriggerPos = backTriggerPos.SubFromAxis(firstCellSize.GetValueOfAxis(DirectionAxis)/2, DirectionAxis);//встать на начало первой ячейки
      backTriggerPos = backTriggerPos.AddToAxis(_triggerBorderIntend, DirectionAxis);//добавить сдвиг от начала ячеек
      backTriggerPos = backTriggerPos.AddToAxis(ForcelLocalTrigerShift, DirectionAxis);//добвить дополнительный локальный сдвиг
      return backTriggerPos;
    }

    private Vector3 CalculateForwardTriggerPos(GameObject firstCellFromForward)
    {
      Vector3 firstCellSize = cellsAndTheirSizes[firstCellFromForward];
      Vector3 forwardTriggerPos = firstCellFromForward.transform.position;

      forwardTriggerPos = forwardTriggerPos.AddToAxis(firstCellSize.GetValueOfAxis(DirectionAxis)/2, DirectionAxis);//встать в конец последней ячейки
      forwardTriggerPos = forwardTriggerPos.SubFromAxis(_triggerBorderIntend, DirectionAxis);//отнять сдвиг с конца ячеек
      forwardTriggerPos = forwardTriggerPos.AddToAxis(ForcelLocalTrigerShift, DirectionAxis);//добвить дополнительный локальный сдвиг

      return forwardTriggerPos;
    }

    /// <summary>
    /// передвинуть элемент с правого конца на левый
    /// </summary>
    public void GoBack()
    {
      Vector3 lastLeftCellPos = Cells[0].transform.position;
      Vector3 lastLeftCellSize = cellsAndTheirSizes[Cells[0]];
      Vector3 lastRightCellSize = cellsAndTheirSizes[Cells[Cells.Length - 1]];
      Vector3 newLastRightCellPos = lastLeftCellPos.SubFromAxis(lastLeftCellSize.GetValueOfAxis(DirectionAxis)/2,DirectionAxis);//левая граница (начало) первой ячейки слева
      //отнять половину размера сдвигаемой ячейки
      newLastRightCellPos = newLastRightCellPos.SubFromAxis(lastRightCellSize.GetValueOfAxis(DirectionAxis)/2, DirectionAxis);
      newLastRightCellPos = newLastRightCellPos.SubFromAxis(CellsOverlapping, DirectionAxis);//добавить нахлест

      //установить на полученую точку
      Cells[Cells.Length - 1].transform.position = newLastRightCellPos;

      Cells.Shift(1);//передвинуть ячейки в масиве вправо
      BackTrigger.transform.position = CalculateBackTriggerPos(Cells[0]);
      ForwardTrigger.transform.position = CalculateForwardTriggerPos(Cells[Cells.Length - 1]);
      CellChangedPosition.InvokeSafe(Cells[0]);
    }


    /// <summary>
    /// передвинуть элемент с левого конца на правый
    /// </summary>
    public void GoForward()
    {
      Vector3 lastRightCellPos = Cells[Cells.Length - 1].transform.position;
      Vector3 lastRightCellSize = cellsAndTheirSizes[Cells[Cells.Length - 1]];
      Vector3 lastLeftCellSize = cellsAndTheirSizes[Cells[0]];
      Vector3 newLastLeftCellPos = lastRightCellPos.AddToAxis(lastRightCellSize.GetValueOfAxis(DirectionAxis) / 2, DirectionAxis);//правая граница (конец) первой ячейки справа
      //добавить половину размера сдвигаемой ячейки
      newLastLeftCellPos = newLastLeftCellPos.AddToAxis(lastLeftCellSize.GetValueOfAxis(DirectionAxis) / 2, DirectionAxis);
      newLastLeftCellPos = newLastLeftCellPos.AddToAxis(CellsOverlapping, DirectionAxis);//отнять нахлест

      //установить на полученую точку
      Cells[0].transform.position = newLastLeftCellPos;

      Cells.Shift(-1); ;//передвинуть ячейки в масиве влево
      BackTrigger.transform.position = CalculateBackTriggerPos(Cells[0]);
      ForwardTrigger.transform.position = CalculateForwardTriggerPos(Cells[Cells.Length - 1]);
      CellChangedPosition.InvokeSafe(Cells[Cells.Length - 1]);
    }
  }
}