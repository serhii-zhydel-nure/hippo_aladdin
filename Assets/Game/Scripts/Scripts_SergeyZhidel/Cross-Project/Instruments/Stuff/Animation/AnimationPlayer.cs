﻿using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class AnimationPlayer : MonoBehaviour
  {
    public Animator Animator;

   
    public void PlayAnimation(string animName)
    {
      Animator.Play(animName);
    }
  }
}
