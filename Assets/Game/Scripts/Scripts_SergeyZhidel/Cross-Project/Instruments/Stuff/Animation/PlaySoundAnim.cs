﻿using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class PlaySoundAnim : MonoBehaviour
  {

    public void PlaySound(string soundName)
    {
      SoundEffectsPlayer.Play(soundName);
    }
  }
}
