﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class SmoothFollow : MonoBehaviour, IDisposable
  {
    public Transform TargetTransform;

    public Vector3 Intend = Vector3.zero;

    public Transform FollovedObjTransform;

    private Camera camera;

    public float dampTime = 0.15f;
    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
      camera = Camera.main;
      FollovedObjTransform = gameObject.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
      Vector3 targetPos = TargetTransform.position + Intend;
      Vector3 point = camera.WorldToViewportPoint(targetPos);
      Vector3 delta = targetPos - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
      Vector3 destination = transform.position + delta;
      transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
    }

    public void Dispose()
    {
      Destroy(this);
    }
  }
}