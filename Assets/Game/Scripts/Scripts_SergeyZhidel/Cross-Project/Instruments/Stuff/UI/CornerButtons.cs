﻿#if PSV_PROTOTYPE
using PSV;
#endif
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.ScenesManagment;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts_SergeyZhidel.UI
{
  public class CornerButtons : MonoBehaviour
  {
    bool _isHiden = true;

    void Awake()
    {
      gameObject.FindChild("ButtonSound").GetComponent<Button>().onClick.AddListener(() =>
      {
        SetSoundValue(!GameSettings.IsMusic.Value);
      });

      gameObject.FindChild("ButtonExit").GetComponent<Button>().onClick.AddListener(() =>
      {
        //if (ScenesFSM.CurrentSceneController.GetType() != typeof(GameScene0GameMap))
        //  ScenesFSM.LoadScene(PSV_Prototype.Scenes.GameMap);
        //else
        ScenesFSM.LoadScene(Scenes.MainMenu);
      });

      gameObject.FindChild("ButtonMenu").GetComponent<Button>().onClick.AddListener(() =>
      {
        if (_isHiden)
          GameObject.Find("Menu").GetComponent<Animator>().Play("ButtonMenu");
        else
          GameObject.Find("Menu").GetComponent<Animator>().Play("ButtonMenu_exit");
        _isHiden = !_isHiden;
      });

      SetSoundValue(GameSettings.IsMusic.Value);
    }

    void Start()
    {
      Canvas canvas = gameObject.transform.GetComponentInParent<Canvas>();
      canvas.renderMode = RenderMode.ScreenSpaceCamera;
      canvas.sortingLayerName = "UI";
      canvas.planeDistance = 10;
      canvas.sortingOrder = 10010;
      canvas.worldCamera = Camera.main;
    }


    private void SetSoundValue(bool value)
    {
      GameSettings.IsMusic.Value = value;
      gameObject.FindChild("SoundOn").GetComponent<Image>().enabled = value;
      gameObject.FindChild("SoundOff").GetComponent<Image>().enabled = !value;
    }
  }
}
