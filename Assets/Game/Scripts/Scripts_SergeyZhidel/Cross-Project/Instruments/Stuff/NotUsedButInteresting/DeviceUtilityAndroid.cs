﻿//namespace Kiloo.Plugins.DeviceUtility.Internal
//{
//    using JetBrains.Annotations;
//    using Kiloo.CommonScriptsContainer.Utility;
//    using System;
//    using System.Diagnostics;
//    using System.Runtime.CompilerServices;
//    using UnityEngine;

//    internal class DeviceUtilityAndroid : MonoBehaviour, IDeviceUtilityPlatformBridge
//    {
//        private AndroidJavaObject _androidBridge;
//        private const int ANDROID_BUTTON_NEGATIVE = -2;
//        private const int ANDROID_BUTTON_NEUTRAL = -3;
//        private const int ANDROID_BUTTON_POSITIVE = -1;
//        private const string CALLBACK_RECEIVER_NAME = "DeviceUtility Receiver";
//        private const string OS_IDENTIFIER = "android";
//        private const char PARAMETER_SEPARATOR = ';';

//        [UsedImplicitly]
//        private void _callback_alertViewDidDismissWithButtonIndex(string message)
//        {
//            try
//            {
//                char[] separator = new char[] { ';' };
//                string[] strArray = message.Split(separator);
//                if (strArray.Length == 2)
//                {
//                    string str = strArray[0].Trim();
//                    char[] trimChars = new char[] { ' ', '-' };
//                    string str2 = strArray[1].Trim(trimChars);
//                    char[] chArray = str.ToCharArray();
//                    char[] chArray2 = str2.ToCharArray();
//                    if ((chArray.Length == 1) && (chArray2.Length == 1))
//                    {
//                        char c = chArray[0];
//                        char ch2 = chArray2[0];
//                        double numericValue = char.GetNumericValue(c);
//                        double num2 = char.GetNumericValue(ch2);
//                        int ticket = (int) numericValue;
//                        int num4 = (int) num2;
//                        Action<int> action = Depository.CheckOut(ticket) as Action<int>;
//                        if (action != null)
//                        {
//                            action(num4 * -1);
//                        }
//                    }
//                    return;
//                }
//            }
//            catch (Exception)
//            {
//            }
//            LogError("Malformed dialog callback message");
//        }

//        [UsedImplicitly]
//        private void Awake()
//        {
//            this._androidBridge = new AndroidJavaObject("com.kiloo.unityplugin.deviceutility.DeviceUtilityBridgeAndroid", new object[0]);
//        }

//        public static DeviceUtilityAndroid CreatePlugin()
//        {
//            GameObject target = new GameObject("DeviceUtility Receiver");
//            UnityEngine.Object.DontDestroyOnLoad(target);
//            return target.AddComponent<DeviceUtilityAndroid>();
//        }

//        public string GetAndroidAdvertisingID()
//        {
//            return this._androidBridge.Call<string>("getAndroidAdvertisingID", new object[0]);
//        }

//        public int GetAndroidApiLevel()
//        {
//            return this._androidBridge.Call<int>("getAndroidApiLevel", new object[0]);
//        }

//        public string GetAndroidExternalSavePath()
//        {
//            return this._androidBridge.Call<string>("getAndroidExternalSavePath", new object[0]);
//        }

//        public int GetAndroidVersionCode()
//        {
//            return this._androidBridge.Call<int>("getAndroidVersionCode", new object[0]);
//        }

//        public int GetBatteryLevel()
//        {
//            return this._androidBridge.Call<int>("getBatteryLevel", new object[0]);
//        }

//        public string GetBuildIdentifier()
//        {
//            return (!this.isKindleDevice ? this.GetOSIdentifier() : "kindle");
//        }

//        public string GetBundleIdentifier()
//        {
//            return this._androidBridge.Call<string>("getBundleIdentifier", new object[0]);
//        }

//        public string GetBundleVersion()
//        {
//            return this._androidBridge.Call<string>("getBundleVersion", new object[0]);
//        }

//        public string GetCachePath()
//        {
//            string str = null;
//            str = this._androidBridge.Call<string>("getCachePath", new object[0]);
//            if (string.IsNullOrEmpty(str))
//            {
//                throw new InvalidOperationException("Could not get cache path");
//            }
//            return str;
//        }

//        public int GetCurrentMemoryUsage()
//        {
//            return this._androidBridge.Call<int>("getDeviceCurrentMemoryUsage", new object[0]);
//        }

//        public int GetDeviceFreeMemory()
//        {
//            return this._androidBridge.Call<int>("getDeviceFreeMemory", new object[0]);
//        }

//        public string GetDeviceIdentifier()
//        {
//            return this._androidBridge.Call<string>("getDeviceIdentifier", new object[0]);
//        }

//        public int GetFreeDiskSpace()
//        {
//            return this._androidBridge.Call<int>("getFreeDiskSpace", new object[0]);
//        }

//        public int GetFreeSDCardSpace()
//        {
//            return this._androidBridge.Call<int>("getFreeSDCardSpace", new object[0]);
//        }

//        public bool GetIOSOtherAudioPlaying()
//        {
//            LogWarning("GetIOSOtherAudioPlaying is not supported on this platform.");
//            return false;
//        }

//        public string GetLocale()
//        {
//            return this._androidBridge.Call<string>("getLocale", new object[0]);
//        }

//        public int GetMemoryUsageLimit()
//        {
//            return this._androidBridge.Call<int>("getDeviceMemoryLimit", new object[0]);
//        }

//        public string GetOSIdentifier()
//        {
//            return "android";
//        }

//        public string GetOSVersion()
//        {
//            return this._androidBridge.Call<string>("getOSVersion", new object[0]);
//        }

//        public int GetPeakMemoryUsage()
//        {
//            return 0;
//        }

//        public int GetTotalDiskSpace()
//        {
//            return this._androidBridge.Call<int>("getTotalDiskSpace", new object[0]);
//        }

//        public int GetTotalSDCardSpace()
//        {
//            return this._androidBridge.Call<int>("getTotalSDCardSpace", new object[0]);
//        }

//        public string GetUserDataPath()
//        {
//            string str = null;
//            str = this._androidBridge.Call<string>("getUserDataPath", new object[0]);
//            if (string.IsNullOrEmpty(str))
//            {
//                throw new InvalidOperationException("Could not get user data path");
//            }
//            return str;
//        }

//        [Conditional("DEVICE_UTILITY_DEBUG_LOGS")]
//        private static void Log(string msg)
//        {
//        }

//        private static void LogError(string msg)
//        {
//            SDebug.LogError("[DeviceUtilityAndroid] " + msg, null);
//        }

//        private static void LogWarning(string msg)
//        {
//        }

//        public void SetDeviceClipBoard(string text)
//        {
//            object[] args = new object[] { text };
//            this._androidBridge.Call("setDeviceClipBoard", args);
//        }

//        public void SetDeviceUtilityDebugEnabled(bool val)
//        {
//            this._androidBridge.Set<bool>("debugLogEnabled", val);
//        }

//        public void SetKeyboardReturnType(int returnKeyType)
//        {
//        }

//        public void ShowNativePopup(string title, string message, string cancelButton, string optionalButton1, string optionalButton2, Action<string> onDismissed)
//        {
//            <ShowNativePopup>c__AnonStorey0 storey = new <ShowNativePopup>c__AnonStorey0 {
//                onDismissed = onDismissed,
//                optionalButton1 = optionalButton1,
//                optionalButton2 = optionalButton2,
//                cancelButton = cancelButton
//            };
//            int num = 0;
//            if (storey.onDismissed != null)
//            {
//                Action<int> deposit = new Action<int>(storey.<>m__0);
//                num = Depository.CheckIn(deposit, this);
//            }
//            object[] args = new object[] { num, title, message, storey.cancelButton, storey.optionalButton1, storey.optionalButton2 };
//            this._androidBridge.Call("showNativePopup", args);
//        }

//        public bool hasPhysicalBackButton
//        {
//            get
//            {
//                return true;
//            }
//        }

//        public bool isFlexionDevice
//        {
//            get
//            {
//                return false;
//            }
//        }

//        public bool isKindleDevice
//        {
//            get
//            {
//                return false;
//            }
//        }

//        [CompilerGenerated]
//        private sealed class <ShowNativePopup>c__AnonStorey0
//        {
//            internal string cancelButton;
//            internal Action<string> onDismissed;
//            internal string optionalButton1;
//            internal string optionalButton2;

//            internal void <>m__0(int buttonPressedIndex)
//            {
//                switch ((buttonPressedIndex + 3))
//                {
//                    case 0:
//                        this.onDismissed(this.optionalButton2);
//                        return;

//                    case 2:
//                        this.onDismissed(this.optionalButton1);
//                        return;
//                }
//                this.onDismissed(this.cancelButton);
//            }
//        }
//    }
//}

