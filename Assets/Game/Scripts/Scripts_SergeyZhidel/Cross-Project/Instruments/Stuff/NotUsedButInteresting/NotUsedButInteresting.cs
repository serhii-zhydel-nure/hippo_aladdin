﻿using System.Collections;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class NotUsedButInteresting
  {
    IEnumerator SimulateThrowByCurve(Transform bullet, Vector3 start,  Vector3 target, float firingAngle = 45.0f, float gravity = 9.8f)
    {
      // Move projectile to the position of throwing object + add some offset if needed.
      bullet.position = start;

      // Calculate distance to target
      float target_Distance = Vector3.Distance(bullet.position, target);

      // Calculate the velocity needed to throw the object to the target at specified angle.
      float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

      // Extract the X  Y componenent of the velocity
      float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
      float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

      // Calculate flight time.
      float flightDuration = target_Distance / Vx;

      // Rotate projectile to face the target.
      bullet.rotation = Quaternion.LookRotation(target - bullet.position);

      float elapse_time = 0;

      while (elapse_time < flightDuration)
      {
        bullet.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

        elapse_time += Time.deltaTime;

        yield return null;
      }
    }
  }
}