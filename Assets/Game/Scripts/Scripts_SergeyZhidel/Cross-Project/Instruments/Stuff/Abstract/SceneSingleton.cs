﻿using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public abstract class SceneSingleton<T> : MonoBehaviour where T:MonoBehaviour
  {
    //C# property to retrieve currently active instance of object 
    public static T Instance
    {
      get
      {
        if (instance == null)
          //create game manager object if required 
          instance = new GameObject(typeof(T).Name).AddComponent<T>();
        return instance;
      }
    }
    //Internal reference to single active instance of object - for singleton behaviour 
    private static T instance;


    protected virtual void Awake()
    {
      //Check if there is an existing instance of this object 
      if (instance != null && instance.GetInstanceID() != GetInstanceID())
        DestroyImmediate(gameObject); //Delete duplicate 
      else
      {
        instance = gameObject.GetComponent<T>(); //Make this object the only instance 
        DontDestroyOnLoad(gameObject); //Set as do not destroyable 
      }
    }
  }

}