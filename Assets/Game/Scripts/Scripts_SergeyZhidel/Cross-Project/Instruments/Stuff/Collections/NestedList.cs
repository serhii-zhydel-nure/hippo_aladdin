﻿using PSV;
using System;
using System.Collections;
using System.Collections.Generic;


namespace Scripts_SergeyZhidel
{
  /// <summary>
  /// Класс для вывода масива масивов или листа листов.
  /// Для использования заменить Scenes на то, что нужно выводить
  /// </summary>
  [Serializable]
  public class NestedList :IList<Scenes>
  {
    public List<Scenes> List;

    public NestedList(IEnumerable<Scenes> listToView)
    {
      List = new List<Scenes>(listToView);
    }

    public int IndexOf(Scenes item)
    {
      return List.IndexOf(item);
    }

    public void Insert(int index, Scenes item)
    {
      List.Insert(index, item);
    }

    public void RemoveAt(int index)
    {
      List.RemoveAt(index);
    }

    public Scenes this[int index]
    {
      get { return List[index]; }
      set { List[index] = value; }
    }

    public IEnumerator<Scenes> GetEnumerator()
    {
      return List.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    public void Add(Scenes item)
    {
      List.Add(item);
    }

    public void Clear()
    {
      List.Clear();
    }

    public bool Contains(Scenes item)
    {
      return List.Contains(item);
    }

    public void CopyTo(Scenes[] array, int arrayIndex)
    {
      List.CopyTo(array, arrayIndex);
    }

    public bool Remove(Scenes item)
    {
      return List.Remove(item);
    }

    public int Count { get { return List.Count; } }
    public bool IsReadOnly { get { return false; } }
  }
}