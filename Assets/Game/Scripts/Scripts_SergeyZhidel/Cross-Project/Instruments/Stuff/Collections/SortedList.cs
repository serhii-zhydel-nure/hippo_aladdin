﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Scripts_SergeyZhidel
{
  [Serializable]
  public class SortedList<T> : ICollection<T> where T : IComparable<T>
  {
    private readonly List<T> _collection = new List<T>();

    public int Count { get { return _collection.Count; } }
    public bool IsReadOnly { get { return false; } }

    public SortedList()
    { }

    public SortedList(int capasity)
    {
      _collection = new List<T>(capasity);
    }

    public SortedList(IList<T> itemList) : this()
    {
      _collection = new List<T>(itemList);
      Sort();
    }

    public T ElementAt(int index)
    {
      return _collection[index];
    }

    public void Add(T item)
    {
      if (Count == 0)
      {
        _collection.Add(item);
        return;
      }
      int minimum = 0;
      int maximum = _collection.Count - 1;

      while (minimum <= maximum)
      {
        int midPoint = (minimum + maximum) / 2;
        int comparison = _collection[midPoint].CompareTo(item);
        if (comparison == 0)
        {
          //   throw new ArgumentException("Dublicate items.");
          return; // already in the list, do nothing
        }
        if (comparison < 0)
          minimum = midPoint + 1;
        else
          maximum = midPoint - 1;
      }
      _collection.Insert(minimum, item);
    }

    /// <summary>
    /// updates item position asn returns its index
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public int Update(T item)
    {
      if (Contains(item))
        Sort();
      else
        Add(item);

      return IndexOfItem(item);
    }

    public int IndexOfItem(T item)
    {
      return _collection.IndexOf(item);
    }

    public bool Contains(T item)
    {
      // TODO: potential optimization
      return _collection.Contains(item);
    }

    public bool Remove(T item)
    {
      // TODO: potential optimization
      return _collection.Remove(item);
    }

    public IEnumerator<T> GetEnumerator()
    {
      return _collection.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    public void Clear()
    {
      _collection.Clear();
    }

    private void Sort()
    {
      _collection.Sort((T obj1, T obj2) => obj1.CompareTo(obj2));
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
      _collection.CopyTo(array, arrayIndex);
    }
  }
}