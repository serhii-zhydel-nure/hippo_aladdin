﻿using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  ////Проигрыватель речи
  /// </summary>
  public class SpeechPlayer : AudioPlayer
  {
    //C# property to retrieve currently active _instance of object 
    public static SpeechPlayer Instance
    {
      get
      {
        if (_instance == null)
        {
          //create game manager object if required 
          _instance = GameManager.Instance.gameObject.CreateChild("SpeechPlayer").AddComponent<SpeechPlayer>();
          _instance._source.volume = GameSettings.SoundsVol;
          GameSettings.SoundsVol.OnValueChanged += newVolume => _instance._source.volume = newVolume;
        }
        return _instance;
      }
    }
    //Internal reference to single active instance of object - for singleton behaviour 
    private static SpeechPlayer _instance;

    protected override AudioClip FindClip(string soundName)
    {
      return ResourcesFolder.GetSpeech(soundName);
    }

 }
}