﻿using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  /// Проигрыватель звукых эфектов типа чтото упало, разлилось, треснуло..
  /// </summary>
  public class SoundEffectsPlayer : AudioPlayer
  {
    public static SoundEffectsPlayer Instance
    {
      get
      {
        if (_instance == null)
        {
          _instance = GameManager.Instance.gameObject.CreateChild("SoundEffectsPlayer").AddComponent<SoundEffectsPlayer>();
          _instance._source.volume = GameSettings.SoundsVol;
          GameSettings.SoundsVol.OnValueChanged += newVolume => _instance._source.volume = newVolume;
          GameSettings.IsSound.OnValueChanged += isSoundOn => _instance._source.mute = !isSoundOn;
        }
        return _instance;
      }
    }

    private static SoundEffectsPlayer _instance;



    public new static void Play(string sound)
    {
      ((AudioPlayer)Instance).Play(sound);
      _instance._source.loop = false;
    }


    public new static void Pause()
    {
      ((AudioPlayer)Instance).Pause();
    }


    protected override AudioClip FindClip(string soundName)
    {
      return ResourcesFolder.GetTracks(soundName);
    }
  }
}