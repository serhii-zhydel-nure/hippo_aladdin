﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  
  public abstract class AudioPlayer:MonoBehaviour
  {
    public bool IsPlaying { get { return _source.isPlaying; } }


    protected AudioSource _source;

    protected virtual void Awake()
    {
      _source = gameObject.AddComponent<AudioSource>();
      _source.volume = 0.5f;
      GameSettings.IsSound.OnValueChanged += isSoundOn => _source.mute = !isSoundOn;
    }


    public virtual void Play(AudioClip sound)
    {
      Stop();
      _source.loop = false;
      _source.clip = sound;
      _source.Play();
    }


    public virtual void Play(string soundName)
    {
      AudioClip clip = FindClip(soundName);

      if (clip == null)
        throw new ArgumentException(soundName);

      Play(clip);
    }

    public virtual void PlayLoop(string soundName)
    {
      Play(soundName);
      _source.loop = true;
    }


    public virtual void Pause()
    {
      _source.Pause();
    }

    public virtual void Stop()
    {
      _source.Stop();
      _source.clip = null;
    }


    public virtual bool IsPlayingSound(string soundName)
    {
      return FindClip(soundName) != null && _source.clip == FindClip(soundName) && IsPlaying;
    }


    protected abstract AudioClip FindClip(string soundName);
  }
}