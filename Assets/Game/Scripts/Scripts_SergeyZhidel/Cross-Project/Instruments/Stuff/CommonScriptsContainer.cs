﻿using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  /// Общий обьект на сцене, чтоб на него разные временные скрипты вешать
  /// </summary>
  public static partial class CommonScriptsContainer
  {
    public static GameObject Instance
    {
      get
      {
        if (_scriptsCommonContainer == null)
          _scriptsCommonContainer = new GameObject("CommonScriptsContainer");

        return _scriptsCommonContainer;
      }
    }

    private static GameObject _scriptsCommonContainer;
  }
  
}