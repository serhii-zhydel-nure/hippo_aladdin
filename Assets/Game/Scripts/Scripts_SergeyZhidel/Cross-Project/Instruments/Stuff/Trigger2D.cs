﻿using System;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using UnityEngine.Events;

namespace Scripts_SergeyZhidel
{
  [RequireComponent(typeof(Collider2D))]
  public class Trigger2D : MonoBehaviour
  {
    public Trigger2DEvent TriggerEnter;
    public Trigger2DEvent TriggerStay;
    public Trigger2DEvent TriggerExit;

    void Start()
    {
      GetComponent<Collider2D>().isTrigger = true;
      if (GetComponent<Rigidbody2D>() != null)
        GetComponent<Rigidbody2D>().isKinematic = true;

      if(TriggerEnter == null)
        TriggerEnter = new Trigger2DEvent();

      if (TriggerStay == null)
        TriggerStay = new Trigger2DEvent();

      if (TriggerExit == null)
        TriggerExit = new Trigger2DEvent();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
      TriggerEnter.Invoke(other);
    }

    void OnTriggerStay2D(Collider2D other)
    {
      TriggerStay.Invoke(other);
    }

    void OnTriggerExit2D(Collider2D other)
    {
      TriggerExit.Invoke(other);
    }
  }

  [Serializable]
  public class Trigger2DEvent : UnityEvent<Collider2D> { }
}