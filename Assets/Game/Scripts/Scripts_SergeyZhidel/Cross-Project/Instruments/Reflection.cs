﻿using System;
using System.Linq;
using System.Reflection;

namespace Scripts_SergeyZhidel
{
  public class Reflection
  {
    public static TInstanceType CreateInstance<TInstanceType>()
    {
      return (TInstanceType)Activator.CreateInstance(typeof(TInstanceType));
    }

    public static TInstanceType CreateInstance<TInstanceType, TParam1>(TParam1 param1)
    {
      return (TInstanceType)Activator.CreateInstance(typeof(TInstanceType), param1);
    }

    public static TInstanceType CreateInstance<TInstanceType, TParam1, TParam2>(TParam1 param1, TParam2 param2)
    {
      return (TInstanceType)Activator.CreateInstance(typeof(TInstanceType), param1, param2);
    }

    public static TInstanceType CreateInstance<TInstanceType, TParam1, TParam2, TParam3>(TParam1 param1, TParam2 param2, TParam3 param3)
    {
      return (TInstanceType)Activator.CreateInstance(typeof(TInstanceType), param1, param2, param3);
    }

    public static T[] GetInheritorsInstances<T>() where T : class
    {
      return (from asm in AppDomain.CurrentDomain.GetAssemblies()
              from type in asm.GetTypes()
              where type.IsClass && type.IsSubclassOf(typeof(T))
              select Activator.CreateInstance(type) as T).ToArray();
    }

    public static Type[] GetInheritorsTypes<T>() where T : class
    {
      Type[] allTypes =
        AppDomain.CurrentDomain.GetAssemblies()
          .SelectMany(asm => asm.GetTypes().Where(type => type.IsClass && type.IsSubclassOf(typeof(T))))
          .ToArray();

      return allTypes;
    }

    public static Type[] GetInheritorsTypes(Type checkedType)
    {
      return (from asm in AppDomain.CurrentDomain.GetAssemblies()
              from type in asm.GetTypes()
              where type.IsClass && type.IsSubclassOf(checkedType)
              select type).ToArray();
    }

    public static TValueType GetPrivateField<TObjType, TValueType>(TObjType obj, string fieldName)
    {
     return (TValueType) 
        typeof(TObjType)//у класа
                        //запросить приватное поле
        .GetField(fieldName, BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance)
        .GetValue(obj);//вернуть его значение из этого обьекта
    }
  }
}