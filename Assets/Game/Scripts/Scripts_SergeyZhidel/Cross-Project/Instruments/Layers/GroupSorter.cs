﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  [DisallowMultipleComponent]
  public class GroupSorter : MonoBehaviour
  {
    private static readonly Dictionary<Renderer, int> _defaultRenderersOrders = new Dictionary<Renderer, int>();
    private static readonly Dictionary<RendererOrderSetter, int> _defaultTextsOrders = new Dictionary<RendererOrderSetter, int>();

    [Tooltip("Определяется пользователем")]
    [SerializeField]
    private int _groupAdditionalOrder;
    public int GroupAdditionalOrder
    {
      get { return _groupAdditionalOrder; }
      set
      {
        _groupAdditionalOrder = value;
        UpdateLayer();
      }
    }

    [Tooltip("Накидуется другими сортировщиками (например HeightLayerSorter) из родительских обьектов")]
    public int ExternalAdditionalOrder = 0;

    [Tooltip("Итого получается слой")]
    public int CalculatedOreder;

    [Tooltip("Как часто проверять изменились ли параметры скрипта")]
    public float ParamsUpdateTime = 0.3f;

    //void OnDestroy()
    //{
    //  foreach (KeyValuePair<Renderer, int> rendererAndDefaultOrder in _defaultRenderersOrders)
    //  {
    //    if (rendererAndDefaultOrder.Key != null)
    //      rendererAndDefaultOrder.Key.sortingOrder = rendererAndDefaultOrder.Value;
    //  }
    //  foreach (KeyValuePair<RendererOrderSetter, int> textAndDefaultOrder in _defaultTextsOrders)
    //  {
    //    if (textAndDefaultOrder.Key != null)
    //      textAndDefaultOrder.Key.SortingOrder = textAndDefaultOrder.Value;
    //  }
    //}

    protected virtual void Start()
    {
      UpdateLayer();
    }

    protected void OnEnable()
    {
      StartCoroutine(SorterParamChecker());
    }

    public void UpdateLayer()
    {
      CalculatedOreder = GroupAdditionalOrder + ExternalAdditionalOrder;
      SetOrderRecursive(this, gameObject, CalculatedOreder);
    }

    void SetOrderRecursive(GroupSorter beginSorter, GameObject obj, int orderNumber)
    {
      GroupSorter layerWorker = obj.GetComponent<GroupSorter>();

      // is under controll of other layer controller 
      if (layerWorker != null && layerWorker != beginSorter)
        return;

      SetOrderToTheAllSortableComponents(obj, orderNumber);

      //set order for children
      foreach (Transform child in obj.transform)
        SetOrderRecursive(beginSorter, child.gameObject, orderNumber);
    }

    private void SetOrderToTheAllSortableComponents(GameObject obj, int orderNumber)
    {
      foreach (RendererOrderSetter meshOrderSetter in obj.GetComponents<RendererOrderSetter>())
      {
        if (!_defaultTextsOrders.ContainsKey(meshOrderSetter))
          _defaultTextsOrders.Add(meshOrderSetter, meshOrderSetter.SortingOrder);

        meshOrderSetter.SortingOrder = _defaultTextsOrders[meshOrderSetter] + orderNumber;
      }

      foreach (SpriteRenderer renderer in obj.GetComponents<SpriteRenderer>())
      {
        if (!_defaultRenderersOrders.ContainsKey(renderer))
          _defaultRenderersOrders.Add(renderer, renderer.sortingOrder);

        renderer.sortingOrder = _defaultRenderersOrders[renderer] + orderNumber;
      }
    }

    //works only in Debug mode
    private IEnumerator SorterParamChecker()
    {
      int lastAdditionalOrder = GroupAdditionalOrder;
      int lastExternalAdditionalOrder = ExternalAdditionalOrder;

      while (true)
      {
        if (GroupAdditionalOrder != lastAdditionalOrder || lastExternalAdditionalOrder != ExternalAdditionalOrder)
        {
          lastAdditionalOrder = GroupAdditionalOrder;
          lastExternalAdditionalOrder = ExternalAdditionalOrder;
          UpdateLayer();
          yield return new WaitForSeconds(ParamsUpdateTime);
        }
        yield return new WaitForSeconds(ParamsUpdateTime);
      }
    }
  }
}