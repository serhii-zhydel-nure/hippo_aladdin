﻿using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class RendererOrderSetter : MonoBehaviour
  {
    public int InspectorsSortingOrder = -1;

    public int SortingOrder
    {
      get { return gameObject.GetComponent<Renderer>().sortingOrder; }
      set
      {
        InspectorsSortingOrder = value;
        gameObject.GetComponent<Renderer>().sortingOrder = value;
      }
    }

    // Use this for initialization
    void Awake()
    {
      if (InspectorsSortingOrder != -1)
      {
        SortingOrder = InspectorsSortingOrder;
      }
    }

#if UNITY_EDITOR
    private int _prevInspectorsSortingOrder = -1;
    void Update()
    {
//      if (InspectorsSortingOrder != _prevInspectorsSortingOrder)
//      {
//        _prevInspectorsSortingOrder = InspectorsSortingOrder;
//        SortingOrder = InspectorsSortingOrder;
//      }
    }
#endif
  }
}
