﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Scripts_SergeyZhidel;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  [Serializable]
  public class LayerSorter : MonoBehaviour, ILayerWorker, IComparable<LayerSorter>
  {
    private const int OrderRangeBetweenIndexInContainer = 100;

    protected static readonly SortedList<LayerSorter> Container = new SortedList<LayerSorter>();
    private static readonly Dictionary<UnityEngine.Object, int> _defaultSpritesOrders = new Dictionary<UnityEngine.Object, int>();

    public bool UsePrioritySorting = true;

    public int Priority = 1;

    public int CalculatedOreder = 0;

    public int AdditionalOrder
    {
      get { return _additionalOrder; }
      set
      {
        _additionalOrder = value;
        ForcedUpdate();
      }
    }

    [SerializeField]
    private int _additionalOrder = 0;

    public float HeightPosition;

    private int _containerIndex = -1;

    public float PositionUpdateTime = 0.4f;
    public float ParamsUpdateTime = 0.4f;

    public string Name;

    public bool _containerContainsThisSorter = false;

    void OnDestroy()
    {
      foreach (SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer>())
      {
        if (_defaultSpritesOrders.ContainsKey(sprite))
        {
          sprite.sortingOrder = _defaultSpritesOrders[sprite];
          _defaultSpritesOrders.Remove(sprite);
        }
      }
    }

    protected virtual void Start()
    {
      Name = gameObject.name;
      HeightPosition = transform.localPosition.y;
      UpdateLayer();

      UpdateRestLayers();
    }

    protected void OnEnable()
    {
      StartCoroutine(PositionChecker());

      StartCoroutine(SorterParamChecker());
    }

    protected void OnDisable()
    {
      Container.Remove(this);
      UpdateRestLayers();
    }

    private static void UpdateRestLayers()
    {
      SortedList<LayerSorter> temporaryList = new SortedList<LayerSorter>(Container.ToList());
      foreach (LayerSorter sorter in temporaryList)
        sorter.UpdateLayer();
    }

    private void UpdateLayer()
    {
      if (UsePrioritySorting)
      {
        int index = Container.Update(this);
        _containerContainsThisSorter = true;

        if (_containerIndex != index)//need update
        {
          _containerIndex = index;
          CalculatedOreder = _containerIndex * OrderRangeBetweenIndexInContainer + AdditionalOrder;
          SetOrderRecursive(this, gameObject, CalculatedOreder);
        }
      }
      else
      {
        if (_containerContainsThisSorter)
        {
          Container.Remove(this);
          _containerContainsThisSorter = false;
          UpdateRestLayers();
        }
        CalculatedOreder = AdditionalOrder;
        SetOrderRecursive(this, gameObject, CalculatedOreder);
      }
    }

    public void ForcedUpdate()
    {
      if (UsePrioritySorting)
      {
        int index = Container.Update(this);
        _containerContainsThisSorter = true;

        if (_containerIndex != index)//need update
        {
          _containerIndex = index;
          CalculatedOreder = _containerIndex * OrderRangeBetweenIndexInContainer + AdditionalOrder;
          SetOrderRecursive(this, gameObject, CalculatedOreder);
        }
      }
      else
      {
        if (_containerContainsThisSorter)//если ранее использовал сортировку по приоритету, а теперь перестал
        {
          Container.Remove(this);
          _containerContainsThisSorter = false;
          UpdateRestLayers();
        }
        CalculatedOreder = AdditionalOrder;
        SetOrderRecursive(this, gameObject, CalculatedOreder);
      }
      UpdateRestLayers();
    }

    void SetOrderRecursive(LayerSorter beginSorter, GameObject obj, int orderNumber)
    {
      ILayerWorker layerWorker = obj.GetComponent<ILayerWorker>();

      // is under controll of other layer controller 
      if (layerWorker != null && layerWorker != beginSorter)
        return;

      SetOrderToTheAllSortableComponents(obj, orderNumber);

      //set order for children
      foreach (Transform child in obj.transform)
        SetOrderRecursive(beginSorter, child.gameObject, orderNumber);
    }

    private void SetOrderToTheAllSortableComponents(GameObject obj, int orderNumber)
    {
      foreach (RendererOrderSetter meshOrderSetter in obj.GetComponents<RendererOrderSetter>())
      {
        if (!_defaultSpritesOrders.ContainsKey(meshOrderSetter))
          _defaultSpritesOrders.Add(meshOrderSetter, meshOrderSetter.SortingOrder);

        meshOrderSetter.SortingOrder = _defaultSpritesOrders[meshOrderSetter] + orderNumber;
      }

			foreach (SpriteRenderer renderer in obj.GetComponents<SpriteRenderer>())
      {
        if (!_defaultSpritesOrders.ContainsKey(renderer))
          _defaultSpritesOrders.Add(renderer, renderer.sortingOrder);

        renderer.sortingOrder = _defaultSpritesOrders[renderer] + orderNumber;
      }
    }

    //works only in Debug mode
    private IEnumerator SorterParamChecker()
    {
      int lastPriority = Priority;
      int lastAdditionalOrder = AdditionalOrder;
      bool lastUsePriorityValue = UsePrioritySorting;

      while (true)
      {
        if (Priority != lastPriority || AdditionalOrder != lastAdditionalOrder || lastUsePriorityValue != UsePrioritySorting)//object position changed
        {
          lastPriority = Priority;
          lastAdditionalOrder = AdditionalOrder;
          lastUsePriorityValue = UsePrioritySorting;
          ForcedUpdate();
          yield return new WaitForSeconds(ParamsUpdateTime);
        }
        yield return new WaitForSeconds(ParamsUpdateTime);
      }
    }

    private IEnumerator PositionChecker()
    {
      while (true)
      {
        if (UsePrioritySorting && transform.hasChanged && HeightPosition != transform.position.y)//object position changed
        {
          int lastIndex = Container.IndexOfItem(this);

          HeightPosition = transform.position.y;

          int currentIndex = Container.Update(this);
          _containerContainsThisSorter = true;

          if (lastIndex != currentIndex)//position changed significantly
          {
            int minIndex = Math.Min(lastIndex, currentIndex);
            int maxIndex = Math.Max(lastIndex, currentIndex);
            
            SortedList<LayerSorter> temporaryList = new SortedList<LayerSorter>(Container.ToList());

            //recalculate all affected Container
            for (int i = 0; i < Container.Count; i++)
            {
              if (i >= minIndex && i <= maxIndex)
                temporaryList.ElementAt(i).UpdateLayer();
            }
          }

          yield return new WaitForSeconds(PositionUpdateTime);
        }
        yield return new WaitForSeconds(PositionUpdateTime);
      }
    }

    public int CompareTo(LayerSorter other)
    {
      if (Priority == other.Priority)
        return HeightPosition.CompareTo(other.HeightPosition) * -1;
      else
        return Priority.CompareTo(other.Priority);
    }

    public override bool Equals(object obj)
    {
      if (obj == null || GetType() != obj.GetType())
        return false;

      LayerSorter p = (LayerSorter)obj;
      return GetInstanceID() == p.GetInstanceID();
    }

    public override int GetHashCode()
    {
      return GetInstanceID().GetHashCode();
    }

    public override string ToString()
    {
      return string.Format("HeightPosition: {1}, Priority: {2}", HeightPosition, Priority);
    }

  }
}