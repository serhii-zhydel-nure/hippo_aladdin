﻿using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

namespace Scripts_SergeyZhidel
{
  public static class Rand
  {
    /// <summary>
    /// Вернуть true с вероятностью percentChance
    /// </summary>
    /// <param name="percentChance">from 0f to 1f</param>
    /// <returns></returns>
    public static bool Chance(float percentChance)
    {
      return Range(0f, 1f) <= percentChance;
    }

    public static bool RandomBool()
    {
      return Chance(0.5f);
    }

    public static int RandomInt()
    {
      return Range(int.MinValue, int.MaxValue);
    }

    public static float RandomFloat()
    {
      return Range(float.MinValue, float.MaxValue);
    }

    public static int Range(int min, int max)
    {
      return Random.Range(min, max);
    }

    public static float Range(float min, float max)
    {
      return Random.Range(min, max);
    }

    public static T GetRandomItem<T>(IEnumerable<T> enumerable)
    {
      return enumerable.ElementAt(Range(0, enumerable.Count()));
    }

    public static T GetRandomItem<T>(T[] array)
    {
      return array[Range(0, array.Length)];
    }

    public static T GetRandomItem<T>(IList<T> list)
    {
      return list[Range(0, list.Count)];
    }

    public static T GetRandomEnum<T>(params T[] exceptList)
    {
      T[] rest = ((T[])Enum.GetValues(typeof(T))).Except(exceptList).ToArray();
      return rest.ElementAt(Range(0, rest.Length));
    }

    public static T[] Shuffle<T>(T[] array)
    {
      int length = array.Length;
      T[] result = new T[length];
      array.CopyTo(result, 0);
      while (length > 0)
      {
        int randIndex = Range(0, length);
        T randItem = result[length - 1];
        result[length - 1] = result[randIndex];
        result[randIndex] = randItem;
        length--;
      }
      return result;
    }

    public static IEnumerable<T> Shuffle<T>(IEnumerable<T> enumerable)
    {
      return Shuffle(enumerable);
    }
  }
}