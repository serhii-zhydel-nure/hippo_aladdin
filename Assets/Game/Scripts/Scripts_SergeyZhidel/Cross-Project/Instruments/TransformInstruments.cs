﻿using System;
using System.Collections;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class TransformInstruments
  {
    public static IEnumerator PulseEternally(GameObject obj)
    {
      Vector3 beforePulseSize = obj.transform.localScale;

       Tuple<Vector3, float>[] pairsSizeAndSeconds = {
          new Tuple<Vector3, float>(beforePulseSize*1.1f, 0.5f),
          new Tuple<Vector3, float>(beforePulseSize*1f, 0.5f),
          new Tuple<Vector3, float>(beforePulseSize*0.9f, 0.5f),
          new Tuple<Vector3, float>(beforePulseSize*1f, 0.5f)
        };

      while (true)
      {
        foreach (Tuple<Vector3, float> pair in pairsSizeAndSeconds)
        {
          yield return obj.transform.ScaleOverSeconds(pair.Item1, pair.Item2);
        }
        
      }
    }

    public static IEnumerator PulseForTime(GameObject obj, float seconds)
    {
      Vector3 beforePulseSize = obj.transform.localScale;
      Tuple<Vector3, float>[] pairsSizeAndSeconds =
        {
          new Tuple<Vector3, float>(beforePulseSize*1.1f, 0.5f),
          new Tuple<Vector3, float>(beforePulseSize*1f, 0.5f),
          new Tuple<Vector3, float>(beforePulseSize*0.9f, 0.5f),
          new Tuple<Vector3, float>(beforePulseSize*1f, 0.5f)
        };

      float pulsedTime = 0;

      while (true)
      {
        foreach (Tuple<Vector3, float> pair in pairsSizeAndSeconds)
        {
          float elapsedTime = 0;
          Vector3 startingScale = obj.transform.localScale;
          while (elapsedTime < pair.Item2 && pulsedTime < seconds)
          {
            obj.transform.localScale = Vector3.Lerp(startingScale, pair.Item1, elapsedTime / pair.Item2);
            elapsedTime += Time.deltaTime;
            pulsedTime += Time.deltaTime;
            yield return null;
          }

          if (pulsedTime >= seconds)
          {
            obj.transform.localScale = beforePulseSize;
            yield break;
          }
          obj.transform.localScale = pair.Item1;
        }
      }

    }

    public static IEnumerator PulseOnce(GameObject obj)
    {
      Vector3 beforePulseSize = obj.transform.localScale;
      yield return obj.transform.PulseSize(new Tuple<Vector3, float>(beforePulseSize*1.2f, 0.2f), new Tuple<Vector3, float>(beforePulseSize*1, 0.2f), new Tuple<Vector3, float>(beforePulseSize*0.8f, 0.2f), new Tuple<Vector3, float>(beforePulseSize*1, 0.2f));
    }

  }
}