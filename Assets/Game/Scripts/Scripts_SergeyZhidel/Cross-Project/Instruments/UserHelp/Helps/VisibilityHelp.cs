﻿using System.Collections;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{

  /// <summary>
  /// Показывать\прятать обьект
  /// </summary>
  public class VisibilityHelp : MonoBehaviour, IUserHepler
  {
    public PopUpStatus HelpStatus { get; private set; }

    void Awake()
    {
      HelpStatus = PopUpStatus.Opened;
    }

    public void GiveHelpToUser()
    {
      if (HelpStatus == PopUpStatus.Hiden || HelpStatus == PopUpStatus.Hiding)
      {
        StopAllCoroutines();
        HelpStatus = PopUpStatus.Opening;
        //показать меню с контентом
        StartCoroutine(ShowObj()
          .Append(() => HelpStatus = PopUpStatus.Opened));
      }
    }

    public void TakeAwayHelpFromUser()
    {
      if (HelpStatus == PopUpStatus.Opened || HelpStatus == PopUpStatus.Opening)
      {
        StopAllCoroutines();
        HelpStatus = PopUpStatus.Hiding;
        StartCoroutine(HideObj()
          .Append(() => HelpStatus = PopUpStatus.Hiden));
      }
    }

    public void ForcedTakeAwayHelpFromUser()
    {
      StopAllCoroutines();
      HelpStatus = PopUpStatus.Hiding;
      StartCoroutine(HideObj(0)
        .Append(() => HelpStatus = PopUpStatus.Hiden));
    }

    private IEnumerator HideObj(float seconds = 1f)
    {
      yield return gameObject.TransparentOverSeconds(0, seconds);
    }

    private IEnumerator ShowObj(float seconds = 1f)
    {
      yield return gameObject.TransparentOverSeconds(1, seconds);
    }
  }
}