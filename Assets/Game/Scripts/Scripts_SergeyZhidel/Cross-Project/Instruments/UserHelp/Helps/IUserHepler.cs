﻿
namespace Scripts_SergeyZhidel

{
  public interface IUserHepler
  {
    PopUpStatus HelpStatus { get; }

    void GiveHelpToUser();

    void TakeAwayHelpFromUser();
  }

  public enum PopUpStatus
  {
    Opened, Hiding, Hiden, Opening
  }
}