﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public class TalkPhraseHelp : MonoBehaviour, IUserHepler
  {
    public List<String> PhrasesToTalk = new List<string>();

    public PopUpStatus HelpStatus { get; private set; }

    public SoundBehavior Speaker;
    private bool _wasSpeakerCreated;
    public float RepeatInterval = 10;//seconds


    private void Start()
    {
      HelpStatus = PopUpStatus.Hiden;
      if (Speaker == null)
      {
        Speaker = gameObject.AddComponent<SoundBehavior>();
        _wasSpeakerCreated = true;
      }
    }

    public void GiveHelpToUser()
    {
      if (HelpStatus == PopUpStatus.Hiden)
      {
        HelpStatus = PopUpStatus.Opened;
        StartCoroutine(Help());
      }
    }

    public void TakeAwayHelpFromUser()
    {
      if (HelpStatus == PopUpStatus.Opened)
      {
        StopAllCoroutines();
        Speaker.StopTalking();
        HelpStatus = PopUpStatus.Hiden;
      }
    }

    private IEnumerator Help()
    {
      while (true)
      {
        yield return StartCoroutine(Speaker.Talk(PhrasesToTalk.ToArray()));
        yield return new WaitForSeconds(RepeatInterval);

      }
    }

    void OnDestroy()
    {
      if (_wasSpeakerCreated)
        Destroy(Speaker);
    }
  }
}