﻿using System.Collections.Generic;
using System.Linq;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  /// Контролирует обьекты, которые должны пульсировать
  /// </summary>
  public class PulsingHelp : MonoBehaviour, IUserHepler
  {
    public PopUpStatus HelpStatus { get; private set; }

    //parcel & pulsing
    Dictionary<MonoBehaviour, Pulse> _stuffPulsing = new Dictionary<MonoBehaviour, Pulse>();

    private bool _isNowPulsing;

    void Awake()
    {
      HelpStatus = PopUpStatus.Hiden;
    }

    void OnEnable()
    {
      if (_isNowPulsing)
        StartPulsing();
    }

    public void StartPulsing()
    {
      HelpStatus = PopUpStatus.Opening;

      _isNowPulsing = true;

      foreach (MonoBehaviour stuff in new Dictionary<MonoBehaviour, Pulse>(_stuffPulsing).Keys)
      {
        RunPulse(stuff);
      }

      HelpStatus = PopUpStatus.Opened;
    }

    private void RunPulse(MonoBehaviour stuff)
    {
      Pulse pulse = stuff.gameObject.AddComponent<Pulse>();

      if (!_stuffPulsing.ContainsKey(stuff))
        _stuffPulsing.Add(stuff, pulse);
      else
        _stuffPulsing[stuff] = pulse;
    }

    public void RemoveFromPulsing(MonoBehaviour stuff)
    {
      if (_isNowPulsing)
        Destroy(_stuffPulsing[stuff]);

      _stuffPulsing.Remove(stuff);
    }

    public void StopPulsing()
    {
      HelpStatus = PopUpStatus.Hiding;

      foreach (KeyValuePair<MonoBehaviour, Pulse> stuffAndCoroutinePair in _stuffPulsing)
        Destroy(stuffAndCoroutinePair.Value);

      foreach (KeyValuePair<MonoBehaviour, Pulse> stuffAndCoroutinePair in _stuffPulsing)
      {
        if (stuffAndCoroutinePair.Key.isActiveAndEnabled && stuffAndCoroutinePair.Value != null)
        {
          stuffAndCoroutinePair.Key.StartCoroutine(stuffAndCoroutinePair.Key.transform.ScaleOverSeconds(stuffAndCoroutinePair.Value.BeforePulseSize, 0.5f));//вернуть в начальное состояние
}
        else
        {
          stuffAndCoroutinePair.Key.transform.localScale = Vector3.one;
        }
      }

      _isNowPulsing = false;

      HelpStatus = PopUpStatus.Hiden;
    }

    public bool ContainsStuff(MonoBehaviour stuff)
    {
      return _stuffPulsing.Keys.Any(food => food.Equals(stuff));
    }

    public void AddStuffToPulse(MonoBehaviour stuff)
    {
      if (_isNowPulsing)
        RunPulse(stuff);
      else
        _stuffPulsing.Add(stuff, null);
    }


    public void GiveHelpToUser()
    {
      StartPulsing();
    }

    public void TakeAwayHelpFromUser()
    {
      StopPulsing();
    }
  }
}