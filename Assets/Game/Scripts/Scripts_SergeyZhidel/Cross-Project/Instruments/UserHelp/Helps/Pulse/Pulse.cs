﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  ////Заставляет пульсировать (скейлиться) обьект
  /// </summary>
  public class Pulse:MonoBehaviour
  {
    public Tuple<Vector3, float>[] pairsSizeAndSeconds ;
    private int curIndex;
    public Vector3 BeforePulseSize;

    /// <summary>
    /// текущее время пульсирования
    /// </summary>
    float _elapsedTime;

    /// <summary>
    /// от какого размера
    /// </summary>
    Vector3 _beforeCurrentPulseScale;

    /// <summary>
    /// до какого размера
    /// </summary>
    Vector3 _toSize;

    /// <summary>
    /// за сколько нужно заскейлить 
    /// </summary>
    private float _timeForScale;

    void Awake()
    {
      BeforePulseSize = transform.localScale;

      pairsSizeAndSeconds = new[]
      {
        new Tuple<Vector3, float>(BeforePulseSize*1.1f, 0.5f),
        new Tuple<Vector3, float>(BeforePulseSize*1f, 0.5f),
        new Tuple<Vector3, float>(BeforePulseSize*0.9f, 0.5f),
        new Tuple<Vector3, float>(BeforePulseSize*1f, 0.5f)
      };
    }

    void InitPulse()
    {
      _beforeCurrentPulseScale = transform.localScale;
      _toSize = pairsSizeAndSeconds[curIndex].Item1;
      _timeForScale = pairsSizeAndSeconds[curIndex].Item2;
      _elapsedTime = 0;
      curIndex++;
      if (curIndex > pairsSizeAndSeconds.Length-1)
        curIndex = 0;
    }


    void Update()
    {
      if (_elapsedTime < _timeForScale)
      {
        transform.localScale = Vector3.Lerp(_beforeCurrentPulseScale, _toSize, _elapsedTime / _timeForScale);
        _elapsedTime += Time.deltaTime;
      }
      else
      {
        InitPulse();
        Update();
      }
    }
  }
}