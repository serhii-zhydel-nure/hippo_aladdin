﻿using System;
using System.Collections.Generic;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  public delegate Delegate PassingPublisher();
  /// <summary>
  /// Определяет когда давать подсказки
  /// </summary>
  public abstract class UserHelpJudge : MonoBehaviour
  {
    //показываются ли сейчас подсказки
    public PopUpStatus HelpStatus = PopUpStatus.Hiden;

    //сами подсказки
    private List<IUserHepler> _userHeplers = new List<IUserHepler>();

    //показали пользователю подсказки
    public event Action OnGiveUserHelp;

    //спрятали подсказки
    public event Action OnTakeHelpBackFromUser;

    //хранятся чтоб можно было отписаться в конце.
    //за этим нужно следить! отписываться вручную, или использовать using(), или самому вызывать Dispose()
    public TransferableProp<Action> UserDidRightAction = new TransferableProp<Action>(() => { });//пользователь сделал правильно
    public TransferableProp<Action> UserDidWrongAction = new TransferableProp<Action>(() => { });//пользователь сделал неправильно


    protected virtual void Start()
    {
      //UserDidRightAction.Value += () => Debug.Log("<color=blue>UserDidRightAction</color>", this);
      //UserDidWrongAction.Value += () => Debug.Log("<color=blue>UserDidWrongAction</color>", this);
    }

    public void NotifyUserDidRightAction()
    {
      UserDidRightAction.Value.InvokeSafe();
    }

    public void NotifyUserDidWrongAction()
    {
      UserDidWrongAction.Value.InvokeSafe();
    }

    protected virtual void GiveUserHelp()
    {
      Debug.Log(String.Format("<color=blue>GiveUserHelp: {0}</color>", GetType().Name), this);
      _userHeplers.ForEach(helper => helper.GiveHelpToUser());
      HelpStatus = PopUpStatus.Opened;
      OnGiveUserHelp.InvokeSafe();
    }


    protected virtual void TakeUserHelpBack()
    {
      Debug.Log(String.Format("<color=blue>TakeUserHelpBack: {0}</color>", GetType().Name), this);
      _userHeplers.ForEach(helper => helper.TakeAwayHelpFromUser());
      HelpStatus = PopUpStatus.Hiden;
      OnTakeHelpBackFromUser.InvokeSafe();
    }


    public void AddUserHelpers(params IUserHepler[] helpers)
    {
      foreach (IUserHepler hepler in helpers)
      {
        if (_userHeplers.Contains(hepler))
        {
          Debug.Log(String.Format("<color=blue>Dublicated user helper: {0}</color>", (hepler as MonoBehaviour).name), this);
          return;
        }
        _userHeplers.Add(hepler);
      }
    }


    public void RemoveUserHelpers(params IUserHepler[] helpers)
    {
      foreach (IUserHepler helper in helpers)
      {
        if (!_userHeplers.Contains(helper))
        {
          Debug.Log(String.Format("<color=blue>No such user helper: {0}</color>", (helper as MonoBehaviour).name), this);
          return;
        }
        _userHeplers.Add(helper);
      }
    }
  }
}