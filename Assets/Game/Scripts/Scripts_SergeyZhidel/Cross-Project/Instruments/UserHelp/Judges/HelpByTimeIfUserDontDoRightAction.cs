﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel
{
  /// <summary>
  /// Если пользователь столько-то времени не делает правильных действий - подсказка
  /// </summary>
  public class HelpByTimeIfUserDontDoRightAction : UserHelpJudge, IDisposable
  {
    //через сколько времени давать подсказку
    public float TimeOfWaitingToGiveHelp
    {
      get { return _timeOfWaitingToGiveHelp; }
      set
      {
        _timeOfWaitingToGiveHelp = value;
        GiveHelpTimer.Interval = value;
      }
    }
    private float _timeOfWaitingToGiveHelp = 10;

    //таймер следящий что пора давать подсказку
    public Timer GiveHelpTimer;

    public static HelpByTimeIfUserDontDoRightAction GetNewInstance(TransferableProp<Action> didRightAction, TransferableProp<Action> didWrongAction, float timeOfWaitingToGiveHelp = 10)
    {
      var judge = CommonScriptsContainer.Instance.AddComponent<HelpByTimeIfUserDontDoRightAction>();
      judge.UserDidRightAction = didRightAction;
      judge.UserDidWrongAction = didWrongAction;
      judge._timeOfWaitingToGiveHelp = timeOfWaitingToGiveHelp;
      judge.GiveHelpTimer = Timer.GiveNewInstance(judge._timeOfWaitingToGiveHelp, "TimerForSelectCorrectFood");
      judge.GiveHelpTimer.OnTick += judge.GiveUserHelp;//если досчитал до конца - показать подсказку
      judge.UserDidRightAction.Value += judge.GiveHelpTimer.Restart;//после правильного действия - сброс таймера (считай заново)
      return judge;
    }

    public static HelpByTimeIfUserDontDoRightAction GetNewInstance(float timeOfWaitingToGiveHelp = 10)
    {
      var judge = CommonScriptsContainer.Instance.AddComponent<HelpByTimeIfUserDontDoRightAction>();
      judge._timeOfWaitingToGiveHelp = timeOfWaitingToGiveHelp;
      judge.GiveHelpTimer = Timer.GiveNewInstance(judge._timeOfWaitingToGiveHelp, "TimerForSelectCorrectFood");
      judge.GiveHelpTimer.OnTick += judge.GiveUserHelp;//если досчитал до конца - показать подсказку
      judge.UserDidRightAction.Value += judge.GiveHelpTimer.Restart;//после правильного действия - сброс таймера (считай заново)
      return judge;
    }


    protected override void GiveUserHelp()
    {
      base.GiveUserHelp();
      GiveHelpTimer.Stop();//не считать пока пользователь не сделает правильное действие
      UserDidRightAction.Value += TakeUserHelpBack; //если было сделано правильный поступок - забарать подсказки
    }


    protected override void TakeUserHelpBack()
    {
      base.TakeUserHelpBack();
      //считать пока пользователь не сделает правильное действие
      GiveHelpTimer.StartWork();
      //ждать пока пользователь опять запутается
      UserDidRightAction.Value -= TakeUserHelpBack;
    }

    public void StartWork()
    {
      Debug.Log("<color=blue>Start Helper</color>", this);
      GiveHelpTimer.StartWork();
    }

    public void Stop()
    {
      GiveHelpTimer.Stop();
      base.TakeUserHelpBack();
      UserDidRightAction.Value -= TakeUserHelpBack;
      Debug.Log("<color=blue>Stop Helper</color>", this);
    }

    public void Restart()
    {
      Stop();
      StartWork();
    }

    public void Dispose()
    {
      UserDidRightAction.Value -= GiveHelpTimer.Restart;
      GiveHelpTimer.Stop();
      base.TakeUserHelpBack();
    }
  }
}