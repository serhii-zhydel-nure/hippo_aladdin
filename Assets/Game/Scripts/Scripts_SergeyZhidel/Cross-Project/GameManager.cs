﻿using System;
#if PSV_PROTOTYPE
using PSV;
#endif
using Scripts_SergeyZhidel.Debuging;
using Scripts_SergeyZhidel.Extenders;
using Scripts_SergeyZhidel.ScenesManagment;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts_SergeyZhidel
{
    /// <summary>
    /// Главный обьект, содержит основные игровые события, служит контейнером для других частей фреймворка
    /// Инициирует игру
    /// </summary>
    public class GameManager : SceneSingleton<GameManager>
    {
        public Action ApplicationInit;
        public Action<string> OnLevelLoaded;

        public Action ApplicationPause;
        public bool IsGamePaused { get; private set; }
        public Action ApplicationUnPause;

        public Action ApplicationGetFocus;
        public bool HasGameFocus { get; private set; }
        public Action ApplicationLostFocus;

        public Action ApplicationQuit;

        /// <summary>
        /// Загружается сразу после движка, после загрузки первой сцены, до всех Awake()
        /// </summary>
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void InitGame()
        {
            DebuggingConfigurations.LoadDebugingConfigurations();
#if (UNITY_EDITOR)
            Scenes current = Scenes.EmptySceneMode;
            if (SceneManager.GetActiveScene().name.TryParceToEnum(ref current) && current != Scenes.InitScene)
            {
                Debug.Log(current.ToString() + ", loading init before target scene");
                //SceneManager.UnloadSceneAsync(current.ToString());
                //SceneManager.LoadScene(Scenes.InitScene.ToString());

                Debug.Log("<color=red>предзагрузка</color>");
                //SceneLoader.first_scene = current;
                //PSVSettings.settings.first_scene = current;
            }
            else
            {
                PSVSettings.settings.first_scene = Scenes.MainMenu;
            }
#endif
            Instance.ApplicationInit.InvokeSafe();
        }

        private void Start()
        {
            OnLevelLoaded += ScenesFSM.LevelWasLoaded;
            //После первой загрузки - инициализация загруженой сцены
            OnLevelWasLoaded();
        }

        private void OnLevelWasLoaded()
        {
            OnLevelLoaded.InvokeSafe(SceneManager.GetActiveScene().name);
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (!hasFocus && HasGameFocus)//первый раз
            {
                ApplicationLostFocus.InvokeSafe();
                HasGameFocus = hasFocus;
            }

            if (hasFocus && !HasGameFocus)//первый раз
            {
                ApplicationGetFocus.InvokeSafe();
                HasGameFocus = hasFocus;
            }
        }

        private void OnApplicationPause(bool pauseState)
        {
            if (pauseState && !IsGamePaused)//первый раз
            {
                ApplicationPause.InvokeSafe();
                IsGamePaused = pauseState;
            }

            if (!pauseState && IsGamePaused)//первый раз
            {
                ApplicationUnPause.InvokeSafe();
                IsGamePaused = pauseState;
            }
        }

        private void OnApplicationQuit()
        {
            ApplicationQuit.InvokeSafe();
        }
    }
}
