﻿namespace Scripts_SergeyZhidel.InputManagement.Global
{
  public abstract class GlobalInputResponder<TGlobalInputChecker, PCheckerParams> 
    : InputResponder<TGlobalInputChecker, PCheckerParams> where TGlobalInputChecker 
    : InputChecker<TGlobalInputChecker, PCheckerParams>
  {

  }
}