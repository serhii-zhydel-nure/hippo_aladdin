﻿using System;
using System.Collections;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Global
{
  /// <summary>
  /// В месте нажатия на экран пользователем показать gameObject 
  /// и дальше двигать за пальцем, 
  /// пока не отожмет (на отжатие скрыть gameObject) 
  /// </summary>
  public class UserFingerFolower : GlobalInputResponder<Global_SwipingScreenWorldPos, Vector2>
  {
    private IEnumerator _lastCoroutine;


    public override void OnStart(Vector2 param)
    {
      Debug.Log(String.Format("<color=blue>OnStart: {0}</color>", gameObject.name));

      //показать gameObject
      gameObject.SetActive(true);//короутина запуститься только на активном обьекте

      gameObject.transform.position = param;

      if (_lastCoroutine != null)//завершить прятанье если не закончилось
        StopCoroutine(_lastCoroutine);

     
      _lastCoroutine = gameObject.TransparentOverSeconds(1, 0.3f);
      StartCoroutine(_lastCoroutine);
    }

    public override void OnContinue(Vector2 param)
    {
      if (!gameObject.activeSelf)
        OnStart(param);

      //переместить
      gameObject.transform.position = param;
    }

    public override void OnFinish(Vector2 param)
    {
      Debug.Log(String.Format("<color=blue>OnFinish: {0}</color>", gameObject.name));

      gameObject.transform.position = param;

      if (_lastCoroutine != null)//завершить показ если не закончился
        StopCoroutine(_lastCoroutine);

      //спрятать gameObject
      _lastCoroutine = gameObject.TransparentOverSeconds(0, 0.3f).Append(() =>
      {
        gameObject.SetActive(false);
      });
      StartCoroutine(_lastCoroutine);
    }

  }
}
