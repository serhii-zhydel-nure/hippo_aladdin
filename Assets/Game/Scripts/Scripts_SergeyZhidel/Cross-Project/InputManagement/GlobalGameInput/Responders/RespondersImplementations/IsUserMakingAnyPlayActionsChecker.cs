﻿using System;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Global
{
  public class IsUserMakingAnyPlayActionsChecker : GlobalInputResponder<Global_AnyPlayActions, Vector2>
  {
    public Action<Vector2> UserPressedScreen;
    public Action<Vector2> UserMoveFinger;
    public Action<Vector2> UserDePressedScreen;
    public Action<Vector2> SomethingHasHappen;

    protected override void Awake()
    {
      base.Awake();
      UserPressedScreen +=(pos)=> SomethingHasHappen.InvokeSafe(pos);
      UserMoveFinger += (pos) => SomethingHasHappen.InvokeSafe(pos);
      UserDePressedScreen += (pos) => SomethingHasHappen.InvokeSafe(pos);
    }

    public override void OnStart(Vector2 param)
    {
      UserPressedScreen.InvokeSafe(param);
    }

    public override void OnContinue(Vector2 param)
    {
      UserMoveFinger.InvokeSafe(param);
    }

    public override void OnFinish(Vector2 param)
    {
      UserDePressedScreen.InvokeSafe(param);
    }
  }
}
