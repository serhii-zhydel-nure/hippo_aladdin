﻿using System;
using System.Collections;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Global
{
  /// <summary>
  /// Абастрактный класс определяющий любое взаимодействие пользователя с игрой через источники ввода (пока только через тач).
  /// 1 обработчик ввода, и много подписчиков, уведомляемых о вводе. 
  /// Для глобального ввода, не предназначеного конкретно одному обьекту, а просто оповещающих всех, кто подписался что событие ввода произошло.
  /// Пример событий: "Был свайп <сторона>", "Был тач <позиция>".
  /// Просто накинуть на обьект (через AddComponent) любую реализацию и пользоваться ее возможностями.
  /// </summary>
  /// <typeparam name="TParam">Определяет параметр, возвращамый при совершении ввода (например координаты точки нажатия на экран или обьект, который был выбран)</typeparam>
  public abstract class GlobalInputChecker<TCheckerType, TParam> : InputChecker<TCheckerType, TParam> 
    where TCheckerType : InputChecker<TCheckerType, TParam>
  {

    //Для сторонних (одноразовых) подписчиков
    public event Action<TParam> OnStartExternalSubscribers;
    public event Action<TParam> OnContinueExternalSubscribers;
    public event Action<TParam> OnFinishExternalSubscribers;

    /// <summary>
    /// Было ли действие в прошлом кадре 
    /// </summary>
    protected bool WasPreviousReaction = false;

    protected void InvokeStart(TParam param)
    {
      foreach (var subscriber in _subscribers)
      {
        subscriber.OnStart(param);
      }

      OnStartExternalSubscribers.InvokeSafe(param);
    }
    protected void InvokeContinue(TParam param)
    {
      foreach (var subscriber in _subscribers)
      {
        subscriber.OnContinue(param);
      }

      OnContinueExternalSubscribers.InvokeSafe(param);
    }
    protected void InvokeFinish(TParam param)
    {
      foreach (var subscriber in _subscribers)
      {
        subscriber.OnFinish(param);
      }

      OnFinishExternalSubscribers.InvokeSafe(param);
    }

    public override void CleanSubscription()
    {
      base.CleanSubscription();
      OnStartExternalSubscribers = param => { };
      OnContinueExternalSubscribers = param => { };
      OnFinishExternalSubscribers = param => { };
    }

    protected override IEnumerator CheckControll()
    {
      while (true)
      {
        if (_subscribers.Count == 0 && !OnStartExternalSubscribers.HasSubscriptions())//dosn't have subscriptions
        {
          enabled = false;//set as not active
        }
        ReadControll();

        if (CheckPeriod == 0)
          yield return null;
        else
          yield return new WaitForSeconds(CheckPeriod);
      }
    }
  }

}