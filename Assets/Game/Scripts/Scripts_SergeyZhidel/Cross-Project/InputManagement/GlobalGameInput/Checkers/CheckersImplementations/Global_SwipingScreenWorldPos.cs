﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Global
{
  /// <summary>
  /// Говорит о том, что пользователь водит пальцем по экрану (как угодно)
  /// </summary>
  public class Global_SwipingScreenWorldPos:GlobalInputChecker<Global_SwipingScreenWorldPos, Vector2>
  {
    protected override void ReadControll()
    {
      if (Input.GetMouseButton(0))//mouse pressed
      {
        if (!WasPreviousReaction) //begin gesture
        {
          WasPreviousReaction = true;
          InvokeStart(Camera.main.ScreenToWorldPoint(Input.mousePosition));

        }
        else
        {
          InvokeContinue(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
      }
      else//mouse depressed
      {
        if (WasPreviousReaction) //finish drag
        {
          InvokeFinish(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        WasPreviousReaction = false;
      }
    }
  }
}