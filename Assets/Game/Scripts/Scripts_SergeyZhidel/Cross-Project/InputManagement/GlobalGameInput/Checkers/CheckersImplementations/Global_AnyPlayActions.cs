﻿using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Global
{
  /// <summary>
  /// Клацает ли пользователь устройство, или тупит
  /// </summary>
  public class Global_AnyPlayActions : GlobalInputChecker<Global_AnyPlayActions, Vector2>
  {
    private Vector3? _mouseDownPos;

    protected override void ReadControll()
    {
      if (Input.GetMouseButton(0))//mouse pressed
      {
        if (Input.GetMouseButtonDown(0) && !_mouseDownPos.HasValue)//нажатие
        {
          _mouseDownPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
          InvokeStart(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
        else
        {
          if (_mouseDownPos.HasValue)
          {
            _mouseDownPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            InvokeContinue(Camera.main.ScreenToWorldPoint(Input.mousePosition));
          }
        }
      }
      else
      {
        if (Input.GetMouseButtonUp(0) && _mouseDownPos.HasValue)//отжатие
        {
          InvokeFinish(Camera.main.ScreenToWorldPoint(Input.mousePosition));
          _mouseDownPos = null;
        }
      }
    }
  }
}