﻿using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement
{
  public abstract class InputResponder<TInputChecker, PCheckerParams> : MonoBehaviour
    where TInputChecker : InputChecker<TInputChecker, PCheckerParams>
  {
    //запомнить контроллер на который подписались
    protected TInputChecker Checker;

    protected virtual void Awake()
    {
      //найти или создать контроллер и подписаться на него
      Checker = InputChecker<TInputChecker, PCheckerParams>.Instance as TInputChecker;
      Checker.AddSubscribers(this);
      Checker.enabled = true;//если у контроллера небыло подписчиков, то он уснул. Подписаться и разбудить его
    }

    protected virtual void OnDestroy()
    {
      Checker.RemoveSubscribers(this);
    }

    /// <summary>
    /// Действие на начало ввода (пользователь начал управлять)
    /// </summary>
    /// <param name="param"></param>
    public abstract void OnStart(PCheckerParams param);

    /// <summary>
    /// Действие на продолжение ввода (пользователь продолжает управлять)
    /// </summary>
    /// <param name="param"></param>
    public abstract void OnContinue(PCheckerParams param);


    /// <summary>
    /// Действие на окончание ввода (пользователь перестал управлять)
    /// </summary>
    /// <param name="param"></param>
    public abstract void OnFinish(PCheckerParams param);
  }
}
