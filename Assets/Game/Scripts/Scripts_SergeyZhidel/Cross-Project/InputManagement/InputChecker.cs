﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement
{
  public abstract class InputChecker<TInputChecker, PCheckerParams> : MonoBehaviour
    where TInputChecker : InputChecker<TInputChecker, PCheckerParams>
  {
    public static GameObject ControllsContainer;

    public static InputChecker<TInputChecker, PCheckerParams> Instance
    {
      get
      {
        if (_instance == null)
        {
          if (ControllsContainer == null)
          {
            ControllsContainer = GameObjectExtender.CreateIfNotExist("InputControllers").CreateIfNotExist("Global");
          }

          //наследник, которого запрашивают
          //первый наследник не имеющий своих наследников (последний в иерархии наследования, уже не абстрактный)
          Type inheritorType = Reflection.GetInheritorsTypes<InputChecker<TInputChecker, PCheckerParams>>()
            .First(type => Reflection.GetInheritorsTypes(type).Count() == 0);

          _instance = (InputChecker<TInputChecker, PCheckerParams>)ControllsContainer.AddComponent(inheritorType);
        }

        return _instance;
      }
    }

    public static InputChecker<TInputChecker, PCheckerParams> _instance;

    //пока есть хоть один подписчик - контроллер будет работать
    protected HashSet<InputResponder<TInputChecker, PCheckerParams>> _subscribers =
     new HashSet<InputResponder<TInputChecker, PCheckerParams>>();


    public float CheckPeriod = 0;//0 - каждый кадр

    public virtual void AddSubscribers(params InputResponder<TInputChecker, PCheckerParams>[] subscribers)
    {
      foreach (InputResponder<TInputChecker, PCheckerParams> subscriber in subscribers)
      {
        _subscribers.Add(subscriber);
      }
    }

    public virtual void RemoveSubscribers(params InputResponder<TInputChecker, PCheckerParams>[] subscribers)
    {
      foreach (InputResponder<TInputChecker, PCheckerParams> subscriber in subscribers)
      {
        _subscribers.Remove(subscriber);
      }
    }

    public virtual void CleanSubscription()
    {
      _subscribers = new HashSet<InputResponder<TInputChecker, PCheckerParams>>();
    }

    protected virtual void OnEnable()
    {
      StartCoroutine(CheckControll());
    }

    protected virtual void OnDisable()
    {
      StopAllCoroutines();
    }

    protected virtual IEnumerator CheckControll()
    {
      while (true)
      {
        if (_subscribers.Count == 0)//dosn't have subscriptions
        {
          enabled = false;//set as not active
        }
        ReadControll();

        if (CheckPeriod == 0)
          yield return null;
        else
          yield return new WaitForSeconds(CheckPeriod);
      }
    }


    /// <summary>
    /// Главный метод в этом классе. 
    /// </summary>
    protected abstract void ReadControll();
  }
}