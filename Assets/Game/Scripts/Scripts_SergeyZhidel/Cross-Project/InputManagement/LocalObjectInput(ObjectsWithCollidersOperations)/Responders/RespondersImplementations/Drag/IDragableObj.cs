﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  public interface IDragableObj
  {
    /// <summary>
    /// Начал тащить
    /// </summary>
    event Action<GameObject> StartDragging;

    /// <summary>
    /// Дотащил удачно
    /// </summary>
    event Action<GameObject> FinishedSuccessfully;


    /// <summary>
    /// Если недотащил, или запрещено дотаскивать
    /// </summary>
    event Action<GameObject> FinishedUnSuccessfully;

    /// <summary>
    /// Куда тащить
    /// </summary>
    Collider2D TargetCollider2D { get; set; }

    /// <summary>
    /// Если на сцене есть одновременно несколько обьектов, которые можно перетаскивать, 
    /// но нужно контролировать порядок их перетаскивания 
    /// (чтоб запретить удачное заверение перетаскивания)
    /// </summary>
    bool CanFinishDraggingSuccessful { get; set; }
  }
}