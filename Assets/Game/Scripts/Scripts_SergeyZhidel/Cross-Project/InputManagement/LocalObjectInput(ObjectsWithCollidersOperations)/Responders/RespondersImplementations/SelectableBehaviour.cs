﻿using System;
using Scripts_SergeyZhidel.Extenders;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  /// <summary>
  /// Добавить возможность выбора обьекта тапом
  /// </summary>
  public class SelectableBehaviour : LocalInputResponder<Tap_Selecting, LocalInputParam>
  {
    public event Action<LocalInputParam> Selected = delegate { };

   
    public override void OnStart(LocalInputParam param)
    {
      Selected.InvokeSafe(param);
    }

    public override void OnContinue(LocalInputParam param)
    {
    }

    public override void OnFinish(LocalInputParam param)
    {
    }


    public void SimulateSelecting()
    {
      Selected.InvokeSafe(new LocalInputParam(gameObject, gameObject.transform.position));
    }
  }
}