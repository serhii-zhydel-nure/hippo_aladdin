﻿using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  /// <summary>
  /// Абстрактный обработчик реакции на пользовательский ввод.
  /// Просто накинуть на обьект (через AddComponent) любую реализацию и пользоваться ее возможностями.
  /// </summary>
  /// <typeparam name="TLocalInputChecker">Контроллер, который будет слушаться (было ли произведено действие (был ли нажат экран, сделан свайп или выбран обьект))</typeparam>
  /// <typeparam name="PCheckerParams"> Параметр, возвращаемый контроллером (где было нажатие, что было выбрано)</typeparam>
  public abstract class LocalInputResponder<TLocalInputChecker, PCheckerParams> 
    : InputResponder<TLocalInputChecker, PCheckerParams> where TLocalInputChecker 
    : InputChecker<TLocalInputChecker, PCheckerParams>
  {
    public bool IsAllowed { get; protected set; }

    protected override void Awake()
    {
      base.Awake();

      if (gameObject.GetComponent<Collider2D>() == null)
        gameObject.AddComponent<BoxCollider2D>();

      IsAllowed = true;
    }

  }

}