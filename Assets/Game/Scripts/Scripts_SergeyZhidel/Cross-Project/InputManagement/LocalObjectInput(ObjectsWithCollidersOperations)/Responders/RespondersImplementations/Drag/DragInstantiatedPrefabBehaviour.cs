﻿using System;
using System.Linq;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  /// <summary>
  /// На месте начала драга инстанцируется префаб, который тащиться до цели.
  /// </summary>
  class DragInstantiatedPrefabBehaviour : LocalInputResponder<Swipe_DragAndDrop, LocalInputParam>, IDragableObj
  {
    public GameObject PrafabToInstantiate = null;
    public GameObject InstantiatedPrefab;

    public event Action<GameObject> StartDragging;
    public event Action<GameObject> FinishedSuccessfully;
    public event Action<GameObject> FinishedUnSuccessfully;
    public event Action<GameObject> AfterMovedBack;

    public Collider2D TargetCollider2D { get; set; }
    public bool CanFinishDraggingSuccessful { get; set; }

    public bool CanBeDragged { get { return _canBeDragged; } private set { _canBeDragged = value; } }
    [SerializeField]
    private bool _canBeDragged = true;

    public Vector3 PositionBeforeDrag;

    public float SecondsMovingBack = 2;

    protected void Start()
    {
      PositionBeforeDrag = transform.position;
      CanBeDragged = true;
    }

    public override void OnStart(LocalInputParam param)
    {
      if (CanBeDragged)
      {
        InstantiatedPrefab = (GameObject)Instantiate(PrafabToInstantiate, transform.position, transform.rotation);
        PositionBeforeDrag = InstantiatedPrefab.transform.position;
        InstantiatedPrefab.transform.position = new Vector3(param.Position.x, param.Position.y, transform.position.y);
        StartDragging.InvokeSafe(InstantiatedPrefab);
      }
    }

    public override void OnContinue(LocalInputParam param)
    {
      InstantiatedPrefab.transform.position = new Vector3(param.Position.x, param.Position.y, transform.position.y);
    }

    public override void OnFinish(LocalInputParam param)
    {
      InstantiatedPrefab.transform.position = new Vector3(param.Position.x, param.Position.y, transform.position.y);
      CheckIsOnCollider(param.Object);
    }

    public void CheckIsOnCollider(GameObject draggedObject)
    {
      if (CanBeDragged)
      {
        CanBeDragged = false;
        Collider2D[] hittedColliders =
          Physics2D.RaycastAll(InstantiatedPrefab.transform.position, Vector2.zero)
            .Select(hitted => hitted.collider)
            .ToArray();
        if (TargetCollider2D != null && hittedColliders.Contains(TargetCollider2D) && CanFinishDraggingSuccessful)
        {
          FinishedSuccessfully.InvokeSafe(InstantiatedPrefab);
          CanBeDragged = true;
        }
        else
        {
          StartCoroutine(InstantiatedPrefab.transform.MoveOverSeconds(PositionBeforeDrag, SecondsMovingBack)
            .Append(() => AfterMovedBack.InvokeSafe(InstantiatedPrefab))
            .Append(() => CanBeDragged = true)
            .Append(() => FinishedUnSuccessfully.InvokeSafe(InstantiatedPrefab))
            );
        }
      }
    }
  }
}
