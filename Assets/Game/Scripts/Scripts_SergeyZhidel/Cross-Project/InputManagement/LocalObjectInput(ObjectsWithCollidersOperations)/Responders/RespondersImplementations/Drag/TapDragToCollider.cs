﻿using System;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  /// <summary>
  /// По тапу на обьект перетащить обьект к цели (Легкий уровень сложности)
  /// </summary>
  public class TapDragToCollider : SelectableBehaviour, IDragableObj
  {
    public bool CanFinishDraggingSuccessful { get; set; }
    public event Action<GameObject> StartDragging;
    public event Action<GameObject> FinishedSuccessfully;
    public event Action<GameObject> FinishedUnSuccessfully;
    public Collider2D TargetCollider2D { get; set; }
    public float MoveSpeed = 15f;

    protected override void Awake()
    {
      base.Awake();

      CanFinishDraggingSuccessful = true;

      Selected += o =>
      {
        if (CanFinishDraggingSuccessful)
        {
          StartDragging.InvokeSafe(gameObject);
          StartCoroutine(gameObject.transform.MoveWithSpeed(TargetCollider2D.gameObject, MoveSpeed)
            .Append(() => FinishedSuccessfully.InvokeSafe(gameObject)));
        }
        else
        {
          FinishedUnSuccessfully.InvokeSafe(gameObject);
        }
      };
    }
  }
}