﻿using System;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  /// <summary>
  /// Считать время, пока пользователь чешит коллайдер, вышел из коллайдера или не двигается - не считать
  /// </summary>
  public class WhileMovingInColliderTimeRecorder : LocalInputResponder<Swipe_ScrubingCollider, LocalInputParam>
  {
    public event Action<LocalInputParam> StartMovingInCollider;
    public event Action<LocalInputParam> ContinueMovingInCollider;
    public event Action<LocalInputParam> FinishMovingInCollider;

    public event Action<int> PersentChanged;
    public event Action<GameObject> WaitingFinished;

    public float MustWaitTime = 5;

    private int _currentPersents;
    private float _waitingTime;
    private float _lastTime;

    private Vector2 _lastPos;
    private Vector2 _curPos;

    public override void OnStart(LocalInputParam param)
    {
      _curPos = _lastPos = param.Position;
      _lastTime = Time.time;
      StartMovingInCollider.InvokeSafe(param);
    }

    public override void OnContinue(LocalInputParam param)
    {
      _curPos = param.Position;
      if (_curPos != _lastPos)
      {
        _lastPos = _curPos;
        TickTime();
        ContinueMovingInCollider.InvokeSafe(param);
      }
      _lastTime = Time.time;
    }

    public override void OnFinish(LocalInputParam param)
    {
      FinishMovingInCollider.InvokeSafe(param);
    }

    private void TickTime()
    {
      _waitingTime += Time.time - _lastTime;

      int persents = Mathf.Clamp(Mathf.RoundToInt(_waitingTime / MustWaitTime * 100),0,100);

      if (persents != _currentPersents)
      {
        _currentPersents = persents;
        PersentChanged.InvokeSafe(_currentPersents);
      }

      if (_waitingTime >= MustWaitTime)
        WaitingFinished.InvokeSafe(gameObject);
    }
  }
}