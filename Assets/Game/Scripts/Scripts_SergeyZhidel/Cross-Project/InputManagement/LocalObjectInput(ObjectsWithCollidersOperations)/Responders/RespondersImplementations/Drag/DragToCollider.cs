﻿using System;
using System.Linq;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  /// <summary>
  /// Просто тащить обьект до колайдера-цели
  /// </summary>
  public class DragToCollider : LocalInputResponder<Swipe_DragAndDrop, LocalInputParam>, IDragableObj
  {
    public event Action<GameObject> StartDragging;
    public event Action<GameObject> FinishedSuccessfully;
    public event Action<GameObject> FinishedUnSuccessfully;
    public event Action<GameObject> AfterMovedBack;
    public bool MoveBackAtItsPlace = true;

    public Collider2D TargetCollider2D { get; set; }

    public bool CanFinishDraggingSuccessful
    {
      get { return _canFinishDraggingSuccessful; }
      set { _canFinishDraggingSuccessful = value; }
    }
    private bool _canFinishDraggingSuccessful = true;

    public bool CanBeDragged { get { return _canBeDragged; } private set { _canBeDragged = value; } }
    [SerializeField]
    private bool _canBeDragged = true;


    public Vector3 PositionBeforeDrag;

    public float SecondsMovingBack = 2;


    protected void Start()
    {
      PositionBeforeDrag = transform.position;
    }

    public override void OnStart(LocalInputParam param)
    {
      if (CanBeDragged)
      {
        PositionBeforeDrag = transform.position;
        transform.position = new Vector3(param.Position.x, param.Position.y, transform.position.y);
        StartDragging.InvokeSafe(gameObject);
      }
    }

    public override void OnContinue(LocalInputParam param)
    {
      transform.position = new Vector3(param.Position.x, param.Position.y, transform.position.y);
    }

    public override void OnFinish(LocalInputParam param)
    {
      transform.position = new Vector3(param.Position.x, param.Position.y, transform.position.y);
      CheckIsOnCollider(param.Object);
    }


    public void CheckIsOnCollider(GameObject draggedObject)
    {
      if (CanBeDragged)
      {
        CanBeDragged = false;
        Collider2D[] hittedColliders =
          Physics2D.RaycastAll(gameObject.transform.position, Vector2.zero)
            .Select(hitted => hitted.collider)
            .ToArray();
        if (TargetCollider2D != null && hittedColliders.Contains(TargetCollider2D) && CanFinishDraggingSuccessful)
        {
          FinishedSuccessfully.InvokeSafe(gameObject);
          CanBeDragged = true;
        }
        else
        {
          FinishedUnSuccessfully.InvokeSafe(gameObject);
          if (MoveBackAtItsPlace)
            StartCoroutine(gameObject.transform.MoveOverSeconds(PositionBeforeDrag, SecondsMovingBack)
              .Append(() => AfterMovedBack.InvokeSafe(draggedObject))
              .Append(() => CanBeDragged = true));
          else
            CanBeDragged = true;
        }
      }
    }
  }
}