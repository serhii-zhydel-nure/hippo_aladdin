﻿using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  /// <summary>
  /// Проверяет было ли действие по отношению к конкретному LocalInputResponder (обьекту с коллайдером) и если да, то сообщает этому обьекту об действии
  /// </summary>
  /// <typeparam name="TLocalInputChecker">Нужен для того чтоб можно было реализовать "HashSet _subscribers"</typeparam>
  /// <typeparam name="PControllParams">Параметр, возвращаемый контроллером (где было нажатие, что было выбрано)</typeparam>
  public abstract class LocalInputChecker<TLocalInputChecker, PControllParams> : InputChecker<TLocalInputChecker, PControllParams> where TLocalInputChecker : InputChecker<TLocalInputChecker, PControllParams>
  {
  
    /// <summary>
    /// Кому в данный момент шлются уведомления. 
    /// Так же определяет было ли начато действие в предыдущем кадре 
    /// (если Responder != null, значит его уже находили, значит ему уже посылали команды)
    /// </summary>
    protected LocalInputResponder<TLocalInputChecker, PControllParams> Responder;


    /// <summary>
    /// Общий метод, часто используется в наследниках
    /// </summary>
    /// <param name="responder"></param>
    /// <returns></returns>
    protected bool TryGetResponderByCollider(ref LocalInputResponder<TLocalInputChecker, PControllParams> responder)
    {
      Vector2 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
      RaycastHit2D hit = Physics2D.Raycast(worldPoint, Vector2.zero);

      //If something was hit1Child, the RaycastHit2D.collider will not be null.
      if (hit.collider != null)
      {
        //это что-то - LocalInputResponder?
        responder = hit.collider.gameObject.GetComponent<LocalInputResponder<TLocalInputChecker, PControllParams>>();
      }
      return responder != null;
    }
  }



  public class LocalInputParam
  {
    public Vector3 Position;
    public GameObject Object;

    public LocalInputParam(GameObject obj, Vector3 position)
    {
      Object = obj;
      Position = position;
    }
  }
}
