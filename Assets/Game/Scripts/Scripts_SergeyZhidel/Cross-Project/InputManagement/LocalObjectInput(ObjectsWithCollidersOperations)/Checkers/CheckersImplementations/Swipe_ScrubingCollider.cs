﻿using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  public class Swipe_ScrubingCollider : LocalInputChecker<Swipe_ScrubingCollider, LocalInputParam>
  {
    private GameObject _clampedObject;

    protected override void ReadControll()
    {
      if (Input.GetMouseButton(0))//mouse pressed
      {
        if (Input.GetMouseButtonDown(0))//нажата первый раз в этом цикле
        {
          //начать зажатие
          if (TryGetResponderByCollider(ref Responder))
          {
            _clampedObject = Responder.gameObject;
            Responder.OnStart(new LocalInputParam(Responder.gameObject,
                                  Camera.main.ScreenToWorldPoint(Input.mousePosition)));
          }
        }
        else
        {
          if (_clampedObject != null)//не перестали ли еще следить за этим зажатием
          {
            //зажат ли и тот ли же обьект?
            if (TryGetResponderByCollider(ref Responder) && Responder.gameObject == _clampedObject)
            {
              Responder.OnContinue(new LocalInputParam(Responder.gameObject,
                                Camera.main.ScreenToWorldPoint(Input.mousePosition)));
            }
            else
            {
              //перестать следить за этим зажатием
              Responder.OnFinish(new LocalInputParam(_clampedObject,
                                Camera.main.ScreenToWorldPoint(Input.mousePosition)));
              _clampedObject = null;
            }
          }

        }
      }
      else//mouse depressed
      {
        if (Responder != null) //finish clamp
        {
          //только тут закачивается рабочий цикл
          Responder.OnFinish(new LocalInputParam(_clampedObject,
                              Camera.main.ScreenToWorldPoint(Input.mousePosition)));
          Responder = null;
          _clampedObject = null;
        }
      }
    }



  }


  public struct MovingInColliderParams
  {
    public Vector3 WorldPosition;
    public GameObject MovedObject;

    public MovingInColliderParams(GameObject movedObject, Vector3 position)
    {
      MovedObject = movedObject;
      WorldPosition = position;
    }
  }
}