﻿using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  public class Tap_Selecting : LocalInputChecker<Tap_Selecting, LocalInputParam>
  {
    private Vector3? _mouseDownPos;

    protected override void ReadControll()
    {
      if (Input.GetMouseButtonDown(0) && !_mouseDownPos.HasValue)//нажатие
      {
        _mouseDownPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
      }
      if (Input.GetMouseButtonUp(0) && _mouseDownPos.HasValue)//отжатие
      {
        if ((Camera.main.ScreenToViewportPoint(Input.mousePosition) - _mouseDownPos.Value).magnitude < 0.05)//если отжалась почти там же где и нажалось
        {
          if (TryGetResponderByCollider(ref Responder))
          {
              Responder.OnStart(new LocalInputParam(Responder.gameObject, Camera.main.ScreenToWorldPoint(Input.mousePosition)));
          }
        }
        _mouseDownPos = null;
      }
    }
  }
}
