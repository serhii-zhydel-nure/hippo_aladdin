﻿using UnityEngine;

namespace Scripts_SergeyZhidel.InputManagement.Local
{
  public class Swipe_DragAndDrop : LocalInputChecker<Swipe_DragAndDrop, LocalInputParam>
  {
    private GameObject _draggedObject;


    protected override void ReadControll()
    {
      if (Input.GetMouseButton(0))//mouse pressed
      {
        if (Input.GetMouseButtonDown(0))
        {
          //begin drag
          if (TryGetResponderByCollider(ref Responder))
          {
            _draggedObject = Responder.gameObject;
            Responder.OnStart(new LocalInputParam(Responder.gameObject,
              Camera.main.ScreenToWorldPoint(Input.mousePosition)));
          }
        }
        else
        {
          if (Responder != null)
            Responder.OnContinue(new LocalInputParam(_draggedObject, Camera.main.ScreenToWorldPoint(Input.mousePosition)));
        }
      }
      else
      {
        //mouse depressed
        if (Responder != null)
        {
          //finish drag
          Responder.OnFinish(new LocalInputParam(_draggedObject, Camera.main.ScreenToWorldPoint(Input.mousePosition)));
          Responder = null;
        }
      }
    }
  }
}