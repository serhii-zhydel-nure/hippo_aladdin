﻿using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class ParticleSystemExtender
  {
    public static void SetEmissionRate(this ParticleSystem ps, int rateEmission)
    {
      var emission = ps.emission;
      var rate = emission.rate;
      rate.constantMax = rateEmission;
      emission.rate = rate;
    }
  }
}