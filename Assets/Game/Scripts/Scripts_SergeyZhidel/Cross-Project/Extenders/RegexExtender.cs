﻿using System.Text;
using System.Text.RegularExpressions;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class RegexExtender
  {
    public static Regex StartsWithText(string text)
    {
      return new Regex("^" + text);
    }

    public static Regex EndsWithText(this Regex regex, string text)
    {
      return new Regex(regex + text + "$");
    }

    public static Regex EndsWithText(string text)
    {
      return new Regex(text + "$");
    }

    public static Regex AppendRegex(this Regex regex, string appendingRegex)
    {
      return new Regex(regex + appendingRegex);
    }

    public static Regex AppendRegex(this Regex regex, Regex appendingRegex)
    {
      return new Regex(regex + appendingRegex.ToString());
    }

    public static Regex AppendWord(this Regex regex, string word)
    {
      return regex.AppendRegex("(" + word + ")");
    }

    public static Regex AppendNonSignedInt(this Regex regex)
    {
      return regex.AppendRegex(@"(\\d+)");
    }

    public static Regex AppendInt(this Regex regex)
    {
      return regex.AppendRegex(@"(\-*\d+)");
    }

    public static Regex AppendChars(this Regex regex, params char[] characters)
    {
      StringBuilder apendingRegex = new StringBuilder("[");
      foreach (char character in characters)
        apendingRegex.Append(character);
      apendingRegex.Append(']');

      return regex.AppendRegex(apendingRegex.ToString());
    }

    public static Regex AppendAnyChar(this Regex regex)
    {
      return regex.AppendRegex(@"([.])");
    }

    public static Regex AppendSpace(this Regex regex)
    {
      return regex.AppendRegex(@"(\\s+)");
    }

    public static Regex AppendRepeater(this Regex regex, RegexRepeater repeaterType, int x = 0, int y = 0)
    {
      StringBuilder regexBuildHelper;
      switch (repeaterType)
      {
        case RegexRepeater.MoreOrEqualZero:
          //[reg*]
          regexBuildHelper = new StringBuilder("(").Append(regex).Append("*)");
          return new Regex(regexBuildHelper.ToString());

        case RegexRepeater.MoreOrEqualToOne:
          //[reg+]
          regexBuildHelper = new StringBuilder("(").Append(regex).Append("+)");
          return new Regex(regexBuildHelper.ToString());

        case RegexRepeater.OneOrZero:
          //[reg?]
          regexBuildHelper = new StringBuilder("(").Append(regex).Append("?)");
          return new Regex(regexBuildHelper.ToString());

        case RegexRepeater.EqualToX:
          //[reg{x}]
          regexBuildHelper = new StringBuilder("(").Append(regex).Append("{").Append(x).Append("})");
          return new Regex(regexBuildHelper.ToString());


        case RegexRepeater.MoreThanX:
          //[reg{x,}]
          regexBuildHelper = new StringBuilder("(").Append(regex).Append("{").Append(x).Append(",})");
          return new Regex(regexBuildHelper.ToString());

        default:
          //case RegexRepeater.MoreThanXLessThanY:
          //[reg{x,y}]
          regexBuildHelper = new StringBuilder("(").Append(regex).Append("{").Append(x).Append(',').Append(y).Append("})");
          return new Regex(regexBuildHelper.ToString());
      }
    }

    public static Regex AppendOrRegex(this Regex regex, Regex alternativeRegex)
    {
      return new Regex(new StringBuilder("(").Append(regex).Append('|').Append(alternativeRegex).Append(')').ToString());
    }

    public static Regex CreateWordRegex(string word)
    {
      return new Regex("(" + word + ")");
    }

    public static Regex CreateSignedIntRegex()
    {
      return new Regex(@"([-+]\\d+)");
    }

    public static Regex CreateNonSignedIntRegex()
    {
      return new Regex(@"(\\d+)");
    }

    public static Regex CreateCharRegex(params char[] characters)
    {
      StringBuilder apendingRegex = new StringBuilder("[");
      foreach (char character in characters)
        apendingRegex.Append(character);
      apendingRegex.Append(']');

      return new Regex(apendingRegex.ToString());
    }

    public static Regex CreateOrRegex()
    {
      return new Regex("|");
    }

  }

  public enum RegexRepeater
  {
    MoreOrEqualZero,
    MoreOrEqualToOne,
    OneOrZero,
    EqualToX,
    MoreThanX,
    MoreThanXLessThanY
  }
}