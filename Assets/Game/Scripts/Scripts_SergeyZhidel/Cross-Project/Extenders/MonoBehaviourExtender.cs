﻿using System;
using System.Collections;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class MonoBehaviourExtender
  {
    public static CoroutineTracker<TResult> StartCoroutine<TResult>(this MonoBehaviour script, IEnumerator coroutine)
    {
      CoroutineTracker<TResult> tracker = coroutine.GetTrack<TResult>();
      script.StartCoroutine(tracker.CoroutineTarget);
      return tracker;
    }

    public static Coroutine StartWithDelay(this MonoBehaviour script, float seconds, IEnumerator currrentCoroutine)
    {
      return script.StartCoroutine(IEnumeratorExtender.RunWithDelay(seconds, currrentCoroutine));
    }

    public static Coroutine StartWithDelay(this MonoBehaviour script, float seconds, Action action)
    {
      return script.StartCoroutine(IEnumeratorExtender.RunWithDelay(seconds, action));
    }

    public static Coroutine StartWhenConditionBecomeFalse(this MonoBehaviour script, IEnumerator currrentCoroutine, Func<bool> waitCondotion)
    {
      return script.StartCoroutine(IEnumeratorExtender.RunWhenConditionBecomeFalse(currrentCoroutine, waitCondotion));
    }

    public static Coroutine StartWhenConditionBecomeFalse(this MonoBehaviour script, Action action, Func<bool> waitCondotion)
    {
      return script.StartCoroutine(IEnumeratorExtender.RunWhenConditionBecomeFalse(action, waitCondotion));
    }

    public static Coroutine StarWhenConditionBecomeTrue(this MonoBehaviour script, Action action, Func<bool> waitCondotion)
    {
      return script.StartCoroutine(IEnumeratorExtender.RunWhenConditionBecomeTrue(action, waitCondotion));
    }

    public static Coroutine StarWhenConditionBecomeTrue(this MonoBehaviour script, IEnumerator currrentCoroutine, Func<bool> waitCondotion)
    {
      return script.StartCoroutine(IEnumeratorExtender.RunWhenConditionBecomeTrue(currrentCoroutine, waitCondotion));
    }

    public static GameObject Find(this MonoBehaviour script, string nameObj)
    {
      return GameObject.Find(nameObj);
    }

    public static GameObject[] FindAll(this MonoBehaviour script, string nameObj)
    {
      return GameObjectExtender.FindAll(nameObj);
    }

    public static GameObject[] FindAllNameMatch(this MonoBehaviour script, Regex regex)
    {
      return Resources.FindObjectsOfTypeAll<GameObject>()
      .Where(obj => regex.IsMatch(obj.name)).ToArray();
    }

    public static GameObject FindFirstNameMatch(this MonoBehaviour script, Regex regex)
    {
      return Resources.FindObjectsOfTypeAll<GameObject>()
        .First(obj => regex.IsMatch(obj.name));
    }

    /// <summary>
    /// Find even if object is hided
    /// </summary>
    /// <param name="someObj"></param>
    /// <param name="name"></param>
    /// <returns></returns>
    public static GameObject FindExtended(this MonoBehaviour script, string objName)
    {
      var result = Resources.FindObjectsOfTypeAll<GameObject>()
        .First(obj => obj.name.Equals(objName)).gameObject;
      if (result != null)
        return result;
      throw new NullReferenceException("No such GameObject: " + objName);
    }

    public static GameObject InstantiateWithInit(this MonoBehaviour script, GameObject prefab, Vector3 position, Quaternion rotation, Action<GameObject> onInit)
    {
      GameObject obj = GameObject.Instantiate(prefab, position, rotation) as GameObject;
      obj.SetActive(false);
      onInit.InvokeSafe(obj);
      obj.SetActive(true);
      return obj;
    }

    public static GameObject InstantiateWithInit(this MonoBehaviour script, GameObject prefab, Action<GameObject> onInit)
    {
      GameObject obj = GameObject.Instantiate(prefab);
      obj.SetActive(false);
      onInit.InvokeSafe(obj);
      obj.SetActive(true);
      return obj;
    }
  }
}