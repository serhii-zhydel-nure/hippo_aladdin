﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public enum Direction
  {
    Right, Left, Up, Down, Back, Forward
  }


  public static class DirectionExtender
  {
    private static readonly Dictionary<Direction, Vector3> Vector3Associations
      = new Dictionary<Direction, Vector3>
      {
      { Direction.Left, Vector3.left},
      { Direction.Right, Vector3.right},
      { Direction.Up, Vector3.up},
      { Direction.Down, Vector3.down},
      { Direction.Back, Vector3.back},
      { Direction.Forward, Vector3.forward}
    };

    public static Vector3 ToVector3(this Direction direction)
    {
      return Vector3Associations[direction];
    }

    public static Axis3D ToAxis3D(this Direction direction)
    {
      switch (direction)
      {
        case Direction.Right:
        case Direction.Left:
          return Axis3D.X;

        case Direction.Up:
        case Direction.Down:
          return Axis3D.Y;

        default:
        //case Direction.Forward:
        //case Direction.Back:
          return Axis3D.Z;
      }
    }


  }
}