﻿using System;
using System.Collections;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{

  public static class IEnumeratorExtender
  {
    public static IEnumerator GetEmpty()
    {
      yield return null;
    }

    public static IEnumerator Append(this IEnumerator currrentCoroutine, params IEnumerator[] nextCoroutines)
    {
      yield return currrentCoroutine;
      foreach (IEnumerator nextCoroutine in nextCoroutines)
        yield return nextCoroutine;
    }

    public static IEnumerator Append(this IEnumerator currrentCoroutine, Action action)
    {
      yield return currrentCoroutine;
      action.Invoke();
    }

    public static IEnumerator Append<T>(this IEnumerator currrentCoroutine, T param, Action<T> action)
    {
      yield return currrentCoroutine;
      action.Invoke(param);
    }

    public static IEnumerator AppendDelay(this IEnumerator currrentCoroutine, float seconds)
    {
      yield return currrentCoroutine;
      yield return new WaitForSeconds(seconds);
    }

    public static IEnumerator RunWithDelay(float seconds, IEnumerator currrentCoroutine)
    {
      yield return new WaitForSeconds(seconds);
      yield return currrentCoroutine;
    }

    public static IEnumerator RunWhenConditionBecomeTrue(IEnumerator currrentCoroutine, Func<bool> waitCondotion)
    {
      yield return new WaitUntil(waitCondotion);
      yield return currrentCoroutine;
    }

    public static IEnumerator RunWhenConditionBecomeTrue(Action action, Func<bool> waitCondotion)
    {
      yield return new WaitUntil(waitCondotion);
      action.Invoke();
    }

    public static IEnumerator RunWhenConditionBecomeFalse(IEnumerator currrentCoroutine, Func<bool> waitCondotion)
    {
      yield return new WaitWhile(waitCondotion);
      yield return currrentCoroutine;
    }

    public static IEnumerator RunWhenConditionBecomeFalse(Action action, Func<bool> waitCondotion)
    {
      yield return new WaitWhile(waitCondotion);
      action.Invoke();
    }

    public static IEnumerator RunWithDelay(float seconds, Action action)
    {
      yield return new WaitForSeconds(seconds);
      action.Invoke();
    }

    private static IEnumerator Track(this IEnumerator currrentCoroutine, CoroutineTracker tracker)
    {
      tracker.State = CoroutineState.Working;
      yield return currrentCoroutine;
      tracker.State = CoroutineState.Finished;
    }
    public static CoroutineTracker GetTrack(this IEnumerator currrentCoroutine)
    {
      CoroutineTracker tracker = new CoroutineTracker();
      IEnumerator trackedCoroutine = currrentCoroutine.Track(tracker);
      tracker.CoroutineTarget = trackedCoroutine;
      return tracker;
    }


    private static IEnumerator Track<TResult>(this IEnumerator currrentCoroutine, CoroutineTracker<TResult> tracker)
    {
      tracker.State = CoroutineState.Working;
      yield return currrentCoroutine;
      object yielded = currrentCoroutine.Current;
      if (yielded is TResult)
        tracker.Result = (TResult)yielded;
      else
        throw new InvalidCastException("Return value is not type of " + typeof(TResult).Name);
      tracker.State = CoroutineState.Finished;
    }
    public static CoroutineTracker<TResult> GetTrack<TResult>(this IEnumerator currrentCoroutine)
    {
      CoroutineTracker<TResult> tracker = new CoroutineTracker<TResult>();
      IEnumerator trackedCoroutine = currrentCoroutine.Track(tracker);
      tracker.CoroutineTarget = trackedCoroutine;
      return tracker;
    }


    public static IEnumerator AppendWaitWhile(this IEnumerator currrentCoroutine, Func<bool> waitCondotion)
    {
      yield return currrentCoroutine;
      yield return new WaitWhile(waitCondotion);
    }

    public static IEnumerator AppendWaitUntil(this IEnumerator currrentCoroutine, Func<bool> waitCondotion)
    {
      yield return currrentCoroutine;
      yield return new WaitUntil(waitCondotion);
    }
  }

 

  public class CoroutineTracker
  {
    public IEnumerator CoroutineTarget;

    public CoroutineState State = CoroutineState.NotStarted;

    public bool IsCoroutineFinished { get { return State == CoroutineState.Finished; } }
  }

  public class CoroutineTracker<TResult> : CoroutineTracker
  {
    private TResult _result;

    public TResult Result
    {
      get
      {
        if (!IsCoroutineFinished)
          throw new Exception("Coroutine is not finished yet.");

        return _result;
      }
      set { _result = value; }
    }
  }


  public enum CoroutineState
  {
    NotStarted, Working, Finished
  }
}