﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class NumberExtender
  {
    /// <summary>
    /// Приблизительно равны с погрешностью
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <param name="epsilon"></param>
    /// <returns></returns>
    public static bool NearlyEqual(this float a, float b, float epsilon = 0.025f)
    {
      double absA = Math.Abs(a);
      double absB = Math.Abs(b);
      double diff = Math.Abs(a - b);

      if (a == b)
      { // shortcut, handles infinities
        return true;
      }
      if (a == 0 || b == 0 || diff < Double.Epsilon)
      {
        // a or b is zero or both are extremely close to it
        // relative error is less meaningful here
        return diff < epsilon;
      }
      // use relative error
      return diff / (absA + absB) < epsilon;
    }

   public static int Clamp(this int num, int min, int max)
    {
      return Mathf.Clamp(num, min, max);
    }

    public static float Clamp(this float num, float min, float max)
    {
      return Mathf.Clamp(num, min, max);
    }

    /// <summary>
    /// Целая часть
    /// </summary>
    /// <param name="num"></param>
    /// <returns></returns>
    public static float Floor(this float num)
    {
      return Mathf.Floor(num);
    }

    /// <summary>
    /// Целая часть
    /// </summary>
    /// <param name="num"></param>
    /// <returns></returns>
    public static int FloorToInt(this float num)
    {
      return Mathf.FloorToInt(num);
    }

    public static bool IsBetween(this int current, int low, int high)
    {
      return ((current > low) && (current < high));
    }
    public static bool IsBetween(this float current, float low, float high)
    {
      return ((current > low) && (current < high));
    }

    /// <summary>
    /// Округлить до целого
    /// </summary>
    /// <param name="f"></param>
    /// <returns></returns>
    public static float Round(this float f)
    {
      return Mathf.Round(f);
    }
    public static float Round(this float f, float roundFactor)
    {
      return ((f / roundFactor).Round() * roundFactor);
    }
    public static int RoundToInt(this float f)
    {
      return Mathf.RoundToInt(f);
    }
    public static int RoundToInt(this float f, float roundFactor)
    {
      return f.Round(roundFactor).RoundToInt();
    }

    public static float Pow(this int i, float power)
    {
      return Mathf.Pow(i, power);
    }
    public static float Pow(this float f, float power)
    {
      return Mathf.Pow(f, power);
    }

    /// <summary>
    /// Это num +/- (num * delta)
    /// </summary>
    /// <param name="num"></param>
    /// <param name="delta"></param>
    /// <returns></returns>
    public static float RandomizeWithDelta(this float num, float delta)
    {
      return (num + Random.Range(-num * delta, num * delta));
    }

    public static T ToEnum<T>(this int valueToParse) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (Enum.IsDefined(typeof(T), valueToParse))
      {
        return (T)(object)valueToParse;
      }
      throw new ArgumentException("wrong string to parce: " + valueToParse);
    }

    public static bool TryParceToEnum<T>(this int valueToParse, ref T res) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (Enum.IsDefined(typeof(T), valueToParse))
      {
        res = (T)(object)valueToParse;
        return true;
      }

      return false;
    }
  }
}