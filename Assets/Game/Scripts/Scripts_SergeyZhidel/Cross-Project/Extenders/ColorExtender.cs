﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class ColorExtender
  {
    public static Color Orange = FromHex("FFBE5A00");
    public static Color Brown = FromHex("8C5028FF");
    public static Color Violet = FromHex("DC50D2FF");

    /// <summary>
    /// toValue - 0-1.0f
    /// </summary>
    /// <param name="color"></param>
    /// <param name="toValue"></param>
    /// <returns></returns>
    public static Color SetBrightness(this Color color, float toValue)
    {
      float hue, saturation, brightness;
      Color.RGBToHSV(color, out hue, out saturation, out brightness);
      brightness = toValue;
      color = Color.HSVToRGB(hue, saturation, brightness);
      return color;
    }

    public static float GetBrightness(this Color color)
    {
      float hue, saturation, brightness;
      Color.RGBToHSV(color, out hue, out saturation, out brightness);
      return brightness;
    }

    /// <summary>
    /// toValue - 0-1.0f
    /// </summary>
    /// <param name="color"></param>
    /// <param name="toValue"></param>
    /// <returns></returns>
    public static Color SetHue(this Color color, float toValue)
    {
      float hue, saturation, brightness;
      Color.RGBToHSV(color, out hue, out saturation, out brightness);
      hue = toValue;
      color = Color.HSVToRGB(hue, saturation, brightness);
      return color;
    }

    public static float GetHue(this Color color)
    {
      float hue, saturation, brightness;
      Color.RGBToHSV(color, out hue, out saturation, out brightness);
      return hue;
    }

    /// <summary>
    /// toValue - 0-1.0f
    /// </summary>
    /// <param name="color"></param>
    /// <param name="toValue"></param>
    /// <returns></returns>
    public static Color SetSaturation(this Color color, float toValue)
    {
      float hue, saturation, brightness;
      Color.RGBToHSV(color, out hue, out saturation, out brightness);
      saturation = toValue;
      color = Color.HSVToRGB(hue, saturation, brightness);
      return color;
    }

    public static float GetSaturation(this Color color)
    {
      float hue, saturation, brightness;
      Color.RGBToHSV(color, out hue, out saturation, out brightness);
      return saturation;
    }

    public static string ToHex(this Color color)
    {
      string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
      return hex;
    }

    public static Color FromHex(string hex)
    {
      hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
      hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
      byte a = 255;//assume fully visible unless specified in hex
      byte r = byte.Parse(hex.Substring(0, 2), NumberStyles.HexNumber);
      byte g = byte.Parse(hex.Substring(2, 2), NumberStyles.HexNumber);
      byte b = byte.Parse(hex.Substring(4, 2), NumberStyles.HexNumber);
      //Only use alpha if the string has enough characters
      if (hex.Length == 8)
      {
        a = byte.Parse(hex.Substring(4, 2), NumberStyles.HexNumber);
      }
      return new Color32(r, g, b, a);
    }

    public static Color GetRandomColorFromStandartsExcept(Color[] exceptionList)
    {
      List<Color> selectionList = new List<Color>
      {
        Color.black,
        Color.blue,
        Color.clear,
        Color.cyan,
        Color.gray,
        Color.magenta,
        Color.red,
        Color.white,
        Color.yellow,
        Orange 
      };

     return selectionList.Where(color => !exceptionList.Contains(color)).ToArray().GetRandomItem();
    }
  }
}
