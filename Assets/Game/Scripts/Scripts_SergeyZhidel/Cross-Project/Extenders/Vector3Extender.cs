﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class Vector3Extender
  {
    /// <summary>
    /// Returns normalized vector-direction between two points
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="destinationPoint"></param>
    /// <returns></returns>
    public static Vector3 GetDirectionVectorToPoint(this Vector3 startPoint, Vector3 destinationPoint)
    {
      return (destinationPoint - startPoint).normalized;
    }

    public static Vector3 GetDirectionVectorToPoint(this Vector3 startPoint, GameObject obj)
    {
      return (obj.transform.position - startPoint).normalized;
    }

    public static Direction GetAxisDirection(this Vector3 directionVector, Axis3D axis)
    {
      switch (axis)
      {
        case Axis3D.X:
          if (directionVector.GetValueOfAxis(axis) < 0)
            return Direction.Left;
          return Direction.Right;

        case Axis3D.Y:
          if (directionVector.GetValueOfAxis(axis) < 0)
            return Direction.Down;
          return Direction.Up;

        default: //Z
          if (directionVector.GetValueOfAxis(axis) < 0)
            return Direction.Back;
          return Direction.Forward;
      }
    }

    public  static Vector3 GetBetweenPoint(Vector3 position1, Vector3 position2)
    {
      return new Vector3(
        (position1.x + position2.x) / 2,
        (position1.y + position2.y) / 2,
        (position1.z + position2.z) / 2);
    }

    /// <summary>
    /// To know where is second point relatively to first
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="destinationPoint"></param>
    /// <param name="axis"></param>
    /// <returns></returns>
    public static Direction GetAxisDirection(this Vector3 startPoint, Vector3 destinationPoint, Axis3D axis)
    {
      return GetDirectionVectorToPoint(startPoint, destinationPoint).GetAxisDirection(axis);
    }

    /// <summary>
    /// To know where is second point relatively to first
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="destinationPoint"></param>
    /// <param name="axis"></param>
    /// <returns></returns>
    public static Direction GetAxisDirection(this Vector3 startPoint, GameObject obj, Axis3D axis)
    {
      return GetDirectionVectorToPoint(startPoint, obj.transform.position).GetAxisDirection(axis);
    }


    public static bool NearlyEqual(this Vector3 sourseVector3, Vector3 checkedVector3, float epsilon = 0.05f)
    {
      return (sourseVector3.x.NearlyEqual(checkedVector3.x, epsilon) &&
              sourseVector3.y.NearlyEqual(checkedVector3.y, epsilon) &&
              sourseVector3.z.NearlyEqual(checkedVector3.z, epsilon));
    }

    public static Vector3 SubFromAxis(this Vector3 vector, float number, Axis3D axis3D)
    {
      switch (axis3D)
      {
        case Axis3D.X:
          vector -= new Vector3(number, 0, 0);
          break;

        case Axis3D.Y:
          vector -= new Vector3(0, number, 0);
          break;

        case Axis3D.Z:
          vector -= new Vector3(0, 0, number);
          break;
      }
      return vector;
    }

    public static Vector3 AddToAxis(this Vector3 vector, float number, Axis3D axis3D)
    {
      switch (axis3D)
      {
        case Axis3D.X:
          vector += new Vector3(number, 0, 0);
          break;

        case Axis3D.Y:
          vector += new Vector3(0, number, 0);
          break;

        case Axis3D.Z:
          vector += new Vector3(0, 0, number);
          break;
      }
      return vector;
    }

    public static Vector3 MulByAxis(this Vector3 vector, float number, Axis3D axis3D)
    {
      switch (axis3D)
      {
        case Axis3D.X:
          vector = new Vector3(vector.x * number, vector.y, vector.z);
          break;

        case Axis3D.Y:
          vector = new Vector3(vector.x, vector.y * number, vector.z);
          break;

        case Axis3D.Z:
          vector = new Vector3(vector.x, vector.y, vector.z * number);
          break;
      }
      return vector;
    }

    public static Vector3 DivIntoAxis(this Vector3 vector, float number, Axis3D axis3D)
    {
      switch (axis3D)
      {
        case Axis3D.X:
          vector = new Vector3(vector.x / number, vector.y, vector.z);
          break;

        case Axis3D.Y:
          vector = new Vector3(vector.x, vector.y / number, vector.z);
          break;

        case Axis3D.Z:
          vector = new Vector3(vector.x, vector.y, vector.z / number);
          break;
      }
      return vector;
    }


    public static float GetValueOfAxis(this Vector3 vector, Axis3D axis3D)
    {
      switch (axis3D)
      {
        case Axis3D.X:
          return vector.x;

        case Axis3D.Y:
          return vector.y;

        default:
          return vector.z;
      }
    }

    public static Vector3 SetValueOfAxis(this Vector3 vector, float value, Axis3D axis3D)
    {
      switch (axis3D)
      {
        case Axis3D.X:
          return new Vector3(value, vector.y, vector.z);

        case Axis3D.Y:
          return new Vector3(vector.x, value, vector.z);

        default:
          return new Vector3(vector.x, vector.y, value);
      }
    }

    public static Vector3 GetRandomVector(Vector3 vectorFrom, Vector3 vectorTo)
    {
      return new Vector3(Rand.Range(vectorFrom.x, vectorTo.x),
                         Rand.Range(vectorFrom.y, vectorTo.y),
                         Rand.Range(vectorFrom.z, vectorTo.z)
                         );
    }

    public static Vector3 GetRandomVectorBasedOThis(this Vector3 vector, float valueFrom, float valueTo, Axis3D randomisableAxis)
    {
      return vector.SetValueOfAxis(Rand.Range(valueFrom, valueTo), randomisableAxis);
    }

    public static Vector3 GetRandomSimilarVector(this Vector3 vector, float lessDiffernce, float moreDiffernce, Axis3D randomisableAxis)
    {
      float randomizableAxisValue = vector.GetValueOfAxis(randomisableAxis);
      return vector.SetValueOfAxis(Rand.Range(randomizableAxisValue - lessDiffernce, randomizableAxisValue + moreDiffernce), randomisableAxis);
    }

    public static Vector2 ToVector2(this Vector3 vector)
    {
      return vector;
    }

    public static Vector3 ClampV3(this Vector3 vec, Vector3 minXYZ, Vector3 maxXYZ)
    {
      return new Vector3(
        Mathf.Clamp(vec.x, minXYZ.x, maxXYZ.x),
        Mathf.Clamp(vec.y, minXYZ.y, maxXYZ.y),
        Mathf.Clamp(vec.z, minXYZ.z, maxXYZ.z)
        );
    }
  }

  public enum Axis3D
  {
    X, Y, Z
  }
}