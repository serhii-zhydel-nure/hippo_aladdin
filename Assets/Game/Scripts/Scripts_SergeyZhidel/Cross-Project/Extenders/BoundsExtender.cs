﻿using System.Linq;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class BoundsExtender
  {
    public static bool Contains(this Bounds bounds, Bounds otherBounds)
    {
      //все ли углы входят в bounds?
      return otherBounds.GetCorners3D().All(otherBoundsCorner => bounds.Contains(otherBoundsCorner));
    }

    /// <summary>
    /// Левый верхний задний    (x- y+ z-), правый верхний задний     (x+ y+ z-), 
    /// левый нижний задний     (x- y- z-), правый нижний задний      (x+ y- z-),
    /// Левый верхний передний  (x- y+ z+), правый верхний передний   (x+ y+ z+), 
    /// левый нижний передний   (x- y- z+), правый нижний передний    (x+ y- z+),
    /// в 2д нужны первые 4
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static Vector3[] GetCorners3D(this Bounds bounds)
    {
      Vector3 fallBorders = bounds.min;
      Vector3 growBorders = bounds.max;

      return new[]
      {
        // Левый верхний задний    (x- y+ z-),                        правый верхний задний       (x+ y+ z-), 
        new Vector3(fallBorders.x, growBorders.y, fallBorders.z), new Vector3(growBorders.x, growBorders.y, fallBorders.z),

        // левый нижний задний     (x- y- z-),                        правый нижний задний        (x+ y- z-),
        new Vector3(fallBorders.x, fallBorders.y, fallBorders.z), new Vector3(growBorders.x, fallBorders.y, fallBorders.z),

        // Левый верхний передний  (x- y+ z+),                        правый верхний передний     (x+ y+ z+), 
        new Vector3(fallBorders.x, growBorders.y, growBorders.z), new Vector3(growBorders.x, growBorders.y, growBorders.z),

        // левый нижний передний   (x- y- z+),                        правый нижний передний      (x+ y- z+),
        new Vector3(fallBorders.x, fallBorders.y, growBorders.z), new Vector3(growBorders.x, fallBorders.y, growBorders.z)
      };
    }

    /// <summary>
    /// Левый верхний задний    (x- y+ z-),   правый верхний задний     (x+ y+ z-), 
    /// левый нижний задний     (x- y- z-),   правый нижний задний      (x+ y- z-)
    /// в 2д нужны первые 4
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static Vector3[] GetCorners2D(this Bounds bounds)
    {
      Vector3 fallBorders = bounds.min;
      Vector3 growBorders = bounds.max;

      return new[]
      {
        // Левый верхний задний    (x- y+ z-),                        правый верхний задний       (x+ y+ z-), 
        new Vector3(fallBorders.x, growBorders.y, fallBorders.z), new Vector3(growBorders.x, growBorders.y, fallBorders.z),

        // левый нижний задний     (x- y- z-),                        правый нижний задний        (x+ y- z-),
        new Vector3(fallBorders.x, fallBorders.y, fallBorders.z), new Vector3(growBorders.x, fallBorders.y, fallBorders.z)
      };
    }

  }
}