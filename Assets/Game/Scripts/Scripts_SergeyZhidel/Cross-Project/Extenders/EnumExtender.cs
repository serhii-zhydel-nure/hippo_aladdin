﻿using System;

namespace Scripts_SergeyZhidel.Extenders
{

  public static class EnumExtender
  {
    #region Enum with [Flags] 
    public static bool HasFlags<T>(this T flags, T flagToTest) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (flags.GetType().GetCustomAttribute<FlagsAttribute>() == null)
        throw new ArgumentException("This enum doesn't have \"Flags\" attribute");

      if (Convert.ToUInt32(flagToTest) == 0)
        throw new ArgumentOutOfRangeException("flagToTest", "Value must not be 0");

      return (Convert.ToUInt32(flags) & Convert.ToUInt32(flagToTest)) == Convert.ToUInt32(flagToTest);
    }

    public static bool IsClear<T>(this T flags, T flagToTest) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (flags.GetType().GetCustomAttribute<FlagsAttribute>() == null)
        throw new ArgumentException("This enum doesn't have \"Flags\" attribute");

      if (Convert.ToUInt32(flagToTest) == 0)
        throw new ArgumentOutOfRangeException("flagToTest", "Value must not be 0");

      return !flags.HasFlags(flagToTest);
    }

    public static bool HasAny<T>(this T flags, T testFlags) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (flags.GetType().GetCustomAttribute<FlagsAttribute>() == null)
        throw new ArgumentException("This enum doesn't have \"Flags\" attribute");

      return ((Convert.ToUInt32(flags) & Convert.ToUInt32(testFlags)) != 0);
    }

    public static T SetFlag<T>(this T flags, T setFlags) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (flags.GetType().GetCustomAttribute<FlagsAttribute>() == null)
        throw new ArgumentException("This enum doesn't have \"Flags\" attribute");

      return (T)Enum.ToObject(typeof(T), Convert.ToUInt32(flags) | Convert.ToUInt32(setFlags));
    }

    public static T ClearFlags<T>(this T flags, T clearFlags) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (flags.GetType().GetCustomAttribute<FlagsAttribute>() == null)
        throw new ArgumentException("This enum doesn't have \"Flags\" attribute");

      return (T)Enum.ToObject(typeof(T), Convert.ToUInt32(flags) & ~Convert.ToUInt32(clearFlags));
    }

    public static void ForEach<T>(this Enum flags, Action<T> processFlag) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (flags.GetType().GetCustomAttribute<FlagsAttribute>() == null)
        throw new ArgumentException("This enum doesn't have \"Flags\" attribute");

      if (processFlag == null) throw new ArgumentNullException("processFlag");
      for (UInt32 bit = 1; bit != 0; bit <<= 1)
      {
        UInt32 temp = Convert.ToUInt32(flags) & bit;
        if (temp != 0) processFlag((T)Enum.ToObject(typeof(T), temp));
      }
    }
    #endregion
  }
}