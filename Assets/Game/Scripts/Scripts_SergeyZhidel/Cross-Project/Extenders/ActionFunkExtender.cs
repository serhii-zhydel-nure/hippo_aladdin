﻿using System;
using System.Linq;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class ActionFunkExtender
  {
    public static void InvokeSafe(this Action action)
    {
      if (action.HasSubscriptions()) action.Invoke();
    }
    public static void InvokeSafe<P>(this Action<P> action, P param)
    {
      if (action.HasSubscriptions()) action.Invoke(param);
    }
    public static void InvokeSafe<P1, P2>(this Action<P1, P2> action, P1 param1, P2 param2)
    {
      if (action.HasSubscriptions()) action.Invoke(param1, param2);
    }

    public static R InvokeSafe<R>(this Func<R> funk)
    {
      if (funk.HasSubscriptions())
        return funk.Invoke();
      return default(R);
    }
    public static R InvokeSafe<P, R>(this Func<P, R> funk, P param)
    {
      if (funk.HasSubscriptions())
        return funk.Invoke(param);
      return default(R);
    }
    public static R InvokeSafe<P1, P2, R>(this Func<P1, P2, R> funk, P1 param1, P2 param2)
    {
      if (funk.HasSubscriptions())
        return funk.Invoke(param1, param2);
      return default(R);
    }


    public static void InvokeOnlyActiveMonoBehaviours(this Action action)
    {
      if (action.HasSubscriptions())
        foreach (Delegate @delegate in action.GetInvocationList().Where(@delegate => @delegate.Target is MonoBehaviour))
        {
          @delegate.Method.Invoke(@delegate.Target, null);
        }
    }
    public static void InvokeOnlyActiveMonoBehaviours<P>(this Action<P> action, P param)
    {
      if (action.HasSubscriptions())
        foreach (Delegate @delegate in action.GetInvocationList().Where(@delegate => @delegate.Target is MonoBehaviour))
        {
          @delegate.Method.Invoke(@delegate.Target, new object[] { param });
        }
    }


    public static bool HasSubscriptions(this Action action)
    {
      return action != null && action.GetInvocationList().Any();
    }
    public static bool HasSubscriptions<P>(this Action<P> action)
    {
      return action != null && action.GetInvocationList().Any();
    }
    public static bool HasSubscriptions<P1, P2>(this Action<P1, P2> action)
    {
      return action != null && action.GetInvocationList().Any();
    }

    public static bool HasSubscriptions<R>(this Func<R> funk)
    {
      return funk != null && funk.GetInvocationList().Any();
    }
    public static bool HasSubscriptions<P, R>(this Func<P, R> funk)
    {
      return funk != null && funk.GetInvocationList().Any();
    }
    public static bool HasSubscriptions<P1, P2, R>(this Func<P1, P2, R> funk)
    {
      return funk != null && funk.GetInvocationList().Any();
    }


    public static void UnsubscribeSafe(ref Action action, Action desubscriber)
    {
      action = (Action)Delegate.RemoveAll(action, desubscriber);
    }
    public static void UnsubscribeSafe<P>(ref Action<P> action, Action<P> desubscriber)
    {
      action = (Action<P>)Delegate.RemoveAll(action, desubscriber);
    }
    public static void UnsubscribeSafe<P1, P2>(ref Action<P1, P2> action, Action<P1, P2> desubscriber)
    {
      action = (Action<P1, P2>)Delegate.RemoveAll(action, desubscriber);
    }

    public static void UnsubscribeSafe<R>(ref Func<R> funk, Func<R> desubscriber)
    {
      funk = (Func<R>)Delegate.RemoveAll(funk, desubscriber);
    }
    public static void UnsubscribeSafe<P, R>(ref Func<P, R> funk, Func<P, R> desubscriber)
    {
      funk = (Func<P, R>)Delegate.RemoveAll(funk, desubscriber);
    }
    public static void UnsubscribeSafe<P1, P2, R>(ref Func<P1, P2, R> funk, Func<P1, P2, R> desubscriber)
    {
      funk = (Func<P1, P2, R>)Delegate.RemoveAll(funk, desubscriber);
    }
  }
}