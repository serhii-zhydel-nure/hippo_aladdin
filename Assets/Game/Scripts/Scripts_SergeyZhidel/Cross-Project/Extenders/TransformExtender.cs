﻿using System;
using System.Collections;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class TransformExtender
  {
    public static void SetOn2DPosition(this Transform transform, string objName)
    {
      transform.SetOn2DPosition(GameObject.Find(objName));
    }

    public static Vector2 Get2DPosition(this Transform transform)
    {
      return transform.position;
    }

    public static void SetOn2DPosition(this Transform transform, GameObject someObj)
    {
      Vector3 pos = someObj.transform.position;
      pos.z = transform.position.z;
      transform.position = pos;
    }

    public static void SetOn2DPosition(this Transform transform, Vector3 position)
    {
      position.z = transform.position.z;
      transform.position = position;
    }

    public static void SetToTransformLocal(this Transform objTransform, Transform destinationTransform)
    {
      objTransform.localPosition = destinationTransform.localPosition;
      objTransform.localScale = destinationTransform.localScale;
      objTransform.localRotation = Quaternion.Euler(destinationTransform.localRotation.eulerAngles);
    }

    public static void SetToTransformGlobal(this Transform objTransform, Transform destinationTransform)
    {
      objTransform.position = destinationTransform.position;
      objTransform.localScale = destinationTransform.localScale;
      objTransform.rotation = Quaternion.Euler(destinationTransform.rotation.eulerAngles);
    }

    public static IEnumerator SetToTransformOverSecondsNoMoreThen360Rotation(this Transform objTransform, Transform destinationTransform,
      float seconds)
    {
      float elapsedTime = 0;

      Vector3 startingPos = objTransform.position;
      Vector3 startingScale = objTransform.localScale;
      Vector3 startingRotation = objTransform.rotation.eulerAngles;

      while (elapsedTime < seconds)
      {
        objTransform.position = Vector3.Lerp(startingPos, destinationTransform.position, elapsedTime / seconds);
        objTransform.localScale = Vector3.Lerp(startingScale, destinationTransform.localScale, elapsedTime / seconds);
        objTransform.rotation = Quaternion.Lerp(Quaternion.Euler(startingRotation), Quaternion.Euler(destinationTransform.rotation.eulerAngles), elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return null;
      }

      objTransform.position = destinationTransform.position;
      objTransform.localScale = destinationTransform.localScale;
      objTransform.rotation = Quaternion.Euler(destinationTransform.rotation.eulerAngles);
    }

    public static IEnumerator SetOnPositionAndRotationOverSecondsNoMoreThen360(this Transform objTransform, Transform destinationTransform,
      float seconds)
    {
      float elapsedTime = 0;

      Vector3 startingPos = objTransform.position;
      Vector3 startingRotation = objTransform.rotation.eulerAngles;

      while (elapsedTime < seconds)
      {
        objTransform.position = Vector3.Lerp(startingPos, destinationTransform.position, elapsedTime / seconds);
        objTransform.rotation = Quaternion.Lerp(Quaternion.Euler(startingRotation), Quaternion.Euler(destinationTransform.rotation.eulerAngles), elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return null;
      }

      objTransform.position = destinationTransform.position;
      objTransform.rotation = Quaternion.Euler(destinationTransform.rotation.eulerAngles);
    }

    public static IEnumerator SetToTransformOverSeconds(this Transform objTransform, Transform destinationTransform, float seconds)
    {
      float elapsedTime = 0;

      Vector3 startingPos = objTransform.position;
      Vector3 startingScale = objTransform.localScale;
      Vector3 startingRotation = objTransform.rotation.eulerAngles;

      while (elapsedTime < seconds)
      {
        objTransform.position = Vector3.Lerp(startingPos, destinationTransform.position, elapsedTime / seconds);
        objTransform.localScale = Vector3.Lerp(startingScale, destinationTransform.localScale, elapsedTime / seconds);
        objTransform.rotation = Quaternion.Euler(Vector3.Lerp(startingRotation, destinationTransform.rotation.eulerAngles, elapsedTime / seconds));
        elapsedTime += Time.deltaTime;
        yield return null;
      }

      objTransform.position = destinationTransform.position;
      objTransform.localScale = destinationTransform.localScale;
      objTransform.rotation = Quaternion.Euler(destinationTransform.rotation.eulerAngles);
    }

    public static IEnumerator SetToTransformOverSecondsWithNoRotation(this Transform objTransform, Transform destinationTransform, float seconds)
    {
      float elapsedTime = 0;

      Vector3 startingPos = objTransform.position;
      Vector3 startingScale = objTransform.localScale;

      while (elapsedTime < seconds)
      {
        objTransform.position = Vector3.Lerp(startingPos, destinationTransform.position, elapsedTime / seconds);
        objTransform.localScale = Vector3.Lerp(startingScale, destinationTransform.localScale, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return null;
      }

      objTransform.position = destinationTransform.position;
      objTransform.localScale = destinationTransform.localScale;
    }

    public static IEnumerator MoveToObjByAxisWithSpeed(this Transform walker, GameObject destination, Axis3D axis, float speed, float intend)
    {
      Vector3 walkerTempPos = walker.position;
      Vector3 destinationTempPos = destination.transform.position;
      while (!walkerTempPos.GetValueOfAxis(axis).NearlyEqual(destinationTempPos.GetValueOfAxis(axis) + intend))
      {
        float destinationAxisValue = destinationTempPos.GetValueOfAxis(axis);//значение оси в точке куда идем
        Vector3 pointToMovePosition = walkerTempPos.SetValueOfAxis(destinationAxisValue, axis).AddToAxis(intend, axis);
        walker.position = Vector2.MoveTowards(walkerTempPos, pointToMovePosition, Time.deltaTime * speed);
        yield return null;
        walkerTempPos = walker.position;
        destinationTempPos = destination.transform.position;
      }
    }


    public static IEnumerator MoveWithSpeed(this Transform objTransform, GameObject obj, float speed)
    {
      float zValue = objTransform.position.z;
      while (objTransform.position.ToVector2() != obj.transform.position.ToVector2())
      {
        objTransform.position = Vector2.MoveTowards(objTransform.position, obj.transform.position, Time.deltaTime * speed);
        objTransform.position = objTransform.position.SetValueOfAxis(zValue, Axis3D.Z);
        yield return null;
      }
    }

    public static IEnumerator MoveWithSpeed(this Transform objTransform, Vector2 destination, float speed)
    {
      while (objTransform.position.ToVector2() != destination)
      {
        objTransform.position = Vector3.MoveTowards(objTransform.position,
                                                  destination,
                                                  Time.deltaTime * speed);
        yield return null;//то же самое что и Update()
      }
    }

    public static IEnumerator MoveInDirectionWhile(this Transform objTransform, Vector3 direction, float speed, Func<bool> conditionWhileMove)
    {
      direction.Normalize();

      Vector3 constVector = direction * speed;

      while (true)
      {
        objTransform.Translate(constVector * Time.deltaTime);
        yield return null;//то же самое что и Update()
      }
    }

    public static IEnumerator MoveOverSeconds(this Transform objTransform, GameObject obj, float seconds)
    {
      float elapsedTime = 0;
      Vector3 startingPos = objTransform.position;
      while (elapsedTime < seconds)
      {
        Vector3 objPos = obj.transform.position;
        objPos.z = startingPos.z;
        objTransform.position = Vector3.Lerp(startingPos, objPos, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }
      Vector3 pos = obj.transform.position;
      pos.z = startingPos.z;
      objTransform.position = pos;
    }

    public static IEnumerator MoveOverSeconds(this Transform objTransform, Vector3 destination, float seconds)
    {
      destination.z = objTransform.position.z;

      float elapsedTime = 0;
      Vector3 startingPos = objTransform.position;
      while (elapsedTime < seconds)
      {
        objTransform.position = Vector3.Lerp(startingPos, destination, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      objTransform.position = destination;
    }

    public static IEnumerator ShiftInDirectionOverSeconds(this Transform objTransform, Vector3 shiftFromCurPos, float seconds)
    {
      shiftFromCurPos.z = objTransform.position.z;

      float elapsedTime = 0;
      while (elapsedTime < seconds)
      {
        Vector3 curShift = Vector3.Lerp(Vector3.zero, shiftFromCurPos, elapsedTime / seconds);
        objTransform.position += curShift;
        elapsedTime += Time.deltaTime;
        shiftFromCurPos -= curShift;
        yield return new WaitForEndOfFrame();
      }
    }


    public static IEnumerator ScaleOverSeconds(this Transform objTransform, Vector3 toSize, float seconds)
    {
      float elapsedTime = 0;
      Vector3 startingScale = objTransform.localScale;
      while (elapsedTime < seconds)
      {
        objTransform.localScale = Vector3.Lerp(startingScale, toSize, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }
      objTransform.localScale = toSize;
    }

    public static IEnumerator MoveAndScaleOverSeconds(this Transform objTransform, Vector3 destination, Vector3 toSize, float seconds)
    {
      float elapsedTime = 0;
      Vector3 startingPos = objTransform.position;
      Vector3 startingScale = objTransform.localScale;

      while (elapsedTime < seconds)
      {
        objTransform.position = Vector3.Lerp(startingPos, destination, elapsedTime / seconds);
        objTransform.localScale = Vector3.Lerp(startingScale, toSize, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      objTransform.position = destination;
      objTransform.localScale = toSize;
    }

    public static IEnumerator MoveAndScaleOverSeconds(this Transform objTransform, GameObject destination, Vector3 toSize, float seconds)
    {
      float elapsedTime = 0;
      Vector3 startingPos = objTransform.position;
      Vector3 startingScale = objTransform.localScale;

      Vector3 destinationPos = destination.transform.position;

      while (elapsedTime < seconds)
      {
        destinationPos = destination.transform.position;
        destinationPos.z = objTransform.position.z;
        objTransform.position = Vector3.Lerp(startingPos, destinationPos, elapsedTime / seconds);
        objTransform.localScale = Vector3.Lerp(startingScale, toSize, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      destinationPos = destination.transform.position;
      destinationPos.z = objTransform.position.z;
      objTransform.position = destinationPos;
      objTransform.localScale = toSize;
    }


    public static IEnumerator MoveAndScaleOverSeconds(this Transform objTransform, GameObject destination, Vector3 intend, Vector3 toSize, float seconds)
    {
      float elapsedTime = 0;
      Vector3 startingPos = objTransform.position;
      Vector3 startingScale = objTransform.localScale;

      Vector3 destinationPos = destination.transform.position + intend;

      while (elapsedTime < seconds)
      {
        destinationPos = destination.transform.position + intend;
        destinationPos.z = objTransform.position.z;
        objTransform.position = Vector3.Lerp(startingPos, destinationPos, elapsedTime / seconds);
        objTransform.localScale = Vector3.Lerp(startingScale, toSize, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      destinationPos = destination.transform.position + intend;
      destinationPos.z = objTransform.position.z;
      objTransform.position = destinationPos;
      objTransform.localScale = toSize;
    }

    public static IEnumerator MoveWithAccelerationAndScaleOverTime(this Transform objTransform, GameObject acceleratedObj, Vector3 destination, Vector3 toSize, float seconds)
    {
      float elapsedTime = 0;
      Vector3 startingPos = objTransform.position;
      Vector3 startingScale = objTransform.localScale;
      Vector3 previousPos = acceleratedObj.transform.position;

      while (elapsedTime < seconds)
      {
        Vector3 currentPos = acceleratedObj.transform.position;
        destination += currentPos - previousPos;
        previousPos = currentPos;

        objTransform.position = Vector3.Lerp(startingPos, destination, elapsedTime / seconds);
        objTransform.localScale = Vector3.Lerp(startingScale, toSize, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      objTransform.position = destination;
      objTransform.localScale = toSize;
    }

    public static IEnumerator MoveScaleRotateOverSeconds(this Transform objTransform, Vector3 destination, Vector3 toSize, Vector3 toAngle, float seconds)
    {
      float elapsedTime = 0;
      Vector3 startingPos = objTransform.position;
      Vector3 startingScale = objTransform.localScale;
      Vector3 startingAngle = objTransform.rotation.eulerAngles;

      while (elapsedTime < seconds)
      {
        objTransform.position = Vector3.Lerp(startingPos, destination, elapsedTime / seconds);
        objTransform.localScale = Vector3.Lerp(startingScale, toSize, elapsedTime / seconds);
        objTransform.eulerAngles = Vector3.Lerp(startingAngle, toAngle, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      objTransform.position = destination;
      objTransform.localScale = toSize;
      objTransform.eulerAngles = toAngle;
    }

    public static IEnumerator RotateOverSecondsOver180(this Transform objTransform, Vector3 toAngle, float seconds)
    {
      objTransform.rotation = Quaternion.Euler(toAngle);

      float elapsedTime = 0;
      Vector3 startingAngle = objTransform.rotation.eulerAngles;

      while (elapsedTime < seconds)
      {
        objTransform.rotation = Quaternion.Lerp(Quaternion.Euler(startingAngle), Quaternion.Euler(toAngle), elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      objTransform.rotation = Quaternion.Euler(toAngle);
    }

    public static IEnumerator RotateOverSecondsOver360(this Transform objTransform, Vector3 toAngle, float seconds)
    {
      float elapsedTime = 0;
      Vector3 startingAngle = objTransform.rotation.eulerAngles;

      while (elapsedTime < seconds)
      {
        objTransform.eulerAngles = Vector3.Lerp(startingAngle, toAngle, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }


    }


    public static SmoothFollow FollowToObject(this Transform transform, GameObject obj, Vector3 intend)
    {
      return transform.gameObject.AddComponentWithInit<SmoothFollow>((script) =>
      {
        //initialization before Awake
        script.FollovedObjTransform = transform;
        script.TargetTransform = obj.transform;
        script.Intend = intend;
      });
    }

    public static SmoothFollow FollowToObject(this Transform transform, GameObject obj)
    {
      return transform.gameObject.AddComponentWithInit<SmoothFollow>((script) =>
      {
        //initialization before Awake
        script.FollovedObjTransform = transform;
        script.TargetTransform = obj.transform;
        script.Intend = (transform.position - obj.transform.position).ToVector2();
      });
    }


    public static IEnumerator CenterToObjectOverSeconds(this Transform thisObj, GameObject obj, float seconds)
    {
      Transform objectTransform = obj.transform;

      float elapsedTime = 0;
      Vector3 startingPos = thisObj.position;
      while (elapsedTime < seconds)
      {
        Vector2 lefp = Vector3.Lerp(startingPos, objectTransform.position, elapsedTime / seconds); ;
        thisObj.position = new Vector3(lefp.x, lefp.y, thisObj.position.z);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      thisObj.position = new Vector3(objectTransform.position.x, objectTransform.position.y, thisObj.position.z);
    }

    public static IEnumerator CenterToObjectAndScaleOverSeconds(this Transform thisObj, GameObject obj, Vector3 intend, Vector3 scale, float seconds)
    {
      Transform objectTransform = obj.transform;

      float elapsedTime = 0;
      Vector3 startingPos = thisObj.position;
      Vector3 startScale = thisObj.localScale;
      while (elapsedTime < seconds)
      {
        Vector2 lefpPos = Vector3.Lerp(startingPos, objectTransform.position + intend, elapsedTime / seconds);
        Vector2 lefpSc = Vector3.Lerp(startScale, scale, elapsedTime / seconds);
        thisObj.position = new Vector3(lefpPos.x, lefpPos.y, thisObj.position.z);
        thisObj.localScale = lefpSc;
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      thisObj.position = new Vector3(objectTransform.position.x, objectTransform.position.y, thisObj.position.z) + intend;
      thisObj.localScale = scale;
    }

    public static IEnumerator PulseSize(this Transform thisObj, params Tuple<Vector3, float>[] pairsSizeAndSeconds)
    {
      foreach (Tuple<Vector3, float> pair in pairsSizeAndSeconds)
      {
        yield return thisObj.ScaleOverSeconds(pair.Item1, pair.Item2);
      }
    }

    public static IEnumerator PulseSizeCycle(this Transform thisObj, params Tuple<Vector3, float>[] pairsSizeAndSeconds)
    {
      while (true)
      {
        foreach (Tuple<Vector3, float> pair in pairsSizeAndSeconds)
        {
          yield return thisObj.ScaleOverSeconds(pair.Item1, pair.Item2);
        }
      }
    }


    public static IEnumerator ShakePos(this Transform thisObj, params Tuple<Vector3, float>[] pairsShiftsAndSeconds)
    {
      foreach (Tuple<Vector3, float> pair in pairsShiftsAndSeconds)
      {
        yield return thisObj.ShiftInDirectionOverSeconds(pair.Item1, pair.Item2);
      }
    }

    public static Direction GetHorisontalPositionFromCenterOfScreen(this Transform thisObj)
    {
      return thisObj.position.x > Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, 0, 0)).x
        ? Direction.Right
        : Direction.Left;
    }
  }
}