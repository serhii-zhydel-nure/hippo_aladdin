﻿using System.Collections;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class Rigidbody2DExtender
  {
    public static IEnumerator WaitUntilVelocityStartChanging(this Rigidbody2D body)
    {
      Vector2 startVelocity = body.velocity;
      yield return new WaitUntil(() => body.velocity != startVelocity);
    }

    public static IEnumerator WaitUntilVelocityHasValue(this Rigidbody2D body, Axis2D axis2D, float value )
    {
      yield return new WaitUntil(() => body.velocity.GetValueOfAxis(axis2D) == value);
    }

    public static IEnumerator WaitUntilVelocityHasValue(this Rigidbody2D body, Vector2 value)
    {
      yield return new WaitUntil(() => body.velocity == value);
    }

    public static IEnumerator WaitUntilFreeFallEnd(this Rigidbody2D body)
    {
      yield return body.WaitUntilVelocityStartChanging();
      yield return body.WaitUntilVelocityHasValue(Axis2D.Y, 0);
    }

  }
}