﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class GameObjectExtender
  {
   
    public static GameObject CreateIfNotExist(string name)
    {
      GameObject result = GameObject.Find(name);
      if(result == null)
        result = new GameObject(name);

      return result;
    }

    public static GameObject Duplicate(this GameObject gameObject)
    {
      GameObject obj2 = Object.Instantiate(gameObject);
      obj2.transform.position = gameObject.transform.position;
      obj2.transform.parent = gameObject.transform.parent;
      obj2.transform.localScale = gameObject.transform.localScale;
      obj2.transform.rotation = gameObject.transform.rotation;
      return obj2;
    }

    public static GameObject CreateIfNotExist(this GameObject parent, string name)
    {
      Transform result = parent.GetComponentsInChildren<Transform>(true).FirstOrDefault(child => child.name == name);
      if (result == null)
        result = parent.CreateChild(name).transform;
      return result.gameObject;
    }

    public static GameObject[] FindAll(string nameObj)
    {
      return Resources.FindObjectsOfTypeAll<GameObject>().Where(obj => obj.name == nameObj).ToArray();
    }


    /// <summary>
    /// Левый верхний задний    (x- y+ z-), правый верхний задний     (x+ y+ z-), 
    /// левый нижний задний     (x- y- z-), правый нижний задний      (x+ y- z-),
    /// Левый верхний передний  (x- y+ z+), правый верхний передний   (x+ y+ z+), 
    /// левый нижний передний   (x- y- z+), правый нижний передний    (x+ y- z+),
    /// в 2д нужны первые 4
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static Vector3[] GetCornersPositions3D(this GameObject obj)
    {
      Renderer[] allRenderers = obj.GetComponentsInChildren<Renderer>();

      Vector3 fallBorders = GetBeginBorders(allRenderers);
      Vector3 growBorders = GetEndBorders(allRenderers);

      return new[]
      {
        // Левый верхний задний    (x- y+ z-),                        правый верхний задний       (x+ y+ z-), 
        new Vector3(fallBorders.x, growBorders.y, fallBorders.z), new Vector3(growBorders.x, growBorders.y, fallBorders.z),

        // левый нижний задний     (x- y- z-),                        правый нижний задний        (x+ y- z-),
        new Vector3(fallBorders.x, fallBorders.y, fallBorders.z), new Vector3(growBorders.x, fallBorders.y, fallBorders.z),

        // Левый верхний передний  (x- y+ z+),                        правый верхний передний     (x+ y+ z+), 
        new Vector3(fallBorders.x, growBorders.y, growBorders.z), new Vector3(growBorders.x, growBorders.y, growBorders.z),

        // левый нижний передний   (x- y- z+),                        правый нижний передний      (x+ y- z+),
        new Vector3(fallBorders.x, fallBorders.y, growBorders.z), new Vector3(growBorders.x, fallBorders.y, growBorders.z)
      };
    }

    /// <summary>
    /// Левый верхний задний  (x- y+ z-), правый верхний задний  (x+ y+ z-), 
    /// левый нижний задний   (x- y- z-), правый нижний задний   (x+ y- z-),
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static Vector3[] GetCornersPositions2D(this GameObject obj)
    {
      /// в 2д нужны первые 4
      return GetCornersPositions3D(obj).Take(4).ToArray();
    }


    /// <summary>
    /// Значения начальных (левая, нижняя и задняя) границ всех рендереров. 
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    private static Vector3 GetBeginBorders(Renderer[] allRenderers)
    {
      if (allRenderers.Length == 0)
        return Vector3.zero;

      Vector3 sizeMin = GetRendererBeginBorders(allRenderers[0]);
      for (int i = 1; i < allRenderers.Length; i++)
      {
        Vector3 curRenderMinSize = GetRendererBeginBorders(allRenderers[i]);
        if (sizeMin.x > curRenderMinSize.x)
          sizeMin.x = curRenderMinSize.x;
        if (sizeMin.y > curRenderMinSize.y)
          sizeMin.y = curRenderMinSize.y;
        if (sizeMin.z > curRenderMinSize.z)
          sizeMin.z = curRenderMinSize.z;
      }
      return sizeMin;
    }

    /// <summary>
    /// Значения конечных (правая, верхняя и передняя) границ всех рендереров. 
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    private static Vector3 GetEndBorders(Renderer[] allRenderers)
    {
      if (allRenderers.Length == 0)
        return Vector3.zero;

      Vector3 sizeMax = GetRendererEndBorders(allRenderers[0]);
      for (int i = 1; i < allRenderers.Length; i++)
      {
        Vector3 curRenderMaxSize = GetRendererEndBorders(allRenderers[i]);
        if (sizeMax.x < curRenderMaxSize.x)
          sizeMax.x = curRenderMaxSize.x;
        if (sizeMax.y < curRenderMaxSize.y)
          sizeMax.y = curRenderMaxSize.y;
        if (sizeMax.z < curRenderMaxSize.z)
          sizeMax.z = curRenderMaxSize.z;
      }
      return sizeMax;
    }

    private static Vector3 GetRendererBeginBorders(Renderer renderer)
    {
      Vector3 sizeMin = Vector3.zero;
      Vector3 rendererSize = renderer.bounds.size;
      Vector3 rendererCenter = renderer.bounds.center;
      sizeMin.x = rendererCenter.x - (rendererSize.x / 2);
      sizeMin.y = rendererCenter.y - (rendererSize.y / 2);
      sizeMin.z = rendererCenter.z - (rendererSize.z / 2);
      return sizeMin;
    }

    private static Vector3 GetRendererEndBorders(Renderer renderer)
    {
      Vector3 sizeMax = Vector3.zero;
      Vector3 rendererSize = renderer.bounds.size;
      Vector3 rendererCenter = renderer.bounds.center;
      sizeMax.x = rendererCenter.x + (rendererSize.x / 2);
      sizeMax.y = rendererCenter.y + (rendererSize.y / 2);
      sizeMax.z = rendererCenter.z + (rendererSize.z / 2);
      return sizeMax;
    }

    public static Bounds GetBounds(this GameObject obj)
    {
      Renderer[] allRenderers = obj.GetComponentsInChildren<Renderer>();
      Vector3 size = GetEndBorders(allRenderers) - GetBeginBorders(allRenderers);
      return new Bounds(obj.GetGeometricalCenter(), size);
    }

    public static Vector3 GetGeometricalCenter(this GameObject obj)
    {
      Renderer[] allRenderers = obj.GetComponentsInChildren<Renderer>();

      Vector3 highest = GetEndBorders(allRenderers);
      Vector3 lowest = GetBeginBorders(allRenderers);

      float wight = highest.x - lowest.x;
      float height = highest.y - lowest.y;
      float depth = highest.z - lowest.z;

      return new Vector3(highest.x - wight / 2, highest.y - height / 2, highest.z - depth / 2);
    }

    /// <summary>
    /// Initialization of script before Awake within oItin
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <param name="onInit"></param>
    /// <returns></returns>
    public static T AddComponentWithInit<T>(this GameObject obj, Action<T> onInit) where T : Component
    {
      bool oldState = obj.activeSelf;
      obj.SetActive(false);
      T component = obj.AddComponent<T>();
      onInit.InvokeSafe(component);
      obj.SetActive(oldState);
      return component;
    }

    public static T AddComponentIfNotExist<T>(this GameObject obj) where T : Component
    {
      T component = obj.GetComponent<T>();
      if (component == null)
        component = obj.AddComponent<T>();
      return component;
    }

    public static void DestroyIfExist<T>(this GameObject obj) where T : Component
    {
      T component = obj.GetComponent<T>();
      if (component != null)
        GameObject.Destroy(component);
    }

    public static GameObject FindChild(this GameObject obj, string searchedName)
    {
      var result = obj.GetComponentsInChildren<Transform>(true).FirstOrDefault(child => child.gameObject.name == searchedName);
      if (result != null)
        return result.gameObject;
      Debug.LogWarning(String.Format("No such child: {0} In gameObject {1}", searchedName, obj.name), obj);
      return null;
    }

    public static GameObject FindParrent(this GameObject obj, string searchedName)
    {
      var result = obj.GetComponentsInParent<Transform>(true).FirstOrDefault(parrent => parrent.gameObject.name == searchedName);
      if (result != null)
        return result.gameObject;
      Debug.LogWarning(String.Format("No such parrent: {0} Of gameObject {1}", searchedName, obj.name), obj);
      return null;
    }

    public static GameObject[] FindChildsNameContains(this GameObject obj, string searchedPartName)
    {
      var result = obj.GetComponentsInChildren<Transform>(true).Where(child => child.gameObject.name.Contains(searchedPartName)).Select(ob => ob.gameObject);
      return result.ToArray();
    }

    public static GameObject[] FindChildsNameMatch(this GameObject obj, Regex regularExpression)
    {
      var result = obj.GetComponentsInChildren<Transform>(true).Where(child => regularExpression.IsMatch(child.name)).Select(ob => ob.gameObject);
      return result.ToArray();
    }

    public static GameObject[] GetChildrenFirstLevel(this GameObject obj)
    {
      List<GameObject> children = new List<GameObject>();
      foreach (Transform child in obj.transform)
        children.Add(child.gameObject);
      return children.ToArray();
    }

    public static GameObject CreateChild(this GameObject obj, GameObject prefab)
    {
      GameObject child = GameObject.Instantiate(prefab);
      child.transform.SetParent(obj.transform);
      child.transform.localPosition = Vector3.zero;
      return child;
    }

    public static GameObject CreateChild(this GameObject obj, string newGameObjectName)
    {
      GameObject child = new GameObject(newGameObjectName);
      child.transform.parent = obj.transform;
      child.transform.localPosition = Vector3.zero;
      return child;
    }

    public static void SetChild(this GameObject obj, GameObject otherObj)
    {
      otherObj.transform.parent = obj.transform;
    }


    public static IEnumerator HideAndShowOverSeconds(this GameObject obj, float seconds)
    {
      obj.SetActive(false);
      yield return new WaitForSeconds(seconds);
      if (obj != null)
        obj.SetActive(true);
    }

    public static IEnumerator DissapearObjectOverSeconds(this GameObject obj, float seconds, bool setOnInActiveInEnd, bool resetAlphaToStartValueInEnd)
    {
      List<Renderer> renderers = obj.GetComponentsInChildren<Renderer>(true).ToList();

      Dictionary<Renderer, float> startAlpha = new Dictionary<Renderer, float>();
      foreach (Renderer renderer in renderers)
        startAlpha.Add(renderer, renderer.material.color.a);

      float waitTime = 0;
      while (waitTime < seconds)
      {
        foreach (Renderer renderer in renderers)
        {
          Color curColor = renderer.material.color;
          curColor.a = Mathf.Lerp(startAlpha[renderer], 0, waitTime / seconds);
          renderer.material.color = curColor;
        }

        yield return null;
        waitTime += Time.deltaTime;
      }

      obj.SetActive(!setOnInActiveInEnd);

      if (resetAlphaToStartValueInEnd)
      {
        foreach (Renderer renderer in renderers)
        {
          Color curColor = renderer.material.color;
          curColor.a = startAlpha[renderer];
          renderer.material.color = curColor;
        }
      }

    }


    public static IEnumerator TransparentOverSeconds(this GameObject obj, float toTransparentValue, float seconds)
    {
      List<Renderer> renderers = obj.GetComponentsInChildren<Renderer>(true).ToList();

      Dictionary<Renderer, float> startAlpha = new Dictionary<Renderer, float>();
      foreach (Renderer renderer in renderers)
        startAlpha.Add(renderer, renderer.material.color.a);

      float waitTime = 0;
      while (waitTime < seconds)
      {
        foreach (Renderer renderer in renderers)
        {
          Color curColor = renderer.material.color;
          curColor.a = Mathf.Lerp(startAlpha[renderer], toTransparentValue, waitTime / seconds);
          renderer.material.color = curColor;
        }
        yield return null;
        waitTime += Time.deltaTime;
      }

      foreach (Renderer renderer in renderers)
      {
        Color curColor = renderer.material.color;
        curColor.a = toTransparentValue;
        renderer.material.color = curColor;
      }
    }


  }
}