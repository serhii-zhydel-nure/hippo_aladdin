﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Random = UnityEngine.Random;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class CollectionsExtender
  {
    public static T[] Shuffle<T>(this T[] array)
    {
      int length = array.Length;
      T[] result = new T[length];
      array.CopyTo(result, 0);
      while (length > 0)
      {
        int randIndex = Random.Range(0, length);
        T randItem = result[length - 1];
        result[length - 1] = result[randIndex];
        result[randIndex] = randItem;
        length--;
      }
      return result;
    }

    public static List<T> Shuffle<T>(this IList<T> list)
    {
      return new List<T>(list.ToArray().Shuffle());
    }

    public static bool IsItemDuplicated<T>(this IEnumerable<T> collection, T item)
    {
      return collection.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => y.Key).Any(dub => dub.Equals(item));
    }

    public static bool HasDuplicates<T>(this IEnumerable<T> collection)
    {
      return collection.GroupBy(x => x)
        .Count(g => g.Count() > 1) > 0;
    }

    public static T GetRandomItem<T>(this T[] array)
    {
      return array[Random.Range(0, array.Length)];
    }

    public static T GetRandomItem<T>(this IList<T> list)
    {
      return list[Random.Range(0, list.Count)];
    }

    public static T GetRandomItem<T>(this ICollection<T> collection)
    {
      return collection.ElementAt(Random.Range(0, collection.Count));
    }

    public static T GetRandomItem<T>(this IEnumerable<T> collection)
    {
      return collection.ElementAt(Random.Range(0, collection.Count()));
    }

    public static string ToStringAll<T>(this IEnumerable<T> collection)
    {
      StringBuilder res = new StringBuilder();
      T[] array = collection.ToArray();
      for (int i = 0; i < array.Length; i++)
      {
        res.Append("item_").Append(i).Append(" = ").Append(array[i]).AppendLine();
      }
      return res.ToString();
    }

    public static void Shift<T>(this T[] array, int shift)
    {
      if (shift == 0 || array.Length <= 1)
        return;

      shift = shift % array.Length;

      if (shift == 0)
        return;

      if (shift < 0)
        shift = array.Length + shift;

      Array.Reverse(array, 0, array.Length);
      Array.Reverse(array, 0, shift);
      Array.Reverse(array, shift, array.Length - shift);
    }


    ///<summary>Finds the index of the first item matching an expression in an enumerable.</summary>
    ///<param name="items">The enumerable to search.</param>
    ///<param name="predicate">The expression to test the items against.</param>
    ///<returns>The index of the first matching item, or -1 if no items match.</returns>
    public static int IndexOf<T>(this IEnumerable<T> items, Func<T, bool> predicate)
    {
      if (items == null) throw new ArgumentNullException("items");
      if (predicate == null) throw new ArgumentNullException("predicate");

      int retVal = 0;
      foreach (var item in items)
      {
        if (predicate(item)) return retVal;
        retVal++;
      }
      return -1;
    }

    ///<summary>Finds the index of the first occurrence of an item in an enumerable.</summary>
    ///<param name="items">The enumerable to search.</param>
    ///<param name="item">The item to find.</param>
    ///<returns>The index of the first matching item, or -1 if the item was not found.</returns>
    public static int IndexOf<T>(this IEnumerable<T> items, T item)
    {
      return items.IndexOf(i => EqualityComparer<T>.Default.Equals(item, i));
    }

    public static Queue<T> ToQueue<T>(this IEnumerable<T> items)
    {
      return new Queue<T>(items);
    }

    public static Stack<T> ToStack<T>(this IEnumerable<T> items)
    {
      return new Stack<T>(items);
    }
  }
}