﻿using System;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class ComponentExtender
  {
    public static T CopyComponent<T>(this T original, GameObject destination) where T : Component
    {
      Type type = original.GetType();
      var dst = destination.AddComponent(type) as T;
      var fields = type.GetFields();

      foreach (var field in fields)
      {
        if (field.IsStatic) continue;
        field.SetValue(dst, field.GetValue(original));
      }

      var props = type.GetProperties();

      foreach (var prop in props)
      {
        if (!prop.CanWrite || prop.Name == "name")
          continue;

        prop.SetValue(dst, prop.GetValue(original, null), null);
      }

      return dst;
    }
  }
}