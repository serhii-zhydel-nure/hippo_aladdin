﻿using System.Collections;
using System.IO;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class SpriteRendererExtender
  {
    public static void SetMaterialFromResourses(this SpriteRenderer sprite, string materialName)
    {
      Material materital = Resources.Load<Material>(Path.Combine("Materials", materialName));
      if (materital == null)
        Debug.LogWarning("No such material in Resources: " + materialName);
      else
        sprite.material = materital;
    }

    public static IEnumerator TransparencyOverSeconds(this SpriteRenderer obj, float value, float seconds)
    {
      float elapsedTime = 0;
      float startingAlpha = obj.color.a;

      while (elapsedTime < seconds)
      {
        Color curColor = obj.color;
        curColor.a = Mathf.Lerp(startingAlpha, value, elapsedTime / seconds);
        obj.color = curColor;
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      Color color = obj.color;
      color.a = value;
      obj.color = color;
    }
  }
}