﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class ImageExtender
  {
    public static IEnumerator TransparencyOverSeconds(this Image obj, float value, float seconds)
    {
      float elapsedTime = 0;
      float startingAlpha = obj.color.a;

      while (elapsedTime < seconds)
      {
        Color curColor = obj.color;
        curColor.a = Mathf.Lerp(startingAlpha, value, elapsedTime / seconds);
        obj.color = curColor;
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      Color color = obj.color;
      color.a = value;
      obj.color = color;
    }
  }
}