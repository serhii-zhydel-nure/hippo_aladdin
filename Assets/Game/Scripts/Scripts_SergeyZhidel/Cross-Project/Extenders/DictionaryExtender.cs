﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class DictionaryExtender
  {
    public static TValue TryToGetValueUnChecked<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
    {
      if (!dictionary.ContainsKey(key))
        Debug.LogWarning(String.Format("<color=red>No such key in dictionary: {0}</color>", key));

      return dictionary[key];
    }


    public static TValue TryToGetValueOrDefault<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key)
    {
      if (!dictionary.ContainsKey(key))
      {
        Debug.LogWarning(String.Format("<color=red>No such key in dictionary: {0}</color>", key));
        return default(TValue);
      }

      return dictionary[key];
    }
  }
}