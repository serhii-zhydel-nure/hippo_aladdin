﻿using System;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class StringExtender
  {
    /// <summary>
    /// Trim all text from start+first "sequence" from start
    /// </summary>
    /// <param name="str"></param>
    /// <param name="sequence"></param>
    /// <returns></returns>
    public static string TrimStartWithSequence(this string str, string sequence)
    {
      int index = str.IndexOf(sequence);
      if (index == -1)
        throw new ArgumentException("No such sequence in string");

      return str.TrimStart(str.Substring(0, index + sequence.Length).ToCharArray());
    }

    /// <summary>
    /// Trim all text from end+first "sequence" from end
    /// </summary>
    /// <param name="str"></param>
    /// <param name="sequence"></param>
    /// <returns></returns>
    public static string TrimEndWithSequence(this string str, string sequence)
    {
      int index = str.LastIndexOf(sequence);
      if (index == -1)
        throw new ArgumentException("No such sequence in string");
      string s = str.Substring(index, str.Length - index);
      return str.TrimEnd(s.ToCharArray());
    }

    public static string GetValueTillSequenceFromEnd(this string str, string sequence)
    {
      int index = str.LastIndexOf(sequence);
      if (index == -1)
        throw new ArgumentException("No such sequence in string");

      var r = str.Substring(index + sequence.Length, str.Length - index - sequence.Length);
      return r;

      // return str.Substring(index+ sequence.Length, str.Length - index);
    }


    public static T ToEnum<T>(this string valueToParse) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (Enum.IsDefined(typeof(T), valueToParse))
      {
        return (T)Enum.Parse(typeof(T), valueToParse);
      }
      throw new ArgumentException("wrong string to parce: " + valueToParse);
    }

    public static bool TryParceToEnum<T>(this string value, ref T res) where T : struct, IComparable, IConvertible, IFormattable
    {
      if (Enum.IsDefined(typeof(T), value))
      {
        res = (T)Enum.Parse(typeof(T), value);
        return true;
      }

      return false;
    }
  }
}