﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class AnimatorExtender
  {
    public static bool IsPlayingAnimation(this Animator animator, string animName)
    {
      if (animator == null)
        return false;

      return animator.GetCurrentAnimatorStateInfo(0).IsName(animName);
    }

    public static IEnumerator WaitWhileInTransition(this Animator animator, int layer = 0)
    {
      yield return new WaitUntil(() => animator.IsInTransition(layer));
      yield return new WaitWhile(() => animator.IsInTransition(layer));
    }

    public static IEnumerator WaitUntilAnimationPlaying(this Animator animator, string animName)
    {
      yield return new WaitUntil(() => animator.IsPlayingAnimation(animName));
    }

    public static void PlayAnimationFromRandomTime(this Animator animator, string animName, int layer = 0)
    {
      animator.Play(animName, layer, Random.Range(0.0f, 1.0f));
    }

    public static IEnumerator PlayAnimation(this Animator animator, string animName, float speed = 1, int layer = 0)
    {
      animator.Play(animName, layer);
      animator.speed = speed;
      yield return new WaitForEndOfFrame();
      yield return new WaitUntil(() => animator.IsPlayingAnimation(animName));//анимация точно начата
    }

    public static IEnumerator PlayFullAnimation(this Animator animator, string animName, bool abortOnStartTransition = false, float speed = 1, int layer = 0)
    {
      yield return animator.PlayAnimation(animName, speed, layer);
      yield return new WaitForEndOfFrame();
      if (abortOnStartTransition)
        //пока анимация играет и не начался переход
        yield return new WaitWhile(() => animator.IsPlayingAnimation(animName) && !animator.IsInTransition(layer));
      else
        //пока анимация играет (включая)
        yield return new WaitWhile(() => animator.IsPlayingAnimation(animName));
      animator.speed = 1;
    }

    public static IEnumerator PlayAnimationForTime(this Animator animator, string animName, float seconds, float speed = 1, int layer = 0)
    {
      int currenAnimState = animator.GetCurrentAnimatorStateInfo(0).fullPathHash;
      float animTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;

      animator.Play(animName, layer);
      animator.speed = speed;
      float waitTime = 0;
      while (waitTime < seconds)
      {
        animator.Play(animName, layer);
        animator.speed = speed;
        if (animator.IsInTransition(layer))//переход на другую анимацию
          animator.Play(animName, layer);
        yield return null;
        waitTime += Time.deltaTime;
      }

      animator.Play(currenAnimState, 0, animTime);
    }

    public static IEnumerator PlayAnimationForTimeOrUntilCondition(this Animator animator, string animName, float seconds, Func<bool> condition, float speed = 1, int layer = 0)
    {
      int currenAnimState = animator.GetCurrentAnimatorStateInfo(0).fullPathHash;
      float animTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;

      animator.Play(animName, layer);
      animator.speed = speed;
      float waitTime = 0;
      while (waitTime < seconds && !condition.Invoke())
      {
        animator.Play(animName, layer);
        animator.speed = speed;
        if (animator.IsInTransition(layer))//переход на другую анимацию
          animator.Play(animName, layer);
        yield return null;
        waitTime += Time.deltaTime;
      }

      animator.Play(currenAnimState, 0, animTime);
    }

    public static IEnumerator PlayAnimationWhile(this Animator animator, string animName, Func<bool> condition, float speed = 1, int layer = 0)
    {
      int currenAnimState = animator.GetCurrentAnimatorStateInfo(0).fullPathHash;
      float animTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;

      animator.Play(animName, layer);
      animator.speed = speed;

      while (condition.Invoke())
      {
        if (animator.IsInTransition(layer))//переход на другую анимацию
          animator.Play(animName, layer);
        yield return null;
      }

      animator.Play(currenAnimState, 0, animTime);
    }

    public static IEnumerator PlayAnimationUntil(this Animator animator, string animName, Func<bool> condition, float speed = 1, int layer = 0)
    {
      int currenAnimState = animator.GetCurrentAnimatorStateInfo(0).fullPathHash;
      float animTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;

      animator.Play(animName, layer);
      animator.speed = speed;

      while (!condition.Invoke())
      {
        if (animator.IsInTransition(layer))//переход на другую анимацию
          animator.Play(animName, layer);
        yield return null;
      }

      animator.Play(currenAnimState, 0, animTime);
    }
  }
}