﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class CameraExtender
  {
    public static IEnumerator MoveAndScaleOverSeconds(this Camera camera, Vector3 destination, float scale, float seconds)
    {
      Transform cameraTransform = camera.transform;
      destination.z = camera.transform.position.z;
      float elapsedTime = 0;
      Vector3 startingPos = cameraTransform.position;
      float startingScale = camera.orthographicSize;
      while (elapsedTime < seconds)
      {
        cameraTransform.position = Vector3.Lerp(startingPos, destination, elapsedTime / seconds);
        camera.orthographicSize = Mathf.Lerp(startingScale, scale, elapsedTime / seconds);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }
      cameraTransform.position = destination;
      camera.orthographicSize = scale;
    }

    public static IEnumerator MoveAndScaleOverSeconds(this Camera camera, GameObject obj, float scale, float seconds)
    {
      Transform cameraTransform = camera.transform;

      float elapsedTime = 0;
      Vector3 startingPos = cameraTransform.position;
      float startingScale = camera.orthographicSize;
      while (elapsedTime < seconds)
      {
        Vector3 objPos = obj.transform.position;
        objPos.z = startingPos.z;
        cameraTransform.position = Vector3.Lerp(startingPos, objPos, elapsedTime / seconds);
        camera.orthographicSize = Vector2.Lerp(new Vector2(startingScale, 0), new Vector2(scale, 0), elapsedTime / seconds).x;
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }
      Vector3 pos = obj.transform.position;
      pos.z = startingPos.z;
      cameraTransform.position = pos;
      camera.orthographicSize = scale;
    }

    public static IEnumerator MoveOverSeconds(this Camera camera, Vector3 destination, float seconds)
    {
      Transform cameraTransform = camera.transform;
      destination.z = camera.transform.position.z;

      float elapsedTime = 0;
      Vector3 startingPos = cameraTransform.position;

      while (elapsedTime < seconds)
      {
        Vector2 lefp = Vector3.Lerp(startingPos, destination, elapsedTime / seconds); ;
        cameraTransform.position = new Vector3(lefp.x, lefp.y, cameraTransform.position.z);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }
      cameraTransform.position = destination;
    }

    public static IEnumerator MoveOverSeconds(this Camera camera, GameObject obj, float seconds)
    {
      Transform objectTransform = obj.transform;
      Transform cameraTransform = camera.transform;

      float elapsedTime = 0;
      Vector3 startingPos = cameraTransform.position;
      while (elapsedTime < seconds)
      {
        Vector2 lefp = Vector3.Lerp(startingPos, objectTransform.position, elapsedTime / seconds); ;
        cameraTransform.position = new Vector3(lefp.x, lefp.y, cameraTransform.position.z);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      cameraTransform.position = new Vector3(objectTransform.position.x, objectTransform.position.y, cameraTransform.position.z);
    }

    /// <summary>
    /// intend - distance between the camera and followed object
    /// </summary>
    /// <returns></returns>
    public static IEnumerator MoveOverSeconds(this Camera camera, GameObject obj, Vector3 intend, float seconds)
    {
      Transform objectTransform = obj.transform;
      Transform cameraTransform = camera.transform;

      float elapsedTime = 0;
      Vector3 startingPos = cameraTransform.position;
      while (elapsedTime < seconds)
      {
        Vector2 lefp = Vector3.Lerp(startingPos, objectTransform.position + intend, elapsedTime / seconds); ;
        cameraTransform.position = new Vector3(lefp.x, lefp.y, cameraTransform.position.z);
        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }

      cameraTransform.position = new Vector3(objectTransform.position.x, objectTransform.position.y, cameraTransform.position.z) + intend;
    }

    /// <summary>
    /// intend - distance between the camera and followed object
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="intend"></param>
    /// <returns></returns>
    public static SmoothFollow FollowToObject(this Camera camera, GameObject obj, Vector3 intend)
    {
      return camera.gameObject.AddComponentWithInit<SmoothFollow>((script) =>
      {
        //initialization before Awake
        script.FollovedObjTransform = camera.transform;
        script.TargetTransform = obj.transform;
        script.Intend = intend;
      });
    }

    /// <summary>
    /// Warning!! This script disable gameobject for second, so coroutines may off
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static SmoothFollow FollowToObject(this Camera camera, GameObject obj)
    {
      return camera.gameObject.AddComponentWithInit<SmoothFollow>((script) =>
      {
        //initialization before Awake
        script.FollovedObjTransform = camera.transform;
        script.TargetTransform = obj.transform;
        script.Intend = (camera.transform.position - obj.transform.position).ToVector2();
      });
    }

    public static Bounds GetBounds(this Camera camera)
    {
      Vector2 cameraSize = new Vector2(camera.aspect * 2f * camera.orthographicSize, 2f * camera.orthographicSize);
      return new Bounds(camera.transform.position, cameraSize);
    }

    public static IEnumerator ClampedMoveToObjectAndScaleOverSeconds(this Camera camera, GameObject targetObj, SpriteRenderer clampSprite, float seconds, float scaleLimit,
    float leftOffset = 0.1f, float rightOffset = 0.1f, float upOffset = 0.1f, float downOffset = 0.1f)
    {
      Vector3 targetCenter = targetObj.GetGeometricalCenter();
      Bounds clampingBounds = clampSprite.gameObject.GetBounds();
      if (!clampingBounds.Contains(targetCenter))
      {
        Debug.Log(String.Format(@"<color=blue>TargetObj {0} is out of clampBorders: {1}</color>", targetObj.name, clampSprite.name), targetObj);
        yield break;
      }

      float elapsedTime = 0;
      Vector3 startingPos = camera.transform.position;
      float startingScale = camera.orthographicSize;

      Vector2 cameraSize = new Vector2(camera.aspect * 2f * camera.orthographicSize, 2f * camera.orthographicSize);
      Bounds objBounds = targetObj.GetBounds();

      float min = startingScale > scaleLimit ? scaleLimit : startingScale;
      float max = startingScale < scaleLimit ? scaleLimit : startingScale;
      //хотим приближаться (размер камеры больше размера обьекта), а лимит стоит больше текущего orthographicSize
      if ((cameraSize.x * cameraSize.y > objBounds.size.x * objBounds.size.y) && scaleLimit > camera.orthographicSize)
      {
        min = 0.1f;
        Debug.Log(String.Format("<color=blue>Wrong limit value: {0}</color>", scaleLimit));
        Debug.Log(String.Format("<color=blue>Limit setted to: {0}</color>", min));
      }
      else
      //хотим отдалятся (размер камеры меньше размера обьекта), а лимит стоит меньше текущего orthographicSize
      if ((cameraSize.x * cameraSize.y < objBounds.size.x * objBounds.size.y) && scaleLimit < camera.orthographicSize)
      {
        max = float.MaxValue;
        Debug.Log(String.Format("<color=blue>Wrong limit value: {0}</color>", scaleLimit));
        Debug.Log(String.Format("<color=blue>Limit setted to: {0}</color>", max));
      }
      float scaleWhenOnlyTargetIsInView = GetOrthographicSizeWhenObjectIsCompletelyInCameraView(camera, objBounds);
      float calculatedScale = Mathf.Clamp(scaleWhenOnlyTargetIsInView, min, max);


      Vector2 minBorders;
      Vector2 maxBorders;
      float cameraZ = camera.transform.position.z;

      Debug.Log(String.Format("<color=blue>scaleWhenOnlyTargetIsInView: {0}</color>", scaleWhenOnlyTargetIsInView), targetObj);
      while (elapsedTime < seconds)
      {
        cameraSize = new Vector2(camera.aspect * 2f * camera.orthographicSize, 2f * camera.orthographicSize);
        minBorders = new Vector2(clampingBounds.center.x - clampingBounds.size.x / 2 //левая граница спрайта
            + cameraSize.x / 2
            + leftOffset,

            clampingBounds.center.y - clampingBounds.size.y / 2 //нижняя граница спрайта
            + cameraSize.y / 2
            + downOffset);


        maxBorders = new Vector2(clampingBounds.center.x + clampingBounds.size.x / 2 //правая граница спрайта
          - cameraSize.x / 2
          - rightOffset,

          clampingBounds.center.y + clampingBounds.size.y / 2 //верхняя граница спрайта
          - cameraSize.y / 2
          - upOffset);

        Vector3 destination = targetObj.transform.position;

        camera.transform.position = Vector3.Lerp(startingPos, destination, elapsedTime / seconds)
          .ClampV3(minBorders, maxBorders)
          .SetValueOfAxis(cameraZ, Axis3D.Z);

        camera.orthographicSize = Mathf.Lerp(startingScale, calculatedScale, elapsedTime / seconds);


        elapsedTime += Time.deltaTime;
        yield return new WaitForEndOfFrame();
      }
    }

    /// <summary>
    /// Размер resultOrthographicSize, когда обьект смог бы целиком поместиться в камеру
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="objBounds"></param>
    /// <param name="stepChangingSize">На сколько за шаг менять OrthographicSize</param>
    /// <param name="cameraAndObjOnTheSamePositions">
    /// Расчитывать из условия, что камера находиться над обьектом, а не смотрит в другую сторону. 
    /// Иначе размер будет расти пока камера не захватит обьект.</param>
    /// <returns></returns>
    public static float GetOrthographicSizeWhenObjectIsCompletelyInCameraView(this Camera camera, Bounds objBounds, float stepChangingSize = 0.05f, bool cameraAndObjOnTheSamePositions = true)
    {
      float resultOrthographicSize = camera.orthographicSize;
      Bounds cameraBounds = camera.GetBounds();

      //уровнять размеры по z, чтоб камера не учитывала его
      cameraBounds.SetMinMax(new Vector3(cameraBounds.min.x, cameraBounds.min.y, float.MinValue), new Vector3(cameraBounds.max.x, cameraBounds.max.y, float.MaxValue));

      if (cameraAndObjOnTheSamePositions)
        objBounds.center = cameraBounds.center;

      if (cameraBounds.Contains(objBounds))//камера больше обьекта и он в нее помещается с излишком
      {
        while (cameraBounds.Contains(objBounds))
        {
          Vector2 cameraSize = new Vector2(camera.aspect * 2f * resultOrthographicSize, 2f * resultOrthographicSize);
          resultOrthographicSize -= stepChangingSize;//камера уменьшается
          cameraBounds.size = cameraSize;
        }
        return resultOrthographicSize + stepChangingSize;
      }
      while (!cameraBounds.Contains(objBounds))
      {
        Vector2 cameraSize = new Vector2(camera.aspect * 2f * resultOrthographicSize, 2f * resultOrthographicSize);
        resultOrthographicSize += stepChangingSize;//камера увеличивается
        cameraBounds.size = cameraSize;
      }
      return resultOrthographicSize - stepChangingSize;
    }
  }
}