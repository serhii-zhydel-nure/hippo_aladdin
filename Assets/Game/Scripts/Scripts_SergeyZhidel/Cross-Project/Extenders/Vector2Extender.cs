﻿using UnityEngine;

namespace Scripts_SergeyZhidel.Extenders
{
  public static class Vector2Extender
  {
    public static Vector3 SubFromAxis(this Vector2 vector, float number, Axis2D axis)
    {
      switch (axis)
      {
        case Axis2D.X:
          vector -= new Vector2(number, 0);
          break;

        case Axis2D.Y:
          vector -= new Vector2(0, number);
          break;
      }
      return vector;
    }

    public static Vector3 AddToAxis(this Vector2 vector, float number, Axis2D axis)
    {
      switch (axis)
      {
        case Axis2D.X:
          vector += new Vector2(number, 0);
          break;

        case Axis2D.Y:
          vector += new Vector2(0, number);
          break;
      }
      return vector;
    }

    public static Vector3 MulByAxis(this Vector2 vector, float number, Axis2D axis)
    {
      switch (axis)
      {
        case Axis2D.X:
          vector = new Vector2(vector.x * number, vector.y);
          break;

        case Axis2D.Y:
          vector = new Vector2(vector.x, vector.y * number);
          break;
      }
      return vector;
    }

    public static Vector3 DivIntoAxis(this Vector2 vector, float number, Axis2D axis)
    {
      switch (axis)
      {
        case Axis2D.X:
          vector = new Vector3(vector.x / number, vector.y);
          break;

        case Axis2D.Y:
          vector = new Vector3(vector.x, vector.y / number);
          break;
      }
      return vector;
    }


    public static float GetValueOfAxis(this Vector2 vector, Axis2D axis)
    {
      switch (axis)
      {
        case Axis2D.X:
          return vector.x;

        default:
          return vector.y;
      }
    }

    public static Vector2 SetValueOfAxis(this Vector2 vector, float value, Axis2D axis2D)
    {
      switch (axis2D)
      {
        case Axis2D.X:
         return new Vector2(value, vector.y);

        default:
          return new Vector2(vector.x, value);
      }
    }

    public static Vector2 GetRandomVectorBasedOThis(this Vector2 vector, float valueFrom, float valueTo, Axis2D randomisableAxis)
    {
      return vector.SetValueOfAxis(Random.Range(valueFrom, valueTo), randomisableAxis);
    }

    public static Vector2 GetRandomSimilarVector(this Vector2 vector, float lessDiffernce, float moreDiffernce, Axis2D randomisableAxis)
    {
      float randomizableAxisValue = vector.GetValueOfAxis(randomisableAxis);
      return vector.SetValueOfAxis(Random.Range(randomizableAxisValue - lessDiffernce, randomizableAxisValue + moreDiffernce), randomisableAxis);
    }


    public static Vector2 GetRandomVector(Vector2 vectorFrom, Vector2 vectorTo)
    {
      return new Vector2(Random.Range(vectorFrom.x, vectorTo.x),
                         Random.Range(vectorFrom.y, vectorTo.y)
                         );
    }

    public static Vector2 ClampV2(this Vector2 vec, Vector2 minXY, Vector3 maxXY)
    {
      return new Vector2(
        Mathf.Clamp(vec.x, minXY.x, maxXY.x),
        Mathf.Clamp(vec.y, minXY.y, maxXY.y)
        );
    }
  }

  public enum Axis2D
  {
    X, Y
  }
}