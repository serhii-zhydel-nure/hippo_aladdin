﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Scripts_SergeyZhidel.Extenders
{
    public static class AudioSourceExtender
    {
        public static IEnumerator VolumeOverSeconds(this AudioSource source, float value, float seconds)
        {
            float elapsedTime = 0;
            float startingVolume = source.volume;

            while (elapsedTime < seconds)
            {
                source.volume = Mathf.Lerp(startingVolume, value, elapsedTime / seconds);
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            source.volume = value;
        }

        public static bool IsPlayingClip(this AudioSource source, AudioClip clip)
        {
            return source.clip == clip && source.isPlaying;
        }

        public static IEnumerator PlayClipFull(this AudioSource source, AudioClip clip)
        {
            source.clip = null;
            source.Stop();
            source.clip = clip;
            source.time = 0;
            source.loop = false;
            source.Play();
            yield return new WaitWhile(() => source.IsPlayingClip(clip));
            source.clip = null;
            source.Stop();
        }

        public static IEnumerator PlayClipSeveralTimes(this AudioSource source, AudioClip clip, int repeatTimes, float delayBetweenRepeats = 0)
        {
            for (int i = 0; i < repeatTimes; i++)
            {
                yield return source.PlayClipFull(clip);
                yield return new WaitForSeconds(delayBetweenRepeats);
            }
        }

        public static IEnumerator PlayClipLoop(this AudioSource source, AudioClip clip)
        {
            source.clip = null;
            source.Stop();
            source.clip = clip;
            source.loop = true;
            source.time = 0;
            source.Play();

            yield return new WaitWhile(() => source.IsPlayingClip(clip));
            source.clip = null;
            source.Stop();
            source.loop = false;
        }

        public static IEnumerator PlayClipWhile(this AudioSource source, AudioClip clip, Func<bool> condition)
        {
            source.clip = null;
            source.Stop();
            source.clip = clip;
            source.loop = true;
            source.time = 0;
            source.Play();


            Debug.Log("<color=blue>start sound</color>");
            yield return new WaitWhile(condition);

            if (source.IsPlayingClip(clip))
            {
                source.loop = false;
                Debug.Log("<color=blue>stop sound</color>");

                source.clip = null;
                source.Stop();
            }
        }


        public static IEnumerator PlayClipLoopWithRandomDelay(this AudioSource source, AudioClip clip, float from, float to)
        {
            yield return new WaitForSeconds(Random.Range(from, to));

            while (true)
            {
                yield return source.PlayClipFull(clip);
                yield return new WaitForSeconds(Random.Range(from, to));
            }
        }

        public static IEnumerator PlayClipFromTimeTillEnd(this AudioSource source, AudioClip clip, float fromTime)
        {
            if (fromTime > clip.length)
            {
                yield break;
            }

            source.clip = null;
            source.Stop();
            source.clip = clip;
            source.time = fromTime;
            source.loop = false;
            source.Play();
            yield return new WaitWhile(() => source.IsPlayingClip(clip));
            source.clip = null;
            source.Stop();
        }

        public static IEnumerator PlayClipFromTimeTillEndAndNextLoop(this AudioSource source, AudioClip clip, float fromTime)
        {
            if (fromTime > clip.length)
            {
                Debug.LogWarning("Start time of playing clip is more than full clip time.");
                yield break;
            }

            source.clip = null;
            source.Stop();
            source.clip = clip;
            source.time = fromTime;
            source.loop = true;
            source.Play();
            yield return new WaitWhile(() => source.IsPlayingClip(clip));
            source.clip = null;
            source.Stop();
        }


        public static IEnumerator PlayListFull(this AudioSource source, AudioClip[] list)
        {
            if (list.Length == 0)
            {
                yield return PlayEmptyList();
                yield break;
            }

            foreach (AudioClip clip in list)
            {
                yield return source.PlayClipFull(clip);
            }
        }

        public static IEnumerator PlayListLoop(this AudioSource source, AudioClip[] list)
        {
            if (list.Length == 0)
            {
                while (true)
                {
                    yield return PlayEmptyList();
                }
            }

            while (true)
            {
                foreach (AudioClip clip in list)
                {
                    yield return source.PlayClipFull(clip);
                }
            }
        }

        public static IEnumerator PlayListFromClipTimeTillEnd(this AudioSource source, AudioClip[] list, AudioClip clip, float fromTime)
        {
            if (list.Length == 0)
            {
                yield return PlayEmptyList();
                yield break;
            }


            if (!list.Contains(clip))
            {
                Debug.LogWarning("This list doesn't contain such clip.");
                yield break;
            }

            if (fromTime > clip.length)
            {
                Debug.LogWarning("Start time of playing clip is more than full clip time.");
                yield break;
            }

            int clipIndex = list.IndexOf(clip);

            yield return source.PlayClipFromTimeTillEnd(clip, fromTime);

            List<AudioClip> restClipAfterPaused = new List<AudioClip>(10);
            if (clipIndex + 1 < list.Length)
                for (int i = clipIndex + 1; i < list.Length; i++)
                {
                    restClipAfterPaused.Add(list[i]);
                }

            yield return source.PlayListFull(restClipAfterPaused.ToArray());

        }

        public static IEnumerator PlayListFromClipTimeTillEndAndNextLoop(this AudioSource source, AudioClip[] list, AudioClip clip, float fromTime)
        {
            if (list.Length == 0)
            {
                while (true)
                {
                    yield return PlayEmptyList();
                }
            }

            yield return source.PlayListFromClipTimeTillEnd(list, clip, fromTime);

            yield return source.PlayListLoop(list);
        }

        private static IEnumerator PlayEmptyList()
        {
            yield return new WaitForEndOfFrame();
        }
    }
}