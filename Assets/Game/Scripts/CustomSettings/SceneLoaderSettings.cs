﻿using System.Collections.Generic;
using PSV.ADS;

namespace PSV
{
    // Attention! Scenes for RateMe | PurchaseMe | HomeAds | InterstitialADS and others specify now in Offer scripts.

    public enum Scenes  //List here all scene names included in the build   (permanent scenes are InitScene, PSV_Splash (or other), Push)
    {
        EmptySceneMode,

        InitScene,
        PSV_Splash,
        //Melnitsa_Splash,
        Push,
        MainMenu,
        NewAdsDemo,
        RateMeScene,
        Cleaner,
        _1Attic_Wiping,
        _2Attic_Placement,
        _3Attic_Book,
        _4Book,
        _9Treasures_1Cutscene,
        _9Treasures_2FindHidenStuff,
        _10Lampa,
        _11Sahara,

        Scene3,
        Scene4,
        Scene5,
        Scene6_KoverDocor,
        Scene7,
        Scene8,
        _9Treasures_3DragGame,
        FinalCutScene,
        Map,
        HomeADSBox,
        PurchaseMeNew,
        None,
    }

    public static class SceneLoaderSettings
	{
		public static bool
			transition_after_ad = true;

		public const AdPosition
			small_banner_default_pos = AdPosition.Bottom_Centered,
			native_default_pos = AdPosition.Bottom_Centered;

        public readonly static AdSize
            small_banner_default_size = AdSize.Standard_banner_320x50,
            native_default_size = AdSize.Native_banner_minimal_320x80;


		public readonly static Dictionary<Scenes, AdPosition>
			small_banner_position_override = new Dictionary<Scenes, AdPosition> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
			{
                //{ Scenes.MainMenu, AdPosition.Bottom_Right},
                			{ Scenes._11Sahara,  AdPosition.Bottom_Left},

            },
			native_position_override = new Dictionary<Scenes, AdPosition> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
			{
				//{ Scenes.Sample, AdPosition.Bottom_Centered},
			};


		public readonly static Dictionary<Scenes, AdSize>
			small_banner_size_override = new Dictionary<Scenes, AdSize> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
			{
                //{ Scenes.MainMenu, AdSize.Smart_banner},
				//{ Scenes.Sample, AdSize.Standard_banner_320x50},
            },
			native_size_override = new Dictionary<Scenes, AdSize> ( ) //change here ad position for certain scenes, scenes not listed here will use default_position
			{
                //{ Scenes.MainMenu, AdSize.Smart_banner},
				//{ Scenes.Sample, AdSize.Standard_banner_320x50},
            };


        public readonly static List<Scenes>
            not_allowed_small_banner = new List<Scenes>() //list here scenes that shouldn't show ad 
            {
				//Scenes.PSV_Splash,
                Scenes.MainMenu,
                //Scenes.PurchaseMeNew,
                Scenes.RateMeScene,
                //Scenes.HomeADSBox,
                //Scenes.NotifyTest, // Test Only
                //Scenes.PopularTestLight, // Test Only
                			Scenes.PSV_Splash,
            Scenes.Push,
            Scenes.MainMenu,
            Scenes._4Book,
            Scenes.Map,
            Scenes._9Treasures_2FindHidenStuff,
            Scenes._11Sahara,
                Scenes._1Attic_Wiping,
            };

		public readonly static List<Scenes>
			native_allowed = new List<Scenes> ( )
			{
				//Scenes.Sample,
			};
    }
}
