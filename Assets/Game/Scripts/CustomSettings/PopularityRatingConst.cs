﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV.PopularNotify
{
    public partial class PopularityRating : IDisposable
    {
        //* Notification settings
        /// <summary>
        /// Style popularity notification. 
        /// 0 - DEFAULT_STYLE - default android notification style.
        /// 1 - WITH_DEFAULT_BG_STYLE - default PSV background style.
        /// 2 - WITH_HIPPO_BG_STYLE - hippo style with background.
        /// For <see cref="SetNotificationStyle(int, string)"/>
        /// </summary>
        public const int default_notify_style = 1;
        /// <summary>
        /// Background popularity notification. 
        /// Not supported for DEFAULT_ANDROID_STYLE!
        /// Empty - set native image by selected style. 
        /// Resource name, from Android/res/drawable folder, like "psv_background_1" in Android resource name format.
        /// For <see cref="SetNotificationStyle(int, string)"/>
        /// </summary>
        public const string default_notify_background_res = "";
        //* ^

        //* Storage data params
        public const string delayDaysCheckDataPref = "PriorityLogicDelayDays";
        public const string dataNextCheckDatePref = "PriorityLogicCheckDate";
        public const string dataFileResourcePath = "PSVPriorityGames"; // In Resources folder, with out ".json"
        
        //* ^
    }
}