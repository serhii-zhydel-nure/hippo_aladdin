﻿using UnityEngine;
using System.Collections.Generic;

namespace PSV
{
    public static partial class OffersManager
    {
        // You can add custom offer to this list
        private static List<ISceneOffer> offers = new List<ISceneOffer>()
        {
            new InterstitialAdsOffer(),
            new PurchaseOffer(), // Remove from this if not exist module
            new RateMeOffer(), // Remove from this if not exist module
            new SleeplessSceneOffer(),
        };
    }
}