﻿
namespace PSV
{
    /// <summary>
    /// used to log events from one place
    /// </summary>
    public enum AnalyticsEvents
    {
        StartApplication,
        CloseApplication,
        LogScreen,
        BannerClicked,
        InterstitialClicked,
        OpenPub,
        OpenPromo,
        Custom,
        NeatPlugError,
        HomeAdsError,
        LevelComplete, //LogEvent(AnalyticsEvents.LevelComplete, "param1:value;param2:value")
        DownloadFromServer,
        Notification,
        VersionApplication,
    }
}