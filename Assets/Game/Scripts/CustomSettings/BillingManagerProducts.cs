﻿namespace PSV.IAP
{
    /// <summary>
    /// List here all products, available for purchase in your game.
    /// Android needs ProductType additionally.
    /// </summary>
    public enum Products
    {
        Empty, //Leave this (service item)
        SKU_ADMOB,
        //SKU_CONSUMABLE,
        //SKU_PREMIUM_SUBSCRIPTION,
    }
}
