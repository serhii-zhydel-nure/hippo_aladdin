﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public class InterstitialAdsOffer : InterstitialAdsBaseOffer
    {
        public static Scenes[]
            not_allowed_interstitial =  //list here scenes which wouldn't show big banner if we will leave them
            {
                Scenes.None,
                Scenes.MainMenu,
                Scenes.RateMeScene,
                Scenes.PurchaseMeNew,
                Scenes.HomeADSBox,
            };
        
        public override Scenes[] ScenesList()
        {
            return not_allowed_interstitial;
        }

        protected override void OnShowDialog( Scenes current_scene, Scenes target_scene )
        {

        }
    }
}