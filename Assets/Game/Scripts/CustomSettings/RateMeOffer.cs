﻿using UnityEngine;
using System.Collections.Generic;
using RateMePlugin;

/// <summary>
/// Will try to show RateMe offer after scenes listed below.
/// Dialog will not always be shown. There are limitations on count of times it was closed, rated good and rated bad.
/// </summary>


namespace PSV
{
	public class RateMeOffer : RataMeBaseOffer
    {
		//private int rate_us_counter = 0;            //for counting unloading of the scenes in rate_me_after_end list
		private const int rate_us_interval = 3;     //after which scene will be able to show dialog

        public const Scenes rateme_scene = Scenes.RateMeScene;
        public const float time_interval = 60.0f;
        
        public readonly Scenes[]
			rate_me_after_end = new Scenes[] //list here scenes which will call rate_me dialog on end
			{

            };
        
        protected override bool ExtraConditions( Scenes current_scene, Scenes target_scene )
		{
			//can place here extra conditions 
			//if you want to show dialog after certain number of scenes passed - this counter can be increased here, 
			//but due to time limit in RateMe itself you should compare the counter by equals or more than value (counter >= val)

			//will work only if CanShowRateUs() returned true - only scenes in rate_me_after_end will cause calling this method
			//but still there is no guarantee that dialog will be shown each time (there is time limit and count of positive/negative rates and rejects)

			//return ++rate_us_counter >= rate_us_interval; //will work after first three scenes

			return true;
		}

        protected override void OnShowDialog( Scenes current_scene, Scenes target_scene )
        {
            //you can manage here dialog shown condition

            //rate_us_counter = 0;	//will reset counter to zero for keeping rate_us_interval actual
        }
        
        public override Scenes[] ScenesList()
        {
            return rate_me_after_end;
        }

        public override Scenes OfferScene()
        {
            return rateme_scene;
        }
    }
}
