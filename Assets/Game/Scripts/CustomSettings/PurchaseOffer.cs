﻿using UnityEngine;
using System.Collections.Generic;


/// <summary>
/// List here scenes after which purchase dialog will be shown.
/// Result will not always be true, there limitation on frequency of displaying it.
/// </summary>

namespace PSV
{
    using PurchaseDialogue;

    public class PurchaseOffer : PurchaseBaseOffer
    {
        public const Scenes purchase_scene = Scenes.PurchaseMeNew;
        public const float primary_interval = 60f;
        public const float secondary_interval = 120f;

        private readonly Scenes[]
            purchase_dialogue_after = new Scenes[]
            {
				//Scenes.NewAdsDemo,
			};
        
        protected override bool ExtraConditions( Scenes current_scene, Scenes target_scene )
        {
            // Example
            //can place here extra conditions 
            //if you want to show dialog after certain number of scenes passed - this counter can be increased here, 
            //but due to time limit in RateMe itself you should compare the counter by equals or more than value (counter >= val)

            //will work only if CanShowRateUs() returned true - only scenes in rate_me_after_end will cause calling this method
            //but still there is no guarantee that dialog will be shown each time (there is time limit and count of positive/negative rates and rejects)

            //return ++rate_us_counter >= rate_us_interval; //will work after first three scenes

            return true;
        }

        protected override void OnShowDialog( Scenes current_scene, Scenes target_scene )
        {
            // Example
            //you can manage here dialog shown condition
            //rate_us_counter = 0;	//will reset counter to zero for keeping rate_us_interval actual
        }

        public override Scenes OfferScene()
        {
            return purchase_scene;
        }

        public override Scenes[] ScenesList()
        {
            return purchase_dialogue_after;
        }
    }
}
