﻿using UnityEngine;
using System.Collections;
using System;

namespace PSV
{
    public static partial class ConstSettings
    {
        public const string click_btn_sound = "Button - Click";

#if UNITY_IOS
        // TODO: Set for iOS
        public const string ios_application_name = "";
        public const string ios_application_id = "";
#endif
        
    }
}