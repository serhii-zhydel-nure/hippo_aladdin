﻿using UnityEngine;
using System.Collections;

namespace PSV
{
    public class SleeplessSceneOffer : SleeplessSceneBaseOffer
    {
        public readonly Scenes[] slepless_scenes =
        {

        };
        
        public override Scenes[] ScenesList()
        {
            return slepless_scenes;
        }
    }
}