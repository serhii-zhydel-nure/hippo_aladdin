﻿using UnityEngine;
using System.Collections.Generic;

namespace PSV
{
    /// <summary>
    /// List here all necessary stream groups to manage them separately. By default should have MUSIC and FX. The rest items can be optionally added.
    /// </summary>
    public enum StreamGroup
    {
        FX,
        VOICE,
        MUSIC,
        //AMBIENT,
    }
}

namespace PSV.Audio
{
    /// <summary>
    /// Contains general settings for StreamGroups. These values are set manually.
    /// </summary>
    public static class Common
    {
        /// <summary>
        /// List of audio stream groups that use GameSettings values for music
        /// </summary>
        public static bool IsMusicLikeGroup( StreamGroup group )
        {
            switch (group)
            {
                case StreamGroup.MUSIC:
                return true;
                default:
                return false;
            }
        }

        /// <summary>
        /// Tells if group is not enabled in GameSettings.
        /// </summary>
        /// <param name="group">StreamGroup to select</param>
        /// <returns>Mute value for selected group</returns>
        [System.Obsolete( "Better use AudioController.GetGroup(group).isMuted() instead" )]
        public static bool IsGroupSettingsMuted( StreamGroup group )
        {
            if (IsMusicLikeGroup( group ))
            {
                return !GameSettings.IsMusicEnabled();
            }
            else
            {
                return !GameSettings.IsSoundsEnabled();
            }
        }

        /// <summary>
        /// Get default stream group volume. (Is set manually)
        /// </summary>
        /// <param name="group">StreamGroup to select</param>
        public static float GetGroupVolume( StreamGroup group )
        {
            switch (group)
            {
                case StreamGroup.FX:
                    return 0.8f;
                case StreamGroup.VOICE:
                    return 1.0f;
                case StreamGroup.MUSIC:
                    return 0.8f;
                default:
                    return 1.0f;
            }
        }

        /// <summary>
        /// Get default solo mode value for group. (Is set manually)
        /// Will keep only one stream playing for the group.
        /// </summary>
        /// /// <param name="group">StreamGroup to select</param>
        public static bool IsSoloGroup( StreamGroup group )
        {
            switch (group)
            {
                case StreamGroup.MUSIC:
                    return true;
                default:
                    return false;
            }
        }
    }

}
