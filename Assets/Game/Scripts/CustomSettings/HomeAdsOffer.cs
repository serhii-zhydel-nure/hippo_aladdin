﻿using UnityEngine;
using System.Collections;

namespace PSV.HomeAds
{
    public partial class HomeAdsOffer : HomeAdsBaseOffer
    {
        public const Scenes home_ads_scene = Scenes.HomeADSBox;
        public const float time_interval = float.MaxValue; // show once per session

        public Scenes[] home_ads_after_end = // list here scenes
        {

        };
        
        protected override bool ExtraConditions( Scenes current_scene, Scenes target_scene )
        {
            //you can place here extra conditions 
            //if you want to show dialog after certain number of scenes passed - this counter can be increased here
            bool extraConditions = true;

            return extraConditions && base.ExtraConditions( current_scene, target_scene );
        }
        
        public HomeAdsOffer() : base( false ) { }
        public HomeAdsOffer( bool is_force_show ) : base( is_force_show ) { }

        public override Scenes OfferScene()
        {
            return home_ads_scene;
        }

        public override Scenes[] ScenesList()
        {
            return home_ads_after_end;
        }
    }
}