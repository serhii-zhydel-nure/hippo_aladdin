﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.nScene7
{
     public class Scene7_ObstacleBase : MonoBehaviour
    {
        public delegate void ObstacleHit(Collider2D collision);
        public static event ObstacleHit OnObstacleHit ;
        public GameObject srcEffect;
        private void Awake()
        {
            AwakeOther();
        }
        public virtual void AwakeOther()
        {

        }
        public virtual void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<Scene7_Carpet>())
            {
                if (OnObstacleHit != null)
                    OnObstacleHit(collision);
                 OnTriggerEnterOther(collision);
            }
        }
        public virtual void OnTriggerEnterOther(Collider2D collision)
        {

        }
    }
}
