﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScrollBase : MonoBehaviour
{
    public enum ScrollType
    {
        byX,
        byY

    }

    public static ScrollBase instance;
    ////public float fResistanceFactor = 0.98f;
    ////public float fStopThreashold = 0.01f;
    public ScrollType currentScrollType;

    Plane planeHit;

    Vector3 GetHitPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float fDist;

        if (planeHit.Raycast(ray, out fDist))
            return ray.GetPoint(fDist);
        else
            return Vector3.zero;
    }

    public bool overrideDistance;
    public bool isDragByColliders;

    public bool bTranslating;
    public bool bIsDragNow;
    public bool bIsSelectDone;
    public ScrollItem currentChoice;

    public Vector3 v3Delta;

    public float dragX;

    public List<ScrollItem> list_items = new List<ScrollItem>();

    public float distanceBetween = 1;
    public float fResistanceFactor = 0.9f;
    public float fStopThreashold = 0.01f;

    Transform BorderPosRightUp, BorderPosLeftDown;

    Vector3 v3StartPos;
    Vector3 v3LastPos;
    float fStartTime;

    bool scrollAllowed;
    [Header("Нужно ли вешать маску всем дочерним")]
    public bool isNeedMask;
    public Material matMaskTop;

    void Awake()
    {
        instance = this;
        foreach (Transform item in GetComponentsInChildren<Transform>())
        {
            if (item.name == "BorderPosLeftDown")
            {
                BorderPosLeftDown = item;
            }
            else if (item.name == "BorderPosRightUp")
            {
                BorderPosRightUp = item;
            }
        }
    }

    void Start()
    {
        InitItems();
    }

    public void InitItems()
    {
        planeHit = new Plane(Vector3.forward, transform.position);

        foreach (var item in GetComponentsInChildren<ScrollItem>())
        {
            list_items.Add(item.GetComponent<ScrollItem>());
            //item.scrollBase = this as ScrollBase;
            item.SetScrollBase(this);
        }


        SetDistancePoses();
        lastRightUpMove = list_items[0];
        lastLeftDownMove = list_items[list_items.Count - 1];

        scrollAllowed = IsMoveNeed();
        if (isNeedMask)
        {
            foreach (var item in list_items)
            {
                foreach (var item2 in item.GetComponentsInChildren<SpriteRenderer>())
                {
                    item2.material = matMaskTop;
                }
            }
        }
    }

    public void ShowHideItems(bool _show)
    {
        foreach (var item in list_items)
        {
            item.gameObject.SetActive(_show);
        }
    }

    void SetDistancePoses()
    {
        for (int i = 0; i < list_items.Count; i++)
        {
            switch (currentScrollType)
            {
                case ScrollType.byX:
                    list_items[i].transform.position = new Vector3(
                        list_items[0].transform.position.x + i * distanceBetween,
                        list_items[i].transform.position.y,
                        list_items[i].transform.position.z);
                    break;
                case ScrollType.byY:
                    if (overrideDistance)
                    {
                        list_items[i].transform.position = new Vector3(
                            list_items[0].transform.position.x,
                            list_items[0].transform.position.y - i * list_items[i].distance,
                            list_items[i].transform.position.z);
                    }
                    else
                    {
                        list_items[i].transform.position = new Vector3(
                            list_items[0].transform.position.x,
                            list_items[0].transform.position.y - i * distanceBetween,
                            list_items[i].transform.position.z);
                    }

                    break;
                default:
                    break;
            }
        }
    }

    bool IsMoveNeed()
    {
        if (FindLastLeftDownElement().transform.position.y < BorderPosLeftDown.position.y)
        {
            return true;
        }
        return false;
    }

    ScrollItem lastLeftDownMove, lastRightUpMove;
    bool isLastRightUpScroll;

    ScrollItem FindLastLeftDownElement()
    {
        ScrollItem temp = list_items[0];
        switch (currentScrollType)
        {
            case ScrollType.byX:
                foreach (var item in list_items)
                {
                    if (item.transform.position.x < temp.transform.position.x)
                    {
                        temp = item;
                    }
                }
                break;
            case ScrollType.byY:
                foreach (var item in list_items)
                {
                    if (item.transform.position.y < temp.transform.position.y)
                    {
                        temp = item;
                    }
                }
                break;
            default:
                break;
        }

        return temp;
    }

    ScrollItem FindLastRighUptElement()
    {
        ScrollItem temp = list_items[0];
        switch (currentScrollType)
        {
            case ScrollType.byX:
                foreach (var item in list_items)
                {
                    if (item.transform.position.x > temp.transform.position.x)
                    {
                        temp = item;
                    }
                }
                break;
            case ScrollType.byY:
                foreach (var item in list_items)
                {
                    if (item.transform.position.y > temp.transform.position.y)
                    {
                        temp = item;
                    }
                }
                break;
            default:
                break;
        }

        return temp;
    }

    float dragTimer;


    float timerNoAction;

    void Update()
    {
        if (!bIsSelectDone && scrollAllowed)
        {
            lastLeftDownMove = FindLastLeftDownElement();
            lastRightUpMove = FindLastRighUptElement();

            if (dragX > 0)// right
                isLastRightUpScroll = true;
            else if (dragX < 0)
                isLastRightUpScroll = false;


            if (!isLastRightUpScroll) // влево вниз
            {
                switch (currentScrollType)
                {
                    case ScrollType.byX:
                        if (lastRightUpMove.transform.position.x < BorderPosRightUp.transform.position.x)
                        {
                            lastLeftDownMove.transform.position = new Vector2(lastRightUpMove.transform.position.x + distanceBetween, lastLeftDownMove.transform.position.y);
                            //lastRightMove = lastLeftMove;
                        }
                        break;
                    case ScrollType.byY:
                        if (lastLeftDownMove.transform.position.y < BorderPosLeftDown.transform.position.y)
                        {
                            if (overrideDistance)
                            {
                                lastLeftDownMove.transform.position = new Vector2(lastRightUpMove.transform.position.x, lastRightUpMove.transform.position.y + lastLeftDownMove.distance);
                            }
                            else
                            {
                                lastLeftDownMove.transform.position = new Vector2(lastRightUpMove.transform.position.x, lastRightUpMove.transform.position.y + distanceBetween);
                            }

                            //lastRightMove = lastLeftMove;
                        }
                        break;
                    default:
                        break;
                }

            }
            else
            {
                switch (currentScrollType)
                {
                    case ScrollType.byX:
                        if (lastLeftDownMove.transform.position.x > BorderPosLeftDown.transform.position.x)
                        {
                            lastRightUpMove.transform.position = new Vector2(lastLeftDownMove.transform.position.x - distanceBetween, lastRightUpMove.transform.position.y);
                            //lastRightMove = lastLeftMove;
                        }
                        break;
                    case ScrollType.byY:
                        if (lastRightUpMove.transform.position.y > BorderPosRightUp.transform.position.y)
                        {
                            if (overrideDistance)
                            {
                                lastRightUpMove.transform.position = new Vector2(lastLeftDownMove.transform.position.x, lastLeftDownMove.transform.position.y - lastRightUpMove.distance);
                            }
                            else
                            {
                                lastRightUpMove.transform.position = new Vector2(lastLeftDownMove.transform.position.x, lastLeftDownMove.transform.position.y - distanceBetween);
                            }

                            //lastRightMove = lastLeftMove;
                        }
                        break;
                    default:
                        break;
                }

            }
            foreach (var item in list_items)
            {
                if (!isLastRightUpScroll)
                {
                    if (item.transform.position.x > BorderPosRightUp.position.x)
                    {

                    }
                    else
                    {

                    }
                }
                else
                {
                    if (item.transform.position.x < BorderPosLeftDown.position.x)
                    {
                    }
                    else
                    {
                    }
                }
            }

            if (bIsDragNow)
            {
                Debug.Log("dragX "+ dragX);
                switch (currentScrollType)
                {
                    case ScrollType.byX:
                        for (int i = 0; i < list_items.Count; i++)
                        {
                            list_items[i].transform.position = new Vector3(
                                list_items[i].transform.position.x + dragX,
                                list_items[i].transform.position.y,
                                list_items[i].transform.position.z);
                        }
                        break;
                    case ScrollType.byY:
                        for (int i = 0; i < list_items.Count; i++)
                        {
                            list_items[i].transform.position = new Vector3(
                                list_items[i].transform.position.x,
                                list_items[i].transform.position.y + dragX*0.1f,
                                list_items[i].transform.position.z);
                        }
                        break;
                    default:
                        break;
                }

                if (dragX == 0)
                {
                    dragTimer += Time.deltaTime;
                    if (dragTimer > 1)
                    {
                        dragTimer = 0;
                        bIsDragNow = false;
                        bIsSelectDone = true;

                    }
                }
                else
                    dragTimer = 0;
            }
            else if (bTranslating)
            {
                switch (currentScrollType)
                {
                    case ScrollType.byX:
                        for (int i = 0; i < list_items.Count; i++)
                        {
                            list_items[i].transform.position = new Vector3(
                                list_items[i].transform.position.x + v3Delta.x,
                                list_items[i].transform.position.y,
                                list_items[i].transform.position.z
                            );
                        }
                        break;
                    case ScrollType.byY:
                        for (int i = 0; i < list_items.Count; i++)
                        {
                            list_items[i].transform.position = new Vector3(
                                list_items[i].transform.position.x,
                                list_items[i].transform.position.y + v3Delta.x,
                                list_items[i].transform.position.z
                            );
                        }
                        break;
                    default:
                        break;
                }

                v3Delta = v3Delta * fResistanceFactor;
                if (v3Delta.magnitude < fStopThreashold)
                    bTranslating = false;
            }
        }//bIsSelectDone


        //#if !UNITY_EDITOR && UNITY_ANDROID

        ///input
        ///
        #region isDragByColliders
        if (!isDragByColliders)
        {


            if (Input.touchCount > 0)
            {
                timerNoAction = 0;

                //// Get movement of the finger since last frame
                //Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPsition;

                //// Move object across XY plane
                //transform.Translate(-touchDeltaPosition.x * Time.deltaTime, 0, 0); 
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    bTranslating = false;

                    v3StartPos = GetHitPoint();
                    v3LastPos = v3StartPos;
                    fStartTime = Time.time;

                }
                else if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    //Debug.Log("TouchPhase.Moved");
                    bIsDragNow = true;
                    Vector3 v3T = GetHitPoint();
                    //transform.Translate(new Vector3((v3T - v3LastPos).x, transform.position.y, transform.position.z));
                    switch (currentScrollType)
                    {
                        case ScrollType.byX:
                            dragX = (v3T - v3LastPos).x;
                            break;
                        case ScrollType.byY:
                            dragX = (v3T - v3LastPos).y;
                            //transform.Translate(new Vector3( transform.position.x, dragX, transform.position.z));
                            break;
                        default:
                            break;
                    }

                    v3LastPos = v3T;
                }

                else if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    bIsDragNow = false;
                    bTranslating = true;

                    v3Delta = GetHitPoint();
                    v3Delta = (v3Delta - v3StartPos) / (Time.time - fStartTime) * Time.deltaTime;
                    //bTranslating = true;

                    bIsSelectDone = false;
                    timerNoAction = 0;
                }

            }
            else
            {
                timerNoAction += Time.deltaTime;
            }
        }
        #endregion
        //if (timerNoAction >0.1f && Scene7.instance.busy)
        //{  

        //    dragX = 0.02f;

        //    for (int i = 0; i < list_items.Count; i++)
        //    {
        //        list_items[i].transform.position = new Vector3(
        //           list_items[i].transform.position.x + dragX,
        //           list_items[i].transform.position.y,
        //           list_items[i].transform.position.z);
        //    }
        //}
        //#endif
    }

}
