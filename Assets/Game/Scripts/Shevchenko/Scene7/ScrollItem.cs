﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;

public class ScrollItem : MonoBehaviour
{
    public delegate void DoDrag();
    public static event DoDrag OnDoDrag_Start;
    public static event DoDrag OnDoDrag_End;

	public float distance = 0;
    public bool isNeedScroll = true;
    public ScrollBase scrollBase;

    public void SetScrollBase(ScrollBase _sb)
    {
        scrollBase = _sb;
    }
//#if UNITY_EDITOR 

    public float fResistanceFactor = 0.98f;
    public float fStopThreashold = 0.01f;

    Plane planeHit;
    Vector3 v3StartPos;
    Vector3 v3LastPos;
    Vector3 v3Delta;
    float fStartTime;


    public bool isAllowedMove = false;

    bool bTranslating;

    [Header("Для скролла по кнопке")]
    public float targetPosY;
    void OnMouseDown()
    {
        if (scrollBase.isDragByColliders)
        {
            Debug.Log("OnMouseDown");
            scrollBase.bTranslating = false;

            v3StartPos = GetHitPoint();
            v3LastPos = v3StartPos;
            fStartTime = Time.time;
            scrollBase.currentChoice = this;
          if(OnDoDrag_Start!=null)  OnDoDrag_Start();
        }

    }

    void OnMouseDrag()
    {
        if (scrollBase.isDragByColliders)
        {

            scrollBase.bIsDragNow = true;
            Vector3 v3T = GetHitPoint();
            //////transform.Translate(new Vector3((v3T - v3LastPos).x, transform.position.y, transform.position.z));

            switch (scrollBase.currentScrollType)
            {
                case ScrollBase.ScrollType.byX:
                    scrollBase.dragX = (v3T - v3LastPos).x;
                    break;
                case ScrollBase.ScrollType.byY:
                    scrollBase.dragX = (v3T - v3LastPos).y;
                    break;
                default:
                    break;
            }
            v3LastPos = v3T;
        }
    }
    void OnMouseUp()
    {
        if (scrollBase.isDragByColliders)
        {

            scrollBase.bIsDragNow = false;
            scrollBase.bTranslating = true;

            v3Delta = GetHitPoint();
            v3Delta = (v3Delta - v3StartPos) / (Time.time - fStartTime) * Time.deltaTime;
            //bTranslating = true;
            scrollBase.v3Delta = v3Delta;
            scrollBase.bIsSelectDone = false;
           if(OnDoDrag_End!=null) OnDoDrag_End();
        }
    }

    Vector3 GetHitPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float fDist;

        if (planeHit.Raycast(ray, out fDist))
            return ray.GetPoint(fDist);
        else
            return Vector3.zero;
    }

    void Start()
    {
        planeHit = new Plane(Vector3.forward, transform.position);
    }

    void Update()
    {
        if (scrollBase.isDragByColliders)
        {

            if (bTranslating)
            {
                //transform.position += v3Delta;
                transform.position = new Vector3(transform.position.x + v3Delta.x, 0, transform.position.z);
                v3Delta = v3Delta * fResistanceFactor;
                if (v3Delta.magnitude < fStopThreashold)
                    bTranslating = false;
            }
        }
    }
//#endif

}