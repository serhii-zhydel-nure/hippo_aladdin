﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.nScene7
{
    public class Scene7_ObstacleBird : Scene7_ObstacleBase
    {
        public Animator anim;
        public bool isIdleFly = true;
        public float speed;

        public override void AwakeOther()
        {
            anim = GetComponentInChildren<Animator>();
            anim.SetFloat("offset", Random.Range(0f, 1f));
            speed = Random.Range(3f, 12f);
            //srcEffect = Resources.Load<GameObject>("Effects/Scene7_ParticlePero");
        }
        private void Update()
        {
            if (isIdleFly)
            {
                transform.Translate(Vector2.left * Time.deltaTime * speed);
            }
        }
        public override void OnTriggerEnterOther(Collider2D collision)
        {
            if (collision.GetComponent<Scene7_Carpet>())
            {
                GameObject go = Instantiate(srcEffect, transform.position, Quaternion.identity, transform.parent) as GameObject;
                Destroy(go, 5);
                StartCoroutine(FlyAway());
                SFX.StopSingle("Птицы");
                SFX.Play("Птицы", .3f);
            }
        }
        GameObject RandomAwayPos;
        IEnumerator FlyAway()
        {
            isIdleFly = false;
            anim.SetTrigger("hit");

            int randAbs = Random.Range(0, 2) == 0 ? 1 : -1;
            int randAbs2 = Random.Range(0, 2) == 0 ? 1 : -1;

            //Debug.Log(randAbs+" "+ randAbs2);
            GameObject go = new GameObject("RandomAwayPos");
            go.transform.position = new Vector2(
                transform.position.x + (Random.Range(20, 50) * randAbs),
                transform.position.y + (Random.Range(20, 50) * randAbs2));
            go.transform.parent = transform.parent;
            RandomAwayPos = go;
            AutoSetDirection(go.transform.position);
            while (Vector2.Distance(transform.position, go.transform.position) != 0)
            {
                transform.position = Vector2.MoveTowards(transform.position, go.transform.position, Time.deltaTime * 15);
                yield return null;
            }
            isIdleFly = true;

        }
        private void OnDisable()
        {
            Destroy(RandomAwayPos);
        }
        void AutoSetDirection(Vector2 _targetPosition)
        {
            if (transform.position.x < _targetPosition.x)
            {
                // right
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }

    }
}
