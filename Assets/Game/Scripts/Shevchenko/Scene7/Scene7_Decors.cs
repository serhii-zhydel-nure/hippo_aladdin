﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.nScene7
{

    public class Scene7_Decors : MonoBehaviour
    {
        public float speed=1;
        public bool isMoveAllowed = true;

         public void AllowDisallowMove(bool _allow)
        {
            isMoveAllowed = _allow;
        }
         void Update()
        {
            if (Scene7.instance.isSmallGame && isMoveAllowed)
            {
                transform.Translate(new Vector3(-.1f* speed, 0, 0));

            }

        }
    }
}
