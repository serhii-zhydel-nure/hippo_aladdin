﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Shevchenko
{
    public class Scene7_BirdDropper : MonoBehaviour
    {
        public static Scene7_BirdDropper instance;
        public bool isEnabled;

        Transform BirdInstpos_Top, BirdInstpos_Bottom;
        List<GameObject> list_CurDroppedBirds = new List<GameObject>();
        List<GameObject> list_src = new List<GameObject>();
        public float lifeTime;

        public bool isUseSrcFromCurResources;

        GameObject goHolder;
        public int min = 5;
        public int max = 10;
        private void Awake()
        {
            instance = this;
            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "BirdInstpos_Top")
                {
                    BirdInstpos_Top = item;
                }
                else if (item.name == "BirdInstpos_Bottom")
                {
                    BirdInstpos_Bottom = item;
                }
            }
            if (isUseSrcFromCurResources)
            {
                list_src = new List<GameObject>(Resources.LoadAll<GameObject>("Scene7/Obstacles"));

            }
            goHolder = new GameObject("BirdHolder");

            StartCoroutine(IeUpdate());
        }
        public void SetEnableDisable(bool _enable)
        {
            isEnabled = _enable;
        }
        public void SetSrc(List<GameObject> _src)
        {
            list_src = new List<GameObject>(_src);
        }
        IEnumerator IeUpdate()
        {
            while (true)
            {
                if (isEnabled)
                {
                    DropObstacle(list_src, Random.Range(min, max));
                }
                yield return new WaitForSeconds(Random.Range(2f, 5f));
            }
        }
        public void DropObstacle(List<GameObject> _src, int count)
        {
            for (int i = 0; i < count; i++)
            {
                GameObject go = Instantiate(_src.RandomOne());
                go.transform.position = GetRandomPos();
                list_CurDroppedBirds.Add(go);
                if (lifeTime!=0)
                {
                    Destroy(go, lifeTime);
                }
                go.transform.parent = goHolder.transform;
            }
        }
        Vector2 GetRandomPos()
        {
            return new Vector2(BirdInstpos_Bottom.position.x, Random.Range(BirdInstpos_Bottom.position.y, BirdInstpos_Top.position.y));
        }
    }
}
