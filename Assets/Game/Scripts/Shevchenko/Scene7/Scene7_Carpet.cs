﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.nScene7
{
    public class Scene7_Carpet : MonoBehaviour
    {
        public static Scene7_Carpet instance;
        Rigidbody2D rigid;
        float targetGravity = 1;
        public Transform SitPosPepa, SitPosGeorge;
        float prevY;
        float targetRotation;
        Transform bone;

        private void Awake()
        {
            instance = this;
            rigid = GetComponent<Rigidbody2D>();

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "SitPosPepa")
                {
                    SitPosPepa = item;
                }
                else
                if (item.name == "SitPosGeorge")
                {
                    SitPosGeorge = item;
                }
                else if (item.name == "Bone")
                {
                    bone = item.transform;
                }
            }
        }
        private void OnEnable()
        {
            Scene7_ObstacleBase.OnObstacleHit += Scene7_ObstacleBase_OnObstacleHit;
        }

        private void OnDisable()
        {
            Scene7_ObstacleBase.OnObstacleHit -= Scene7_ObstacleBase_OnObstacleHit;

        }

        private void Scene7_ObstacleBase_OnObstacleHit(Collider2D collision)
        {
            HitMe();
        }
        bool isVelocityCanGrow;
        Vector2 targetVelocity;
        bool isDown;
        IEnumerator ieVelocity;
        IEnumerator IeVelocity()
        {
            while (!Mathf.Approximately(rigid.velocity.magnitude, 0))
            {

                rigid.velocity = Vector2.Lerp(rigid.velocity, targetVelocity, Time.deltaTime * 5);
                yield return null;
            }
        }
        enum MoveDir { up, down }
        MoveDir lastMoveDir;
        void Update()
        {
            if (isDown)
            {
                targetRotation = 0;
            }
            else  if (prevY < transform.position.y)//вверх
            {
                Kover.instance.FlyUp();

                targetRotation = 12;
            }
            else if (prevY > transform.position.y)//вниз
            {
                Kover.instance.FlyDown();

                targetRotation = -12;
            }
            else // ровно
            {
                targetRotation = 0;
            }
            bone.transform.localRotation = Quaternion.Lerp(bone.transform.localRotation,
                Quaternion.Euler(new Vector3(0, 0, targetRotation)), Time.deltaTime * 2);

            prevY = transform.position.y;
            //transform.Translate(new Vector3(.1f, 0, 0));
            if (transform.position.y < 0)
            {
                if (!isDown)
                {
                    isDown = true;
                    if (ieVelocity != null) StopCoroutine(ieVelocity);
                    ieVelocity = IeVelocity();

                    StartCoroutine(ieVelocity);
                    Debug.Log("DOWN");
                }

                targetGravity = 0;
            }
            else
            {
                if (isDown)
                {
                    isDown = false;
                    if (ieVelocity != null) StopCoroutine(ieVelocity);

                    Debug.Log("!isDown");
                }
                targetGravity = 1;
            }
            rigid.gravityScale = Mathf.MoveTowards(rigid.gravityScale, targetGravity, Time.deltaTime);

        }
        public void PressMe()
        {
            if (transform.position.y < 25)
            {

                Kover.instance.UpClick();
                if (ieVelocity != null) StopCoroutine(ieVelocity);

                rigid.velocity = Vector3.zero;
                rigid.AddForce(Vector3.up * 10, ForceMode2D.Impulse);
            }
        }

        public void HitMe()
        {
            Kover.instance.ObstacleHit();
        }
        public void StopMovement()
        {
            rigid.isKinematic = true;
            rigid.velocity = Vector3.zero;

        }

    }
}
