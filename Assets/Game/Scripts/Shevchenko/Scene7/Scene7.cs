﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko.nScene7;
using DG.Tweening;
using PSV_Tutorials;
using PSV;

namespace Shevchenko
{

    public class Scene7 : GameController
    {
        public static Scene7 instance;
        public bool isSmallGame;
        public List<GameObject> list_SrcObstacles;

        DecorationController mainDecorationController;
        List<Scene7_Decors> list_decors = new List<Scene7_Decors>();
        FingerTutorial finger;

        public override void AwakeOther()
        {
            instance = this;
            list_SrcObstacles = new List<GameObject>(Resources.LoadAll<GameObject>("Scene7/Obstacles"));
            mainDecorationController = GetComponent<DecorationController>();
            list_decors = new List<Scene7_Decors>(FindObjectsOfType<Scene7_Decors>());
            FingerManager.Init();															   //

        }
        public override void OnEnable()
        {
            mainDecorationController.OnDecorationReplaced += DecorationController_OnDecorationReplaced;
            Scene7_ObstacleBase.OnObstacleHit += Scene7_ObstacleBase_OnObstacleHit;
        }

        public override void OnDisable()
        {
            mainDecorationController.OnDecorationReplaced -= DecorationController_OnDecorationReplaced;
            Scene7_ObstacleBase.OnObstacleHit -= Scene7_ObstacleBase_OnObstacleHit;

        }

        private void Scene7_ObstacleBase_OnObstacleHit(Collider2D collision)
        {
            pepa.Talk(new string[] { "hp-52", "hp-63", "hp-64", "ep-hp-16", "ep-hp-24" , "Джи смеется 2","Джи смеется 3" });
            foreach (var item in list_AllPlayers)
            {
                item.SetKoverHit();
            }
        }

        private void DecorationController_OnDecorationReplaced(DecorationItem decorItem)
        {
            for (int i = 0; i < Random.Range(5, 15); i++)
            {
                //decorItem.DropRandomObstacle(list_SrcObstacles.RandomOne());

            }
        }

        private void Update()
        {
            if (Scene7.instance.isSmallGame)
            {

                if (Input.GetMouseButtonDown(0))
                {
                    Scene7_Carpet.instance.PressMe();
                    SFX.Play("Звук вылета элементов панели, фруктов с корзины");
                }
            }
        }
        public override IEnumerator Process()
        {
            //CameraController.instance.AllowMove(true)
            //    .SetFollow(pepa.transform, new Vector2(0,3))
            //    .SetSpeedMove(5)
            //    .SetScale(6);
            george.SetTriggerEmotion(EmotionTriggerType.Joy);

            //У нас получился ковер-самолет!		hp-60
            yield return new WaitWhile(() => pepa.Talk("hp-60"));

            //Жми на ковер, чтобы отправиться в путь.		hp-61
            yield return new WaitWhile(() => pepa.Talk("hp-61"));
            finger = new FingerTutorial(new Tap(Scene7_Carpet.instance.transform.position, true).SetLoops(3));
            finger.Play();

            Scene7_BirdDropper.instance.SetSrc(list_SrcObstacles);
            Scene7_BirdDropper.instance.SetEnableDisable(true);

            yield return new WaitWhile(() => !Input.GetMouseButtonDown(0));
            pepa.Walk(Scene7_Carpet.instance.SitPosPepa.position);
            george.Walk(Scene7_Carpet.instance.SitPosGeorge.position);
            george.WalkSetWalkAnimType(WalkAnimType.SpecRun);
            yield return StartCoroutine(WaitFowWalkAll());
            pepa.transform.parent = Scene7_Carpet.instance.SitPosPepa.parent;
            george.transform.parent = Scene7_Carpet.instance.SitPosGeorge.parent;
            //Вперед! К той башне.		hp-62
            yield return new WaitWhile(() => pepa.Talk("hp-62"));


            yield return null;
            yield return StartCoroutine(SmallGame());

            //yield return new WaitForSeconds(5);

            Scene7_Carpet.instance.StopMovement();
            foreach (var item in list_decors)
            {
                item.AllowDisallowMove(false);
            }
            mainDecorationController.StopAutomaticReplace();

            GetComponent<FollowCamera>().target = null;

            CameraController.instance.AllowMove(true)
                .SetFollow(Scene7_Carpet.instance.transform)
                .SetSpeedMove(10)
                .SetScale(20)
                .SetCameraMovementType(CameraController.CameraMovementType.towards);

            mainDecorationController.ManualSetNextItem(GameObject.Find("bg_FIN").GetComponent<DecorationItem>());
            yield return StartCoroutine(MoveToCorrectPlaceTowards(Scene7_Carpet.instance.transform,
                GameObject.Find("FinPos").transform, 15));
            pepa.StopTalking();
            pepa.animPlayer.Play("joy");
            george.animPlayer.Play("joy");
            yield return new WaitForSeconds(2);

            MapProgressController.SetSceneCompleted();
                 SceneLoader.SwitchToScene(Scenes.Map);
         }
        public static IEnumerator MoveToCorrectPlaceTowards(Transform _from, Transform _to, float _speed)
        {
            Vector2 prev = _from.position;
            do
            {
                _from.position = Vector2.MoveTowards(_from.position, _to.position, Time.deltaTime * _speed);
                yield return new WaitForEndOfFrame();
                 prev = _from.position;

            } while (Vector2.Distance(_from.position, _to.position) > 0.005f);
        }
        public override IEnumerator SmallGame()
        {
            StartCoroutine(FingerHelper());
            foreach (var item in list_AllPlayers)
            {
                item.SetKoverSit();
            }
            Kover.instance.SetFly(true);

            //        CameraController.instance.AllowMove(true)
            //.SetFollow(Scene7_Carpet.instance.transform)
            //.SetSpeedMove(1)
            //.SetScale(12)
            //.SetCameraMovementType(CameraController.CameraMovementType.lerp);
            //CameraController.instance.AllowMove(false);
            GetComponent<FollowCamera>().target = Scene7_Carpet.instance.gameObject;
            isSmallGame = true;
            yield return new WaitForSeconds(50);
            isSmallGame = false;
            Scene7_BirdDropper.instance.SetEnableDisable(false);

            Debug.Log("finished!");
        }

        IEnumerator FingerHelper()
        {
            yield return new WaitForSeconds(3);
            finger = new FingerTutorial(new Tap(Scene7_Carpet.instance.transform.position, true).SetParent(Scene7_Carpet.instance.transform).SetLoops(5));
            finger.Play();

        }
    
    }
}
