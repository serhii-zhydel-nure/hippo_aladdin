﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour
{
    public static FollowCamera instance;

    public bool isSimpleFollow;

    public float interpVelocity;
    public float minDistance;
    public float followDistance;
    public GameObject target;
    public Vector3 offset;
    Vector3 targetPos;
    Camera currentCam;
    float targetCamScale, defaultCamScale;
    public float damping = .25f;
    public float scaleDelta = 1;
    private void Awake()
    {
        instance = this;
        currentCam = GetComponent<Camera>();
        targetCamScale = currentCam.orthographicSize;
        defaultCamScale = currentCam.orthographicSize;
    }
    void Start()
    {
        targetPos = transform.position;
    }

    void LateUpdate()
    {
        if (target)
        {
            if (isSimpleFollow)
            {
                Vector3 posNoZ = transform.position + offset;
                posNoZ.z = target.transform.position.z;

                Vector3 targetDirection = (target.transform.position - posNoZ);


                interpVelocity = targetDirection.magnitude * 20;

                targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

                transform.position = Vector3.MoveTowards(transform.position, targetPos, Time.deltaTime * damping);


                targetCamScale = Mathf.Clamp(defaultCamScale * targetDirection.magnitude, defaultCamScale, defaultCamScale * 1.5f);


                currentCam.orthographicSize = Mathf.Lerp(currentCam.orthographicSize, targetCamScale, Time.deltaTime);

            }
            else
            {
                Vector3 targetPosition = target.transform.position + offset;
                targetPosition = new Vector3(targetPosition.x, targetPosition.y, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, targetPosition, damping * Time.deltaTime);


            }
        }
    }


}