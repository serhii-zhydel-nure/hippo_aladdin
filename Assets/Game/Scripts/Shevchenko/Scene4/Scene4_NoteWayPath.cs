﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Shevchenko.nScene4
{
    public class Scene4_NoteWayPath : MonoBehaviour
    {
        public BezierSpline bezierSpline;
        public List<Transform> list_Nodes = new List<Transform>();
        private void Awake()
        {
            bezierSpline = GetComponent<BezierSpline>();
            list_Nodes.AddRange(GetComponentsInChildren<Transform>());
        }

        void Start()
        {

        }

        void Update()
        {

        }
    }
}
