﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko.nScene4
{
    public class Scene4_Snake : MonoBehaviour
    {

        public static Scene4_Snake instance;
        List<Transform> list_pulpits = new List<Transform>();
        List<SpriteRenderer> list_hypno = new List<SpriteRenderer>();
        public bool isShown = true;
        Animator anim;
        float targetColorA = 1;

        private void Awake()
        {
            instance = this;
            anim = GetComponent<Animator>();

            //foreach (var item in GetComponentsInChildren<Transform>(true))
            //{
            //    if (item.name.Contains("pulp"))
            //    {
            //        list_pulpits.Add(item);
            //    }
            //    else if (item.name.Contains("hypno"))
            //    {
            //        list_hypno.Add(item.GetComponent<SpriteRenderer>());
            //    }
            //}

        }

        bool isLastStateWasShow;

        public void SetShowHide(bool _show)
        {

            anim.SetBool("show", _show);

            if (_show)
            {
                if (!isLastStateWasShow)
                {
                    isLastStateWasShow = true;
                    int rnd = Random.Range(0, 3);
 
                    anim.SetInteger("idleType", rnd);
                }
            }
            else
            {
                isLastStateWasShow = false;
            }
        }
        public void SetAnger(bool _anger)
        {

            anim.SetBool("anger", _anger);

            if (_anger)
            {
                SFX.PlayLoop("шипение змеи");

                if (!isLastStateWasShow)
                {
                    isLastStateWasShow = true;
                    int rnd = Random.Range(0, 3);

                    anim.SetInteger("idleType", rnd);
                }
            }
            else
            {
                isLastStateWasShow = false;
                SFX.StopLoop("шипение змеи");

            }
        }
        private void Update()
        {
            //foreach (var item in list_hypno)
            //{
            //    item.transform.Rotate(item.transform.localRotation.x, item.transform.localRotation.y, -4);
            //    item.color = Color.Lerp(item.color, new Color(item.color.r, item.color.g, item.color.b, targetColorA), Time.deltaTime*3);
            //}


        }
    }
}

