﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.nScene4
{
    public class Scene4_Note : MonoBehaviour
    {
        public delegate void Click();
        public static event Click OnClickCorrect;
        public static event Click OnClickWrong;
        public static event Click OnNoteFullyMissed;
        public static event Click OnNoteMissed;

        public Scene4_NoteWayPath currentWay;
        public float speed = 2;
         public bool isInRange;
        public bool isUsed;
        public bool isFinishingWay;
        IEnumerator curColorLerp;

        SplineWalker splineWalker;
        static GameObject srcEffect;
        Animator anim;
        public bool isNext;
        public bool isMissed;
        Transform ImgObj, ImgTen;

        private void Awake()
        {
            if (!srcEffect)
            {
                srcEffect = Resources.Load<GameObject>("Scene4/2D_PickItem_04");
            }
            splineWalker = GetComponent<SplineWalker>();
            anim = GetComponent<Animator>();

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "ImgObj")
                {
                    ImgObj = item;
                }else if (item.name == "ImgTen")
                {
                    ImgTen = item;
                }
            }
        }
        public void InitNote(Scene4_NoteWayPath _NoteWayPath)
        {
            currentWay = _NoteWayPath;
            transform.position = currentWay.list_Nodes[0].transform.position;
            splineWalker.spline = _NoteWayPath.bezierSpline;
        }

        void Update()
        {
            //if (currentWay && !isWayEnded)
            //{
            //    transform.position = Vector3.MoveTowards(transform.position,
            //       currentWay.list_Nodes[currentNodeNum].transform.position, Time.deltaTime * speed);
            //    if (Vector3.Distance(transform.position,
            //        currentWay.list_Nodes[currentNodeNum].transform.position) == 0)
            //    {
            //        if (currentNodeNum == currentWay.list_Nodes.Count - 1)
            //        {
            //            isWayEnded = true;
            //        }
            //        if (currentNodeNum == currentWay.list_Nodes.Count - 2)
            //        {
            //            LerpColor(0);
            //        }
            //        currentNodeNum++;

            //    }
            //}
            if (splineWalker.progress > .8f)
            {
                if (!isFinishingWay)
                {
                    LerpColor(0);
                    AnimEnableDisable(false);
                    isFinishingWay = true;
                    isUsed = true;
                    if (OnNoteFullyMissed != null)
                        OnNoteFullyMissed();
                    Destroy(gameObject, 8);

                }
            }
            else if (splineWalker.progress > .7f&& splineWalker.progress < .8f)// пропустили ноту остановить всё
            {
                if (!isMissed && isNext&&!isUsed)
                {
                    isMissed = true;
                    if (OnNoteMissed != null)
                        OnNoteMissed();
                     
                }
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            //if (collision.name == "NoteActualPosition")
            //{
            //    if (!isUsed)
            //    {
            //        isInRange = true;
            //        if (!isUsed) LerpColor(Color.green);
            //        AnimEnableDisable(true);
            //    }
            //}
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            //if (collision.name == "NoteActualPosition")
            //{
            //    isInRange = false;
            //    if (!isUsed)
            //    {
            //        LerpColor(Color.white);
            //        OnNoteMissed();
            //        AnimEnableDisable(false);

            //    }
            //}
        }
        public void ClickMe()
        {
            if (!isUsed)
            {
                //AnimEnableDisable(false);

                //LerpColor(0);
                //isUsed = true;
                //if (isInRange)
                if (isNext)
                {
                    AnimEnableDisable(false);

                    LerpColor(0);
                    isUsed = true;

                    if (OnClickCorrect != null) OnClickCorrect();
                    Debug.Log("Correct");
                }
                else
                {
                    if (OnClickWrong != null) { OnClickWrong(); }
                }
                Destroy(Instantiate(srcEffect, transform.position, Quaternion.identity), 5);

            }
        }

        public void LerpColor(int _target)
        {
            if (curColorLerp != null)
            {
                StopCoroutine(curColorLerp);
            }
            curColorLerp = StaticParams.LerpColor(gameObject, _target,5);
            StartCoroutine(curColorLerp);
        }
        public void LerpColor(Color _col)
        {
            if (curColorLerp != null)
            {
                StopCoroutine(curColorLerp);
            }
            curColorLerp = StaticParams.LerpColor(gameObject, _col, 1);
            StartCoroutine(curColorLerp);
        }

        public void AnimEnableDisable(bool _enable)
        {
            anim.SetBool("isActive", _enable);
        }
        public void SetMove(bool _move)
        {
            splineWalker.isMoveAllowed = _move;
        }
        public void SetMeNext()
        {
            if (!isUsed)
            {
                isNext = true;

                isInRange = true;
                if (!isUsed) LerpColor(Color.green);
                AnimEnableDisable(true);
            }
        }
    }


}