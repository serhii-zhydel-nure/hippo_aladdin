﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko.nScene4;

namespace Shevchenko
{
    public class Scene4_NotesController : MonoBehaviour
    {
        public static Scene4_NotesController instance;

        List<GameObject> list_srcNotes;
        public List<Scene4_Note> list_NotesOnScene = new List<Scene4_Note>();
        List<Scene4_NoteWayPath> list_ways;
        public bool isSpawnAllowed = true;

        private void Awake()
        {
            instance = this;
            list_srcNotes = new List<GameObject>(Resources.LoadAll<GameObject>("Scene4/Notes"));
            list_ways = new List<Scene4_NoteWayPath>(FindObjectsOfType<Scene4_NoteWayPath>());

        }
        private void OnEnable()
        {
            Scene4_Note.OnClickCorrect += Scene4_Note_OnClickCorrect;
            Scene4_Note.OnClickWrong += Scene4_Note_OnClickWrong;
            Scene4_Note.OnNoteMissed += Scene4_Note_OnNoteMissed;
        }



        private void OnDisable()
        {
            Scene4_Note.OnClickCorrect -= Scene4_Note_OnClickCorrect;
            Scene4_Note.OnClickWrong -= Scene4_Note_OnClickWrong;
            Scene4_Note.OnNoteMissed -= Scene4_Note_OnNoteMissed;
        }

        private void Scene4_Note_OnNoteMissed()
        {
            SetMove(false);
        }

        private void Scene4_Note_OnClickWrong()
        {
            //throw new System.NotImplementedException();
        }

        private void Scene4_Note_OnClickCorrect()
        {
            SetMove(true);
        }

        public void Spawn()
        {
            if (isSpawnAllowed)
            {
                Scene4_NoteWayPath way = list_ways[Random.Range(0, list_ways.Count)];
                GameObject goSrc = list_srcNotes[Random.Range(0, list_srcNotes.Count)];
                GameObject go = (GameObject)Instantiate(goSrc);
                go.name = goSrc.name;
                go.GetComponent<Scene4_Note>().InitNote(way);
                list_NotesOnScene.Add(go.GetComponent<Scene4_Note>());
                LayerController.instance.AddNewJustInstWorker(go);

            }
            DefineNext();
        }
        public void GameEndAction()
        {
            list_NotesOnScene = list_NotesOnScene.ClearNulls();
            foreach (var item in list_NotesOnScene)
            {
                if (item != null)
                {
                    item.AnimEnableDisable(false);

                    item.isUsed = true;
                    item.LerpColor(0);
                }
            }
        }
        public void DefineNext()
        {
            //list_NotesOnScene =new List<Scene4_Note>( list_NotesOnScene.ClearNulls());
            List<Scene4_Note> temp = new List<Scene4_Note>();
            foreach (var item in list_NotesOnScene)
            {
                if (item != null)
                {
                    temp.Add(item);
                 }
             }
            list_NotesOnScene = new List< Scene4_Note>(temp);
            list_NotesOnScene.Sort(delegate (Scene4_Note us1, Scene4_Note us2)
            { return us1.transform.position.x.CompareTo(us2.transform.position.x); });
            for (int i = 0; i < list_NotesOnScene.Count; i++)
            {
                if (!list_NotesOnScene[i].isUsed)
                {
                    list_NotesOnScene[i].SetMeNext();
                    break;
                }
            }
        }
        public void SetMove(bool _move)
        {
            isSpawnAllowed = _move;
            foreach (var item in list_NotesOnScene)
            {
                if (item != null)
                {
                    item.SetMove(_move);
                }
            }
        }
    }
}
