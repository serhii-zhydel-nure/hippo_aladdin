﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko.nScene4;
using PSV;
using PSV_Tutorials;

namespace Shevchenko
{
    public class Scene4 : GameController
    {
        public delegate void SmallGameStarted();
        public static event SmallGameStarted OnSmallGameStarted;

        public static Scene4 instance;
        bool isSmallGame;
        float progressBarValue = .5f;
        AudioSource musicSpeaker;
        float targetVolume = 0;
        
        public override void AwakeOther()
        {
            instance = this;
            musicSpeaker = GameObject.Find("MusicSpeaker").GetComponent<AudioSource>();
            musicSpeaker.clip = Resources.Load<AudioClip>("Sounds/NonLanguage/Fleuta");
            musicSpeaker.loop = true;

        }
        public override void OnEnable()
        {
            Scene4_Note.OnClickCorrect += Scene4_Note_OnClickCorrect;
            Scene4_Note.OnClickWrong += Scene4_Note_OnClickWrong;
            Scene4_Note.OnNoteFullyMissed += Scene4_Note_OnNoteMissed;
        }

         public override  void OnDisable()
        {
            Scene4_Note.OnClickCorrect -= Scene4_Note_OnClickCorrect;
            Scene4_Note.OnClickWrong -= Scene4_Note_OnClickWrong;
            Scene4_Note.OnNoteFullyMissed -= Scene4_Note_OnNoteMissed;

        }
        private void Scene4_Note_OnNoteMissed()
        {
            Scene4_NotesController.instance.DefineNext();
            Scene4_NotesController.instance.SetMove(true);
            if (StaticParams.currentGameDifficulty == GameDifficulty.hard)
            {
                HardClickWrong();
            }

        }
        private void Scene4_Note_OnClickWrong()
        {
            Debug.Log("Wrong");

            //Это не та нота.		hp-43
              //Упс.		hp-44	
                   //Неверно.		hp-45	

            pepa.Say(new string[] { "hp-43", "hp-44", "hp-45" });

            if (StaticParams.currentGameDifficulty == GameDifficulty.easy)
            {
                //progressBarValue += .15f;
                HardClickWrong();

            }
            else
            {
                HardClickWrong();
            }
        }
        private void Scene4_Note_OnClickCorrect()
        {
            if (StaticParams.currentGameDifficulty == GameDifficulty.easy)
            {
                progressBarValue += .15f;
            }
            else
            {
                HardClickCorrect();
            }
        }
        public override IEnumerator Process()
        {
            SFX.PlayLoop("фон базара", .3f);

             StartCoroutine(StaticParams.LerpColorUI(Scene4_ProgressBar.instance.gameObject, 0, 10000));

            yield return new WaitForSeconds(1);
            //Scene4_Snake.instance.SetShowHide(true);
            //pApa.SetDudka(true);

            CameraController.instance.AllowMove(true)
               .SetCameraMovementType(CameraController.CameraMovementType.lerp)
               .SetFollow(george.transform, 2)
               .SetSpeedMove(8)
               .SetScale(8);
            foreach (var item in list_AllPlayers)
            {
                if (item.playerType == PlayerType.george || item.playerType == PlayerType.pEpa)
                {
                    item.WalkSetWalkAnimType(WalkAnimType.SpecRun);

                }
            }
            pepa.Walk("WayPoint (1)");
            george.Walk("WayPoint (2)");
            yield return StartCoroutine(WaitFowWalkAll(new PlayerController[] { pepa, george }));
            CameraController.instance
   .SetFollow("CamPos (2)")
   .SetSpeedMove(4)
   .SetScale(10);
            //Змея-змея!		ji-4
   yield return new WaitWhile(() => george.Talk("ji-4"));
            CameraController.instance
   .SetFollow(Scene4_Snake.instance.transform)
   .SetSpeedMove(4)
   .SetDelta(new Vector2(0,  3))
   .SetScale(6);
            Scene4_Snake.instance.SetShowHide(true);

            pApa.SetDudka(true);
            Scene4_Snake.instance.SetAnger(false);
             george.SetScare();
 
            //ГИППИ - Здорово! Я тоже так хочу.
            //Здорово! Я тоже так хочу.		hp-39	
            yield return new WaitWhile(() => pepa.Talk("hp-39"));

            CameraController.instance
   .SetFollow("CamPos (2)")
   .SetSpeedMove(4)
   .SetScale(10);

            //ГИППИ - Давай станем настоящими заклинателями змей.
            //Давай станем настоящими заклинателями змей.		hp-40
            yield return new WaitWhile(() => pepa.Talk("hp-40"));

            pApa.SetDudka(false);
            Scene4_Snake.instance.SetAnger(true);

            //ГИППИ - Жми на корзину со змеей, чтобы начать игру.
            //Жми на корзину со змеей, чтобы начать игру.		hp-41

            yield return new WaitWhile(() => pepa.Talk("hp-41"));

            finger = new FingerTutorial(
            new Tap(Scene4_Snake.instance.transform.position).SetLoops(-1) 

        ).SetLoops(1000).OnUpdate(() => Check());
            finger.Play();
            while (!Input.GetMouseButtonDown(0))
            {
                yield return null;
            }
            CameraController.instance.AllowMove(true)
 .SetCameraMovementType(CameraController.CameraMovementType.lerp)
  .SetScale(8);

            //Жми на ноты. 		hp-42
            pepa.TalkQueue("hp-42");

            if (SoundMusicController.instance)
                SoundMusicController.instance.SetVolume(0);
            AudioController.EnableMusic(false);
            yield return StartCoroutine(SmallGame());
            if (SoundMusicController.instance)
                 SoundMusicController.instance.SetVolume(.2f);
            AudioController.EnableMusic(true);

            CameraController.instance
 .SetFollow("CamPos (2)")
 .SetSpeedMove(4)
 .SetScale(9);
            yield return new WaitForSeconds(1);

            //Это было “чудес-с-с  дес-с мею).		hp-46
             yield return new WaitWhile(() => pepa.Talk("hp-46"));
            SFX.StopLoop("Толпа на рынке");

            pepa.SetApplause();
            george.SetApplause();
            pApa.SetReverance();

            yield return new WaitForSeconds(3);

            MapProgressController.SetSceneCompleted();
                 SceneLoader.SwitchToScene(Scenes.Map);
         }
        private void Update()
        {
            if (isSmallGame)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);

                    if (hits != null)
                    {
                        foreach (var item in hits)
                        {
                            if (item.collider.GetComponent<Scene4_Note>())
                            {
                                item.collider.GetComponent<Scene4_Note>().ClickMe();
                                Scene4_NotesController.instance.DefineNext();
                                SFX.Play("Собирает элементы коврика, убирает вещи на чердаке, находит Джи на рынке",.3f);
                            }

                        }
                    }
                }
            }
        }

        public override IEnumerator SmallGame()
        {
            if (OnSmallGameStarted != null)
            {
                OnSmallGameStarted();
            }

            Debug.Log("SmallGame");
            progressBarValue = 0;
            Scene4_Snake.instance.SetShowHide(true);

            StartCoroutine(StaticParams.LerpColorUI(Scene4_ProgressBar.instance.gameObject, 1, 1));
            StartCoroutine(StaticParams.LerpColor(Scene4_NoteActualPosition.instance.gameObject, 1, 1));
            isSmallGame = true;
            if (StaticParams.currentGameDifficulty == GameDifficulty.easy)
            {
                StartCoroutine(ProgressBar());

            }
            else
            {
                StartCoroutine(ProgressBar());

            }
            StartCoroutine(GameTimer());
            MusicPlayPause(true);

            while (isSmallGame)
            {
                yield return new WaitForSeconds(Random.Range(.5f, 1.2f));
                Scene4_NotesController.instance.Spawn();
            }
            StartCoroutine(GameEnd());
            Scene4_Snake.instance.SetShowHide(false);

        }

        IEnumerator GameTimer()
        {
            float smallGameTimer = 30;

            while (smallGameTimer > 0)
            {
                smallGameTimer -= Time.deltaTime;
                yield return null;

            }
            isSmallGame = false;
        }
        IEnumerator ProgressBar()
        {
            while (isSmallGame)
            {
                if (progressBarValue <= 0)
                {
                    //Scene4_Snake.instance.SetShowHide(false);
                    Scene4_Snake.instance.SetAnger(true);

                    //MusicPlayPause(false);
                    progressBarValue = 0;
                    targetVolume = 0;
                }
                else
                if (progressBarValue > 1)
                {
                    targetVolume = 1;
                    progressBarValue = 1;
                    progressBarValue -= Time.deltaTime / 8;
                    //Scene4_Snake.instance.SetShowHide(true);
                    Scene4_Snake.instance.SetAnger(false);

                    //MusicPlayPause(true);
                }
                else
                {
                    targetVolume = 1;

                    progressBarValue -= Time.deltaTime / 8;
                    //Scene4_Snake.instance.SetShowHide(true);
                    Scene4_Snake.instance.SetAnger(false);

                    //MusicPlayPause(true);
                }

                if (targetVolume == 0)
                {
                    pepa.SetDance(false);
                    george.SetDance(false);
                    pApa.SetDudka(false);
                }
                else
                {
                    pepa.SetDance(true);
                    george.SetDance(true);
                    pApa.SetDudka(true);
                }
                musicSpeaker.volume = Mathf.Lerp(musicSpeaker.volume, targetVolume, Time.deltaTime * 2);
                Scene4_ProgressBar.instance.SetProgress(progressBarValue);
                yield return null;
            }
        }
        IEnumerator GameEnd()
        {
            pepa.SetDance(false);
            george.SetDance(false);
            pApa.SetDudka(false);
            StartCoroutine(StaticParams.LerpColorUI(Scene4_ProgressBar.instance.gameObject, 0, 1));
            StartCoroutine(StaticParams.LerpColor(Scene4_NoteActualPosition.instance
                .gameObject, 0, 1));
            Scene4_NotesController.instance.GameEndAction();
            StaticParams.PushEffectFireworks();
            do
            {
                musicSpeaker.volume = Mathf.Lerp(musicSpeaker.volume, 0, Time.deltaTime * 2);
                yield return null;

            } while (musicSpeaker.volume != 0);
        }
        //IEnumerator HardSoundWorker()
        //{
        //    Scene4_Snake.instance.SetShowHide(true);
        //    targetVolume = 1;
        //    while (isSmallGame)
        //    {
        //        if (progressBarValue <= 0)
        //        {
        //            Scene4_Snake.instance.SetShowHide(false);
        //            //MusicPlayPause(false);
        //            progressBarValue = 0;
        //            targetVolume = 0;
        //        }
        //        else
        //        if (progressBarValue > 1)
        //        {
        //            targetVolume = 1;
        //            progressBarValue = 1;
        //            progressBarValue -= Time.deltaTime / 8;
        //            Scene4_Snake.instance.SetShowHide(true);

        //            //MusicPlayPause(true);
        //        }
        //        else
        //        {
        //            targetVolume = 1;

        //            progressBarValue -= Time.deltaTime / 8;
        //            Scene4_Snake.instance.SetShowHide(true);

        //            //MusicPlayPause(true);
        //        }

        //        musicSpeaker.volume = Mathf.Lerp(musicSpeaker.volume, targetVolume, Time.deltaTime * 2);
        //        Scene4_ProgressBar.instance.SetProgress(progressBarValue);
        //        yield return null;
        //    }
        //}
        void HardClickCorrect()
        {
            //Scene4_Snake.instance.SetShowHide(true);
            progressBarValue += .15f;
            targetVolume = 1;
            Scene4_NotesController.instance.SetMove(true);
        }
        void HardClickWrong()
        {
            //Scene4_Snake.instance.SetShowHide(false);
            progressBarValue = 0;
            targetVolume = 0;
            Scene4_NotesController.instance.SetMove(false);

        }
        void MusicPlayPause(bool _play)
        {
            if (_play)
            {
                if (StaticParams.isMusic)
                {
                    if (!musicSpeaker.isPlaying)
                    {
                        musicSpeaker.Play();

                    }
                    musicSpeaker.UnPause();
                }
            }
            else
            {
                musicSpeaker.Pause();
            }
        }
    }
}
