﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Shevchenko.nScene5
{
	public class Scene5_ProgressBar : ProgressBarBase
	{
		public static Scene5_ProgressBar instance;
		public RectTransform progressBar;

		void Awake()
		{
			instance = this;
		}

		public void MoveProgressBar(float percentage)
		{
			progressBar.localScale = new Vector3(percentage, 1, 1);
		}
	}
}
