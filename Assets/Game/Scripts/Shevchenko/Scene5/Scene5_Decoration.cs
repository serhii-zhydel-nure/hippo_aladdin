﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.nScene5
{
    public class Scene5_Decoration : MonoBehaviour
    {
        public bool isFinalDecoration;

        public Transform   leftPos, rightPos, CharFinalPos, CharFinalPos_2, 
            grandmaPosFirst, grandmaPosSecond,
            pepaPosFirst, pepaPosSecond;


        private void Awake()
        {

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "LeftPos")
                     leftPos = item;
                 else if (item.name == "RightPos")
                     rightPos = item;
                else if (item.name == "CharFinalPos")
                    CharFinalPos = item;
                else if (item.name == "grandmaPosFirst")
                    grandmaPosFirst = item;
                else if (item.name == "grandmaPosSecond")
                    grandmaPosSecond = item;
                else if (item.name == "pepaPosFirst")
                    pepaPosFirst = item;
                else if (item.name == "pepaPosSecond")
                    pepaPosSecond = item;
                //CharFinalPos (1)
                else if (item.name == "CharFinalPos (1)")
                    CharFinalPos_2 = item;
            }
        }
    }
}
