﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class ThrowSimulation : MonoBehaviour
    {
        public delegate void ThrowStart(Vector3 _pos);
        public static event ThrowStart OnThrow05;
        public delegate void Throw();
        public static event Throw OnThrowCancel;
        public static event Throw OnThrowSuccess;

        [Header("Разрешен ли апдейтовский ввод")]
        public bool isThrowAllowed;
        public bool is2d;

        //public Transform Target;
        public float firingAngle = 45.0f;
        public float gravity = 9.8f;
        public float delayBeforeDrop = 2;

        Transform thrownObj;
        GameObject src_ring, src_DelayTimer;
        private Transform myTransform;

        float rateDelayTimer;

        public virtual void Awake()
        {
            myTransform = transform;
            src_ring = Resources.Load<GameObject>("RingBell");
            src_DelayTimer = Resources.Load<GameObject>("DelayTimer");
            rateDelayTimer = 1 / delayBeforeDrop;
        }



        float timer;
        bool isTimeToTimer, isDropDone, isDelayStarted;
        Ray clickRay;
        float i_color = 0.0001f;
        GameObject goDelayTimer;
        Vector2 startClickPos;

        public virtual void Update()
        {
            if (isThrowAllowed)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                    i_color = 0.0001f;
                    startClickPos = Input.mousePosition;
                }
                if (Input.GetMouseButton(0))
                {
                    if (!isDropDone)
                    {
                        timer += Time.deltaTime;
                        if ((Vector2.Distance(startClickPos, new Vector2(Input.mousePosition.x, Input.mousePosition.y)) < Screen.width * 0.05f && Mathf.Abs(Input.mousePosition.x - startClickPos.x) < Screen.width * 0.05f))
                        {


                            if (timer > 0.5f && !isDelayStarted)
                            {
                                isDelayStarted = true;
                                RaycastHit hit;
                                if (Physics.Raycast(clickRay, out hit))
                                {
                                    goDelayTimer = (GameObject)Instantiate(src_DelayTimer, hit.point, Quaternion.identity);

                                }
                                if (OnThrow05 != null)
                                {
                                    OnThrow05(hit.point);
                                }
                            }
                        }
                        else // если увел палец
                        {
                            ThrowCancel();
                        }


                        if (timer > 0.5f)
                        {
                            RaycastHit hit;
                            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
                            {

                                goDelayTimer.transform.position = hit.point;

                            }
                            if (i_color < 1)
                            {
                                i_color += Time.deltaTime * rateDelayTimer;
                                if (goDelayTimer) goDelayTimer.GetComponentInChildren<Renderer>().material.SetFloat("_Cutoff", i_color);

                            }

                        }

                        if (timer > delayBeforeDrop + 0.5f)
                        {
                            isDropDone = true;

                            RaycastHit hit;
                            if (Physics.Raycast(clickRay, out hit))
                            {
                                Transform objectHit = hit.transform;
                                thrownObj = (GameObject.Instantiate(src_ring, transform.position, Quaternion.identity) as GameObject).transform;
                                StartCoroutine(SimulateThrow(hit.point));
                                if (OnThrowSuccess != null)
                                {
                                    OnThrowSuccess();
                                }
                            }

                        }
                    }

                }
                if (Input.GetMouseButtonUp(0))
                {
                    ThrowCancel();
                }
            }

        }

        public virtual void ThrowCancel()
        {
            isDelayStarted = false;
            isDropDone = false;
            timer = 0;
            Destroy(goDelayTimer);
            startClickPos = Vector2.zero;
            if (OnThrowCancel != null)
            {
                OnThrowCancel();
            }
        }
        //IEnumerator SimulateThrow(Transform _target)
        //{

        //    // Move projectile to the position of throwing object + add some offset if needed.
        //    bell.position = myTransform.position + new Vector3(0, 0.0f, 0);

        //    // Calculate distance to target
        //    float target_Distance = Vector3.Distance(bell.position, _target.position);

        //    // Calculate the velocity needed to throw the object to the target at specified angle.
        //    float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        //    // Extract the X  Y componenent of the velocity
        //    float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        //    float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        //    // Calculate flight time.
        //    float flightDuration = target_Distance / Vx;

        //    // Rotate projectile to face the target.
        //    bell.rotation = Quaternion.LookRotation(_target.position - bell.position);

        //    float elapse_time = 0;

        //    while (elapse_time < flightDuration)
        //    {
        //        bell.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

        //        elapse_time += Time.deltaTime;

        //        yield return null;
        //    }
        //    bell.GetComponent<BellController>().BellFallAction();

        //}
        public virtual IEnumerator SimulateThrow(Vector3 _target)
        {

            // Move projectile to the position of throwing object + add some offset if needed.
            thrownObj.position = myTransform.position + new Vector3(0, 0.0f, 0);

            // Calculate distance to target
            float target_Distance = Vector3.Distance(thrownObj.position, _target);

            // Calculate the velocity needed to throw the object to the target at specified angle.
            float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

            // Extract the X  Y componenent of the velocity
            float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
            float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

            // Calculate flight time.
            float flightDuration = target_Distance / Vx;

            // Rotate projectile to face the target.
            thrownObj.rotation = Quaternion.LookRotation(_target - thrownObj.position);

            float elapse_time = 0;

            while (elapse_time < flightDuration)
            {
                thrownObj.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

                elapse_time += Time.deltaTime;

                yield return null;
            }

        }

        public virtual IEnumerator SimulateThrow(Transform _thrownObj, Vector3 _target)
        {

            // Move projectile to the position of throwing object + add some offset if needed.
            _thrownObj.position = myTransform.position + new Vector3(0, 0.0f, 0);

            // Calculate distance to target
            float target_Distance = Vector3.Distance(_thrownObj.position, _target);

            // Calculate the velocity needed to throw the object to the target at specified angle.
            float projectile_Velocity = target_Distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

            // Extract the X  Y componenent of the velocity
            float Vx = Mathf.Sqrt(projectile_Velocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
            float Vy = Mathf.Sqrt(projectile_Velocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

            // Calculate flight time.
            float flightDuration = target_Distance / Vx;

            //if (is2d)
            //{
            //    // Rotate projectile to face the target.
            //    _thrownObj.rotation = Quaternion.LookRotation(_target - _thrownObj.position);

            //}

            float elapse_time = 0;

            while (elapse_time < flightDuration)
            {
                _thrownObj.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

                elapse_time += Time.deltaTime;

                yield return null;
            }

        }

    }
}