﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko.nScene5
{

    public class Scene5_HeadBox : MonoBehaviour
    {
        public static Scene5_HeadBox instance;
        List<Transform> list_Poses = new List<Transform>();
        List<Transform> list_InstantiatePoses = new List<Transform>();
        Transform obj;

        private void Awake()
        {
            instance = this;

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name.Contains("PosToHit"))
                {
                    list_Poses.Add(item);
                }
                else if (item.name.Contains("InstantiatePos"))
                {
                    list_InstantiatePoses.Add(item);
                }
                else if (item.name.Contains("OBJ"))
                {
                    obj = item;
                }
            }

        }
        public Transform GetRandomPoint()
        {
            return list_Poses[Random.Range(0, list_Poses.Count)];
        }
        public GameObject DropNewItem(GameObject _src)
        {
            GameObject go = (GameObject)Instantiate(_src);
            go.transform.position = list_InstantiatePoses[Random.Range(0, list_InstantiatePoses.Count)].position;
            return go;
        }
        public void SetEnableDisable(bool _enable)
        {
            obj.gameObject.SetActive(_enable);
        }
    }
}
