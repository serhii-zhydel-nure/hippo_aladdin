﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Shevchenko.nScene5
{
    public class Scene5_Item : ThrowSimulation
    {
        public delegate void GeorgeHit();
        public static event GeorgeHit OnGeorgeHit;
        public static event GeorgeHit OnHitFail;

        public enum ItemState
        { firstDrop, hitten }
        public ItemState currentItemState;

        private Sequence _sequence;
        public Transform target;
        Rigidbody2D rigid;

        private void Start()
        {
            defPos = transform.position;
            rigid = GetComponent<Rigidbody2D>();
        }

        Vector3 defPos;
        public override void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                //StartCoroutine(SimulateThrow(transform, target.position));

                Debug.Log("DOJump");
                //_sequence = transform.DOJump(target.position,4,  1, 1.5f).OnUpdate(()=>{ })
                //    .OnComplete(()=> 
                //    {

                //        Debug.Log("OnComplete");
                //    }) ;            
                HitMe();
            }
            if (Input.GetKeyDown(KeyCode.S))
            {

                Debug.Log("defPos");
                transform.position = defPos;
                rigid.velocity = Vector3.zero;
            }
            if (Input.GetKeyDown(KeyCode.D))
            {

                Debug.Log("zero");
                rigid.velocity = Vector3.zero;
            }
        }
        public void HitMe()
        {
            rigid.velocity = Vector3.zero;

            rigid.AddForce((Scene5_HeadBox.instance.GetRandomPoint()
                .position - transform.position).normalized * 15, ForceMode2D.Impulse);
            currentItemState = ItemState.hitten;
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (currentItemState == ItemState.hitten)
            {
                if (collision.name == "Collector")
                {
                    Destroy(gameObject);
                }
                else if (collision.name == "George")
                {
                    if (OnGeorgeHit!= null)
                    {
                        OnGeorgeHit();
                    }
                    collision.GetComponent<PlayerController>().SetTriggerEmotion(EmotionTriggerType.ScaredOnce);
                }
                else if (collision.name == "TriggerFail")
                {
                    if (OnHitFail != null)
                    {
                        OnHitFail();
                    }
                }
            }
        }
        private void OnBecameInvisible()
        {
            Destroy(gameObject);
         }
    }
}
