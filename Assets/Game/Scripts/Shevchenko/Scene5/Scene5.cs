﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko.nScene5;
using PSV;

namespace Shevchenko
{
    public class Scene5 : GameController
    {
        List<GameObject> list_SrcItems;
        List<GameObject> list_ItemsOnFloor = new List<GameObject>();

        List<Scene5_Decoration> mas_Decorations = new List<Scene5_Decoration>();
        Scene5_Decoration finalDecor;
        bool isSmallGame;
        float helperTimer, gameTimer;
        GameObject headBox;

        public static Scene5 instance;

        public override void AwakeOther()
        {
            instance = this;
            foreach (var item in FindObjectsOfType<Scene5_Decoration>())
            {
                if (!item.isFinalDecoration)
                {
                    mas_Decorations.Add(item);
                }
                else
                    finalDecor = item;


            }
            list_SrcItems = new List<GameObject>(Resources.LoadAll<GameObject>("Scene5/Fruits"));
            //headBox = FindObjectOfType<Scene5_HeadBox>().gameObject;
            //headBox.SetActive(false);
            Scene5_HeadBox.instance.SetEnableDisable(false);

            AudioController.Release();
            //AudioController.PlayMusic("Loop 22 song");
            AudioController.PlaySound("Loop 22 song", StreamGroup.MUSIC, 0.3f, true);
        }

        public override void OnEnable()
        {
            Scene5_Item.OnGeorgeHit += Scene5_Item_OnGeorgeHit;
            Scene5_Item.OnHitFail += Scene5_Item_OnHitFail;
        }

        public override void OnDisable()
        {
            Scene5_Item.OnGeorgeHit -= Scene5_Item_OnGeorgeHit;
            Scene5_Item.OnHitFail -= Scene5_Item_OnHitFail;


        }

        private void Scene5_Item_OnHitFail()
        {
            //Мимо.		hp-51
            if (!pepa.IsTalkingNow())
            {
                StartCoroutine(StaticParams.Say(pepa, "hp-51"));
            }
        }

        private void Scene5_Item_OnGeorgeHit()
        {
            //Упс!		hp-52

            if (!pepa.IsTalkingNow())
            {
                StartCoroutine(StaticParams.Say(pepa, "hp-52"));

            }
        }

        void Update()
        {
            if (isSmallGame)
            {
				helperTimer += Time.deltaTime;
                if (Input.GetMouseButtonDown(0))
                {
                    // Store the point where the user has clicked as a Vector3
                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    // Retrieve all raycast hits from the click position and store them in an array called "hits"
                    RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);


                    if (hits != null)
                    {
                        // Say which gameObject has been clicked (currently this code only works if there are TWO objects in the path of the raycast)
                        foreach (var item in hits)
                        {

                            if (item.collider.GetComponent<Scene5_Item>())
                            {
                                helperTimer = 0;
								AudioController.PlaySound("Подбирает упавшие овощи");
                                item.collider.GetComponent<Scene5_Item>().HitMe();
                                //fruitsOnFloor--;
                                list_ItemsOnFloor.Remove(item.collider.gameObject);
                                if (CheckVisibleOnFloor() < 3)
                                {
                                    george.walkSpeed = 3;
                                    grandMa.walkSpeed = 3;
                                    //									george.SetAnimSpeedMultiplyer(1f);
                                    //george.animPlayer.speed = 1;
                                    //grandMa.animPlayer.speed = 1;
                                }
                            }
                        }
                        // In here we'd want something like: if hit 1's sorting layer is higher than hit 0 then return hit 1
                    }
                }
                //				if ()
                //				{
                //					
                //				}
            }
        }

        public override IEnumerator Process()
        {
            yield return new WaitForEndOfFrame();
            if (CameraController.instance == null)
            {
                Debug.Log("No CameraController instance");
            }
            Debug.Log("Process");
            //			CameraController.instance.AllowMove(true)
            //                                    .SetFollow(grandMa.transform, new Vector2(0, 3))
            //									.SetSpeedMove(3)
            //                                    .SetScale(6.5f);
            grandMa.Walk("GrandmaWayPoint");
            yield return new WaitWhile(() => grandMa.IsWalkingNow());
            CameraController.instance.AllowMove(true)
             .SetFollow("FirstCamPos")
             .SetSpeedMove(3)
             .SetScale(6.5f);
            //О дитя мое, я стара и немощна, помоги мне донести корзину с фруктами.		gm-1
            yield return new WaitWhile(() => grandMa.Talk("gm-1"));
            //headBox.SetActive(true);
            //CameraController.instance.SetFollow(george.transform, new Vector2(0, 3))
            //                     .SetSpeedMove(3)
            //                     .SetScale(5);
            george.SetPhoto();
            GameObject goHeadBoxDecor = GameObject.Find("HeadBoxDecor");
            yield return StartCoroutine(StaticParams.MoveToCorrectPlace(goHeadBoxDecor.transform, Scene5_HeadBox.instance.transform, 5));
            goHeadBoxDecor.SetActive(false);
            Scene5_HeadBox.instance.SetEnableDisable(true);

            george.SetTriggerEmotion(EmotionTriggerType.Joy);
            //Мы с превеликой радостью поможем вам.		hp-47	
            yield return new WaitWhile(() => pepa.Talk("hp-47"));
			yield return new WaitForSeconds(0.2f);
			yield return new WaitWhile(() => pepa.Talk("hp-49"));
            yield return StartCoroutine(SmallGame());

            george.walkSpeed = 3;
            grandMa.walkSpeed = 3;

            //george.animPlayer.speed = 1;
            //grandMa.animPlayer.speed = 1;

            CameraController.instance.SetFollow(george.transform, new Vector2(1, 3))
                .SetSpeedMove(3)
                .SetScale(5);
            //grandMa.transform.position = finalDecor.grandmaPosFirst.position;
            grandMa.Walk(finalDecor.grandmaPosSecond.position);

            pepa.transform.position = finalDecor.pepaPosFirst.position;
            pepa.Walk(finalDecor.pepaPosSecond.position);
            Debug.Log("WaitFowWalkAll()");
            yield return StartCoroutine(WaitFowWalkAll());
            Debug.Log("EVERYBODY FINISHED WALKING");
            //Благодарю вас, дети мои, да хранит вас аллах.		gm-2
            yield return new WaitWhile(() => grandMa.Talk("gm-2"));

            CameraController.instance
            .SetScale(7);

            pepa.SetTriggerEmotion(EmotionTriggerType.Joy);
            george.SetTriggerEmotion(EmotionTriggerType.Joy);
            yield return new WaitForSeconds(2);
            MapProgressController.SetSceneCompleted();
                 SceneLoader.SwitchToScene(Scenes.Map);
         }

		float startPos;
		public float targetDistance = 90;
		public float distance;
        public override IEnumerator SmallGame()
        {
            isSmallGame = true;
			startPos = george.transform.position.x;
            AudioController.PlaySound("Джи смеется 3");
            CameraController.instance.SetFollow(george.transform, new Vector2(-2, 3));
            george.WalkSetWalkAnimType(WalkAnimType.SpecRun);
            george.walkSpeed = 3;
            grandMa.walkSpeed = 3;

            george.Walk(10000);
            grandMa.Walk(10000);
            //			george.Walk("GeorgeWayPoint");
            StartCoroutine(DecorationWorker());
            //			StartCoroutine(Instantiator());
            StartCoroutine(Helper());

            while (isSmallGame)
            {
//                gameTimer += Time.deltaTime;
				distance = george.transform.position.x - startPos;
//                float percentage = 1 - (gameTimer / 30);
				float percentage = 1 - (distance / targetDistance);
                //				Debug.Log("Percentage: " + percentage);
                Scene5_ProgressBar.instance.MoveProgressBar(percentage);
				if (distance > targetDistance)
                {
                    isSmallGame = false;
					george.walkSpeed = 3f;
					grandMa.walkSpeed = 3f;
                }
                yield return null;
            }
            //george.animPlayer.speed = 1;
            //grandMa.animPlayer.speed = 1;
            yield return StartCoroutine(FinalDecorationWorker());
            george.Walk(finalDecor.CharFinalPos.position);
            grandMa.Walk(finalDecor.CharFinalPos_2.position);
        }

        bool currentMoveDirRight = true;
        float lastXpos = 34;

        IEnumerator DecorationWorker()
        {
            while (true)
            {
                if (currentMoveDirRight)
                {
                    mas_Decorations.Sort(delegate (Scene5_Decoration us1, Scene5_Decoration us2)
                        {
                            return us2.transform.transform.position.x.CompareTo(us1.transform.transform.position.x);
                        });

                    foreach (var item in mas_Decorations)
                    {
                        if (item.rightPos.position.x < george.transform.position.x)
                        {
                            Vector3 screenPoint = Camera.main.WorldToViewportPoint(item.rightPos.position);
                            bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                            if (!onScreen)
                            {
                                //lastXpos += 34;
                                item.transform.position = new Vector3(mas_Decorations[0].transform.position.x + 19, item.transform.position.y, 0);
                            }
                        }
                    }
                }
                else
                {
                    mas_Decorations.Sort(delegate (Scene5_Decoration us1, Scene5_Decoration us2)
                        {
                            return us1.transform.position.x.CompareTo(us2.transform.position.x);
                        });

                    foreach (var item in mas_Decorations)
                    {
                        if (item.leftPos.position.x > george.transform.position.x)
                        {
                            Vector3 screenPoint = Camera.main.WorldToViewportPoint(item.leftPos.position);
                            bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;


                            if (!onScreen)
                            {
                                lastXpos -= 34;
                                item.transform.position = new Vector3(mas_Decorations[0].transform.position.x - 34, item.transform.position.y, 0);
                            }
                        }
                    }
                }

                yield return new WaitForSeconds(1);
            }
        }

        IEnumerator FinalDecorationWorker()
        {
            Debug.Log("Message");

            while (true)
            {
                if (currentMoveDirRight)
                {
                    mas_Decorations.Sort(delegate (Scene5_Decoration us1, Scene5_Decoration us2)
                        {
                            return us2.transform.transform.position.x.CompareTo(us1.transform.transform.position.x);
                        });

                    foreach (var item in mas_Decorations)
                    {
                        if (item.rightPos.position.x < george.transform.position.x)
                        {
                            Vector3 screenPoint = Camera.main.WorldToViewportPoint(item.rightPos.position);
                            bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                            if (!onScreen)
                            {
                                Debug.Log("Message");
                                //lastXpos += 34;
                                finalDecor.transform.position =
                                    new Vector3(mas_Decorations[0].transform.position.x + 19, item.transform.position.y, 0);
                                yield break;
                            }
                        }
                    }
                }
                yield return null;
            }
        }

        IEnumerator Helper()
        {
            while (isSmallGame)
            {
                if (helperTimer > 8)
                {
                    helperTimer = 0;
					Debug.Log("HELP");
                    //Жми на фрукты, чтобы закинуть их обратно в корзину.		hp-49																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																													
                    //Жми на фрукты.		hp-50																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																													

                    yield return new WaitWhile(() => pepa.Talk(new string[] { "hp-49", "hp-50" }));
                }
                yield return null;

            }
        }

        //int fruitsOnFloor;

        public IEnumerator Instantiator()
        {
            //			while (isSmallGame)
            //			{
            //				Scene5_HeadBox.instance.DropNewItem(list_SrcItems.RandomOne());
            //				yield return new WaitForSeconds(Random.Range(.1f, 1f));
            //			}
            yield return new WaitForSeconds(0.4f);
            //			int fruitsCount = UnityEngine.Random.Range(2, 5);
            for (int i = 0; i < UnityEngine.Random.Range(1, 4); i++)
            {
                AudioController.PlaySound("Звук вылета элементов панели, фруктов с корзины");
                GameObject go = Scene5_HeadBox.instance.DropNewItem(list_SrcItems.RandomOne());
                list_ItemsOnFloor.Add(go);

 
                if (CheckVisibleOnFloor() > 2)
                {
                    george.walkSpeed = 1.5f;
                    grandMa.walkSpeed = 1.5f;

                    //					george.SetAnimSpeedMultiplyer(0.5f);
//                    george.animPlayer.speed = 0.5f;
//                    grandMa.animPlayer.speed = 0.5f;
                }

                yield return new WaitForSeconds(Random.Range(.1f, 0.5f));
            }

        }
        int CheckVisibleOnFloor()
        {
            int fruitsOnFloor = 0;
            foreach (var item in list_ItemsOnFloor)
            {
                if (item)
                {
                    Vector3 screenPoint = Camera.main.WorldToViewportPoint(item.transform.position);
                    bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                    if (onScreen)
                    {
                        fruitsOnFloor++;
                    }
                }

            }
             return fruitsOnFloor;
        }
    }
}
