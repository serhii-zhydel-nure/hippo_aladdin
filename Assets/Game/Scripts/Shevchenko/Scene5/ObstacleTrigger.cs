﻿using UnityEngine;
using System.Collections;
using Shevchenko;

public class ObstacleTrigger : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		//Debug.Log("Obstacle Trigger 0");
		if (other.GetComponent<PlayerController>())
		{
			if (other.GetComponent<PlayerController>().playerType == PlayerType.george)
			{
				//Debug.Log("Obstacle Trigger");
				other.GetComponent<PlayerController>().animPlayer.SetInteger("ActionType", 15);
				other.GetComponent<PlayerController>().animPlayer.SetTrigger("Action");
				StartCoroutine(Scene5.instance.Instantiator());
			}

		}
	}
}
