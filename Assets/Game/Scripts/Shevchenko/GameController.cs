﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV_Tutorials;
namespace Shevchenko
{
    public class GameController : MonoBehaviour
    {
        [HideInInspector]
        public PlayerController pepa, george, pApa, mama, grandMa;

        public List<PlayerController> list_AllPlayers = new List<PlayerController>();
        [HideInInspector]
        public FingerTutorial finger;

        public virtual void Awake()
        {
            foreach (var item in FindObjectsOfType<PlayerController>())
            {
                if (item.playerType == PlayerType.pEpa)
                {
                    pepa = item;
                    list_AllPlayers.Add(item);
                }
                    

                else if (item.playerType == PlayerType.george)
                {
                    george = item;
                    list_AllPlayers.Add(item);
                }
                    
                else if (item.playerType == PlayerType.pApa)
                {
                    pApa = item;
                    list_AllPlayers.Add(item);
                }
                    
                else if (item.playerType == PlayerType.mama)
                {
                    mama = item;
                    list_AllPlayers.Add(item);
                }
                    
                else if (item.playerType == PlayerType.grandMa)
                {
                    grandMa = item;
                    list_AllPlayers.Add(item);
                }
                    
                //list_AllPlayers.Add(item);
            }

            AwakeOther();
            FingerManager.Init();															   //
        }
        public virtual void OnEnable()
        {

        }
        public virtual void OnDisable()
        {

        }
        public virtual void Start()
        {
            StartCoroutine(Process());
        }
        public virtual void AwakeOther()
        {

        }

        public virtual IEnumerator Process()
        {
            yield return null;
        }
        public virtual IEnumerator SmallGame()
        {
            yield return null;
        }
        public virtual IEnumerator WaitFowWalkAll()
        {
            do
            {
                int walkingChars = 0;
                foreach (var item in list_AllPlayers)
                {
                    if (item.IsWalkingNow())
                    {
                        walkingChars++;
                    }
                }
                if (walkingChars == 0)
                {
                    yield break;
                }
                yield return null;
            } while (true);
        }
        public virtual IEnumerator WaitFowWalkAll(PlayerController[] _players)
        {
            do
            {
                int walkingChars = 0;
                foreach (var item in _players)
                {
                    if (item.IsWalkingNow())
                    {
                        walkingChars++;
                    }
                }
                if (walkingChars == 0)
                {
                    yield break;
                }
                yield return null;
            } while (true);
        }
        public void Check()
        {
            if (Input.GetMouseButtonDown(0) || Input.touchCount > 0)
            {
                if (finger.IsPlaying)
                {
                    finger.Hide();
                }
             }
            
        }
    }
}