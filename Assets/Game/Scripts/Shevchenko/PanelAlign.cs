﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
    public class PanelAlign : MonoBehaviour
    {
        public delegate void PanelAction(PanelAlign panel);
        public static event PanelAction OnPanelShow;

        public enum PanelType
        {
            left, right, top
        }
        public enum PanelState
        {
            hide, show, shownNow, hiddenNow
        }
        public class PanelElementParams
        {
            public GameObject go;
            public Vector2 defaultLocalPos;
            public DragGame_Child dragGame_Child;
        }
        [System.Serializable]
        public class OrdersLayerParams
        {
            public int defaultOrderInLayer;
            public SpriteRenderer currentSprite;
            public string defaultSortingLayerName;
        }

        [Header("Если нужна отдельная камера")]
        public Camera curCamera;
        public PanelType currentPanelType;
        public PanelState currentPanelState;
        public float deltaStepHide = 0.1f;
        public float deltaStepShow = 0.1f;

        public float deltaStep = 0.1f;
        public bool needChangeLayers;

        float deltaScreenPercent;
        Vector2 correctPlace;

        Transform targetPos;
        float defaultCameraOrtoSize;
        bool isMoveByCamAllowed = false;

        List<PanelElementParams> list_CurrentPanelElementParams = new List<PanelElementParams>();
        public List<OrdersLayerParams> list_orderParams = new List<OrdersLayerParams>();
        [Header("Отсутп по второй оси")]
        public float deltaStepOtherAxis = 1;

        void Awake()
        {
            if (!curCamera)
            {
                curCamera = Camera.main;
            }
            deltaStep = deltaStepHide;
            foreach (var item in GetComponentsInChildren<DragGame_Child>())
            {
                PanelElementParams pep = new PanelElementParams();
                pep.go = item.gameObject;
                pep.defaultLocalPos = item.transform.localPosition;
                pep.dragGame_Child = item.GetComponent<DragGame_Child>();
                list_CurrentPanelElementParams.Add(pep);
            }

            foreach (var item in GetComponentsInChildren<SpriteRenderer>(true))
            {
                item.sortingLayerName = "UI";
            }

            foreach (SpriteRenderer item in GetComponentsInChildren<SpriteRenderer>(true))
            {
                OrdersLayerParams op = new OrdersLayerParams();
                op.currentSprite = item;
                op.defaultOrderInLayer = item.sortingOrder;
                op.defaultSortingLayerName = item.sortingLayerName;
                list_orderParams.Add(op);
            }
        }
        void Start()
        {
            switch (currentPanelType)
            {
                case PanelType.left:
                    transform.position =
                        curCamera.ScreenToWorldPoint(new Vector3(0, Screen.height * 0.5f, 10));
                    transform.position = new Vector3(transform.position.x - deltaStep, transform.position.y, transform.position.z);

                    correctPlace = curCamera.ScreenToWorldPoint(new Vector3(0, Screen.height * 0.5f, 10)); ;
                    correctPlace = new Vector3(correctPlace.x + deltaStep, transform.position.y, transform.position.z);
                    break;
                case PanelType.right:
                    transform.position =
        curCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height * 0.5f, 10));
                    transform.position = new Vector3(transform.position.x + deltaStep, transform.position.y, transform.position.z);

                    correctPlace = curCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height * 0.5f, 10)); ;
                    correctPlace = new Vector3(correctPlace.x - deltaStep, transform.position.y, transform.position.z);

                    break;

                case PanelType.top:
                    transform.position =
        curCamera.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height, 10));
                    transform.position = new Vector3(transform.position.x, transform.position.y + deltaStep, transform.position.z);

                    correctPlace = curCamera.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * deltaStep, 10)); ;
                    //correctPlace = new Vector3(transform.position.x, correctPlace.y - Screen.height * deltaX, transform.position.z);

                    //deltaScreenPercent
                    break;

                default:
                    break;
            }
            GameObject go = new GameObject();
            go.transform.position = transform.position;
            go.name = name + "_targetPos";
            go.transform.parent = transform.parent;
            targetPos = go.transform;

            defaultCameraOrtoSize = curCamera.orthographicSize;
            if (currentPanelState == PanelState.hide)
            {
                UpdateMove(transform, currentPanelType == PanelType.top ? -1 : -3);

            }
        }

        public void ShowPanel()
        {
            SFX.Play("Звук вылета элементов панели, фруктов с корзины");
            deltaStep = deltaStepShow;

            //StaticParams.OnMoveToCorrectPlaceEnded += StaticParams_OnMoveToCorrectPlaceEnded;
            //StartCoroutine(StaticParams.MoveToCorrectPlace(transform, correctPlace, 2));
            currentPanelState = PanelState.show;
            if (needChangeLayers)
            {
                SetManual__OrderInLayer(100);
                foreach (var item in list_CurrentPanelElementParams)
                {
                    item.dragGame_Child.UpdateDefaultLayerParams();
                }
            }
            foreach (var item2 in list_CurrentPanelElementParams)
            {
                if (item2.dragGame_Child)
                {
                    item2.dragGame_Child.TurnMyCollider(true);
                }

            }
            if (OnPanelShow != null)
            {
                OnPanelShow(this);
            }
        }


        public void HidePanel()
        {
            deltaStep = deltaStepHide;
            isMoveByCamAllowed = false;
            //StartCoroutine(StaticParams.MoveToCorrectPlace(transform, targetPos, 2));
            currentPanelState = PanelState.hide;
            if (needChangeLayers)
            {
                ReturnDefault__OrderInLayer();
                foreach (var item in list_CurrentPanelElementParams)
                {
                    item.dragGame_Child.UpdateDefaultLayerParams();
                }
            }
            foreach (var item2 in list_CurrentPanelElementParams)
            {
                if (item2.dragGame_Child)
                {
                    item2.dragGame_Child.TurnMyCollider(false);
                }

            }
        }

        public static void HideAllPanels()
        {
            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
            {

                item.HidePanel();
            }
        }

        public static void ShowAllPanels()
        {
            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
            {

                item.ShowPanel();
            }
        }

        public static void ShowPanel(string _name)
        {
            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
            {
                if (item.name == _name)
                {

                    item.ShowPanel();
                    break;
                }
            }
        }

        public static void HidePanel(string _name)
        {
            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
            {
                if (item.name == _name)
                {

                    item.HidePanel();
                    break;
                }
            }
        }
        void OnBecameVisible()
        {

            Debug.Log("1111");
        }
        void LateUpdate()
        {
            if (transform.parent != null)
            {
                //if (isMoveByCamAllowed)
                //{



                //    switch (currentPanelType)
                //    {
                //        case PanelType.left:

                //            correctPlace = curCamera.ScreenToWorldPoint(new Vector3(0 + Screen.width * deltaStep, Screen.height * 0.5f, 10)); ;
                //            transform.position = correctPlace;
                //            targetPos.position = curCamera.ScreenToWorldPoint(new Vector3(0 + Screen.width * deltaStep, Screen.height * 0.5f, 10));
                //            break;
                //        case PanelType.right:

                //            correctPlace = curCamera.ScreenToWorldPoint(new Vector3(Screen.width - Screen.width * deltaStep, Screen.height * 0.5f, 10));
                //            transform.position = correctPlace;
                //            targetPos.position = curCamera.ScreenToWorldPoint(new Vector3(Screen.width + Screen.width * deltaStep, Screen.height * 0.5f, 10));

                //            break;

                //        case PanelType.top:
                //            correctPlace = curCamera.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height - Screen.height * deltaStep, 10));
                //            transform.position = correctPlace;
                //            targetPos.position = curCamera.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height + Screen.height * deltaStep, 10));
                //            break;

                //        default:
                //            break;
                //    }
                //}else
                //{
                float size = curCamera.orthographicSize / defaultCameraOrtoSize;
                transform.localScale = new Vector3(size, size, size);

                switch (currentPanelState)
                {
                    case PanelState.hide:
                        deltaStep = deltaStepHide;
                        UpdateMove(targetPos, currentPanelType == PanelType.top ? -1 : -3);
                        transform.position = Vector2.Lerp(transform.position, targetPos.position, Time.deltaTime * 4);
                        if (Vector2.Distance(transform.position, targetPos.position) < 0.05f)
                        {
                            currentPanelState = PanelState.hiddenNow;
                        }
                        break;
                    case PanelState.show:
                        deltaStep = deltaStepShow;
                        UpdateMove(targetPos, 1);
                        if (Vector2.Distance(transform.position, targetPos.position) < 0.05f)
                        {
                            currentPanelState = PanelState.shownNow;
                        }
                        transform.position = Vector2.Lerp(transform.position, targetPos.position, Time.deltaTime * 3);

                        break;
                    case PanelState.shownNow:
                        UpdateMove(targetPos, 1);
                        transform.position = Vector2.Lerp(transform.position, targetPos.position, Time.deltaTime * 5);

                        break;
                    case PanelState.hiddenNow:
                        UpdateMove(transform, currentPanelType == PanelType.top ? -1 : -3);

                        break;
                    default:
                        break;
                }


                //}
            }
        }

        void UpdateMove(Transform _objToMove, int _dir)
        {
            switch (currentPanelType)
            {
                case PanelType.left:
                    _objToMove.position = curCamera.ScreenToWorldPoint(new Vector3(0 + Screen.width * deltaStep * _dir, Screen.height * 0.5f * deltaStepOtherAxis, 10)); ;
                    break;
                case PanelType.right:
                    _objToMove.position = curCamera.ScreenToWorldPoint(new Vector3(Screen.width - Screen.width * deltaStep * _dir, Screen.height * 0.5f * deltaStepOtherAxis, 10));
                    break;

                case PanelType.top:
                    _objToMove.position = curCamera.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f * deltaStepOtherAxis, Screen.height - Screen.height * deltaStep * _dir, 10));
                    break;

                default:
                    break;
            }
        }

        //public void ReturnAllElements()
        //{
        //    List<PanelElementParams> temp = new List<PanelElementParams>();
        //    foreach (var item in list_CurrentPanelElementParams)
        //    {

        //    }
        //}

        public static void TurnColliders(string _name, bool _on)
        {
            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
            {
                if (item.name == _name)
                {
                    foreach (var item2 in item.list_CurrentPanelElementParams)
                    {
                        item2.dragGame_Child.TurnMyCollider(_on);
                    }

                    break;
                }
            }

        }

        public void ReturnDefault__OrderInLayer()
        {
            if (GetComponent<LayerWorker>()) return;
            if (transform.parent) if (transform.parent.GetComponent<LayerWorker>()) return;


            foreach (var item in list_orderParams)
            {
                item.currentSprite.sortingOrder = item.defaultOrderInLayer;
            }
        }

        public void SetManual__OrderInLayer(int _orderInLayer)
        {
            foreach (var item in list_orderParams)
            {
                item.currentSprite.sortingOrder = _orderInLayer + item.defaultOrderInLayer;
            }
        }

    }
}



//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;

//namespace Shevchenko
//{
//    public class PanelAlign : MonoBehaviour
//    {
//        public delegate void PanelAction(PanelAlign panel);
//        public static event PanelAction OnPanelShow;

//        public enum PanelType
//        {
//            left, right, top
//        }
//        public enum PanelState
//        {
//            hide, show, shownNow, hiddenNow
//        }
//        public class PanelElementParams
//        {
//            public GameObject go;
//            public Vector2 defaultLocalPos;
//            //public DragGame_Child dragGame_Child;
//        }
//        [System.Serializable]
//        public class OrdersLayerParams
//        {
//            public int defaultOrderInLayer;
//            public SpriteRenderer currentSprite;
//            public string defaultSortingLayerName;
//        }

//        public bool isNeedScale = false;
//        public PanelType currentPanelType;
//        public PanelState currentPanelState;
//        public float deltaStepHide = 0.1f;
//        public float deltaStepShow = 0.1f;

//        public float deltaStep = 0.1f;
//        public bool needChangeLayers;

//        float deltaScreenPercent;
//        Vector2 correctPlace;

//        Transform targetPos;
//        float defaultCameraOrtoSize;
//        bool isMoveByCamAllowed = false;

//        List<PanelElementParams> list_CurrentPanelElementParams = new List<PanelElementParams>();
//        public List<OrdersLayerParams> list_orderParams = new List<OrdersLayerParams>();

//        [Header("Никогда не прятать методом HideAllPanels")]
//        public bool isNeverHide;
//        [Header("Отсутп по второй оси")]
//        public float deltaStepOtherAxis = 1;
//        void Awake()
//        {
//            deltaStep = deltaStepShow;
//            //foreach (var item in GetComponentsInChildren<DragGame_Child>())
//            //{
//            //    PanelElementParams pep = new PanelElementParams();
//            //    pep.go = item.gameObject;
//            //    pep.defaultLocalPos = item.transform.localPosition;
//            //    //pep.dragGame_Child = item.GetComponent<DragGame_Child>();
//            //    list_CurrentPanelElementParams.Add(pep);
//            //}

//            foreach (var item in GetComponentsInChildren<SpriteRenderer>(true))
//            {
//                item.sortingLayerName = "UI";
//            }

//            foreach (SpriteRenderer item in GetComponentsInChildren<SpriteRenderer>(true))
//            {
//                OrdersLayerParams op = new OrdersLayerParams();
//                op.currentSprite = item;
//                op.defaultOrderInLayer = item.sortingOrder;
//                op.defaultSortingLayerName = item.sortingLayerName;
//                list_orderParams.Add(op);
//            }
//        }
//        void Start()
//        {
//            switch (currentPanelType)
//            {
//                case PanelType.left:
//                    transform.position =
//                        Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * 0.5f, 10));
//                    transform.position = new Vector3(transform.position.x - deltaStep, transform.position.y, transform.position.z);

//                    correctPlace = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * 0.5f, 10)); ;
//                    correctPlace = new Vector3(correctPlace.x + deltaStep, transform.position.y, transform.position.z);
//                    break;
//                case PanelType.right:
//                    transform.position =
//        Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height * 0.5f, 10));
//                    transform.position = new Vector3(transform.position.x + deltaStep, transform.position.y, transform.position.z);

//                    correctPlace = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height * 0.5f, 10)); ;
//                    correctPlace = new Vector3(correctPlace.x - deltaStep, transform.position.y, transform.position.z);

//                    break;

//                case PanelType.top:
//                    transform.position =
//        Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height, 10));
//                    transform.position = new Vector3(transform.position.x, transform.position.y + deltaStep, transform.position.z);

//                    correctPlace = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height * deltaStep, 10)); ;
//                    //correctPlace = new Vector3(transform.position.x, correctPlace.y - Screen.height * deltaX, transform.position.z);

//                    //deltaScreenPercent
//                    break;

//                default:
//                    break;
//            }
//            GameObject go = new GameObject();
//            go.transform.position = transform.position;
//            go.name = name + "_targetPos";
//            go.transform.parent = transform.parent;
//            targetPos = go.transform;

//            defaultCameraOrtoSize = Camera.main.orthographicSize;
//            if (currentPanelState == PanelState.hide)
//            {
//                UpdateMove(transform, currentPanelType == PanelType.top ? -1 : -3);

//            }
//        }

//        public void ShowPanel()
//        {
//            deltaStep = deltaStepShow;
//            transform.SetAsLastSibling();
//            //StaticParams.OnMoveToCorrectPlaceEnded += StaticParams_OnMoveToCorrectPlaceEnded;
//            //StartCoroutine(StaticParams.MoveToCorrectPlace(transform, correctPlace, 2));
//            currentPanelState = PanelState.show;
//            if (needChangeLayers)
//            {
//                SetManual__OrderInLayer(100);
//                //foreach (var item in list_CurrentPanelElementParams)
//                //{
//                //    //item.dragGame_Child.UpdateDefaultLayerParams();
//                //}
//            }
//            //foreach (var item2 in list_CurrentPanelElementParams)
//            //{
//            //    //if (item2.dragGame_Child)
//            //    //{
//            //    //    item2.dragGame_Child.TurnMyCollider(true);
//            //    //}

//            //}
//        }


//        public void HidePanel()
//        {
//            deltaStep = deltaStepHide;
//            isMoveByCamAllowed = false;
//            //StartCoroutine(StaticParams.MoveToCorrectPlace(transform, targetPos, 2));
//            currentPanelState = PanelState.hide;
//            if (needChangeLayers)
//            {
//                ReturnDefault__OrderInLayer();
//                //foreach (var item in list_CurrentPanelElementParams)
//                //{
//                //    //item.dragGame_Child.UpdateDefaultLayerParams();
//                //}
//            }
//            //foreach (var item2 in list_CurrentPanelElementParams)
//            //{
//            //    //if (item2.dragGame_Child)
//            //    //{
//            //    //    item2.dragGame_Child.TurnMyCollider(false);
//            //    //}

//            //}
//        }

//        public static void HideAllPanels()
//        {
//            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
//            {
//                if (!item.isNeverHide)
//                {
//                    item.HidePanel();
//                }
//            }
//        }

//        public static void ShowAllPanels()
//        {
//            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
//            {

//                item.ShowPanel();

//            }
//        }



//        public static PanelAlign ShowPanel(string _name)
//        {
//            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
//            {
//                if (item.name == _name)
//                {

//                    item.ShowPanel();
//                    if (OnPanelShow != null)
//                    {
//                        OnPanelShow(item);
//                    }
//                    return item;
//                }
//            }
//            Debug.LogError("Панель с таким именем не найдена: " + _name);
//            return null;
//        }
//        public static void HidePanel(string _name)
//        {
//            foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
//            {
//                if (item.name == _name)
//                {

//                    item.HidePanel();
//                    break;
//                }
//            }
//        }
//        void OnBecameVisible()
//        {

//            Debug.Log("1111");
//        }
//        void LateUpdate()
//        {
//            if (transform.parent != null)
//            {
//                //if (isMoveByCamAllowed)
//                //{



//                //    switch (currentPanelType)
//                //    {
//                //        case PanelType.left:

//                //            correctPlace = Camera.main.ScreenToWorldPoint(new Vector3(0 + Screen.width * deltaStep, Screen.height * 0.5f, 10)); ;
//                //            transform.position = correctPlace;
//                //            targetPos.position = Camera.main.ScreenToWorldPoint(new Vector3(0 + Screen.width * deltaStep, Screen.height * 0.5f, 10));
//                //            break;
//                //        case PanelType.right:

//                //            correctPlace = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width - Screen.width * deltaStep, Screen.height * 0.5f, 10));
//                //            transform.position = correctPlace;
//                //            targetPos.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width + Screen.width * deltaStep, Screen.height * 0.5f, 10));

//                //            break;

//                //        case PanelType.top:
//                //            correctPlace = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height - Screen.height * deltaStep, 10));
//                //            transform.position = correctPlace;
//                //            targetPos.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f, Screen.height + Screen.height * deltaStep, 10));
//                //            break;

//                //        default:
//                //            break;
//                //    }
//                //}else
//                //{
//                if (isNeedScale)
//                {
//                    float size = Camera.main.orthographicSize / defaultCameraOrtoSize;
//                    transform.localScale = new Vector3(size, size, size);


//                }
//                switch (currentPanelState)
//                {
//                    case PanelState.hide:
//                        deltaStep = deltaStepHide;
//                        UpdateMove(targetPos, currentPanelType == PanelType.top ? -1 : -3);
//                        transform.position = Vector2.Lerp(transform.position, targetPos.position, Time.deltaTime * 4);
//                        if (Vector2.Distance(transform.position, targetPos.position) < 0.05f)
//                        {
//                            currentPanelState = PanelState.hiddenNow;
//                        }
//                        break;
//                    case PanelState.show:
//                        deltaStep = deltaStepShow;
//                        UpdateMove(targetPos, 1);
//                        if (Vector2.Distance(transform.position, targetPos.position) < 0.05f)
//                        {
//                            currentPanelState = PanelState.shownNow;
//                        }
//                        transform.position = Vector2.Lerp(transform.position, targetPos.position, Time.deltaTime * 3);

//                        break;
//                    case PanelState.shownNow:
//                        UpdateMove(targetPos, 1);
//                        transform.position = Vector2.Lerp(transform.position, targetPos.position, Time.deltaTime * 5);

//                        break;
//                    case PanelState.hiddenNow:
//                        UpdateMove(transform, currentPanelType == PanelType.top ? -1 : -3);

//                        break;
//                    default:
//                        break;
//                }


//                //}
//            }
//        }

//        void UpdateMove(Transform _objToMove, int _dir)
//        {
//            switch (currentPanelType)
//            {
//                case PanelType.left:
//                    _objToMove.position = Camera.main.ScreenToWorldPoint(new Vector3(0 + Screen.width * deltaStep * _dir, Screen.height * 0.5f * deltaStepOtherAxis, 10)); ;
//                    break;
//                case PanelType.right:
//                    _objToMove.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width - Screen.width * deltaStep * _dir, Screen.height * 0.5f * deltaStepOtherAxis, 10));
//                    break;

//                case PanelType.top:
//                    _objToMove.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.5f * deltaStepOtherAxis, Screen.height - Screen.height * deltaStep * _dir, 10));
//                    break;

//                default:
//                    break;
//            }
//        }

//        //public void ReturnAllElements()
//        //{
//        //    List<PanelElementParams> temp = new List<PanelElementParams>();
//        //    foreach (var item in list_CurrentPanelElementParams)
//        //    {

//        //    }
//        //}

//        public static void TurnColliders(string _name, bool _on)
//        {
//            //foreach (var item in GameObject.FindObjectsOfType<PanelAlign>())
//            //{
//            //    if (item.name == _name)
//            //    {
//            //        foreach (var item2 in item.list_CurrentPanelElementParams)
//            //        {
//            //            //item2.dragGame_Child.TurnMyCollider(_on);
//            //        }

//            //        break;
//            //    }
//            //}

//        }

//        public void ReturnDefault__OrderInLayer()
//        {
//            if (GetComponent<LayerWorker>()) return;
//            if (transform.parent) if (transform.parent.GetComponent<LayerWorker>()) return;


//            foreach (var item in list_orderParams)
//            {
//                item.currentSprite.sortingOrder = item.defaultOrderInLayer;
//            }
//        }

//        public void SetManual__OrderInLayer(int _orderInLayer)
//        {
//            foreach (var item in list_orderParams)
//            {
//                item.currentSprite.sortingOrder = _orderInLayer + item.defaultOrderInLayer;
//            }
//        }

//    }
//}
