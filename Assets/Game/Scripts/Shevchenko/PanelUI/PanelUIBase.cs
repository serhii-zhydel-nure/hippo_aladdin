﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PSV;

namespace Shevchenko
{
    public class PanelUIBase : MonoBehaviour
    {
        public delegate void Save();
        public static event Save OnSave;
        public static event Save OnAgain;
        public static event Save OnCancel;

        //public static PanelUIBase instance;
        public GameObject PanelObj;
        public Animator anim;

        public virtual void Awake()
        {
            //instance = this;
            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                //if (item.name.Contains("Obj"))
                //{
                //    PanelObj = item.gameObject;
                //    PanelObj.SetActive(false);
                //    anim = item.GetComponent<Animator>();
                //}
                //else
                if (item.name == "ButtonMenu")
                {
                    item.GetComponent<Button>().onClick.AddListener(()
    => StartCoroutine(OnButtonMenu()));

                }
                else if (item.name == "ButtonCancel")
                {
                    item.GetComponent<Button>().onClick.AddListener(()
    => StartCoroutine(OnButtonCancel()));

                }
                else if (item.name == "ButtonSave")
                {
                    item.GetComponent<Button>().onClick.AddListener(()
                        => StartCoroutine(OnButtonSave()));
                }
                else if (item.name == "ButtonAgain")
                {
                    item.GetComponent<Button>().onClick.AddListener(()
                        => StartCoroutine(OnButtonAgain()));
                }

            }
            InitPanelObj();
            AwakeOther();
        }
        public virtual void AwakeOther()
        {

        }
        public virtual void InitPanelObj()
        {

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name.Contains("Obj"))
                {
                    PanelObj = item.gameObject;
                    PanelObj.SetActive(false);
                    anim = item.GetComponent<Animator>();
                }
            }
        }

        public virtual void ShowHide(bool _show)
        {
            if (_show)
            {
                PanelObj.SetActive(true);
                StartCoroutine(IeDelayAppear());
            }
            else
            {
                StartCoroutine(IeDelay());
            }
        }
        public virtual IEnumerator IeDelayAppear()
        {
            yield return new WaitForEndOfFrame();
          if(anim)  anim.SetTrigger("Trigger");

        }
        public virtual IEnumerator IeDelay()
        {
            yield return new WaitForSeconds(.2f);
            PanelObj.SetActive(false);

        }
        public virtual IEnumerator OnButtonSave()
        {
            yield return new WaitForSeconds(.06f);
            ShowHide(false);
             if (OnSave != null)
            {
                OnSave();
            }

        }
        public virtual IEnumerator OnButtonAgain()
        {
            yield return new WaitForSeconds(.06f);
            ShowHide(false);
            if (OnAgain != null)
            {
                OnAgain();
            }

        }
        public virtual IEnumerator OnButtonCancel()
        {
            yield return new WaitForSeconds(.06f);
            ShowHide(false);
            if (OnCancel!=null)
            {
                OnCancel();
            }
        }
        public virtual IEnumerator OnButtonMenu()
        {
            yield return new WaitForSeconds(.06f);
                 SceneLoader.SwitchToScene(Scenes.MainMenu);
         }
    }
}
