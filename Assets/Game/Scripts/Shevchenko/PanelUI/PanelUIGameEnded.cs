﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class PanelUIGameEnded : PanelUIBase
    {
        public static PanelUIGameEnded instance;
        public override void AwakeOther()
        {
            instance = this;
        }
        public override void InitPanelObj()
        {
            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name.Contains("Obj"))
                {
                    PanelObj = item.gameObject;
                    PanelObj.SetActive(false);
                    anim = item.GetComponent<Animator>();
                }
            }
        }
    }
}