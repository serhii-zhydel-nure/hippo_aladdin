﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class PanelUISave : PanelUIBase
    {
        public static PanelUISave instance;
        public delegate void PanelShowHide(bool _enabled);
        public static event PanelShowHide OnPanelShowHide;

        //public static PanelUISave instance;
        public override void AwakeOther()
        {
            instance = this;
        }
        public override void InitPanelObj()
        {
            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name.Contains("Obj"))
                {
                    PanelObj = item.gameObject;
                    PanelObj.SetActive(false);
                    anim = item.GetComponent<Animator>();
                }
            }
        }

        public override void ShowHide(bool _show)
        {
            base.ShowHide(_show);
            
            if (OnPanelShowHide!= null)
            {
                OnPanelShowHide(_show);
            }
        }
    }
}
