﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelUICamera : MonoBehaviour {

 	void Start () {
        Canvas can = GetComponent<Canvas>();
        can.renderMode = RenderMode.ScreenSpaceCamera;
        can.worldCamera = Camera.main;
        can.sortingLayerName = "UI";
        can.sortingOrder = 1000;
	}
	
	 
}
