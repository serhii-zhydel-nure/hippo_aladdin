﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
    public class LayerController : MonoBehaviour
    {
        public static LayerController instance;
        public float updateIntencity = 0.5f;
        public List<LayerWorker> list_LayerElements = new List<LayerWorker>();
        public int layerStep = 1000;

        void Awake()
        {
            instance = this;
        }
        void Start()
        {
            foreach (var item in FindObjectsOfType<LayerWorker>())
            {
                list_LayerElements.Add(item);
            }
            StartCoroutine(LayerWork());
        }

        IEnumerator LayerWork()
        {
            yield return new WaitForEndOfFrame();
            ManualLayerRefresh();

            while (true)
            {
                 ManualLayerRefresh();
                yield return new WaitForSeconds(updateIntencity);

            }
        }

        public void RefreshAllLists_AddNewItems()
        {
             RemoveNulls();
            foreach (var item in list_LayerElements)
            {
                item.CheckForNewItems();
            }
            ManualLayerRefresh();
        }
        public void AddNewJustInstWorker(LayerWorker _lw)
        {
            if (!list_LayerElements.Contains(_lw))
            {
                list_LayerElements.Add(_lw);
                ManualLayerRefresh();

            }
        }
        public void AddNewJustInstWorker(GameObject _go)
        {
            if (!_go.GetComponent<LayerWorker>())
            {
                Debug.Log("НЕТ ВОРКЕРА");
                return;
            }
            list_LayerElements.Add(_go.GetComponent<LayerWorker>());
            ManualLayerRefresh();
        }
        void RemoveNulls()
        {
            List<LayerWorker> temp = new List<LayerWorker>();

             foreach (var item in list_LayerElements)
            {
                if (item != null)
                {
                    temp.Add(item);
                 }
            }
            list_LayerElements = new List<LayerWorker>(temp);
        }
        int curDeltaCounter = 0;

        public void ManualLayerRefresh()
        {
            RemoveNulls();
            //list_LayerElements = new List<LayerWorker>( list_LayerElements.ClearNulls());
            List<LayerWorker> tempLower = new List<LayerWorker>();
            List<LayerWorker> tempUpper = new List<LayerWorker>();

            list_LayerElements.Sort(delegate (LayerWorker us1, LayerWorker us2)
            { return us2.transform.position.y.CompareTo(us1.transform.position.y); });

            foreach (var item in list_LayerElements)
            {
                item.iAmChild = false;
                item.list_MyParents.Clear();
                item.list_MyChilds.Clear();

            }
            foreach (var item in list_LayerElements)
            {
                foreach (var item2 in item.GetComponentsInChildren<LayerWorker>())
                {
                    if (item2 != item)
                    {
                        item.list_MyChilds.Add(item2);
                        item2.iAmChild = true;

                        item2.list_MyParents.Add(item);

                    }
                }
            }
            for (int i = 0; i < list_LayerElements.Count; i++)
            {
                if (!list_LayerElements[i])
                {
                    continue;
                }

                if (!list_LayerElements[i].ignore && !list_LayerElements[i].lower && !list_LayerElements[i].upper
                    && !list_LayerElements[i].iAmChild)
                {
                    list_LayerElements[i].SetTemp__OrderInLayer(curDeltaCounter);
                    curDeltaCounter += layerStep;
                    foreach (var item in list_LayerElements[i].list_MyChilds)
                    {
                        item.SetTemp__OrderInLayer(curDeltaCounter);
                        curDeltaCounter += layerStep;
                    }


                }
                else if (list_LayerElements[i].lower && !list_LayerElements[i].ignore && !list_LayerElements[i].upper)
                {
                    tempLower.Add(list_LayerElements[i]);
                }
                else if (!list_LayerElements[i].lower && !list_LayerElements[i].ignore && list_LayerElements[i].upper)
                {
                    tempUpper.Add(list_LayerElements[i]);
                }
                else
                {
                    list_LayerElements[i].ReturnDefault__OrderInLayer();
                }
            }

            //Debug.Log("tempLower "+ tempLower.Count);
            tempLower.Sort(delegate (LayerWorker us1, LayerWorker us2)
            { return us1.lowerUpperPosition.CompareTo(us2.lowerUpperPosition); });

            for (int i = 0; i < tempLower.Count; i++)
            {
                tempLower[i].SetTemp__OrderInLayer(i + 1 * -layerStep);
            }


            tempUpper.Sort(delegate (LayerWorker us1, LayerWorker us2)
            { return us1.lowerUpperPosition.CompareTo(us2.lowerUpperPosition); });

            for (int i = 0; i < tempUpper.Count; i++)
            {

                tempUpper[i].SetTemp__OrderInLayer((list_LayerElements.Count + (i + 1)) * layerStep);
            }
            curDeltaCounter = 0;

        }
    }
}
