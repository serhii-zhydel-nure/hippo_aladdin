﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Shevchenko
{
    public class SoundEffectsController : MonoBehaviour
    {
        public static List<AudioClip> list_sound_effects = new List<AudioClip>();
        public static SoundTrack soundTrackNOW;
        public static GameObject MusicGO;

        void OnEnable()
        {
            Languages.OnLangChange += Languages_OnLangChange; ;
        }
        void OnDisable()
        {
            Languages.OnLangChange -= Languages_OnLangChange;
        }
        private void Languages_OnLangChange(Languages.Language lang)
        {
            ClearSoundList();
            SwitchLanguage(lang.ToString());
            SaveLanguage(lang);
        }

        static bool isResourcesLoaded;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                StopAll();
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                Debug.Log("PlayerPrefs.DeleteAll");
                PlayerPrefs.DeleteAll();
            }
            //if (Input.GetKeyDown(KeyCode.A))
            //{
            //    if (RateMePlugin.RateMeModule.Instance.CanShow(Time.time))
            //    {
            //        RateMeSceneController.nextSceneName = "Scene7";
            //        StartCoroutine(StaticParams.AutoFade("RateMe", false));
            //    }
            //}
        }
        //void OnGUI()
        //{
        //    if (GUI.Button(new Rect(10, Screen.height * 0.25f, Screen.height * 0.12f, Screen.height * 0.12f), "Skip"))
        //    {
        //        StopAll();
        //    }
        //}
        void Start()
        {


            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            if (!isResourcesLoaded)
            {
                isResourcesLoaded = true;
                //GameObject go = new GameObject();
                //go.AddComponent<SoundMusicController>();
                //go.AddComponent<AudioSource>();
                //go.GetComponent<AudioSource>().volume = 0.2f;
                //DontDestroyOnLoad(go);
                //go.name = "Music";
                //MusicGO = go;

#if UNITY_ANDROID && !UNITY_EDITOR
        AndroidJavaClass localeClass = new AndroidJavaClass("java/util/Locale");
        AndroidJavaObject defaultLocale = localeClass.CallStatic<AndroidJavaObject>("getDefault");
        AndroidJavaObject usLocale = localeClass.GetStatic<AndroidJavaObject>("US");
        string systemLanguage = defaultLocale.Call<string>("getDisplayLanguage", usLocale);
#else
                string systemLanguage = Application.systemLanguage.ToString();
#endif
                //string systemLanguage = Application.systemLanguage.ToString();


                if (!PlayerPrefs.HasKey("languageManual"))
                {
                    if (list_sound_effects.Count == 0)
                    {

                        SwitchLanguage(systemLanguage);
                    }
                }
                else
                {
                    SwitchLanguage(PlayerPrefs.GetString("languageManual"));
                }


            }
        }

        //static void SetImage()
        //{
        //    Languages.Language lg = SoundEffectsController.GetCurrentLanguage();
        //    //if (LangButton.Instance)
        //    //{
        //    //    LangButton.Instance.SetSprite(lg);
        //    //}


        //}
        public static void SwitchLanguage(string _language)
        {
            ////////////////////////////////////////////////////////////////////////////////////////////
            //_language = "English";
            ///////////////////////////////////////////////////////////////////////////////////////////

            switch (_language)
            {
                ////               Russian
                case "Russian":
                    LoadFromResources(SystemLanguage.Russian.ToString());
                    break;
                case "Ukrainian":
                    LoadFromResources(SystemLanguage.Russian.ToString());
                    break;
                case "Belarusian":
                    LoadFromResources(SystemLanguage.Russian.ToString());
                    break;
                case "Kazakh":
                    LoadFromResources(SystemLanguage.Russian.ToString());
                    break;
                case "Turkmen":
                    LoadFromResources(SystemLanguage.Russian.ToString());
                    break;
                case "Kirgisian":
                    LoadFromResources(SystemLanguage.Russian.ToString());
                    break;
                case "Karakalpak":
                    LoadFromResources(SystemLanguage.Russian.ToString());
                    break;
                case "Uzbek":
                    LoadFromResources(SystemLanguage.Russian.ToString());
                    break;



                case "English":
                    LoadFromResources(SystemLanguage.English.ToString());
                    break;

                /////                Spanish
                case "Spanish":
                    LoadFromResources(SystemLanguage.Spanish.ToString());
                    break;
                case "Catalan":
                    LoadFromResources(SystemLanguage.Spanish.ToString());
                    break;


                case "Portuguese":
                    LoadFromResources(SystemLanguage.Portuguese.ToString());
                    //CreateLoadObject();
                    break;

                case "French":
                    LoadFromResources(SystemLanguage.French.ToString());
                    //CreateLoadObject();
                    break;


                //Polish
                case "Polish":
                    LoadFromResources(SystemLanguage.Polish.ToString());
                    //CreateLoadObject();
                    break;
                /////                German
                case "German":
                    LoadFromResources(SystemLanguage.German.ToString());
                    //CreateLoadObject();
                    break;
                case "Arabic":
                    LoadFromResources(SystemLanguage.Arabic.ToString());
                    //CreateLoadObject();
                    break;
                case "Italian":
                    LoadFromResources(SystemLanguage.Italian.ToString());
                    //CreateLoadObject();
                    break;

                case "Chinese":
                    LoadFromResources(SystemLanguage.Chinese.ToString());
                    //CreateLoadObject();
                    break;

                //case "Malay":
                //    LoadFromResources(SystemLanguage.Indonesian.ToString());
                //    //CreateLoadObject();
                //    break;


                default:
                    LoadFromResources(SystemLanguage.English.ToString());
                    break;
            }
        }

        public static void ClearSoundList()
        {
            //foreach (var item in list_sound_effects)
            //{
            //    Resources.UnloadAsset(item);
            //}
            list_sound_effects.Clear();
            Resources.UnloadUnusedAssets();
        }
        public static void LoadFromResources(string lg)
        {
            Debug.Log("LoadFromResources(string lg)  " + lg);


            foreach (var item in Resources.LoadAll<AudioClip>("Sounds/MultiLanguage/" + lg))
            {
                if (item.name.Contains("music"))
                    continue;
                list_sound_effects.Add(item);
            }
            // load other sounds
            foreach (var item in Resources.LoadAll<AudioClip>("Sounds/NonLanguage"))
            {

                list_sound_effects.Add(item);
            }
            //SetImage();
        }

        public static void SaveLanguage(Languages.Language _lang)
        {
            PlayerPrefs.SetString("languageManual", _lang.ToString());
        }
        //    public static Languages.Language GetCurrentLanguage()
        //    {
        //        string lang;

        //        if (PlayerPrefs.HasKey("languageManual"))
        //        {
        //            lang = PlayerPrefs.GetString("languageManual");
        //        }
        //        else
        //        {
        //#if UNITY_ANDROID && !UNITY_EDITOR
        //        AndroidJavaClass localeClass = new AndroidJavaClass("java/util/Locale");
        //        AndroidJavaObject defaultLocale = localeClass.CallStatic<AndroidJavaObject>("getDefault");
        //        AndroidJavaObject usLocale = localeClass.GetStatic<AndroidJavaObject>("US");
        //        lang = defaultLocale.Call<string>("getDisplayLanguage", usLocale);
        //#else
        //            lang = Application.systemLanguage.ToString();
        //#endif

        //        }

        //        switch (lang)
        //        {
        //            ////               Russian
        //            case "Russian": return Languages.Language.Russian;
        //            case "Ukrainian": return Languages.Language.Russian;
        //            case "Belarusian": return Languages.Language.Russian;
        //            case "Kazakh": return Languages.Language.Russian;
        //            case "Turkmen": return Languages.Language.Russian;
        //            case "Kirgisian": return Languages.Language.Russian;
        //            case "Karakalpak": return Languages.Language.Russian;
        //            case "Uzbek": return Languages.Language.Russian;



        //            case "English": return Languages.Language.English;

        //            /////                Spanish
        //            case "Spanish": return Languages.Language.Spanish;
        //            case "Catalan": return Languages.Language.Spanish;


        //            case "Portuguese": return Languages.Language.Portuguese;

        //            /////                French

        //            case "French": return Languages.Language.French;
        //            ///////                Indonesian
        //            //case "Indonesian":return Languages. Language.Indonesian;

        //            //case "Malay":return Languages. Language.Indonesian;


        //            case "Polish": return Languages.Language.Polish;

        //            default: return Languages.Language.English;

        //        }
        //    }
        static void CreateLoadObject()
        {
            GameObject goLoadObj = new GameObject();
            goLoadObj.name = "Loader Object";
            goLoadObj.AddComponent<AssetBundleLoader>();
            DontDestroyOnLoad(goLoadObj);
        }

        /*
        public static void Play_ByType(AuType _auT)
        {
            AudioClip clip;
            switch (_auT)
            {
                //case AuType.life:
                //    clip = SelectSingle(_auT.ToString());
                //    break;
                //case AuType.hits:
                //    clip = SelectRandom(_auT.ToString());
                //    break;



                default:
                    clip = SelectSingle(_auT.ToString());
                    break;
            }

            if (  clip)
                AudioSource.PlayClipAtPoint(clip, Vector2.zero, 0.5f);
        }

        public static void Play_Random_By_Name(string _name)
        {
            AudioClip clip;

            clip = SelectRandom(_name);

            if (  clip)
                AudioSource.PlayClipAtPoint(clip, Vector2.zero, 0.5f);
        }

        public static void Play_By_Name(string _name)
        {

            AudioClip clip;

            clip = SelectSingle(_name);

            foreach (var item in list_sound_effects)
            {
                if (item.name.Contains(_name))
                {
                    clip = item;
                }
            }

            if (  clip)
                AudioSource.PlayClipAtPoint(clip, Vector2.zero, 0.5f);

        }
          */
        public static List<SoundTrack> list_PlayQueue_SoundTrack = new List<SoundTrack>();
        public static List<AudioClip> list_PlayQueue = new List<AudioClip>();

        public static int Play_By_Name_Delayed(string _name)
        {

            AudioClip clip;

            clip = SelectSingle(_name);

            foreach (var item in list_sound_effects)
            {
                if (item.name.Contains(_name))
                {
                    clip = item;
                }
            }

            if (clip)
            {
                //AudioSource.PlayClipAtPoint(clip, Vector2.zero, 0.5f);

                if (list_PlayQueue.Count == 0)
                {
                    StartNextSound(0, clip);

                }
                list_PlayQueue.Add(clip);

            }
            if (clip)
                return clip.GetHashCode();
            else return 0;
        }
 
        public static int StopAnd_Play_By_Name_Once(string _name)
        {
            StopAll();

            AudioClip clip;

            clip = SelectSingle(_name);

            //foreach (var item in list_sound_effects)
            //{
            //    if (item.name.Contains(_name))
            //    {
            //        clip = item;
            //    }
            //}

            if (clip)
            {
                //AudioSource.PlayClipAtPoint(clip, Vector2.zero, 0.5f);

                //if (list_PlayQueue.Count == 0)
                //{
                StartNextSound(0, clip);

                //}
                //list_PlayQueue.Add(clip);

            }
            if (clip)
                return clip.GetHashCode();
            else return 0;

            //return Play_By_Name_Delayed(_name);
        }
 
        public static int StopAnd_Play_By_Name_Random(string[] _name )
        {
            StopAll();

            string nm = _name[Random.Range(0, _name.Length)];
            return Play_By_Name_Delayed(nm );
        }
        //public static int Play_By_Name_Random(string[] _name, bool isStrongNameEqual)
        //{

        //    string nm = _name[Random.Range(0, _name.Length)];
        //    return Play_By_Name_Delayed(nm, true);
        //}
        public static void StopAll()
        {
            //if (list_PlayQueue_SoundTrack.Count > 0)
            //{
            //    list_PlayQueue_SoundTrack[0].AudioSrc.Stop();
            //}
            foreach (var item in list_PlayQueue_SoundTrack)
            {
                item.AudioSrc.Stop();
            }
            list_PlayQueue_SoundTrack.Clear();
            list_PlayQueue.Clear();
        }

        static void StartNextSound(float timePosition, AudioClip _clip)
        {
            byte volume;
            if (!StaticParams.isSound) volume = 0; else volume = 1;

            SoundTrack track = SoundTrack.PlaySound(_clip, volume);
            list_PlayQueue_SoundTrack.Add(track);
            soundTrackNOW = track;

            // событие начала звука
            track.start_action += soundStartEvent;

            //// событие срабатывает каждый кадр
            //track.processing += soundProcessEvent;

            // это событие подходит только для зацикленного воспроизведения
            // оно срабатывает каждый раз, когда звук завершается
            track.complete_action += soundCompleteEvent;

            // событие срабатывает при завершении воспроизведения звука
            track.destroy_action += soundDestroyEvent;


            if (timePosition > 0)
            {
                // перематывает точку воспроизведения на определённый момент, в секундах
                track.setTimePosition(timePosition);
            }
        }

        static void soundStartEvent(SoundTrack track)
        {
            //Debug.Log("Sound Start event!");

        }

        static void soundProcessEvent(SoundTrack track)
        {
            //track.volume = track.playing_time % 1f;
        }

        static void soundCompleteEvent(SoundTrack track, float offset)
        {
            //Debug.Log("Sound Complete event! " + offset);
        }

        static void soundDestroyEvent(SoundTrack track, bool atEndOfSound, float offset, AudioClip _clip)
        {
            //Debug.Log("Sound Destroy event! " + offset);

            // проверяет было ли уничтожение инициировано из за окончания воспроизведения
            // atEndOfSound будет равен false в случае если воспроизведения было прервано по другим причинам
            // к примеру при удалении звука со сцены вручную и т.д.
            if (atEndOfSound)
            {
                // данный подход демонстрирует как можно плавно соединить несколько последовательных звуков
                // offset - это неизбежная задержка вызова события 
                // можно её компенсировать используя track.setTimePosition для следующего звука

                // это сделано чтобы звуки в редакторе не воспроизводились бесконечно
                // так как если звук очень короткий, это будет весьма неприятный тест
            }
            list_PlayQueue.Remove(_clip);
            list_PlayQueue_SoundTrack.Remove(track);

            //if (list_PlayQueue.Count > 0)
            //{
            //    Debug.Log(list_PlayQueue.Count);
            //    StartNextSound(0, list_PlayQueue[0]);
            //}
        }

        public static bool IsThisClipEnded(int _hashForCheck)
        {
            if (!soundTrackNOW)
                return false;

            if (soundTrackNOW.AudioSrc.clip.GetHashCode().Equals(_hashForCheck))
            {
                return true;
            }
            else
                return false;
        }

        //public static bool IsQueueAlreadyContainsClip(string _clipName)
        //{
        //    if (list.)
        //    {

        //    }
        //}

        private static AudioClip SelectRandom(string _name)
        {
            List<AudioClip> list_temp = new List<AudioClip>();
            foreach (var item in list_sound_effects)
            {
                if (item.name.Contains(_name))
                {
                    list_temp.Add(item);
                }
            }
            return list_temp[Random.Range(0, list_temp.Count)];
        }

        public static AudioClip SelectSingle(string _name)
        {
            foreach (var item in list_sound_effects)
            {
                if (item.name.Contains(_name))
                {
                    return item;
                }
            }
            return null;
        }
    }
}