﻿using UnityEngine;
using System.Collections;
using PSV;

namespace Shevchenko
{
    public class FirstCutScene : GameController
    {



        public override IEnumerator Process()
        {
            pepa.SetStandAndLookUp(true);
            george.SetStandAndLookUp(true);
            yield return new WaitForSeconds(2);
            pepa.SetStandAndLookUp(false);
            //SFX.Play("ep-hp-13");
            yield return new WaitWhile(() => pepa.Talk("ep-hp-13"));

            //Однажды, мы решили узнать, какие тайны хранит пыльный чердак.		hp-2
            yield return new WaitWhile(() => pepa.Talk("hp-2"));
            george.SetStandAndLookUp(false);
            yield return new WaitForSeconds(.2f);

            george.Walk("GeorgePos (1)");
            yield return new WaitWhile(() => george.IsWalkingNow());
            SFX.Play("Джи смеется 2");
            george.SetTriggerEmotion(EmotionTriggerType.Joy);
            yield return new WaitForSeconds(1f);

            george.SetClimbeOnChair();
            yield return new WaitForSeconds(2.5f);
            george.SetHighJump();
            yield return new WaitForSeconds(.7f);

            SFX.Play("Открывается чердак");
             
            FirstCutScene_Trap.instance.SetAction();

            yield return new WaitForSeconds(.7f);

            george.SetClimbeBackFromChair();
            yield return new WaitForSeconds(2.3f);
            george.Walk("GeorgePos (2)");
            pepa.Walk("PepaPos (3)");
            yield return new WaitForSeconds(2f);
             
                SceneLoader.SwitchToScene(Scenes._1Attic_Wiping);
         }
    }
}
