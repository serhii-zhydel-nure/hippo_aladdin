﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class FirstCutScene_Trap : MonoBehaviour {

        public static FirstCutScene_Trap instance;
        Animator anim;

        private void Awake()
        {
            instance = this;
            anim = GetComponent<Animator>();
        }

        public void SetAction()
        {
            anim.SetTrigger("Action");
            StartCoroutine(ActionSound());
        }

        IEnumerator ActionSound()
        {
            yield return new WaitForSeconds(3);
            SFX.Play("Лестница ударилась об пол с чердака");

        }
    }
}
