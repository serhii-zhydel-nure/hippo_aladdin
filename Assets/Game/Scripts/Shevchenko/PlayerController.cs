﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
    public enum PlayerType
    {
        pEpa, george, mama, pApa, grandMa, other, nonHippoCharacter
    }
    public enum WalkAnimType
    {
        Walk, SpecRun, Run, BagWalk
    }
    public enum EmotionTriggerType
    {
        ScaredOnce =8, Joy = 7, JoyFootOut = 11
    }
    public enum EmotionBoolType
    {
        Sad
    }
    public partial class PlayerController : MonoBehaviour
    {
        public delegate void TriggerHit(PlayerController _player, Collider2D _coll);
        public static event TriggerHit OnTriggerHit;

        public delegate void OnAnimationEvent();
        public static event OnAnimationEvent OnAnimationEnded;

        class WalkQueue
        {
            public Vector2 targetPosition;
             public string pointName;
        }

        public PlayerType playerType;
        public Vector3 defaultPlayerSize;
        public float walkSpeed = 4.5f;

        List<WalkQueue> list_WalkQueue = new List<WalkQueue>();
        public Animator animPlayer;

         WalkAnimType currentWalkAnimType = WalkAnimType.Walk;

        public bool isFakeWalk;
        List<string> list_TalkQueue = new List<string>();


        void Awake()
        {
             defaultPlayerSize = transform.localScale;
            animPlayer = GetComponent<Animator>();
            if (playerType == PlayerType.other)
            {
                animPlayer.speed = Random.RandomRange(0.6f, 1.5f);
                StartCoroutine(ChangeAnimRandomSpeedOtherChars());
            }
            animPlayer.SetFloat("RandomOffset", Random.Range(0f, 1f));

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "pupil_R")
                {
                    pupil_R = item;
                }
            }
        }
        private void OnEnable()
        {
            Talker.OnTalkFinished += Talker_OnTalkFinished;
            Talker.OnTalkStarted += Talker_OnTalkStarted;
        }

        private void OnDisable()
        {
            Talker.OnTalkFinished -= Talker_OnTalkFinished;
            Talker.OnTalkStarted -= Talker_OnTalkStarted;

        }
        private void Talker_OnTalkStarted(Talker.TalkParams _tp)
        {
            foreach (var item in list_TalkQueue.ToArray())
            {
                if (item == _tp.targetName)
                {
                    amITalkingSomethingNow = true;
                    animPlayer.SetBool("IdleTalking", true);
                }
            }
        }


        private void Talker_OnTalkFinished(Talker.TalkParams _tp)
        {
            foreach (var item in list_TalkQueue)
            {
                if (item == _tp.targetName)
                {
                    //Debug.Log("Talker_OnTalkFinished "+ _tp.targetName);
                    amITalkingSomethingNow = false;
                    animPlayer.SetBool("IdleTalking", false);
                    list_TalkQueue.Remove(item);
                    break;
                }
            }

        }
        IEnumerator ChangeAnimRandomSpeedOtherChars()
        {
            while (true)
            {
                animPlayer.speed = Random.RandomRange(0.6f, 1.5f);
                yield return new WaitForSeconds(Random.RandomRange(3, 6));
            }
        }
        void Update()
        {
            if (walkNow && !isFakeWalk)
            {
                animPlayer.SetBool(currentWalkAnimType.ToString(), true);
                transform.position = Vector2.MoveTowards(transform.position,
                    new Vector2(targetPosition.x, targetPosition.y), Time.deltaTime * walkSpeed);
                transform.localScale = Vector2.MoveTowards(transform.localScale, targetPlayerSize, Time.deltaTime / distanseForScale);

                if (Vector2.Distance(transform.position, targetPosition) < 0.01f)
                {
                    if (list_WalkQueue.Count > 0)
                    {
                        targetPosition = list_WalkQueue[0].targetPosition;
                        AutoSetDirection(targetPosition);
                        list_WalkQueue.RemoveAt(0);
                    }
                    else
                    {
                        walkNow = false;
                        animPlayer.SetBool(currentWalkAnimType.ToString(), false);
                    }
                }
            }
            if (lookAtTarget)
            {
                Vector3 dir = lookAtTarget.position - pupil_R.position;
                animPlayer.SetFloat("EyeBackToFront", transform.localRotation.eulerAngles.y == 0 ? dir.x : dir.x * -1);
                animPlayer.SetFloat("EyeDownToUp", dir.y);

            }
        }
        #region WALK_METHODS
        int wayHash;
        bool walkNow;
        Vector2 targetPosition, targetPlayerSize;
        float distanseForScale;

        public void Walk(string _pointName)
        {
            targetPlayerSize = defaultPlayerSize;
            targetPosition = GameObject.Find(_pointName).transform.position;
            AutoSetDirection(targetPosition);
            list_WalkQueue.Clear();

            walkNow = true;
        }

        public void Walk(string _pointName, float _targetScale)
        {

            targetPlayerSize = new Vector2(defaultPlayerSize.x * _targetScale, defaultPlayerSize.y * _targetScale);
            //Debug.Log(targetPlayerSize.x + " " + defaultPlayerSize.x);
            targetPosition = GameObject.Find(_pointName).transform.position;
            distanseForScale = Vector2.Distance(transform.position, targetPosition);

            AutoSetDirection(targetPosition);
            list_WalkQueue.Clear();

            walkNow = true;
        }
        public void Walk(Vector2 _targetPos)
        {
            targetPlayerSize = defaultPlayerSize;
            targetPosition = _targetPos;
            AutoSetDirection(targetPosition);
            list_WalkQueue.Clear();

            walkNow = true;
        }

        public void Walk(float _Xdistance)
        {
            targetPlayerSize = defaultPlayerSize;
            targetPosition = new Vector2(transform.position.x + _Xdistance, transform.position.y);
            AutoSetDirection(targetPosition);
            list_WalkQueue.Clear();

            walkNow = true;
        }

        public void Walk(float _Xdistance, float _Ydistance)
        {
            targetPlayerSize = defaultPlayerSize;
            targetPosition = new Vector2(transform.position.x + _Xdistance, transform.position.y + _Ydistance);
            AutoSetDirection(targetPosition);
            list_WalkQueue.Clear();

            walkNow = true;
        }
        public void WalkAddQueue(string _pointName)
        {
            WalkQueue wq = new WalkQueue();
            Transform tr = GameObject.Find(_pointName).transform;
            wq.targetPosition = tr.position;
            wq.pointName = tr.name;
            list_WalkQueue.Add(wq);

        }
        public void WalkAddQueue(Transform _pointTr)
        {
            WalkQueue wq = new WalkQueue();
            //Transform tr = GameObject.Find(_pointName).transform;
            wq.targetPosition = _pointTr.position;
            wq.pointName = _pointTr.name;
            list_WalkQueue.Add(wq);

        }

        public void WalkStop(WalkDirection _direction)
        {
            if (_direction == WalkDirection.left) transform.rotation = Quaternion.Euler(0, 180, 0);
            else if (_direction == WalkDirection.right) transform.rotation = Quaternion.Euler(0, 0, 0);
            else if (_direction == WalkDirection.inverse)
            {
                if (transform.rotation.eulerAngles.y == 0)
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                else transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            walkNow = false;
            animPlayer.SetBool(currentWalkAnimType.ToString(), false);
        }

        public bool IsWalkingNow()
        {
            return walkNow;
        }

        public void WalkSetWalkAnimType(WalkAnimType _wat)
        {
            animPlayer.SetBool(currentWalkAnimType.ToString(), false);

            currentWalkAnimType = _wat;
        }
        void AutoSetDirection(Vector2 _targetPosition)
        {
            if (transform.position.x < _targetPosition.x)
            {
                // right
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }
        public bool CheckAnim(string _animNameCompare)
        {
            //Debug.Log("fullPathHash " + animPlayer.GetCurrentAnimatorStateInfo(0).fullPathHash
            //    + "nameHash   " + animPlayer.GetCurrentAnimatorStateInfo(0).nameHash
            //   + "   shortNameHash  " + animPlayer.GetCurrentAnimatorStateInfo(0).shortNameHash
            //     + "   _animNameCompare  " + Animator.StringToHash(_animNameCompare)
            //                  + "   IsInTransition  " + animPlayer.IsInTransition(0)

            //                  + "  " + _animNameCompare

            //    );

            //return animPlayer.GetCurrentAnimatorStateInfo(0).IsName(_animNameCompare);
            if (animPlayer.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animPlayer.IsInTransition(0) && animPlayer.GetCurrentAnimatorStateInfo(0).shortNameHash == Animator.StringToHash(_animNameCompare))
            {
                return true;
            }
            else return false;
        }

        #endregion

        #region TalkMethods
        int soundHash;

        public bool amITalkingSomethingNow;

        public bool Talk(string _clipName)
        {
            if (soundHash == 0)
            {

                SoundEffectsController.StopAll();
                soundHash = SoundEffectsController.StopAnd_Play_By_Name_Once(_clipName);
                //list_mySoundQueue.Add(_clipName);
                amITalkingSomethingNow = true;
            }
            if (SoundEffectsController.IsThisClipEnded(soundHash))
            {

                animPlayer.SetBool("IdleTalking", true);
                return true;
            }
            else
            {
                amITalkingSomethingNow = false;

                animPlayer.SetBool("IdleTalking", false);
                soundHash = 0;
                //list_mySoundQueue.Remove(_clipName);
                return false;
            }

        }
        public bool Talk(string[] _clipName)
        {
            if (soundHash == 0)
                soundHash = SoundEffectsController.StopAnd_Play_By_Name_Random(_clipName);
            //else
            //    SoundEffectsController.Play_By_Name_Random(_clipName, isStrongNameEqual);

            if (SoundEffectsController.IsThisClipEnded(soundHash))
            {
                animPlayer.SetBool("IdleTalking", true);
                return true;
            }
            else
            {

                animPlayer.SetBool("IdleTalking", false);
                soundHash = 0;
                return false;
            }

        }
        public bool IsTalkingNow()
        {
            return soundHash == 0 ? false : true;
        }
        public void StopTalking()
        {
            SoundEffectsController.StopAll();
            animPlayer.SetBool("IdleTalking", false);
            soundHash = 0;

            amITalkingSomethingNow = false;
            if (Talker.instance)
            {
                Talker.instance.StopTalkingAndClearQueue();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_clipName"></param>
        /// <param name="_checkIsTalkingNow">Если сейчас базарит, то фразу не говорить</param>
        public void Say(string _clipName, bool _checkIsTalkingNow = false)
        {
            if (!IsTalkingNow())
            {
                StartCoroutine(StaticParams.Say(this, _clipName));
            }
        }        /// <summary>
                 /// 
                 /// </summary>
                 /// <param name="_clipName"></param>
                 /// <param name="_checkIsTalkingNow">Если сейчас базарит, то фразу не говорить</param>
        public void Say(string[] _clipName, bool _checkIsTalkingNow = false)
        {
            if (!IsTalkingNow())
            {
                StartCoroutine(StaticParams.Say(this, _clipName));
            }
        }

        public void TalkQueue(string _clipName, bool _checkIsPhraseUsed = false)
        {
            list_TalkQueue.Add(_clipName);
            Talker.instance.TalkQueue(_clipName, _checkIsPhraseUsed);
        }
        string lastPhrase;

        public void TalkQueue(string[] _clipNames, bool _checkIsPhraseUsed = false)
        {
            List<string> temp = new List<string>(_clipNames);
            temp = temp.Shuffle();
            if (lastPhrase != temp[0])
                lastPhrase = temp[0];
            else lastPhrase = temp[1];

            list_TalkQueue.Add(lastPhrase);
            Talker.instance.TalkQueue(lastPhrase, _checkIsPhraseUsed);
        }
        #endregion

        public void Sit(bool _sit)
        {
            animPlayer.SetBool("sitIdle", _sit);
        }

        public void CallAnimEnded()
        {
            OnAnimationEnded();
        }

        public void SimulateIdleTalking(bool _isTalk)
        {
            animPlayer.SetBool("IdleTalking", _isTalk);
        }
        public void SetTriggerEmotion(EmotionTriggerType _emotion)
        {
             animPlayer.SetInteger("ActionType", (int)_emotion);
             animPlayer.SetTrigger("Action");

             //animPlayer.SetTrigger(_emotion.ToString());
        }

        public void SetBoolEmotion(EmotionBoolType _emotion, bool _isEnable)
        {
            animPlayer.SetBool(_emotion.ToString(), _isEnable);

        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (OnTriggerHit != null)
            { OnTriggerHit(this, collision); }
        }
        public void SpadeWork()
        {
            animPlayer.SetInteger("ActionType", 12);
            animPlayer.SetTrigger("Action");
        }
        public void SetDudka(bool _enable)
        {
            animPlayer.SetBool("DudkaPlay", _enable);
        }
        public void SetDance(bool _enable)
        {
            animPlayer.SetBool("Dance_" + playerType.ToString(), _enable);

        }
        public void SetReverance()
        {
            //playerAnim.Play("reverance");
            animPlayer.SetInteger("ActionType", 3);
            animPlayer.SetTrigger("Action");

        }
        public void SetApplause()
        {
            animPlayer.Play("applause_" + Random.Range(0, 2));

        }
        public void SetScare()
        {
            animPlayer.Play("Scare_" + Random.Range(0, 2));

        }
        public void SetKoverSit()
        {
            animPlayer.Play("Idle_sit_kover_2 0");
        }
        public void SetKoverHit()
        {
            animPlayer.SetTrigger("TriggerKoverHit");
        }
        public void SetRastopirka()
        {
            //playerAnim.Play("rastopyrka_0");

            //playerAnim.SetTrigger("TriggerRastopirka");
            animPlayer.SetInteger("ActionType", 4);
            animPlayer.SetTrigger("Action");

        }
        public void SetInBox(bool _enable)
        {
            //playerAnim.SetBool("inBox", _enable);
            animPlayer.SetInteger("ActionType", 9);
            animPlayer.SetBool("bAction", _enable);

        }
        public void SetSleep(bool _enable, bool immediate = false)
        {
            animPlayer.SetBool("sleep", _enable);
            if (immediate)
            {
                animPlayer.Play("sleep_idle");
             }
        }
         public void SetAnimSpeedMultiplyer(float _speed)
        {
            animPlayer.SetFloat("animSpeed", _speed);

        }
        public void SetFakeTalkAnim(bool _talk)
        {
            animPlayer.SetBool("IdleTalking", _talk);

        }
        public void SetPush(bool _Push)
        {
            animPlayer.SetBool("Push", _Push);


        }
        public void SetFakeWalk(bool _walk)
        {
            isFakeWalk = _walk;
            animPlayer.SetBool(currentWalkAnimType.ToString(), _walk);

        }
        public void SetClimbeOnChair()
        {
            animPlayer.SetInteger("ActionType", 1);
            animPlayer.SetTrigger("Action");
            StartCoroutine(ClimbeOnChair());

        }
        IEnumerator ClimbeOnChair()
        {
            yield return new WaitForSeconds(.5f);
            Vector2 _to = new Vector2(transform.position.x + 1, transform.position.y + 2);
            do
            {
                transform.position = Vector2.MoveTowards(transform.position, _to, Time.deltaTime * 2.5f);
                yield return new WaitForEndOfFrame();
            } while (Vector2.Distance(transform.position, _to) != 0);
        }
        public void SetClimbeBackFromChair()
        {
            animPlayer.SetInteger("ActionType", 1);
            animPlayer.SetTrigger("Action");
            StartCoroutine(ClimbeBackFromChair());

        }
        IEnumerator ClimbeBackFromChair()
        {
            yield return new WaitForSeconds(.5f);
            Vector2 _to = new Vector2(transform.position.x - 1, transform.position.y - 2);
            do
            {
                transform.position = Vector2.MoveTowards(transform.position, _to, Time.deltaTime * 2.5f);
                yield return new WaitForEndOfFrame();
            } while (Vector2.Distance(transform.position, _to) != 0);
        }
        public void SetHighJump()
        {
            animPlayer.SetInteger("ActionType", 2);
            animPlayer.SetTrigger("Action");
        }
        public void SetStandAndLookUp(bool _look)
        {
            animPlayer.SetInteger("ActionType", 5);
            animPlayer.SetBool("bAction", _look);

        }
        public void SetPaint(bool _paint)
        {
            animPlayer.SetInteger("ActionType", 10);
            animPlayer.SetBool("bAction", _paint);

        }
        public void SetMeHiddentIdle(bool _hidden)
        {
            animPlayer.SetInteger("ActionType", 13);
            animPlayer.SetBool("bAction", _hidden);

        }
        public void SetPhoto( )
        {
            animPlayer.SetInteger("ActionType", 14);
            animPlayer.SetTrigger("Action");

        }

        #region EYESCONTROL
        Transform lookAtTarget;
        public Transform pupil_R;
        public void SetEnableDisableEyesControl(bool _enable, Transform _target = null)
        {
            animPlayer.SetBool("EyeControll", _enable);
            lookAtTarget = _target;
            if (_enable)
            {
                if (lookAtTarget.GetComponent<PlayerController>())
                    lookAtTarget = lookAtTarget.GetComponent<PlayerController>().pupil_R;

            }

        }
        public void SetEnableDisableEyesControl(bool _enable, string _target)
        {
            if (_enable)
            {
                GameObject go = GameObject.Find(_target);
                if (go)
                {
                    lookAtTarget = go.transform;
                    if (lookAtTarget.GetComponent<PlayerController>())
                        lookAtTarget = lookAtTarget.GetComponent<PlayerController>().pupil_R;
                    animPlayer.SetBool("EyeControll", _enable);

                }
                else
                {

                    Debug.Log("НЕ НАЙДЕН ОБЪЕКТ НА КОТОРЫЙ ЗЫРИТЬ");
                }

            }
            else
            {
                lookAtTarget = null;
            }
        }
        #endregion
    }
}