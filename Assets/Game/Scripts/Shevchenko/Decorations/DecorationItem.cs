﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
    public class DecorationItem : MonoBehaviour
    {

        public Transform leftPos, rightPos;
        BoxCollider2D coll;

        public List<GameObject> list_Obstacles = new List<GameObject>();

        public virtual void Awake()
        {
            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "LeftPos")
                    leftPos = item;
                else if (item.name == "RightPos")
                    rightPos = item;

            }
            AwakeOther();
        }
        public virtual void AwakeOther()
        {
            coll = GetComponent<BoxCollider2D>();
          }
        public virtual   void DropRandomObstacle(GameObject _src)
        {
            GameObject go = (GameObject)Instantiate(_src);
            go.transform.position = transform.position;
            go.transform.parent = transform;
            Vector2 pos = Random.insideUnitSphere * 18;
            go.transform.localPosition = new Vector3(pos.x, pos.y, 0);
            list_Obstacles.Add(go);
        }
        public virtual  void ItemPlaced()
        {
            foreach (var item in list_Obstacles)
            {
                Destroy(item);
            }
            list_Obstacles.Clear();
            ItemPlacedOther();
        }
        public virtual void ItemPlacedOther()
        {

        }
    }
}
