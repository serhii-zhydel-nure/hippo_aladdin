﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Shevchenko
{
    public class DecorationController : MonoBehaviour
    {
        public delegate void DecorCtrlr(float _deltaY);
        public static event DecorCtrlr OnDeltaYChanged;

        public delegate void DecorationReplaced(DecorationItem decorItem);
        public event DecorationReplaced OnDecorationReplaced;
        public static DecorationController instance;

        [Header("Если длинна массива декораций=0, ищем просто по сцене")]
        public List<DecorationItem> list_Decorations = new List<DecorationItem>();
        public bool currentMoveDirRight = true;
        public bool useYdelta;
        public Camera targetCam;
        public bool isEnabled = true;

        public virtual void Awake()
        {
            instance = this;
            if (GetComponent<Camera>())
            {
                targetCam = GetComponent<Camera>();
            }
            else
            {
                targetCam = Camera.main;
            }
            if (list_Decorations.Count == 0)
            {

                Debug.Log("Длинна массива декораций=0, ищем просто по сцене");
                list_Decorations.AddRange(FindObjectsOfType<DecorationItem>());

            }
        }

        public virtual void Start()
        {
            InitPoses();
            StartCoroutine(DecorationWorker());

        }
        void InitPoses()
        {

            for (int i = 1; i < list_Decorations.Count; i++)
            {
                float step = Mathf.Abs(list_Decorations[i].leftPos.position.x - list_Decorations[i].rightPos.transform.position.x);
                list_Decorations[i].transform.position = new Vector3(list_Decorations[i - 1].transform.position.x + step, list_Decorations[i].transform.position.y, 0);
                list_Decorations[i].ItemPlaced();
                ItemPlaced(list_Decorations[i]);
                if (useYdelta)
                { // ищем предыдущую 
                    DecorationItem prev = null;

                    prev = list_Decorations[i - 1];

                    float deltaY = list_Decorations[i].leftPos.position.y - prev.rightPos.position.y;
                    list_Decorations[i].transform.position =
                        new Vector2(list_Decorations[i].transform.position.x, list_Decorations[i].transform.position.y - deltaY);
                    //Debug.Log("deltaY "+ deltaY+" prev " , prev);
                    if (OnDeltaYChanged != null)
                    {
                        OnDeltaYChanged(deltaY);
                    }
                }
            }
        }
        public void StopAutomaticReplace()
        {
            isEnabled = false;
        }
        public void ManualSetNextItem(DecorationItem _di)
        {
            list_Decorations.Sort(delegate (DecorationItem us1, DecorationItem us2)
            { return us1.transform.transform.position.x.CompareTo(us2.transform.transform.position.x); });
            bool isItemFound = false;

            for (int i = 0; i < list_Decorations.Count; i++)
            {
                if (!isItemFound)
                {
                    Vector3 screenPoint = targetCam.WorldToViewportPoint(list_Decorations[i].rightPos.position);
                    bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                    if (!onScreen&& list_Decorations[i].transform.position.x> targetCam.transform.position.x)
                    {
                        //float step = Mathf.Abs(_di.leftPos.position.x - _di.rightPos.transform.position.x);
                        float step = _di.transform.position.x - _di.leftPos.transform.position.x;
                        step += list_Decorations[i].rightPos.position.x - list_Decorations[i].transform.position.x;

                        _di.transform.position = new Vector3(list_Decorations[i].transform.position.x + step, _di.transform.position.y, 0);
                        _di.ItemPlaced();
                        ItemPlaced(_di);

                        isItemFound = true;
                    }
                }
                else
                {
                    float step = Mathf.Abs(_di.leftPos.position.x - _di.rightPos.transform.position.x);

                    list_Decorations[i].transform.position = new Vector3(list_Decorations[0].transform.position.x - step * 4, _di.transform.position.y, 0);

                }
            }
        }

        public virtual IEnumerator DecorationWorker()
        {
            while (true)
            {
                if (isEnabled)
                {
                    if (currentMoveDirRight)
                    {
                        MoveRight();
                    }
                    else // ВЛЕВО НЕ ДОПИЛЕНЛО
                    {

                    }
                }


                yield return new WaitForSeconds(1);
            }
        }
        public virtual void MoveRight()
        {
            list_Decorations.Sort(delegate (DecorationItem us1, DecorationItem us2)
            { return us1.transform.transform.position.x.CompareTo(us2.transform.transform.position.x); });

            for (int i = 0; i < list_Decorations.Count; i++)
            {

                //if (list_Decorations[i].rightPos.position.x < targetPlayer.position.x)
                //{
                float fromScreenPosX = targetCam.ScreenToWorldPoint(Vector2.zero).x;
                if (list_Decorations[i].rightPos.position.x < fromScreenPosX)
                {
                    //float step = Mathf.Abs(list_Decorations[i].leftPos.position.x - list_Decorations[i].rightPos.transform.position.x);
                    float step =    list_Decorations[i].transform.position.x- list_Decorations[i].leftPos.transform.position.x;
                      step += list_Decorations.Last().rightPos.position.x- list_Decorations.Last().transform.position.x  ;
 
                    list_Decorations[i].transform.position = new Vector3(list_Decorations.Last().transform.position.x + step, list_Decorations[i].transform.position.y, 0);
                    list_Decorations[i].ItemPlaced();
                    ItemPlaced(list_Decorations[i]);
                    if (useYdelta)
                    { // ищем предыдущую 
                        DecorationItem prev = null;
                        if (i == list_Decorations.Count - 1)
                        {
                            prev = list_Decorations[0];

                        }
                        else
                        {
                            prev = list_Decorations[i + 1];
                        }
                        float deltaY = list_Decorations[i].leftPos.position.y - prev.rightPos.position.y;
                        list_Decorations[i].transform.position =
                            new Vector2(list_Decorations[i].transform.position.x, list_Decorations[i].transform.position.y - deltaY);
                        //Debug.Log("deltaY "+ deltaY+" prev " , prev);
                        if (OnDeltaYChanged != null)
                        {
                            OnDeltaYChanged(deltaY);
                        }
                    }
                    break;
                }
                //}
            }
        }
        /*
                 public virtual void MoveRight()
        {
            list_Decorations.Sort(delegate (DecorationItem us1, DecorationItem us2)
            { return us2.transform.transform.position.x.CompareTo(us1.transform.transform.position.x); });

            for (int i = 0; i < list_Decorations.Count; i++)
            {

                //if (list_Decorations[i].rightPos.position.x < targetPlayer.position.x)
                //{
                float fromScreenPosX = targetCam.ScreenToWorldPoint(Vector2.zero).x;
                if (list_Decorations[i].rightPos.position.x < fromScreenPosX)
                {
                    //float step = Mathf.Abs(list_Decorations[i].leftPos.position.x - list_Decorations[i].rightPos.transform.position.x);
                    float step = Mathf.Abs(list_Decorations[i].leftPos.position.x - list_Decorations[i].rightPos.transform.position.x);

                    list_Decorations[i].transform.position = new Vector3(list_Decorations[0].transform.position.x + step, list_Decorations[i].transform.position.y, 0);
                    list_Decorations[i].ItemPlaced();
                    ItemPlaced(list_Decorations[i]);
                    if (useYdelta)
                    { // ищем предыдущую 
                        DecorationItem prev = null;
                        if (i == list_Decorations.Count - 1)
                        {
                            prev = list_Decorations[0];

                        }
                        else
                        {
                            prev = list_Decorations[i + 1];
                        }
                        float deltaY = list_Decorations[i].leftPos.position.y - prev.rightPos.position.y;
                        list_Decorations[i].transform.position =
                            new Vector2(list_Decorations[i].transform.position.x, list_Decorations[i].transform.position.y - deltaY);
                        //Debug.Log("deltaY "+ deltaY+" prev " , prev);
                        if (OnDeltaYChanged != null)
                        {
                            OnDeltaYChanged(deltaY);
                        }
                    }
                    break;
                }
                //}
            }
        }
*/
        public virtual void ItemPlaced(DecorationItem item)
        {

            if (OnDecorationReplaced != null)
            {
                OnDecorationReplaced(item);
            }
        }

    }
}
