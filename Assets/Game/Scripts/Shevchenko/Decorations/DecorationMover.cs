﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{

    public class DecorationMover : MonoBehaviour
    {
        public float speed = 1;
        public bool isMoveAllowed = true;

        public void AllowDisallowMove(bool _allow)
        {
            isMoveAllowed = _allow;
        }
        void Update()
        {
            if (isMoveAllowed)
            {
                transform.Translate(new Vector3(-.1f * speed, 0, 0));

            }

        }
    }
}
