﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class TextureExtension
{
	public struct Point
	{
		public short x;
		public short y;

		public Point(short aX, short aY)
		{
			x = aX;
			y = aY;
		}

		public Point(int aX, int aY)
			: this((short)aX, (short)aY)
		{
		}
	}

	public static void FloodFillArea(this Texture2D aTex, Texture2D contour, int aX, int aY, Color aFillColor)
	{
		int w = contour.width;
		int h = contour.height;
		Color[] sourceColors = aTex.GetPixels();
		Color[] colors = contour.GetPixels();
		Color refCol = colors[aX + aY * w];
		Queue<Point> nodes = new Queue<Point>();
		nodes.Enqueue(new Point(aX, aY));
		while (nodes.Count > 0)
		{
			Point current = nodes.Dequeue();
			for (int i = current.x; i < w; i++)
			{
				Color C = colors[i + current.y * w];
				if (C != refCol || C == aFillColor)
					break;
				colors[i + current.y * w] = aFillColor;
				sourceColors[i + current.y * w] = aFillColor;
				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
			for (int i = current.x - 1; i >= 0; i--)
			{
				Color C = colors[i + current.y * w];
				if (C != refCol || C == aFillColor)
					break;
				colors[i + current.y * w] = aFillColor;
				sourceColors[i + current.y * w] = aFillColor;
				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (C == refCol && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
		}
		aTex.SetPixels(sourceColors);
//		return colors;
	}

	public static void FloodFillArea(this Texture2D aTex, int aX, int aY, Color aFillColor, float tolerance)
	{
		float tol = tolerance / 100;
		int w = aTex.width;
		int h = aTex.height;
		Color[] colors = aTex.GetPixels();
		Color refCol = colors[aX + aY * w];
		Queue<Point> nodes = new Queue<Point>();
		nodes.Enqueue(new Point(aX, aY));
		while (nodes.Count > 0)
		{
			Point current = nodes.Dequeue();
			for (int i = current.x; i < w; i++)
			{
				Color C = colors[i + current.y * w];
				if (/*C != refCol*/!ColorTest(refCol, C, tol) || C == aFillColor)
					break;
				colors[i + current.y * w] = aFillColor;
				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (/*C == refCol*/ColorTest(refCol, C, tol) && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (/*C == refCol*/ColorTest(refCol, C, tol) && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
			for (int i = current.x - 1; i >= 0; i--)
			{
				Color C = colors[i + current.y * w];
				if (!ColorTest(refCol, C, tol) || C == aFillColor)
					break;
				colors[i + current.y * w] = aFillColor;
				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (/*C == refCol*/ColorTest(refCol, C, tol) && C != aFillColor)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (ColorTest(refCol, C, tol)&& C != aFillColor)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
		}
		aTex.SetPixels(colors);
	}
 
	public static void FloodFillBorder(this Texture2D aTex, int aX, int aY, Color aFillColor, Color aBorderColor)
	{
		int w = aTex.width;
		int h = aTex.height;
		Color[] colors = aTex.GetPixels();
		byte[] checkedPixels = new byte[colors.Length];
		Color refCol = aBorderColor;
		Queue<Point> nodes = new Queue<Point>();
		nodes.Enqueue(new Point(aX, aY));
		while (nodes.Count > 0)
		{
			Point current = nodes.Dequeue();

			for (int i = current.x; i < w; i++)
			{
				if (checkedPixels[i + current.y * w] > 0 || colors[i + current.y * w] == refCol)
					break;
				colors[i + current.y * w] = aFillColor;
				checkedPixels[i + current.y * w] = 1;
				if (current.y + 1 < h)
				{
					if (checkedPixels[i + current.y * w + w] == 0 && colors[i + current.y * w + w] != refCol)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					if (checkedPixels[i + current.y * w - w] == 0 && colors[i + current.y * w - w] != refCol)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
			for (int i = current.x - 1; i >= 0; i--)
			{
				if (checkedPixels[i + current.y * w] > 0 || colors[i + current.y * w] == refCol)
					break;
				colors[i + current.y * w] = aFillColor;
				checkedPixels[i + current.y * w] = 1;
				if (current.y + 1 < h)
				{
					if (checkedPixels[i + current.y * w + w] == 0 && colors[i + current.y * w + w] != refCol)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					if (checkedPixels[i + current.y * w - w] == 0 && colors[i + current.y * w - w] != refCol)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
		}
		aTex.SetPixels(colors);
	}

 	public static void FloodFillBorder(this Texture2D aTex, Texture2D contour, int aX, int aY, Color aFillColor, Color aBorderColor)
	{
		int w = aTex.width;
		int h = aTex.height;
		Color[] sourceColors = aTex.GetPixels();
		//		List<Color> targetColors = new List<Color>();
		Color[] colors = contour.GetPixels();
		byte[] checkedPixels = new byte[colors.Length];
		Color refCol = aBorderColor;
		Queue<Point> nodes = new Queue<Point>();
		nodes.Enqueue(new Point(aX, aY));	
		while (nodes.Count > 0)
		{
			Point current = nodes.Dequeue();

			for (int i = current.x; i < w; i++)
			{
				if (checkedPixels[i + current.y * w] > 0 || IsDarker(colors[i + current.y * w], refCol))
					break;
				colors[i + current.y * w] = aFillColor;
				sourceColors[i + current.y * w] = aFillColor;
				checkedPixels[i + current.y * w] = 1;
				if (current.y + 1 < h)
				{
					if (checkedPixels[i + current.y * w + w] == 0 && !IsDarker(colors[i + current.y * w], refCol))
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					if (checkedPixels[i + current.y * w - w] == 0 && !IsDarker(colors[i + current.y * w], refCol))
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
			for (int i = current.x - 1; i >= 0; i--)
			{
				if (checkedPixels[i + current.y * w] > 0 || IsDarker(colors[i + current.y * w], refCol))
					break;
				colors[i + current.y * w] = aFillColor;
				sourceColors[i + current.y * w] = aFillColor;
				checkedPixels[i + current.y * w] = 1;
				if (current.y + 1 < h)
				{
					if (checkedPixels[i + current.y * w + w] == 0 && !IsDarker(colors[i + current.y * w], refCol))
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					if (checkedPixels[i + current.y * w - w] == 0 && !IsDarker(colors[i + current.y * w], refCol))
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
		}
		aTex.SetPixels(sourceColors);
	}
	public static Texture2D FloodFillBorder(this Texture2D aTex, Texture2D contour, Rect area, int aX, int aY, Color aFillColor, Color aBorderColor)
	{
		Texture2D result = new Texture2D((int)area.width, (int)area.height);
		int w = (int)area.width;
		int h = (int)area.height;
//		Debug.Log("aTex.width: " + aTex.width + " aTex.height: " + aTex.height);
		Color[] sourceColors = aTex.GetPixels((int)area.x, (int)area.y, (int)area.width, (int)area.height);
		Color[] colors = contour.GetPixels((int)area.x, (int)area.y, (int)area.width, (int)area.height);
		byte[] checkedPixels = new byte[colors.Length];
		Color refCol = aBorderColor;
		Queue<Point> nodes = new Queue<Point>();
		nodes.Enqueue(new Point(aX, aY));	
		while (nodes.Count > 0)
		{
			Point current = nodes.Dequeue();

			for (int i = current.x; i < w; i++)
			{
				if (checkedPixels[i + current.y * w] > 0 || IsDarker(colors[i + current.y * w], refCol))
					break;
				colors[i + current.y * w] = aFillColor;
				sourceColors[i + current.y * w] = aFillColor;
				checkedPixels[i + current.y * w] = 1;
				if (current.y + 1 < h)
				{
					if (checkedPixels[i + current.y * w + w] == 0 && !IsDarker(colors[i + current.y * w], refCol))
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					if (checkedPixels[i + current.y * w - w] == 0 && !IsDarker(colors[i + current.y * w], refCol))
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
			for (int i = current.x - 1; i >= 0; i--)
			{
				if (checkedPixels[i + current.y * w] > 0 || IsDarker(colors[i + current.y * w], refCol))
					break;
				colors[i + current.y * w] = aFillColor;
				sourceColors[i + current.y * w] = aFillColor;
				checkedPixels[i + current.y * w] = 1;
				if (current.y + 1 < h)
				{
					if (checkedPixels[i + current.y * w + w] == 0 && !IsDarker(colors[i + current.y * w], refCol))
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					if (checkedPixels[i + current.y * w - w] == 0 && !IsDarker(colors[i + current.y * w], refCol))
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
		}
		result.SetPixels(sourceColors);
		result.Apply();
		return result;
	}

 
	public static void Stroke(this Texture2D aTex, int aX, int aY, Color color, int thickness = 1)
	{
		int w = aTex.width;
		int h = aTex.height;

		Color[] colors = aTex.GetPixels();
		byte[] checkedPixels = new byte[colors.Length];
		Color refCol = colors[aX + aY * w];
//		Color refCol = Color.clear;
		Queue<Point> nodes = new Queue<Point>();
		nodes.Enqueue(new Point(aX, aY));
		while (nodes.Count > 0)
		{
			Point current = nodes.Dequeue();
			for (int i = current.x; i < w; i++)
			{
				Color C = colors[i + current.y * w];
				if (C != refCol)
				{
					colors[i + current.y * w] = Color.blue;
					break;
				}
				if (C == color)
				{
					break;
				}

				colors[i + current.y * w] = color;
				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (C == refCol && C != color)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (C == refCol && C != color)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
			for (int i = current.x - 1; i >= 0; i--)
			{
				Color C = colors[i + current.y * w];

				if (C != refCol)
				{
					colors[i + current.y * w] = Color.blue;
					break;
				}
				if (C == color)
				{
					break;
				}
				colors[i + current.y * w] = color;
				if (current.y + 1 < h)
				{
					C = colors[i + current.y * w + w];
					if (C == refCol && C != color)
						nodes.Enqueue(new Point(i, current.y + 1));
				}
				if (current.y - 1 >= 0)
				{
					C = colors[i + current.y * w - w];
					if (C == refCol && C != color)
						nodes.Enqueue(new Point(i, current.y - 1));
				}
			}
		}
		aTex.SetPixels(colors);


//		for (int x = 0; x < aTex.width; x++)
//		{
//			for (int y = 0; y < aTex.height; y++)
//			{
//				if (aTex.GetPixel(x, y) == refColor)
//				{
//					if (aTex.GetPixel(x + 1, y) != refColor && checkedPixels[x + 1, y] == 0)
//					{
//						checkedPixels[x + 1, y] = 1;
//						aTex.SetPixel(x + 1, y, color);
//					}
//					if (aTex.GetPixel(x - 1, y) != refColor && checkedPixels[x - 1, y] == 0)
//					{
//						checkedPixels[x - 1, y] = 1;
//						aTex.SetPixel(x - 1, y, color);
//					}
//					if (aTex.GetPixel(x, y + 1) != refColor && checkedPixels[x, y + 1] == 0)
//					{
//						checkedPixels[x, y + 1] = 1;
//						aTex.SetPixel(x, y + 1, color);
//					}
//					if (aTex.GetPixel(x, y - 1) != refColor && checkedPixels[x, y - 1] == 0)
//					{
//						checkedPixels[x, y - 1] = 1;
//						aTex.SetPixel(x, y - 1, color);
//					}
//				}
//			}
//		}
//		aTex.Apply();
	}
	private static bool IsDarker(Color refColor, Color targetColor)
	{
		if (refColor.r == targetColor.r && refColor.g == targetColor.g && refColor.b == targetColor.b && refColor.a > targetColor.a)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	private static bool IsDarker(Color32 refColor, Color32 targetColor)
	{
		if (refColor.r == targetColor.r && refColor.g == targetColor.g && refColor.b == targetColor.b && refColor.a > targetColor.a)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public static bool ColorTest(Color c1, Color c2, float tol) {
		float diffRed   = Mathf.Abs(c1.r - c2.r);
		float diffGreen = Mathf.Abs(c1.g - c2.g);
		float diffBlue  = Mathf.Abs(c1.b - c2.b);
		//Those values you can just divide by the amount of difference saturations (255), and you will get the difference between the two.

		float pctDiffRed   = (float)diffRed   / 255;
		float pctDiffGreen = (float)diffGreen / 255;
		float pctDiffBlue  = (float)diffBlue  / 255;

		//After which you can just find the average color difference in percentage.
		float diffPercentage = (pctDiffRed + pctDiffGreen + pctDiffBlue) / 3 * 100;

		if(diffPercentage >= tol) {
			return false;
		} else { 
			return true;
		}
	}
}
