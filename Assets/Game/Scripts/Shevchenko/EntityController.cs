﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Shevchenko
{
    public enum EntityGenderType
    {
        unisex, male, female
    }
    public enum EntityPart
    {
        Head, Body, Leg, Hand
    }

    public class EntityController : MonoBehaviour
    {
        public static EntityController instance;

        List<Entity> list_allEntities = new List<Entity>();
        public List<EntityPlayer> list_players = new List<EntityPlayer>();

        public List<Entity> mas_head = new List<Entity>();
        public List<Entity> mas_body = new List<Entity>();
        public List<Entity> mas_leg = new List<Entity>();
        public List<Entity> mas_hand = new List<Entity>();
        public int[] mas_rand_mas_head;
        public int[] mas_rand_mas_body;
        public int[] mas_rand_mas_leg;
        public int[] mas_rand_mas_hand;

        public GameObject goCloud;
        Animator animCloud;
        Transform gocloud_base, CloudEntityPos;
        GameObject currentCloudEntity;

        void Awake()
        {
            gocloud_base = (Transform)Helpers.Find("cloud_base", typeof(Transform));
            // goCloud = (GameObject)Helpers.Find("cloud1_ctrlr", typeof(GameObject));
            animCloud = (Animator)(Animator)Helpers.Find("cloud1_ctrlr_", typeof(Animator));
            CloudEntityPos = (Transform)Helpers.Find("CloudEntityPos", typeof(Transform));

            instance = this;
            foreach (EntityPlayer item in FindObjectsOfType<EntityPlayer>())
            {
                list_players.Add(item);
            }

            foreach (Entity item in GameObject.FindObjectsOfType<Entity>())
            {
                list_allEntities.Add(item);
            }
            Sort();
            SetWantedEntities();
        }

        void SetWantedEntities()
        {
            foreach (var item in list_players)
            {
                item.AddWantedEntedEntity(RandomOne(mas_body, item, mas_rand_mas_body));
                item.AddWantedEntedEntity(RandomOne(mas_head, item, mas_rand_mas_head));
                //item.AddWantedEntedEntity(RandomOne(mas_hand, item, mas_rand_mas_hand));
                // item.AddWantedEntedEntity(RandomOne(mas_leg, item, mas_rand_mas_leg));

            }
        }

        Entity RandomOne(List<Entity> _mas, EntityPlayer _EntityPlayer, int[] _mas_RandomNumbers)
        {
            for (int i = 0; i < _mas.Count; i++)
            {
                if (!_mas[_mas_RandomNumbers[i]].reserved && (CompareGender(_mas[_mas_RandomNumbers[i]], _EntityPlayer)))
                {
                    _mas[_mas_RandomNumbers[i]].reserved = true;
                    return _mas[_mas_RandomNumbers[i]];

                }
            }
            Debug.Log("NO ONE RANDOM THING FOUND");
            return null;
        }

        bool CompareGender(Entity _Entity, EntityPlayer _EntityPlayer)
        {
            PlayerType playerType = _EntityPlayer.GetComponent<PlayerController>().playerType;

            switch (playerType)
            {
                case PlayerType.pEpa: return _Entity.forPepa ? true : false;
                case PlayerType.george: return _Entity.forGeorge ? true : false;
                case PlayerType.mama: return _Entity.forMama ? true : false;
                case PlayerType.pApa: return _Entity.forPApa ? true : false;
                case PlayerType.other: return false;
                default: return false;
            }
        }

        int[] RandomRawEntity(int max)
        {
            System.Random random;
            random = new System.Random();
            return Enumerable.Range(0, max).OrderBy(n => random.Next()).ToArray();
        }

        void Sort()
        {
            foreach (var item in list_allEntities)
            {
                switch (item.currentPart)
                {
                    case EntityPart.Head:
                        mas_head.Add(item);
                        break;
                    case EntityPart.Body:
                        mas_body.Add(item);
                        break;
                    case EntityPart.Leg:
                        mas_leg.Add(item);
                        break;
                    case EntityPart.Hand:
                        mas_hand.Add(item);
                        break;
                    default:
                        break;
                }
            }
            mas_rand_mas_head = RandomRawEntity(mas_head.Count);
            mas_rand_mas_body = RandomRawEntity(mas_body.Count);
            mas_rand_mas_leg = RandomRawEntity(mas_leg.Count);
            mas_rand_mas_hand = RandomRawEntity(mas_hand.Count);
        }


        public void CloudSetWantedEntity(GameObject _goEntityResource, float _scaleFactor)
        {

            //animCloud

            if (currentCloudEntity) Destroy(currentCloudEntity);
            currentCloudEntity = (GameObject)(Instantiate(_goEntityResource, CloudEntityPos.position, Quaternion.identity));
            if (!goCloud.activeInHierarchy) currentCloudEntity.SetActive(false);
            //currentCloudEntity.GetComponent<Entity>().enabled = false;
            currentCloudEntity.GetComponent<BoxCollider2D>().enabled = false;
            currentCloudEntity.transform.parent = CloudEntityPos;
            currentCloudEntity.transform.localScale = new Vector3(_scaleFactor, _scaleFactor, _scaleFactor);
            currentCloudEntity.transform.localPosition = currentCloudEntity.GetComponent<Entity>().deltaPosCloud;
            currentCloudEntity.transform.localRotation = Quaternion.Euler(0, 0, currentCloudEntity.GetComponent<Entity>().angleRotationCloud);
            StaticParams.SetLayer(currentCloudEntity.transform, "Default");
            StaticParams.SetOrderInLayer(currentCloudEntity.transform, 1);
            goCloud.SetActive(true);
        }

        public List<GameObject> list_currentCloudEntities = new List<GameObject>();

        public void CloudSetWantedEntity(List<Entity> _list_entities)
        {
            foreach (var item in list_currentCloudEntities)
            {
                item.transform.parent = null;
                Destroy(item);
            }
            list_currentCloudEntities.Clear();

            //animCloud

            if (currentCloudEntity) Destroy(currentCloudEntity);
            for (int i = 0; i < 4; i++)
            {
                Transform posInCloud = null;
                foreach (Transform item in goCloud.GetComponentsInChildren<Transform>(true))
                {
                    if (item.name.Contains(_list_entities[i].currentPart.ToString()))
                    {
                        posInCloud = item;
                    }
                }

                Debug.Log("posInCloud " + posInCloud);
                currentCloudEntity = (GameObject)(Instantiate(_list_entities[i].gameObject, posInCloud.position, Quaternion.identity));
                currentCloudEntity.GetComponent<BoxCollider2D>().enabled = false;
                currentCloudEntity.transform.parent = posInCloud.transform;
                currentCloudEntity.transform.localScale = new Vector3(_list_entities[i].scaleForCloud, _list_entities[i].scaleForCloud, _list_entities[i].scaleForCloud);

                currentCloudEntity.transform.localRotation = Quaternion.Euler(0, 0, currentCloudEntity.GetComponent<Entity>().angleRotationCloud);
                StaticParams.SetLayer(currentCloudEntity.transform, "Default");
                StaticParams.SetOrderInLayer(currentCloudEntity.transform, 1);

                list_currentCloudEntities.Add(currentCloudEntity);
                Debug.Log(list_currentCloudEntities[i].name);

            }
            if (!goCloud.activeInHierarchy)
                foreach (var item in list_currentCloudEntities)
                {
                    item.SetActive(false);
                }
            goCloud.SetActive(true);

        }

        public void CloudSetWantedEntity(PlayerController _pc)
        {
            //animCloud

            if (currentCloudEntity) Destroy(currentCloudEntity);
            StartCoroutine(Wear(_pc));


        }

        IEnumerator Wear(PlayerController _pc)
        {
            currentCloudEntity = (GameObject)(Instantiate(_pc.gameObject, new Vector2(-20, -20), Quaternion.identity));

            //currentCloudEntity.GetComponent<Animator>().enabled = false;
            EntityPlayer ep = currentCloudEntity.GetComponent<EntityPlayer>();

            //GameObject goEntity = (GameObject)Instantiate(ep.list_MyWantedEntities[0].gameObject, new Vector3(-30, -30, 0), Quaternion.identity);
            for (int i = 0; i < ep.list_MyWantedEntities.Count; i++)
            {
                switch (ep.list_MyWantedEntities[i].currentPart)
                {
                    case EntityPart.Head:
                        GameObject res = Resources.Load<GameObject>(
                     "Scene7/Head/" + ep.list_MyWantedEntities[i].name);

                        StartCoroutine(ep.DropLoadedEntity(res));
                        break;

                    case EntityPart.Body:
                        GameObject res2 = Resources.Load<GameObject>(
        "Scene7/Body/" + ep.list_MyWantedEntities[i].name);

                        StartCoroutine(ep.DropLoadedEntity(res2));

                        break;
                    case EntityPart.Leg:
                        GameObject res3 = Resources.Load<GameObject>(
           "Scene7/Leg/" + ep.list_MyWantedEntities[i].name);

                        StartCoroutine(ep.DropLoadedEntity(res3));

                        break;
                    case EntityPart.Hand:
                        GameObject res4 = Resources.Load<GameObject>(
                      "Scene7/Hand/" + ep.list_MyWantedEntities[i].name);

                        StartCoroutine(ep.DropLoadedEntity(res4));
                        break;
                    default:
                        break;
                }

            }


            yield return new WaitForSeconds(.1f);

            if (!goCloud.activeInHierarchy) currentCloudEntity.SetActive(false);
            currentCloudEntity.GetComponent<BoxCollider2D>().enabled = false;
            StaticParams.SetLayer(currentCloudEntity.transform, "Default");
            StaticParams.SetOrderInLayer(currentCloudEntity.transform, 1);
            //currentCloudEntity.transform.position = CloudEntityPos.position;
            goCloud.SetActive(true);
            yield return new WaitForSeconds(.1f);

            //currentCloudEntity.transform.parent = CloudEntityPos;
            currentCloudEntity.transform.localScale = new Vector3(currentCloudEntity.transform.localScale.x * 0.8f, currentCloudEntity.transform.localScale.x * 0.8f, currentCloudEntity.transform.localScale.x * 0.8f);

        }





        public void CloudTimeToShowEntity()
        {

            if (currentCloudEntity)
            {
                currentCloudEntity.SetActive(true);

            }
            if (list_currentCloudEntities.Count > 0)
            {
                foreach (var item in list_currentCloudEntities)
                {
                    item.SetActive(true);
                }
            }
            currentCloudEntity.transform.position = CloudEntityPos.position;
            currentCloudEntity.transform.parent = CloudEntityPos;

        }

        public void CloudHide()
        {
            animCloud.Play("cloud_finish");
        }

    }
}
