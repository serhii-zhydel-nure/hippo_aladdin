﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Shevchenko
{
    public class EntityPlayer : MonoBehaviour
    {
        public class EntityPlayerPartParams
        {
            public string currentEntityPartName;
            public Entity currentEntityPart;
            public Transform curParentPosition;
            public Transform curParentPosition2;
            public Transform curBodyNaked;
            public Transform DefaultDress;
            public Transform DefaultDress2;
            public int defaultOrderInLayer;
            public string defaultOrderName;

            public int defaultOrderInLayer2;
            public string defaultOrderName2;
            public float targetScale;
        }

        EntityPlayerPartParams paramHead = new EntityPlayerPartParams();
        EntityPlayerPartParams paramLeg = new EntityPlayerPartParams();
        EntityPlayerPartParams paramHand = new EntityPlayerPartParams();
        EntityPlayerPartParams paramBody = new EntityPlayerPartParams();

        public List<Entity> list_MyWantedEntities = new List<Entity>();
        Entity WantedEntity_head, WantedEntity_body, WantedEntity_leg, WantedEntity_Hand;

        Entity currentWantedEntity;


        void Awake()
        {
            foreach (Transform item in transform.GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "Entity_hand") paramHand.curParentPosition = item;
                else if (item.name == "a_0013") paramHand.DefaultDress = item;


                else if (item.name == "Entity_body") paramBody.curParentPosition = item;
                else if (item.name == "a_0006") paramBody.DefaultDress = item;
                else if (item.name == "BodyNaked") paramBody.curBodyNaked = item;

                else if (item.name == "Entity_head") paramHead.curParentPosition = item;
                else if (item.name == "head_sprite") paramHead.DefaultDress = item;


                else if (item.name == "Entity_leg1") paramLeg.curParentPosition = item;
                else if (item.name == "Entity_leg2") paramLeg.curParentPosition2 = item;
                else if (item.name == "a_0001") paramLeg.DefaultDress = item;
                else if (item.name == "a_0003") paramLeg.DefaultDress2 = item;
            }
        }

        void Start()
        {
            LoadDefEntityParams();
        }

        void LoadDefEntityParams()
        {
            paramBody.defaultOrderInLayer = paramBody.DefaultDress.GetComponent<SpriteRenderer>().sortingOrder;
            paramBody.defaultOrderName = paramBody.DefaultDress.GetComponent<SpriteRenderer>().sortingLayerName;

            paramHand.defaultOrderInLayer = paramHand.DefaultDress.GetComponent<SpriteRenderer>().sortingOrder;
            paramHand.defaultOrderName = paramHand.DefaultDress.GetComponent<SpriteRenderer>().sortingLayerName;

            paramHead.defaultOrderInLayer = paramHead.DefaultDress.GetComponent<SpriteRenderer>().sortingOrder;
            paramHead.defaultOrderName = paramHead.DefaultDress.GetComponent<SpriteRenderer>().sortingLayerName;

            paramLeg.defaultOrderInLayer = paramLeg.DefaultDress.GetComponent<SpriteRenderer>().sortingOrder;
            paramLeg.defaultOrderName = paramLeg.DefaultDress.GetComponent<SpriteRenderer>().sortingLayerName;
            paramLeg.defaultOrderInLayer2 = paramLeg.DefaultDress2.GetComponent<SpriteRenderer>().sortingOrder;
            paramLeg.defaultOrderName2 = paramLeg.DefaultDress2.GetComponent<SpriteRenderer>().sortingLayerName;

        }

        public void AddWantedEntedEntity(Entity _en)
        {
            list_MyWantedEntities.Add(_en);
            switch (_en.currentPart)
            {
                case EntityPart.Body:
                    WantedEntity_body = _en;
                    break;

                case EntityPart.Hand:
                    WantedEntity_Hand = _en;
                    break;

                case EntityPart.Head:
                    WantedEntity_head = _en;
                    break;

                case EntityPart.Leg:
                    WantedEntity_leg = _en;
                    break;

                default:
                    break;
            }
        }

        public bool CheckAmIWanThisEntity(Entity _ent)
        {
            if (list_MyWantedEntities.Contains(_ent))
            {
                return true;
            }
            else return false;
        }
        //public bool CheckAmIWanThisEntity(Entity _ent)
        //{
        //    if (_ent == currentWantedEntity)
        //    {
        //        return true;
        //    }
        //    else return false;
        //}
        public void SetNextEntity()
        {
            if (currentWantedEntity) currentWantedEntity.isWantedEntityGotten = true;
            currentWantedEntity = WhatIsNextEntity();
        }
        public Entity WhatIsNextEntity()
        {
            foreach (var item in list_MyWantedEntities)
            {
                if (!item.isWantedEntityGotten)
                {
                    return item;
                }
            }
            return null;
        }

        public void SetCloud()
        {
            Entity next = WhatIsNextEntity();
            if (next)
            {
                //EntityController.instance.CloudSetWantedEntity(next.goResourceCurrentEntity, next.scaleForCloud);

            }

        }
 
        public void TryRemoveOldWntity(EntityPart _entityPart)
        {
            GameObject go = null;
            switch (_entityPart)
            {
                case EntityPart.Head:
                    if (!paramHead.currentEntityPart)
                        return;
                    go = paramHead.currentEntityPart.gameObject;
                    break;
                case EntityPart.Body:
                    if (!paramBody.currentEntityPart)
                        return;
                    go = paramBody.currentEntityPart.gameObject;
                    break;
                case EntityPart.Leg:
                    if (!paramLeg.currentEntityPart)
                        return;
                    go = paramLeg.currentEntityPart.gameObject;
                    StartCoroutine(StaticParams.LerpColor(paramLeg.currentEntityPart.leg1.gameObject, 0));
                    StartCoroutine(StaticParams.LerpColor(paramLeg.currentEntityPart.leg2.gameObject, 0));
                    Destroy(paramLeg.currentEntityPart.leg1.gameObject, 5);
                    Destroy(paramLeg.currentEntityPart.leg2.gameObject, 5);

                    break;
                case EntityPart.Hand:
                    if (!paramHand.currentEntityPart)
                        return;
                    go = paramHand.currentEntityPart.gameObject;
                    break;
                default:
                    break;
            }
            if (!go)
            {
                return;
            }
            StartCoroutine(StaticParams.LerpColor(go, 0));
            Destroy(go, 5);

        }
        public void PlaceCorrectEntity(Entity _Entity, bool isNeedSave = false)
        {
            TryRemoveOldWntity(_Entity.currentPart);

            Transform to = _Entity.transform;
            Transform to2 = null;


            switch (_Entity.currentPart)
            {
                case EntityPart.Body:
                    to = paramBody.curParentPosition;
                    paramBody.currentEntityPart = _Entity;

                    SetOrderInLayer(paramBody.currentEntityPart, paramBody.defaultOrderInLayer, 1, paramBody.defaultOrderName);
                    StartCoroutine(DisableOldEntity(paramBody.DefaultDress.gameObject, paramBody.curBodyNaked.gameObject, _Entity.transform, to));
                    paramBody.targetScale = _Entity.GetTargetScaleKoef(this);
                    StartCoroutine(Scale(paramBody.currentEntityPart.transform, paramBody.targetScale));
                    StartCoroutine(Rotate(paramBody.currentEntityPart, paramBody.currentEntityPart.GetTargetDeltaRotation(this)));
                    paramBody.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    break;

                case EntityPart.Hand:
                    to = paramHand.curParentPosition;
                    paramHand.currentEntityPart = _Entity;
                    SetOrderInLayer(paramHand.currentEntityPart, paramHand.defaultOrderInLayer, 0, paramHand.defaultOrderName);

                    paramHand.targetScale = _Entity.GetTargetScaleKoef(this);
                    StartCoroutine(Scale(paramHand.currentEntityPart.transform, paramHand.targetScale));
                    StartCoroutine(Rotate(paramHand.currentEntityPart, paramHand.currentEntityPart.GetTargetDeltaRotation(this)));
                    paramHand.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    //WantedEntity_Hand = _Entity;
                    break;

                case EntityPart.Head:
                    to = paramHead.curParentPosition;
                    paramHead.currentEntityPart = _Entity;
                    SetOrderInLayer(paramHead.currentEntityPart, paramHead.defaultOrderInLayer, 6, paramHead.defaultOrderName);

                    paramHead.targetScale = _Entity.GetTargetScaleKoef(this);
                    StartCoroutine(Scale(paramHead.currentEntityPart.transform, paramHead.targetScale));
                    StartCoroutine(Rotate(paramHead.currentEntityPart, paramHead.currentEntityPart.GetTargetDeltaRotation(this)));
                    paramHead.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    //WantedEntity_head = _Entity;
                    break;

                case EntityPart.Leg:

                    //WantedEntity_leg = _Entity;
                    to = paramLeg.curParentPosition;
                    to2 = paramLeg.curParentPosition2;

                    paramLeg.currentEntityPart = _Entity;
                    //SetOrderInLayer(paramHead.currentEntityPart, paramHead.defaultOrderInLayer, 6, paramHead.defaultOrderName);

                    //paramLeg.currentEntityPart.leg1.FindChild("qq").gameObject.SetActive(false);
                    //paramLeg.currentEntityPart.leg2.FindChild("qq").gameObject.SetActive(false);

                    paramLeg.targetScale = _Entity.GetTargetScaleKoef(this);
                    StartCoroutine(Scale(paramLeg.currentEntityPart.leg1.transform, paramLeg.targetScale));
                    StartCoroutine(Scale(paramLeg.currentEntityPart.leg2.transform, paramLeg.targetScale));

                    paramLeg.currentEntityPart.leg1.transform.localRotation = Quaternion.Euler(Vector2.zero);
                    paramLeg.currentEntityPart.leg2.transform.localRotation = Quaternion.Euler(Vector2.zero);

                    SetOrderInLayer(paramLeg.currentEntityPart.leg1, paramLeg.defaultOrderInLayer, 1, paramLeg.defaultOrderName);
                    SetOrderInLayer(paramLeg.currentEntityPart.leg2, paramLeg.defaultOrderInLayer2, 1, paramLeg.defaultOrderName2);

                    paramLeg.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);
                    paramLeg.curParentPosition2.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    break;

                default:
                    break;
            }

            float tempScale = _Entity.transform.localScale.x / transform.localScale.x;
            if (_Entity.transform.parent) _Entity.transform.localRotation = _Entity.transform.parent.parent.localRotation;


            _Entity.transform.parent = to;
            _Entity.transform.localScale = new Vector3(tempScale, tempScale, tempScale);

            if (to2)
            {
                _Entity.leg1.transform.parent = _Entity.transform.parent;
                _Entity.leg1.transform.localScale = new Vector3(1 / transform.localScale.x, 1 / transform.localScale.x, 1 / transform.localScale.x);

                _Entity.leg2.transform.parent = to2;
                _Entity.leg2.transform.localScale = _Entity.transform.localScale;
                StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.leg1, to,5));
                StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.leg2, to2,5));
            }


            StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.transform, to,5));
            LayerController.instance.RefreshAllLists_AddNewItems();
            if (isNeedSave) SaveEntity(_Entity);
        }

        public void PlaceCorrectEntityImmediate(Entity _Entity, bool isNeedSave = false)
        {
            TryRemoveOldWntity(_Entity.currentPart);

            Transform to = _Entity.transform;
            Transform to2 = null;

 
            switch (_Entity.currentPart)
            {
                case EntityPart.Body:
                    _Entity.transform.parent = paramBody.curParentPosition;

                    to = paramBody.curParentPosition;
                    paramBody.currentEntityPart = _Entity;

                    SetOrderInLayer(paramBody.currentEntityPart, paramBody.defaultOrderInLayer, 1, paramBody.defaultOrderName);
                    StartCoroutine(DisableOldEntity(paramBody.DefaultDress.gameObject, paramBody.curBodyNaked.gameObject, _Entity.transform, to));
                    paramBody.targetScale = _Entity.GetTargetScaleKoef(this);
                    //StartCoroutine(Scale(paramBody.currentEntityPart.transform, paramBody.targetScale));
                    paramBody.currentEntityPart.transform.localScale = new Vector3( paramBody.targetScale, paramBody.targetScale, paramBody.targetScale);

                    _Entity.transform.localRotation = Quaternion.Euler( paramBody.currentEntityPart.GetTargetDeltaRotation(this));
                    //StartCoroutine(Rotate(paramBody.currentEntityPart, paramBody.currentEntityPart.GetTargetDeltaRotation(this)));
                    paramBody.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    break;

                case EntityPart.Hand:
                     Debug.LogError("НЕ ДОПИСАНО");
                    to = paramHand.curParentPosition;
                    paramHand.currentEntityPart = _Entity;
                    SetOrderInLayer(paramHand.currentEntityPart, paramHand.defaultOrderInLayer, 0, paramHand.defaultOrderName);

                    paramHand.targetScale = _Entity.GetTargetScaleKoef(this);
                    StartCoroutine(Scale(paramHand.currentEntityPart.transform, paramHand.targetScale));
                    StartCoroutine(Rotate(paramHand.currentEntityPart, paramHand.currentEntityPart.GetTargetDeltaRotation(this)));
                    paramHand.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    //WantedEntity_Hand = _Entity;
                    break;

                case EntityPart.Head:
                    _Entity.transform.parent = paramHead.curParentPosition;

                    to = paramHead.curParentPosition;
                    paramHead.currentEntityPart = _Entity;
                    SetOrderInLayer(paramHead.currentEntityPart, paramHead.defaultOrderInLayer, 6, paramHead.defaultOrderName);

                    paramHead.targetScale = _Entity.GetTargetScaleKoef(this);
                    //StartCoroutine(Scale(paramHead.currentEntityPart.transform, paramHead.targetScale));

                    paramHead.currentEntityPart.transform.localScale = new Vector3(paramBody.targetScale, paramBody.targetScale, paramBody.targetScale);

                    _Entity.transform.localRotation = Quaternion.Euler(paramHead.currentEntityPart.GetTargetDeltaRotation(this));

                    //StartCoroutine(Rotate(paramHead.currentEntityPart, paramHead.currentEntityPart.GetTargetDeltaRotation(this)));
                    paramHead.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    //WantedEntity_head = _Entity;
                    break;

                case EntityPart.Leg:
                    Debug.LogError("НЕ ДОПИСАНО");

                    //WantedEntity_leg = _Entity;
                    to = paramLeg.curParentPosition;
                    to2 = paramLeg.curParentPosition2;

                    paramLeg.currentEntityPart = _Entity;
                    //SetOrderInLayer(paramHead.currentEntityPart, paramHead.defaultOrderInLayer, 6, paramHead.defaultOrderName);

                    //paramLeg.currentEntityPart.leg1.FindChild("qq").gameObject.SetActive(false);
                    //paramLeg.currentEntityPart.leg2.FindChild("qq").gameObject.SetActive(false);

                    paramLeg.targetScale = _Entity.GetTargetScaleKoef(this);
                    StartCoroutine(Scale(paramLeg.currentEntityPart.leg1.transform, paramLeg.targetScale));
                    StartCoroutine(Scale(paramLeg.currentEntityPart.leg2.transform, paramLeg.targetScale));

                    paramLeg.currentEntityPart.leg1.transform.localRotation = Quaternion.Euler(Vector2.zero);
                    paramLeg.currentEntityPart.leg2.transform.localRotation = Quaternion.Euler(Vector2.zero);

                    SetOrderInLayer(paramLeg.currentEntityPart.leg1, paramLeg.defaultOrderInLayer, 1, paramLeg.defaultOrderName);
                    SetOrderInLayer(paramLeg.currentEntityPart.leg2, paramLeg.defaultOrderInLayer2, 1, paramLeg.defaultOrderName2);

                    paramLeg.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);
                    paramLeg.curParentPosition2.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    break;

                default:
                    break;
            }

            //float tempScale = _Entity.transform.localScale.x / transform.localScale.x;
            //if (_Entity.transform.parent) _Entity.transform.localRotation = _Entity.transform.parent.parent.localRotation;


            //_Entity.transform.localScale = new Vector3(tempScale, tempScale, tempScale);

            if (to2)
            {
                _Entity.leg1.transform.parent = _Entity.transform.parent;
                _Entity.leg1.transform.localScale = new Vector3(1 / transform.localScale.x, 1 / transform.localScale.x, 1 / transform.localScale.x);

                _Entity.leg2.transform.parent = to2;
                _Entity.leg2.transform.localScale = _Entity.transform.localScale;
                StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.leg1, to, 5));
                StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.leg2, to2, 5));
            }


            StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.transform, to, 5));
            LayerController.instance.RefreshAllLists_AddNewItems();
            if (isNeedSave) SaveEntity(_Entity);
        }


        public void PlaceCorrectEntityCLOUD(Entity _Entity)
        {
            Transform to = _Entity.transform;
            Transform to2 = null;

            gameObject.SetActive(true);
            switch (_Entity.currentPart)
            {
                case EntityPart.Body:
                    to = paramBody.curParentPosition;
                    paramBody.currentEntityPart = _Entity;
                    SetOrderInLayer(paramBody.currentEntityPart, paramBody.defaultOrderInLayer, 1, paramBody.defaultOrderName);
                    //StartCoroutine(DisableOldEntity(paramBody.DefaultDress.gameObject, paramBody.curBodyNaked.gameObject, _Entity.transform, to));
                    paramBody.DefaultDress.gameObject.SetActive(false);
                    paramBody.curBodyNaked.gameObject.SetActive(true);
                    paramBody.targetScale = _Entity.GetTargetScaleKoef(this);
                    //StartCoroutine(Scale(paramBody.currentEntityPart.transform, paramBody.targetScale));
                    paramBody.currentEntityPart.transform.localScale = new Vector3(paramBody.targetScale, paramBody.targetScale, paramBody.targetScale);

                    paramBody.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    StartCoroutine(Rotate(paramBody.currentEntityPart, paramBody.currentEntityPart.GetTargetDeltaRotation(this)));

                    break;

                case EntityPart.Hand:
                    to = paramHand.curParentPosition;
                    paramHand.currentEntityPart = _Entity;
                    SetOrderInLayer(paramHand.currentEntityPart, paramHand.defaultOrderInLayer, 0, paramHand.defaultOrderName);

                    paramHand.targetScale = _Entity.GetTargetScaleKoef(this);
                    StartCoroutine(Scale(paramHand.currentEntityPart.transform, paramHand.targetScale));
                    StartCoroutine(Rotate(paramHand.currentEntityPart, paramHand.currentEntityPart.GetTargetDeltaRotation(this)));
                    paramHand.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    break;

                case EntityPart.Head:
                    to = paramHead.curParentPosition;
                    paramHead.currentEntityPart = _Entity;
                    SetOrderInLayer(paramHead.currentEntityPart, paramHead.defaultOrderInLayer, 6, paramHead.defaultOrderName);

                    //paramHead.targetScale = _Entity.GetTargetScaleKoef(this);
                    //StartCoroutine(Scale(paramHead.currentEntityPart.transform, paramHead.targetScale));
                    StartCoroutine(Rotate(paramHead.currentEntityPart, paramHead.currentEntityPart.GetTargetDeltaRotation(this)));

                    break;

                case EntityPart.Leg:

                    //WantedEntity_leg = _Entity;
                    to = paramLeg.curParentPosition;
                    to2 = paramLeg.curParentPosition2;

                    paramLeg.currentEntityPart = _Entity;
                    //SetOrderInLayer(paramHead.currentEntityPart, paramHead.defaultOrderInLayer, 6, paramHead.defaultOrderName);

                    paramLeg.currentEntityPart.leg1.Find("qq").gameObject.SetActive(false);
                    paramLeg.currentEntityPart.leg2.Find("qq").gameObject.SetActive(false);

                    paramLeg.targetScale = _Entity.GetTargetScaleKoef(this);
                    StartCoroutine(Scale(paramLeg.currentEntityPart.leg1.transform, paramLeg.targetScale));
                    StartCoroutine(Scale(paramLeg.currentEntityPart.leg2.transform, paramLeg.targetScale));

                    paramLeg.currentEntityPart.leg1.transform.localRotation = Quaternion.Euler(Vector2.zero);
                    paramLeg.currentEntityPart.leg2.transform.localRotation = Quaternion.Euler(Vector2.zero);

                    SetOrderInLayer(paramLeg.currentEntityPart.leg1, paramLeg.defaultOrderInLayer, 1, paramLeg.defaultOrderName);
                    SetOrderInLayer(paramLeg.currentEntityPart.leg2, paramLeg.defaultOrderInLayer2, 1, paramLeg.defaultOrderName2);
                    break;

                default:
                    break;
            }

            float tempScale = _Entity.transform.localScale.x / transform.localScale.x;
            //if (_Entity.transform.parent) _Entity.transform.localRotation = _Entity.transform.parent.parent.localRotation;


            _Entity.transform.SetParent(to.transform, false);
            _Entity.transform.localScale = new Vector3(tempScale, tempScale, tempScale);

            switch (_Entity.currentPart)
            {
                case EntityPart.Body:
                    paramBody.targetScale = _Entity.GetTargetScaleKoef(this);

                    paramBody.currentEntityPart.transform.localScale = new Vector3(paramBody.targetScale, paramBody.targetScale, paramBody.targetScale);
                    break;
                case EntityPart.Head:
                    paramHead.targetScale = _Entity.GetTargetScaleKoef(this); Debug.Log("paramHead.targetScale " + paramHead.targetScale);
                    //StartCoroutine(Scale(paramHead.currentEntityPart.transform, paramHead.targetScale));
                    paramHead.currentEntityPart.transform.localScale = new Vector3(paramHead.targetScale, paramHead.targetScale, paramHead.targetScale);
                    paramHead.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                    break;
            }
            if (to2)
            {
                _Entity.leg1.transform.parent = _Entity.transform.parent;
                _Entity.leg1.transform.localScale = new Vector3(1 / transform.localScale.x, 1 / transform.localScale.x, 1 / transform.localScale.x);

                _Entity.leg2.transform.parent = to2;
                _Entity.leg2.transform.localScale = _Entity.transform.localScale;
                StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.leg1, to,5));
                StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.leg2, to2,5));
            }


            //StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.transform, to));

            _Entity.transform.localPosition = Vector3.zero;
            _Entity.transform.position = to.transform.position;

        }

        /*
            public void PlaceCorrectEntityWithoutSave(Entity _Entity)
            {
                Transform to = _Entity.transform;
                Transform to2 = null;


                switch (_Entity.currentPart)
                {
                    case EntityPart.Body:
                        to = paramBody.curParentPosition;
                        paramBody.currentEntityPart = _Entity;
                        SetOrderInLayer(paramBody.currentEntityPart, paramBody.defaultOrderInLayer, 1, paramBody.defaultOrderName);
                        //WantedEntity_body = _Entity;
                        paramBody.curBodyNaked.gameObject.SetActive(true);
                        paramBody.DefaultDress.gameObject.SetActive(false);
                        //Debug.Log(_Entity.transform.localScale.x);
                        paramBody.targetScale = _Entity.GetTargetScaleKoef(this);
                        StartCoroutine(Scale(paramBody.currentEntityPart.transform, paramBody.targetScale));
                        //StartCoroutine(Rotate(paramBody.currentEntityPart, paramBody.currentEntityPart.GetTargetDeltaRotation(this)));
                        paramBody.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                        break;

                    case EntityPart.Hand:
                        to = paramHand.curParentPosition;
                        paramHand.currentEntityPart = _Entity;
                        SetOrderInLayer(paramHand.currentEntityPart, paramHand.defaultOrderInLayer, -1, paramHand.defaultOrderName);

                        paramHand.targetScale = _Entity.GetTargetScaleKoef(this);
                        StartCoroutine(Scale(paramHand.currentEntityPart.transform, paramHand.targetScale));
                        //StartCoroutine(Rotate(paramHand.currentEntityPart, paramHand.currentEntityPart.GetTargetDeltaRotation(this)));
                        paramHand.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                        //WantedEntity_Hand = _Entity;
                        break;

                    case EntityPart.Head:
                        to = paramHead.curParentPosition;
                        paramHead.currentEntityPart = _Entity;
                        SetOrderInLayer(paramHead.currentEntityPart, paramHead.defaultOrderInLayer, 6, paramHead.defaultOrderName);

                        paramHead.targetScale = _Entity.GetTargetScaleKoef(this);
                        StartCoroutine(Scale(paramHead.currentEntityPart.transform, paramHead.targetScale));
                        StartCoroutine(Rotate(paramHead.currentEntityPart, paramHead.currentEntityPart.GetTargetDeltaRotation(this)));
                        paramHead.curParentPosition.transform.localPosition = _Entity.GetTargetDeltaPos(this);

                        //WantedEntity_head = _Entity;
                        break;

                    case EntityPart.Leg:

                        //WantedEntity_leg = _Entity;
                        to = paramLeg.curParentPosition;
                        to2 = paramLeg.curParentPosition2;

                        paramLeg.currentEntityPart = _Entity;
                        //SetOrderInLayer(paramHead.currentEntityPart, paramHead.defaultOrderInLayer, 6, paramHead.defaultOrderName);

                        paramLeg.currentEntityPart.leg1.FindChild("qq").gameObject.SetActive(false);
                        paramLeg.currentEntityPart.leg2.FindChild("qq").gameObject.SetActive(false);

                        paramLeg.targetScale = _Entity.GetTargetScaleKoef(this);
                        StartCoroutine(Scale(paramLeg.currentEntityPart.leg1.transform, paramLeg.targetScale));
                        StartCoroutine(Scale(paramLeg.currentEntityPart.leg2.transform, paramLeg.targetScale));

                        paramLeg.currentEntityPart.leg1.transform.localRotation = Quaternion.Euler(Vector2.zero);
                        paramLeg.currentEntityPart.leg2.transform.localRotation = Quaternion.Euler(Vector2.zero);

                        SetOrderInLayer(paramLeg.currentEntityPart.leg1, paramLeg.defaultOrderInLayer, 1, paramLeg.defaultOrderName);
                        SetOrderInLayer(paramLeg.currentEntityPart.leg2, paramLeg.defaultOrderInLayer2, 1, paramLeg.defaultOrderName2);
                        break;

                    default:
                        break;
                }

                float tempScale = _Entity.transform.localScale.x / transform.localScale.x;
                //if (_Entity.transform.parent) _Entity.transform.localRotation = _Entity.transform.parent.parent.localRotation;


                _Entity.transform.parent = to;
                _Entity.transform.localScale = new Vector3(tempScale, tempScale, tempScale);
                _Entity.transform.localRotation = Quaternion.Euler (_Entity.GetTargetDeltaRotation(this));

                if (to2)
                {
                    _Entity.leg1.transform.parent = _Entity.transform.parent;
                    _Entity.leg1.transform.localScale = new Vector3(1 / transform.localScale.x, 1 / transform.localScale.x, 1 / transform.localScale.x);

                    _Entity.leg2.transform.parent = to2;
                    _Entity.leg2.transform.localScale = _Entity.transform.localScale;
                    StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.leg1, to));
                    StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.leg2, to2));
                }


                StartCoroutine(StaticParams.MoveToCorrectPlace(_Entity.transform, to));
             }

            */

        IEnumerator DisableOldEntity(GameObject _oldEntity, GameObject _bodyNaked, Transform _from, Transform _to)
        {
            yield return new WaitUntil(() => Vector2.Distance(_from.position, _to.position) < 0.007f);
            //_oldEntity.SetActive(false);
            StartCoroutine(StaticParams.LerpColor(_oldEntity, 0, 3));
            _bodyNaked.SetActive(true);
        }

        IEnumerator PlaceEntity(Entity _Entity, Transform _to)
        {

            do
            {
                _Entity.transform.position = Vector2.Lerp(_Entity.transform.position, _to.position, Time.deltaTime * 5);
                yield return new WaitForEndOfFrame();

            } while (Vector2.Distance(_Entity.transform.position, _to.position) > 0.005f);
        }

        IEnumerator Rotate(Entity _Entity, Vector3 _to)
        {
           
            yield return new WaitForEndOfFrame();
            _Entity.transform.rotation = Quaternion.Euler(_to);
        }

        IEnumerator Scale(Transform _transScale, float _to)
        {
            do
            {
                float target = Mathf.Lerp(_transScale.localScale.x, _to, Time.deltaTime * 3);
                _transScale.localScale = new Vector3(
                    target, target, target
                    );
                yield return new WaitForEndOfFrame();
            } while (Mathf.Abs(_transScale.localScale.x - _to) > 0.005f);
        }

        IEnumerator Scale(Transform _transScale, float _to, float _speed)
        {
            do
            {
                float target = Mathf.Lerp(_transScale.localScale.x, _to, Time.deltaTime * _speed);
                _transScale.localScale = new Vector3(
                    target, target, target
                    );
                yield return new WaitForEndOfFrame();
            } while (Mathf.Abs(_transScale.localScale.x - _to) > 0.005f);
        }

        void SetOrderInLayer(Entity go, int _sortingorder, int _delta, string _sortingOrderName)
        {
            Debug.Log("_sortingorder " + _sortingorder);
            SpriteRenderer[] sr = go.GetComponentsInChildren<SpriteRenderer>(true);
            foreach (SpriteRenderer item in sr)
            {
                item.sortingOrder = item.sortingOrder + _sortingorder + _delta;
                item.sortingLayerName = _sortingOrderName;
            }
        }

        void SetOrderInLayer(Transform go, int _sortingorder, int _delta, string _sortingOrderName)
        {
            SpriteRenderer[] sr = go.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer item in sr)
            {
                item.sortingOrder = item.sortingOrder + _sortingorder + _delta;
                item.sortingLayerName = _sortingOrderName;
            }
        }

        void SaveEntity(Entity _Entity)
        {

            PlayerPrefs.SetString(GetComponent<PlayerController>().playerType.ToString() + _Entity.currentPart.ToString(), _Entity.name);
        }

        public void LoadPlayerEntity(bool _body, bool _head, bool _hands, bool _legs)
        {
            LoadDefEntityParams();
            if (_body)
            {

                GameObject res = Resources.Load<GameObject>(
                   "Scene7/Body/" + PlayerPrefs.GetString(GetComponent<PlayerController>().playerType.ToString() + "Body"));

                StartCoroutine(DropLoadedEntity(res));
            }

            if (_head)
            {
                Debug.Log("name   " + PlayerPrefs.GetString(GetComponent<PlayerController>().playerType.ToString() + "Head"));

                GameObject res = Resources.Load<GameObject>(
                   "Scene7/Head/" + PlayerPrefs.GetString(GetComponent<PlayerController>().playerType.ToString() + "Head"));
                //Debug.Log("res  " + res.name);

                StartCoroutine(DropLoadedEntity(res));

            }

            if (_hands)
            {

                GameObject res = Resources.Load<GameObject>(
                   "Scene7/Hand/" + PlayerPrefs.GetString(GetComponent<PlayerController>().playerType.ToString() + "Hand"));

                StartCoroutine(DropLoadedEntity(res));

            }

            if (_legs)
            {
                GameObject res = Resources.Load<GameObject>(
                   "Scene7/Leg/" + PlayerPrefs.GetString(GetComponent<PlayerController>().playerType.ToString() + "Leg"));

                StartCoroutine(DropLoadedEntity(res));

            }



        }
        public IEnumerator DropLoadedEntity(GameObject _res)
        {
            if (_res)
            {
                GameObject go = (GameObject)Instantiate(_res, transform.position, Quaternion.identity);
                go.name = _res.name;
                //go.GetComponent<Entity>().enabled = false;
                yield return new WaitForEndOfFrame();
                PlaceCorrectEntityCLOUD(go.GetComponent<Entity>());
                go.GetComponent<BoxCollider2D>().enabled = false;

            }
            else
            {
                Debug.Log("CANT LOAD ENTITY");
            }

        }

        public IEnumerator DropLoadedEntityCloud(GameObject _res)
        {
            if (_res)
            {
                GameObject go = (GameObject)Instantiate(_res, transform.position, Quaternion.identity);
                go.name = _res.name;
                //go.GetComponent<Entity>().enabled = false;
                yield return new WaitForEndOfFrame();
                PlaceCorrectEntityCLOUD(go.GetComponent<Entity>());
                go.GetComponent<BoxCollider2D>().enabled = false;

            }
            else
            {
                Debug.Log("CANT LOAD ENTITY");
            }

        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log("LoadPlayerEntity");
                LoadPlayerEntity(true, true, true, true);
            }
        }
    }
}
