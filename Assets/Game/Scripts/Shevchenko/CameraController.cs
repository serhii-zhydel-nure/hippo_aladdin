﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{

    public class CameraController : MonoBehaviour
    {
        public enum CameraMovementType
        {
            lerp, towards
        }
        public static CameraController instance;

        public bool moveAllowed;
        bool needFollow;
        Vector2 posTarget;
        Transform trFollowFarget;
        float targetScale;

        public float speedMove = 1;
        public float speedScale = 1;

        public Camera curCamera;
        bool isInMoveNow;
        CameraMovementType currentCameraMovementType = CameraMovementType.lerp;
        public Vector2 deltaFollow;

        void Awake()
        {
            instance = this;
            curCamera = GetComponent<Camera>();
            targetScale = curCamera.orthographicSize;
            Debug.Log("CameraController Awake");
        }

        void LateUpdate()
        {
            if (moveAllowed)
            {
                if (posTarget != null && !needFollow)
                {
                    if (currentCameraMovementType == CameraMovementType.lerp)
                    {
                        transform.position = Vector3.Lerp(transform.position, new Vector3(posTarget.x, posTarget.y, -10), Time.deltaTime * speedMove);
                        curCamera.orthographicSize = Mathf.Lerp(curCamera.orthographicSize, targetScale, Time.deltaTime * speedScale);
                    }
                    else
                    {
                        transform.position = Vector3.MoveTowards(transform.position, new Vector3(posTarget.x, posTarget.y, -10), Time.deltaTime * speedMove);
                        curCamera.orthographicSize = Mathf.MoveTowards(curCamera.orthographicSize, targetScale, Time.deltaTime * speedScale);

                    }

                    isInMoveNow = Vector2.Distance(transform.position, posTarget) < 0.1f ? false : true;


                }
                else
                if (needFollow && trFollowFarget)
                {
                    if (currentCameraMovementType == CameraMovementType.lerp)
                    {
                        transform.position = Vector3.Lerp(transform.position, new Vector3(trFollowFarget.position.x + deltaFollow.x, trFollowFarget.position.y + deltaFollow.y, -10), Time.deltaTime * speedMove);
                        curCamera.orthographicSize = Mathf.Lerp(curCamera.orthographicSize, targetScale, Time.deltaTime * speedScale);

                    }
                    else
                    {
                        transform.position = Vector3.MoveTowards(transform.position, new Vector3(trFollowFarget.position.x + deltaFollow.x, trFollowFarget.position.y + deltaFollow.y, -10), Time.deltaTime * speedMove);
                        curCamera.orthographicSize = Mathf.MoveTowards(curCamera.orthographicSize, targetScale, Time.deltaTime * speedScale);

                    }

                    isInMoveNow = Vector2.Distance(transform.position, trFollowFarget.position) < 0.1f ? false : true;

                }
            }
        }

        public CameraController SetTarget(float _x, float _y)
        {
            posTarget = new Vector2(_x, _y);
            return this;
        }

        public CameraController SetTarget(Vector2 _vec)
        {
            posTarget = _vec;
            return this;
        }

        public CameraController SetTarget(Transform _tr)
        {
            posTarget = _tr.position;
            return this;
        }
        public CameraController SetTarget(Transform _tr, float _deltaY)
        {
            posTarget = new Vector2(_tr.position.x, _tr.position.y + _deltaY);
            return this;
        }
        public CameraController SetTarget(string _targetName)
        {
            GameObject go = GameObject.Find(_targetName);
            if (!go)
            {
                Debug.LogError("НЕ НАЙДЕНА ЦЕЛЬ: " + _targetName);
                return this;
            }
            posTarget = GameObject.Find(_targetName).transform.position;
            return this;
        }
        public CameraController SetScale(float _sc)
        {
            targetScale = _sc;
            return this;
        }
        public CameraController AllowMove(bool _allowed)
        {
            moveAllowed = _allowed;
            return this;
        }
        public CameraController SetSpeedMove(float _speed)
        {
            speedMove = _speed;
            return this;
        }
        public CameraController SetSpeedScale(float _speed)
        {
            speedScale = _speed;
            return this;
        }
        public CameraController SetFollow(Transform _tr)
        {
            deltaFollow = Vector2.zero;
            trFollowFarget = _tr;
            needFollow = true;
            return this;
        }
        public CameraController SetFollow(string _tr)
        {
            deltaFollow = Vector2.zero;
            GameObject go = GameObject.Find(_tr);
            if (!go)
            {
                Debug.LogError("Не НАЙДЕНА ТОЧКА ЗА КОТОРОЙ СЛЕДИТЬ");
                return this;
            }
            trFollowFarget = go.transform;
            needFollow = true;
            return this;
        }
        public CameraController SetFollow(Transform _tr, float _deltaY)
        {
            deltaFollow = new Vector2(0, _deltaY);
            trFollowFarget = _tr;
            needFollow = true;
            return this;
        }
        public CameraController SetFollow(Transform _tr, Vector2 _delta)
        {
            deltaFollow = _delta;
            trFollowFarget = _tr;
            needFollow = true;
            return this;
        }
        public void StopFollow()
        {
            posTarget = trFollowFarget.transform.position;
            needFollow = false;
        }
        public CameraController SetDelta(Vector2 _delta)
        {
            deltaFollow = _delta;
            return this;
        }
        public bool IsCameraInMoveNow()
        {
            return isInMoveNow;
        }

        public CameraController SetCameraMovementType(CameraMovementType _CameraMovementType)
        {
            currentCameraMovementType = _CameraMovementType;
            return this;
        }
    }
}