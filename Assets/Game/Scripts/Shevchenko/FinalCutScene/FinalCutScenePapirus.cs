﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class FinalCutScenePapirus : MonoBehaviour {
        public static FinalCutScenePapirus instance;
        Animator anim;

        private void Awake()
        {
            instance = this;
            anim = GetComponent<Animator>();
        }
        public void SetAppear()
        {
            anim.SetTrigger("Appear");
        }
    }
}
