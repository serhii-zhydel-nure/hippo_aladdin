﻿using UnityEngine;
using System.Collections;

public class FinalCutSceneFinger : MonoBehaviour
{
    public static FinalCutSceneFinger instance;
    public bool isEnabled;

    private void Awake()
    {
        instance = this;
        transform.position = new Vector2(-1000, -1000);
    }
 public void EnableDisable(bool _isEnabled)
    {
        isEnabled = _isEnabled;
        if (!_isEnabled)
        {
            transform.position = new Vector2(-1000, -1000);

        }
    }
    void Update()
    {
        if (isEnabled)
        {
            if (Input.GetMouseButtonDown(0))
            {
                transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            if (Input.GetMouseButton(0))
            {
                transform.position = Vector2.Lerp(transform.position, 
                    (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition), Time.deltaTime * 5);
            }
            if (Input.GetMouseButtonUp(0))
            {
                transform.position = new Vector2(-1000, -1000);

            }
        }
    }
}
