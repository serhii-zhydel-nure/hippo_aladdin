﻿using UnityEngine;
using System.Collections;
using PSV_Tutorials;
using DG.Tweening;
using PSV;

namespace Shevchenko
{
    public class FinalCutScene : GameController
    {
        public GameObject srcFirework;

        FingerTutorial finger;
        bool isSmallGame;
        int swipeCounter;
        float progressBarValue;
        float pbarStep = .1f;

        bool lastDirWasRight, firstSwipeDone;
        Vector2 lastPos, delta;
        float holdTimer;

        private void Update()
        {
            if (isSmallGame)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    lastPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                    SFX.PlayLoop("Вытирает что-то");
                }
                if (Input.GetMouseButton(0))
                {

                    delta = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition) - lastPos;
                    lastPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

                    if (delta.magnitude != 0)
                    {

                        Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                        if (hits != null)
                        {
                            foreach (var item in hits)
                            {
                                if (item.collider.name == "Lamp")
                                {
                                    finger.Stop();
                                    holdTimer += Time.deltaTime;
                                    LampEffect.instance.SetScale(holdTimer * 2);
                                    if (holdTimer > 4)
                                    {
                                        isSmallGame = false;
                                        LampEffect.instance.LauchSecondSmoke();
                                    }
                                    if (!SFX.IsSFXPlayingNow("появляется джин"))
                                    {
                                        SFX.Play("появляется джин");
                                    }
                                }
                            }
                        }
                    }
                }
                if (Input.GetMouseButtonUp(0))
                {
                    SFX.StopLoop("Вытирает что-то");

                }
            }
        }


        public override IEnumerator Process()
        {
            StaticParams.SetColor(ProgressBarBase.instance.gameObject, 0);
            SFX.PlayLoop("Спят");
            pepa.SetSleep(true, true);
            george.WalkStop(WalkDirection.left);
             george.SetSleep(true, true);
            mama.Walk("WayPosMama (1)");
            yield return new WaitWhile(() => mama.IsWalkingNow());
            //Вот вы где! Просыпайтесь, пора обедать. 		mh-2
            yield return new WaitWhile(() => mama.Talk("mh-2"));
            //Дети просыпаются, зевают.
            pepa.SetSleep(false);
            SFX.StopLoop("Спят");

            yield return new WaitForSeconds(.5f);
            george.SetSleep(false);

            while (true)
            {
                if (pepa.animPlayer.GetAnimatorTransitionInfo(0).IsName("sleep_stop_ -> idle"))
                    break;
                yield return null;
            }

            pepa.SetRastopirka();
            yield return new WaitForSeconds(.1f);
            george.SetRastopirka();
            yield return new WaitForSeconds(2f);


            //ГИППИ - Неужели это был лишь сон? 
            yield return new WaitWhile(() => pepa.Talk("mh-3"));
            //Джи вдруг извлекает из коробки лампу. Все в удивлении толпятся возле него.
            george.WalkStop(WalkDirection.left);
            //george.SpadeWork();
            george.Walk("GeorgePos (1)");
            yield return new WaitWhile(() => george.IsWalkingNow());
            george.SetInBox(true);
            yield return new WaitForSeconds(1f);
            george.SetInBox(false);
            SFX.Play("Звук вылета элементов панели, фруктов с корзины");
            yield return StartCoroutine(StaticParams.MoveToCorrectPlace(GameObject.Find("Lamp").transform,
                GameObject.Find("LampPos").transform, 5));
            george.Walk("GeorgePos (2)");
            //george.WalkStop(WalkDirection.right);

            //Реплика-подсказка:
            //ГИППИ -  Потри лампу. hp-76
            yield return new WaitWhile(() => pepa.Talk("hp-76"));


            // мини игра
            ieFingerHelper = IeFingerHelper();
            StartCoroutine(ieFingerHelper);
            FinalCutSceneFinger.instance.EnableDisable(true);
            isSmallGame = true;
            //StartCoroutine(StaticParams.LerpColorUI(ProgressBarBase.instance.gameObject, 1, 1));
            yield return new WaitWhile(() => isSmallGame);

            StopCoroutine(ieFingerHelper);
            finger.Stop();
            FinalCutSceneFinger.instance.EnableDisable(false);
            foreach (var item in list_AllPlayers)
            {
                item.SetTriggerEmotion(EmotionTriggerType.Joy);
            }
            StartCoroutine(StaticParams.LerpColorUI(ProgressBarBase.instance.gameObject, 0, 1));

            //StaticParams.PushEffectFireworks();
            GameObject go = Instantiate(srcFirework);
            go.transform.position = Camera.main.ScreenToWorldPoint
                            (
                            new Vector3(Screen.width * 0.5f, Screen.height * .9f, 10)
                            );
            SFX.Play("Фейерверк");
            Transform tFinalPosPapirus = GameObject.Find("FinalPosPapirus").transform;
            //StartCoroutine(StaticParams.MoveToCorrectPlace(FinalCutScenePapirus.instance.transform, tFinalPosPapirus));
            //StartCoroutine(StaticParams.Rotate(FinalCutScenePapirus.instance.transform, tFinalPosPapirus));
            //StartCoroutine(StaticParams.Scale(FinalCutScenePapirus.instance.transform, tFinalPosPapirus));
            FinalCutScenePapirus.instance.transform.MoveTo(tFinalPosPapirus);
            FinalCutScenePapirus.instance.transform.Scale(tFinalPosPapirus);
            FinalCutScenePapirus.instance.transform.Rotate(tFinalPosPapirus);

            yield return new WaitForSeconds(2);
            FinalCutScenePapirus.instance.SetAppear();

            MapProgressController.SetSceneCompleted();
    
                SceneLoader.SwitchToScene(Scenes.Map);
 
        }
        IEnumerator ieFingerHelper;
        IEnumerator IeFingerHelper()
        {
            FingerManager.Init();															   //

            Transform[] array = new Transform[2] { GameObject.Find("FingerPos (1)").transform, GameObject.Find("FingerPos (2)").transform };
            finger =
                            new FingerTutorial(
                            new Drag(array, .8f).SetLoops(4, LoopType.Yoyo)   //loops the gesture wih Yoyo type.
                                     ).SetLoops(500);
            while (true)
            {
                finger.Play();
                yield return new WaitForSeconds(5);
            }
        }
    }
}
