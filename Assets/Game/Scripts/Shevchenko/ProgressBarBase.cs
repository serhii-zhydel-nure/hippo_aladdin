﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace Shevchenko
{
    public class ProgressBarBase : MonoBehaviour {

    public static ProgressBarBase instance;
    Image ProgressbarObj;
    public virtual  void Awake()
    {
        instance = this;
        foreach (var item in GetComponentsInChildren<Transform>(true))
        {
            if (item.name == "ProgressbarObj")
            {
                ProgressbarObj = item.GetComponent<Image>();
            }
        }
    }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_val">от 0 до 1</param>
        public virtual void SetProgress(float _val)
    {
        ProgressbarObj.rectTransform.localScale = new Vector3(_val, 1, 1);
    }
    }
}
