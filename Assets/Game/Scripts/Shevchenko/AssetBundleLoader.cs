﻿using System;
using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class AssetBundleLoader : MonoBehaviour
    {

        void Start()
        {

            // Clear Cache
            //Caching.CleanCache();

            //string loadUrl = "file://d:\\ONE";
            string loadUrl = "http://prazdnikskazka.kh.ua/" + Application.systemLanguage;


            //loadUrl = "http://prazdnikskazka.kh.ua/" + "Portuguese";


#if UNITY_ANDROID && !UNITY_EDITOR
            loadUrl += "_android.unity3d";
#elif UNITY_IPHONE && !UNITY_EDITOR
            loadUrl += ".iphone.unity3d";
#else
            loadUrl += "_android.unity3d";
#endif

            StartCoroutine(load(loadUrl, 1));
        }

        // Update is called once per frame
        void Update()
        {
            // progress
            //int percent = (int)(www.progress * 100);
            //guitext.text = percent.ToString() + "%";
        }

        private WWW www;

        private IEnumerator load(string url, int version)
        {
            // wait for the caching system to be ready
            while (!Caching.ready)
                yield return null;

            // load AssetBundle file from Cache if it exists with the same version or download and store it in the cache
            www = WWW.LoadFromCacheOrDownload(url, version);
            yield return www;

            Debug.Log("Loaded ");

            if (www.error != null)
                throw new Exception("WWW download had an error: " + www.error);

            AssetBundle assetBundle = www.assetBundle;
            assetBundle.LoadAllAssets();
            //Instantiate(assetBundle.mainAsset); // Instantiate(assetBundle.Load("AssetName"));

            //foreach (var item in assetBundle.AllAssetNames())
            //{
            //    Debug.Log(item);
            //}

            SoundEffectsController.list_sound_effects.Clear();

            foreach (AudioClip item in assetBundle.LoadAllAssets<AudioClip>())
            {
                SoundEffectsController.list_sound_effects.Add(item);
            }

            // Unload the AssetBundles compressed contents to conserve memory
            assetBundle.Unload(false);
            Destroy(gameObject);
        }

        void OnGUI()
        {
            int percent = (int)(www.progress * 100);

            GUI.Label(new Rect(50, 50, 50, 50), percent.ToString());



        }

    }
}