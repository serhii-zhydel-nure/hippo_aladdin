﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PSV;
using Shevchenko.Map;
using PSV.IAP;

namespace Shevchenko
{
    public class MapItem_Chargeable : MonoBehaviour
    {

        public bool isFreeUsed;
        public bool isPurchased;
        public Products product = Products.SKU_ADMOB;
        MapItem curMapItem;
        GameObject goLocker;

        private void Awake()
        {
            curMapItem = GetComponent<MapItem>() ;

            //isFreeUsed = PlayerPrefs.GetInt("isFreeUsed"+ curScene.ToString()) == 0 ? false : true;
            isFreeUsed = true;
            isPurchased = PlayerPrefs.GetInt("isPurchased") == 0 ? false : true;

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "spriteLocker")
                {
                    goLocker = item.gameObject;
                }
            }
            if (!isFreeUsed || isPurchased)
            {
                EnableDisableLocker(false);
            }
        }
        private void Start()
        {
            if (isPurchased )
            {
                curMapItem.SetMeUnlocked();
            }
        }
        private void OnEnable()
        {
            BillingManager.OnPurchaseSucceded += OnPurchaseSucceded;
            BillingManager.OnPurchaseCancelled += OnPurchaseCancelled;

        }
        private void OnDisable()
        {
            BillingManager.OnPurchaseSucceded -= OnPurchaseSucceded;
            BillingManager.OnPurchaseCancelled -= OnPurchaseCancelled;

        }


        private void OnPurchaseSucceded(ProductProperties prop)
        {
            if (prop.product == product && prop.type != ProductType.Consumable)
            {
                PlayerPrefs.SetInt("isPurchased", 1);
                isPurchased = true;
                EnableDisableLocker(false);

            }
        }

        private void OnPurchaseCancelled(ProductProperties prop)
        {
            if (prop.product == product && prop.type != ProductType.Consumable)
            {
            }
        }

        public void SetFreeUsed()
        {
            PlayerPrefs.SetInt("isFreeUsed" + curMapItem.curScene.ToString(), 1);
            //EnableDisableLocker(true);

        }
        void EnableDisableLocker(bool _enable)
        {
            goLocker.SetActive(_enable);
        }
    }
}
