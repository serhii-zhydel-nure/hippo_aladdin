﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.Map
{
    public class MapPapirusRoll : MonoBehaviour
    {
        public static MapPapirusRoll instance;

        public float deltaX;
        Vector2 defPos, targetPos;
        Animator anim;

        private void Awake()
        {
            instance = this;
            defPos = transform.position;
            anim = GetComponent<Animator>();
        }
        public void SetDefPos()
        {
            transform.position = defPos;
        }
        
        public void SetDoMove(Vector2 _targetPos)
        {
            targetPos = new Vector2(_targetPos.x + deltaX, transform.position.y);
            if (ieMover != null) StopCoroutine(ieMover);

            ieMover = IeMove();
            StartCoroutine(ieMover);
        }
        IEnumerator ieMover;
        IEnumerator IeMove()
        {
            anim.SetBool("roll", true);
            while (Vector2.Distance(transform.position, targetPos) > .5f)
            {
                transform.position = Vector2.Lerp(transform.position, targetPos, Time.deltaTime);
                MapScrollArea.instance.SetScale(Vector2.Distance(defPos, transform.position));

                yield return null;
            }
            anim.SetBool("roll", false);

        }

    }
}
