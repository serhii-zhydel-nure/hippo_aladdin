﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko.Map
{
    public class MapMask : MonoBehaviour
    {

        public Material matBase;
        public Material matChild;
        public Material matDefault;

        public SpriteRenderer spriteMaskBase;
        public List<GameObject> list_MaskedChilds = new List<GameObject>();

        private void Awake()
        {
             matBase = Resources.Load<Material>("SpriteMask/MaskSprite_BASE");
            matChild = Resources.Load<Material>("SpriteMask/MaskSprite_CHILD");
            matDefault = Resources.Load<Material>("SpriteMask/DefaultShaaderMat");
            //InitMask();

            foreach (var item in list_MaskedChilds)
            {

                foreach (var item2 in item.GetComponentsInChildren<SpriteRenderer>(true))
                {
                    item2.material = matChild;
                }
            }

         }
      

    }
}
