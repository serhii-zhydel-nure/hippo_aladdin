﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV;
using UnityEngine.EventSystems;
using PSV.IAP;
using PSV.PurchaseDialogue;

namespace Shevchenko.Map
{
    public class MapController : MonoBehaviour
    {
        public Products product =  Products.SKU_ADMOB;

        public List<MapItem> list_MapItemsQueue = new List<MapItem>();
        bool isSelectDone;
        MapItem lastSelectedMapItem;

        private void Start()
        {
            for (int i = 0; i < list_MapItemsQueue.Count; i++)
            {
                if (i == 0 && !list_MapItemsQueue[i].isCompleted)// если это первый и он не пройден то он следующий
                {
                    list_MapItemsQueue[i].SetMeNext();
                    list_MapItemsQueue[i].SetMeUnlocked();
                    break;
                 }
                // следующий
                if (list_MapItemsQueue[i].isCompleted && i != list_MapItemsQueue.Count - 1 && !list_MapItemsQueue[i + 1].isCompleted)
                {
                    list_MapItemsQueue[i + 1].SetMeNext();
                    list_MapItemsQueue[i + 1].SetMeUnlocked();
                    break;
                }

            }
            StartCoroutine(Process());
        }
        IEnumerator Process()
        {
            yield return new WaitForSeconds(1);
            SFX.Play("открытие уровня на карте2");
            for (int i = 0; i < list_MapItemsQueue.Count; i++)
            {
                if (list_MapItemsQueue[i].isNext)
                {
                   MapPapirusRoll.instance.SetDoMove(list_MapItemsQueue[i].transform.position);
                    CameraObserve.instance.SetTarget(list_MapItemsQueue[i].transform, 2);
                    break;
                }
                else if (i == list_MapItemsQueue.Count - 1 && list_MapItemsQueue[i].isUnlocked)
                {
                    MapPapirusRoll.instance.SetDoMove(list_MapItemsQueue[i].transform.position);
                    CameraObserve.instance.SetTarget(list_MapItemsQueue[i].transform, 2);

                    break;

                }
            }
            //CameraObserve.instance.SetTarget(MapPapirusRoll.instance.transform,2)
            //    .SetScriptMoveDelta(-MapPapirusRoll.instance.deltaX,0);
            yield return new WaitForSeconds(.5f);

            DoWay();

        }
        private void OnEnable()
        {
            BillingManager.OnPurchaseSucceded += OnPurchaseSucceded;
            Swipe.OnMouseClickUp += Swipe_OnMouseClickUp;
        }

        private void OnDisable()
        {
            BillingManager.OnPurchaseSucceded -= OnPurchaseSucceded;
            Swipe.OnMouseClickUp -= Swipe_OnMouseClickUp;

        }
        private void Swipe_OnMouseClickUp(Vector3 pos)
        {
            if (!isSelectDone)
            {

                if (!IsPointerOverUIObject())
                {
                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                    if (hits != null)
                    {
                        foreach (var item in hits)
                        {
                            if (item.collider.GetComponent<MapItem>())
                            {
                                MapItem mi = item.collider.GetComponent<MapItem>();
                                if (mi.GetComponent<MapItem_Chargeable>())
                                {
                                    MapItem_Chargeable mic = mi.GetComponent<MapItem_Chargeable>();
                                    if (!mic.isPurchased)
                                    {
                                        StartPurchase(mi);
                                    }
                                }
                                if (mi.isUnlocked)
                                {
                                    StartLoadLevel(mi);
                                }
                            }
                        }

                    }
                }
            }
        }
         private void OnPurchaseSucceded(ProductProperties prop)
        {
            if (prop.product == product && prop.type != ProductType.Consumable  )
            {
                StartLoadLevel(lastSelectedMapItem);
            }
        }
   
        void StartLoadLevel(MapItem mi)
        {
            StartCoroutine(IeStarLoadLevel(mi));
        }
        IEnumerator IeStarLoadLevel(MapItem mi)
        {
            isSelectDone = true;
            yield return null;
            MapProgressController.SetScenePlayingNow(mi.curScene);

                 SceneLoader.SwitchToScene(mi.curScene);
 
        }
        void StartPurchase(MapItem mi)
        {
            lastSelectedMapItem = mi;
            FindObjectOfType<PurchaseDialogueUI>().OpenDialogue();
        }

        void DoWay()
        {
            if (ieWay != null)
            {
                StopCoroutine(ieWay);
            }
            ieWay = IeWay();
            StartCoroutine(ieWay);
        }
        IEnumerator ieWay;

        IEnumerator IeWay()
        {
            foreach (var item in list_MapItemsQueue)
            {
                if (item.isUnlocked && !item.isNext)
                {
                    yield return StartCoroutine(item.IeWayToMe(.05f));
                }
                else if (item.isUnlocked && item.isNext)
                {
                    yield return StartCoroutine(item.IeWayToMe(.1f));

                }
            }
        }



#if DEBUG || UNITY_EDITOR
        private void OnGUI()
        {
            if (GUI.Button(new Rect(Screen.width * .01f, Screen.height * .1f, Screen.height * .1f, Screen.height * .1f), "UnlockAll"))
            {
                MapPapirusRoll.instance.SetDefPos();

                foreach (var item in FindObjectsOfType<MapItem>())
                {
                    if (item.GetComponent<MapItem_Chargeable>())
                    {
                        MapItem_Chargeable mic = item.GetComponent<MapItem_Chargeable>();
                        mic.isPurchased = true;

                    }
                    item.isNext = false;
                    item.isUnlocked = true;
                    item.ResetWayToMe();
                }
                StartCoroutine(Process());

            }
        }

#endif
        private bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }


}
