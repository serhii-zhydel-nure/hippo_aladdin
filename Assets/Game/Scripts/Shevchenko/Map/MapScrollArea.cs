﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.Map
{
    public class MapScrollArea : MonoBehaviour
    {
        public static MapScrollArea instance;
        public float defScaleX;
        public float deltaFactor = 1;
        private void Awake()
        {
            instance = this;
            defScaleX = transform.localScale.x;
        }
        public void SetScale(float scaleFactor)
        {
            if (scaleFactor>0)
            {
                transform.localScale = new Vector2(defScaleX + scaleFactor * deltaFactor, transform.localScale.y);

            }
        }
    }
}
