﻿using UnityEngine;
using System.Collections;
using PSV;

public class MapProgressController : MonoBehaviour
{


    public static Scenes scenePlayingNow = Scenes.EmptySceneMode;

    public static void SetSceneCompleted()
    {
        PlayerPrefs.SetInt(scenePlayingNow.ToString() + "_completed", 1);
    }
    public static void SetScenePlayingNow(Scenes _Scene)
    {
        Debug.Log("scenePlayingNow "+ _Scene);
        scenePlayingNow = _Scene;

    }
    public static bool CheckIsLevelCompleted(Scenes _Scene)
    {
        if (PlayerPrefs.GetInt(_Scene.ToString() + "_completed") == 1)
        {
            return true;
        }
        else return false;
    }
}
