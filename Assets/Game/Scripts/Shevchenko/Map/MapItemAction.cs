﻿using UnityEngine;
using System.Collections;

namespace Shevchenko.Map
{
    public class MapItemAction : MonoBehaviour
    {
        public bool isDontDisableOnStart;
        Transform tr2D_Star_01;

        public enum ActionType
        {
            lvl1_Paint, jeorgeJoyAndDelay, lvl4_Snake, jeoFakeSpecRun,
            lvl6_Kover, lvl7_KoverFly, lvl8_run, lvl9_chest, lvl10
        }
        public ActionType curActionType;

        PlayerController pepa, george;

        Transform ObjUnlocked, ObjLocked;

        private void Awake()
        {
            foreach (var item in GetComponentsInChildren<PlayerController>())
            {
                if (item.playerType == PlayerType.pEpa)
                    pepa = item;
                else if (item.playerType == PlayerType.george)
                    george = item;
            }

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "ObjUnlocked")
                {
                    ObjUnlocked = item;
                }
                else if (item.name == "ObjLocked")
                {
                    ObjLocked = item;
                }
                else if (item.name == "2D_Star_01")
                {
                    tr2D_Star_01 = item;
                }
            }
            //SetEnable();
        }
        private void Start()
        {
            switch (curActionType)
            {
                case ActionType.lvl1_Paint:

                    StartCoroutine(lvl1_Paint());
                    break;
                case ActionType.jeorgeJoyAndDelay:
                    StartCoroutine(jeorgeJoyAndDelay());
                    break;
                case ActionType.lvl4_Snake:
                    StartCoroutine(lvl4_Snake());
                    break;
                case ActionType.jeoFakeSpecRun:
                    george.WalkSetWalkAnimType(WalkAnimType.SpecRun);

                    george.SetFakeWalk(true);
                    break;
                case ActionType.lvl7_KoverFly:
                    StartCoroutine(lvl7_KoverFly());
                    break;
                case ActionType.lvl8_run:
                    StartCoroutine(lvl8_run());
                    break;
                case ActionType.lvl9_chest:
                    StartCoroutine(lvl9_chest());
                    break;
                default:
                    break;
            }
            if (GetComponent<MapItem_Chargeable>())
                SetEnable();
            else
            {
                if (!isDontDisableOnStart)
                {
                    SetDisableImmediate();

                }
            }

        }
        IEnumerator lvl1_Paint()
        {
            while (true)
            {
                pepa.SetPaint(true);
                yield return new WaitForSeconds(1);

            }
        }
        private IEnumerator lvl9_chest()
        {
            //Animator ChestOpen = null;

            //foreach (var item in GetComponentsInChildren<Animator>(true))
            //{
            //    if (item.name == "ChestOpen (1)")
            //    {
            //        ChestOpen = item;
            //    }
            //}
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(2f, 3f));
                //ChestOpen.speed = 1;
                // ChestOpen.Play("ChestOpening");

                //yield return new WaitForSeconds(Random.Range(2f, 3f));
                //ChestOpen.speed = -1;
                //ChestOpen.Play("ChestOpening");
            }
        }

        IEnumerator jeorgeJoyAndDelay()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(2f, 4f));
                if (ObjUnlocked.gameObject.activeInHierarchy)
                    george.SetTriggerEmotion(EmotionTriggerType.Joy);

            }
        }
        IEnumerator lvl4_Snake()
        {
            while (true)
            {
                if (ObjUnlocked.gameObject.activeInHierarchy)

                    nScene4.Scene4_Snake.instance.SetShowHide(true);
                yield return new WaitForSeconds(Random.Range(2f, 3f));
                if (ObjUnlocked.gameObject.activeInHierarchy)
                    nScene4.Scene4_Snake.instance.SetShowHide(false);
                yield return new WaitForSeconds(Random.Range(2f, 3f));

            }
        }
        IEnumerator lvl7_KoverFly()
        {

            while (true)
            {
                george.SetKoverSit();
                pepa.SetKoverSit();

                yield return new WaitForSeconds(Random.Range(2f, 3f));
                if (ObjUnlocked.gameObject.activeInHierarchy)
                {
                    george.SetKoverHit();
                    pepa.SetKoverHit();
                }


            }
        }
        IEnumerator lvl8_run()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(2f, 3f));
                if (ObjUnlocked.gameObject.activeInHierarchy)
                    george.SetScare();
            }
        }
        public void SetEnable()
        {
            ObjUnlocked.gameObject.SetActive(true);

            StartCoroutine(StaticParams.LerpColor(ObjUnlocked.gameObject, 1));
            StartCoroutine(StaticParams.LerpColor(ObjLocked.gameObject, 0));
        }
        public void SetNotNext()
        {

            foreach (var item in ObjUnlocked.GetComponentsInChildren<Animator>(true))
            {
                item.enabled = false;
            }
        }
        public void SetEnableNext()
        {
            tr2D_Star_01.gameObject.SetActive(true);
        }
        public void SetDisableImmediate()
        {
            StaticParams.SetColor(ObjUnlocked.gameObject, 0);
            StaticParams.SetColor(ObjLocked.gameObject, 1);
            ObjUnlocked.gameObject.SetActive(false);
        }

    }
}
