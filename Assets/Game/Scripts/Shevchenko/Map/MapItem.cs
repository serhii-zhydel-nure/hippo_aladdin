﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PSV;

namespace Shevchenko.Map
{
    public class MapItem : MonoBehaviour
    {
        public Scenes curScene;
        public bool isUnlocked;
        public bool isCompleted;
        public bool isNext;
        List<Transform> list_Way = new List<Transform>();
        MapItemAction curMapItemAction;

        private void Awake()
        {
            curMapItemAction = GetComponent<MapItemAction>();

            if (MapProgressController.CheckIsLevelCompleted(curScene))
            {
                isUnlocked = true;
                isCompleted = true;
            }

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name.Contains("WayPoint"))
                {
                    list_Way.Add(item);
                }
            }
            list_Way.Sort(delegate (Transform us1, Transform us2)
          { return us1.name.GetNumberFromName().CompareTo(us2.name.GetNumberFromName()); });

            ResetWayToMe();
          
        }
        public void SetMeUnlocked()
        {
            isUnlocked = true;

        }
        public void SetMeNext()
        {
            isNext = true;
        }
        public void ResetWayToMe()
        {
            foreach (var item in list_Way)
            {
                StaticParams.SetColor(item.gameObject, 0);
            }
        }
        public IEnumerator IeWayToMe(float _delay)
        {
            foreach (var item in list_Way)
            {
                StartCoroutine(StaticParams.LerpColor(item.gameObject, 1));
                yield return new WaitForSeconds(_delay);
            }
            curMapItemAction.SetEnable();

            if (!isNext)
            {
                curMapItemAction.SetNotNext();
            }else
            {
                curMapItemAction.SetEnableNext();
            }
        }
    }
}
