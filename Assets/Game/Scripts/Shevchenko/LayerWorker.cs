﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
    public class LayerWorker : MonoBehaviour
    {
        public bool ignore;
        public bool lower;
        public bool upper;
        /// <summary>
        /// Чем больше значение, тем дальше
        /// </summary>
        public int lowerUpperPosition;

        public List<LayerWorker> list_MyChilds = new List<LayerWorker>();
        public List<LayerWorker> list_MyParents = new List<LayerWorker>();
        public bool iAmChild;

        [System.Serializable]
        public class OrdersLayerParams
        {
            public int defaultOrderInLayer;
            public Renderer currentRenderer;
            public string defaultSortingLayerName;
        }
        public List<OrdersLayerParams> list_defaultOrderParams = new List<OrdersLayerParams>();

        bool isAllowedChangeLayer = true;

        [Header("Нужно ли пересортировать дефолтные слои от ноля")]
        public bool isNeedZeroResort;
        void Start()
        {
            foreach (Renderer item in GetComponentsInChildren<Renderer>(true))
            {
                OrdersLayerParams op = new OrdersLayerParams();
                op.currentRenderer = item;
                op.defaultOrderInLayer = item.sortingOrder;
                op.defaultSortingLayerName = item.sortingLayerName;
                list_defaultOrderParams.Add(op);
            }
            if (isNeedZeroResort)
            {
                RebuildDefaultOrder();

            }
            LayerController.instance.AddNewJustInstWorker(this);
        }
        /// <summary>
        /// пройдётся по всем слоям и выстроит их в порядке от 0
        /// </summary>
        void RebuildDefaultOrder()
        {
            list_defaultOrderParams.Sort(delegate (OrdersLayerParams us1, OrdersLayerParams us2)
            { return us1.defaultOrderInLayer.CompareTo(us2.defaultOrderInLayer); });

            int curLayer = 0;
            int prevLayer = 0;
            for (int i = 0; i < list_defaultOrderParams.Count; i++)
            {
                if (i == 0)
                {
                    prevLayer = list_defaultOrderParams[i].defaultOrderInLayer;
                    list_defaultOrderParams[i].defaultOrderInLayer = curLayer;
                }
                else
                {
                    if (prevLayer != list_defaultOrderParams[i].defaultOrderInLayer)
                    {
                        curLayer++;
                    }
                    prevLayer = list_defaultOrderParams[i].defaultOrderInLayer;
                     list_defaultOrderParams[i].defaultOrderInLayer = curLayer;

                }
            }
        }
        public void ReturnDefault__OrderInLayer()
        {
            foreach (var item in list_defaultOrderParams)
            {
                item.currentRenderer.sortingOrder = item.defaultOrderInLayer;
            }
        }

        public void SetTemp__OrderInLayer(int _layerStep)
        {
            if (isAllowedChangeLayer)
            {
                foreach (var item in list_defaultOrderParams)
                {
                    if (item != null)
                        if (item.currentRenderer != null) item.currentRenderer.sortingOrder = item.defaultOrderInLayer + _layerStep;
                }
            }
        }

        public void CheckForNewItems()
        {
            List<OrdersLayerParams> temp = new List<OrdersLayerParams>();

            foreach (Renderer item in GetComponentsInChildren<Renderer>(true))
            {
                bool isNeedAdd = true;
                foreach (var item2 in list_defaultOrderParams)
                {
                    if (item2.currentRenderer == item)
                    {
                        isNeedAdd = false;
                    }
                }
                if (isNeedAdd)
                {
                    OrdersLayerParams op = new OrdersLayerParams();
                    op.currentRenderer = item;
                    op.defaultOrderInLayer = item.sortingOrder;
                    op.defaultSortingLayerName = item.sortingLayerName;
                    temp.Add(op);
                }

            }
            foreach (var item in temp)
            {
                list_defaultOrderParams.Add(item);
            }
            if (isNeedZeroResort)
            {
                RebuildDefaultOrder();

            }
        }

        //public void ReturnDefault__SortingLayer()
        //{
        //    foreach (var item in list_defaultOrderParams)
        //    {
        //        item.currentSprite.sortingLayerName = item.defaultSortingLayerName;
        //    }
        //}

        //public void SetTemp__SortingLayer()
        //{
        //         foreach (var item in list_defaultOrderParams)
        //        {
        //            item.currentSprite.sortingLayerName = dragSortingLayerName;
        //        }
        //}
    }
}
