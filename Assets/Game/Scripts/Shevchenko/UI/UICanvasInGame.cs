﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using PSV;

public class UICanvasInGame : MonoBehaviour
{
    private void Awake()
    {

        foreach (var item in GetComponentsInChildren<Transform>(true))
        {
            if (item.name == "ButtonMenu")
            {
                item.GetComponent<Button>().onClick.AddListener(() => { StartCoroutine(IeButtonMenu()); });
            }
        }

    }

    IEnumerator IeButtonMenu()
    {
        yield return new WaitForSeconds(.2f);
       
            SceneLoader.SwitchToScene(Scenes.Map);
 
    }

}
