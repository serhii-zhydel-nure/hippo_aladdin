﻿using UnityEngine;
using System.Collections;

public interface IMassovka
{

    IEnumerator Process();
}
