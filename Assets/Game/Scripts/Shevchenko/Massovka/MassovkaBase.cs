﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko;

public class MassovkaBase : MonoBehaviour, IMassovka
{

    public PlayerController pc_1;
    public PlayerController pc_2;

    List<PlayerController> list_AllPc;

    public virtual  void Awake()
    {
        list_AllPc = new List<PlayerController>(FindObjectsOfType<PlayerController>());


    }
    public virtual void Start()
    {
        StartCoroutine(Process());
    }

    public virtual IEnumerator Process()
    {
        yield return null;
    }
    public virtual PlayerController FindNearestPlayer(PlayerController _curPc)
    {
        PlayerController targ = list_AllPc[0] == _curPc? list_AllPc[1]: list_AllPc[0];
        foreach (var item in list_AllPc)
        {
            if (item!= _curPc)
            {
                if (Vector2.Distance(_curPc.transform.position, item.transform.position)<Vector2.Distance(_curPc.transform.position, targ.transform.position))
                {
                    targ = item;
                }
            }
        }
        return targ;
    }
}
