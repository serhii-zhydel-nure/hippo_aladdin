﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko;

public class Massovka_6 : MassovkaBase
{
    public delegate void PointReached(Transform _point, PlayerController _pc);
    public static event PointReached OnPointReached;

    [Header("ходит по рандомным точкам, остановившись базарит")]
    public string desc;

    List<Transform> list_Way = new List<Transform>();

    public override void Awake()
    {
        base.Awake();

        foreach (var item in GetComponentsInChildren<Transform>(true))
        {
            if (item.name.Contains("MasWayPos"))
            {
                list_Way.Add(item);
            }
        }

        list_Way.Sort(delegate (Transform us1, Transform us2)
              { return StaticParams.GetNumberFromName(us1.name).CompareTo(StaticParams.GetNumberFromName(us2.name)); });
        point = list_Way.RandomOne();

    }


    public override IEnumerator Process()
    {
        yield return null;
        IeCur = Part1();
        StartCoroutine(IeCur);
    }
    IEnumerator IeCur;
    Transform point;
    IEnumerator Part1()
    {
        while (true)
        {
            for (int i = 0; i < list_Way.Count; i++)
            {
                point = list_Way.Except(point).RandomOne();
                pc_1.Walk(point.position);
                yield return new WaitWhile(() => pc_1.IsWalkingNow());
                pc_1.WalkStop(WalkDirection.inverse);
                if (OnPointReached != null)
                {
                    OnPointReached(point, pc_1);
                }
                pc_1.SetFakeTalkAnim(true);
                yield return new WaitForSeconds(Random.Range(2f, 3.5f));
                pc_1.SetFakeTalkAnim(false);

            }
            yield return null;

        }
    }

}
