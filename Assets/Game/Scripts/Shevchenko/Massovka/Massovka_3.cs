﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko;

public class Massovka_3 : MassovkaBase
{
    [Header("Тусуется за прилавком")]
    public string desc;

    List<Transform> list_Way = new List<Transform>();

    public List<Transform> list_ReactPoints = new List<Transform>();

    public override void Awake()
    {
        base.Awake();

        foreach (var item in GetComponentsInChildren<Transform>(true))
        {
            if (item.name.Contains("MasWayPos"))
            {
                list_Way.Add(item);
            }
        }

        list_Way.Sort(delegate (Transform us1, Transform us2)
              { return StaticParams.GetNumberFromName(us1.name).CompareTo(StaticParams.GetNumberFromName(us2.name)); });
    }
    private void OnEnable()
    {
        Massovka_6.OnPointReached += Massovka_6_OnPointReached;
    }

 
    private void OnDisable()
    {
        Massovka_6.OnPointReached -= Massovka_6_OnPointReached;

    }
    private void Massovka_6_OnPointReached(Transform _point,PlayerController _pc)
    {
        if (list_ReactPoints.Contains(_point))
        {
            if (ieCur != null)
                StopCoroutine(ieCur);
            ieCur = IeTalk(_pc);
            StartCoroutine(ieCur);

        }
    }

    public override IEnumerator Process()
    {
        yield return null;
        ieCur = IeWalk();
        StartCoroutine(ieCur);
    }
    IEnumerator ieCur;

    public IEnumerator IeWalk()
    {

        while (true)
        {
            for (int i = 0; i < list_Way.Count; i++)
            {
                pc_1.Walk(list_Way[i].position);
                yield return new WaitWhile(() => pc_1.IsWalkingNow());
                pc_1.WalkStop(WalkDirection.inverse);

                yield return new WaitForSeconds(Random.Range(2f, 3.5f));
            }
            yield return null;

        }
    }
    IEnumerator IeTalk(PlayerController _pcTarg)
    {
        pc_1.SetEnableDisableEyesControl(true, _pcTarg.transform);
        _pcTarg.SetEnableDisableEyesControl(true, pc_1.transform);
        yield return new WaitForSeconds(Random.Range(0.5f, 1f));

        pc_1.WalkStop(WalkDirection.none);
        pc_1.SetFakeTalkAnim(true);
        yield return new WaitForSeconds(Random.Range(1.5f, 2.5f));
        pc_1.SetFakeTalkAnim(false);

        ieCur = IeWalk();
        StartCoroutine(ieCur);
        pc_1.SetEnableDisableEyesControl(false   );
        _pcTarg.SetEnableDisableEyesControl(false );

    }
}
