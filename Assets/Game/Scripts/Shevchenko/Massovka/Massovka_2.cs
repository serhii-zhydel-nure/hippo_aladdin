﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko;

public class Massovka_2 : MassovkaBase
{
    [Header("Циклично ходит по точкам")]
    public string desc;

    public List<Transform> list_Way = new List<Transform>();

    public override void Awake()
    {
        base.Awake();

        foreach (var item in GetComponentsInChildren<Transform>(true))
        {
            if (item.name.Contains("MasWayPos"))
            {
                list_Way.Add(item);
            }
        }

        list_Way.Sort(delegate (Transform us1, Transform us2)
              { return StaticParams.GetNumberFromName(us1.name).CompareTo(StaticParams.GetNumberFromName(us2.name)); });

    }
    public override IEnumerator Process()
    {
        while (true)
        {
            for (int i = 0; i < list_Way.Count; i++)
            {
                pc_1.Walk(list_Way[i].position);
                yield return new WaitWhile(()=>pc_1.IsWalkingNow());
                if (i== 9)
                {
                    pc_1.WalkStop(WalkDirection.inverse);

                }
                pc_1.SetEnableDisableEyesControl(true, FindNearestPlayer(pc_1).transform);

                yield return new WaitForSeconds(Random.Range(2f, 3.5f));
                pc_1.SetEnableDisableEyesControl(false);

            }
            yield return null;

        }
    }
}
