﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko;

public class Massovka_5 : MassovkaBase
{
    [Header("подойдёт по событию и будет зрителем")]
    public string desc;

    List<Transform> list_Way = new List<Transform>();

    public override void Awake()
    {
        base.Awake();

        foreach (var item in GetComponentsInChildren<Transform>(true))
        {
            if (item.name.Contains("MasWayPos"))
            {
                list_Way.Add(item);
            }
        }

        list_Way.Sort(delegate (Transform us1, Transform us2)
              { return StaticParams.GetNumberFromName(us1.name).CompareTo(StaticParams.GetNumberFromName(us2.name)); });
     }
    private void OnEnable()
    {
        Scene4.OnSmallGameStarted += Scene4_OnSmallGameStarted;

    }
    private void OnDisable()
    {
        Scene4.OnSmallGameStarted -= Scene4_OnSmallGameStarted;
    }

    private void Scene4_OnSmallGameStarted()
    {
        //StopCoroutine(IeCur);
        IeCur = Part2();
        StartCoroutine(IeCur);
    }

    public override IEnumerator Process()
    {
        yield return null;
        //IeCur = Part1();
        //StartCoroutine(IeCur);
    }
    IEnumerator IeCur;
     IEnumerator Part2()
    {
        pc_1.WalkStop(WalkDirection.none);

        pc_1.SetFakeTalkAnim(false);
        pc_1.Walk(transform.Find("MasOtherWayPos (3)").position);

        yield return new WaitWhile(() => pc_1.IsWalkingNow());
        pc_1.WalkStop(WalkDirection.left);

        while (true)
        {
            pc_1.SetApplause();
            yield return new WaitForSeconds(Random.Range( 1.5f,2f));

        }
    }
}
