﻿using UnityEngine;
using System.Collections;
using Shevchenko;

public class MassovkaOne : MassovkaBase
{
    [Header("Просто тоят два типо на месте и базарят")]
    public string desc;

    public override void Awake()
    {
        base.Awake();
    }
    public override IEnumerator Process()
    {
        pc_1.SetEnableDisableEyesControl(true, pc_2.transform);
        pc_2.SetEnableDisableEyesControl(true, pc_1.transform);

         while (true)
        {
            pc_1.SetFakeTalkAnim(true);
            yield return new WaitForSeconds(Random.Range(1f, 2.5f));
            pc_1.SetFakeTalkAnim(false);

            pc_2.SetFakeTalkAnim(true);
            yield return new WaitForSeconds(Random.Range(1f, 2.5f));
            pc_2.SetFakeTalkAnim(false);

            pc_1.SetFakeTalkAnim(true);
            yield return new WaitForSeconds(Random.Range(1f, 2.5f));
            pc_1.SetFakeTalkAnim(false);

            pc_2.SetReverance();
            yield return new WaitForSeconds(2);

            yield return null;

        }
    }
}
