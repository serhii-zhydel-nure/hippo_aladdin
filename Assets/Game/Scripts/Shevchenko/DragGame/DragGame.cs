﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Shevchenko
{
    public enum DragGameType
    {
        getOnlyOneByName, getAnyByCompareNames, getAnyAndHardSetTargetParent,
        hardSetChildrenAndParents,
        /// <summary>
        /// тут рекомендуется жёстко задать родителей куда тащить у детеныша
        /// </summary>
        hardSetChildrenParentFromChildList
    }
    public enum ClickChildType
    {
        oneClick, twoPointClick,
        /// <summary>
        /// только для тяжёлого - драг и двойной клик
        /// </summary>
        dragAndDoubleClick
    }

    public class DragGame : MonoBehaviour
    {
        [System.Serializable]
        public class DragDebug
        {
            public GameObject from, to;
        }
        public static DragGame instance;

        public Camera curCamera;
        public bool isDebug;
        public DragDebug[] mas_DragDebug;
        public delegate void DropCorrect(DragGame_Child _child, DragGame_Parent _parent);
        public static event DropCorrect OnDropCorrect;
        public static event DropCorrect OnDropWrong;
        public static event DropCorrect OnMoveToCorrectPlaceCompleted;

        public delegate void DropCorrectClick(DragGame_Child _child);
        public static event DropCorrectClick OnDropCorrectClick;

        public delegate void DropWrong();
        public static event DropWrong OnDropWrongNoPar;

        public delegate void DropWrongOnePar(DragGame_Child _child);
        public static event DropWrongOnePar OnDropWrongOnePar;
        public static event DropWrongOnePar OnDropWrongAndMoveBackCompleted;

        public delegate void DragUpdate(DragGame_Child _child);
        public static event DragUpdate OnDragUpdate;

        public delegate void DropStartedOnePar(DragGame_Child _child);
        public static DropStartedOnePar OnDragStartedOnePar;
        public static event DropStartedOnePar OnEasyFirstClickDone;

        public delegate void GameFinished();
        public static event GameFinished OnGameFinished;

        public delegate void delHelper();
        public static event delHelper OnHelperFirst;
        public static event delHelper OnHelperSecond;

        [Header("Если лист определен использовать ТОЛЬКО родителей из листа как целевые")]
        public List<DragGame_Parent> list_TargetParents = new List<DragGame_Parent>();
        [Header("Многократное использование любого родителя")]
        public bool isAllowMultipleParentUse;

        [Header("Если лист определен использовать ТОЛЬКО ДЕТИШЕК из листа как целевые")]
        public List<DragGame_Child> list_targetChilds = new List<DragGame_Child>();

        PlayerController pepaController_base;
        public DragGameType currentDragGameType;

        public bool isEasy;
        public bool game = false;
        public DragGame_Child hit
        {
            get { return HIT; }
            set
            {
                HIT = value;
                //Debug.Log("----hit "   + new System.Diagnostics.StackTrace());
            }
        }
        public DragGame_Child hitDoubleClick;
        public DragGame_Child HIT;
        public bool pressed;
        public int partsLeft;
        bool isGetByName;
        string nameChild;
        float helperTimer;
        float delayClick;

        void Awake()
        {
            instance = this;
            //isEasy = PlayerPrefs.GetInt("isEasy") == 0 ? true : false;
            isEasy = StaticParams.currentGameDifficulty == GameDifficulty.easy ? true : false;
#if UNITY_EDITOR

#else
      
#endif
            if (!curCamera)
            {
                curCamera = Camera.main;
            }
        }
        void Start()
        {
            if (isDebug)
            {
                OverrideLevelParams(100, DragGameType.getAnyByCompareNames);
                foreach (var item in mas_DragDebug)
                {
                    if (!item.from.GetComponent<DragGame_Child>())
                    {
                        item.from.AddComponent<DragGame_Child>();
                    }
                    if (!item.to.GetComponent<DragGame_Parent>())
                    {
                        item.to.AddComponent<DragGame_Parent>();
                    }
                }
                isEasy = false;
                StartGame();
            }
        }

        void StartGame()
        {
            game = true;
            RunHelper();
        }
        /// <summary>
        /// только для getAnyByCompareNames
        /// </summary>
        /// <param name="_partsLeft"></param>
        /// <param name="_dgt"></param>
        public virtual void OverrideLevelParams(int _partsLeft, DragGameType _dgt = DragGameType.getAnyByCompareNames)
        {
            currentDragGameType = _dgt;

            switch (_dgt)
            {

                case DragGameType.getAnyByCompareNames:
                    partsLeft = _partsLeft;
                    nameChild = "";
                    isGetByName = false;
                    list_targetChilds = null;
                    list_TargetParents = null;
                    break;
                default:
                    Debug.LogError("Неправильно заданы параметры драгалки");

                    break;
            }
            StartGame();
        }
        /// <summary>
        /// только для getOnlyOneByName
        /// </summary>
        /// <param name="_partsLeft"></param>
        /// <param name="_dgt"></param>
        /// <param name="_nameChild"></param>
        public virtual void OverrideLevelParams(int _partsLeft, DragGameType _dgt, string _nameChild)
        {
            currentDragGameType = _dgt;

            switch (_dgt)
            {

                case DragGameType.getOnlyOneByName:
                    partsLeft = _partsLeft;
                    nameChild = _nameChild;
                    isGetByName = true;
                    list_targetChilds = null;
                    list_TargetParents = null;

                    break;
                case DragGameType.getAnyByCompareNames:
                    partsLeft = _partsLeft;
                    nameChild = "";
                    isGetByName = false;
                    Debug.LogError("INCORRECT SET PARAMS! NEED -- DragGameType.getOnlyOneByName");
                    break;
                default:
                    Debug.LogError("Неправильно заданы параметры драгалки");
                    break;
            }
            StartGame();
        }
        /// <summary>
        /// только getAnyAndHardSetTargetParent - можно тащить любого детишку, родители берутся из общего списка  
        /// </summary>
        /// <param name="_partsLeft"></param>
        /// <param name="_list_TargetParents"></param>
        /// <param name="_dgt"></param>
        public virtual void OverrideLevelParams(int _partsLeft, List<DragGame_Parent> _list_TargetParents, DragGameType _dgt = DragGameType.getAnyAndHardSetTargetParent)
        {
            currentDragGameType = _dgt;

            switch (_dgt)
            {

                case DragGameType.getAnyAndHardSetTargetParent:
                    partsLeft = _partsLeft;
                    nameChild = "";
                    isGetByName = false;
                    list_TargetParents = new List<DragGame_Parent>(_list_TargetParents);
                    break;
                default:
                    Debug.LogError("Неправильно задан старт драгалки");
                    break;
            }
            StartGame();
        }
        /// <summary>
        /// только hardSetChildrenAndParents - детишки и родители только из общего списка
        /// </summary>
        /// <param name="_partsLeft"></param>
        /// <param name="_list_TargetChildrens"></param>
        /// <param name="_list_TargetParents"></param>
        /// <param name="_dgt"></param>
        public virtual void OverrideLevelParams(int _partsLeft, List<DragGame_Child> _list_TargetChildrens, List<DragGame_Parent> _list_TargetParents, DragGameType _dgt = DragGameType.hardSetChildrenAndParents)
        {
            currentDragGameType = _dgt;
            switch (_dgt)
            {

                case DragGameType.hardSetChildrenAndParents:
                    partsLeft = _partsLeft;
                    nameChild = "";
                    isGetByName = false;
                    list_TargetParents = new List<DragGame_Parent>(_list_TargetParents);
                    list_targetChilds = new List<DragGame_Child>(_list_TargetChildrens);
                    break;
                default:
                    Debug.LogError("Неправильно задан старт драгалки");
                    break;
            }
            StartGame();
        }
        /// <summary>
        /// только hardSetChildrenParentFromChildList - детишки из общего списка, родителей нужно присвоить детишкам 
        /// </summary>
        /// <param name="_partsLeft"></param>
        /// <param name="_list_TargetChildrens"></param>
        /// <param name="_dgt"></param>
        public virtual void OverrideLevelParams(int _partsLeft, List<DragGame_Child> _list_TargetChildrens, DragGameType _dgt = DragGameType.hardSetChildrenParentFromChildList)
        {
            currentDragGameType = _dgt;
            switch (_dgt)
            {

                case DragGameType.hardSetChildrenParentFromChildList:
                    partsLeft = _partsLeft;
                    nameChild = "";
                    isGetByName = false;
                    list_targetChilds = new List<DragGame_Child>(_list_TargetChildrens);
                    if (list_targetChilds.Count == 0)
                    {
                        Debug.LogError("Ошибка! Лист пустой!");

                    }
                    list_TargetParents = new List<DragGame_Parent>();
                    break;
                default:
                    Debug.LogError("Неправильно задан старт драгалки");
                    break;
            }
            StartGame();
        }

   public virtual     void Update()
        {
            if (isDebug)
            {
                UpdateDrag();
            }
            if (game)
            {
                delayClick += Time.deltaTime;
                if (partsLeft > 0)
                {
                    UpdateDrag();
                    UpdateClick();
                    UpdateDoubleClick();
                }
                else if (partsLeft <= 0)
                {
                    game = false;
                    if (OnGameFinished != null)
                    {
                        OnGameFinished();
                    }
                }
            }
        }

        float delayTimer;
        bool isDelayReady;

        ///////////////////////////////////////////////////////// Common methods
        public void ManualMouseDown()
        {
            //Debug.Log(">>>ManualMouseDown() -->" + new System.Diagnostics.StackTrace());
            if (game && delayClick > .2f)
            {
                delayClick = 0;
                Vector3 clickPosition = curCamera.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);

                if (hits != null)
                {
                    foreach (var item in hits)
                    {
                        if (item.collider.GetComponent<Button>())
                        {

                            Debug.Log("Нажали на UI кнопку, прерываем draggame");
                            return;
                        }
                    }
                    foreach (RaycastHit2D item in hits)
                    {

                        if (item.collider)
                            if (item.collider.GetComponent<DragGame_Child>())
                            {
                                hit = item.collider.GetComponent<DragGame_Child>();
                                DragGame_Child dgc = hit;

                                if (!dgc.busyBack && !pressed
                                     && dgc.currentState != DragGame_Child_State.dropCorrect
                                     && dgc.currentState != DragGame_Child_State.drag)
                                {
                                    if (CheckIsClickCorrect(dgc)) // проверяем правильно ли мы нажали
                                    {
                                        if (!isEasy)
                                        {
                                            //if (dgc.curDragChildType == ClickChildType.dragAndDoubleClick)
                                            //{
                                            //    Debug.Log("dgc.curDragChildType = " + ClickChildType.dragAndDoubleClick);
                                            //    DoubleClick_First(dgc);
                                            //}
                                            pressed = true;

                                            dgc = dgc.DragStart();
                                            hit = dgc;

                                            dgc.SetState(DragGame_Child_State.drag, null);
                                            if (dgc.ieCurrentMover != null) StopCoroutine(hit.GetComponent<DragGame_Child>().ieCurrentMover);
                                            if (OnDragStartedOnePar != null) OnDragStartedOnePar(dgc);

                                            Debug.Log("CheckIsClickCorrect", dgc);
                                            return;
                                        }
                                        else // для лёгкого нажатия
                                        {
                                            if (dgc.curDragChildType == ClickChildType.oneClick)
                                            {
                                                hit = dgc;

                                                if (!EasyDoSingleClick(dgc))
                                                {
                                                    if (OnDropWrongNoPar != null) OnDropWrongNoPar();
                                                    if (OnDropWrongOnePar != null)
                                                    {
                                                        if (hit) OnDropWrongOnePar(hit.GetComponent<DragGame_Child>());
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                DoubleClick_First(dgc);

                                            }
                                            break;
                                        }

                                    }
                                    else hit = null;
                                }
                                else hit = null;
                            }
                            else
                            {

                            }
                    }
                    CheckHitOnSucessObj(hits);

                }
            }
        }

        DragGame_Child RayCastChild(Vector2 pos)
        {
            RaycastHit2D[] hits = Physics2D.RaycastAll(pos, Vector2.zero);

            if (hits != null)
            {
                foreach (RaycastHit2D item in hits)
                {
                    if (item.collider.GetComponent<DragGame_Child>())
                    {
                        return item.collider.GetComponent<DragGame_Child>();
                    }
                }
            }
            return null;
        }
        ///////////////////////////////////////////////////////// DRAG

        public void UpdateDrag()
        {
            if (!isEasy)
            {
                HelperUpdate();


                DragMouseDown();
                DragMouse();
                DragMouseUp();
            }

        }

        public void DragMouseDown()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 clickPosition = curCamera.ScreenToWorldPoint(Input.mousePosition);
                // сперва проверяем случай если на панели залазит на нижний слой
                DragGame_Child item = RayCastChild(clickPosition);

                if (item)
                {
                    if (item.isItemOnPanel)
                    {
                        return;
                    }

                    if (!item.isItemOnPanel)
                    {
                        //Debug.Log("DragGame -> HardMouseDown ", item);
                        ManualMouseDown();

                    }
                }
            }
        }
        /// <summary>
        /// используется   для ручного вызова нажатия - нужно для скроллов
        /// и в апдейте харда
        /// </summary>
        void CheckHitOnSucessObj(RaycastHit2D[] hits)
        {
            if (!hitDoubleClick)
            {

                // проверяем есть ли тут успешно брошенные
                foreach (var item in hits)
                {
                    if (item.collider.GetComponent<DragGame_Child>())
                    {
                        DragGame_Child dgc = item.collider.GetComponent<DragGame_Child>();
                        if (dgc.currentState == DragGame_Child_State.dropCorrect)
                        {
                            if (OnDropCorrectClick != null)
                                OnDropCorrectClick(dgc);
                            return;
                        }

                    }
                }
            }
        }

        void DragMouse()
        {
            if (Input.GetMouseButton(0))
            {
                if (pressed)
                {
                    if (hit)
                    {
                        hit.transform.position = Vector2.Lerp(hit.transform.position, GetDeltaPos(), Time.deltaTime * 5);
                        if (OnDragUpdate != null)
                        {
                            OnDragUpdate(hit);
                        }
                    }
                }
            }
        }
        void DragMouseUp()
        {
            RaycastHit2D hit2 = new RaycastHit2D();
            if (Input.GetMouseButtonUp(0))
            {
                //if (hit)
                //{

                //    if (hit.curDragChildType == ClickChildType.dragAndDoubleClick)
                //    {
                //        if (DoubleClickMoseDown_Second())
                //        {
                //            Debug.Log("DoubleClickMoseDown_Second");
                //            return;
                //        }
                //    }
                //}
                pressed = false;
                bool isDropSuccess = false;

                //Vector3 clickPosition = curCamera.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(GetDeltaPos(), Vector2.zero);

                if (hits != null)
                {
                    foreach (var item in hits)
                    {
                        hit2 = item;
                        if (hit)
                            if (hit2.transform.GetComponent<DragGame_Parent>())
                            {
                                DragGame_Parent dgp = hit2.collider.GetComponent<DragGame_Parent>();
                                DragGame_Child dgc = hit.GetComponent<DragGame_Child>();

                                if (CheckIsDropCorrect(dgp, hit) && dgc.currentState == DragGame_Child_State.drag)
                                {
                                    Debug.Log("CheckIsDropCorrect", dgp);
                                    isDropSuccess = true;
                                    DoMoveToCorrectPlace(dgc, dgp);

                                    partsLeft--;

                                    dgc.SetState(DragGame_Child_State.dropCorrect, dgp);
                                    dgp.DropCorrect(dgc);
                                    if (OnDropCorrect != null)
                                    {
                                        OnDropCorrect(dgc, dgp);
                                    }
                                    hit.transform.parent = hit2.transform.parent;

                                    hit = null;
                                    RunHelper();
                                    break;
                                }
                                else { }
                            }
                    }
                }


                if (!isDropSuccess && hit)
                {
                    DragGame_Child dgc = hit;

                    if (!dgc.placed)
                    {
                        dgc.SetState(DragGame_Child_State.dropWrong, null);
                        if (dgc.ieCurrentMover != null) StopCoroutine(hit.GetComponent<DragGame_Child>().ieCurrentMover);
                        dgc.ieCurrentMover = MoveBack(hit.transform);
                        StartCoroutine(dgc.ieCurrentMover);

                        hit.GetComponent<BoxCollider2D>().enabled = true;
                        if (OnDropWrongOnePar != null) OnDropWrongOnePar(dgc);
                        hit = null;
                    }
                    else
                    {
                        if (OnDropWrong != null) OnDropWrong(dgc, hit2.transform.GetComponent<DragGame_Parent>());
                        hit = null;
                    }
                }
            }
        }

        Vector2 GetDeltaPos()
        {
            if (hit)
            {
                if (hit.vDeltaDragPos != Vector2.zero)
                {
                    return ((Vector2)curCamera.ScreenToWorldPoint(Input.mousePosition)) + hit.vDeltaDragPos;
                }
            }
            return curCamera.ScreenToWorldPoint(Input.mousePosition);

        }


        ///////////////////////////////////////////////////////// DOUBLE CLICK
        Vector3 startClickPos;

        private void UpdateDoubleClick()
        {
            DoubleClickMoseDown();
            DoubleClickMouseUp();
        }
        void DoubleClickMoseDown()
        {
            if (Input.GetMouseButtonDown(0))
            {
                startClickPos = curCamera.ScreenToWorldPoint(Input.mousePosition);
                //Debug.Log("startClickPos " + startClickPos + " " + new System.Diagnostics.StackTrace());

            }
        }
        void DoubleClickMouseUp()
        {
            if (Input.GetMouseButtonUp(0))
            {
                #region ТОЛЬКО для ClickChildType.dragAndDoubleClick
                // сперва проверим есть ли в месте нажатия parent
                // 
                RaycastHit2D[] hits = Physics2D.RaycastAll(curCamera.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                if (hits != null)
                {
                    if (hitDoubleClick)
                    {
                        foreach (RaycastHit2D item2 in hits)
                        {
                            if (item2.collider.GetComponent<DragGame_Parent>() && hitDoubleClick.curDragChildType == ClickChildType.dragAndDoubleClick)
                            {

                                if (CheckIsDropCorrect(item2.collider.GetComponent<DragGame_Parent>(), hitDoubleClick))
                                {
                                    DoubleClickMouseDown_Second();
                                    return;
                                }

                            }
                        }
                    }
                }

                if (Vector2.Distance(startClickPos, curCamera.ScreenToWorldPoint(Input.mousePosition)) < .5f)
                {
                    // делаем выбор
                    DragGame_Child item = RayCastChild(curCamera.ScreenToWorldPoint(Input.mousePosition));

                    if (item)
                    {
                        if (item.curDragChildType == ClickChildType.dragAndDoubleClick)
                        {
                            if (CheckIsClickCorrect(item) && !item.placed)
                            {
                                DoubleClick_First(item);
                            }
                            else
                            {
                                DisableActiveChildHelpers();
                            }
                        }
                    }
                    else
                    {
                        DisableActiveChildHelpers();
                    }
                }
                #endregion
                if (isEasy)
                {


                    if (Vector2.Distance(startClickPos, curCamera.ScreenToWorldPoint(Input.mousePosition)) < .5f)
                    {

                        // отрабатываем второй клик
                        //Debug.Log("сперва отрабатываем второй клик");
                        //if (CameraObserve.instance)
                        //{
                        //    if (hitDoubleClick && !CameraObserve.instance.IsScrollingNow)
                        //    {
                        //        if (DoubleClickMouseDown_Second())
                        //        {
                        //            return;
                        //        }
                        //    }
                        //}
                        //else
                        if (hitDoubleClick)
                        {
                            if (DoubleClickMouseDown_Second())
                            {
                                return;
                            }
                        }
                        /////////////////////////////// отрабатываем первый клик
                        foreach (RaycastHit2D item in hits)
                        {
                            if (item.collider)
                                if (item.collider.GetComponent<DragGame_Child>())
                                {
                                    DragGame_Child dgc = item.collider.GetComponent<DragGame_Child>();

                                    if (!dgc.busyBack && !pressed && dgc.currentState != DragGame_Child_State.dropCorrect)
                                    {
                                        if (CheckIsClickCorrect(dgc)) // проверяем правильно ли мы нажали
                                        {
                                            if (IeNullHit != null) StopCoroutine(IeNullHit);

                                            hit = item.collider.GetComponent<DragGame_Child>();

                                            if (!dgc.isItemOnPanel)
                                            {
                                                if (dgc.curDragChildType == ClickChildType.twoPointClick)
                                                {
                                                    hit = dgc;
                                                    DoubleClick_First(dgc);

                                                }
                                            }
                                            break;

                                        }
                                        else hit = null;
                                    }
                                    else hit = null;
                                }
                                else { }
                        }
                    }
                }
            }
        }
        void DoubleClick_First(DragGame_Child dgc)
        {
            hit = dgc;
            hitDoubleClick = dgc;
            hitDoubleClick = dgc;
            //Debug.Log("_____ SELECT = " + dgc.name + " " + new System.Diagnostics.StackTrace(), dgc.gameObject);
            DisableActiveChildHelpers();
            dgc.EnableDisableHelper(true);
            dgc.StartStopIEHelper(true);

            if (OnEasyFirstClickDone != null)
            {
                OnEasyFirstClickDone(dgc);
            }
        }

        bool DoubleClickMouseDown_Second()
        {

            if (game)
            {
                Vector3 clickPosition = curCamera.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                if (hitDoubleClick) // если уже было на что-то нажато
                {
                    if (hitDoubleClick.curDragChildType == ClickChildType.twoPointClick
                        || hitDoubleClick.curDragChildType == ClickChildType.dragAndDoubleClick)
                        if (!IsPointerOverUIObject())
                            //Debug.Log(Vector2.Distance(startClickPos, curCamera.ScreenToWorldPoint(Input.mousePosition)) + " " + new System.Diagnostics.StackTrace());

                            //if (Vector2.Distance(startClickPos, curCamera.ScreenToWorldPoint(Input.mousePosition)) < .5f)

                            foreach (RaycastHit2D item in hits)
                            {
                                if (item.collider)
                                {
                                    if (item.collider.GetComponent<DragGame_Parent>())
                                    {
                                        DragGame_Parent dgp = item.collider.GetComponent<DragGame_Parent>();

                                        if (CheckIsDropCorrect(dgp, hitDoubleClick) && dgp.enabled)
                                        {
                                            EasyMakeSucessfullDrop(hitDoubleClick, dgp);


                                            // обнуляем клик с задержкой
                                            if (hitDoubleClick.isMultipleUse)
                                            {
                                                DoNullHit(0);

                                            }
                                            else
                                                DoNullHit();
                                            //hit = null;    
                                            return true;
                                        }
                                    }
                                }
                            }
                }
            }
            return false;
        }
        IEnumerator IeNullHit;
        IEnumerator NullHit(float _delay = 3)
        {

            yield return new WaitForSeconds(_delay);
            if (!list_DisallowCancelHit.Contains(hitDoubleClick))
            {
                hit = null;
                hitDoubleClick = null;
                DisableActiveChildHelpers();

            }
        }

        void DoNullHit(float _delay = 3)
        {
            //Debug.Log("DoNullHit");
            if (IeNullHit != null) StopCoroutine(IeNullHit);
            IeNullHit = NullHit(_delay);
            StartCoroutine(IeNullHit);
        }
        List<DragGame_Child> list_DisallowCancelHit = new List<DragGame_Child>();
        public void SetDisallowCancelHit(List<DragGame_Child> _list_DisallowCancelHit)
        {
            list_DisallowCancelHit = new List<DragGame_Child>(_list_DisallowCancelHit);
        }
        ///////////////////////////////////////////////////////// CLICK
        #region EASY
        public virtual void UpdateClick()
        {
            if (isEasy)
            {
                HelperUpdate();

                if (Input.GetMouseButtonDown(0))
                {
                    //if (delayClick > .2f)
                    //{
                    //    delayClick = 0;
                    SingleClickMouseDown();

                    //}
                }
            }
        }
        /*
        public void EasyMouseDown()
        {
            if (game)
            {
                Vector3 clickPosition = curCamera.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);

                if (hits != null)
                {                    
                    // сперва отрабатываем второй клик

                    if (DoubleClickMoseDown_Second())
                    {
                        return;
                    }
                    // сперва отрабатываем второй клик
                    //if (hit) // если уже было на что-то нажато
                    //{
                        //if (hit.curDragChildType == ClickChildType.twoPointClick)
                        //    foreach (RaycastHit2D item in hits)
                        //    {
                        //        if (item.collider)
                        //        {
                        //            if (item.collider.GetComponent<DragGame_Parent>())
                        //            {
                        //                DragGame_Parent dgp = item.collider.GetComponent<DragGame_Parent>();


                        //                if (CheckIsDropCorrect(dgp, hit) && dgp.enabled)
                        //                {
                        //                    EasyMakeSucessfullDrop(hit, dgp);


                        //                    // обнуляем клик с задержкой
                        //                    DoNullHit();
                        //                    //hit = null;    
                        //                    return;
                        //                }
                        //            }
                        //        }
                        //    }
                    //}
                    /////////////////////////////// отрабатываем первый клик
                    foreach (RaycastHit2D item in hits)
                    {
                        if (item.collider)
                            if (item.collider.GetComponent<DragGame_Child>())
                            {
                                DragGame_Child dgc = item.collider.GetComponent<DragGame_Child>();

                                if (!dgc.busyBack && !pressed && dgc.currentState != DragGame_Child_State.dropCorrect)
                                {
                                    if (CheckIsClickCorrect(dgc)) // проверяем правильно ли мы нажали
                                    {
                                        if (IeNullHit != null) StopCoroutine(IeNullHit);

                                        hit = item.collider.GetComponent<DragGame_Child>();

                                        if (!dgc.isItemOnPanel)
                                        {
                                            if (dgc.curDragChildType == ClickChildType.oneClick)
                                            {
                                                hit = dgc;

                                                if (!EasyDoSingleClick(dgc))
                                                {
                                                    if (OnDropWrongNoPar != null) OnDropWrongNoPar();
                                                    if (OnDropWrongOnePar != null)
                                                    {
                                                        if (hit)
                                                            OnDropWrongOnePar(hit.GetComponent<DragGame_Child>());
                                                    }
                                                }

                                            }
                                            else // dgc.curDragChildType == ClickChildType.twoPointClick
                                            {
                                                EasyFirstClickDone(dgc);

                                            }

                                        }
                                        break;

                                    }
                                    else hit = null;
                                }
                                else hit = null;
                            }
                            else { }
                    }
                }
            }
        }
        */
        public void SingleClickMouseDown()
        {
            if (game)
            {
                Vector3 clickPosition = curCamera.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                foreach (var item in hits)
                {
                    if (item.collider.GetComponent<Button>())
                    {

                        Debug.Log("Нажали на UI кнопку, прерываем draggame");
                        return;
                    }
                }
                if (hits != null)
                {
                    //// сперва отрабатываем второй клик

                    //Debug.Log("сперва отрабатываем второй клик");
                    //if (DoubleClickMouseDown_Second())
                    //{
                    //    return;
                    //}

                    /////////////////////////////// отрабатываем первый клик
                    foreach (RaycastHit2D item in hits)
                    {
                        if (item.collider)
                            if (item.collider.GetComponent<DragGame_Child>())
                            {
                                DragGame_Child dgc = item.collider.GetComponent<DragGame_Child>();

                                if (!dgc.busyBack && !pressed && dgc.currentState != DragGame_Child_State.dropCorrect)
                                {
                                    if (CheckIsClickCorrect(dgc)) // проверяем правильно ли мы нажали
                                    {
                                        if (IeNullHit != null) StopCoroutine(IeNullHit);

                                        hit = item.collider.GetComponent<DragGame_Child>();

                                        if (!dgc.isItemOnPanel)
                                        {
                                            if (dgc.curDragChildType == ClickChildType.oneClick)
                                            {
                                                hit = dgc;

                                                if (!EasyDoSingleClick(dgc))
                                                {
                                                    if (OnDropWrongNoPar != null) OnDropWrongNoPar();
                                                    if (OnDropWrongOnePar != null)
                                                    {
                                                        if (hit)
                                                            OnDropWrongOnePar(hit.GetComponent<DragGame_Child>());
                                                    }
                                                }

                                            }
                                            else // dgc.curDragChildType == ClickChildType.twoPointClick
                                            {
                                                //DoubleClick_First(dgc);

                                            }

                                        }
                                        break;

                                    }
                                    else hit = null;
                                }
                                else hit = null;
                            }
                            else { }
                    }

                }
            }
        }
        bool EasyDoSingleClick(DragGame_Child dgc)
        {
            DragGame_Parent hit2 = null;
            bool isDropCorrect = false;
            foreach (DragGame_Parent item2 in dgc.list_TargetParents.Shuffle())
            {
                hit2 = item2;
                DragGame_Parent dgp = item2;

                if (CheckIsDropCorrect(item2, dgc) && dgp.enabled)
                {
                    // проверяем не занят ли родитель
                    if (isAllowMultipleParentUse || dgp.isAllowMultipleUse || !hit2.transform.GetComponent<DragGame_Parent>().isParentTaken)
                    {
                        EasyMakeSucessfullDrop(dgc, dgp);
                        isDropCorrect = true;
                        break;
                    }
                }
            }

            return isDropCorrect;
        }
        void EasyMakeSucessfullDrop(DragGame_Child dgc, DragGame_Parent dgp)
        {
            hit = dgc;

            dgc = dgc.DragStart();
            dgc.SetState(DragGame_Child_State.drag, null);
            if (dgc.ieCurrentMover != null) StopCoroutine(hit.GetComponent<DragGame_Child>().ieCurrentMover);
            if (OnDragStartedOnePar != null) OnDragStartedOnePar(dgc);

            DoMoveToCorrectPlace(dgc, dgp, true);

            partsLeft--;
            dgc.SetState(DragGame_Child_State.dropCorrect, dgp);
            dgp.DropCorrect(dgc);

            StartCoroutine(DelayRefreshScale(dgc, dgp));
            if (OnDropCorrect != null) OnDropCorrect(dgc, dgp);
            dgc.transform.parent = dgp.transform.parent;
            RunHelper();

        }
        IEnumerator DelayRefreshScale(DragGame_Child dgc, DragGame_Parent dgp)
        {
            yield return new WaitForEndOfFrame();

            dgc.SetState(DragGame_Child_State.dropCorrect, dgp);

        }
        #endregion

        ///////////////////////////////////////////////////////// MOVE METHODS
        void DoMoveToCorrectPlace(DragGame_Child dgc, DragGame_Parent dgp, bool randomPos = false)
        {
            if (dgp.currentParentUseType == DragGame_Parent.ParentUseType.useParentCenterPosition)
            {
                if (dgc.ieCurrentMover != null) StopCoroutine(dgc.ieCurrentMover);
                dgc.ieCurrentMover = MoveToCorrectPlace(dgc.transform, dgp.transform);
                StartCoroutine(dgc.ieCurrentMover);

            }
            else if (dgp.currentParentUseType == DragGame_Parent.ParentUseType.useAnyDropColliderPosition)
            {
                if (dgc.ieCurrentMover != null) StopCoroutine(dgc.ieCurrentMover);
                GameObject go = new GameObject("TempPos");
                dgp.tempHolderDropPos = go.transform;
                if (randomPos)
                {
                    if (dgc.curDragChildType == ClickChildType.oneClick)
                    {
                        go.transform.position = dgp.GetRandomPosInsideCollider();

                    }
                    else
                    {
                        go.transform.position = curCamera.ScreenToWorldPoint(Input.mousePosition);

                    }
                }
                else
                    go.transform.position = GetDeltaPos();
                go.transform.parent = dgp.transform;
                Destroy(go, 30);
                dgc.ieCurrentMover = MoveToCorrectPlace(dgc.transform, dgp.transform, true);
                StartCoroutine(dgc.ieCurrentMover);
            }
        }

        bool CheckIsClickCorrect(DragGame_Child _hit)
        {
            if (currentDragGameType == DragGameType.hardSetChildrenAndParents
                || currentDragGameType == DragGameType.hardSetChildrenAndParents
                || currentDragGameType == DragGameType.hardSetChildrenParentFromChildList)
            {
                if (list_targetChilds.Contains(_hit))
                {
                    return true;
                }
            }
            else if (currentDragGameType == DragGameType.getAnyByCompareNames)
            {
                return true;
            }
            else if (currentDragGameType == DragGameType.getOnlyOneByName)
            {
                if (_hit.name == nameChild)
                {
                    return true;
                }
                return true;
            }
            return false;
        }
        public virtual bool CheckIsDropCorrect(DragGame_Parent _dgp, DragGame_Child hit)
        {
            if (!hit) return false;
            if (!_dgp) return false;

            if (currentDragGameType == DragGameType.getAnyByCompareNames)
            {
                if (_dgp.isAllowMultipleUse || isAllowMultipleParentUse)
                {
                    if (_dgp.name.Contains(hit.name))
                    {
                        return true;
                    }
                    else return false;
                }
             }

            if (!isAllowMultipleParentUse)
            {
                if (_dgp.isParentTaken)
                {
                    return false;
                }

            }
            else _dgp.isParentTaken = false;
            // если жёстко задан родитель куда тащить у детеныша
            if (hit.list_TargetParents.Count != 0)
            {
                foreach (var item in hit.list_TargetParents)
                {
                    if (item == _dgp)
                    {
                        return true;
                    }
                }

            }
            // если задан родитель куда тащить в основном скрипте
            if (list_TargetParents != null)
            {
                if (list_TargetParents.Contains(_dgp))
                {
                    return true;
                }
            }

            if (isGetByName)
            {
                if (_dgp.name.Contains(hit.name) && nameChild == hit.name)
                {
                    return true;
                }
                else return false;
            }


            else return false;
        }

        public virtual IEnumerator MoveToCorrectPlace(Transform _from, Transform _to, bool useTempParentPos = false)
        {
            float speed = _from.GetComponent<DragGame_Child>().speedMoveToCorrectPlace;
            if (useTempParentPos)
            {

                Transform pos = _to.GetComponent<DragGame_Parent>().tempHolderDropPos;
                //Debug.Log("MoveToCorrectPlace " + new System.Diagnostics.StackTrace(), pos.gameObject);

                do
                {
                    yield return null;

                    if (_from)
                        _from.position = Vector2.Lerp(_from.position, pos.position, Time.deltaTime * 5 * speed);
                    else
                        yield break;

                } while (Vector2.Distance(_from.position, pos.position) > 0.005f);

            }
            else
            {
                //Debug.Log("MoveToCorrectPlace " + new System.Diagnostics.StackTrace(), _to.gameObject);

                do
                {
                    yield return null;

                    if (_from)
                        _from.position = Vector2.Lerp(_from.position, _to.position, Time.deltaTime * 5 * speed);
                    else
                        yield break;
                } while (Vector2.Distance(_from.position, _to.position) > 0.005f);

            }

            _from.GetComponent<DragGame_Child>().SetDropCorrect__OrderInLayer(_to.GetComponent<DragGame_Parent>());
            _from.GetComponent<DragGame_Child>().SetDropCorrect_SortingLayerName();

            if (OnMoveToCorrectPlaceCompleted != null)
            {
                OnMoveToCorrectPlaceCompleted(_from.GetComponent<DragGame_Child>(), _to.GetComponent<DragGame_Parent>());
            }
        }

        public virtual IEnumerator MoveBack(Transform _objectToMove)
        {
            Debug.Log("MoveBack " + new System.Diagnostics.StackTrace(), _objectToMove.gameObject);
            DragGame_Child dgc = _objectToMove.GetComponent<DragGame_Child>();
            dgc.placed = false;
            dgc.busyBack = true;
            do
            {
                _objectToMove.transform.position = Vector2.Lerp(_objectToMove.transform.position, _objectToMove.GetComponent<DragGame_Child>().defaultPosition.position, Time.deltaTime * 5);
                yield return null;
            } while (Vector2.Distance(_objectToMove.transform.position, _objectToMove.GetComponent<DragGame_Child>().defaultPosition.position) > 0.05f);
            dgc.busyBack = false;
            dgc.ReturnDefault__OrderInLayer();
            dgc.ReturnDefault__SortingLayer();
            dgc.ReturnedBack();
            //if (dgc.currentParentHoldMe)
            //{
            //    dgc.currentParentHoldMe.ho
            //}
            if (OnDropWrongAndMoveBackCompleted != null)
            {
                OnDropWrongAndMoveBackCompleted(dgc);
            }
        }

        /// //////////////////////////////////////////////// HELPER
#region HELPER METHODS
        public virtual void HelperUpdate()
        {
            helperTimer += Time.deltaTime;
        }
        IEnumerator ieHelper;
        public virtual void RunHelper()
        {

            if (ieHelper != null) StopCoroutine(ieHelper);
            ieHelper = Helper();
            StartCoroutine(ieHelper);
        }
        byte helperDelay = 5;
        public virtual void OverrideHelperDelay(byte _time)
        {
            helperDelay = _time;
        }
        public virtual IEnumerator Helper()
        {
            while (game)
            {
                yield return new WaitForSeconds(helperDelay);
                //           Если ребёнок тупит:
                //ПЕПА - Перетяни стикер на нужное место.
                //while (pepaController_base.TalkDelayed("dr - 5 ")) yield return new WaitForEndOfFrame();
                if (!game) break;
                if (OnHelperFirst != null)
                {
                    OnHelperFirst();
                }

                yield return new WaitForSeconds(helperDelay);
                //Если продолжает тупить – подсветить контур случайного стикера и место, куда его
                //нужно перетянуть.
                if (!game) break;

                if (OnHelperSecond != null)
                {
                    OnHelperSecond();

                }

            }
        }

        public virtual void OverrideHelperBloomer(string _name)
        {
            foreach (var item in GameObject.FindObjectsOfType<DragGame_Child>())
            {
                if (item.currentState != DragGame_Child_State.dropCorrect && item.name == _name)
                {
                    item.EnableDisableHelper(true);
                }
            }
        }
        public virtual void OverrideHelperBloomer()
        {
            foreach (var item in GameObject.FindObjectsOfType<DragGame_Child>())
            {
                if (item.currentState != DragGame_Child_State.dropCorrect)
                {
                    item.EnableDisableHelper(true);

                }
            }
        }

        List<DragGame_Child> list_ActiveChildsHelpers = new List<DragGame_Child>();
        public void DisableActiveChildHelpers()
        {

            foreach (var item in list_ActiveChildsHelpers)
            {
                item.StartStopIEHelper(false);
            }
            list_ActiveChildsHelpers.Clear();
        }
        public void StoreActiveChildHelper(DragGame_Child dgc)
        {
            if (!list_ActiveChildsHelpers.Contains(dgc))
            {
                list_ActiveChildsHelpers.Add(dgc);
            }
        }
        #endregion
        /// ////////////////////////////////////////////////

        /// <summary>
        /// Разрешить многократное использование ВСЕХ родителей, так же можно у конкретного родителя настроить
        /// </summary>
        /// <param name="_Allow"></param>
        public virtual void SwitchAllowMultipleParentUse(bool _Allow)
        {
            isAllowMultipleParentUse = _Allow;
        }

        public virtual void SimulateDropWrongAndMoveBack(DragGame_Child element)
        {
            element.SetState(DragGame_Child_State.dropWrong, null);
            element.GetComponent<BoxCollider2D>().enabled = true;
            element.transform.parent = element.defaultPosition.transform.parent;
            if (element.ieCurrentMover != null) StopCoroutine(element.ieCurrentMover);
            element.ieCurrentMover = MoveBack(element.transform);
            StartCoroutine(element.ieCurrentMover);
            if (element.currentParentHoldMe)
            {

                element.currentParentHoldMe.currentHoldedChild = null;
            }


        }

        public virtual void ReturnBackChildElement(DragGame_Child element)
        {
            element.SetState(DragGame_Child_State.dropWrong, null);

            if (element.ieCurrentMover != null) StopCoroutine(element.ieCurrentMover);
            element.ieCurrentMover = MoveBack(element.transform);
            StartCoroutine(element.ieCurrentMover);
            element.transform.parent = element.defaultPosition.parent;

            element.SetState(DragGame_Child_State.defaultState, null);
            element.GetComponent<BoxCollider2D>().enabled = true;
            element.ReturnDefault__OrderInLayer();
            element.ReturnDefault__SortingLayer();
            element.currentParentHoldMe.currentHoldedChild = null;
        }

        private bool IsPointerOverUIObject()
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }


    }
}