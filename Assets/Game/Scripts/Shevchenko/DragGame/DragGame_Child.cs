﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Shevchenko
{
    public enum DragGame_Child_State
    {
        defaultState, dropCorrect, drag, dropWrong
    }
    [RequireComponent(typeof(BoxCollider2D))]
    public class DragGame_Child : MonoBehaviour
    {
        public delegate void CloneCreated(DragGame_Child _childClone, DragGame_Child _childBase);
        public static event CloneCreated OnCloneCreated;

        public delegate void DeleteReturnedChild(DragGame_Child _childClone);
        public static event DeleteReturnedChild OnDeleteReturnedChild;

        [System.Serializable]
        public class OrdersLayerParams
        {
            public int defaultOrderInLayer;
            public SpriteRenderer currentSprite;
            public string defaultSortingLayerName;
        }

        [Header("Можно Тащить, только нажать и полетит или две позиции")]
        public ClickChildType curDragChildType;
        [Header("Многократное использование, дубликат")]
        public bool isMultipleUse;
        [Header("Если выбрано, инстантировать не копию а этот объект")]
        public GameObject srcDragObject;
        [Header("Удалять объект после неправильного броска и возврата назад")]
        public bool deleteMeAfterDropWrong;
        //public bool DeleteMeAfterDropWrong { get { return deleteMeAfterDropWrong; } set { Debug.Break(); deleteMeAfterDropWrong= value; }}
        [Header("Жестко задать родителей, куда тащить")]
        public List<DragGame_Parent> list_TargetParents = new List<DragGame_Parent>();
        [Header("Разрешить таскать повторно после успешного перетаскивания")]
        public bool isAllowDragAgain;
        [Header("Определяет находится на панели или просто валяется")]
        public bool isItemOnPanel;

        //[Header("Для лёгкого - клик объект - второй клик - позиция")]
        // public bool doubleClickSelectType;

        public DragGame_Child_State currentState;
        public int number;
        [HideInInspector]
        public Transform defaultPosition;

        public DragGame_Parent currentParentHoldMe;

        //[HideInInspector]
        public bool placed;
        //[HideInInspector]
        public bool busyBack, isInMoveNow;

        public List<OrdersLayerParams> list_orderParams = new List<OrdersLayerParams>();

        [System.NonSerialized]
        public IEnumerator ieCurrentMover ;

        public bool useDragParentOrder = true;
         public int dragOrderInLayer = 100;
 
        public bool isNeedChangeSortingLayerNAME;
         public string dragSortingLayerName;
        public string layerNameAfterDropCorrect = "Default";
        public int objValue;

        public bool useDragParentSize = true;
        public bool isNeedChangeSize = true;
        float size_Default;
        public float size_CurrentTarget;
        public float Size_CurrentTarget { get { return size_CurrentTarget; } set { size_CurrentTarget = value; } }
        public float size_Drag = 0;
        [Tooltip("Если useDragParentSize будет false то использует этот параметр")]
        public float size_End = 1;
        public bool isIAmClone;

        /// <summary>
        /// Using parent drag element rotation
        /// </summary>
        public bool isNeedChangeRotate;
        public bool isNeedChangeRotateWhileDrag;
        bool defaultIsNeedChangeRotateWhileDrag;
        public int angleWhileDrag;
        Vector3 targetAngleWhileDrag;
        public Vector3 defaultAngleWhileDrag;
 
         Animator animHelper;
        public bool useIEScaleHelper;
        Vector3 defaultScale;
        bool isIeHelperActiveNow, pauseIEHelper;
        float deltaScelaHelper;

        [Header("Использовать сдвиг при таскании")]
        public Vector2 vDeltaDragPos = Vector2.zero;
        [Header("Скорость к финальной точке")]
        public float speedMoveToCorrectPlace = 1;
        void Awake()
        {
            // default Pos
            GameObject go = new GameObject();
            go.transform.position = transform.position;
            go.name = name + "_defaultPosition";
            go.transform.parent = transform.parent;
            defaultPosition = go.transform;

            defaultScale = transform.localScale;
            deltaScelaHelper = transform.localScale.x * 0.1f;

            defaultIsNeedChangeRotateWhileDrag = isNeedChangeRotateWhileDrag;
        }

        void Start()
        {
            size_Default = transform.localScale.x;
            if (!isIAmClone)
            {
                InitOrders();
            }
            if (isNeedChangeSize)
            {
                Size_CurrentTarget = size_Default;
                if (size_Drag == 0) size_Drag = size_Default;
            }

            defaultAngleWhileDrag = transform.localRotation.eulerAngles ;
            targetAngleWhileDrag = defaultAngleWhileDrag;

            if (curDragChildType == ClickChildType.twoPointClick || curDragChildType == ClickChildType.dragAndDoubleClick)
            {
                useIEScaleHelper = true;
            }
        }
        void InitOrders()
        {
            if (list_orderParams.Count == 0)
            {
                foreach (SpriteRenderer item in GetComponentsInChildren<SpriteRenderer>(true))
                {
                    OrdersLayerParams op = new OrdersLayerParams();
                    op.currentSprite = item;
                    op.defaultOrderInLayer = item.sortingOrder;
                    op.defaultSortingLayerName = item.sortingLayerName;
                    list_orderParams.Add(op);
                }

            }
        }
        //private void OnEnable()
        //{
        //    SaveController.OnSaveStarted += SaveController_OnSaveStarted;
        //}
        //private void OnDisable()
        //{
        //    SaveController.OnSaveStarted -= SaveController_OnSaveStarted;

        //}
        //private void SaveController_OnSaveStarted(ObjectIdentifier oi)
        //{
        //    if (GetComponent<DragGame_Child>())
        //    {
        //        StopAllCoroutines();
        //        ieCurrentMover = null;
 
        //    }
        //}

        public void UpdateDefaultLayerParams()
        {
            list_orderParams.Clear();
            foreach (SpriteRenderer item in GetComponentsInChildren<SpriteRenderer>(true))
            {
                OrdersLayerParams op = new OrdersLayerParams();
                op.currentSprite = item;
                op.defaultOrderInLayer = item.sortingOrder;
                op.defaultSortingLayerName = item.sortingLayerName;
                list_orderParams.Add(op);
            }

        }
        void Update()
        {

            if (isIeHelperActiveNow && useIEScaleHelper && !pauseIEHelper)
            {
                if (transform.localScale.x < defaultScale.x + defaultScale.x * 0.01f)
                {
                    Size_CurrentTarget = defaultScale.x + deltaScelaHelper;
                }
                else if (transform.localScale.x > defaultScale.x + deltaScelaHelper - (defaultScale.x * 0.01f))
                {
                    Size_CurrentTarget = defaultScale.x;
                }
                float target = Mathf.MoveTowards(transform.localScale.x, Size_CurrentTarget, Time.deltaTime * (defaultScale.x / 2));
                transform.localScale = new Vector3(
                    target, target, target
                    );
            }
            else
            {
                if (isNeedChangeSize)
                {
                    float target = Mathf.Lerp(transform.localScale.x, Size_CurrentTarget, Time.deltaTime * 2);
                    transform.localScale = new Vector3(
                        target, target, target
                        );
                }
            }

            if (isNeedChangeRotateWhileDrag || isNeedChangeRotate)
            {
                //transform.localRotation = Quaternion.Lerp(transform.localRotation,
                //    Quaternion.Euler(new Vector3(transform.localRotation.x, 0,
                //    targetAngleWhileDrag)), Time.deltaTime * 2 * speedMoveToCorrectPlace);
                transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(targetAngleWhileDrag),Time.deltaTime*2*speedMoveToCorrectPlace);
            }
            //else if (isNeedChangeRotate)
            //{
            //    transform.localRotation = Quaternion.Lerp(transform.localRotation,
            //        Quaternion.Euler(new Vector3(transform.localRotation.x, 0,
            //        targetAngleWhileDrag)), Time.deltaTime * 2 * speedMoveToCorrectPlace);

            //}

        }
        /// <summary>
        /// Call from DragGame update on correct drop
        /// </summary>
        void DropCorret(DragGame_Parent _DragGame_Parent)
        {
            currentParentHoldMe = _DragGame_Parent;
            if (isAllowDragAgain)
            {
                currentState = DragGame_Child_State.defaultState;

            }
            else
            {
                currentState = DragGame_Child_State.dropCorrect;
            }
            if (isNeedChangeRotate)
            {
                DoDropCorrectRotate(_DragGame_Parent.transform);
            }
            if (useDragParentSize)
            {
                float parentScale = 1;
                if (_DragGame_Parent.transform.parent != null)
                {
                    //parentScale = _DragGame_Parent.transform.parent.transform.localScale.x;
                }
                Size_CurrentTarget = _DragGame_Parent.defaultScale.x / parentScale;
            }else
            {
                Size_CurrentTarget = size_End;
            }
            //SetLayer_name__Default();
        }
        void DoDropCorrectRotate(Transform _tr)
        {
            //if (ieCurrentRotator != null) StopCoroutine(ieCurrentRotator);
            //ieCurrentRotator = StaticParams.Rotate(transform, _tr.transform, 3);
            //StartCoroutine(ieCurrentRotator);
            targetAngleWhileDrag = _tr.localRotation.eulerAngles ;
        }
        public void DoManualDropCorrectRotate(Quaternion _rot)
        {
            //if (ieCurrentRotator != null) StopCoroutine(ieCurrentRotator);
            //ieCurrentRotator = StaticParams.Rotate(transform, _rot, 3);
            //StartCoroutine(ieCurrentRotator);
            targetAngleWhileDrag = _rot.eulerAngles;
        }

        public void SetState(DragGame_Child_State _state, DragGame_Parent _dgp)
        {
            //Debug.Log("----SetState " + _state + "  " + new System.Diagnostics.StackTrace());
            switch (_state)
            {
                case DragGame_Child_State.defaultState:
                    currentState = DragGame_Child_State.defaultState;
                    Size_CurrentTarget = size_Default;
                    targetAngleWhileDrag = defaultAngleWhileDrag;
                    isNeedChangeRotateWhileDrag = defaultIsNeedChangeRotateWhileDrag;

                    break;

                case DragGame_Child_State.dropCorrect:
                    //Size_CurrentTarget = size_End; -вызовется из метода DropCorret
                    currentState = DragGame_Child_State.dropCorrect;
                    isNeedChangeRotateWhileDrag = false;

                    if (isNeedChangeRotate)
                    {
                        targetAngleWhileDrag = _dgp.transform.localRotation.eulerAngles;
                    }
                    isItemOnPanel = false;
                    placed = true;
                    DropCorret(_dgp);
                    break;

                case DragGame_Child_State.drag:
                    Size_CurrentTarget = size_Drag;
                    currentState = DragGame_Child_State.drag;
                    targetAngleWhileDrag = new Vector3(0, 0, angleWhileDrag);
                    break;

                case DragGame_Child_State.dropWrong:
                    Size_CurrentTarget = size_Default;
                    currentState = DragGame_Child_State.dropWrong;
                    targetAngleWhileDrag = defaultAngleWhileDrag;
                    isNeedChangeRotateWhileDrag = defaultIsNeedChangeRotateWhileDrag;
                    transform.parent = defaultPosition.transform.parent;
                    foreach (var item in list_orderParams)
                    {
                        item.currentSprite.sortingLayerName = "UI";
                    }
                    break;
                default:
                    break;
            }
            CheckStatusIEHelper();
        }

        public DragGame_Child DragStart()
        {
            //StoreAgainDefaultPosition();

            //создаём дубликат если многократное использование
            if (isMultipleUse && !srcDragObject && !isIAmClone)
            {

                GameObject go = (GameObject)Instantiate(gameObject);
                go.name = name + "_Clone";
                go.transform.position = transform.position;
                go.transform.parent = transform.parent;
                go.transform.localScale = transform.localScale;
                go.transform.localRotation = transform.localRotation;
                DragGame_Child dgc_Clone = go.GetComponent<DragGame_Child>();
                dgc_Clone.defaultPosition.parent = transform.parent;
                dgc_Clone.defaultPosition.localPosition = defaultPosition.localPosition;
                dgc_Clone.isIAmClone = true;
                dgc_Clone.SetTemp__OrderInLayer();
                dgc_Clone.SetTemp__SortingLayer();
                dgc_Clone.curDragChildType = curDragChildType;
                dgc_Clone.isAllowDragAgain = isAllowDragAgain;
                if (DragGame.instance.currentDragGameType == DragGameType.hardSetChildrenParentFromChildList)
                     dgc_Clone.SetTargetParents(list_TargetParents);

                 if (OnCloneCreated != null)
                {
                    OnCloneCreated(dgc_Clone, this);
                }
                return go.GetComponent<DragGame_Child>();
            }
            // если таскать будем префаб
            else if (isMultipleUse && srcDragObject)
            {
                GameObject go = (GameObject)Instantiate(srcDragObject);
                go.name = srcDragObject.name;
                go.transform.position = transform.position;
                //go.transform.parent = transform.parent;
                go.transform.localScale = srcDragObject.transform.localScale;
                go.transform.localRotation = transform.localRotation;
                DragGame_Child dgc_Clone = go.GetComponent<DragGame_Child>();

                dgc_Clone.defaultPosition.parent = transform.parent;
                dgc_Clone.defaultPosition.localPosition = defaultPosition.localPosition;
                dgc_Clone.UpdateDefaultLayerParams();
                dgc_Clone.SetTemp__OrderInLayer();
                dgc_Clone.SetTemp__SortingLayer();
                dgc_Clone.isItemOnPanel = isItemOnPanel;
                dgc_Clone.size_Drag = size_Drag;
                if (DragGame.instance.currentDragGameType == DragGameType.hardSetChildrenParentFromChildList)
                    dgc_Clone.SetTargetParents(list_TargetParents);

                if (OnCloneCreated != null)
                {
                    OnCloneCreated(dgc_Clone, this);
                }

                return go.GetComponent<DragGame_Child>();
            }
            else // если работаем с текущим объектом
            {
                SetTemp__OrderInLayer();
                SetTemp__SortingLayer();

            }
            //если можно таскать повторно обнулить правильное расположение
            if (isAllowDragAgain)
            {
                placed = false;
            }
            return this;


        }

        public void ReturnDefault__OrderInLayer()
        {
            //if (isImageInChild)
            //{
            //    transform.GetComponentInChildren<SpriteRenderer>().sortingOrder = defaultOrderInLayer;
            //}
            //else
            //    GetComponent<SpriteRenderer>().sortingOrder = defaultOrderInLayer;
            if (GetComponent<LayerWorker>()) return;
            if (transform.parent) if (transform.parent.GetComponent<LayerWorker>()) return;


            foreach (var item in list_orderParams)
            {
                item.currentSprite.sortingOrder = item.defaultOrderInLayer;
            }
        }

        public void SetDropCorrect__OrderInLayer(DragGame_Parent _DragGame_Parent)
        {
            //if (isImageInChild)
            //{
            //    transform.GetComponentInChildren<SpriteRenderer>().sortingOrder = defaultOrderInLayer;
            //}
            //else
            //    GetComponent<SpriteRenderer>().sortingOrder = defaultOrderInLayer;
            if (GetComponent<LayerWorker>()) return;

            if (useDragParentOrder)
            {
                if (_DragGame_Parent.GetComponentInChildren<SpriteRenderer>(true))
                {
                    //int layer = _DragGame_Parent.GetComponentInChildren<SpriteRenderer>(true).sortingOrder;
                    int layer = _DragGame_Parent.GetMaxOrderValue();
                    list_orderParams.Sort(delegate (OrdersLayerParams us1, OrdersLayerParams us2)
                    { return us1.defaultOrderInLayer.CompareTo(us2.defaultOrderInLayer); });
                    int minVal = list_orderParams[0].defaultOrderInLayer;

                    foreach (var item in list_orderParams)
                    {

                        item.currentSprite.sortingOrder = layer + item.defaultOrderInLayer - minVal + 1;
                    }
                    return;
                }
            }

            foreach (var item in list_orderParams)
            {
                item.currentSprite.sortingOrder = item.defaultOrderInLayer;
            }

        }

        public void SetManual__OrderInLayer(int _orderInLayer)
        {
            //if (isImageInChild)
            //{
            //    transform.GetComponentInChildren<SpriteRenderer>().sortingOrder = defaultOrderInLayer;
            //}
            //else
            //    GetComponent<SpriteRenderer>().sortingOrder = defaultOrderInLayer;
            foreach (var item in list_orderParams)
            {
                item.currentSprite.sortingOrder = _orderInLayer + item.defaultOrderInLayer;
            }
        }
        public void SetTemp__OrderInLayer()
        {
            foreach (var item in list_orderParams)
            {
                item.currentSprite.sortingOrder = item.currentSprite.sortingOrder + dragOrderInLayer;
            }
        }

        public void ReturnDefault__SortingLayer()
        {
            if (isNeedChangeSortingLayerNAME)
                //if (isImageInChild)
                //{
                //    transform.GetComponentInChildren<SpriteRenderer>().sortingLayerName = defaultSortingLayerName;
                //}
                //else
                //    GetComponent<SpriteRenderer>().sortingLayerName = defaultSortingLayerName;
                foreach (var item in list_orderParams)
                {
                    item.currentSprite.sortingLayerName = item.defaultSortingLayerName;
                }

        }

        //public void SetLayer_name__Default()
        //{
        //    foreach (var item in list_orderParams)
        //    {
        //        item.currentSprite.sortingLayerName = layerNameAfterDropCorrect;
        //    }
        //}
        public void SetTemp__SortingLayer()
        {
            if (isNeedChangeSortingLayerNAME)

                //if (isImageInChild)
                //{
                //    transform.GetComponentInChildren<SpriteRenderer>().sortingLayerName = dragSortingLayerName;
                //}
                //else
                //    GetComponent<SpriteRenderer>().sortingLayerName = dragSortingLayerName;
                foreach (var item in list_orderParams)
                {
                    item.currentSprite.sortingLayerName = dragSortingLayerName;
                }
        }
        public void SetTemp__SortingLayer(string _name)
        {
            foreach (var item in list_orderParams)
            {
                item.currentSprite.sortingLayerName = _name;
            }
        }
        public void SetDropCorrect_SortingLayerName()
        {
            if (isNeedChangeSortingLayerNAME)
            {
                foreach (var item in list_orderParams)
                {
                    item.currentSprite.sortingLayerName = layerNameAfterDropCorrect;
                }
            }
            else
            {
                foreach (var item in list_orderParams)
                {
                    item.currentSprite.sortingLayerName = "Default";
                }
            }
        }

        public void StoreAgainDefaultPosition()
        {
            defaultPosition.position = new Vector2(transform.position.x, transform.position.y);

        }

        public void ReturnedBack()
        {
            SetState(DragGame_Child_State.defaultState, null);
            if (GetComponentInParent<ScrollerPanelController>())
            {
                isItemOnPanel = true;
            }
            //currentState = DragGame_Child_State.defaultState;
            if (deleteMeAfterDropWrong)
            {
                //Debug.Log("----deleteMeAfterDropWrong " + new System.Diagnostics.StackTrace());
                if (OnDeleteReturnedChild != null)
                {
                    OnDeleteReturnedChild(this);
                }
                Destroy(gameObject);
                Destroy(defaultPosition.gameObject);
            }
        }
      
        public void EnableDisableHelper(bool _enable)
        {
            //animHelper = GetComponentInChildren<Animator>();
            foreach (var item in GetComponentsInChildren<Animator>())
            {
                if (item.name.Contains("Helper"))
                {
                    animHelper = item;
                }
            }
            if (animHelper)
            {
                animHelper.gameObject.SetActive(_enable);
            }
 
        }

        public void TurnMyCollider(bool _on)
        {
            if (GetComponent<BoxCollider2D>())
            {
                GetComponent<BoxCollider2D>().enabled = _on;

            }
        }

        void CheckStatusIEHelper()
        {
            if (isIeHelperActiveNow && curDragChildType != ClickChildType.twoPointClick)
            {

                switch (currentState)
                {
                    case DragGame_Child_State.defaultState:
                        SetIEHelperPause(false);
                        break;
                    case DragGame_Child_State.dropCorrect:
                        SetIEHelperPause(true);
                        break;
                    case DragGame_Child_State.drag:
                        SetIEHelperPause(true);
                        break;
                    case DragGame_Child_State.dropWrong:
                        SetIEHelperPause(true);
                        break;
                    default:
                        break;
                }
            }
        }

        public void SetIEHelperPause(bool _on)
        {
            if (useIEScaleHelper)
            {
                pauseIEHelper = _on;

                //if (_on)
                //{
                //    pauseIEHelper = true;
                //    //if (ieAnimScaler != null) StopCoroutine(ieAnimScaler);
                //    //ieAnimScaler = AnimScaler();
                //    //StartCoroutine(ieAnimScaler);
                //}
                //else
                //{
                //    pauseIEHelper = false;
                //       //if (ieAnimScaler != null) StopCoroutine(ieAnimScaler);
                //       //isIeHelperActiveNow = false;

                //}

            }
        }
        Transform lastParent;
        public void StartStopIEHelper(bool _enable)
        {
            isIeHelperActiveNow = _enable;
            if (_enable)
            {
                DragGame.instance.StoreActiveChildHelper(this);
                if (lastParent != transform.parent)
                {
                    lastParent = transform.parent;
                    defaultScale = transform.localScale;
                    deltaScelaHelper = transform.localScale.x * 0.1f;

                }
                //defaultScale = transform.localScale;
                //deltaScelaHelper = transform.localScale.x * 0.1f;

            }
        }

        //IEnumerator ieAnimScaler;
        //IEnumerator AnimScaler()
        //{

        //    Vector3 target = new Vector3(transform.localScale.x + 0.15f, transform.localScale.y + 0.15f, 1);

        //    while (true)
        //    {
        //        do
        //        {
        //            transform.localScale = Vector2.MoveTowards(transform.localScale, target, Time.deltaTime);
        //            yield return null;

        //        } while (!Mathf.Approximately(transform.localScale.x, target.x));

        //        if (Mathf.Approximately(transform.localScale.x, defaultScale.x))
        //        {
        //            target = new Vector3(defaultScale.x + 0.15f, defaultScale.y + 0.15f, 1);
        //        }
        //        else
        //        {
        //            target = defaultScale;
        //        }
        //    }
        //}

        public void ReturnMeBack()
        {

        }
        public void TurnOffSpriterenderers(bool _enabled)
        {
            foreach (var item in list_orderParams)
            {
                item.currentSprite.enabled = _enabled;
            }
        }

        public void SetTargetParents(List<DragGame_Parent> _list_targetParents)
        {
            list_TargetParents = new List<DragGame_Parent>(_list_targetParents);
        }
    }
}