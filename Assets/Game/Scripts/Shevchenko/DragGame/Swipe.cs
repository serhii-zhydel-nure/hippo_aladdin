﻿using UnityEngine;
using System.Collections;
using System;
namespace Shevchenko
{
    public class Swipe : MonoBehaviour
    {
        public delegate void DoSwipe();
        public static event DoSwipe OnSwipeUp;
        public static event DoSwipe OnSwipeDown;
        public static event DoSwipe OnSwipeLeft;
        public static event DoSwipe OnSwipeRight;

        public delegate void DoSwipePos(Vector3 pos);
        public static event DoSwipePos OnSwipeMouseDown;
        public static event DoSwipePos OnSwipeMouseUP;
        public static event DoSwipePos OnMouseClickUp;
        public static event DoSwipePos OnMouseDoubleClick;

        void Update()
        {
            DirectionAngleInput();
        }
        private Vector2 currentPos;
        private bool activeInput;
        float clickTimer;

      
        private void DirectionAngleInput()
        {
            clickTimer += Time.deltaTime;
            if (Input.GetMouseButtonDown(0))
            {
                currentPos = Input.mousePosition;
                activeInput = true;
                if (OnSwipeMouseDown != null)
                {
                    OnSwipeMouseDown(currentPos);
                }
            }

            if (Input.GetMouseButton(0))
            {
                if (activeInput)
                {
                    float ang = GetAngle(currentPos, Input.mousePosition);
                    if ((Input.mousePosition.x - currentPos.x) > 10)
                    {
                        if (ang < 45 && ang > -45)
                        {
                            //directInput = DirectionInput.Right;
                            activeInput = false;

                            Right();
                        }
                        else if (ang >= 45)
                        {
                            //directInput = DirectionInput.Up;
                            activeInput = false;

                            Up();
                        }
                        else if (ang <= -45)
                        {
                            ////directInput = DirectionInput.Down;
                            activeInput = false;

                            Down();
                        }
                    }
                    else if ((Input.mousePosition.x - currentPos.x) < -10)
                    {
                        if (ang < 45 && ang > -45)
                        {
                            //directInput = DirectionInput.Left;
                            activeInput = false;

                            Left();
                        }
                        else if (ang >= 45)
                        {
                             //directInput = DirectionInput.Down;
                            activeInput = false;

                            Down();
                        }
                        else if (ang <= -45)
                        {
                            //directInput = DirectionInput.Up;
                            activeInput = false;
                            Up();
                            
                        }
                    }
                    else if ((Input.mousePosition.y - currentPos.y) > 10)
                    {
                        if ((Input.mousePosition.x - currentPos.x) > 0)
                        {
                            if (ang > 45 && ang <= 90)
                            {
                                //directInput = DirectionInput.Up;
                                activeInput = false;
                                Up();
                               
                            }
                            else if (ang <= 45 && ang >= -45)
                            {
                                //directInput = DirectionInput.Right;
                                activeInput = false;
                                Right();
                             
                            }
                        }
                        else if ((Input.mousePosition.x - currentPos.x) < 0)
                        {
                            if (ang < -45 && ang >= -89)
                            {
                                //directInput = DirectionInput.Up;
                                activeInput = false;
                                Up();

                               
                            }
                            else if (ang >= -45)
                            {
                                //directInput = DirectionInput.Left;
                                activeInput = false;
                                Left();
                               
                            }
                        }
                    }
                    else if ((Input.mousePosition.y - currentPos.y) < -10)
                    {
                        if ((Input.mousePosition.x - currentPos.x) > 0)
                        {
                            if (ang > -89 && ang < -45)
                            {
                                //directInput = DirectionInput.Down;
                                activeInput = false;
                                Down();
                               
                            }
                            else if (ang >= -45)
                            {
                                //directInput = DirectionInput.Right;
                                activeInput = false;
                                Right();
                               
                            }
                        }
                        else if ((Input.mousePosition.x - currentPos.x) < 0)
                        {
                            if (ang > 45 && ang < 89)
                            {
                                //directInput = DirectionInput.Down;
                                activeInput = false;
                                Down();
                              
                            }
                            else if (ang <= 45)
                            {
                                //directInput = DirectionInput.Left;
                                activeInput = false;
                                Left();
                                
                            }
                        }

                    }
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                //directInput = DirectionInput.Null;
                activeInput = false;
                if (OnSwipeMouseUP != null)
                {
                    OnSwipeMouseUP(Input.mousePosition);
                }

                if (Vector2.Distance(Camera.main.ScreenToWorldPoint(currentPos), Camera.main.ScreenToWorldPoint(Input.mousePosition)) < 0.1f)
                {
                    if (clickTimer < .3f)
                    {
                        clickTimer = 0;
                        if (OnMouseDoubleClick != null)
                        {
                            OnMouseDoubleClick(Input.mousePosition);
                        }
                    }
                    else
                    {
                        clickTimer = 0;

                        if (OnMouseClickUp != null)
                        {
                            OnMouseClickUp(Input.mousePosition);
                        }
                    }

                }

            }

        }

        public static float GetAngle(Vector3 form, Vector3 to)
        {
            Vector3 nVector = Vector3.zero;
            nVector.x = to.x;
            nVector.y = form.y;
            float a = to.y - nVector.y;
            float b = nVector.x - form.x;
            float tan = a / b;
            return RadToDegree(Mathf.Atan(tan));
        }
        public static float RadToDegree(float radius)
        {
            return (radius * 180) / Mathf.PI;
        }

        void Right()
        {
            if (OnSwipeRight != null)
            {
                OnSwipeRight();
            }
        }

        void Left()
        {
            if (OnSwipeLeft != null)
            {
                OnSwipeLeft();
            }
        }
        void Up()
        {
            if (OnSwipeUp != null)
            {
                OnSwipeUp();
            }
        }
        private void Down()
        {
            if (OnSwipeDown != null)
            {
                OnSwipeDown();
            }
        }
    }
}

