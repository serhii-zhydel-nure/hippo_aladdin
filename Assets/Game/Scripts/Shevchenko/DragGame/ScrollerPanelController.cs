﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Shevchenko;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class ScrollerPanelController : MonoBehaviour
{
    public ScrollRect[] scrolls;
    public enum ScrollType { horizontal, vertical }
    public ScrollType currentScrollType;

    private void Awake()
    {
        scrolls = GetComponentsInChildren<ScrollRect>();
        if (scrolls.Length == 0)
        {
            Debug.LogError("Во Вложенных объектах нет панели!");
        }
        if (scrolls[0].horizontal && !scrolls[0].vertical)
        {
            currentScrollType = ScrollType.horizontal;
        }
        else if (!scrolls[0].horizontal && scrolls[0].vertical)
        {
            currentScrollType = ScrollType.vertical;
        }
        else

        {
            Debug.LogError("НЕПРАВИЛЬНО ЗАДАНЫ ПАРАМЕТРЫ СКРОЛЛЕРА");
        }
        foreach (var item in GetComponentsInChildren<DragGame_Child>())
        {
            item.isItemOnPanel = true;
        }
    }
    private void Start()
    {
        //DragGame.instance.AllowDisallowDrag(false);


    }
    private void OnEnable()
    {
      Shevchenko.Swipe.OnSwipeDown += Swipe_OnSwipeDown;
        Shevchenko.Swipe.OnSwipeLeft += Swipe_OnSwipeLeft;
        Shevchenko.Swipe.OnSwipeRight += Swipe_OnSwipeRight;
        Shevchenko.Swipe.OnSwipeUp += Swipe_OnSwipeUp;
        Shevchenko.Swipe.OnSwipeMouseDown += Swipe_OnSwipeMouseDown;
        Shevchenko.Swipe.OnSwipeMouseUP += Swipe_OnSwipeMouseUP;
        Shevchenko.Swipe.OnMouseClickUp += Swipe_OnMouseClick;
    }


    private void OnDisable()
    {
        Shevchenko.Swipe.OnSwipeDown -= Swipe_OnSwipeDown;
        Shevchenko.Swipe.OnSwipeLeft -= Swipe_OnSwipeLeft;
        Shevchenko.Swipe.OnSwipeRight -= Swipe_OnSwipeRight;
        Shevchenko.Swipe.OnSwipeUp -= Swipe_OnSwipeUp;
        Shevchenko.Swipe.OnSwipeMouseDown -= Swipe_OnSwipeMouseDown;
        Shevchenko.Swipe.OnSwipeMouseUP -= Swipe_OnSwipeMouseUP;
        Shevchenko.Swipe.OnMouseClickUp -= Swipe_OnMouseClick;

    }

    private bool hovering = false;

    void Update()
    {
        //if (DragGame.instance.isEasy)
        //{
        //    if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        //    {
        //        if (IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        //        {
        //            Debug.Log("Hit UI, Ignore Touch");
        //            Debug.Log("Swipe_OnSwipeDown -> DragGame.instance.ManualMouseDown");

        //            DragGame.instance.ManualMouseDown();

        //        }
        //        else
        //        {
        //            Debug.Log("Handle Touch");
        //        }
        //    }
        //}
    }
    bool IsPointerOverGameObject(int fingerId)
    {
        EventSystem eventSystem = EventSystem.current;
        return (eventSystem.IsPointerOverGameObject(fingerId)
            && eventSystem.currentSelectedGameObject != null);
    }
    private void Swipe_OnMouseClick(Vector3 pos)
    {
#if !UNITY_EDITOR && UNITY_ANDROID

                Debug.Log("Swipe_OnMouseClick--> "+ EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId));
#endif

        // для лёгкого уровня сложности
#if UNITY_EDITOR && UNITY_ANDROID
        if (EventSystem.current.IsPointerOverGameObject())
            if (DragGame.instance.isEasy)
            {
                DragGame.instance.ManualMouseDown();
                //Debug.Log("Swipe_OnSwipeDown -> DragGame.instance.ManualMouseDown");

            }
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
 
        if (IsPointerOverUIObject())
        {
            if (DragGame.instance.isEasy)
            {
                Debug.Log("___Swipe_OnSwipeDown -> DragGame.instance.ManualMouseDown");

                DragGame.instance.ManualMouseDown();
            }
        }
#endif
    }
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
    private void Swipe_OnSwipeMouseUP(Vector3 pos)
    {
        foreach (var item in scrolls)
        {
            item.enabled = true;
        }

    }

    private void Swipe_OnSwipeMouseDown(Vector3 pos)
    {

        //throw new System.NotImplementedException();
    }

    private void Swipe_OnSwipeDown()
    {
#if UNITY_EDITOR && UNITY_ANDROID
        if (EventSystem.current.IsPointerOverGameObject())
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
                if (IsPointerOverUIObject())
#endif
        {
            if (currentScrollType == ScrollType.vertical)
            {
                foreach (var item in scrolls)
                {
                    item.enabled = true;
                }
            }
            else
            {
                foreach (var item in scrolls)
                {
                    item.enabled = false;
                }

                Debug.Log("Swipe_OnSwipeDown -> DragGame.instance.ManualMouseDown");
                DragGame.instance.ManualMouseDown();
            }
        }

    }
    private void Swipe_OnSwipeUp()
    {
#if UNITY_EDITOR && UNITY_ANDROID
        if (EventSystem.current.IsPointerOverGameObject())
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
                if (IsPointerOverUIObject())
#endif
        {
            if (currentScrollType == ScrollType.vertical)
            {
                foreach (var item in scrolls)
                {
                    item.enabled = true;
                }
            }
            else
            {
                foreach (var item in scrolls)
                {
                    item.enabled = false;
                }
                DragGame.instance.ManualMouseDown();

            }
        }

    }

    private void Swipe_OnSwipeRight()
    {
#if UNITY_EDITOR && UNITY_ANDROID
        if (EventSystem.current.IsPointerOverGameObject())
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
                if (IsPointerOverUIObject())
#endif
        {
            if (currentScrollType == ScrollType.vertical)
            {
                Debug.Log("Swipe_OnSwipeRight");
                foreach (var item in scrolls)
                {
                    item.enabled = false;
                }
                DragGame.instance.ManualMouseDown();

            }
            else
            {
                foreach (var item in scrolls)
                {
                    item.enabled = true;
                }

            }
        }

    }

    private void Swipe_OnSwipeLeft()
    {
#if UNITY_EDITOR && UNITY_ANDROID
        if (EventSystem.current.IsPointerOverGameObject())
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
                if (IsPointerOverUIObject())
#endif
        {
            if (currentScrollType == ScrollType.vertical)
            {
                foreach (var item in scrolls)
                {
                    item.enabled = false;
                }
                DragGame.instance.ManualMouseDown();

            }
            else
            {
                foreach (var item in scrolls)
                {
                    item.enabled = true;
                }
            }
        }

    }
}
