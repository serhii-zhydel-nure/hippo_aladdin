﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
    //[RequireComponent(typeof(BoxCollider2D))]

    public class DragGame_Parent : MonoBehaviour
    {
        public delegate void HoldedChildChanged(DragGame_Parent _currentParent, DragGame_Child _oldChild, DragGame_Child _newChild);
        public static event HoldedChildChanged OnHoldedChildChanged;

        public enum ParentUseType
        {
            useParentCenterPosition, useAnyDropColliderPosition
        }
        public ParentUseType currentParentUseType;
        public int number;
        public bool isAllowMultipleUse;
        public bool isParentTaken;
        public DragGame_Child currentHoldedChild;
        Animator anim;
        [HideInInspector]
        public Vector3 defaultScale;
        /// <summary>
        /// Хранит позицию при useAnyDropColliderPosition
        /// </summary>
        [HideInInspector]
        public Transform tempHolderDropPos;
        void Awake()
        {
            if (GetComponent<Animator>())
            {
                anim = GetComponent<Animator>();
            }
            defaultScale = transform.localScale;
        }

        public void SetHelper(bool _on)
        {
            //anim.SetBool("bloom", _on);
            if (_on)
            {
                if (ieAnimScaler != null) StopCoroutine(ieAnimScaler);
                ieAnimScaler = AnimScaler();
                StartCoroutine(ieAnimScaler);
            }
            else
            {
                if (ieAnimScaler != null) StopCoroutine(ieAnimScaler);
                ieAnimScaler = StaticParams.Scale(transform, defaultScale, 1);
                StartCoroutine(ieAnimScaler);
            }
        }

        IEnumerator ieAnimScaler;
        IEnumerator AnimScaler()
        {

            Vector3 target = new Vector3(transform.localScale.x + 0.15f, transform.localScale.y + 0.15f, 1);

            while (true)
            {
                do
                {
                    transform.localScale = Vector2.MoveTowards(transform.localScale, target, Time.deltaTime);
                    yield return null;

                } while (!Mathf.Approximately(transform.localScale.x, target.x));

                if (Mathf.Approximately(transform.localScale.x, defaultScale.x))
                {
                    target = new Vector3(defaultScale.x + 0.15f, defaultScale.y + 0.15f, 1);
                }
                else
                {
                    target = defaultScale;
                }
            }
        }

        //вызывается при успешном броске
        public void DropCorrect(DragGame_Child _dgc)
        {
            if (OnHoldedChildChanged != null)
            {
                OnHoldedChildChanged(this, currentHoldedChild, _dgc);
            }
            currentHoldedChild = _dgc;
            if ( isAllowMultipleUse)
            {
                isParentTaken = false;
            }else
            {
                isParentTaken = true;
            }
        }

        public int GetMaxOrderValue()
        {
            List<SpriteRenderer> list_Sprites = new List<SpriteRenderer>(
                GetComponentsInChildren<SpriteRenderer>(true));
            if (list_Sprites.Count == 0)
            {
                Debug.LogError("Нет ни одного рендерера!");
            }
            list_Sprites.Sort(delegate (SpriteRenderer us1, SpriteRenderer us2)
            { return us1.sortingOrder.CompareTo(us2.sortingOrder); });
            int maxVal = list_Sprites[list_Sprites.Count-1].sortingOrder;

            return maxVal;
        }
        public Vector2 GetRandomPosInsideCollider()
        {
            Vector3 rndPosWithin;
            rndPosWithin = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            if (GetComponent<BoxCollider2D>())
            {
                rndPosWithin = GetComponent<BoxCollider2D>().transform.TransformPoint(rndPosWithin * .5f);
            }
            else if (GetComponent<PolygonCollider2D>())
            {
                rndPosWithin = GetComponent<PolygonCollider2D>().transform.TransformPoint(rndPosWithin * .5f);
            }

            return rndPosWithin;
        }
    }
}