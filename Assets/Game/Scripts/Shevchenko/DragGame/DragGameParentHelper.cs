﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
    public class DragGameParentHelper : MonoBehaviour
    {
        public static DragGameParentHelper instance;
        public bool useOriginalParentAlpha;
        public float activeHelperAlpha = 0.4f;
        public float NotActiveHelperAlpha = 0f;

        List<DragGame_Parent> list_DragGame_Parent = new List<DragGame_Parent>();

        void Awake()
        {
            instance = this;
            foreach (var item in GameObject.FindObjectsOfType<DragGame_Parent>())
            {
                list_DragGame_Parent.Add(item);
                StartCoroutine(StaticParams.LerpColor(item.gameObject, NotActiveHelperAlpha, 100));
            }


        }


        public void SetHelperOneElement(string _name)
        {
            foreach (var item in list_DragGame_Parent)
            {
                if (item.name.Contains(_name) && !item.isParentTaken)
                {
                    StartCoroutine(StaticParams.LerpColor(item.gameObject, activeHelperAlpha, 2));
                    item.SetHelper(true);
                }
            }
        }

        public void SetHelperOneElement(DragGame_Parent _element)
        {
            foreach (var item in list_DragGame_Parent)
            {
                if (item == _element && !item.isParentTaken)
                {
                    StartCoroutine(StaticParams.LerpColor(item.gameObject, activeHelperAlpha, 2));
                    item.SetHelper(true);
                }
            }
        }
        public void StopHelperOneElement(DragGame_Parent _element, bool _needHideByColorLerp)
        {
            foreach (var item in list_DragGame_Parent)
            {
                if (item == _element)
                {
                    if (_needHideByColorLerp)
                    {
                        StartCoroutine(StaticParams.LerpColor(item.gameObject, NotActiveHelperAlpha, 2));
                    }

                    item.SetHelper(false);
                }
            }
        }
        public void SetHelperForAll()
        {
            foreach (var item in list_DragGame_Parent)
            {
                if (!item.isParentTaken)
                {
                    StartCoroutine(StaticParams.LerpColor(item.gameObject, activeHelperAlpha, 2));
                    item.SetHelper(true);
                }
            }
        }

        public void SetHelperForAllInObject(string _name)
        {
            foreach (var item in Helpers.Find(_name).GetComponentsInChildren<DragGame_Parent>())
            {
                if (!item.isParentTaken)
                {
                    StartCoroutine(StaticParams.LerpColor(item.gameObject, activeHelperAlpha, 2));
                    item.SetHelper(true);
                }
            }
        }
        public void StopHelperForAllInObject(string _name, bool _needHideByColorLerp)
        {
            foreach (var item in Helpers.Find(_name).GetComponentsInChildren<DragGame_Parent>())
            {
                if (_needHideByColorLerp)
                {
                    StartCoroutine(StaticParams.LerpColor(item.gameObject, NotActiveHelperAlpha, 2));
                }

                item.SetHelper(false);

            }
        }
        public void SetHelperForAllInObject(GameObject _go)
        {
            foreach (var item in _go.GetComponentsInChildren<DragGame_Parent>())
            {
                if (!item.isParentTaken)
                {
                    StartCoroutine(StaticParams.LerpColor(item.gameObject, activeHelperAlpha, 2));
                    item.SetHelper(true);
                }
            }
        }

    }
}