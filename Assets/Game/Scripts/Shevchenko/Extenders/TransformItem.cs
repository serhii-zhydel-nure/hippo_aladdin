﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TransformItem : MonoBehaviour
{
    public bool isInMoveNow;
    public bool isInScaleNow;
    public bool isInRotateNow;
    public bool isInColorLerpNow;

    public IEnumerator curMover;
    public IEnumerator curScaler;
    public IEnumerator curRotator;
    public IEnumerator curColorLerper;

    #region MOVE
    public void StopMover()
    {
        if (curMover != null)
        {
            StopCoroutine(curMover);
        }
    }
    public void MoveToCorrectPlace(Transform _from, Transform _to, float _speed = 1)
    {
        StopMover();
        curMover = IeMoveToCorrectPlace(_from, _to, _speed);
        StartCoroutine(curMover);
    }
    public void MoveToCorrectPlaceTowards(Transform _from, Transform _to, float _speed = 1)
    {
        StopMover();
        curMover = IeMoveToCorrectPlaceTowards(_from, _to, _speed);
        StartCoroutine(curMover);
    }
    public IEnumerator IeMoveToCorrectPlace(Transform _from, Transform _to, float _speed = 1)
    {
        isInMoveNow = true;
        do
        {
            _from.position = Vector2.Lerp(_from.position, _to.position, Time.deltaTime * _speed);
            yield return new WaitForEndOfFrame();
        } while (Vector2.Distance(_from.position, _to.position) > 0.005f);
        isInMoveNow = false;

    }
    public IEnumerator IeMoveToCorrectPlaceTowards(Transform _from, Transform _to, float _speed)
    {
        isInMoveNow = true;

        do
        {
            _from.position = Vector2.MoveTowards(_from.position, _to.position, Time.deltaTime * _speed);
            yield return new WaitForEndOfFrame();
        } while (Vector2.Distance(_from.position, _to.position) != 0);
        isInMoveNow = false;

    }

    #endregion

    #region ROTATE

    public void StopRotator()
    {
        if (curRotator != null)
        {
            StopCoroutine(curRotator);
        }
    }
    public void Rotate(Transform _from, Transform _to, float _speed = 1)
    {
        StopRotator();
        curRotator = IeRotate(_from, _to, _speed);
        StartCoroutine(curRotator);

    }
    public void Rotate(Transform _from, Vector3 _to, float _speed = 1)
    {
        StopRotator();
        curRotator = IeRotate(_from, _to, _speed);
        StartCoroutine(curRotator);

    }
    public IEnumerator IeRotate(Transform _from, Transform _to, float _speed = 1)
    {
        isInRotateNow = true;
        do
        {
            _from.rotation = Quaternion.Lerp(_from.rotation, _to.rotation, Time.deltaTime * _speed);
            yield return new WaitForEndOfFrame();
        } while (_from.rotation != _to.rotation);
        isInRotateNow = false;

    }
    public IEnumerator IeRotate(Transform _from, Vector3 _to, float _speed = 1)
    {
        isInRotateNow = true;
        do
        {
            _from.rotation = Quaternion.Lerp(_from.rotation, Quaternion.Euler(_to), Time.deltaTime * _speed);
            yield return new WaitForEndOfFrame();
        } while (_from.rotation != Quaternion.Euler(_to));
        isInRotateNow = false;

    }
    #endregion

    #region SCALE
    public void StopScale()
    {
        if (curScaler != null)
        {
            StopCoroutine(curScaler);
        }
    }
    public void Scale(Transform _from, Transform _to, float _speed = 1)
    {
        StopScale();
        curScaler = IeScale(_from, _to, _speed);
        StartCoroutine(curScaler);
    }
    public void Scale(Transform _from, Vector3 _to, float _speed)
    {
        StopScale();
        curScaler = IeScale(_from, _to, _speed);
        StartCoroutine(curScaler);
    }
    public IEnumerator IeScale(Transform _from, Transform _to, float _speed = 1)
    {
        isInScaleNow = true;
        do
        {
            _from.localScale = Vector3.Lerp(_from.localScale, _to.localScale, Time.deltaTime * _speed);
            yield return new WaitForEndOfFrame();
        } while (!Mathf.Approximately(_from.localScale.x, _to.localScale.x));
        isInScaleNow = false;

    }


    public IEnumerator IeScale(Transform _from, Vector3 _to, float _speed)
    {
        isInScaleNow = true;

        do
        {
            _from.localScale = Vector3.Lerp(_from.localScale, _to, Time.deltaTime * _speed);
            yield return new WaitForEndOfFrame();

        } while (!Mathf.Approximately(_from.localScale.x, _to.x));
        isInScaleNow = false;

    }


    #endregion

    #region COLOR
    public void StopColorLerp()
    {
        if (curColorLerper != null)
        {
            StopCoroutine(curColorLerper);
        }
    }
    public void LerpColor(Transform _go, float _colorTarget_a, float _speed = 1)
    {
        StopColorLerp();
        curColorLerper = IeLerpColor(_go, _colorTarget_a, _speed);
        StartCoroutine(curColorLerper);
    }
    public void LerpColorUI(Transform _go, float _colorTarget_a, float _speed = 1)
    {
        StopColorLerp();
        curColorLerper = IeLerpColorUI(_go, _colorTarget_a, _speed);
        StartCoroutine(curColorLerper);
    }
    public static IEnumerator IeLerpColor(Transform _go, float _colorTarget_a, float _speed = 1)
    {
        SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>(true);
        if (sprites.Length == 0)
        {
            Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
            yield break;
        }
        float _a = sprites[0].color.a;

        do
        {
            _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
            foreach (var sr in sprites)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }
            yield return new WaitForEndOfFrame();
        } while (_a != _colorTarget_a);

    }

    public static IEnumerator IeLerpColorUI(Transform _go, float _colorTarget_a, float _speed)
    {
        Image[] spritesUI = _go.GetComponentsInChildren<Image>(true);
        Text[] texts = _go.GetComponentsInChildren<Text>(true);

        if (spritesUI.Length == 0)
        {

            Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
            yield break;
        }
        float _a = spritesUI[0].color.a;

        do
        {
            _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
            foreach (var sr in spritesUI)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }
            foreach (var sr in texts)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }

            yield return new WaitForEndOfFrame();
        } while (_a != _colorTarget_a);


    }

    #endregion
}
