﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using PSV;
using System;
using System.Collections.Generic;

namespace Shevchenko
{
    public enum WalkDirection
    {
        left, right, none, inverse
    }
    public enum GameDifficulty
    {
        easy, hard
    }

    public class StaticParams : MonoBehaviour
    {
        public static GameDifficulty currentGameDifficulty = GameDifficulty.easy;
        public static bool isSound = true;
        public static bool isMusic = true;

        public static bool CkeckIsThisAnimPlaying(Animator _animator, string _animationName)
        {
            return _animator.GetCurrentAnimatorStateInfo(0).IsName(_animationName);
        }

        public static IEnumerator Say(PlayerController _pepa, string _phrase)
        {
            SoundEffectsController.StopAll();
            while (_pepa.Talk(_phrase)) yield return new WaitForEndOfFrame();
        }
        public static IEnumerator Say(PlayerController _pepa, string[] _phrase)
        {
            SoundEffectsController.StopAll();
            while (_pepa.Talk(_phrase)) yield return new WaitForEndOfFrame();
        }

        public static IEnumerator AutoFade(string _scene, bool _isNeedAds)
        {
            //        GameObject go = new GameObject();
            //        go.transform.parent = CreateFadeCanvas().transform;
            //        //}
            //        go.transform.parent = GameObject.FindObjectOfType<Canvas>().transform;
            //        go.AddComponent<RectTransform>();
            //        go.AddComponent<Image>();
            //        go.name = "AutoFade";
            //        go.GetComponent<RectTransform>().position = Vector2.zero;
            //        go.GetComponent<RectTransform>().sizeDelta = new Vector2(10000, 10000);

            //        go.SetActive(true);
            //        Image sr = go.GetComponent<Image>();

            //        float _a;
            //        _a = sr.color.a;

            //        do
            //        {
            //            _a = Mathf.MoveTowards(_a, 255, Time.deltaTime * 220);
            //            sr.color = new Color32((byte)sr.color.r, (byte)sr.color.g, (byte)sr.color.b, (byte)_a);

            //            go.GetComponent<Image>().color = new Color32((byte)sr.color.r, (byte)sr.color.g, (byte)sr.color.b, (byte)_a);

            //            yield return new WaitForEndOfFrame();
            //        } while (_a != 255);
            ////#if !UNITY_EDITOR
            ////        if (_isNeedAds) ManagerGoogle.Instance.ShowFullscreenBanner();
            ////#endif
            //        Application.LoadLevel(_scene);
            yield return new WaitForEndOfFrame();
            SceneLoader.SwitchToScene((Scenes)Enum.Parse(typeof(Scenes), _scene), SceneLoader.TransitionMethod.Tween);

        }
        static GameObject CreateFadeCanvas()
        {
            GameObject goCanv = new GameObject();
            goCanv.AddComponent<RectTransform>();
            goCanv.AddComponent<Canvas>();
            goCanv.GetComponent<Canvas>().sortingLayerName = "UI";
            goCanv.GetComponent<Canvas>().sortingOrder = 10000;
            goCanv.AddComponent<CanvasScaler>();
            goCanv.AddComponent<GraphicRaycaster>();
            goCanv.name = "CanvasFade";
            return goCanv;

        }
        public static IEnumerator AutoFade()
        {
            GameObject go = Helpers.Find("AutoFade");
            go.SetActive(true);
            Image sr = go.GetComponent<Image>();

            byte a = 255;
            sr.color = new Color32((byte)sr.color.r, (byte)sr.color.g, (byte)sr.color.b, (byte)a);

            float _a;
            _a = 255;// sr.color.a;

            do
            {
                _a = Mathf.MoveTowards(_a, 0, Time.deltaTime * 220);
                sr.color = new Color32((byte)sr.color.r, (byte)sr.color.g, (byte)sr.color.b, (byte)_a);

                yield return new WaitForEndOfFrame();
            } while (_a != 0);
            go.SetActive(false);
        }

        #region TRANSFORM METHODS
        public static IEnumerator MoveToCorrectPlace(Transform _from, Transform _to, float _speed = 1)
        {
            if (!_from.GetComponent<TransformItem>())
                _from.gameObject.AddComponent<TransformItem>();
            TransformItem ti = _from.GetComponent<TransformItem>();

            ti.MoveToCorrectPlace(_from, _to, _speed);
            yield return null;
            yield return new WaitWhile(() => ti.isInMoveNow);
        }
        public static IEnumerator MoveToCorrectPlaceTowards(Transform _from, Transform _to, float _speed)
        {
            if (!_from.GetComponent<TransformItem>())
                _from.gameObject.AddComponent<TransformItem>();
            TransformItem ti = _from.GetComponent<TransformItem>();

            ti.MoveToCorrectPlaceTowards(_from, _to, _speed);
            yield return null;
            yield return new WaitWhile(() => ti.isInMoveNow);

        }

        public static IEnumerator Rotate(Transform _from, Transform _to, float _speed = 1)
        {
            if (!_from.GetComponent<TransformItem>())
                _from.gameObject.AddComponent<TransformItem>();
            TransformItem ti = _from.GetComponent<TransformItem>();

            ti.Rotate(_from, _to, _speed);
            yield return null;
            yield return new WaitWhile(() => ti.isInRotateNow);
        }
        public static IEnumerator Rotate(Transform _from, Vector3 _to, float _speed = 1)
        {
            if (!_from.GetComponent<TransformItem>())
                _from.gameObject.AddComponent<TransformItem>();
            TransformItem ti = _from.GetComponent<TransformItem>();

            ti.Rotate(_from, _to, _speed);
            yield return null;
            yield return new WaitWhile(() => ti.isInRotateNow);
        }

        public static IEnumerator Scale(Transform _from, Vector3 _to, float _speed = 1)
        {
            if (!_from.GetComponent<TransformItem>())
                _from.gameObject.AddComponent<TransformItem>();
            TransformItem ti = _from.GetComponent<TransformItem>();

            ti.Scale(_from, _to, _speed);
            yield return null;
            yield return new WaitWhile(() => ti.isInRotateNow);
        }
        public static IEnumerator Scale(Transform _from, Transform _to, float _speed = 1)
        {
            if (!_from.GetComponent<TransformItem>())
                _from.gameObject.AddComponent<TransformItem>();
            TransformItem ti = _from.GetComponent<TransformItem>();

            ti.Scale(_from, _to, _speed);
            yield return null;
            yield return new WaitWhile(() => ti.isInRotateNow);
        }
        #endregion

        public static void SetLayer(string _ObjName, string _LayerName)
        {
            GameObject go = GameObject.Find(_ObjName);
            if (!go) go = Helpers.Find(_ObjName);
            if (go)
            {
                SpriteRenderer[] sr = go.GetComponentsInChildren<SpriteRenderer>(true);
                foreach (SpriteRenderer item in sr)
                {
                    item.sortingLayerName = _LayerName;
                }
            }
        }
        public static void SetLayer(Transform _Obj, string _LayerName)
        {
            //GameObject go = GameObject.Find(_ObjName);
            if (_Obj)
            {
                SpriteRenderer[] sr = _Obj.GetComponentsInChildren<SpriteRenderer>(true);
                foreach (SpriteRenderer item in sr)
                {
                    item.sortingLayerName = _LayerName;
                }
            }
        }
        public static void SetOrderInLayer(string _ObjName, int _sortingorder)
        {
            GameObject go = GameObject.Find(_ObjName);
            if (!go) go = Helpers.Find(_ObjName);

            if (go)
            {
                SpriteRenderer[] sr = go.GetComponentsInChildren<SpriteRenderer>(true);
                foreach (SpriteRenderer item in sr)
                {
                    item.sortingOrder = item.sortingOrder + _sortingorder;
                }
            }
        }
        public static void SetOrderInLayer(Transform _Obj, int _sortingorder)
        {
            //GameObject go = GameObject.Find(_ObjName);
            if (_Obj)
            {
                SpriteRenderer[] sr = _Obj.GetComponentsInChildren<SpriteRenderer>(true);
                foreach (SpriteRenderer item in sr)
                {
                    item.sortingOrder = item.sortingOrder + _sortingorder;
                }
            }
        }

        public static IEnumerator LerpColorUI(GameObject _go, float _colorTarget_a, float _speed)
        {
            Image[] spritesUI = _go.GetComponentsInChildren<Image>(true);
            Text[] texts = _go.GetComponentsInChildren<Text>(true);

            if (spritesUI.Length == 0)
            {

                Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
                yield break;
            }
            //Image sr = go.GetComponent<Image>();

            //byte a = 255;
            //sr.color = new Color32((byte)sr.color.r, (byte)sr.color.g, (byte)sr.color.b, (byte)a);
            float _a = spritesUI[0].color.a;

            do
            {
                _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
                foreach (var sr in spritesUI)
                {
                    sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
                }
                foreach (var sr in texts)
                {
                    sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
                }

                yield return new WaitForEndOfFrame();
            } while (_a != _colorTarget_a);


        }
        public static IEnumerator LerpColorUI(GameObject _go, float _colorTarget_a, float _speed, Dictionary<string, float> _list_Exclude)
        {
            List<Image> list_Sprites = new List<Image>(_go.GetComponentsInChildren<Image>(true));
            Text[] texts = _go.GetComponentsInChildren<Text>(true);

            //Image[] sprites = _go.GetComponentsInChildren<Image>(true);
            if (list_Sprites.Count == 0)
            {

                Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
                yield break;
            }
            //Image sr = go.GetComponent<Image>();

            //byte a = 255;
            //sr.color = new Color32((byte)sr.color.r, (byte)sr.color.g, (byte)sr.color.b, (byte)a);
            float _a = list_Sprites[0].color.a;

            do
            {
                _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
                foreach (var sr in list_Sprites)
                {
                    if (_list_Exclude.ContainsKey(sr.name))
                    {
                        float val;
                        _list_Exclude.TryGetValue(sr.name, out val);
                        if (_a < val)
                        {
                            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
                        }
                    }
                    else
                        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
                }
                foreach (var sr in texts)
                {
                    sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
                }

                yield return new WaitForEndOfFrame();
            } while (_a != _colorTarget_a);
        }

        public static IEnumerator LerpColor(GameObject _go, int _colorTarget_a, float _speed)
        {
            SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>(true);

            //Image sr = go.GetComponent<Image>();

            //byte a = 255;
            //sr.color = new Color32((byte)sr.color.r, (byte)sr.color.g, (byte)sr.color.b, (byte)a);
            float _a = sprites[0].color.a;

            do
            {
                _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
                foreach (var sr in sprites)
                {
                    if (sr.GetComponent<ColorLimit>())
                    {
                        ColorLimit lim = sr.GetComponent<ColorLimit>();
                        if (lim.staticColor != new Color(0, 0, 0, 0))
                        {
                            sr.color = new Color(lim.staticColor.r, lim.staticColor.g, lim.staticColor.b, Mathf.Clamp(_a, 0, lim.colorLimit));

                        }
                        else
                            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, Mathf.Clamp(_a, 0, lim.colorLimit));

                    }
                    else
                        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
                }


                yield return new WaitForEndOfFrame();
            } while (_a != _colorTarget_a);


            //float _a;
            //_a = sprites[0].color.a;// sr.color.a;

            //do
            //{
            //    _a = Mathf.MoveTowards(_a, 0, Time.deltaTime * 220);

            //    foreach (var item in sprites)
            //    {
            //        item.color = new Color32((byte)item.color.r, (byte)item.color.g, (byte)item.color.b, (byte)_a);
            //    }
            //    yield return new WaitForEndOfFrame();
            //} while (_a != 0);
        }
        public static IEnumerator LerpColor(GameObject _go, float _colorTarget_a, float _speed = 1)
        {
            SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>(true);
            if (sprites.Length == 0)
            {

                Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
                yield break;
            }
            //Image sr = go.GetComponent<Image>();

            //byte a = 255;
            //sr.color = new Color32((byte)sr.color.r, (byte)sr.color.g, (byte)sr.color.b, (byte)a);
            float _a = sprites[0].color.a;

            do
            {
                _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
                foreach (var sr in sprites)
                {
                    if (sr.GetComponent<ColorLimit>())
                    {
                        ColorLimit lim = sr.GetComponent<ColorLimit>();
                        if (lim.staticColor != new Color(0, 0, 0, 0))
                        {
                            sr.color = new Color(lim.staticColor.r, lim.staticColor.g, lim.staticColor.b, Mathf.Clamp(_a, 0, lim.colorLimit));

                        }
                        else
                            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, Mathf.Clamp(_a, 0, lim.colorLimit));

                    }
                    else
                    {
                        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);

                    }
                }


                yield return new WaitForEndOfFrame();
            } while (_a != _colorTarget_a);

        }
        public static IEnumerator LerpColor(GameObject _go, Color _col, float _speed = 1)
        {
            SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>(true);
            if (sprites.Length == 0)
            {
                Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
                yield break;
            }
            Color _a = sprites[0].color;
            do
            {
                //_a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
                foreach (var sr in sprites)
                {
                    if (sr.GetComponent<ColorLimit>())
                    {
                        ColorLimit lim = sr.GetComponent<ColorLimit>();
                        if (lim.staticColor != new Color(0, 0, 0, 0))
                        {
                            //sr.color = new Color(lim.staticColor.r, lim.staticColor.g, lim.staticColor.b, Mathf.Clamp(_a, 0, lim.colorLimit));
                            sr.color = Color.Lerp(sr.color, lim.staticColor, Time.deltaTime * _speed);

                        }
                        //else
                        //    sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, Mathf.Clamp(_a, 0, lim.colorLimit));

                    }
                    else
                    
                        sr.color = Color.Lerp(sr.color, _col, Time.deltaTime * _speed);
                }

                yield return new WaitForEndOfFrame();
            } while (_a != _col);

        }

        public static int[] GenerateRandomMas(int _max)
        {
            System.Random random = new System.Random();
            return Enumerable.Range(0, _max).OrderBy(n => random.Next()).ToArray();
        }

        public static void SetColor(GameObject _go, float _colorTarget_a)
        {
            Image[] sprites = _go.GetComponentsInChildren<Image>(true);
            Text[] texts = _go.GetComponentsInChildren<Text>(true);
            SpriteRenderer[] spritesSimple = _go.GetComponentsInChildren<SpriteRenderer>(true);


            foreach (var sr in sprites)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _colorTarget_a);
            }
            foreach (var sr in spritesSimple)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _colorTarget_a);
            }
            foreach (var sr in texts)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _colorTarget_a);
            }

        }
        public static void SetColor(GameObject _go, Color _targetCol)
        {
            Image[] sprites = _go.GetComponentsInChildren<Image>(true);
            Text[] texts = _go.GetComponentsInChildren<Text>(true);
            SpriteRenderer[] spritesSimple = _go.GetComponentsInChildren<SpriteRenderer>(true);


            foreach (var sr in sprites)
            {
                sr.color = _targetCol;
            }
            foreach (var sr in texts)
            {
                sr.color = _targetCol;
            }
            foreach (var sr in spritesSimple)
            {
                sr.color = _targetCol;
            }

        }


        public static void PushEffectFireworks()
        {
            GameObject go = (GameObject)Resources.Load<GameObject>("Effects/EffectsFireworks");
            SFX.Play("Фейерверк");
            //float randX = UnityEngine.Random.Range(Screen.width * 0.3f, Screen.width * 0.8f);
            //float randY = UnityEngine.Random.Range(Screen.height * 0.6f, Screen.height * 0.9f);

            //Debug.Log(randX+" "+randY);
            Instantiate
                (
                    go,
                    Camera.main.ScreenToWorldPoint
                            (
                            new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 10)
                            )
                     ,
                    Quaternion.identity
                );
        }

        /// <summary>
        /// достать номер переменной из имени
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public static int GetNumberFromName(string itemName)
        {
            int number = 0;
            string num = itemName.Substring(itemName.IndexOf("(") + 1);
            num = num.Replace(")", "");
            int.TryParse(num, out number);
            return number;
        }
        public static PlayerController GetPlayer(PlayerType _PlayerType)
        {
            foreach (var item in GameObject.FindObjectsOfType<PlayerController>())
            {
                if (item.playerType == _PlayerType)
                {
                    return item;
                }
            }
            return null;
        }

    }
}

public class Helpers : System.Object
{
    public static UnityEngine.Object Find(string name, System.Type type)
    {
        UnityEngine.Object[] objs = Resources.FindObjectsOfTypeAll(type);

        foreach (UnityEngine.Object obj in objs)
        {
            if (obj.name == name)
            {
                return obj;
            }
        }

        return null;
    }

    public static GameObject Find(string name)
    {
        UnityEngine.Object[] objs = Resources.FindObjectsOfTypeAll(typeof(GameObject));

        foreach (UnityEngine.Object obj in objs)
        {
            if (obj.name == name)
            {
                return obj as GameObject;
            }
        }

        return null;
    }

    public static GameObject[] FindAll(string name)
    {
        UnityEngine.Object[] objs = Resources.FindObjectsOfTypeAll(typeof(GameObject));
        int counter = 0;
        GameObject[] result;

        foreach (UnityEngine.Object obj in objs)
        {
            if (obj.name.Contains(name))
            {
                counter++;
            }
        }
        result = new GameObject[counter];

        counter = 0;
        for (int i = 0; i < objs.Length; i++)
        {
            if (objs[i].name.Contains(name))
            {
                result[counter] = objs[i] as GameObject;
                counter++;

            }
        }
        return result;
    }

}
public static class Extensions
{
    public static System.Collections.Generic.List<T> Shuffle<T>(this List<T> _List)
    {
        List<T> tempList = new List<T>(_List);

        for (int i = 0; i < tempList.Count; i++)
        {
            T temp = tempList[i];
            int randomIndex = UnityEngine.Random.Range(i, tempList.Count);
            tempList[i] = tempList[randomIndex];
            tempList[randomIndex] = temp;
        }
        return tempList;
    }
    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> _List)
    {
        IEnumerable<T> tempList = new T[_List.Count()];

        for (int i = 0; i < tempList.Count(); i++)
        {
            T temp = tempList.ElementAt(i);
            int randomIndex = UnityEngine.Random.Range(i, tempList.Count());
            var one = tempList.ElementAt(i);
            one = tempList.ElementAt(randomIndex);
            var two = tempList.ElementAt(randomIndex);
            two = temp;
        }
        return tempList;
    }
    public static System.Collections.Generic.List<T> Except<T>(this List<T> _List, List<T> _except)
    {
        List<T> tempList = new List<T>(_List);

        foreach (var item in _List)
        {
            if (!_except.Contains(item))
            {
                tempList.Add(item);
            }
        }

        return tempList;
    }
    public static System.Collections.Generic.List<T> Except<T>(this List<T> _List, T _except)
    {
        List<T> tempList = new List<T>(_List);

        foreach (var item in _List)
        {
            if (item.Equals(_except))
            {
                continue;
            }
            tempList.Add(item);

        }

        return tempList;
    }
    public static T RandomOne<T>(this IEnumerable<T> _List)
    {
        return _List.ElementAt(UnityEngine.Random.Range(0, _List.Count()));
    }

    public static IEnumerable<T> ClearNulls<T>(this IEnumerable<T> _List)
    {
        return _List.Where((item) => item != null);
    }
    public static List<T> ClearNulls<T>(this List<T> _List)
    {
        List<T> temp = new List<T>();
        foreach (var item in _List)
        {
            if (item == null)
            {
                Debug.Log("NULLL");

            }
            else
            {
                temp.Add(item);

            }
        }
        return temp;
        //return _List.Where((item) => item != null).ToList();
    }
    public static T Last<T>(this IEnumerable<T> _List)
    {
        return _List.ElementAt(_List.Count() - 1);
    }

    public static int GetNumberFromName(this string _name)
    {
        int number = 0;
        string num = _name.Substring(_name.IndexOf("(") + 1);
        num = num.Replace(")", "");
        int.TryParse(num, out number);
        return number;

    }
}

public static class TransformExtensions
{
    public static void MoveTo(this Transform _from, Transform _to, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.MoveToCorrectPlace(_from, _to, _speed);
    }
    public static void MoveTowardsTo(this Transform _from, Transform _to, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.MoveToCorrectPlaceTowards(_from, _to, _speed);
    }

    public static void Rotate(this Transform _from, Transform _to, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.Rotate(_from, _to, _speed);
    }
    public static void Rotate(this Transform _from, Vector3 _to, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.Rotate(_from, _to, _speed);
    }

    public static void Scale(this Transform _from, Vector3 _to, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.Scale(_from, _to, _speed);
    }
    public static void Scale(this Transform _from, Transform _to, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.Scale(_from, _to, _speed);
    }

    public static void LerpColor(this Transform _from, float _colorTarget_a, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.LerpColor(_from, _colorTarget_a, _speed);
    }
    public static void LerpColor(this GameObject _from, float _colorTarget_a, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.LerpColor(_from.transform, _colorTarget_a, _speed);
    }
    public static void LerpColorUI(this GameObject _from, float _colorTarget_a, float _speed = 1)
    {
        if (!_from.GetComponent<TransformItem>())
            _from.gameObject.AddComponent<TransformItem>();
        TransformItem ti = _from.GetComponent<TransformItem>();

        ti.LerpColorUI(_from.transform, _colorTarget_a, _speed);
    }
}
