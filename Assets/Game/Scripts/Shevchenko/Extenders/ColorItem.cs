﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ColorItem : MonoBehaviour
{

    public bool isColorLerpNow;
    public IEnumerator curMover;

    public void StopMover()
    {
        if (curMover != null)
        {
            StopCoroutine(curMover);
        }
    }

    public void LerpColorUI()
    {
        StopMover();

    }
    public IEnumerator IeLerpColorUI(GameObject _go, float _colorTarget_a, float _speed = 1)
    {
        isColorLerpNow = true;
        Image[] spritesUI = _go.GetComponentsInChildren<Image>(true);
        Text[] texts = _go.GetComponentsInChildren<Text>(true);

        if (spritesUI.Length == 0)
        {
            Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
            yield break;
        }
        float _a = spritesUI[0].color.a;

        do
        {
            _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
            foreach (var sr in spritesUI)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }
            foreach (var sr in texts)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }

            yield return new WaitForEndOfFrame();
        } while (_a != _colorTarget_a);
        isColorLerpNow = false;
    }
    public IEnumerator IeLerpColorUI(GameObject _go, float _colorTarget_a, float _speed, Dictionary<string, float> _list_Exclude)
    {
        isColorLerpNow = true;

        List<Image> list_Sprites = new List<Image>(_go.GetComponentsInChildren<Image>(true));
        Text[] texts = _go.GetComponentsInChildren<Text>(true);

        if (list_Sprites.Count == 0)
        {

            Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
            yield break;
        }
        float _a = list_Sprites[0].color.a;

        do
        {
            _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
            foreach (var sr in list_Sprites)
            {
                if (_list_Exclude.ContainsKey(sr.name))
                {
                    float val;
                    _list_Exclude.TryGetValue(sr.name, out val);
                    if (_a < val)
                    {
                        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
                    }
                }
                else
                    sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }
            foreach (var sr in texts)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }

            yield return new WaitForEndOfFrame();
        } while (_a != _colorTarget_a);
        isColorLerpNow = false;
    }

    public IEnumerator IeLerpColor(GameObject _go, int _colorTarget_a, float _speed = 1)
    {
        isColorLerpNow = true;

        SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>(true);
        float _a = sprites[0].color.a;

        do
        {
            _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
            foreach (var sr in sprites)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }


            yield return new WaitForEndOfFrame();
        } while (_a != _colorTarget_a);
        isColorLerpNow = false;

    }
    public IEnumerator IeLerpColor(GameObject _go, float _colorTarget_a, float _speed = 1)
    {
        isColorLerpNow = true;

        SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>(true);
        if (sprites.Length == 0)
        {

            Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
            yield break;
        }
        float _a = sprites[0].color.a;

        do
        {
            _a = Mathf.MoveTowards(_a, _colorTarget_a, Time.deltaTime * _speed);
            foreach (var sr in sprites)
            {
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, _a);
            }
            yield return new WaitForEndOfFrame();
        } while (_a != _colorTarget_a);
        isColorLerpNow = false;

    }
    public IEnumerator IeLerpColor(GameObject _go, Color _col, float _speed = 1)
    {
        isColorLerpNow = true;

        SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>(true);
        if (sprites.Length == 0)
        {
            Debug.Log("!!!!!!!!!!!!!!!! No Colors To Lerp");
            yield break;
        }
        Color _a = sprites[0].color;
        do
        {
            foreach (var sr in sprites)
            {
                sr.color = Color.Lerp(sr.color, _col, Time.deltaTime * _speed);
            }

            yield return new WaitForEndOfFrame();
        } while (_a != _col);
        isColorLerpNow = false;
    }
}
