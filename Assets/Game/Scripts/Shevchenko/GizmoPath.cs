﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Shevchenko
{
    public class GizmoPath : MonoBehaviour {
        public Color lineColor = Color.white;
        public bool isCircle;
        [Header("Искать только по имена а не все подряд")]
        public bool isUseName;
        public string nodeName;

        private List<Transform> nodes = new List<Transform>();

        void OnDrawGizmos()
        {
            Gizmos.color = lineColor;
            List<Transform> list_points;
            if (isUseName)
            {
                list_points = new List<Transform>(GetComponentsInChildren<Transform>()
    .Where(item => item.name.Contains(nodeName)));
             }else
            {
                list_points = new List<Transform>(GetComponentsInChildren<Transform>());
            }
            //foreach (var item in GetComponentsInChildren<Transform>())
            //{
            //    if (true)
            //    {

            //    }
            //}
            //Transform[] pathTransforms = GetComponentsInChildren<Transform>().Where(item =>item.name.Contains("WayPoint"));
            nodes = new List<Transform>();

            for (int i = 0; i < list_points.Count; i++)
            {
                if (list_points[i] != transform)
                {
                    nodes.Add(list_points[i]);
                }
            }

            if (!isCircle)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    Vector3 currentNode = nodes[i].position;
                    Vector3 previousNode = Vector3.zero;

                    if (i > 0)
                    {
                        previousNode = nodes[i - 1].position;

                        Gizmos.DrawLine(previousNode, currentNode);
                        Gizmos.DrawWireSphere(currentNode, 0.3f);
                    }
                    //else if (i == 0 && nodes.Count > 1)
                    //{
                    //    previousNode = nodes[nodes.Count - 1].position;
                    //}

                }
            }
            else
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    Vector3 currentNode = nodes[i].position;
                    Vector3 previousNode = Vector3.zero;

                    if (i > 0)
                    {
                        previousNode = nodes[i - 1].position;
                    }
                    else if (i == 0 && nodes.Count > 1)
                    {
                        previousNode = nodes[nodes.Count - 1].position;
                    }
                    Gizmos.DrawLine(previousNode, currentNode);
                    Gizmos.DrawWireSphere(currentNode, 0.3f);

                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            List<Transform> list_points;
            if (isUseName)
            {
                list_points = new List<Transform>(GetComponentsInChildren<Transform>()
    .Where(item => item.name.Contains(nodeName)));
            }
            else
            {
                list_points = new List<Transform>(GetComponentsInChildren<Transform>());
            }
            //foreach (var item in GetComponentsInChildren<Transform>())
            //{
            //    if (true)
            //    {

            //    }
            //}
            //Transform[] pathTransforms = GetComponentsInChildren<Transform>().Where(item =>item.name.Contains("WayPoint"));
            nodes = new List<Transform>();

            for (int i = 0; i < list_points.Count; i++)
            {
                if (list_points[i] != transform)
                {
                    nodes.Add(list_points[i]);
                }
            }

            if (!isCircle)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    Vector3 currentNode = nodes[i].position;
                    Vector3 previousNode = Vector3.zero;

                    if (i > 0)
                    {
                        previousNode = nodes[i - 1].position;

                        Gizmos.DrawLine(previousNode, currentNode);
                        Gizmos.DrawWireSphere(currentNode, 0.3f);
                    }
                    //else if (i == 0 && nodes.Count > 1)
                    //{
                    //    previousNode = nodes[nodes.Count - 1].position;
                    //}

                }
            }
            else
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    Vector3 currentNode = nodes[i].position;
                    Vector3 previousNode = Vector3.zero;

                    if (i > 0)
                    {
                        previousNode = nodes[i - 1].position;
                    }
                    else if (i == 0 && nodes.Count > 1)
                    {
                        previousNode = nodes[nodes.Count - 1].position;
                    }
                    Gizmos.DrawLine(previousNode, currentNode);
                    Gizmos.DrawWireSphere(currentNode, 0.3f);

                }
            }

        }

       
    }
}
