﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PSV;

namespace Shevchenko.nScene6
{
    public class Scene6 : GameController
    {
        List<Transform> list_SceneKists = new List<Transform>();
        int actionCounter;

        public override void Awake()
        {
            base.Awake();
            InitDrag();
        }

        void InitDrag()
        {
            List<Scene6_DecType> list_AllItems = new List<nScene6.Scene6_DecType>(FindObjectsOfType<Scene6_DecType>());
            foreach (var item in list_AllItems.Where(e => e.GetComponent<DragGame_Child>()))
            {
                List<DragGame_Parent> tempPar = new List<Shevchenko.DragGame_Parent>();
                foreach (var item2 in list_AllItems.Where(e => e.curDecType == item.curDecType).ToList())
                {
                    tempPar.Add(item2.GetComponent<DragGame_Parent>());

                }
                item.GetComponent<DragGame_Child>().SetTargetParents(tempPar);
            }

            List<DragGame_Child> tempChild = new List<DragGame_Child>();
            foreach (var item2 in list_AllItems.Where(e => e.GetComponent<DragGame_Child>()).ToList())
            {
                tempChild.Add(item2.GetComponent<DragGame_Child>());

            }

            DragGame.instance.OverrideLevelParams(1, tempChild, DragGameType.hardSetChildrenParentFromChildList);

        }
        void Update()
        {

        }
        
        public override void OnEnable()
        {
            DragGame.OnDropCorrect += DragGame_OnDropCorrect;
            DragGame_Parent.OnHoldedChildChanged += DragGame_Parent_OnHoldedChildChanged;
            DragGame.OnMoveToCorrectPlaceCompleted += DragGame_OnMoveToCorrectPlaceCompleted;
            PanelUIBase.OnSave += PanelUIBase_OnSave;
        }

      
        public override void OnDisable()
        {
            DragGame.OnDropCorrect -= DragGame_OnDropCorrect;
            DragGame_Parent.OnHoldedChildChanged -= DragGame_Parent_OnHoldedChildChanged;
            DragGame.OnMoveToCorrectPlaceCompleted -= DragGame_OnMoveToCorrectPlaceCompleted;
            PanelUIBase.OnSave -= PanelUIBase_OnSave;

        }
        private void PanelUIBase_OnSave()
        {
            StartCoroutine(GameSaved());
        }
        IEnumerator GameSaved()
        {
            PanelAlign.HideAllPanels();
            yield return new WaitForSeconds(.1f);
            //Мы создали шедевр.		hp-59
            Talker.instance.StopTalkingAndClearQueue();
            pepa.TalkQueue("hp-59");
            yield return new WaitWhile(() => pepa.amITalkingSomethingNow);
            PainterController.instance.SaveAllToFile();
            yield return new WaitForSeconds(.1f);

            MapProgressController.SetSceneCompleted();
                 SceneLoader.SwitchToScene(Scenes.Map);
         }
        private void DragGame_OnMoveToCorrectPlaceCompleted(DragGame_Child _child, DragGame_Parent _parent)
        {
            Scene6_DecType dt = _child.GetComponent<Scene6_DecType>();
            SFX.Play("Собирает элементы коврика, убирает вещи на чердаке, находит Джи на рынке");
            if (dt)
            {
                switch (dt.curDecType)
                {
                    case DecType.Ugol:
                        actionCounter++;
                        break;
                    case DecType.Frame:
                        actionCounter++;

                        break;
                    case DecType.Center:
                        actionCounter++;

                        break;
                    case DecType.Kist:
                        actionCounter++;
                        Scene6_KistScene.instance.SetFakeKists(_child.gameObject);

                        break;
                    default:
                        break;
                }
                if (actionCounter>=3)
                {
                    Scene6_PanelSave.instance.ShowHide(true);
                }

                foreach (var item in _child.GetComponentsInChildren<Transform>(true))
                {
                    if (item.name == "Shadow")
                    {
                        item.gameObject.LerpColor(0);
                    }
                }
            }
        }
        private void DragGame_OnDropCorrect(DragGame_Child _child, DragGame_Parent _parent)
        {
            //DragGame.instance.OverrideLevelParams(1, new List<DragGame_Child>(FindObjectsOfType<DragGame_Child>()),
            //    new List<DragGame_Parent>(FindObjectsOfType<DragGame_Parent>()));
            InitDrag();
            /*
              Так держать.		hp-55 
              Изумительно.		hp-56
              У тебя отличный вкус.		hp-57 
              Отличный выбор!		hp-58	
             */
            Talker.instance.StopTalkingAndClearQueue();
            pepa.TalkQueue(new string[] { "hp-55", "hp-56", "hp-57", "hp-58" },false);
        }
        private void DragGame_Parent_OnHoldedChildChanged(DragGame_Parent _currentParent, DragGame_Child _oldChild, DragGame_Child _newChild)
        {
            if (_oldChild)
            {
                StartCoroutine(StaticParams.LerpColor(_oldChild.gameObject, 0));
                Destroy(_oldChild.gameObject, 5);

            }
        }
        public override IEnumerator Process()
        {
            CameraController.instance.AllowMove(true).SetFollow(pepa.transform, 6).SetScale(7).SetSpeedMove(2);
                       yield return new WaitForSeconds(1);
            pepa.Walk("PepaWay (1)");
            george.Walk("GeorgeWay (2)");
            yield return new WaitWhile(() => pepa.IsWalkingNow());
            //DragGame.instance.OverrideLevelParams(4, DragGameType.getAnyByCompareNames);
            //DragGame.instance.OverrideLevelParams(1, new List<DragGame_Child>(FindObjectsOfType<DragGame_Child>()),
            //    new List<DragGame_Parent>(FindObjectsOfType<DragGame_Parent>()));
            yield return null;
            //Эти ковры так чудесны! Давай украсим ковер.		hp-53
             //Выбери узор.		hp-54
             pepa.TalkQueue("hp-53");
            pepa.TalkQueue("hp-54");
            yield return new WaitWhile(() => pepa.amITalkingSomethingNow);

            CameraController.instance.StopFollow() ;
            //CameraController.instance.SetTarget(-0.5f, 4.817634f).SetScale(5);
            CameraController.instance.SetTarget(-0.5f, 5.64f).SetScale(5);
            PanelAlign.ShowPanel("ScrollerMainItems");

        }
    }
}
