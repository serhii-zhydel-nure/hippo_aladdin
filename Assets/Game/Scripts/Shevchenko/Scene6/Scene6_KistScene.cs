﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko.nScene6
{

    public class Scene6_KistScene : MonoBehaviour
    {
        List<Transform> list_FakePoses = new List<Transform>();
        Transform KistFakeSTARTPos;
        List<GameObject> list_CurFakes = new List<GameObject>();


        public static Scene6_KistScene instance;
        private void Awake()
        {
            instance = this;

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "KistFakePos")
                {
                    list_FakePoses.Add(item);
                }
                else if (item.name == "KistFakeSTARTPos")
                {
                    KistFakeSTARTPos = item;
                }

            }
        }
        public void SetFakeKists(GameObject _kist)
        {

            foreach (var item in list_CurFakes)
            {
                StartCoroutine(StaticParams.LerpColor(item, 0));
                Destroy(item, 8);
            }
            list_CurFakes.Clear();
            foreach (var item in _kist.GetComponentsInChildren<Transform>(true))
            {
                item.gameObject.layer = 0;

                if (item.name == "Shadow")
                {
                    StaticParams.SetColor(item.gameObject, 0);
                }

            }
            for (int i = 0; i < 4; i++)
            {
                GameObject go = Instantiate(_kist);
                go.transform.position = KistFakeSTARTPos.position;
                Destroy(go.GetComponent<DragGame_Child>());
                Destroy(go.GetComponent<BoxCollider2D>());

                foreach (var item in go.GetComponentsInChildren<SpriteRenderer>(true))
                {
                    item.sortingOrder = 10020;
                }
                StartCoroutine(StaticParams.MoveToCorrectPlace(go.transform, list_FakePoses[i], 5));
                go.transform.Rotate(list_FakePoses[i].transform, 3);
                list_CurFakes.Add(go);
            }
        }
    }
}
