﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace Shevchenko.nScene6
{
    public class Scene6_PanelButton : MonoBehaviour
    {
        public PanelAlign currentPanel;

        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(() => PanelAlign.ShowPanel(currentPanel.name));
        }
    }
}
