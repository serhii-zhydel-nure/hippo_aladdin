﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko.nScene6
{
     public class Scene6_PanelsController : MonoBehaviour
    {
        List<PanelAlign> list_AllOtherPanels = new List<PanelAlign>();
        private void Awake()
        {
            foreach (var item in FindObjectsOfType<PanelAlign>())
            {
                if (item.name != "ScrollerMainItems")
                {
                    list_AllOtherPanels.Add(item);
                }
            }
        }
        private void OnEnable()
        {
            PanelAlign.OnPanelShow += PanelAlign_OnPanelShow;
        }
  
        private void OnDisable()
        {
            PanelAlign.OnPanelShow -= PanelAlign_OnPanelShow;

        }
        private void PanelAlign_OnPanelShow(PanelAlign panel)
        {
            foreach (var item in list_AllOtherPanels)
            {
                if (item != panel)
                {
                    item.HidePanel();
                }
            }
        }
    }
}
