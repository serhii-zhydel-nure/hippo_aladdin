﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Shevchenko;
namespace Shevchenko.nScene6
{
    public class Scene6_ScrollerController : MonoBehaviour
    {
    
        private void OnEnable()
        {
             DragGame.OnDropWrong += DragGame_OnDropWrong;
            DragGame.OnDropWrongNoPar += DragGame_OnDropWrongNoPar;
            DragGame.OnDropWrongOnePar += DragGame_OnDropWrongOnePar;
            DragGame.OnDropWrongAndMoveBackCompleted += DragGame_OnDropWrongAndMoveBackCompleted;
        }
        private void OnDisable()
        {
             DragGame.OnDropWrong -= DragGame_OnDropWrong;
            DragGame.OnDropWrongNoPar -= DragGame_OnDropWrongNoPar;
            DragGame.OnDropWrongOnePar -= DragGame_OnDropWrongOnePar;
            DragGame.OnDropWrongAndMoveBackCompleted -= DragGame_OnDropWrongAndMoveBackCompleted;

        }
        private void DragGame_OnDropWrongAndMoveBackCompleted(DragGame_Child element)
        {
            ReturnImage(element);
        }

        private void DragGame_OnDropWrongOnePar(DragGame_Child element)
        {
            //ReturnImage(element);
        }

        private void DragGame_OnDropWrongNoPar()
        {
            //ReturnImage(element);
        }

        private void DragGame_OnDropWrong(DragGame_Child element, DragGame_Parent parent)
        {
            //ReturnImage(  element);
        }
        void ReturnImage(DragGame_Child element)
        {
            Debug.Log("ReturnImage");
            foreach (var item in element.GetComponentsInChildren<SpriteRenderer>(true))
            {
                item.enabled = false;

            }
            foreach (var item in element.GetComponentsInChildren<Image>(true))
            {
                item.enabled = true;

            }
        }
        private void DragGame_OnDropStartedOnePar(DragGame_Child element)
        {

            foreach (var item in element.GetComponentsInChildren<Image>(true))
            {
                item.enabled = false;

            }
            foreach (var item in element.GetComponentsInChildren<SpriteRenderer>(true))
            {
                item.enabled = true;

            }
            Debug.Log("Message " + element.name, element.gameObject);
        }

    


 
    }
}
