﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LampEffect : MonoBehaviour
{
    public static LampEffect instance;
    public enum EffectType { first, second }
    public EffectType curEffectType;
    public float scale;

    List<ParticleSystem> list_Particles;
    bool isDisappear;

    private void Awake()
    {
        instance = this;
        list_Particles = new List<ParticleSystem>(GetComponentsInChildren<ParticleSystem>());
    }

    void Update()
    {
        switch (curEffectType)
        {
            case EffectType.first:
                foreach (var item in list_Particles)
                {
                    item.startSize = scale;
                }
                if (isDisappear)
                {
                    scale = Mathf.Lerp(scale, 0, Time.deltaTime * 2);
                }
                break;
            case EffectType.second:
                if (isDisappear)
                {
                    scale = Mathf.Lerp(scale, 0, Time.deltaTime * 2);
                    foreach (var item in list_Particles)
                    {
                        item.startSize = scale;

                    }
                }
                else
                {
                    foreach (var item in list_Particles)
                    {
                        if (item.name == "LampEffect")
                        {
                            item.startLifetime = scale;

 
                        }
                    }
                }

                break;
            default:
                break;
        }

    }
    public void SetScale(float _scale)
    {
        scale = _scale;
    }
    public void SetDisappear()
    {
        isDisappear = true;
    }
    public void LauchSecondSmoke()
    {

        foreach (var item in GetComponentsInChildren<Transform>(true))
        {
            if (item.name == "SecondCloud")
            {
                item.gameObject.SetActive(true);
            }
        }
    }
    public void StopSecondSmoke()
    {

        foreach (var item in GetComponentsInChildren<ParticleSystem>(true))
        {
            if (item.name == "SecondCloud")
            {
                item.Stop();
            }
        }
    }
}
