﻿using System.Collections.Generic;
using Scripts_SergeyZhidel.Extenders;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Camera))]
public class CameraObserve : MonoBehaviour
{
    public static CameraObserve instance;

    [Tooltip("Спрайт в пределах которого камера будет перемещаться")]
    public SpriteRenderer Borders;

    public bool UseInertia = true;
    [Tooltip("Коэффициент затухания инерции в каждом кадре")]
    [Range(0.2f, 0.99f)]
    public float InertiaReduse = 0.8f;

    [Header("Чтоб камера не приблежалась к самому краю спрайта")]
    public float LeftOffset = 2;
    public float RightOffset = 2;
    public float UpOffset = 2;
    public float DownOffset = 2;

    private Vector2 _minBorders;
    private Vector2 _maxBorders;
    private Camera _camera;

    private Vector2 _beginPressWordPos;

    //для инерции
    private Vector2 _swipeWord;
    private Vector2? _beginPressViewportPos = null;
    private float _timeStartSwipe = -1;
    private Vector2 _swipeDirection;
    private float _acceleration = 0;

    public bool IsScrollingNow = false;
    public bool onlyXScroll;

    Vector3 defAreaScale;
    public bool isCameraInMoveByScript;
    public Transform target;
    public float speedScriptMove =1;
    float scriptDeltaX, scriptDeltaY;

    private void Awake()
    {
        instance = this;
        defAreaScale = Borders.transform.localScale;

    }
    void Start()
    {
        _camera = GetComponent<Camera>();
        CalculateBorders();

        if (_beginPressViewportPos == null) //начат свайп
        {
            _beginPressViewportPos = _camera.ScreenToViewportPoint(Input.mousePosition);
            _beginPressWordPos = _camera.ScreenToWorldPoint(Input.mousePosition);
            _timeStartSwipe = Time.time;
        }
        else
        {
            Vector3 touchWordPos = _camera.ScreenToWorldPoint(Input.mousePosition);
            _swipeWord = -(new Vector2(touchWordPos.x, touchWordPos.y) - _beginPressWordPos);//для отталкивания свайпом, а не следования за ним, стоит минус
                                                                                             //расчет новой позиции камеры
            transform.position = new Vector3(Mathf.Clamp(transform.position.x + _swipeWord.x, _minBorders.x, _maxBorders.x),
             onlyXScroll ? transform.position.y : Mathf.Clamp(transform.position.y + _swipeWord.y, _minBorders.y, _maxBorders.y), -10.0f);
        }
    }

    public void EnableDisable(bool _enable)
    {
        //isEnabled = _enable;
        enabled = _enable;
        if (_enable)
        {
            _beginPressViewportPos = null;
        }
    }
    void CalculateBorders()
    {
        Vector2 spriteSize = Borders.bounds.size;
        Vector2 spriteCenter = Borders.bounds.center;
        Vector2 cameraSize = new Vector2(_camera.aspect * 2f * _camera.orthographicSize, 2f * _camera.orthographicSize);

        _minBorders = new Vector2(
          spriteCenter.x - spriteSize.x / 2 //левая граница спрайта
          + cameraSize.x / 2
          + LeftOffset,

          spriteCenter.y - spriteSize.y / 2 //нижняя граница спрайта
          + cameraSize.y / 2
          + DownOffset);

        _maxBorders = new Vector2(
          spriteCenter.x + spriteSize.x / 2 //правая граница спрайта
          - cameraSize.x / 2
          - RightOffset,

          spriteCenter.y + spriteSize.y / 2 //верхняя граница спрайта
          - cameraSize.y / 2
          - UpOffset);
    }
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }


    public void LateUpdate()
    {
       
        if (Borders.transform.hasChanged)
        {
             Borders.transform.hasChanged = false;
            CalculateBorders();
        }
        if (Input.GetMouseButton(0))//зажатие
        {
            if (!IsPointerOverUIObject())
            {
                if (_beginPressViewportPos == null) //начат свайп
                {
                    isCameraInMoveByScript = false;

                    _beginPressViewportPos = _camera.ScreenToViewportPoint(Input.mousePosition);
                    _beginPressWordPos = _camera.ScreenToWorldPoint(Input.mousePosition);
                    _timeStartSwipe = Time.time;
                }
                else
                {
                    if ((_beginPressWordPos - _camera.ScreenToWorldPoint(Input.mousePosition).ToVector2()).magnitude > 0.01f)
                    {
                        IsScrollingNow = true;

                        Vector3 touchWordPos = _camera.ScreenToWorldPoint(Input.mousePosition);
                        _swipeWord = -(new Vector2(touchWordPos.x, touchWordPos.y) - _beginPressWordPos);//для отталкивания свайпом, а не следования за ним, стоит минус
                                                                                                         //расчет новой позиции камеры
                        transform.position = new Vector3(Mathf.Clamp(transform.position.x + _swipeWord.x, _minBorders.x, _maxBorders.x),
                        onlyXScroll ? transform.position.y : Mathf.Clamp(transform.position.y + _swipeWord.y, _minBorders.y, _maxBorders.y), -10.0f);
                    }
                }
            }
        }
        else//отжатие
                  if (_beginPressViewportPos != null) //только отжали
        {
            Vector3 touchWordPos = _camera.ScreenToWorldPoint(Input.mousePosition);
            _swipeWord = -(new Vector2(touchWordPos.x, touchWordPos.y) - _beginPressWordPos);//для отталкивания свайпом, а не следования за ним, стоит минус
                                                                                             //расчет новой позиции камеры
            transform.position = new Vector3(Mathf.Clamp(transform.position.x + _swipeWord.x, _minBorders.x, _maxBorders.x),
             onlyXScroll ? transform.position.y : Mathf.Clamp(transform.position.y + _swipeWord.y, _minBorders.y, _maxBorders.y), -10.0f);

            //для расчета инерции нужны универсальные единицы для всех экранов, чтоб везде была одинаковая инерция
            Vector3 touchViewportPos = _camera.ScreenToViewportPoint(Input.mousePosition);
            Vector2 swipeViewport = -(new Vector2(touchViewportPos.x, touchViewportPos.y) - _beginPressViewportPos.Value);

            //высчитать ускорение
            _acceleration =
              swipeViewport.magnitude < 0.1f //если свайп был очень маленький (меньше 10% экрана)
              || Time.time - _timeStartSwipe > 0.25f ? 0  //или свайп длился дольше 0.25 секунды, то убрать инерцию
               : (swipeViewport.magnitude / (Time.time - _timeStartSwipe) * 2);

            //нормализировать направление свайпа
            _swipeDirection = swipeViewport.normalized;

            _beginPressViewportPos = null;
            IsScrollingNow = false;
        }
        else //следующий кадр
        {
            if (UseInertia && _acceleration > 0.2f)
            {
                Vector2 inertia = _swipeDirection * _acceleration * Time.deltaTime;
                _acceleration *= InertiaReduse;

                transform.position = new Vector3(Mathf.Clamp(transform.position.x + inertia.x, _minBorders.x, _maxBorders.x),
                onlyXScroll ? transform.position.y : Mathf.Clamp(transform.position.y + inertia.y, _minBorders.y, _maxBorders.y), -10.0f);
            }
        }
        if (isCameraInMoveByScript)
        {
            if (target)
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(
                    Mathf.Clamp(target.position.x+ scriptDeltaX, _minBorders.x, _maxBorders.x),
                    onlyXScroll ? transform.position.y :
                    Mathf.Clamp(target.position.y+ scriptDeltaY, _minBorders.y, _maxBorders.y), -10.0f),

                    Time.deltaTime * speedScriptMove);
             }

        }
    }
    public CameraObserve SetTarget(Transform _target, float _speed = 1)
    {
        target = _target;
        speedScriptMove = _speed;
        isCameraInMoveByScript = true;
        return this;
    }
    public CameraObserve SetScriptMoveDelta(float _x, float _y)
    {
        scriptDeltaX = _x;
        scriptDeltaY = _y;
        return this;

    }
}

