﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class Entity : MonoBehaviour
    {
        //public Color currentColor;

        //public EntityGenderType currentGender;
        public bool forPepa, forGeorge, forMama, forPApa;

        public EntityPart currentPart;
        [HideInInspector]
        public bool reserved;
        [HideInInspector]
        public bool isWantedEntityGotten;

        [Header("Масштаб")]

        public float scalePepa = 1;
         public float scaleGeorge = 1;
         public float scaleMama = 1;
         public float scalePApa = 1;
        [Header("Сдвиг позиции")]
        public Vector2 delta_Pepa_BodyClothPos;
        public Vector2 delta_George_BodyClothPos;
        public Vector2 delta_Mama_BodyClothPos;
        public Vector2 delta_PApa_BodyClothPos;

        [Header("Вращение")]
         public Vector3 delta_Pepa_BodyRot;
        public Vector3 delta_George_BodyRot;
        public Vector3 delta_Mama_BodyRot;
        public Vector3 delta_PApa_BodyRot;


        [HideInInspector]
        public string entityName;
        [HideInInspector]
        public GameObject goResourceCurrentEntity;
        [Header("Остальное")]

        public float scaleForCloud = 1;
        public Vector2 deltaPosCloud;
        public float angleRotationCloud;
        [HideInInspector]
        public Transform leg1, leg2;

        Animator EntityBloom;
        private void Awake()
        {
            if (currentPart == EntityPart.Leg)
            {
                foreach (Transform item in GetComponentsInChildren<Transform>(true))
                {
                    if (item.name == "Leg_r")
                    {
                        leg1 = item;
                    }
                    else if (item.name == "Leg_l")
                    {
                        leg2 = item;
                    }

                }
            }
        }
        void Start()
        {
            goResourceCurrentEntity = gameObject;//(GameObject)Resources.Load<GameObject>("Scene7/" + currentPart.ToString() + gameObject.name);

            entityName = name;
     
            foreach (Transform item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "EntityBloom")
                {
                    EntityBloom = item.GetComponent<Animator>();
                }
            }
            //foreach (SpriteRenderer item in transform.GetComponentsInChildren<SpriteRenderer>(true))
            //{
            //    item.color = currentColor;
            //}
        }
        public void TurnONBloom()
        {
            EntityBloom.SetBool("bloom", true);
        }
        public void TurnOFFBloom()
        {
            EntityBloom.SetBool("bloom", false);
            EntityBloom.gameObject.SetActive(false);
        }

        public void LegOrderWorker()
        {

        }

        public float GetTargetScaleKoef(EntityPlayer _EntityPlayer)
        {

            PlayerType playerType = _EntityPlayer.GetComponent<PlayerController>().playerType;

            switch (playerType)
            {
                case PlayerType.pEpa: return scalePepa;
                case PlayerType.george: return scaleGeorge;
                case PlayerType.mama: return scaleMama;
                case PlayerType.pApa: return scalePApa;
                case PlayerType.other: return 1;
                default: return 1;
            }
        }

        public Vector2 GetTargetDeltaPos(EntityPlayer _EntityPlayer)
        {

            PlayerType playerType = _EntityPlayer.GetComponent<PlayerController>().playerType;

            switch (playerType)
            {
                case PlayerType.pEpa: return delta_Pepa_BodyClothPos;
                case PlayerType.george: return delta_George_BodyClothPos;
                case PlayerType.mama: return delta_Mama_BodyClothPos;
                case PlayerType.pApa: return delta_PApa_BodyClothPos;
                case PlayerType.other: return Vector2.zero;
                default: return Vector2.zero;
            }
        }

        public Vector3 GetTargetDeltaRotation(EntityPlayer _EntityPlayer)
        {

            PlayerType playerType = _EntityPlayer.GetComponent<PlayerController>().playerType;

            switch (playerType)
            {
                case PlayerType.pEpa: return delta_Pepa_BodyRot;
                case PlayerType.george: return delta_George_BodyRot;
                case PlayerType.mama: return delta_Mama_BodyRot;
                case PlayerType.pApa: return delta_PApa_BodyRot;
                case PlayerType.other: return Vector2.zero;
                default: return Vector2.zero;
            }
        }
    }
}
