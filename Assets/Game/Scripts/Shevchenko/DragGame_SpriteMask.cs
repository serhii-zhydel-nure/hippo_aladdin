﻿using UnityEngine;
using System.Collections;

namespace Shevchenko
{
    public class DragGame_SpriteMask : MonoBehaviour
    {
        public static DragGame_SpriteMask instance;

        public Material matBase;
        public Material matChild;
        public Material matDefault;

        public SpriteRenderer spriteMaskBase;

        private void Awake()
        {
            instance = this;
               matBase = Resources.Load<Material>("SpriteMask/MaskSprite_BASE");
            matChild = Resources.Load<Material>("SpriteMask/MaskSprite_CHILD");
            matDefault = Resources.Load<Material>("SpriteMask/DefaultShaaderMat");
            InitMask();

        }

        public void InitMask()
        {
            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "SpriteMaskBase")
                {
                    if (!spriteMaskBase)
                    {
                        spriteMaskBase = item.GetComponent<SpriteRenderer>();
                    }
                    spriteMaskBase.material = matBase;
                }
            }

            foreach (var item in GetComponentsInChildren<DragGame_Child>(true))
            {

                foreach (var item2 in item.GetComponentsInChildren<SpriteRenderer>(true))
                {
                    item2.material = matChild;
                }
            }
        }
        private void OnEnable()
        {
            DragGame.OnDragStartedOnePar += DragGame_OnDragStartedOnePar;
            DragGame.OnDropWrongAndMoveBackCompleted += DragGame_OnDropWrongAndMoveBackCompleted;
        }
        private void OnDisable()
        {
            DragGame.OnDragStartedOnePar -= DragGame_OnDragStartedOnePar;
            DragGame.OnDropWrongAndMoveBackCompleted -= DragGame_OnDropWrongAndMoveBackCompleted;

        }
        private void DragGame_OnDropWrongAndMoveBackCompleted(DragGame_Child _child)
        {
            if (_child.isItemOnPanel)
            {
                SetMat(_child, matChild);
            }
        }

        private void DragGame_OnDragStartedOnePar(DragGame_Child _child)
        {
            if (_child.isItemOnPanel)
            {
                SetMat(_child, matDefault);
            }

        }


        void SetMat(DragGame_Child _child,Material mat)
        {

            foreach (var item in _child.GetComponentsInChildren<SpriteRenderer>(true))
            {
       
                item.material = mat;
            }
        }
       
    }
}
