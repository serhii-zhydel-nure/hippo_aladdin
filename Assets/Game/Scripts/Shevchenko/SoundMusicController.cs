﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
    public class SoundMusicController : MonoBehaviour
    {
        public static SoundMusicController instance;

        public static List<AudioClip> list_font_music = new List<AudioClip>();
        AudioSource audioSource;

        void Awake()
        {
            instance = this;


            if (list_font_music.Count == 0)
            {
                foreach (var item in Resources.LoadAll<AudioClip>("Sounds/Music"))
                {
                    if (item.name.Contains("music"))
                    {
                        list_font_music.Add(item);
                    }
                }
            }

        }
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.volume = 0.2f;
        }


        void Update()
        {
            if (StaticParams.isMusic)
            {
                if (!audioSource.isPlaying)
                {
                    audioSource.clip = list_font_music[Random.Range(0, list_font_music.Count)];
                    audioSource.Play();
                }
            }
            else
                audioSource.Stop();

        }
        public void SetVolume(float _volume)
        {
            if (!audioSource)
            {
                audioSource = GetComponent<AudioSource>();
            }
            else
                audioSource.volume = _volume;

        }
        public float GetVolume()
        {
            return audioSource.volume;
        }
    }
}