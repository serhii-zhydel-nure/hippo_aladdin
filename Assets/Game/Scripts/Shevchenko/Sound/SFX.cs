﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
namespace Shevchenko
{

    public class SFX : MonoBehaviour
    {
        public class TalkParams
        {
            public string targetName;
            public SoundTrack track;
        }
        static List<TalkParams> list_loops = new List<TalkParams>();
        static List<TalkParams> list_curSfx = new List<TalkParams>();

        public delegate void SFXPlay(SoundTrack track);
        public static event SFXPlay OnSFXStarted;
        public static event SFXPlay OnSFXFinished;

        public static void Play(string _clipName, float _volume = 1)
        {
            //Debug.Log("--SFX-> "+new System.Diagnostics.StackTrace());
            //_clipName = _clipName.Replace(" ", "");
            AudioClip clip = SoundEffectsController.SelectSingle(_clipName);

            if (StaticParams.isSound && clip)
            {
                 SoundTrack track = SoundTrack.PlaySound(clip, _volume);
                track.destroy_action += soundDestroyEvent;
                TalkParams tp = new TalkParams();
                tp.track = track;
                tp.targetName = _clipName;
                list_curSfx.Add(tp);

                if (OnSFXStarted != null)
                {
                    OnSFXStarted(track);
                }
            }

        }
        public static void Play(string[] _clipName, float _volume = 1)
        {
            //Debug.Log("--SFX-> "+new System.Diagnostics.StackTrace());
            //_clipName = _clipName.Replace(" ", "");
            AudioClip clip = SoundEffectsController.SelectSingle(_clipName.RandomOne());

            if (StaticParams.isSound && clip)
            {
                SoundTrack track = SoundTrack.PlaySound(clip, _volume);
                track.destroy_action += soundDestroyEvent;
                TalkParams tp = new TalkParams();
                tp.track = track;
                tp.targetName = clip.name;
                list_curSfx.Add(tp);

                if (OnSFXStarted != null)
                {
                    OnSFXStarted(track);
                }
            }

        }

        public static void PlayLoop(string _clipName, float _volume = 1)
        {
             AudioClip clip = SoundEffectsController.SelectSingle(_clipName);
            foreach (var item in list_loops)
            {
                if (item.targetName == _clipName)
                {
                    return;
                }
            }
            if (StaticParams.isSound && clip)
            {
                TalkParams tp = new TalkParams();
                tp.targetName = _clipName;

                SoundTrack track = SoundTrack.PlaySound(clip, _volume, 1, int.MaxValue);
                tp.track = track;
                list_loops.Add(tp);
                track.destroy_action += soundDestroyEvent;
                if (OnSFXStarted != null)
                {
                    OnSFXStarted(track);
                }
            }
        }
        public static void StopLoop(string _clipName)
        {
            foreach (var item in list_loops.ToArray())
            {
                if (item.targetName == _clipName)
                {
                    if (item.track)
                    {
                        item.track.AudioSrc.Stop();
                        Destroy(item.track.AudioSrc.gameObject);
                    }
                    list_loops.Remove(item);
                    break;
                }
            }
        }
        public static void StopSingle(string _clipName)
        {
            foreach (var item in list_curSfx.ToArray())
            {
                if (item.targetName == _clipName)
                {
                    if (item.track)
                    {
                        item.track.AudioSrc.Stop();
                        Destroy(item.track.AudioSrc.gameObject);
                    }
                    list_curSfx.Remove(item);
                    break;
                }
            }
        }

        public static bool IsSFXPlayingNow(string _clipName)
        {
            foreach (var item in list_curSfx)
            {
                if (item.targetName == _clipName)
                {
                    return true;
                }
            }
            foreach (var item in list_loops)
            {
                if (item.targetName == _clipName)
                {
                    return true;
                }
            }
            return false;
        }
        static void soundDestroyEvent(SoundTrack track, bool atEndOfSound, float offset, AudioClip _clip)
        {
            foreach (var item in list_curSfx)
            {
                if (item.track == track)
                {
                    list_curSfx.Remove(item);
                    break;
                }
            }
            if (OnSFXFinished != null)
            {
                OnSFXFinished(track);
            }
        }
        private void OnDisable()
        {
            foreach (var item in list_loops.ToArray())
            {
                if (item.track)
                {
                    item.track.AudioSrc.Stop();
                    Destroy(item.track.AudioSrc.gameObject);
                }
            }
            list_loops.Clear();
        }

    }
}
