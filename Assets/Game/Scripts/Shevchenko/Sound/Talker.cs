﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using PSV.Audio;

namespace Shevchenko
{
    public class Talker : MonoBehaviour
    {
        public static Talker instance;

        public List<string> list_usedPhrases = new List<string>();

        public class TalkParams
        {
            public string targetName;
            public SoundTrack track;
        }
        public delegate void TalkEvent(TalkParams _tp);
        public static event TalkEvent OnTalkStarted;
        public static event TalkEvent OnTalkFinished;
        string lastRandomPhrase;
        List<TalkParams> list_TalkQueue = new List<TalkParams>();
        public bool isTalkingNow;
        private void Awake()
        {
            instance = this;
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                StopTalkingAndClearQueue();
            }
            //Debug.Log("isTalkingNow "+ isTalkingNow);
        }
        public void StopTalkingAndClearQueue()
        {
            foreach (var item in list_TalkQueue)
            {
                if (item.track != null)
                {
                    if (OnTalkFinished != null)
                    {
                        OnTalkFinished(item);
                    }
                    item.track.AudioSrc.Stop();
                }
            }
            list_TalkQueue.Clear();
        }
        public void TalkQueue(string[] _name)
        {

            list_usedPhrases.AddRange(_name);
            foreach (var item in _name)
            {
                TalkQueue(item);
            }
        }
        public void TalkQueue(string _name, bool _checkIsPhraseUsed = false)
        {
            //Debug.Log("--TalkQueue->  " + _name + new System.Diagnostics.StackTrace());
            if (_checkIsPhraseUsed)
            {
                if (list_usedPhrases.Contains(_name))
                {

                    return;
                }
            }
            list_usedPhrases.Add(_name);
            TalkParams tp = new TalkParams();
            tp.targetName = _name;

            if (!isTalkingNow)
            {
                 DoTalk(tp);
            }
            list_TalkQueue.Add(tp);
            isTalkingNow = true;
        }
        public string TalkQueueRandom(string[] _name, bool _checkIsPhraseUsed = false)
        {

            if (_name.Length > 1)
            {
                System.Random random = new System.Random();

                var start = Enumerable.Range(0, _name.Length).OrderBy(n => random.Next()).ToArray();
                if (lastRandomPhrase == _name[start[0]])
                {
                    lastRandomPhrase = _name[start[1]];
                }
                else lastRandomPhrase = _name[start[0]];
            }
            else
            {
                lastRandomPhrase = _name[0];
            }

            if (_checkIsPhraseUsed)
            {
                if (list_usedPhrases.Contains(lastRandomPhrase))
                {
                    Debug.Log("Фраза была использована ранее!");
                    return "";
                }
            }
            TalkParams tp = new TalkParams();
            tp.targetName = lastRandomPhrase;
            list_usedPhrases.Add(lastRandomPhrase);

            if (!isTalkingNow)
            {
                DoTalk(tp);
            }
            list_TalkQueue.Add(tp);
            isTalkingNow = true;
            return tp.targetName;
        }
        void DoTalk(TalkParams _tp)
        {
            _tp.targetName = _tp.targetName.Replace(" ", "");
            //AudioClip clip = SoundEffectsController.SelectSingle(_tp.targetName);
            AudioClip clip = LanguageAudio.GetSoundByName(_tp.targetName);
            foreach (var item in SoundEffectsController.list_sound_effects)
            {
                if (item.name == _tp.targetName)
                {
                    clip = item;
                }
            }
            if (StaticParams.isSound && clip)
            {

                SoundTrack track = SoundTrack.PlaySound(clip);
                track.destroy_action += soundDestroyEvent;
                _tp.track = track;
            }
            if (OnTalkStarted != null)
            {
                OnTalkStarted(_tp);
            }
        }
        void soundDestroyEvent(SoundTrack track, bool atEndOfSound, float offset, AudioClip _clip)
        {
        
             foreach (var item in list_TalkQueue)
            {
                if (item.track == track)
                {
                    if (OnTalkFinished != null)
                    {
                        OnTalkFinished(item);
                    }
                    list_TalkQueue.Remove(item);
                    break;
                }
            }
           
            if (list_TalkQueue.Count > 0)
            {
                DoTalk(list_TalkQueue[0]);
            }
            else
            {
                isTalkingNow = false;
            }
           
        }
        private void OnDestroy()
        {
            StopTalkingAndClearQueue();
        }
    }
}
