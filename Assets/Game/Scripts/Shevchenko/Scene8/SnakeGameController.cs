﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

using PSV;

public enum CurrentWalkStateDir
{
    none,
    left,
    right,
    up,
    down
}

public enum CurrentGameState
{
    play,
    pause,
    win
}
namespace Shevchenko
{
    [DisallowMultipleComponent]
    public class SnakeGameController : MonoBehaviour
    {
        public bool isTutorialLevel;

        //static List<SnakePlayer> list_PlayersToLoad = new List<SnakePlayer>();
        public static SnakeGameController instance;
        public List<SnakePlayer> list_TrainPlayers = new List<SnakePlayer>();
        public List<SnakePlayer> list_AllPlayersInScene = new List<SnakePlayer>();

        public SnakePlayer mainPlayer, SnakePlayer_Pepa;
        public static CurrentWalkStateDir currentWalkStateDir;
        public CurrentGameState currentGameState;
        public float distanceForGetPlayer = 2;
        public float distanceBetweenPlayers = 2;
        public float speed = 3;
        protected GameObject goSourceEffectWin;
        Button ButtonLeft, ButtonRight;

        TextMesh textPoints, textCharacters;
        public static int score;
        //int targetCountPlayersToCollect;

        private float tutorialTimer;
        private bool tutorialAllowed;

        public bool gameOver;

        public int keysCount = -1;
        //	[HideInInspector]
        public int keysCollected;

        //public AudioClip keySound, doorOpenSound, doorCloseSound, fireworksSound;
        public List<AudioClip> screams;
        public List<AudioClip> laughters;
        public List<AudioClip> dudkas;
        public AudioClip marakas;
        public bool lastLevel;

        public bool showExit;
        public GameObject arrowPrefab;

        public Scenes nextScene;
        public AudioClip backgroundMusic;

        protected virtual void Awake()
        {
            //		PlayerPrefs.DeleteAll ();
            instance = this;
            gameOver = false;
            tutorialTimer = 2;



            goSourceEffectWin = Resources.Load<GameObject>("Effects");

            screams = new List<AudioClip>();
            laughters = new List<AudioClip>();
            dudkas = new List<AudioClip>();
            foreach (var item in Resources.LoadAll<AudioClip>("Sounds/SFX"))
            {
                if (item.name.Contains("kriki"))
                {
                    screams.Add(item);
                }
                if (item.name.Contains("smex"))
                {
                    laughters.Add(item);
                }
                if (item.name.Contains("dydka"))
                {
                    dudkas.Add(item);
                }

            }
            backgroundMusic = Resources.Load<AudioClip>("Sounds/SFX/Loop 6 song");
            if (GameObject.Find("ButtonLeft")) ButtonLeft = GameObject.Find("ButtonLeft").GetComponent<Button>();
            if (GameObject.Find("ButtonRight")) ButtonRight = GameObject.Find("ButtonRight").GetComponent<Button>();

            if (ButtonLeft) ButtonLeft.onClick.AddListener(() =>
                {
                    switch (currentWalkStateDir)
                    {
                        case CurrentWalkStateDir.none:
                            break;
                        case CurrentWalkStateDir.left:
                            Down();
                            break;
                        case CurrentWalkStateDir.right:
                            Up();
                            break;
                        case CurrentWalkStateDir.up:
                            Left();
                            break;
                        case CurrentWalkStateDir.down:
                            Right();
                            break;
                        default:
                            break;
                    }
                });
            if (ButtonRight) ButtonRight.onClick.AddListener(() =>
                  {


                      switch (currentWalkStateDir)
                      {
                          case CurrentWalkStateDir.none:
                              break;
                          case CurrentWalkStateDir.left:
                              Up();
                              break;
                          case CurrentWalkStateDir.right:
                              Down();
                              break;
                          case CurrentWalkStateDir.up:
                              Right();
                              break;
                          case CurrentWalkStateDir.down:
                              Left();
                              break;
                          default:
                              break;
                      }
                  });
        }

        IEnumerator tutorialCoroutine;

        void Start()
        {

            if (isTutorialLevel)
            {
                CameraController.instance.AllowMove(true);
                tutorialCoroutine = Tutorial();
                StartCoroutine(tutorialCoroutine);
            }
            Play();
        }

        Animator animTutorial;
        CurrentWalkStateDir tutorialCurrentWalkStateDir;
        bool tutorialCorrectAction;

        IEnumerator Tutorial()
        {
            yield return new WaitForEndOfFrame();
            currentGameState = CurrentGameState.pause;
            foreach (var item in list_TrainPlayers)
            {
                item.animPlayer.Play("Ezda_odevanie");
                item.WalkPause(WalkDirection.none);
            }
            yield return new WaitForSeconds(3);

            currentGameState = CurrentGameState.play;

            tutorialCurrentWalkStateDir = CurrentWalkStateDir.none;
            animTutorial = GameObject.Find("Tutorial").GetComponent<Animator>();
            speed = 1.5f;
            yield return new WaitForEndOfFrame();

            tutorialCurrentWalkStateDir = CurrentWalkStateDir.up;
            while (SnakePlayer_Pepa.Talk ("hp-8" ))
                yield return new WaitForEndOfFrame();
            animTutorial.Play("up");
            while (SnakePlayer_Pepa.Talk("hp-9" ))
                yield return new WaitForEndOfFrame();

            //подсветка кнопки
            while (SnakePlayer_Pepa.Talk("hp-10" ))
                yield return new WaitForEndOfFrame();
            //эту фразу вызывать если нет действия 3 секунды
            tutorialAllowed = true;

            yield return new WaitWhile(() => !tutorialCorrectAction);
            yield return new WaitForEndOfFrame();
            tutorialCorrectAction = false;
            tutorialAllowed = false;
            tutorialTimer = 1.5f;
            animTutorial.ResetTrigger("done");
            animTutorial.SetBool("bDone", false);

            tutorialCurrentWalkStateDir = CurrentWalkStateDir.left;
            while (SnakePlayer_Pepa.Talk("hp-12" ))
                yield return new WaitForEndOfFrame();
            animTutorial.Play("left");
            while (SnakePlayer_Pepa.Talk("hp-9" ))
                yield return new WaitForEndOfFrame();
            tutorialAllowed = true;
            yield return new WaitWhile(() => !tutorialCorrectAction);
            yield return new WaitForEndOfFrame();
            tutorialCorrectAction = false;
            tutorialAllowed = false;
            tutorialTimer = 1.5f;
            animTutorial.ResetTrigger("done");
            animTutorial.SetBool("bDone", false);

            tutorialCurrentWalkStateDir = CurrentWalkStateDir.down;
            while (SnakePlayer_Pepa.Talk("hp-12" ))
                yield return new WaitForEndOfFrame();
            animTutorial.Play("down");
            while (SnakePlayer_Pepa.Talk("hp-9" ))
                yield return new WaitForEndOfFrame();
            //        animTutorial.Play("down");
            tutorialAllowed = true;
            yield return new WaitWhile(() => !tutorialCorrectAction);
            yield return new WaitForEndOfFrame();
            tutorialCorrectAction = false;
            tutorialAllowed = false;
            tutorialTimer = 1.5f;
            animTutorial.ResetTrigger("done");
            animTutorial.SetBool("bDone", false);

            tutorialCurrentWalkStateDir = CurrentWalkStateDir.right;
            while (SnakePlayer_Pepa.Talk("hp-12" ))
                yield return new WaitForEndOfFrame();
            animTutorial.Play("right");
            while (SnakePlayer_Pepa.Talk("hp-9" ))
                yield return new WaitForEndOfFrame();
            //        animTutorial.Play("right");
            tutorialAllowed = true;
            yield return new WaitWhile(() => !tutorialCorrectAction);
            yield return new WaitForEndOfFrame();
            tutorialCorrectAction = false;
            tutorialAllowed = false;
            tutorialTimer = 1.5f;
            //animTutorial.ResetTrigger("done");
            //animTutorial.SetBool("bDone", false);

            speed = 2;

            while (SnakePlayer_Pepa.Talk("hp-13" ))
                yield return new WaitForEndOfFrame();
            PlayerPrefs.SetInt("TutorialFinished", 1);
        }

        //	void OnTriggerEnter2D(Collider2D other)
        //	{
        //		if (other.tag == "Player" && !tutorialCorrectAction)
        //		{
        //			StartCoroutine ("Warning");
        //		}
        //	}
        protected virtual void Play()
        {
            list_AllPlayersInScene.Clear();
            list_TrainPlayers.Clear();

            LoadPlayers();
            SnakePlayer_Pepa = GameObject.Find("Peppa").GetComponent<SnakePlayer>();

            CameraController.instance.SetFollow(mainPlayer.transform);

            //list_TrainPlayers.Add(GameObject.Find("George (1)").GetComponent<SnakePlayer>());

            currentWalkStateDir = CurrentWalkStateDir.right;
            currentGameState = CurrentGameState.play;
            foreach (var item in list_TrainPlayers)
            {
                item.Walk(30, 0);
                item.amIInTrain = true;
            }
            foreach (var item in GameObject.FindObjectsOfType<SnakePlayer>())
            {
                list_AllPlayersInScene.Add(item);
            }
            foreach (var item in list_AllPlayersInScene)
            {
                item.animPlayer.SetInteger("Kulbits", UnityEngine.Random.Range(1, 3));
                if (!item.amIInTrain)
                {
                    item.animPlayer.Play("Ezda_odevanie");
                }
            }
            SnakePlayer.OnFunElementGet += SnakePlayer_OnFunElementGet;
            StartCoroutine("LayerWork");
            //FunElementsGlobalController.instance.Start_FunElementsWorker();

        }



        ///------ EVENT METHODS

        public void SnakePlayer_OnGameover()
        {

            Debug.Log("Message");
            currentGameState = CurrentGameState.pause;
            gameOver = true;
            foreach (var item in list_TrainPlayers)
            {
                if (item.isMainPlayer)
                {
                    item.WalkPause(WalkDirection.none);

                }
                //			else
                //			{
                //				item.LeaveSnake();
                //			}
            }
            StartCoroutine(LeaveSnakeDelayed(list_TrainPlayers));

            //SnakePlayer.OnGameover -= SnakePlayer_OnGameover;
            //SnakePlayer.OnSelfKick -= SnakePlayer_OnSelfKick;
            SnakePlayer.OnFunElementGet -= SnakePlayer_OnFunElementGet;
            if (animTutorial)
            {
                animTutorial.Play("TutorialFinger_DefaultHidden");
            }
            SoundEffectsController.StopAll();
            if (tutorialCoroutine != null)
            {
                StopCoroutine(tutorialCoroutine);
            }
            StopCoroutine("LayerWork");
            StartCoroutine("TalkGameover");
        }

        IEnumerator LeaveSnakeDelayed(List<SnakePlayer> pc)
        {
            for (int i = pc.Count - 1; i >= 0; i--)
            {
                pc[i].LeaveSnake();
                RandomLaughter();
                yield return new WaitForSeconds(0.2f);
            }

        }

        IEnumerator TalkGameover()
        {
            SoundEffectsController.StopAll();
            while (SnakePlayer_Pepa.Talk("hp-20" ))
                yield return new WaitForEndOfFrame();
            while (SnakePlayer_Pepa.Talk("hp-21" ))
                yield return new WaitForEndOfFrame();
        }

        public void SnakePlayer_OnSelfKick(SnakePlayer _pc)
        {
            for (int i = list_TrainPlayers[0].mySnakeNumber; i <= _pc.mySnakeNumber; i++)
            {
                foreach (var item in list_TrainPlayers)
                {
                    if (item.mySnakeNumber == i)
                    {
                        item.LeaveSnake();
                        list_TrainPlayers.Remove(item);
                        break;
                    }
                }
            }

        }

        public void SnakePlayer_OnFunElementGet(FunElement _fe)
        {

            //FunElementsGlobalController.instance.CollectNewFunElement(_fe);
        }

        /// <summary>
        ///  LEVEL COMPLETED -- WIN
        /// </summary>
        public virtual void SnakePlayer_OnDoorExitReached()
        {
            if (list_AllPlayersInScene.Count == list_TrainPlayers.Count)
            {
                Debug.Log("nLEVEL COMPLETED");
                //			textGui2 += "\nLEVEL COMPLETED";
                currentGameState = CurrentGameState.win;
                //SnakePlayer.OnDoorExitReached -= SnakePlayer_OnDoorExitReached;
                StartCoroutine(WinEffect());

            }
        }

        IEnumerator WinEffect()
        {

            for (int i = 0; i < 6; i++)
            {
                //			float randX = UnityEngine.Random.Range(Screen.width * 0.3f, Screen.width * 0.8f);
                //			float randY = UnityEngine.Random.Range(Screen.height * 0.6f, Screen.height * 0.9f);

                float randX = UnityEngine.Random.Range(transform.position.x * 0.8f, transform.position.x * 1.2f);
                float randY = UnityEngine.Random.Range(transform.position.y * 0.8f, transform.position.y * 1.2f);
                //Debug.Log(randX+" "+randY);
                Instantiate
                    (
                    goSourceEffectWin,
                    new Vector3(randX, randY, 0)
                         //				GetComponent<Camera>().ScreenToWorldPoint
                         //                            (
                         //					new Vector3(randX, randY, 0)
                         //					new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0)
                         //					Vector3.zero
                         //				)
                         ,
                    Quaternion.identity
                );
                yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.3f));
            }
        }

        int enterCounter;

        public void SnakePlayer_OnDoorEnterReachedAnyPlayer()
        {
            CameraController.instance.AllowMove(true);
            enterCounter++;
            if (enterCounter == list_TrainPlayers.Count)
            {

                StartCoroutine(CloseDoor());
                //animEnterDoor.transform.root.GetComponent<LayerWorker>().isAllovedChangeOrder = false;
            }
            else
            {
                Debug.Log("SnakePlayer_OnDoorEnterReachedAnyPlayer");
                //			animEnterDoor.SetBool("isOpen", true);
            }
        }

        IEnumerator CloseDoor()
        {
            yield return new WaitForSeconds(1);
            //		animEnterDoor.SetBool("isOpen", false);
        }

        protected int exitCounter;

        public virtual void SnakePlayer_OnDoorExitReachedAnyPlayer()
        {
            exitCounter++;

            Debug.Log(" exitCounter " + exitCounter);
            if (exitCounter == list_TrainPlayers.Count)
            {
                Debug.Log("all Exit");

                SavePlayers();

                //SnakePlayer.OnGameover -= SnakePlayer_OnGameover;
                //SnakePlayer.OnSelfKick -= SnakePlayer_OnSelfKick;
                SnakePlayer.OnFunElementGet -= SnakePlayer_OnFunElementGet;
                //SnakePlayer.OnDoorExitReached -= SnakePlayer_OnDoorExitReached;
                //SnakePlayer.OnDoorExitReachedAnyPlayer -= SnakePlayer_OnDoorExitReachedAnyPlayer;
                //SnakePlayer.OnDoorEnterReachedAnyPlayer -= SnakePlayer_OnDoorEnterReachedAnyPlayer;
                if (tutorialCoroutine != null)
                {
                    StopCoroutine(tutorialCoroutine);
                }
                //			if (!lastLevel)
                //			{
                //				StartCoroutine (StaticParams.AutoFade (Application.loadedLevel + 12, true));
                //			}
                //			else
                //			{
                //				StartCoroutine (StaticParams.AutoFade (15, false));
                //			}
                SceneLoader.SwitchToScene(nextScene);

                //			Application.LoadLevel(Application.loadedLevel + 1);
            }
        }

        IEnumerator Warning()
        {
            while (SnakePlayer_Pepa.Talk("hp-11" ))
                yield return new WaitForEndOfFrame();
            SoundEffectsController.StopAll();
        }


        protected virtual void Update()
        {
            if (!tutorialCorrectAction && tutorialAllowed)
            {
                tutorialTimer -= Time.deltaTime;
                if (tutorialTimer < 0)
                {
                    Debug.Log("Starting WARNING");
                    StartCoroutine(Warning());
                    tutorialAllowed = false;
                    tutorialTimer = 1.5f;
                }
            }
            if ((currentGameState == CurrentGameState.play || currentGameState == CurrentGameState.win) && currentGameState != CurrentGameState.pause)
            {
                foreach (var item in list_TrainPlayers)
                {
                    //				item.animPlayer.StartPlayback ();
                }
                PlayerDirectionWorker();
                CheckNearestPlayers();
                CheckGameState();
            }
            else
            {
                //			Debug.Log ("StopAllPlayers");
                foreach (var item in list_TrainPlayers)
                {
                    //				item.animPlayer.StopPlayback ();
                }
                StopAllPlayers();
            }
            if (currentGameState == CurrentGameState.play)
            {
                DirectionAngleInput();

            }


            if (list_AllPlayersInScene.Count != 0)
            {
                //			TextCharactersController.instance.SetText((list_AllPlayersInScene.Count - list_TrainPlayers.Count).ToString() + "/" + list_AllPlayersInScene.Count.ToString());
            }
            else
            {
                //			TextCharactersController.instance.SetText("");
            }

        }

        bool isOnceGameCompleted;

        protected virtual void CheckGameState()
        {

            if (list_AllPlayersInScene.Count == list_TrainPlayers.Count && keysCollected == keysCount)
            {
                if (!isOnceGameCompleted)
                {
                    isOnceGameCompleted = true;


                }
                //			textGui = "Can go out";
                if (showExit)
                {
                    GameObject arrow = Instantiate(Resources.Load<GameObject>("arrow")) as GameObject;
                    arrow.transform.position = new Vector3(0, -5, 0);
                    showExit = false;
                }
            }
            else
            {
            }
        }

        public void KeySound()
        {

        }


        protected void PlayerDirectionWorker()
        {
            foreach (var item in list_TrainPlayers)
            {
                if (!item.AmIHaveQueue() && item.amIInTrain)
                {
                    Vector2 dir = CalculateDirWhenNoQueue();

                    item.Walk(dir.x, dir.y);

                }

                for (int i = 0; i < list_TrainPlayers.Count; i++)
                {
                    if (i != list_TrainPlayers.Count - 1)
                    {
                        if (Vector2.Distance(list_TrainPlayers[i + 1].transform.position, list_TrainPlayers[i].transform.position) > distanceBetweenPlayers + 0.2f)
                        {
                            list_TrainPlayers[i].walkSpeed = speed * 2;
                        }
                        else if (Vector2.Distance(list_TrainPlayers[i + 1].transform.position, list_TrainPlayers[i].transform.position) < distanceBetweenPlayers - 0.2f)
                        {
                            list_TrainPlayers[i].walkSpeed = speed / 2;
                        }
                        else
                        {
                            list_TrainPlayers[i].walkSpeed = speed;
                        }
                    }
                    else
                    {
                        list_TrainPlayers[i].walkSpeed = speed;
                    }
                }
            }
        }

        Vector2 CalculateDirWhenNoQueue()
        {
            switch (currentWalkStateDir)
            {
                case CurrentWalkStateDir.none:
                    return Vector2.zero;
                case CurrentWalkStateDir.left:
                    return new Vector2(-100, 0);
                case CurrentWalkStateDir.right:
                    return new Vector2(100, 0);
                case CurrentWalkStateDir.up:
                    return new Vector2(0, 100);
                case CurrentWalkStateDir.down:
                    return new Vector2(0, -100);
            }
            return Vector2.zero;
        }

        private Vector2 currentPos;
        private bool activeInput;

        protected void CheckNearestPlayers()
        {
            foreach (var item in list_AllPlayersInScene)
            {
                if (!item.amIInTrain && item.iAmFreeAndCanBeTaken)
                {
                    if (Vector2.Distance(item.transform.position, mainPlayer.transform.position) < distanceForGetPlayer)
                    {
                        item.amIInTrain = true;
                        item.iAmFreeAndCanBeTaken = false;
                        list_TrainPlayers.Add(item);
                        mainPlayer.isMainPlayer = false;
                        item.isMainPlayer = true;
                        item.mySnakeNumber = mainPlayer.mySnakeNumber + 1;
                        mainPlayer = item;
                        CameraController.instance.SetFollow(mainPlayer.transform);
                        //list_TrainPlayers[list_TrainPlayers.Count - 2].WalkInsertQueue(list_TrainPlayers.Last().transform.position);
                        SetChangeDirection(mainPlayer.transform.position);
                        //SetPlayerNumbersInSnake();
                    }
                }
            }
        }

        private void DirectionAngleInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                currentPos = Input.mousePosition;
                activeInput = true;
            }

            if (Input.GetMouseButton(0))
            {
                if (activeInput)
                {
                    float ang = GetAngle(currentPos, Input.mousePosition);
                    if ((Input.mousePosition.x - currentPos.x) > 20)
                    {
                        if (ang < 45 && ang > -45)
                        {
                            //directInput = DirectionInput.Right;
                            Right();
                            activeInput = false;
                        }
                        else if (ang >= 45)
                        {
                            //directInput = DirectionInput.Up;
                            Up();
                            activeInput = false;
                        }
                        else if (ang <= -45)
                        {
                            ////directInput = DirectionInput.Down;
                            Down();
                            activeInput = false;
                        }
                    }
                    else if ((Input.mousePosition.x - currentPos.x) < -20)
                    {
                        if (ang < 45 && ang > -45)
                        {
                            //directInput = DirectionInput.Left;
                            Left();
                            activeInput = false;
                        }
                        else if (ang >= 45)
                        {
                            //directInput = DirectionInput.Down;
                            Down();
                            activeInput = false;
                        }
                        else if (ang <= -45)
                        {
                            //directInput = DirectionInput.Up;
                            Up();
                            activeInput = false;
                        }
                    }
                    else if ((Input.mousePosition.y - currentPos.y) > 20)
                    {
                        if ((Input.mousePosition.x - currentPos.x) > 0)
                        {
                            if (ang > 45 && ang <= 90)
                            {
                                //directInput = DirectionInput.Up;
                                Up();
                                activeInput = false;
                            }
                            else if (ang <= 45 && ang >= -45)
                            {
                                //directInput = DirectionInput.Right;
                                Right();
                                activeInput = false;
                            }
                        }
                        else if ((Input.mousePosition.x - currentPos.x) < 0)
                        {
                            if (ang < -45 && ang >= -89)
                            {
                                //directInput = DirectionInput.Up;
                                Up();
                                activeInput = false;
                            }
                            else if (ang >= -45)
                            {
                                //directInput = DirectionInput.Left;
                                Left();
                                activeInput = false;
                            }
                        }
                    }
                    else if ((Input.mousePosition.y - currentPos.y) < -20)
                    {
                        if ((Input.mousePosition.x - currentPos.x) > 0)
                        {
                            if (ang > -89 && ang < -45)
                            {
                                //directInput = DirectionInput.Down;
                                Down();
                                activeInput = false;
                            }
                            else if (ang >= -45)
                            {
                                //directInput = DirectionInput.Right;
                                Right();
                                activeInput = false;
                            }
                        }
                        else if ((Input.mousePosition.x - currentPos.x) < 0)
                        {
                            if (ang > 45 && ang < 89)
                            {
                                //directInput = DirectionInput.Down;
                                Down();
                                activeInput = false;
                            }
                            else if (ang <= 45)
                            {
                                //directInput = DirectionInput.Left;
                                Left();
                                activeInput = false;
                            }
                        }

                    }
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                //directInput = DirectionInput.Null;
                activeInput = false;
            }

        }

        public static float GetAngle(Vector3 form, Vector3 to)
        {
            Vector3 nVector = Vector3.zero;
            nVector.x = to.x;
            nVector.y = form.y;
            float a = to.y - nVector.y;
            float b = nVector.x - form.x;
            float tan = a / b;
            return RadToDegree(Mathf.Atan(tan));
        }

        public static float RadToDegree(float radius)
        {
            return (radius * 180) / Mathf.PI;
        }

        public void Right()
        {

            //Debug.Log("Right()");
            if (currentWalkStateDir != CurrentWalkStateDir.left)
            {
                currentWalkStateDir = CurrentWalkStateDir.right;
                SetChangeDirection(mainPlayer.transform.position);

            }

        }

        public void Left()
        {
            if (currentWalkStateDir != CurrentWalkStateDir.right)
            {
                //Debug.Log("Left()");
                currentWalkStateDir = CurrentWalkStateDir.left;
                SetChangeDirection(mainPlayer.transform.position);
            }

        }

        public void Up()
        {
            if (currentWalkStateDir != CurrentWalkStateDir.down)
            {
                //Debug.Log("Up()");

                currentWalkStateDir = CurrentWalkStateDir.up;
                SetChangeDirection(mainPlayer.transform.position);
            }
        }

        public void Down()
        {
            if (currentWalkStateDir != CurrentWalkStateDir.up)
            {
                //Debug.Log("Down()");
                currentWalkStateDir = CurrentWalkStateDir.down;
                SetChangeDirection(mainPlayer.transform.position);
            }
        }

        ///------  


        public void SetChangeDirection(Vector2 _storePos)
        {
            foreach (var item in list_TrainPlayers)
            {
                if (!item.isMainPlayer && item.amIInTrain)
                {
                    item.WalkAddQueue(_storePos);

                }
            }
            if (isTutorialLevel)
            {

                if (tutorialCurrentWalkStateDir == currentWalkStateDir)
                {
                    //animTutorial.SetTrigger("done");
                    animTutorial.SetBool("bDone", true);

                    tutorialCorrectAction = true;
                }
            }
        }

        //void SetPlayerNumbersInSnake()
        //{
        //    for (int i = 0; i < list_TrainPlayers.Count; i++)
        //    {
        //        list_TrainPlayers[i].mySnakeNumber = i;
        //    }
        //}

        protected void StopAllPlayers()
        {
            foreach (var item in list_TrainPlayers)
            {
                item.WalkStop(WalkDirection.none);
                //			item.WalkPause();
            }
        }

        public void PausePlayers()
        {
            foreach (var item in list_TrainPlayers)
            {
                //			item.WalkStop(WalkDirection.none);
                item.WalkPause();
            }
        }


        void RestartGame()
        {
            //RandomBonus.index--;
            Application.LoadLevel(Application.loadedLevel);
        }

        void SavePlayers()
        {
            for (int i = 0; i < list_TrainPlayers.Count; i++)
            {
                PlayerPrefs.SetString("lvl_" + (Application.loadedLevel + 1).ToString() + i.ToString(), list_TrainPlayers[i].name);
            }
            PlayerPrefs.SetInt((Application.loadedLevel + 1).ToString() + "_countPlayersSaved", list_TrainPlayers.Count);
        }

        protected virtual void LoadPlayers()
        {
            Vector2 startPoint = Vector2.zero;
            GameObject goo = GameObject.Find("StartPoint");
            if (goo)
                startPoint = GameObject.Find("StartPoint").transform.position;

            if (PlayerPrefs.GetInt(Application.loadedLevel + "_countPlayersSaved") > 0)
            {
                int counter = 0;

                for (int i = 0; i < PlayerPrefs.GetInt((Application.loadedLevel).ToString() + "_countPlayersSaved"); i++)
                {
                    GameObject source = Resources.Load<GameObject>("Players/" +
                                            PlayerPrefs.GetString("lvl_" + (Application.loadedLevel).ToString() + i.ToString()));

                    GameObject go = Instantiate(source, new Vector3(startPoint.x - (distanceBetweenPlayers * (PlayerPrefs.GetInt(Application.loadedLevel + "_countPlayersSaved") - counter)), startPoint.y), Quaternion.identity) as GameObject;
                    go.name = source.name;
                    if (i == PlayerPrefs.GetInt((Application.loadedLevel).ToString() + "_countPlayersSaved") - 1)
                    {
                        go.GetComponent<SnakePlayer>().isMainPlayer = true;
                        mainPlayer = go.GetComponent<SnakePlayer>();
                    }
                    go.GetComponent<SnakePlayer>().amIInTrain = true;
                    go.GetComponent<SnakePlayer>().mySnakeNumber = counter;
                    go.GetComponent<SnakePlayer>().iAmFreeAndCanBeTaken = false;
                    list_TrainPlayers.Add(go.GetComponent<SnakePlayer>());
                    counter++;
                }
                Debug.Log("LoadPlayers");
            }
            else
            {

                SnakePlayer pc = GameObject.Find("George").GetComponent<SnakePlayer>();
                pc.amIInTrain = true;
                pc.iAmFreeAndCanBeTaken = false;
                pc.mySnakeNumber = 0;
                list_TrainPlayers.Add(pc);

                pc = GameObject.Find("Peppa").GetComponent<SnakePlayer>();
                pc.amIInTrain = true;
                pc.iAmFreeAndCanBeTaken = false;
                pc.isMainPlayer = true;
                pc.mySnakeNumber = 1;
                list_TrainPlayers.Add(pc);

                mainPlayer = pc;
            }
        }

        public void RandomScream()
        {
            if (GameSettings.IsSoundsEnabled())
            {
                AudioSource.PlayClipAtPoint(screams[UnityEngine.Random.Range(0, screams.Count)], transform.position, 0.3f);
            }
        }

        public void RandomLaughter()
        {
            if (GameSettings.IsSoundsEnabled())
            {
                AudioSource.PlayClipAtPoint(laughters[UnityEngine.Random.Range(0, laughters.Count)], transform.position, 0.3f);
            }
        }

        public void RandomDudka()
        {
            if (GameSettings.IsSoundsEnabled())
            {
                Debug.Log("Playing random dudka");
                AudioSource.PlayClipAtPoint(dudkas[UnityEngine.Random.Range(0, dudkas.Count)], transform.position, 0.7f);
            }
        }

        //	public void MarakasSound()
        //	{
        //		if (GameSettings.IsSoundsEnabled())
        //		{
        //			AudioSource.PlayClipAtPoint(marakas, transform.position, 0.5f);
        //		}
        //	}
    }
}
