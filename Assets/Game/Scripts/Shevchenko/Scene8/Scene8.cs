﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko.nScene8;
using PSV;

namespace Shevchenko
{
    public class Scene8 : GameController
    {
        public static Scene8 instance;
        public bool isControlEnabled;

        List<GameObject> list_SrcObstacles;
        List<Scene8_Line> list_Scene8_Line;
        Scene8_Line current_Scene8_Line;

        public enum WalkState { walking, endWalking, idle }
        public enum WalkType { walking, running }
        public WalkState currentWalkState;
        public WalkType currentWalkType;
        //float targetY;
        FollowCamera followCamera;
        public GameObject goFireworks;

        public override void AwakeOther()
        {
            instance = this;
            list_SrcObstacles = new List<GameObject>
             (Resources.LoadAll<GameObject>("Scene8/Obstacles"));
            followCamera = GetComponent<FollowCamera>();

        }
        public override void Start()
        {
            base.Start();
            list_Scene8_Line = new List<nScene8.Scene8_Line>(FindObjectsOfType<Scene8_Line>());

            list_Scene8_Line.Sort(delegate (Scene8_Line us1, Scene8_Line us2)
                { return us1.curLineNumber.CompareTo(us2.curLineNumber); });
            current_Scene8_Line = list_Scene8_Line[1]; // задаём стартовую линию
        }
        public override void OnEnable()
        {
            GetComponent<DecorationController>().OnDecorationReplaced += DecorationController_OnDecorationReplaced;
            PlayerController.OnTriggerHit += PlayerController_OnTriggerHit;
            Swipe.OnMouseClickUp += Swipe_OnMouseClickUp;
            Swipe.OnSwipeUp += Swipe_OnSwipeUp;
            Swipe.OnSwipeDown += Swipe_OnSwipeDown;

        }



        public override void OnDisable()
        {
            GetComponent<DecorationController>().OnDecorationReplaced -= DecorationController_OnDecorationReplaced;
            PlayerController.OnTriggerHit -= PlayerController_OnTriggerHit;
            Swipe.OnMouseClickUp -= Swipe_OnMouseClickUp;
            Swipe.OnSwipeUp -= Swipe_OnSwipeUp;
            Swipe.OnSwipeDown -= Swipe_OnSwipeDown;
        }

        private void Swipe_OnSwipeDown()
        {
            if (current_Scene8_Line.curLineNumber != 2)
            {
                current_Scene8_Line = list_Scene8_Line[current_Scene8_Line.curLineNumber + 1];

            }
        }

        private void Swipe_OnSwipeUp()
        {
            if (current_Scene8_Line.curLineNumber != 0)
            {
                current_Scene8_Line = list_Scene8_Line[current_Scene8_Line.curLineNumber - 1];

            }
        }

        private void Swipe_OnMouseClickUp(Vector3 pos)
        {
            if (isControlEnabled)
            {

                Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);

                if (hits != null)
                {
                    foreach (var item in hits)
                    {
                        if (item.collider.GetComponent<Scene8_Line>())
                        {
                            //targetY = item.collider.transform.position.y;
                            current_Scene8_Line = item.collider.GetComponent<Scene8_Line>();
                        }
                    }
                }

            }
        }
        private void DecorationController_OnDecorationReplaced(DecorationItem decorItem)
        {
            decorItem.DropRandomObstacle(list_SrcObstacles.RandomOne());
        }

        private void PlayerController_OnTriggerHit(PlayerController _player, Collider2D _coll)
        {

        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Debug.Log("stop");
                SetWalkState(WalkState.endWalking);


            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("WEAR");
                Entity en = GameObject.Find("EntityFoot").GetComponent<Entity>();
                pepa.GetComponent<EntityPlayer>().PlaceCorrectEntity(en);

            }
            if (Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("WEAR");
                Entity en = GameObject.Find("Body_1 (1)").GetComponent<Entity>();
                pepa.GetComponent<EntityPlayer>().PlaceCorrectEntity(en);

            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                Debug.Log("walking");
                SetWalkState(WalkState.walking);

            }


            pepa.transform.position = Vector2.Lerp(pepa.transform.position,
                    new Vector2(pepa.transform.position.x, current_Scene8_Line.transform.position.y), Time.deltaTime * 3);
            george.transform.position = Vector2.Lerp(george.transform.position,
                    new Vector2(george.transform.position.x, pepa.transform.position.y), Time.deltaTime * 3);

            switch (currentWalkState)
            {
                case WalkState.walking:

                    break;
                case WalkState.endWalking:
                    if (Vector2.Distance(pepa.transform.position,
                        new Vector2(pepa.transform.position.x, current_Scene8_Line.transform.position.y)) < .5f)
                    //if (Mathf.Approximately(pepa.transform.position.y,targetY))
                    {
                        currentWalkState = WalkState.idle;
                    }
                    break;
                case WalkState.idle:
                    //foreach (var item in list_AllPlayers)
                    //    item.animPlayer.SetBool("SpecRun", false);
                    SetWalkState(WalkState.idle);
                    break;
                default:
                    break;
            }
        }

        public override IEnumerator Process()
        {
            //Мы нашли  древнюю сокровищницу востока!		hp-65

            yield return new WaitWhile(() => pepa.Talk("hp-65"));
            //Собирай сундуки и избегай ловушек.		hp-66
            yield return new WaitWhile(() => pepa.Talk("hp-66"));
            yield return StartCoroutine(SmallGame());

            MapProgressController.SetSceneCompleted();
                 SceneLoader.SwitchToScene(Scenes.Map);
 
        }
        public override IEnumerator SmallGame()
        {
            Walk(true);
            followCamera.offset = new Vector3(-10, 0);
            yield return new WaitForSeconds(50);

            DecorationController dc = GetComponent<DecorationController>();
            dc.StopAutomaticReplace();

            foreach (var item in FindObjectsOfType<Scene8_DecorItem>())
            {
                if (item.isFinal)
                {
                    dc.ManualSetNextItem(item);
                    break;
                }
            }
            yield return null;
            Transform stopPos = GameObject.Find("STOPPos").transform;
            yield return new WaitWhile(() => pepa.transform.position.x < stopPos.position.x);
            //Scene8_Decors.instance.SetSpeed(0);
            SFX.Play("MUSIC_EFFECT_Platform_Positiv");
            Walk(false);
            yield return new WaitForSeconds(.5f);
            pepa.SetTriggerEmotion(EmotionTriggerType.Joy);
            yield return new WaitForSeconds(.5f);
            george.SetTriggerEmotion(EmotionTriggerType.Joy);
            //StaticParams.PushEffectFireworks();
            //Фейерверк
            SFX.Play("Фейерверк");

            goFireworks.SetActive(true);
            yield return new WaitForSeconds(3);

        }
        public void Walk(bool _walk)
        {
            isControlEnabled = _walk;

            Scene8_Decors.instance.IsScrollAllowed(_walk);
            if (_walk)
            {
                SetWalkState(WalkState.walking);
            }
            else
                SetWalkState(WalkState.endWalking);

        }
        void SetWalkState(WalkState _WalkState)
        {
            currentWalkState = _WalkState;
            switch (currentWalkState)
            {
                case WalkState.walking:
                    if (currentWalkType == WalkType.walking)
                    {
                        foreach (var item in list_AllPlayers)
                        {
                            item.animPlayer.SetBool("Run", false);
                            item.animPlayer.SetBool("SpecRun", true);
                        }
                    }
                    else if (currentWalkType == WalkType.running)
                    {
                        foreach (var item in list_AllPlayers)
                        {
                            item.animPlayer.SetBool("SpecRun", false);
                            item.animPlayer.SetBool("Run", true);
                        }
                    }

                    break;

                case WalkState.endWalking:
                    break;
                case WalkState.idle:
                    foreach (var item in list_AllPlayers)
                    {
                        item.animPlayer.SetBool("SpecRun", false);
                        item.animPlayer.SetBool("Run", false);
                    }
                    break;
                default:
                    break;
            }
        }
        void SetWalkType(WalkType _WalkType)
        {
            currentWalkType = _WalkType;
        }
        #region BONUS_METHODS
        bool isSpeedUp;
        Entity srcEntityFoots;
        public void BonusSpeedUp(Collider2D collision)
        {
            StartCoroutine(IeBonusSpeedUp(collision));
        }
        IEnumerator IeBonusSpeedUp(Collider2D collision)
        {
            if (!isSpeedUp)
            {
                isSpeedUp = true;
                SetWalkType(WalkType.running);
                SetWalkState(WalkState.walking);
                if (!srcEntityFoots)
                {
                    srcEntityFoots = Resources.Load<Entity>("Scene8/EntityFoot");
                }
                foreach (var item in list_AllPlayers)
                {
                    Entity en = Instantiate(srcEntityFoots);
                    en.transform.position = collision.transform.position;
                    item.GetComponent<EntityPlayer>().PlaceCorrectEntity(en);
                    //item.SetAnimSpeedMultiplyer(1.5f);
                }
                Debug.Log("IeBonusSpeedUp");
                float speedScale = 3;
                float timer = 5;
                float defSpeed = Scene8_Decors.instance.speed;
                while (timer > 0)
                {
                    if (timer > 5 * .6f)
                    {
                        speedScale = Mathf.MoveTowards(speedScale, 5, Time.deltaTime * 2);
                        //Debug.Log("timer UP " + timer + "  " + timer * .7f + " speedScale " + speedScale);

                    }
                    else if (timer < 5 * .3f)
                    {
                        //Debug.Log("timer DOWN " + timer);
                        speedScale = Mathf.MoveTowards(speedScale, defSpeed, Time.deltaTime);
                    }
                    Scene8_Decors.instance.SetSpeed(speedScale);
                    timer -= Time.deltaTime;

                    yield return null;
                }
                Debug.Log("----------------------END");

                isSpeedUp = false;
                foreach (var item in list_AllPlayers)
                {
                    item.GetComponent<EntityPlayer>().TryRemoveOldWntity(EntityPart.Leg);
                    //item.SetAnimSpeedMultiplyer(1);

                }
                //speedScale = 1;
                Scene8_Decors.instance.SetSpeed(defSpeed);
                SetWalkType(WalkType.walking);
                if (currentWalkState == WalkState.walking)
                {
                    SetWalkState(WalkState.walking);

                }

            }
        }
        public class WhoHasEntity
        {
            public PlayerType playerType;
            public EntityPart entity;
            public bool collected;

        }
        List<WhoHasEntity> list_WhoHasEntity;

        public WhoHasEntity CheckEntity()
        {
            if (list_WhoHasEntity == null)
            {
                list_WhoHasEntity = new List<WhoHasEntity>();
                WhoHasEntity whe = new WhoHasEntity();
                whe.playerType = PlayerType.pEpa;
                whe.entity = EntityPart.Head;
                list_WhoHasEntity.Add(whe);

                whe = new WhoHasEntity();
                whe.playerType = PlayerType.pEpa;
                whe.entity = EntityPart.Body;
                list_WhoHasEntity.Add(whe);

                whe = new WhoHasEntity();
                whe.playerType = PlayerType.george;
                whe.entity = EntityPart.Head;
                list_WhoHasEntity.Add(whe);

                whe = new WhoHasEntity();
                whe.playerType = PlayerType.george;
                whe.entity = EntityPart.Body;
                list_WhoHasEntity.Add(whe);
            }
            foreach (var item in list_WhoHasEntity)
            {
                if (!item.collected)
                {
                    return item;
                }
            }
            return null;
        }
        #endregion
    }
}
