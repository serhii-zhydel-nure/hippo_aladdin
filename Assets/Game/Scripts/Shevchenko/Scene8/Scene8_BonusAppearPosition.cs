﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko.nScene8
{
    public class Scene8_BonusAppearPosition : MonoBehaviour
    {
        List<Transform> list_InstPoses = new List<Transform>();
        public bool isBusyNow;

        private void Awake()
        {
            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name.Contains("InstPos"))
                {
                    list_InstPoses.Add(item);
                }
            }

        }

        public void RecieveItem(GameObject _go)
        {
         
             isBusyNow = true;
             Transform rndPos = list_InstPoses.RandomOne();

            _go.transform.position = rndPos.position;
            _go.transform.parent = rndPos;

        }
    }
}
