﻿using UnityEngine;
using System.Collections;
namespace Shevchenko.nScene8
{
    public class Scene8_Decors : MonoBehaviour
    {
        public static Scene8_Decors instance;
        bool isScrollAllowed;
        public float speed =1;

        private void Awake()
        {
            instance = this;
        }
        public void IsScrollAllowed(bool _isAllowed)
        {
            isScrollAllowed = _isAllowed;
        }
 
        void Update()
        {
            if (isScrollAllowed)
            {
                transform.Translate(new Vector3(-.1f, 0, 0)* speed);
             }
         }
        public void SetSpeed(float _speed)
        {
            speed = _speed;
        }
    }
}
