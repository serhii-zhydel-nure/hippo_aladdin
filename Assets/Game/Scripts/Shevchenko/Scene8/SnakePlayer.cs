﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko
{
 
    public class SnakePlayer : MonoBehaviour
    {
        public delegate void OnAnimationEvent();
        public static event OnAnimationEvent OnAnimationEnded;

        //public delegate void DelPlayerEvents();
        //public static event DelPlayerEvents OnGameover;
        //public static event DelPlayerEvents OnDoorExitReached;
        //public static event DelPlayerEvents OnDoorExitReachedAnyPlayer;
        //public static event DelPlayerEvents OnDoorEnterReachedAnyPlayer;

        //public delegate void DelKick(PlayerController _PlayerController);
        //public static event DelKick OnSelfKick;

        public delegate void DelFunElement(FunElement _fe);
        public static event DelFunElement OnFunElementGet;

        [System.Serializable]
        public class WalkQueue
        {
            public Vector2 targetPosition;
            //public WalkDirection walkDirection;
            public string pointName;
            public bool visited;
        }

        public PlayerType playerType;
        public Vector3 defaultPlayerSize;
        public float walkSpeed = 4.5f;

        public List<WalkQueue> list_WalkQueue = new List<WalkQueue>();
        public Animator animPlayer;

        [HideInInspector]
        public float points;
        [HideInInspector]
        public Animator playerAnim;
        WalkAnimType currentWalkAnimType = WalkAnimType.Walk;
        //int animPlayerID;

        int playerScore;
        public bool isMainPlayer;
        public bool HasQueue;
        public bool amIInTrain;
        public bool iAmFreeAndCanBeTaken = true;
        public bool isDontDestroyOnLoad;

        public bool isDebug;
        WalkQueue currentWalkQueueItem;
        public int mySnakeNumber;
        public SnakePlayer pcNeighbor;

        int turns;

        void Awake()
        {
            playerAnim = GetComponent<Animator>();
            defaultPlayerSize = transform.localScale;
            animPlayer = GetComponent<Animator>();
            if (playerType == PlayerType.other)
            {
                animPlayer.speed = Random.RandomRange(0.6f, 1.5f);
                StartCoroutine(ChangeAnimRandomSpeedOtherChars());
            }

            //if (playerType == PlayerType.pEpa) { animPlayerID = 1; animPlayer.SetInteger("characterID", animPlayerID); }
            //else if (playerType == PlayerType.george) { animPlayerID = 2; animPlayer.SetInteger("characterID",animPlayerID);}
            //else if (playerType == PlayerType.mama) { animPlayerID = 3; animPlayer.SetInteger("characterID",animPlayerID);}
            //else if (playerType == PlayerType.pApa) { animPlayerID = 4; animPlayer.SetInteger("characterID",animPlayerID);}

        }

        IEnumerator ChangeAnimRandomSpeedOtherChars()
        {
            while (true)
            {
                animPlayer.speed = Random.RandomRange(0.6f, 1.5f);
                yield return new WaitForSeconds(Random.RandomRange(3, 6));
            }
        }
        void Update()
        {
            try
            {
                animPlayer.speed = SnakeGameController.instance.speed / 3;
            }
            catch (System.Exception ex)
            {

            }




            if (walkNow)
            {
                animPlayer.SetBool(currentWalkAnimType.ToString(), true);
                transform.position = Vector2.MoveTowards(transform.position,
                    currentWalkQueueItem.targetPosition, Time.deltaTime * walkSpeed);
                transform.localScale = Vector2.MoveTowards(transform.localScale, targetPlayerSize, Time.deltaTime / distanseForScale);

                if (Vector2.Distance(transform.position, currentWalkQueueItem.targetPosition) == 0)
                {
                    if (list_WalkQueue.Contains(currentWalkQueueItem)) list_WalkQueue.Remove(currentWalkQueueItem);

                    if (list_WalkQueue.Count > 0)
                    {
                        currentWalkQueueItem = list_WalkQueue[0];
                        AutoSetDirection(currentWalkQueueItem.targetPosition);
                    }
                    else
                    {
                        walkNow = false;
                        animPlayer.SetBool(currentWalkAnimType.ToString(), false);
                    }
                }
            }

            //if (SoundEffectsController.IsThisClipEnded(soundHash))
            //{
            //    animPlayer.SetBool("IdleTalking", true);
            //}
            //else
            //{

            //    animPlayer.SetBool("IdleTalking", false);
            //    soundHash = 0;
            //}
        }

        int wayHash;
        bool walkNow;
        Vector2 targetPosition, targetPlayerSize;
        float distanseForScale;



        public void Walk(float _Xdistance, float _Ydistance)
        {
            targetPlayerSize = defaultPlayerSize;
            targetPosition = new Vector2(transform.position.x + _Xdistance, transform.position.y + _Ydistance);
            AutoSetDirection(targetPosition);
            list_WalkQueue.Clear();

            walkNow = true;

            WalkQueue wq = new WalkQueue();
            wq.targetPosition = new Vector2(transform.position.x + _Xdistance, transform.position.y + _Ydistance);
            wq.pointName = wq.targetPosition.ToString();

            currentWalkQueueItem = wq;
            //list_WalkQueue.Add(wq);
        }

        public void WalkAddQueue(Vector2 _point)
        {
            walkNow = true;
            WalkQueue wq = new WalkQueue();
            wq.targetPosition = _point;
            wq.pointName = _point.ToString();
            if (list_WalkQueue.Count == 0)
            {
                targetPosition = _point;
                AutoSetDirection(targetPosition);
                currentWalkQueueItem = wq;
            }
            list_WalkQueue.Add(wq);


        }
        public void WalkInsertQueue(Vector2 _point)
        {
            walkNow = true;
            WalkQueue wq = new WalkQueue();
            wq.targetPosition = _point;
            wq.pointName = _point.ToString();
            if (list_WalkQueue.Count == 0)
            {
                targetPosition = _point;
                AutoSetDirection(targetPosition);
                currentWalkQueueItem = wq;
            }
            list_WalkQueue.Insert(0, wq);
        }

        public void WalkContinue()
        {
            walkNow = true;
        }

        public void WalkPause(WalkDirection _direction)
        {
            walkNow = false;
            if (_direction == WalkDirection.left) transform.rotation = Quaternion.Euler(0, 180, 0);
            else if (_direction == WalkDirection.right) transform.rotation = Quaternion.Euler(0, 0, 0);
            else if (_direction == WalkDirection.inverse)
            {
                if (transform.rotation.eulerAngles.y == 0)
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                else transform.rotation = Quaternion.Euler(0, 0, 0);
            }

            animPlayer.SetBool(currentWalkAnimType.ToString(), false);
        }

        public void WalkStop(WalkDirection _direction)
        {
            walkNow = false;
            list_WalkQueue.Clear();
            currentWalkQueueItem = null;
            if (_direction == WalkDirection.left) transform.rotation = Quaternion.Euler(0, 180, 0);
            else if (_direction == WalkDirection.right) transform.rotation = Quaternion.Euler(0, 0, 0);
            else if (_direction == WalkDirection.inverse)
            {
                if (transform.rotation.eulerAngles.y == 0)
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                else transform.rotation = Quaternion.Euler(0, 0, 0);
            }

            animPlayer.SetBool(currentWalkAnimType.ToString(), false);
        }

        public void WalkPause()
        {
            walkNow = false;
        }
        public bool AmIHaveQueue()
        {
            HasQueue = list_WalkQueue.Count > 0 ? true : false;
            return list_WalkQueue.Count > 0 ? true : false;
        }

        public bool WalkCheck()
        {
            return walkNow;
        }

        public void WalkSetWalkAnimType(WalkAnimType _wat)
        {
            currentWalkAnimType = _wat;
        }
        void AutoSetDirection(Vector2 _targetPosition)
        {
            if (transform.position.x < _targetPosition.x)
            {
                // right
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }

        public bool CheckAnim(string _animNameCompare)
        {
            //Debug.Log("fullPathHash " + animPlayer.GetCurrentAnimatorStateInfo(0).fullPathHash
            //    + "nameHash   " + animPlayer.GetCurrentAnimatorStateInfo(0).nameHash
            //   + "   shortNameHash  " + animPlayer.GetCurrentAnimatorStateInfo(0).shortNameHash
            //     + "   _animNameCompare  " + Animator.StringToHash(_animNameCompare)
            //                  + "   IsInTransition  " + animPlayer.IsInTransition(0)

            //                  + "  " + _animNameCompare

            //    );

            //return animPlayer.GetCurrentAnimatorStateInfo(0).IsName(_animNameCompare);
            if (animPlayer.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !animPlayer.IsInTransition(0) && animPlayer.GetCurrentAnimatorStateInfo(0).shortNameHash == Animator.StringToHash(_animNameCompare))
            {
                return true;
            }
            else return false;
        }

        int soundHash;
        //List<string> list_mySoundQueue = new List<string>();
        bool amITalkingSomethingNow;
        public bool Talk(string _clipName)
        {
            if (soundHash == 0)
            {

                SoundEffectsController.StopAll();
                soundHash = SoundEffectsController.StopAnd_Play_By_Name_Once(_clipName);
                //list_mySoundQueue.Add(_clipName);
                amITalkingSomethingNow = true;
            }
            if (SoundEffectsController.IsThisClipEnded(soundHash))
            {

                animPlayer.SetBool("IdleTalking", true);
                return true;
            }
            else
            {
                amITalkingSomethingNow = false;

                animPlayer.SetBool("IdleTalking", false);
                soundHash = 0;
                //list_mySoundQueue.Remove(_clipName);
                return false;
            }

        }
        public bool Talk(string[] _clipName)
        {
            if (soundHash == 0)
                soundHash = SoundEffectsController.StopAnd_Play_By_Name_Random(_clipName);
            //else
            //    SoundEffectsController.Play_By_Name_Random(_clipName, isStrongNameEqual);

            if (SoundEffectsController.IsThisClipEnded(soundHash))
            {
                animPlayer.SetBool("IdleTalking", true);
                return true;
            }
            else
            {

                animPlayer.SetBool("IdleTalking", false);
                soundHash = 0;
                return false;
            }

        }

        public bool TalkDelayed(string _clipName)
        {
            if (soundHash == 0)
            {
                soundHash = SoundEffectsController.Play_By_Name_Delayed(_clipName);
                //list_mySoundQueue.Add(_clipName);
                amITalkingSomethingNow = true;
            }
            if (SoundEffectsController.IsThisClipEnded(soundHash))
            {
                animPlayer.SetBool("IdleTalking", true);
                return true;
            }
            else
            {

                animPlayer.SetBool("IdleTalking", false);
                soundHash = 0;
                //list_mySoundQueue.Remove(_clipName);
                return false;
            }

        }
        /*
        public bool TalkDelayed(string _clipName, bool isStrongNameEqual)
        {
            Debug.Log("PlayerController.TalkDelayed: " + _clipName);
            if (soundHash == 0)
                soundHash = SoundEffectsController.Play_By_Name_Delayed(_clipName, true);
            //else
            //    SoundEffectsController.Play_By_Name_Delayed(_clipName, true);

            if (SoundEffectsController.IsThisClipEnded(soundHash))
            {
                animPlayer.SetBool("IdleTalking", true);
                return true;
            }
            else
            {

                animPlayer.SetBool("IdleTalking", false);
                soundHash = 0;
                return false;
            }

        }
        public bool TalkDelayed_RandomPhrase(string[] _clipName, bool isStrongNameEqual)
        {
            if (soundHash == 0)
                soundHash = SoundEffectsController.StopAnd_Play_By_Name_Random(_clipName );
            //else
            //    SoundEffectsController.Play_By_Name_Random(_clipName, isStrongNameEqual);

            if (SoundEffectsController.IsThisClipEnded(soundHash))
            {
                animPlayer.SetBool("IdleTalking", true);
                return true;
            }
            else
            {

                animPlayer.SetBool("IdleTalking", false);
                soundHash = 0;
                return false;
            }

        }
        */
        void OnTriggerEnter2D(Collider2D coll)
        {
             if (coll.name.Contains("DoorExit") && amIInTrain)
            {
                //SnakeGameController.instance.PlayerController_OnDoorExitReachedAnyPlayer();
            }
            else if (coll.name.Contains("DoorEnterCollider") && amIInTrain)
            {
                //SnakeGameController.instance.PlayerController_OnDoorEnterReachedAnyPlayer();
            }// no else

            if (coll.name.Contains("DoorExit") && isMainPlayer && amIInTrain)
            {
                //SnakeGameController.instance.PlayerController_OnDoorExitReached();
            }
            else if ((coll.name.Contains("Border") || coll.tag == "Obstacle") && isMainPlayer)
            {
                Debug.Log("Colliding obstacle!");
                 //SnakeGameController.instance.PlayerController_OnGameover();
            }
            else if (coll.tag == "Player" && coll.GetComponent<SnakePlayer>().amIInTrain && isMainPlayer)
            {
                 //SnakeGameController.instance.PlayerController_OnSelfKick(coll.GetComponent<SnakePlayer>());
            }
            else if ((coll.name.Contains("Border") || coll.tag == "Obstacle") && !isMainPlayer && !iAmFreeAndCanBeTaken && !amIInTrain)
            {
                WalkStop(WalkDirection.none);
            }
            else if (coll.GetComponent<FunElement>() && isMainPlayer && amIInTrain)
            {
                GameObject effectObj = Instantiate(Resources.Load<GameObject>("3D_Recovery_02")) as GameObject;
                //			effectObj.transform.parent = transform;
                //			effectObj.transform.localPosition = Vector2.zero;
                effectObj.transform.position = transform.position;

                StartCoroutine(DestroyEffect(effectObj));
                OnFunElementGet(coll.GetComponent<FunElement>());
                coll.GetComponent<FunElement>().GetThisFunElement(this);
                //            TextScoreController.instance.AddScore(coll.GetComponent<FunElement>().currentElementScore);
            }
            else if (coll.tag == "Key")
            {
                Debug.Log("KEY!");
                GameObject effectObj = Instantiate(Resources.Load<GameObject>("3D_Recovery_02")) as GameObject;
                //			effectObj.transform.parent = transform;
                //			effectObj.transform.localPosition = Vector2.zero;
                effectObj.transform.position = transform.position;

                StartCoroutine(DestroyEffect(effectObj));
                coll.gameObject.SetActive(false);
                SnakeGameController.instance.keysCollected++;
                SnakeGameController.instance.KeySound();
                //StartCoroutine(FunElementsGlobalController.instance.ScreamSounds(1));
                //KeysController.instance.SetKey();
            }
            //if (isMainPlayer)
            //{
            //    if (coll.name == "TurnPoint0")
            //    {
            //        FinalScene.instance.Up();
            //        turns++;
            //    }
            //    else if (coll.name == "TurnPoint1")
            //    {
            //        FinalScene.instance.Left();
            //        turns++;
            //    }
            //    else if (coll.name == "TurnPoint2")
            //    {
            //        FinalScene.instance.Down();
            //        turns++;
            //    }
            //    else if (coll.name == "TurnPoint3")
            //    {
            //        if (turns != 0)
            //        {
            //            FinalScene.instance.Right();
            //            turns++;
            //        }
            //    }
            //    else if (coll.name == "ScarePoint")
            //    {
            //        CutScene.TeacherFall();
            //    }
            //}

        }


        IEnumerator DestroyEffect(GameObject obj)
        {
            yield return new WaitForSeconds(4f);
            //		obj.SetActive(false);
            //		SnakeGameController.instance.list_LayerElements.Remove(obj.GetComponent<LayerWorker>());
            Destroy(obj);
        }
        public void LeaveSnake()
        {
            iAmFreeAndCanBeTaken = false;
            amIInTrain = false;
            WalkPause(WalkDirection.none);
            list_WalkQueue.Clear();
            Walk(Random.Range(transform.position.x - 7, transform.position.x + 7), Random.Range(transform.position.y - 7, transform.position.y + 7));

            float defaultSpeed = walkSpeed;
            walkSpeed = walkSpeed * 3;
            StartCoroutine(LeveSnakeDelayMet(defaultSpeed));
        }

        IEnumerator LeveSnakeDelayMet(float defaultSpeed)
        {
            yield return new WaitForEndOfFrame();

            yield return new WaitWhile(() => walkNow);
            //        yield return new WaitForSeconds(1.5f);
            walkSpeed = defaultSpeed;
            iAmFreeAndCanBeTaken = true;
            //        yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
            animPlayer.Play("signature_move1");
        }

        //public void Walk(string _pointName)
        //{
        //    targetPlayerSize = defaultPlayerSize;
        //    targetPosition = GameObject.Find(_pointName).transform.position;
        //    AutoSetDirection(targetPosition);
        //    list_WalkQueue.Clear();

        //    walkNow = true;
        //}

        //public void Walk(string _pointName, float _targetScale)
        //{
        //    targetPlayerSize = new Vector2(defaultPlayerSize.x * _targetScale, defaultPlayerSize.y * _targetScale);
        //    targetPosition = GameObject.Find(_pointName).transform.position;
        //    distanseForScale = Vector2.Distance(transform.position, targetPosition);

        //    AutoSetDirection(targetPosition);
        //    list_WalkQueue.Clear();

        //    walkNow = true;
        //}
        //public void Walk(Vector2 _targetPos)
        //{
        //    targetPlayerSize = defaultPlayerSize;
        //    targetPosition = _targetPos;
        //    AutoSetDirection(targetPosition);
        //    list_WalkQueue.Clear();

        //    walkNow = true;
        //}

        //public void Walk(float _Xdistance)
        //{
        //    targetPlayerSize = defaultPlayerSize;
        //    targetPosition = new Vector2(transform.position.x + _Xdistance, transform.position.y);
        //    AutoSetDirection(targetPosition);
        //    list_WalkQueue.Clear();

        //    walkNow = true;
        //}




        ///// <summary>
        ///// NO ANIM
        ///// </summary>
        ///// <param name="_clipName"></param>
        //public void Talk(string _clipName)
        //{
        //    SoundEffectsController.Play_By_Name_Delayed(_clipName);
        //}
        ///// <summary>
        ///// NO ANIM
        ///// </summary>
        ///// <param name="_clipName"></param>

        //public void Talk_StopAndTalk(string _clipName)
        //{
        //    SoundEffectsController.StopAnd_Play_By_Name_Delayed(_clipName);
        //}
        ///// <summary>
        ///// NO ANIM
        ///// </summary>
        ///// <param name="_clipName"></param>

        //public void Talk_StopAndTalk(string _clipName,bool strongName)
        //{
        //    SoundEffectsController.StopAnd_Play_By_Name_Delayed(_clipName, strongName);
        //}
        public void Sit()
        {
            animPlayer.SetBool("sitIdle", true);
        }

        public void CallAnimEnded()
        {
            OnAnimationEnded();
        }

        public void UseFunElementAction(FunElement _fe)
        {
            animPlayer.Play(_fe.currentFunElementType.ToString());
            _fe.DropScoreDigit(this);
        }

        public void PlaySFX(string path)
        {
            if (GameSettings.IsSoundsEnabled())
            {
                AudioSource.PlayClipAtPoint(Resources.Load<AudioClip>(path), FindObjectOfType<Camera>().transform.position, 0.5f);
            }
        }
    }
}
