﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Shevchenko.nScene8
{
    public class Scene8_DecorItem : DecorationItem
    {
        public bool isFinal;

        List<Scene8_BonusAppearPosition> list_AppearPoses;

        public override void AwakeOther()
        {
            list_AppearPoses = new List<Scene8_BonusAppearPosition>
                (GetComponentsInChildren<Scene8_BonusAppearPosition>());
        }

        public override void DropRandomObstacle(GameObject _src)
        {
            GameObject go = (GameObject)Instantiate(_src);
            Scene8_BonusAppearPosition appearPos = list_AppearPoses.RandomOne();

            appearPos.RecieveItem(go);
            list_Obstacles.Add(go);

        }
    }
}
