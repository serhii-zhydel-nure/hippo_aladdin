﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Shevchenko.nScene8
{
    public class Scene8_Bonus : MonoBehaviour
    {
        public enum BonusType
        { chest, obstPlita, obstKlapan, obstCages, BonusSpeedUp }
        public BonusType currentBonusType;
        public bool isUsed;

        Animator anim;
        int counter;
        List<GameObject> list_SrcEffects;
        private void Awake()
        {
            anim = GetComponent<Animator>();
            list_SrcEffects = new List<GameObject>(Resources.LoadAll<GameObject>("Scene8/SmokeEffects"));
        }
        private void Start()
        {
            counter = Scene8.instance.list_AllPlayers.Count;
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<PlayerController>())
            {
                 switch (currentBonusType)
                {
                    case BonusType.chest:
                        chest();
                        break;
                    case BonusType.obstPlita:
                        obstPlita(collision);
                        break;
                    case BonusType.obstKlapan:
                        obstKlapan(collision);
                        break;
                    case BonusType.obstCages:
                        obstCages(collision);
                        break;
                    case BonusType.BonusSpeedUp:
                        BonusSpeedUp(collision);
                        break;
                    default:
                        break;
                }
            }
        }
        private void BonusSpeedUp(Collider2D collision)
        {
            if (!isUsed)
            {
                isUsed = true;
                StartCoroutine(StaticParams.LerpColor( gameObject, 0));

                Scene8.instance.BonusSpeedUp(collision);
                SFX.Play("появляется джин");

            }
        }
        //IEnumerator IeBonusSpeedUp(Collider2D collision)
        //{
        //    float speedScale = 1;
        //    float timer = 5;
        //    float defSpeed = Scene8_Decors.instance.speed;
        //    while (timer > 0)
        //    {
        //        if (timer > 5 * .7f )
        //        {
        //            speedScale = Mathf.MoveTowards(speedScale, 5, Time.deltaTime);
        //            //Debug.Log("timer UP " + timer+"  "+ timer * .7f);

        //        }
        //        else if (timer < 5 * .3f)
        //        {
        //             //Debug.Log("timer DOWN " + timer);
        //            speedScale = Mathf.MoveTowards(speedScale, 1, Time.deltaTime);
        //        }
        //        Scene8_Decors.instance.SetSpeed(speedScale);
        //        timer -= Time.deltaTime;

        //        yield return null;
        //    }

        //    Debug.Log("END");
        //    //speedScale = 1;
        //    Scene8_Decors.instance.SetSpeed(defSpeed);

        //}
        private void obstCages(Collider2D collision)
        {
            if (!isUsed)
            {
                isUsed = true;
                StartCoroutine(IeCage(collision));

            }
        }
        IEnumerator IeCage(Collider2D collision)
        {
            GameObject goCageObj = null;
            GameObject goBrokenCageParts = null;
            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "CageObj")
                {
                    goCageObj = item.gameObject;
                }
                else if (item.name == "BrokenCageParts")
                {
                    goBrokenCageParts = item.gameObject;
                }
            }
            SFX.Play("Клетка падает на Джи");
            Scene8.instance.Walk(false);
            //Жми на клетку.		hp-67
            StartCoroutine(StaticParams.Say(Scene8.instance.pepa, "hp-67"));
            int rand = UnityEngine.Random.Range(0, 2);
            PlayerController pc;
            if (rand == 0)
                pc = Scene8.instance.pepa;
            else pc = Scene8.instance.george;
            StaticParams.SetOrderInLayer(goCageObj.transform, pc.GetComponentInChildren<SpriteRenderer>().sortingOrder * 2);
            StaticParams.SetOrderInLayer(goBrokenCageParts.transform, pc.GetComponentInChildren<SpriteRenderer>().sortingOrder * 2);

            goCageObj.transform.position = new Vector3(pc.transform.position.x, pc.transform.position.y + 10);
            goCageObj.SetActive(true);
            goCageObj.GetComponent<Rigidbody2D>().isKinematic = false;
            while (goCageObj.transform.position.y > pc.transform.position.y)
            {
                yield return null;
            }
            goCageObj.GetComponent<Rigidbody2D>().isKinematic = true;
            goCageObj.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            while (true)
            {
                bool hit = false;
                if (Input.GetMouseButtonDown(0))
                {
                    Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);
                    if (hits != null)
                    {
                        foreach (var item in hits)
                        {
                            if (item.collider.name.Contains("CageObj"))
                            {
                                hit = true;
                            }
                        }
                    }
                }
                if (hit)
                {
                    break;
                }
                yield return null;

            }
            SFX.Play("Открывается чердак");

            goBrokenCageParts.transform.position = goCageObj.transform.position;
            GameObject goEff = (GameObject)Instantiate(
                 Resources.Load<GameObject>("Scene8/2D_Firework_01"));
            goEff.transform.position = goCageObj.transform.position;
            goEff.transform.parent = transform;
            goBrokenCageParts.SetActive(true);
            foreach (var item in goBrokenCageParts.GetComponentsInChildren<Rigidbody2D>())
            {
                Vector3 rnd = new Vector3(UnityEngine.Random.Range(0, 365),
                    UnityEngine.Random.Range(0, 365), 0);
                item.AddForce(rnd.normalized * 3, ForceMode2D.Impulse);
                item.angularVelocity = 20;
                item.AddTorque(UnityEngine.Random.Range(0, 365), ForceMode2D.Impulse);
            }
            goCageObj.SetActive(false);

            Scene8.instance.Walk(true);
        }
        private void obstKlapan(Collider2D collision)
        {
            if (!isUsed)
            {
                isUsed = true;
                StartCoroutine(IeKlapan(collision));

            }
        }
        IEnumerator IeKlapan(Collider2D collision)
        {
            Scene8.instance.Walk(false);
            Transform effectPos = null;

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "EffectPos")
                {
                    effectPos = item;
                }
            }
            SFX.Play("появляется джин");
            SFX.Play(new string[] { "diz 1 - Смех джи 6", "dzi 2 - Смех джи 9", "Джи смеется 2", "Джи смеется 3" });
            SFX.Play(new string[] { "ep-hp-16", "ep-hp-24" });

            GameObject go = (GameObject)Instantiate(list_SrcEffects.RandomOne());
            go.transform.position = effectPos.position;
            go.transform.parent = effectPos.transform;
            go.GetComponent<Renderer>().sortingOrder = 2000;
            foreach (var item in Scene8.instance.list_AllPlayers)
            {
                item.SetTriggerEmotion(EmotionTriggerType.ScaredOnce);
                yield return new WaitForSeconds(UnityEngine.Random.Range(.1f, .3f));
            }

            //Destroy(go, 5);
            yield return new WaitForSeconds(1.5f);
            Scene8.instance.Walk(true);
            StartCoroutine(StaticParams.LerpColor(gameObject, 0));

        }
        void chest()
        {
            if (!isUsed)
            {
                isUsed = true;
                StartCoroutine(ieChest());
            }
        }

        IEnumerator ieChest()
        {
            Scene8.instance.Walk(false);

            GetComponentInChildren<Animator>().SetTrigger("Open");
            yield return new WaitForSeconds(.5f);
            SFX.Play("открывается сундук");
            yield return new WaitForSeconds(.5f);
            SFX.Play("Звук вылета элементов панели, фруктов с корзины");

            List<Entity> src_Entity = new List<Entity>(Resources.LoadAll<Entity>("Scene8/Clothes"));
            Scene8.WhoHasEntity whe = Scene8.instance.CheckEntity();
            Entity en;
            if (whe != null)
            {
                if (whe.playerType == PlayerType.pEpa)
                {
                    en = Instantiate(src_Entity.Where(e => e.forPepa && e.currentPart == whe.entity).RandomOne());

                }else
                {
                    en = Instantiate(src_Entity.Where(e => e.forGeorge && e.currentPart == whe.entity).RandomOne());

                }
                whe.collected = true;
            }
            else
                  en = Instantiate(src_Entity.RandomOne());
            en.transform.position = transform.position;
            if (en.forGeorge)
            {
                Scene8.instance.george.GetComponent<EntityPlayer>().PlaceCorrectEntity(en);
            }
            else
            {
                Scene8.instance.pepa.GetComponent<EntityPlayer>().PlaceCorrectEntity(en);

            }
            yield return new WaitForSeconds(.5f);
            Scene8.instance.Walk(true);

        }
        void obstPlita(Collider2D collision)
        {
            PlayerController pc = collision.GetComponent<PlayerController>();
            if (pc.playerType == PlayerType.george)
            {
                 SFX.Play(new string[] { "diz 1 - Смех джи 6", "dzi 2 - Смех джи 9", "Джи смеется 2", "Джи смеется 3" });
            }
            else
            {
                SFX.Play(new string[] { "ep-hp-16", "ep-hp-24" });
             }
            pc.SetTriggerEmotion(EmotionTriggerType.ScaredOnce);
            counter--;
            if (counter == 0)
            {
                anim.enabled = false;
                StartCoroutine(StaticParams.LerpColor(gameObject, 0));

            }

        }
    }
}
