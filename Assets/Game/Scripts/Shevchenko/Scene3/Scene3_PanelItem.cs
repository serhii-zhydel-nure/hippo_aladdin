﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Shevchenko.nScene3
{
    public class Scene3_PanelItem : MonoBehaviour
    {
        static List<Scene3_PanelItem> list_Scene3_PanelItem;

        public int number;
        public bool isItemBeenFound;

        private void Awake()
        {
            if (list_Scene3_PanelItem == null)
            {
                list_Scene3_PanelItem = new List<Scene3_PanelItem>();
            }
            list_Scene3_PanelItem.Add(this);
            number = StaticParams.GetNumberFromName(name);
            list_Scene3_PanelItem.Sort(delegate (Scene3_PanelItem us1, Scene3_PanelItem us2)
            { return us1.number.CompareTo(us2.number); });
            StaticParams.SetColor (gameObject, .4f);
        }

        private void OnDisable()
        {
            list_Scene3_PanelItem = null;
        }
        public static void ItemFound()
        {
            foreach (var item in list_Scene3_PanelItem)
            {
                if (!item.isItemBeenFound)
                {
 
                    item.isItemBeenFound = true;
                    item.LerpColor();
                    break;
                }
            }
        }

        public void LerpColor()
        {
            StartCoroutine(StaticParams.LerpColorUI(gameObject, 1, 1));
        }
    }
}