﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class Scene3_Telega : MonoBehaviour
    {
        public delegate void TimeToHide();
        public static event TimeToHide OnTimeToHide;

        public static Scene3_Telega instance;

        Transform TelegaPos_Left, TelegaPos_Right, CenterCamHidePos;

        public enum Dir { left_right, right_left, reverce }
        Dir lastDir;
        Vector2 defScale;
        Animator animTelega;
        PlayerController curPc;

        private void Awake()
        {
            instance = this;
            StaticParams.SetLayer(gameObject.transform, "UI");
            TelegaPos_Left = GameObject.Find("TelegaPos_Left").transform;
            TelegaPos_Right = GameObject.Find("TelegaPos_Right").transform;
            CenterCamHidePos = GameObject.Find("CenterCamHidePos").transform;
            defScale = transform.localScale;
            transform.position = new Vector2(-100, -100);

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name == "Telega")
                {
                    animTelega = item.GetComponent<Animator>();
                    animTelega.SetBool("move", true);
                }
                else if (item.GetComponent< PlayerController>())
                {
                    curPc = item.GetComponent<PlayerController>();
                    if (curPc.enabled)
                    {
                        curPc.SetFakeWalk(true);
                        curPc.SetPush(true);

                    }
                }
            }
        }
        public IEnumerator RunTelega(Dir _dir)
        {
            //Vector2 fromScreenPos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height * .1f, 0));
            //TelegaPos_Right.position = new Vector2(fromScreenPos.x + 20 , fromScreenPos.y);

            //fromScreenPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height * .1f, 0));
            //TelegaPos_Left.position = new Vector2(fromScreenPos.x - 20 , fromScreenPos.y);
            float scale = defScale.x * Camera.main.orthographicSize / 5;
            transform.localScale = new Vector2(scale, scale);
            Transform from = TelegaPos_Left, to = TelegaPos_Left;
            SFX.PlayLoop("тележка едет");
            switch (_dir)
            {
                case Dir.left_right:
                    transform.localRotation = Quaternion.Euler(Vector3.zero);

                    from = TelegaPos_Left;
                    to = TelegaPos_Right;
                    lastDir = Dir.left_right;
                    break;
                case Dir.right_left:
                    transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));

                    from = TelegaPos_Right;
                    to = TelegaPos_Left;
                    lastDir = Dir.right_left;
                    break;
                case Dir.reverce:
                    if (lastDir == Dir.left_right)
                    {
                        transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));

                        lastDir = Dir.right_left;
                        from = TelegaPos_Right;
                        to = TelegaPos_Left;
                    }
                    else
                    {
                        transform.localRotation = Quaternion.Euler(Vector3.zero);

                        lastDir = Dir.left_right;
                        to = TelegaPos_Right;
                        lastDir = Dir.left_right;

                    }
                    break;

            }
            transform.position = from.position;
            yield return new WaitForSeconds(1.5f);

            StartCoroutine(CheckTimeToHide());

            yield return StartCoroutine(StaticParams.MoveToCorrectPlaceTowards(transform, to, Time.deltaTime * 800));
            //yield return new WaitForSeconds(1.5f);
            SFX.StopLoop("тележка едет");

            transform.position = new Vector2(-100, -100);

        }
        IEnumerator CheckTimeToHide()
        {
            while (true)
            {
                if (lastDir == Dir.left_right)
                {
                    if (transform.position.x > CenterCamHidePos.position.x)
                    {
                        Debug.Log("break left_right");
                        break;
                    }
                }
                else
                {
                    if (transform.position.x < CenterCamHidePos.position.x)
                    {
                        Debug.Log("break");
                        break;
                    }
                }
                yield return null;
            }
 
            if (OnTimeToHide != null)
            {
                OnTimeToHide();
            }
        }
    }
}
