﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shevchenko.nScene3;
using PSV;

namespace Shevchenko
{
    public class Scene3 : GameController
    {
        List<Scene3_HidePoint> list_Scene3_HidePoint = new List<Scene3_HidePoint>();
        GameObject srcEffectSmoke;
        Transform curHidePoint;

        public override void AwakeOther()
        {
            list_Scene3_HidePoint.AddRange(FindObjectsOfType<Scene3_HidePoint>());
            list_Scene3_HidePoint = new List<Scene3_HidePoint>(list_Scene3_HidePoint.Shuffle());
            srcEffectSmoke = Resources.Load<GameObject>("Effects/Scene3Smoke");
        }
        public override void OnEnable()
        {
            base.OnEnable();
            Scene3_Telega.OnTimeToHide += Scene3_Telega_OnTimeToHide;
        }

        public override void OnDisable()
        {
            base.OnDisable();
            Scene3_Telega.OnTimeToHide -= Scene3_Telega_OnTimeToHide;

        }
        private void Scene3_Telega_OnTimeToHide()
        {

            george.transform.position = curHidePoint.position;
            george.SetMeHiddentIdle(true);

        }
        public override IEnumerator Process()
        {
            SFX.PlayLoop("фон базара", .3f);
            CameraController.instance
                .AllowMove(true)
                .SetTarget(pepa.transform, 2).
                SetCameraMovementType(CameraController.CameraMovementType.lerp)
                .SetScale(6);

            //ДЖИ - О-о-о. (удивленно)		ji-2
            yield return new WaitWhile(() => george.Talk("ji-2"));
            //ГИППИ - Ух ты! (удивленно)Ух ты! (удивленно)Ух ты! (удивленно)Ух ты! (удивленно)		hp-32
            yield return new WaitWhile(() => pepa.Talk("hp-32"));

            // Крупный план на Гиппи.
            //ГИППИ -Вот это да, восточный базар! Нужно хорошенько осмотреться здесь.		hp-33

            //yield return new WaitWhile(() => pepa.Talk("hp-33"));
            pepa.Say("hp-33");

            CameraController.instance.AllowMove(true)
                    .SetCameraMovementType(CameraController.CameraMovementType.towards)
                    .SetTarget("CamPos (1)")
                    .SetSpeedMove(8)
                    .SetScale(9);
            yield return new WaitForEndOfFrame();
            yield return new WaitWhile(() => CameraController.instance.IsCameraInMoveNow());
            CameraController.instance.SetTarget("CamPos (2)");
            yield return new WaitForEndOfFrame();
            yield return new WaitWhile(() => CameraController.instance.IsCameraInMoveNow());

            george.Walk("AwayPos");
            george.WalkSetWalkAnimType(WalkAnimType.SpecRun);

            CameraController.instance
    .AllowMove(true)
    .SetTarget(pepa.transform, 2).
    SetCameraMovementType(CameraController.CameraMovementType.lerp)
    .SetScale(6);
            yield return StartCoroutine(SmallGame());

            CameraController.instance
                        .AllowMove(true)
                        .SetTarget("CenterCamHidePos")
                        .SetCameraMovementType(CameraController.CameraMovementType.lerp)
                        .SetScale(6);
            george.Walk("FinPosGeorge");
            pepa.Walk("FinPosPepa");
            yield return new WaitWhile(() => pepa.IsWalkingNow());
            yield return new WaitWhile(() => george.IsWalkingNow());
            pepa.SetReverance();
            george.SetReverance();
            StaticParams.PushEffectFireworks();
            yield return new WaitForSeconds(2);

            MapProgressController.SetSceneCompleted();
            SFX.StopLoop("Толпа на рынке");

            SceneLoader.SwitchToScene(Scenes.Map);
        }

        public override IEnumerator SmallGame()
        {
            yield return null;
            //Ой, куда подевался Джи?		hp-34
            yield return new WaitWhile(() => pepa.Talk("hp-34"));
            //Давай отыщем Джи, пока он не напроказничал.		hp-35
            yield return new WaitWhile(() => pepa.Talk("hp-35"));

            list_Scene3_HidePoint = list_Scene3_HidePoint.Shuffle();

            for (int i = 0; i < 5; i++)
            {
                curHidePoint = list_Scene3_HidePoint[i].transform;
                if (i != 0)
                {


                    CameraController.instance.AllowMove(true)
    .SetCameraMovementType(CameraController.CameraMovementType.lerp)
    .SetTarget("CenterCamHidePos")
    /*.SetScale(13.72065f)*/;

                    yield return StartCoroutine(Scene3_Telega.instance.RunTelega(Scene3_Telega.Dir.reverce));
                }
                else
                {
                    george.transform.position = curHidePoint.position;

                }
                CameraController.instance.AllowMove(true)
.SetCameraMovementType(CameraController.CameraMovementType.lerp)
.SetTarget(list_Scene3_HidePoint[i].CamPosLookAt)
.SetScale(10);

                //GameObject effect = (GameObject)Instantiate(srcEffectSmoke);
                //effect.name = srcEffectSmoke.name;
                //effect.transform.position = list_Scene3_HidePoint[i].transform.position;
                //george.transform.position = list_Scene3_HidePoint[i].transform.position;

                //yield return new WaitForSeconds(2);


                ////Найди Джи и нажми на него.		hp-36
                //yield return new WaitWhile(() => pepa.Talk("hp-36"));
                pepa.TalkQueue("hp-36");
                Laugh(true);
                bool isGeorgeFound = false;

                float nextTimerEmotion = Random.Range(2f, 3f);
                float timer = 0;
                while (!isGeorgeFound)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        Debug.Log("Message");
                        Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        RaycastHit2D[] hits = Physics2D.RaycastAll(clickPosition, Vector2.zero);

                        if (hits != null)
                        {
                            foreach (RaycastHit2D hit in hits)
                            {
                                if (hit.collider.GetComponent<PlayerController>())
                                {
                                    if (hit.collider.GetComponent<PlayerController>().playerType ==
                                         PlayerType.george)
                                    {
                                        Scene3_PanelItem.ItemFound();
                                        isGeorgeFound = true;
                                        SFX.Play("Собирает элементы коврика, убирает вещи на чердаке, находит Джи на рынке");
                                        george.SetMeHiddentIdle(false);
                                        george.SetTriggerEmotion(EmotionTriggerType.Joy);

                                        pepa.StopTalking();
                                        // ГИППИ - Вот ты где!		hp-37	
                                        //ГИППИ - Попался!		hp-38
                                        yield return new WaitWhile(() => pepa.Talk(new string[] { "hp-37", "hp-38" }));

                                    }
                                }

                            }
                        }
                    }
                    yield return null;
                    timer += Time.deltaTime;
                    if (timer > nextTimerEmotion)
                    {
                        nextTimerEmotion = timer + Random.Range(2f, 3f);
                        george.SetTriggerEmotion(EmotionTriggerType.JoyFootOut);
                    }
                }
                yield return new WaitForSeconds(.5f);


                Laugh(false);
                //george.Walk(list_Scene3_HidePoint[i].list_posAfter.position);
                george.Walk(list_Scene3_HidePoint[i].list_posAfter[0].position);
                foreach (var item in list_Scene3_HidePoint[i].list_posAfter)
                {
                    george.WalkAddQueue(item);
                }
                //george.WalkAddQueue(list_Scene3_HidePoint[i].list_posAfter[0].position);

                yield return new WaitWhile(() => george.IsWalkingNow());
                george.SetTriggerEmotion(EmotionTriggerType.Joy);


            }
        }
        void Laugh(bool _enable)
        {
            if (_enable)
            {
                if (ieLaugh != null)  StopCoroutine(ieLaugh);
                ieLaugh = IeLaugh();
                StartCoroutine(ieLaugh);

            }else
            {
                if (ieLaugh != null) StopCoroutine(ieLaugh);

            }
        }
        IEnumerator ieLaugh;
        IEnumerator IeLaugh()
        {
            do
            {
                SFX.Play("Джи смеется 1");
                yield return new WaitWhile(() => SFX.IsSFXPlayingNow("Джи смеется 1"));
                yield return new WaitForSeconds(Random.Range(3, 6));

            } while (true);
        }
    }
}