﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Shevchenko.nScene3
{
    public class Scene3_HidePoint : MonoBehaviour
    {
        public List<Transform> list_posAfter = new List<Transform>();
        public Transform CamPosLookAt;

        private void Awake()
        {

            foreach (var item in GetComponentsInChildren<Transform>(true))
            {
                if (item.name.Contains("PosAfter"))
                {
                    list_posAfter.Add(item);
                }
                else if (item.name.Contains("CamPosLookAt"))
                {
                    CamPosLookAt = item;
                }
            }
            list_posAfter.Sort(delegate (Transform us1, Transform us2)
            { return StaticParams.GetNumberFromName(us1.name).CompareTo(StaticParams.GetNumberFromName(us2.name)); });

        }
    }
}