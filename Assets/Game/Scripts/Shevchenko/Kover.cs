﻿using UnityEngine;
using System.Collections;
namespace Shevchenko
{
    public class Kover : MonoBehaviour
    {
        public static Kover instance;
        public Material mat;
        MeshRenderer currentMesh;
        Animator anim;

        [Header("При отсутствии сохранённой загрузит дефолтную")]
        public bool isUseDefaultTexture;
        public Texture2D defKoverTexture;

        private void Awake()
        {
            instance = this;
            currentMesh = GetComponentInChildren<MeshRenderer>();
            anim = GetComponentInChildren<Animator>();
        }
        void Start()
        {
            StartCoroutine(IeLoadSavedTexture());
        }
        IEnumerator IeLoadSavedTexture()
        {
            yield return new WaitForEndOfFrame();
            LoadSavedTexture();
        }
        void Update()
        {

            if (Input.GetKeyDown(KeyCode.G))
            {

                LoadSavedTexture();
                Debug.Log("KeyCode.G");

            }
        }
        public void LoadSavedTexture()
        {
            Debug.Log("LoadSavedTexture");
            // Create a texture. Texture size does not matter, since
            // LoadImage will replace with with incoming image size.
            string fullPath = Application.persistentDataPath + "/";
            //string fileName = currentMeshCollider.transform.parent.name + currentMeshCollider.name + ".png";
            string fileName = "KoverBlendPlane.png";
            Texture2D tex = new Texture2D(2, 2);

            try
            {
                tex.LoadImage(System.IO.File.ReadAllBytes(fullPath + fileName));
                //GetComponent<Renderer>().material.mainTexture = tex;

                mat.mainTexture = tex;
                currentMesh.material = mat;
            }
            catch (System.Exception)
            {

                Debug.Log("Текстура не существует\n " + fullPath + fileName);
                if (isUseDefaultTexture)
                {
                    mat.mainTexture = defKoverTexture;
                    currentMesh.material = mat;

                }
            }

        }

        public void SetFly(bool _fly)
        {
            if (_fly)
            {
                SFX.Play("Дует ветер когда они летят на ковре",.3f);
            }else
            {
                SFX.StopLoop("Дует ветер когда они летят на ковре" );

            }
            anim.SetBool("fly", _fly);

        }
        public void ObstacleHit()
        {
            anim.SetTrigger("hit");
        }
        bool isUpAnimAllowed = true;
        public void UpClick()
        {
            isUpAnimAllowed = false;
            if (!anim.GetBool("dirUp"))
            {
                anim.SetTrigger("up");
             }
            anim.SetBool("dirUp", true);

        }
        public void FlyUp()
        {
 
            anim.SetBool("dirUp", true);

        }
        public void FlyDown()
        {
             anim.SetBool("dirUp", false);
            isUpAnimAllowed = true;
        }
    }
}
